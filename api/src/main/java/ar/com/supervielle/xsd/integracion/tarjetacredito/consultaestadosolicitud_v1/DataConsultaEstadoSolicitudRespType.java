
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadosolicitud_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCtaTarjetaType;


/**
 * <p>Clase Java para DataConsultaEstadoSolicitudRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaEstadoSolicitudRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="secuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="TipoSolicitud">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="Marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Cuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idCtaTarjetaType"/>
 *                   &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="secuEmbozadoVisa" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="SubCanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Estados">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Estado" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Error" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaEstadoSolicitudRespType", propOrder = {
    "row"
})
public class DataConsultaEstadoSolicitudRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaEstadoSolicitudRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaEstadoSolicitudRespType.Row }
     * 
     * 
     */
    public List<DataConsultaEstadoSolicitudRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaEstadoSolicitudRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="secuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="TipoSolicitud">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="Marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Cuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idCtaTarjetaType"/>
     *         &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="secuEmbozadoVisa" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="SubCanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Estados">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Estado" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Error" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroSolicitud",
        "secuencia",
        "tipoSolicitud",
        "fechaSolicitud",
        "marca",
        "cuenta",
        "numeroTarjeta",
        "secuEmbozadoVisa",
        "cliente",
        "canal",
        "subCanal",
        "estados",
        "error"
    })
    public static class Row {

        protected int nroSolicitud;
        protected int secuencia;
        @XmlElement(name = "TipoSolicitud", required = true)
        protected DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud tipoSolicitud;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaSolicitud;
        @XmlElement(name = "Marca", required = true)
        protected CodDescNumType marca;
        @XmlElement(name = "Cuenta", required = true)
        protected IdCtaTarjetaType cuenta;
        @XmlElement(required = true)
        protected String numeroTarjeta;
        protected int secuEmbozadoVisa;
        @XmlElement(name = "Cliente", required = true)
        protected IdClienteType cliente;
        @XmlElement(name = "Canal", required = true)
        protected CodDescNumType canal;
        @XmlElement(name = "SubCanal", required = true)
        protected CodDescNumType subCanal;
        @XmlElement(name = "Estados", required = true)
        protected DataConsultaEstadoSolicitudRespType.Row.Estados estados;
        @XmlElement(name = "Error", required = true)
        protected CodDescStringType error;

        /**
         * Obtiene el valor de la propiedad nroSolicitud.
         * 
         */
        public int getNroSolicitud() {
            return nroSolicitud;
        }

        /**
         * Define el valor de la propiedad nroSolicitud.
         * 
         */
        public void setNroSolicitud(int value) {
            this.nroSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad secuencia.
         * 
         */
        public int getSecuencia() {
            return secuencia;
        }

        /**
         * Define el valor de la propiedad secuencia.
         * 
         */
        public void setSecuencia(int value) {
            this.secuencia = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud }
         *     
         */
        public DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud getTipoSolicitud() {
            return tipoSolicitud;
        }

        /**
         * Define el valor de la propiedad tipoSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud }
         *     
         */
        public void setTipoSolicitud(DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud value) {
            this.tipoSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaSolicitud() {
            return fechaSolicitud;
        }

        /**
         * Define el valor de la propiedad fechaSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaSolicitud(XMLGregorianCalendar value) {
            this.fechaSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad marca.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getMarca() {
            return marca;
        }

        /**
         * Define el valor de la propiedad marca.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setMarca(CodDescNumType value) {
            this.marca = value;
        }

        /**
         * Obtiene el valor de la propiedad cuenta.
         * 
         * @return
         *     possible object is
         *     {@link IdCtaTarjetaType }
         *     
         */
        public IdCtaTarjetaType getCuenta() {
            return cuenta;
        }

        /**
         * Define el valor de la propiedad cuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCtaTarjetaType }
         *     
         */
        public void setCuenta(IdCtaTarjetaType value) {
            this.cuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroTarjeta() {
            return numeroTarjeta;
        }

        /**
         * Define el valor de la propiedad numeroTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroTarjeta(String value) {
            this.numeroTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad secuEmbozadoVisa.
         * 
         */
        public int getSecuEmbozadoVisa() {
            return secuEmbozadoVisa;
        }

        /**
         * Define el valor de la propiedad secuEmbozadoVisa.
         * 
         */
        public void setSecuEmbozadoVisa(int value) {
            this.secuEmbozadoVisa = value;
        }

        /**
         * Obtiene el valor de la propiedad cliente.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getCliente() {
            return cliente;
        }

        /**
         * Define el valor de la propiedad cliente.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setCliente(IdClienteType value) {
            this.cliente = value;
        }

        /**
         * Obtiene el valor de la propiedad canal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getCanal() {
            return canal;
        }

        /**
         * Define el valor de la propiedad canal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setCanal(CodDescNumType value) {
            this.canal = value;
        }

        /**
         * Obtiene el valor de la propiedad subCanal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getSubCanal() {
            return subCanal;
        }

        /**
         * Define el valor de la propiedad subCanal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setSubCanal(CodDescNumType value) {
            this.subCanal = value;
        }

        /**
         * Obtiene el valor de la propiedad estados.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaEstadoSolicitudRespType.Row.Estados }
         *     
         */
        public DataConsultaEstadoSolicitudRespType.Row.Estados getEstados() {
            return estados;
        }

        /**
         * Define el valor de la propiedad estados.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaEstadoSolicitudRespType.Row.Estados }
         *     
         */
        public void setEstados(DataConsultaEstadoSolicitudRespType.Row.Estados value) {
            this.estados = value;
        }

        /**
         * Obtiene el valor de la propiedad error.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getError() {
            return error;
        }

        /**
         * Define el valor de la propiedad error.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setError(CodDescStringType value) {
            this.error = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Estado" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "estado"
        })
        public static class Estados {

            @XmlElement(name = "Estado", required = true)
            protected List<DataConsultaEstadoSolicitudRespType.Row.Estados.Estado> estado;

            /**
             * Gets the value of the estado property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the estado property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEstado().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaEstadoSolicitudRespType.Row.Estados.Estado }
             * 
             * 
             */
            public List<DataConsultaEstadoSolicitudRespType.Row.Estados.Estado> getEstado() {
                if (estado == null) {
                    estado = new ArrayList<DataConsultaEstadoSolicitudRespType.Row.Estados.Estado>();
                }
                return this.estado;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "fecha"
            })
            public static class Estado {

                protected int codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar fecha;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 */
                public int getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 */
                public void setCodigo(int value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad fecha.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFecha() {
                    return fecha;
                }

                /**
                 * Define el valor de la propiedad fecha.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFecha(XMLGregorianCalendar value) {
                    this.fecha = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "grupo",
            "codigo",
            "descripcion"
        })
        public static class TipoSolicitud {

            @XmlElement(required = true)
            protected String grupo;
            protected int codigo;
            @XmlElement(required = true)
            protected String descripcion;

            /**
             * Obtiene el valor de la propiedad grupo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGrupo() {
                return grupo;
            }

            /**
             * Define el valor de la propiedad grupo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGrupo(String value) {
                this.grupo = value;
            }

            /**
             * Obtiene el valor de la propiedad codigo.
             * 
             */
            public int getCodigo() {
                return codigo;
            }

            /**
             * Define el valor de la propiedad codigo.
             * 
             */
            public void setCodigo(int value) {
                this.codigo = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Define el valor de la propiedad descripcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

        }

    }

}
