
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaSaldosRespType }
     * 
     */
    public DataConsultaSaldosRespType createDataConsultaSaldosRespType() {
        return new DataConsultaSaldosRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaSaldos }
     * 
     */
    public ReqConsultaSaldos createReqConsultaSaldos() {
        return new ReqConsultaSaldos();
    }

    /**
     * Create an instance of {@link DataConsultaSaldosReqType }
     * 
     */
    public DataConsultaSaldosReqType createDataConsultaSaldosReqType() {
        return new DataConsultaSaldosReqType();
    }

    /**
     * Create an instance of {@link RespConsultaSaldos }
     * 
     */
    public RespConsultaSaldos createRespConsultaSaldos() {
        return new RespConsultaSaldos();
    }

    /**
     * Create an instance of {@link DataConsultaSaldosRespType.Row }
     * 
     */
    public DataConsultaSaldosRespType.Row createDataConsultaSaldosRespTypeRow() {
        return new DataConsultaSaldosRespType.Row();
    }

}
