
package ar.com.supervielle.xsd.integracion.clienteprevisional.altaanticipohaberes_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.clienteprevisional.altaanticipohaberes_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.clienteprevisional.altaanticipohaberes_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesReqType2 }
     * 
     */
    public DataAltaAnticipoHaberesReqType2 createDataAltaAnticipoHaberesReqType2() {
        return new DataAltaAnticipoHaberesReqType2();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesReqType2 .AltaAnticipo }
     * 
     */
    public DataAltaAnticipoHaberesReqType2 .AltaAnticipo createDataAltaAnticipoHaberesReqType2AltaAnticipo() {
        return new DataAltaAnticipoHaberesReqType2 .AltaAnticipo();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesRespType2 }
     * 
     */
    public DataAltaAnticipoHaberesRespType2 createDataAltaAnticipoHaberesRespType2() {
        return new DataAltaAnticipoHaberesRespType2();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesRespType2 .Row }
     * 
     */
    public DataAltaAnticipoHaberesRespType2 .Row createDataAltaAnticipoHaberesRespType2Row() {
        return new DataAltaAnticipoHaberesRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesRespType }
     * 
     */
    public DataAltaAnticipoHaberesRespType createDataAltaAnticipoHaberesRespType() {
        return new DataAltaAnticipoHaberesRespType();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesRespType.Row }
     * 
     */
    public DataAltaAnticipoHaberesRespType.Row createDataAltaAnticipoHaberesRespTypeRow() {
        return new DataAltaAnticipoHaberesRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesReqType }
     * 
     */
    public DataAltaAnticipoHaberesReqType createDataAltaAnticipoHaberesReqType() {
        return new DataAltaAnticipoHaberesReqType();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesReqType.AltaAnticipo }
     * 
     */
    public DataAltaAnticipoHaberesReqType.AltaAnticipo createDataAltaAnticipoHaberesReqTypeAltaAnticipo() {
        return new DataAltaAnticipoHaberesReqType.AltaAnticipo();
    }

    /**
     * Create an instance of {@link ReqAltaAnticipoHaberes }
     * 
     */
    public ReqAltaAnticipoHaberes createReqAltaAnticipoHaberes() {
        return new ReqAltaAnticipoHaberes();
    }

    /**
     * Create an instance of {@link RespAltaAnticipoHaberes }
     * 
     */
    public RespAltaAnticipoHaberes createRespAltaAnticipoHaberes() {
        return new RespAltaAnticipoHaberes();
    }

    /**
     * Create an instance of {@link RespAltaAnticipoHaberesV11 }
     * 
     */
    public RespAltaAnticipoHaberesV11 createRespAltaAnticipoHaberesV11() {
        return new RespAltaAnticipoHaberesV11();
    }

    /**
     * Create an instance of {@link ReqAltaAnticipoHaberesV11 }
     * 
     */
    public ReqAltaAnticipoHaberesV11 createReqAltaAnticipoHaberesV11() {
        return new ReqAltaAnticipoHaberesV11();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesReqType2 .AltaAnticipo.Anticipo }
     * 
     */
    public DataAltaAnticipoHaberesReqType2 .AltaAnticipo.Anticipo createDataAltaAnticipoHaberesReqType2AltaAnticipoAnticipo() {
        return new DataAltaAnticipoHaberesReqType2 .AltaAnticipo.Anticipo();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesRespType2 .Row.Respuesta }
     * 
     */
    public DataAltaAnticipoHaberesRespType2 .Row.Respuesta createDataAltaAnticipoHaberesRespType2RowRespuesta() {
        return new DataAltaAnticipoHaberesRespType2 .Row.Respuesta();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesRespType.Row.Respuesta }
     * 
     */
    public DataAltaAnticipoHaberesRespType.Row.Respuesta createDataAltaAnticipoHaberesRespTypeRowRespuesta() {
        return new DataAltaAnticipoHaberesRespType.Row.Respuesta();
    }

    /**
     * Create an instance of {@link DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo }
     * 
     */
    public DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo createDataAltaAnticipoHaberesReqTypeAltaAnticipoAnticipo() {
        return new DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo();
    }

}
