
package ar.com.supervielle.xsd.integracion.cliente.consultaclientexdoc_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaClientexDocReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaClientexDocReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="paisOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaClientexDocReqType", propOrder = {
    "identificador"
})
public class DataConsultaClientexDocReqType {

    @XmlElement(required = true)
    protected DataConsultaClientexDocReqType.Identificador identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaClientexDocReqType.Identificador }
     *     
     */
    public DataConsultaClientexDocReqType.Identificador getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaClientexDocReqType.Identificador }
     *     
     */
    public void setIdentificador(DataConsultaClientexDocReqType.Identificador value) {
        this.identificador = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="paisOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paisOrigen",
        "tipoDoc",
        "numDoc"
    })
    public static class Identificador {

        protected String paisOrigen;
        protected String tipoDoc;
        @XmlElement(required = true)
        protected String numDoc;

        /**
         * Obtiene el valor de la propiedad paisOrigen.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaisOrigen() {
            return paisOrigen;
        }

        /**
         * Define el valor de la propiedad paisOrigen.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaisOrigen(String value) {
            this.paisOrigen = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoDoc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoDoc() {
            return tipoDoc;
        }

        /**
         * Define el valor de la propiedad tipoDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoDoc(String value) {
            this.tipoDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad numDoc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumDoc() {
            return numDoc;
        }

        /**
         * Define el valor de la propiedad numDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumDoc(String value) {
            this.numDoc = value;
        }

    }

}
