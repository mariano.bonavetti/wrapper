
package ar.com.supervielle.xsd.integracion.clienteprevisional.altaanticipohaberes_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataAltaAnticipoHaberesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAnticipoHaberesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="beneficio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="respuesta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Exception" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAnticipoHaberesRespType", propOrder = {
    "row"
})
public class DataAltaAnticipoHaberesRespType2 {

    @XmlElement(name = "Row")
    protected List<DataAltaAnticipoHaberesRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataAltaAnticipoHaberesRespType2 .Row }
     * 
     * 
     */
    public List<DataAltaAnticipoHaberesRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataAltaAnticipoHaberesRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="beneficio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="respuesta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Exception" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "beneficio",
        "respuesta"
    })
    public static class Row {

        @XmlElement(required = true)
        protected BigInteger beneficio;
        @XmlElement(required = true)
        protected DataAltaAnticipoHaberesRespType2 .Row.Respuesta respuesta;

        /**
         * Obtiene el valor de la propiedad beneficio.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBeneficio() {
            return beneficio;
        }

        /**
         * Define el valor de la propiedad beneficio.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBeneficio(BigInteger value) {
            this.beneficio = value;
        }

        /**
         * Obtiene el valor de la propiedad respuesta.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaAnticipoHaberesRespType2 .Row.Respuesta }
         *     
         */
        public DataAltaAnticipoHaberesRespType2 .Row.Respuesta getRespuesta() {
            return respuesta;
        }

        /**
         * Define el valor de la propiedad respuesta.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaAnticipoHaberesRespType2 .Row.Respuesta }
         *     
         */
        public void setRespuesta(DataAltaAnticipoHaberesRespType2 .Row.Respuesta value) {
            this.respuesta = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Exception" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "estado",
            "exception"
        })
        public static class Respuesta {

            @XmlElement(required = true)
            protected String estado;
            @XmlElement(name = "Exception")
            protected Object exception;

            /**
             * Obtiene el valor de la propiedad estado.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEstado() {
                return estado;
            }

            /**
             * Define el valor de la propiedad estado.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEstado(String value) {
                this.estado = value;
            }

            /**
             * Obtiene el valor de la propiedad exception.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getException() {
                return exception;
            }

            /**
             * Define el valor de la propiedad exception.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setException(Object value) {
                this.exception = value;
            }

        }

    }

}
