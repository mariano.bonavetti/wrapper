
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para idMovimientoPagoSueldoType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="idMovimientoPagoSueldoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="transaccion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="numeroRelacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="fechaContabilizacion" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="fechaValor" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="correlacionAsiento" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="usuarioIngreso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuarioConfirmacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sistema" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "idMovimientoPagoSueldoType", propOrder = {
    "empresa",
    "sucursal",
    "modulo",
    "transaccion",
    "numeroRelacion",
    "fechaContabilizacion",
    "fechaValor",
    "correlacionAsiento",
    "usuarioIngreso",
    "usuarioConfirmacion",
    "sistema"
})
public class IdMovimientoPagoSueldoType {

    @XmlElement(required = true)
    protected BigDecimal empresa;
    @XmlElement(required = true)
    protected BigDecimal sucursal;
    @XmlElement(required = true)
    protected BigDecimal modulo;
    @XmlElement(required = true)
    protected BigDecimal transaccion;
    @XmlElement(required = true)
    protected BigDecimal numeroRelacion;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaContabilizacion;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaValor;
    @XmlElement(required = true)
    protected BigDecimal correlacionAsiento;
    @XmlElement(required = true)
    protected String usuarioIngreso;
    @XmlElement(required = true)
    protected String usuarioConfirmacion;
    @XmlElement(required = true)
    protected String sistema;

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEmpresa(BigDecimal value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSucursal() {
        return sucursal;
    }

    /**
     * Define el valor de la propiedad sucursal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSucursal(BigDecimal value) {
        this.sucursal = value;
    }

    /**
     * Obtiene el valor de la propiedad modulo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getModulo() {
        return modulo;
    }

    /**
     * Define el valor de la propiedad modulo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setModulo(BigDecimal value) {
        this.modulo = value;
    }

    /**
     * Obtiene el valor de la propiedad transaccion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransaccion() {
        return transaccion;
    }

    /**
     * Define el valor de la propiedad transaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransaccion(BigDecimal value) {
        this.transaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroRelacion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumeroRelacion() {
        return numeroRelacion;
    }

    /**
     * Define el valor de la propiedad numeroRelacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumeroRelacion(BigDecimal value) {
        this.numeroRelacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaContabilizacion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaContabilizacion() {
        return fechaContabilizacion;
    }

    /**
     * Define el valor de la propiedad fechaContabilizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaContabilizacion(XMLGregorianCalendar value) {
        this.fechaContabilizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValor.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaValor() {
        return fechaValor;
    }

    /**
     * Define el valor de la propiedad fechaValor.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaValor(XMLGregorianCalendar value) {
        this.fechaValor = value;
    }

    /**
     * Obtiene el valor de la propiedad correlacionAsiento.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCorrelacionAsiento() {
        return correlacionAsiento;
    }

    /**
     * Define el valor de la propiedad correlacionAsiento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCorrelacionAsiento(BigDecimal value) {
        this.correlacionAsiento = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioIngreso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioIngreso() {
        return usuarioIngreso;
    }

    /**
     * Define el valor de la propiedad usuarioIngreso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioIngreso(String value) {
        this.usuarioIngreso = value;
    }

    /**
     * Obtiene el valor de la propiedad usuarioConfirmacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuarioConfirmacion() {
        return usuarioConfirmacion;
    }

    /**
     * Define el valor de la propiedad usuarioConfirmacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuarioConfirmacion(String value) {
        this.usuarioConfirmacion = value;
    }

    /**
     * Obtiene el valor de la propiedad sistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistema() {
        return sistema;
    }

    /**
     * Define el valor de la propiedad sistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistema(String value) {
        this.sistema = value;
    }

}
