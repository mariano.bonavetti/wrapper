
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadas_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaPersonasVinculadasRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaPersonasVinculadasRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="pais" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="tipoDoc" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="vinculacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaPersonasVinculadasRespType", propOrder = {
    "row"
})
public class DataConsultaPersonasVinculadasRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaPersonasVinculadasRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaPersonasVinculadasRespType.Row }
     * 
     * 
     */
    public List<DataConsultaPersonasVinculadasRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaPersonasVinculadasRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="pais" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="tipoDoc" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="vinculacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "pais",
        "tipoDoc",
        "numero",
        "nombreApellido",
        "vinculacion",
        "titularidad"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected CodDescNumType pais;
        @XmlElement(required = true)
        protected CodDescNumType tipoDoc;
        @XmlElement(required = true)
        protected String numero;
        @XmlElement(required = true)
        protected String nombreApellido;
        @XmlElement(required = true)
        protected CodDescStringType vinculacion;
        @XmlElement(required = true)
        protected CodDescStringType titularidad;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad pais.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getPais() {
            return pais;
        }

        /**
         * Define el valor de la propiedad pais.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setPais(CodDescNumType value) {
            this.pais = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoDoc.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getTipoDoc() {
            return tipoDoc;
        }

        /**
         * Define el valor de la propiedad tipoDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setTipoDoc(CodDescNumType value) {
            this.tipoDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad numero.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumero() {
            return numero;
        }

        /**
         * Define el valor de la propiedad numero.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumero(String value) {
            this.numero = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreApellido.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreApellido() {
            return nombreApellido;
        }

        /**
         * Define el valor de la propiedad nombreApellido.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreApellido(String value) {
            this.nombreApellido = value;
        }

        /**
         * Obtiene el valor de la propiedad vinculacion.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getVinculacion() {
            return vinculacion;
        }

        /**
         * Define el valor de la propiedad vinculacion.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setVinculacion(CodDescStringType value) {
            this.vinculacion = value;
        }

        /**
         * Obtiene el valor de la propiedad titularidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTitularidad() {
            return titularidad;
        }

        /**
         * Define el valor de la propiedad titularidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTitularidad(CodDescStringType value) {
            this.titularidad = value;
        }

    }

}
