
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaproductoscorporativos_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataAltaProductosCorporativosReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaProductosCorporativosReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmpresaYCuenta" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdentificadorSolicitud">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Oficial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
 *                   &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="denominacionBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="codigoBanca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Limites">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="limiteCompraCuenta" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="tipoIVA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="cartera" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="grupoAfinidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="nombreContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="comercioMadre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RubrosHabilitados" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RubrosInhabilitados" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Domicilio" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="codigoGeografico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="emailUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="emailDominio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="activaControlLimite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CuentaDebito" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tipoDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DomicilioCorrespondencia" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="codigoLimitePrestamo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Tarjetas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Tarjeta" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdentificadorSolicitud">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                       &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                       &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Oficial" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="cuentaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                             &lt;element name="DatosCliente">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;choice>
 *                                         &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="datosClienteNuevo">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                   &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                   &lt;element name="Domicilio" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                             &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                             &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                             &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/choice>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="embozado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="documentoTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                             &lt;element name="sucursalOrigenBT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                             &lt;element name="habilitacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="Limites">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                                       &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                                       &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                                       &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaProductosCorporativosReqType", propOrder = {
    "empresaYCuenta",
    "tarjetas"
})
public class DataAltaProductosCorporativosReqType {

    @XmlElement(name = "EmpresaYCuenta")
    protected DataAltaProductosCorporativosReqType.EmpresaYCuenta empresaYCuenta;
    @XmlElement(name = "Tarjetas")
    protected DataAltaProductosCorporativosReqType.Tarjetas tarjetas;

    /**
     * Obtiene el valor de la propiedad empresaYCuenta.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta }
     *     
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta getEmpresaYCuenta() {
        return empresaYCuenta;
    }

    /**
     * Define el valor de la propiedad empresaYCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta }
     *     
     */
    public void setEmpresaYCuenta(DataAltaProductosCorporativosReqType.EmpresaYCuenta value) {
        this.empresaYCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjetas.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaProductosCorporativosReqType.Tarjetas }
     *     
     */
    public DataAltaProductosCorporativosReqType.Tarjetas getTarjetas() {
        return tarjetas;
    }

    /**
     * Define el valor de la propiedad tarjetas.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaProductosCorporativosReqType.Tarjetas }
     *     
     */
    public void setTarjetas(DataAltaProductosCorporativosReqType.Tarjetas value) {
        this.tarjetas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdentificadorSolicitud">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Oficial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
     *         &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="denominacionBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="codigoBanca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Limites">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="limiteCompraCuenta" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tipoIVA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="cartera" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="grupoAfinidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="nombreContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="comercioMadre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RubrosHabilitados" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RubrosInhabilitados" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Domicilio" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codigoGeografico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="emailUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="emailDominio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="activaControlLimite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CuentaDebito" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tipoDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DomicilioCorrespondencia" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="codigoLimitePrestamo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificadorSolicitud",
        "codigoMarca",
        "codigoEntidad",
        "oficial",
        "identificador",
        "cuentaBT",
        "denominacionBT",
        "codigoBanca",
        "tipoProducto",
        "codigoEmpresa",
        "denominacion",
        "cuartaLinea",
        "limites",
        "tipoIVA",
        "cartera",
        "grupoAfinidad",
        "nombreContacto",
        "comercioMadre",
        "rubrosHabilitados",
        "rubrosInhabilitados",
        "domicilio",
        "emailUsuario",
        "emailDominio",
        "sucursal",
        "modeloLiquidacion",
        "activaControlLimite",
        "formaPago",
        "cuentaDebito",
        "domicilioCorrespondencia",
        "codigoLimitePrestamo"
    })
    public static class EmpresaYCuenta {

        @XmlElement(name = "IdentificadorSolicitud", required = true)
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud identificadorSolicitud;
        @XmlElement(required = true)
        protected String codigoMarca;
        @XmlElement(required = true)
        protected String codigoEntidad;
        @XmlElement(name = "Oficial")
        protected CodDescStringType oficial;
        @XmlElement(name = "Identificador")
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected String cuentaBT;
        protected String denominacionBT;
        protected Integer codigoBanca;
        protected BigInteger tipoProducto;
        protected String codigoEmpresa;
        protected String denominacion;
        protected String cuartaLinea;
        @XmlElement(name = "Limites", required = true)
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites limites;
        protected BigInteger tipoIVA;
        protected BigInteger cartera;
        protected String grupoAfinidad;
        protected String nombreContacto;
        protected String comercioMadre;
        @XmlElement(name = "RubrosHabilitados")
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados rubrosHabilitados;
        @XmlElement(name = "RubrosInhabilitados")
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados rubrosInhabilitados;
        @XmlElement(name = "Domicilio")
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio domicilio;
        protected String emailUsuario;
        protected String emailDominio;
        protected Integer sucursal;
        protected String modeloLiquidacion;
        protected String activaControlLimite;
        protected String formaPago;
        @XmlElement(name = "CuentaDebito")
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito cuentaDebito;
        @XmlElement(name = "DomicilioCorrespondencia")
        protected DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia domicilioCorrespondencia;
        protected Integer codigoLimitePrestamo;

        /**
         * Obtiene el valor de la propiedad identificadorSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud getIdentificadorSolicitud() {
            return identificadorSolicitud;
        }

        /**
         * Define el valor de la propiedad identificadorSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud }
         *     
         */
        public void setIdentificadorSolicitud(DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud value) {
            this.identificadorSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoMarca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoMarca() {
            return codigoMarca;
        }

        /**
         * Define el valor de la propiedad codigoMarca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoMarca(String value) {
            this.codigoMarca = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoEntidad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoEntidad() {
            return codigoEntidad;
        }

        /**
         * Define el valor de la propiedad codigoEntidad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoEntidad(String value) {
            this.codigoEntidad = value;
        }

        /**
         * Obtiene el valor de la propiedad oficial.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getOficial() {
            return oficial;
        }

        /**
         * Define el valor de la propiedad oficial.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setOficial(CodDescStringType value) {
            this.oficial = value;
        }

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaBT.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaBT() {
            return cuentaBT;
        }

        /**
         * Define el valor de la propiedad cuentaBT.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaBT(String value) {
            this.cuentaBT = value;
        }

        /**
         * Obtiene el valor de la propiedad denominacionBT.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDenominacionBT() {
            return denominacionBT;
        }

        /**
         * Define el valor de la propiedad denominacionBT.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDenominacionBT(String value) {
            this.denominacionBT = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoBanca.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodigoBanca() {
            return codigoBanca;
        }

        /**
         * Define el valor de la propiedad codigoBanca.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodigoBanca(Integer value) {
            this.codigoBanca = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoProducto.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTipoProducto() {
            return tipoProducto;
        }

        /**
         * Define el valor de la propiedad tipoProducto.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTipoProducto(BigInteger value) {
            this.tipoProducto = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoEmpresa.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoEmpresa() {
            return codigoEmpresa;
        }

        /**
         * Define el valor de la propiedad codigoEmpresa.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoEmpresa(String value) {
            this.codigoEmpresa = value;
        }

        /**
         * Obtiene el valor de la propiedad denominacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDenominacion() {
            return denominacion;
        }

        /**
         * Define el valor de la propiedad denominacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDenominacion(String value) {
            this.denominacion = value;
        }

        /**
         * Obtiene el valor de la propiedad cuartaLinea.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuartaLinea() {
            return cuartaLinea;
        }

        /**
         * Define el valor de la propiedad cuartaLinea.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuartaLinea(String value) {
            this.cuartaLinea = value;
        }

        /**
         * Obtiene el valor de la propiedad limites.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites getLimites() {
            return limites;
        }

        /**
         * Define el valor de la propiedad limites.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites }
         *     
         */
        public void setLimites(DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites value) {
            this.limites = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoIVA.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTipoIVA() {
            return tipoIVA;
        }

        /**
         * Define el valor de la propiedad tipoIVA.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTipoIVA(BigInteger value) {
            this.tipoIVA = value;
        }

        /**
         * Obtiene el valor de la propiedad cartera.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCartera() {
            return cartera;
        }

        /**
         * Define el valor de la propiedad cartera.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCartera(BigInteger value) {
            this.cartera = value;
        }

        /**
         * Obtiene el valor de la propiedad grupoAfinidad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGrupoAfinidad() {
            return grupoAfinidad;
        }

        /**
         * Define el valor de la propiedad grupoAfinidad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGrupoAfinidad(String value) {
            this.grupoAfinidad = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreContacto.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreContacto() {
            return nombreContacto;
        }

        /**
         * Define el valor de la propiedad nombreContacto.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreContacto(String value) {
            this.nombreContacto = value;
        }

        /**
         * Obtiene el valor de la propiedad comercioMadre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComercioMadre() {
            return comercioMadre;
        }

        /**
         * Define el valor de la propiedad comercioMadre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComercioMadre(String value) {
            this.comercioMadre = value;
        }

        /**
         * Obtiene el valor de la propiedad rubrosHabilitados.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados getRubrosHabilitados() {
            return rubrosHabilitados;
        }

        /**
         * Define el valor de la propiedad rubrosHabilitados.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados }
         *     
         */
        public void setRubrosHabilitados(DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados value) {
            this.rubrosHabilitados = value;
        }

        /**
         * Obtiene el valor de la propiedad rubrosInhabilitados.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados getRubrosInhabilitados() {
            return rubrosInhabilitados;
        }

        /**
         * Define el valor de la propiedad rubrosInhabilitados.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados }
         *     
         */
        public void setRubrosInhabilitados(DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados value) {
            this.rubrosInhabilitados = value;
        }

        /**
         * Obtiene el valor de la propiedad domicilio.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio getDomicilio() {
            return domicilio;
        }

        /**
         * Define el valor de la propiedad domicilio.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio }
         *     
         */
        public void setDomicilio(DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio value) {
            this.domicilio = value;
        }

        /**
         * Obtiene el valor de la propiedad emailUsuario.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmailUsuario() {
            return emailUsuario;
        }

        /**
         * Define el valor de la propiedad emailUsuario.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmailUsuario(String value) {
            this.emailUsuario = value;
        }

        /**
         * Obtiene el valor de la propiedad emailDominio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmailDominio() {
            return emailDominio;
        }

        /**
         * Define el valor de la propiedad emailDominio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmailDominio(String value) {
            this.emailDominio = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSucursal(Integer value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad modeloLiquidacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModeloLiquidacion() {
            return modeloLiquidacion;
        }

        /**
         * Define el valor de la propiedad modeloLiquidacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModeloLiquidacion(String value) {
            this.modeloLiquidacion = value;
        }

        /**
         * Obtiene el valor de la propiedad activaControlLimite.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivaControlLimite() {
            return activaControlLimite;
        }

        /**
         * Define el valor de la propiedad activaControlLimite.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivaControlLimite(String value) {
            this.activaControlLimite = value;
        }

        /**
         * Obtiene el valor de la propiedad formaPago.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFormaPago() {
            return formaPago;
        }

        /**
         * Define el valor de la propiedad formaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFormaPago(String value) {
            this.formaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaDebito.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito getCuentaDebito() {
            return cuentaDebito;
        }

        /**
         * Define el valor de la propiedad cuentaDebito.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito }
         *     
         */
        public void setCuentaDebito(DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito value) {
            this.cuentaDebito = value;
        }

        /**
         * Obtiene el valor de la propiedad domicilioCorrespondencia.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia }
         *     
         */
        public DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia getDomicilioCorrespondencia() {
            return domicilioCorrespondencia;
        }

        /**
         * Define el valor de la propiedad domicilioCorrespondencia.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia }
         *     
         */
        public void setDomicilioCorrespondencia(DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia value) {
            this.domicilioCorrespondencia = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoLimitePrestamo.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodigoLimitePrestamo() {
            return codigoLimitePrestamo;
        }

        /**
         * Define el valor de la propiedad codigoLimitePrestamo.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodigoLimitePrestamo(Integer value) {
            this.codigoLimitePrestamo = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tipoDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tipoDebito",
            "sucursal",
            "tipo",
            "numero"
        })
        public static class CuentaDebito {

            protected String tipoDebito;
            protected String sucursal;
            protected String tipo;
            protected String numero;

            /**
             * Obtiene el valor de la propiedad tipoDebito.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipoDebito() {
                return tipoDebito;
            }

            /**
             * Define el valor de la propiedad tipoDebito.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipoDebito(String value) {
                this.tipoDebito = value;
            }

            /**
             * Obtiene el valor de la propiedad sucursal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSucursal() {
                return sucursal;
            }

            /**
             * Define el valor de la propiedad sucursal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSucursal(String value) {
                this.sucursal = value;
            }

            /**
             * Obtiene el valor de la propiedad tipo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipo() {
                return tipo;
            }

            /**
             * Define el valor de la propiedad tipo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipo(String value) {
                this.tipo = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codigoGeografico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "calle",
            "numero",
            "piso",
            "lod",
            "nroLOD",
            "codigoPostal",
            "codigoGeografico",
            "partido",
            "telefono"
        })
        public static class Domicilio {

            protected String calle;
            protected String numero;
            protected String piso;
            @XmlElement(name = "LOD")
            protected String lod;
            protected String nroLOD;
            protected String codigoPostal;
            protected String codigoGeografico;
            protected String partido;
            protected String telefono;

            /**
             * Obtiene el valor de la propiedad calle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalle() {
                return calle;
            }

            /**
             * Define el valor de la propiedad calle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalle(String value) {
                this.calle = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

            /**
             * Obtiene el valor de la propiedad piso.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPiso() {
                return piso;
            }

            /**
             * Define el valor de la propiedad piso.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPiso(String value) {
                this.piso = value;
            }

            /**
             * Obtiene el valor de la propiedad lod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLOD() {
                return lod;
            }

            /**
             * Define el valor de la propiedad lod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLOD(String value) {
                this.lod = value;
            }

            /**
             * Obtiene el valor de la propiedad nroLOD.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNroLOD() {
                return nroLOD;
            }

            /**
             * Define el valor de la propiedad nroLOD.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNroLOD(String value) {
                this.nroLOD = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoPostal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoPostal() {
                return codigoPostal;
            }

            /**
             * Define el valor de la propiedad codigoPostal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoPostal(String value) {
                this.codigoPostal = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoGeografico.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoGeografico() {
                return codigoGeografico;
            }

            /**
             * Define el valor de la propiedad codigoGeografico.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoGeografico(String value) {
                this.codigoGeografico = value;
            }

            /**
             * Obtiene el valor de la propiedad partido.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPartido() {
                return partido;
            }

            /**
             * Define el valor de la propiedad partido.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPartido(String value) {
                this.partido = value;
            }

            /**
             * Obtiene el valor de la propiedad telefono.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTelefono() {
                return telefono;
            }

            /**
             * Define el valor de la propiedad telefono.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTelefono(String value) {
                this.telefono = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "calle",
            "numero",
            "piso",
            "lod",
            "nroLOD",
            "codigoPostal"
        })
        public static class DomicilioCorrespondencia {

            protected String calle;
            protected String numero;
            protected String piso;
            @XmlElement(name = "LOD")
            protected String lod;
            protected String nroLOD;
            protected String codigoPostal;

            /**
             * Obtiene el valor de la propiedad calle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalle() {
                return calle;
            }

            /**
             * Define el valor de la propiedad calle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalle(String value) {
                this.calle = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

            /**
             * Obtiene el valor de la propiedad piso.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPiso() {
                return piso;
            }

            /**
             * Define el valor de la propiedad piso.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPiso(String value) {
                this.piso = value;
            }

            /**
             * Obtiene el valor de la propiedad lod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLOD() {
                return lod;
            }

            /**
             * Define el valor de la propiedad lod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLOD(String value) {
                this.lod = value;
            }

            /**
             * Obtiene el valor de la propiedad nroLOD.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNroLOD() {
                return nroLOD;
            }

            /**
             * Define el valor de la propiedad nroLOD.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNroLOD(String value) {
                this.nroLOD = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoPostal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoPostal() {
                return codigoPostal;
            }

            /**
             * Define el valor de la propiedad codigoPostal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoPostal(String value) {
                this.codigoPostal = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "operacion",
            "tipoSolicitud",
            "nroSolicitudGrupo",
            "nroSolicitud",
            "canal",
            "subCanal"
        })
        public static class IdentificadorSolicitud {

            protected int operacion;
            protected int tipoSolicitud;
            protected BigInteger nroSolicitudGrupo;
            protected BigInteger nroSolicitud;
            @XmlElement(required = true)
            protected BigInteger canal;
            @XmlElement(required = true)
            protected BigInteger subCanal;

            /**
             * Obtiene el valor de la propiedad operacion.
             * 
             */
            public int getOperacion() {
                return operacion;
            }

            /**
             * Define el valor de la propiedad operacion.
             * 
             */
            public void setOperacion(int value) {
                this.operacion = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoSolicitud.
             * 
             */
            public int getTipoSolicitud() {
                return tipoSolicitud;
            }

            /**
             * Define el valor de la propiedad tipoSolicitud.
             * 
             */
            public void setTipoSolicitud(int value) {
                this.tipoSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitudGrupo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitudGrupo() {
                return nroSolicitudGrupo;
            }

            /**
             * Define el valor de la propiedad nroSolicitudGrupo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitudGrupo(BigInteger value) {
                this.nroSolicitudGrupo = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitud() {
                return nroSolicitud;
            }

            /**
             * Define el valor de la propiedad nroSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitud(BigInteger value) {
                this.nroSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad canal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCanal() {
                return canal;
            }

            /**
             * Define el valor de la propiedad canal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCanal(BigInteger value) {
                this.canal = value;
            }

            /**
             * Obtiene el valor de la propiedad subCanal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubCanal() {
                return subCanal;
            }

            /**
             * Define el valor de la propiedad subCanal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubCanal(BigInteger value) {
                this.subCanal = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="limiteCompraCuenta" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "moneda",
            "limiteDisponible",
            "limiteCompra",
            "limiteCompraCuenta"
        })
        public static class Limites {

            protected String moneda;
            protected BigInteger limiteDisponible;
            protected BigInteger limiteCompra;
            protected Double limiteCompraCuenta;

            /**
             * Obtiene el valor de la propiedad moneda.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoneda() {
                return moneda;
            }

            /**
             * Define el valor de la propiedad moneda.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoneda(String value) {
                this.moneda = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteDisponible.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLimiteDisponible() {
                return limiteDisponible;
            }

            /**
             * Define el valor de la propiedad limiteDisponible.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLimiteDisponible(BigInteger value) {
                this.limiteDisponible = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLimiteCompra() {
                return limiteCompra;
            }

            /**
             * Define el valor de la propiedad limiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLimiteCompra(BigInteger value) {
                this.limiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCompraCuenta.
             * 
             * @return
             *     possible object is
             *     {@link Double }
             *     
             */
            public Double getLimiteCompraCuenta() {
                return limiteCompraCuenta;
            }

            /**
             * Define el valor de la propiedad limiteCompraCuenta.
             * 
             * @param value
             *     allowed object is
             *     {@link Double }
             *     
             */
            public void setLimiteCompraCuenta(Double value) {
                this.limiteCompraCuenta = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rubro"
        })
        public static class RubrosHabilitados {

            @XmlElement(required = true)
            protected List<String> rubro;

            /**
             * Gets the value of the rubro property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rubro property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRubro().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getRubro() {
                if (rubro == null) {
                    rubro = new ArrayList<String>();
                }
                return this.rubro;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rubro"
        })
        public static class RubrosInhabilitados {

            @XmlElement(required = true)
            protected List<String> rubro;

            /**
             * Gets the value of the rubro property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rubro property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRubro().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getRubro() {
                if (rubro == null) {
                    rubro = new ArrayList<String>();
                }
                return this.rubro;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Tarjeta" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdentificadorSolicitud">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                             &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                             &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Oficial" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="cuentaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                   &lt;element name="DatosCliente">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;choice>
     *                               &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                               &lt;element name="datosClienteNuevo">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="Domicilio" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                   &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                   &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                   &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/choice>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="embozado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="documentoTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                   &lt;element name="sucursalOrigenBT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                   &lt;element name="habilitacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="Limites">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                             &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                             &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                             &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tarjeta"
    })
    public static class Tarjetas {

        @XmlElement(name = "Tarjeta", required = true)
        protected List<DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta> tarjeta;

        /**
         * Gets the value of the tarjeta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tarjeta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTarjeta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta }
         * 
         * 
         */
        public List<DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta> getTarjeta() {
            if (tarjeta == null) {
                tarjeta = new ArrayList<DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta>();
            }
            return this.tarjeta;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdentificadorSolicitud">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                   &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Oficial" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="cuentaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *         &lt;element name="DatosCliente">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;choice>
         *                     &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                     &lt;element name="datosClienteNuevo">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="Domicilio" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                         &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                         &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                                         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                                         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                                         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                         &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/choice>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="embozado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="documentoTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *         &lt;element name="sucursalOrigenBT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *         &lt;element name="habilitacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="Limites">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *                   &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *                   &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *                   &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identificadorSolicitud",
            "nroReferencia",
            "codigoMarca",
            "codigoEntidad",
            "oficial",
            "cuentaEmpresa",
            "identificador",
            "datosCliente",
            "embozado",
            "nacionalidad",
            "sexo",
            "documentoTributario",
            "estadoCivil",
            "fechaNacimiento",
            "ocupacion",
            "sucursalOrigenBT",
            "habilitacion",
            "area",
            "sector",
            "limites",
            "cuartaLinea"
        })
        public static class Tarjeta {

            @XmlElement(name = "IdentificadorSolicitud", required = true)
            protected DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud identificadorSolicitud;
            @XmlElement(required = true)
            protected String nroReferencia;
            @XmlElement(required = true)
            protected String codigoMarca;
            @XmlElement(required = true)
            protected String codigoEntidad;
            @XmlElement(name = "Oficial")
            protected DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial oficial;
            @XmlElement(required = true)
            protected String cuentaEmpresa;
            @XmlElement(name = "Identificador", required = true)
            protected IdClienteType identificador;
            @XmlElement(name = "DatosCliente", required = true)
            protected DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente datosCliente;
            protected String embozado;
            protected BigInteger nacionalidad;
            protected String sexo;
            protected String documentoTributario;
            protected String estadoCivil;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaNacimiento;
            protected Integer ocupacion;
            protected Integer sucursalOrigenBT;
            protected String habilitacion;
            protected String area;
            protected String sector;
            @XmlElement(name = "Limites", required = true)
            protected DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites limites;
            protected String cuartaLinea;

            /**
             * Obtiene el valor de la propiedad identificadorSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud }
             *     
             */
            public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud getIdentificadorSolicitud() {
                return identificadorSolicitud;
            }

            /**
             * Define el valor de la propiedad identificadorSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud }
             *     
             */
            public void setIdentificadorSolicitud(DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud value) {
                this.identificadorSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad nroReferencia.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNroReferencia() {
                return nroReferencia;
            }

            /**
             * Define el valor de la propiedad nroReferencia.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNroReferencia(String value) {
                this.nroReferencia = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoMarca.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoMarca() {
                return codigoMarca;
            }

            /**
             * Define el valor de la propiedad codigoMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoMarca(String value) {
                this.codigoMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoEntidad.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoEntidad() {
                return codigoEntidad;
            }

            /**
             * Define el valor de la propiedad codigoEntidad.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoEntidad(String value) {
                this.codigoEntidad = value;
            }

            /**
             * Obtiene el valor de la propiedad oficial.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial }
             *     
             */
            public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial getOficial() {
                return oficial;
            }

            /**
             * Define el valor de la propiedad oficial.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial }
             *     
             */
            public void setOficial(DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial value) {
                this.oficial = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaEmpresa.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaEmpresa() {
                return cuentaEmpresa;
            }

            /**
             * Define el valor de la propiedad cuentaEmpresa.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaEmpresa(String value) {
                this.cuentaEmpresa = value;
            }

            /**
             * Obtiene el valor de la propiedad identificador.
             * 
             * @return
             *     possible object is
             *     {@link IdClienteType }
             *     
             */
            public IdClienteType getIdentificador() {
                return identificador;
            }

            /**
             * Define el valor de la propiedad identificador.
             * 
             * @param value
             *     allowed object is
             *     {@link IdClienteType }
             *     
             */
            public void setIdentificador(IdClienteType value) {
                this.identificador = value;
            }

            /**
             * Obtiene el valor de la propiedad datosCliente.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente }
             *     
             */
            public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente getDatosCliente() {
                return datosCliente;
            }

            /**
             * Define el valor de la propiedad datosCliente.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente }
             *     
             */
            public void setDatosCliente(DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente value) {
                this.datosCliente = value;
            }

            /**
             * Obtiene el valor de la propiedad embozado.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmbozado() {
                return embozado;
            }

            /**
             * Define el valor de la propiedad embozado.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmbozado(String value) {
                this.embozado = value;
            }

            /**
             * Obtiene el valor de la propiedad nacionalidad.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNacionalidad() {
                return nacionalidad;
            }

            /**
             * Define el valor de la propiedad nacionalidad.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNacionalidad(BigInteger value) {
                this.nacionalidad = value;
            }

            /**
             * Obtiene el valor de la propiedad sexo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSexo() {
                return sexo;
            }

            /**
             * Define el valor de la propiedad sexo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSexo(String value) {
                this.sexo = value;
            }

            /**
             * Obtiene el valor de la propiedad documentoTributario.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocumentoTributario() {
                return documentoTributario;
            }

            /**
             * Define el valor de la propiedad documentoTributario.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocumentoTributario(String value) {
                this.documentoTributario = value;
            }

            /**
             * Obtiene el valor de la propiedad estadoCivil.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEstadoCivil() {
                return estadoCivil;
            }

            /**
             * Define el valor de la propiedad estadoCivil.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEstadoCivil(String value) {
                this.estadoCivil = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaNacimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaNacimiento() {
                return fechaNacimiento;
            }

            /**
             * Define el valor de la propiedad fechaNacimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaNacimiento(XMLGregorianCalendar value) {
                this.fechaNacimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad ocupacion.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getOcupacion() {
                return ocupacion;
            }

            /**
             * Define el valor de la propiedad ocupacion.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setOcupacion(Integer value) {
                this.ocupacion = value;
            }

            /**
             * Obtiene el valor de la propiedad sucursalOrigenBT.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getSucursalOrigenBT() {
                return sucursalOrigenBT;
            }

            /**
             * Define el valor de la propiedad sucursalOrigenBT.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setSucursalOrigenBT(Integer value) {
                this.sucursalOrigenBT = value;
            }

            /**
             * Obtiene el valor de la propiedad habilitacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHabilitacion() {
                return habilitacion;
            }

            /**
             * Define el valor de la propiedad habilitacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHabilitacion(String value) {
                this.habilitacion = value;
            }

            /**
             * Obtiene el valor de la propiedad area.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getArea() {
                return area;
            }

            /**
             * Define el valor de la propiedad area.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setArea(String value) {
                this.area = value;
            }

            /**
             * Obtiene el valor de la propiedad sector.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSector() {
                return sector;
            }

            /**
             * Define el valor de la propiedad sector.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSector(String value) {
                this.sector = value;
            }

            /**
             * Obtiene el valor de la propiedad limites.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites }
             *     
             */
            public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites getLimites() {
                return limites;
            }

            /**
             * Define el valor de la propiedad limites.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites }
             *     
             */
            public void setLimites(DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites value) {
                this.limites = value;
            }

            /**
             * Obtiene el valor de la propiedad cuartaLinea.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuartaLinea() {
                return cuartaLinea;
            }

            /**
             * Define el valor de la propiedad cuartaLinea.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuartaLinea(String value) {
                this.cuartaLinea = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;choice>
             *           &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *           &lt;element name="datosClienteNuevo">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                     &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                     &lt;element name="Domicilio" minOccurs="0">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                               &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                               &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                               &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                               &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                               &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                               &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                               &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                               &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *         &lt;/choice>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "cuentaClienteExistente",
                "datosClienteNuevo"
            })
            public static class DatosCliente {

                protected String cuentaClienteExistente;
                protected DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo datosClienteNuevo;

                /**
                 * Obtiene el valor de la propiedad cuentaClienteExistente.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCuentaClienteExistente() {
                    return cuentaClienteExistente;
                }

                /**
                 * Define el valor de la propiedad cuentaClienteExistente.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCuentaClienteExistente(String value) {
                    this.cuentaClienteExistente = value;
                }

                /**
                 * Obtiene el valor de la propiedad datosClienteNuevo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo }
                 *     
                 */
                public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo getDatosClienteNuevo() {
                    return datosClienteNuevo;
                }

                /**
                 * Define el valor de la propiedad datosClienteNuevo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo }
                 *     
                 */
                public void setDatosClienteNuevo(DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo value) {
                    this.datosClienteNuevo = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="Domicilio" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "nombre",
                    "apellido",
                    "domicilio"
                })
                public static class DatosClienteNuevo {

                    protected String nombre;
                    protected String apellido;
                    @XmlElement(name = "Domicilio")
                    protected DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio domicilio;

                    /**
                     * Obtiene el valor de la propiedad nombre.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNombre() {
                        return nombre;
                    }

                    /**
                     * Define el valor de la propiedad nombre.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNombre(String value) {
                        this.nombre = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad apellido.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getApellido() {
                        return apellido;
                    }

                    /**
                     * Define el valor de la propiedad apellido.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setApellido(String value) {
                        this.apellido = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad domicilio.
                     * 
                     * @return
                     *     possible object is
                     *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio }
                     *     
                     */
                    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio getDomicilio() {
                        return domicilio;
                    }

                    /**
                     * Define el valor de la propiedad domicilio.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio }
                     *     
                     */
                    public void setDomicilio(DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio value) {
                        this.domicilio = value;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                     *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                     *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                     *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "calle",
                        "numero",
                        "apartamento",
                        "piso",
                        "pais",
                        "localidad",
                        "provincia",
                        "codigoPostal",
                        "codigoPostalNuevo"
                    })
                    public static class Domicilio {

                        protected String calle;
                        protected String numero;
                        protected String apartamento;
                        protected String piso;
                        protected BigInteger pais;
                        protected BigInteger localidad;
                        protected BigInteger provincia;
                        protected String codigoPostal;
                        protected String codigoPostalNuevo;

                        /**
                         * Obtiene el valor de la propiedad calle.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCalle() {
                            return calle;
                        }

                        /**
                         * Define el valor de la propiedad calle.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCalle(String value) {
                            this.calle = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad numero.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNumero() {
                            return numero;
                        }

                        /**
                         * Define el valor de la propiedad numero.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNumero(String value) {
                            this.numero = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad apartamento.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getApartamento() {
                            return apartamento;
                        }

                        /**
                         * Define el valor de la propiedad apartamento.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setApartamento(String value) {
                            this.apartamento = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad piso.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPiso() {
                            return piso;
                        }

                        /**
                         * Define el valor de la propiedad piso.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPiso(String value) {
                            this.piso = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad pais.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getPais() {
                            return pais;
                        }

                        /**
                         * Define el valor de la propiedad pais.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setPais(BigInteger value) {
                            this.pais = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad localidad.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getLocalidad() {
                            return localidad;
                        }

                        /**
                         * Define el valor de la propiedad localidad.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setLocalidad(BigInteger value) {
                            this.localidad = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad provincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getProvincia() {
                            return provincia;
                        }

                        /**
                         * Define el valor de la propiedad provincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setProvincia(BigInteger value) {
                            this.provincia = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad codigoPostal.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCodigoPostal() {
                            return codigoPostal;
                        }

                        /**
                         * Define el valor de la propiedad codigoPostal.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCodigoPostal(String value) {
                            this.codigoPostal = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad codigoPostalNuevo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCodigoPostalNuevo() {
                            return codigoPostalNuevo;
                        }

                        /**
                         * Define el valor de la propiedad codigoPostalNuevo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCodigoPostalNuevo(String value) {
                            this.codigoPostalNuevo = value;
                        }

                    }

                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "operacion",
                "tipoSolicitud",
                "nroSolicitudGrupo",
                "nroSolicitud",
                "canal",
                "subCanal"
            })
            public static class IdentificadorSolicitud {

                protected int operacion;
                protected int tipoSolicitud;
                protected BigInteger nroSolicitudGrupo;
                protected BigInteger nroSolicitud;
                @XmlElement(required = true)
                protected BigInteger canal;
                @XmlElement(required = true)
                protected BigInteger subCanal;

                /**
                 * Obtiene el valor de la propiedad operacion.
                 * 
                 */
                public int getOperacion() {
                    return operacion;
                }

                /**
                 * Define el valor de la propiedad operacion.
                 * 
                 */
                public void setOperacion(int value) {
                    this.operacion = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoSolicitud.
                 * 
                 */
                public int getTipoSolicitud() {
                    return tipoSolicitud;
                }

                /**
                 * Define el valor de la propiedad tipoSolicitud.
                 * 
                 */
                public void setTipoSolicitud(int value) {
                    this.tipoSolicitud = value;
                }

                /**
                 * Obtiene el valor de la propiedad nroSolicitudGrupo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNroSolicitudGrupo() {
                    return nroSolicitudGrupo;
                }

                /**
                 * Define el valor de la propiedad nroSolicitudGrupo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNroSolicitudGrupo(BigInteger value) {
                    this.nroSolicitudGrupo = value;
                }

                /**
                 * Obtiene el valor de la propiedad nroSolicitud.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNroSolicitud() {
                    return nroSolicitud;
                }

                /**
                 * Define el valor de la propiedad nroSolicitud.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNroSolicitud(BigInteger value) {
                    this.nroSolicitud = value;
                }

                /**
                 * Obtiene el valor de la propiedad canal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCanal() {
                    return canal;
                }

                /**
                 * Define el valor de la propiedad canal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCanal(BigInteger value) {
                    this.canal = value;
                }

                /**
                 * Obtiene el valor de la propiedad subCanal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getSubCanal() {
                    return subCanal;
                }

                /**
                 * Define el valor de la propiedad subCanal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setSubCanal(BigInteger value) {
                    this.subCanal = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
             *         &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
             *         &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
             *         &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "limiteCompra",
                "limiteCuotas",
                "limiteAdelante",
                "limiteTransaccion"
            })
            public static class Limites {

                protected Double limiteCompra;
                protected Double limiteCuotas;
                protected Double limiteAdelante;
                protected Double limiteTransaccion;

                /**
                 * Obtiene el valor de la propiedad limiteCompra.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Double }
                 *     
                 */
                public Double getLimiteCompra() {
                    return limiteCompra;
                }

                /**
                 * Define el valor de la propiedad limiteCompra.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Double }
                 *     
                 */
                public void setLimiteCompra(Double value) {
                    this.limiteCompra = value;
                }

                /**
                 * Obtiene el valor de la propiedad limiteCuotas.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Double }
                 *     
                 */
                public Double getLimiteCuotas() {
                    return limiteCuotas;
                }

                /**
                 * Define el valor de la propiedad limiteCuotas.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Double }
                 *     
                 */
                public void setLimiteCuotas(Double value) {
                    this.limiteCuotas = value;
                }

                /**
                 * Obtiene el valor de la propiedad limiteAdelante.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Double }
                 *     
                 */
                public Double getLimiteAdelante() {
                    return limiteAdelante;
                }

                /**
                 * Define el valor de la propiedad limiteAdelante.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Double }
                 *     
                 */
                public void setLimiteAdelante(Double value) {
                    this.limiteAdelante = value;
                }

                /**
                 * Obtiene el valor de la propiedad limiteTransaccion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Double }
                 *     
                 */
                public Double getLimiteTransaccion() {
                    return limiteTransaccion;
                }

                /**
                 * Define el valor de la propiedad limiteTransaccion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Double }
                 *     
                 */
                public void setLimiteTransaccion(Double value) {
                    this.limiteTransaccion = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion"
            })
            public static class Oficial {

                @XmlElement(required = true)
                protected String codigo;
                protected String descripcion;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

            }

        }

    }

}
