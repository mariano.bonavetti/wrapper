
package ar.com.supervielle.xsd.integracion.cliente.consultadatospersona_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaDatosPersonaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDatosPersonaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="tipoPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="emails">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="paginaWeb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDatosPersonaRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cliente/consultaDatosPersona-v1.1", propOrder = {
    "row"
})
public class DataConsultaDatosPersonaRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaDatosPersonaRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaDatosPersonaRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaDatosPersonaRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaDatosPersonaRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="tipoPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="emails">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="paginaWeb" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "fechaAlta",
        "tipoPersona",
        "emails",
        "paginaWeb"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdClienteType identificador;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAlta;
        @XmlElement(required = true)
        protected CodDescStringType tipoPersona;
        @XmlElement(required = true)
        protected DataConsultaDatosPersonaRespType2 .Row.Emails emails;
        @XmlElement(required = true)
        protected String paginaWeb;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAlta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAlta() {
            return fechaAlta;
        }

        /**
         * Define el valor de la propiedad fechaAlta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAlta(XMLGregorianCalendar value) {
            this.fechaAlta = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoPersona.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipoPersona() {
            return tipoPersona;
        }

        /**
         * Define el valor de la propiedad tipoPersona.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipoPersona(CodDescStringType value) {
            this.tipoPersona = value;
        }

        /**
         * Obtiene el valor de la propiedad emails.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDatosPersonaRespType2 .Row.Emails }
         *     
         */
        public DataConsultaDatosPersonaRespType2 .Row.Emails getEmails() {
            return emails;
        }

        /**
         * Define el valor de la propiedad emails.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDatosPersonaRespType2 .Row.Emails }
         *     
         */
        public void setEmails(DataConsultaDatosPersonaRespType2 .Row.Emails value) {
            this.emails = value;
        }

        /**
         * Obtiene el valor de la propiedad paginaWeb.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaginaWeb() {
            return paginaWeb;
        }

        /**
         * Define el valor de la propiedad paginaWeb.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaginaWeb(String value) {
            this.paginaWeb = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "email"
        })
        public static class Emails {

            @XmlElement(required = true)
            protected List<String> email;

            /**
             * Gets the value of the email property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the email property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEmail().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getEmail() {
                if (email == null) {
                    email = new ArrayList<String>();
                }
                return this.email;
            }

        }

    }

}
