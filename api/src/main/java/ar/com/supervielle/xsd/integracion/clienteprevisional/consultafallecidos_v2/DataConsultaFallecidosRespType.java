
package ar.com.supervielle.xsd.integracion.clienteprevisional.consultafallecidos_v2;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataConsultaFallecidosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaFallecidosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="origenInformacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaFallecimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="idCiudadano" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaFallecidosRespType", propOrder = {
    "row"
})
public class DataConsultaFallecidosRespType {

    @XmlElement(name = "Row")
    protected DataConsultaFallecidosRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaFallecidosRespType.Row }
     *     
     */
    public DataConsultaFallecidosRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaFallecidosRespType.Row }
     *     
     */
    public void setRow(DataConsultaFallecidosRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="origenInformacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaFallecimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="idCiudadano" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nombre",
        "apellido",
        "fechaNacimiento",
        "codigo",
        "descripcion",
        "origenInformacion",
        "fechaFallecimiento",
        "idCiudadano"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String nombre;
        @XmlElement(required = true)
        protected String apellido;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaNacimiento;
        protected int codigo;
        @XmlElement(required = true)
        protected String descripcion;
        @XmlElement(required = true)
        protected String origenInformacion;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaFallecimiento;
        @XmlElement(required = true)
        protected BigInteger idCiudadano;

        /**
         * Obtiene el valor de la propiedad nombre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombre() {
            return nombre;
        }

        /**
         * Define el valor de la propiedad nombre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombre(String value) {
            this.nombre = value;
        }

        /**
         * Obtiene el valor de la propiedad apellido.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApellido() {
            return apellido;
        }

        /**
         * Define el valor de la propiedad apellido.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApellido(String value) {
            this.apellido = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaNacimiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaNacimiento() {
            return fechaNacimiento;
        }

        /**
         * Define el valor de la propiedad fechaNacimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaNacimiento(XMLGregorianCalendar value) {
            this.fechaNacimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad codigo.
         * 
         */
        public int getCodigo() {
            return codigo;
        }

        /**
         * Define el valor de la propiedad codigo.
         * 
         */
        public void setCodigo(int value) {
            this.codigo = value;
        }

        /**
         * Obtiene el valor de la propiedad descripcion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescripcion() {
            return descripcion;
        }

        /**
         * Define el valor de la propiedad descripcion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescripcion(String value) {
            this.descripcion = value;
        }

        /**
         * Obtiene el valor de la propiedad origenInformacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigenInformacion() {
            return origenInformacion;
        }

        /**
         * Define el valor de la propiedad origenInformacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigenInformacion(String value) {
            this.origenInformacion = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaFallecimiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaFallecimiento() {
            return fechaFallecimiento;
        }

        /**
         * Define el valor de la propiedad fechaFallecimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaFallecimiento(XMLGregorianCalendar value) {
            this.fechaFallecimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad idCiudadano.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getIdCiudadano() {
            return idCiudadano;
        }

        /**
         * Define el valor de la propiedad idCiudadano.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setIdCiudadano(BigInteger value) {
            this.idCiudadano = value;
        }

    }

}
