
package ar.com.supervielle.xsd.integracion.clienteprevisional.altaanticipohaberes_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataAltaAnticipoHaberesReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAnticipoHaberesReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AltaAnticipo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Anticipo" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="beneficio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAnticipoHaberesReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/clientePrevisional/altaAnticipoHaberes-v1", propOrder = {
    "altaAnticipo"
})
public class DataAltaAnticipoHaberesReqType {

    @XmlElement(name = "AltaAnticipo", required = true)
    protected DataAltaAnticipoHaberesReqType.AltaAnticipo altaAnticipo;

    /**
     * Obtiene el valor de la propiedad altaAnticipo.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAnticipoHaberesReqType.AltaAnticipo }
     *     
     */
    public DataAltaAnticipoHaberesReqType.AltaAnticipo getAltaAnticipo() {
        return altaAnticipo;
    }

    /**
     * Define el valor de la propiedad altaAnticipo.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAnticipoHaberesReqType.AltaAnticipo }
     *     
     */
    public void setAltaAnticipo(DataAltaAnticipoHaberesReqType.AltaAnticipo value) {
        this.altaAnticipo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Anticipo" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="beneficio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "anticipo"
    })
    public static class AltaAnticipo {

        @XmlElement(name = "Anticipo", required = true)
        protected List<DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo> anticipo;

        /**
         * Gets the value of the anticipo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the anticipo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAnticipo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo }
         * 
         * 
         */
        public List<DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo> getAnticipo() {
            if (anticipo == null) {
                anticipo = new ArrayList<DataAltaAnticipoHaberesReqType.AltaAnticipo.Anticipo>();
            }
            return this.anticipo;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="beneficio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "beneficio",
            "cuentaBancaria"
        })
        public static class Anticipo {

            @XmlElement(required = true)
            protected BigInteger beneficio;
            @XmlElement(required = true)
            protected IdCuentaBANTOTALType cuentaBancaria;

            /**
             * Obtiene el valor de la propiedad beneficio.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getBeneficio() {
                return beneficio;
            }

            /**
             * Define el valor de la propiedad beneficio.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setBeneficio(BigInteger value) {
                this.beneficio = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaBancaria.
             * 
             * @return
             *     possible object is
             *     {@link IdCuentaBANTOTALType }
             *     
             */
            public IdCuentaBANTOTALType getCuentaBancaria() {
                return cuentaBancaria;
            }

            /**
             * Define el valor de la propiedad cuentaBancaria.
             * 
             * @param value
             *     allowed object is
             *     {@link IdCuentaBANTOTALType }
             *     
             */
            public void setCuentaBancaria(IdCuentaBANTOTALType value) {
                this.cuentaBancaria = value;
            }

        }

    }

}
