
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesemitidos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesemitidos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesemitidos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosRespType2 }
     * 
     */
    public DataConsultaChequesEmitidosRespType2 createDataConsultaChequesEmitidosRespType2() {
        return new DataConsultaChequesEmitidosRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosReqType }
     * 
     */
    public DataConsultaChequesEmitidosReqType createDataConsultaChequesEmitidosReqType() {
        return new DataConsultaChequesEmitidosReqType();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosRespType }
     * 
     */
    public DataConsultaChequesEmitidosRespType createDataConsultaChequesEmitidosRespType() {
        return new DataConsultaChequesEmitidosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosRespType.Row }
     * 
     */
    public DataConsultaChequesEmitidosRespType.Row createDataConsultaChequesEmitidosRespTypeRow() {
        return new DataConsultaChequesEmitidosRespType.Row();
    }

    /**
     * Create an instance of {@link RespConsultaChequesEmitidos }
     * 
     */
    public RespConsultaChequesEmitidos createRespConsultaChequesEmitidos() {
        return new RespConsultaChequesEmitidos();
    }

    /**
     * Create an instance of {@link ReqConsultaChequesEmitidos }
     * 
     */
    public ReqConsultaChequesEmitidos createReqConsultaChequesEmitidos() {
        return new ReqConsultaChequesEmitidos();
    }

    /**
     * Create an instance of {@link RespConsultaChequesEmitidos2 }
     * 
     */
    public RespConsultaChequesEmitidos2 createRespConsultaChequesEmitidos2() {
        return new RespConsultaChequesEmitidos2();
    }

    /**
     * Create an instance of {@link ReqConsultaChequesEmitidos2 }
     * 
     */
    public ReqConsultaChequesEmitidos2 createReqConsultaChequesEmitidos2() {
        return new ReqConsultaChequesEmitidos2();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosReqType2 }
     * 
     */
    public DataConsultaChequesEmitidosReqType2 createDataConsultaChequesEmitidosReqType2() {
        return new DataConsultaChequesEmitidosReqType2();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosRespType2 .Row }
     * 
     */
    public DataConsultaChequesEmitidosRespType2 .Row createDataConsultaChequesEmitidosRespType2Row() {
        return new DataConsultaChequesEmitidosRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosReqType.RangoFecha }
     * 
     */
    public DataConsultaChequesEmitidosReqType.RangoFecha createDataConsultaChequesEmitidosReqTypeRangoFecha() {
        return new DataConsultaChequesEmitidosReqType.RangoFecha();
    }

    /**
     * Create an instance of {@link DataConsultaChequesEmitidosRespType.Row.EntidadReceptora }
     * 
     */
    public DataConsultaChequesEmitidosRespType.Row.EntidadReceptora createDataConsultaChequesEmitidosRespTypeRowEntidadReceptora() {
        return new DataConsultaChequesEmitidosRespType.Row.EntidadReceptora();
    }

}
