
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaListadoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *                   &lt;element name="detalleTarjeta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                             &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                             &lt;element name="flagCoorporativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                             &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fechaDeVigencia">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="tipoTarjeta">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="producto">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="grupoAfinidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="limites" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="flagBoletinada" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                             &lt;element name="estadoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="datosEntrega" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="detalleCuenta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tipoCta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="estadoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="cuentaClienteBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="fechaCierre" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="fechaVencimiento" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="saldos">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="saldoActual" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="saldoAnterior" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="tipoPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                             &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                             &lt;element name="cuentaDebito" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="tasas" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="tasaMensual" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="tasaAnual" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                 &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="datosAdicionales" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaListado-v1.3", propOrder = {
    "row"
})
public class DataConsultaListadoRespType3 {

    @XmlElement(name = "Row")
    protected List<DataConsultaListadoRespType3 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaListadoRespType3 .Row }
     * 
     * 
     */
    public List<DataConsultaListadoRespType3 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaListadoRespType3 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
     *         &lt;element name="detalleTarjeta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                   &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                   &lt;element name="flagCoorporativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fechaDeVigencia">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="tipoTarjeta">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="producto">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="grupoAfinidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="limites" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="flagBoletinada" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                   &lt;element name="estadoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="datosEntrega" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="detalleCuenta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tipoCta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="estadoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="cuentaClienteBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="fechaCierre" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="fechaVencimiento" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="saldos">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="saldoActual" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="saldoAnterior" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="tipoPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                   &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                   &lt;element name="cuentaDebito" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="tasas" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="tasaMensual" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="tasaAnual" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="datosAdicionales" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "detalleTarjeta",
        "detalleCuenta",
        "datosAdicionales"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdTarjetaType identificador;
        @XmlElement(required = true)
        protected DataConsultaListadoRespType3 .Row.DetalleTarjeta detalleTarjeta;
        @XmlElement(required = true)
        protected DataConsultaListadoRespType3 .Row.DetalleCuenta detalleCuenta;
        protected DataConsultaListadoRespType3 .Row.DatosAdicionales datosAdicionales;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdTarjetaType }
         *     
         */
        public IdTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdTarjetaType }
         *     
         */
        public void setIdentificador(IdTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad detalleTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta }
         *     
         */
        public DataConsultaListadoRespType3 .Row.DetalleTarjeta getDetalleTarjeta() {
            return detalleTarjeta;
        }

        /**
         * Define el valor de la propiedad detalleTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta }
         *     
         */
        public void setDetalleTarjeta(DataConsultaListadoRespType3 .Row.DetalleTarjeta value) {
            this.detalleTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad detalleCuenta.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta }
         *     
         */
        public DataConsultaListadoRespType3 .Row.DetalleCuenta getDetalleCuenta() {
            return detalleCuenta;
        }

        /**
         * Define el valor de la propiedad detalleCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta }
         *     
         */
        public void setDetalleCuenta(DataConsultaListadoRespType3 .Row.DetalleCuenta value) {
            this.detalleCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad datosAdicionales.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoRespType3 .Row.DatosAdicionales }
         *     
         */
        public DataConsultaListadoRespType3 .Row.DatosAdicionales getDatosAdicionales() {
            return datosAdicionales;
        }

        /**
         * Define el valor de la propiedad datosAdicionales.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoRespType3 .Row.DatosAdicionales }
         *     
         */
        public void setDatosAdicionales(DataConsultaListadoRespType3 .Row.DatosAdicionales value) {
            this.datosAdicionales = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "atributo"
        })
        public static class DatosAdicionales {

            protected List<DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo> atributo;

            /**
             * Gets the value of the atributo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the atributo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAtributo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo }
             * 
             * 
             */
            public List<DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo> getAtributo() {
                if (atributo == null) {
                    atributo = new ArrayList<DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo>();
                }
                return this.atributo;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "valor"
            })
            public static class Atributo {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String valor;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad valor.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValor() {
                    return valor;
                }

                /**
                 * Define el valor de la propiedad valor.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValor(String value) {
                    this.valor = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tipoCta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="estadoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="cuentaClienteBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="fechaCierre" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="fechaVencimiento" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="saldos">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="saldoActual" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="saldoAnterior" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="tipoPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *         &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *         &lt;element name="cuentaDebito" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="tasas" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="tasaMensual" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="tasaAnual" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cuentaTarjeta",
            "tipoCta",
            "estadoCuenta",
            "cuentaClienteBT",
            "fechaCierre",
            "fechaVencimiento",
            "cartera",
            "saldos",
            "tipoPago",
            "formaPago",
            "cuentaDebito",
            "tasas"
        })
        public static class DetalleCuenta {

            @XmlElement(required = true, nillable = true)
            protected String cuentaTarjeta;
            @XmlElement(required = true)
            protected CodDescStringType tipoCta;
            @XmlElement(required = true)
            protected CodDescStringType estadoCuenta;
            protected String cuentaClienteBT;
            protected DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre fechaCierre;
            protected DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento fechaVencimiento;
            @XmlElement(required = true)
            protected CodDescStringType cartera;
            @XmlElement(required = true)
            protected DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos saldos;
            protected CodDescStringType tipoPago;
            protected CodDescStringType formaPago;
            protected DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito cuentaDebito;
            protected DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas tasas;

            /**
             * Obtiene el valor de la propiedad cuentaTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaTarjeta() {
                return cuentaTarjeta;
            }

            /**
             * Define el valor de la propiedad cuentaTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaTarjeta(String value) {
                this.cuentaTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoCta.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getTipoCta() {
                return tipoCta;
            }

            /**
             * Define el valor de la propiedad tipoCta.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setTipoCta(CodDescStringType value) {
                this.tipoCta = value;
            }

            /**
             * Obtiene el valor de la propiedad estadoCuenta.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getEstadoCuenta() {
                return estadoCuenta;
            }

            /**
             * Define el valor de la propiedad estadoCuenta.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setEstadoCuenta(CodDescStringType value) {
                this.estadoCuenta = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaClienteBT.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaClienteBT() {
                return cuentaClienteBT;
            }

            /**
             * Define el valor de la propiedad cuentaClienteBT.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaClienteBT(String value) {
                this.cuentaClienteBT = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaCierre.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre getFechaCierre() {
                return fechaCierre;
            }

            /**
             * Define el valor de la propiedad fechaCierre.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre }
             *     
             */
            public void setFechaCierre(DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre value) {
                this.fechaCierre = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaVencimiento.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento getFechaVencimiento() {
                return fechaVencimiento;
            }

            /**
             * Define el valor de la propiedad fechaVencimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento }
             *     
             */
            public void setFechaVencimiento(DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento value) {
                this.fechaVencimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad cartera.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getCartera() {
                return cartera;
            }

            /**
             * Define el valor de la propiedad cartera.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setCartera(CodDescStringType value) {
                this.cartera = value;
            }

            /**
             * Obtiene el valor de la propiedad saldos.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos getSaldos() {
                return saldos;
            }

            /**
             * Define el valor de la propiedad saldos.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos }
             *     
             */
            public void setSaldos(DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos value) {
                this.saldos = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoPago.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getTipoPago() {
                return tipoPago;
            }

            /**
             * Define el valor de la propiedad tipoPago.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setTipoPago(CodDescStringType value) {
                this.tipoPago = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPago.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getFormaPago() {
                return formaPago;
            }

            /**
             * Define el valor de la propiedad formaPago.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setFormaPago(CodDescStringType value) {
                this.formaPago = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaDebito.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito getCuentaDebito() {
                return cuentaDebito;
            }

            /**
             * Define el valor de la propiedad cuentaDebito.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito }
             *     
             */
            public void setCuentaDebito(DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito value) {
                this.cuentaDebito = value;
            }

            /**
             * Obtiene el valor de la propiedad tasas.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas getTasas() {
                return tasas;
            }

            /**
             * Define el valor de la propiedad tasas.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas }
             *     
             */
            public void setTasas(DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas value) {
                this.tasas = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "sucursal",
                "tipo",
                "numero"
            })
            public static class CuentaDebito {

                @XmlElement(required = true)
                protected String sucursal;
                @XmlElement(required = true)
                protected String tipo;
                @XmlElement(required = true)
                protected String numero;

                /**
                 * Obtiene el valor de la propiedad sucursal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSucursal() {
                    return sucursal;
                }

                /**
                 * Define el valor de la propiedad sucursal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSucursal(String value) {
                    this.sucursal = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTipo() {
                    return tipo;
                }

                /**
                 * Define el valor de la propiedad tipo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTipo(String value) {
                    this.tipo = value;
                }

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumero(String value) {
                    this.numero = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "anterior",
                "proximo"
            })
            public static class FechaCierre {

                @XmlElement(required = true, nillable = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar anterior;
                @XmlElement(required = true, nillable = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar proximo;

                /**
                 * Obtiene el valor de la propiedad anterior.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getAnterior() {
                    return anterior;
                }

                /**
                 * Define el valor de la propiedad anterior.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setAnterior(XMLGregorianCalendar value) {
                    this.anterior = value;
                }

                /**
                 * Obtiene el valor de la propiedad proximo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getProximo() {
                    return proximo;
                }

                /**
                 * Define el valor de la propiedad proximo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setProximo(XMLGregorianCalendar value) {
                    this.proximo = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "anterior",
                "proximo"
            })
            public static class FechaVencimiento {

                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar anterior;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar proximo;

                /**
                 * Obtiene el valor de la propiedad anterior.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getAnterior() {
                    return anterior;
                }

                /**
                 * Define el valor de la propiedad anterior.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setAnterior(XMLGregorianCalendar value) {
                    this.anterior = value;
                }

                /**
                 * Obtiene el valor de la propiedad proximo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getProximo() {
                    return proximo;
                }

                /**
                 * Define el valor de la propiedad proximo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setProximo(XMLGregorianCalendar value) {
                    this.proximo = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="saldoActual" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="saldoAnterior" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "saldoActual",
                "saldoAnterior"
            })
            public static class Saldos {

                protected DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual saldoActual;
                protected DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior saldoAnterior;

                /**
                 * Obtiene el valor de la propiedad saldoActual.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual }
                 *     
                 */
                public DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual getSaldoActual() {
                    return saldoActual;
                }

                /**
                 * Define el valor de la propiedad saldoActual.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual }
                 *     
                 */
                public void setSaldoActual(DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual value) {
                    this.saldoActual = value;
                }

                /**
                 * Obtiene el valor de la propiedad saldoAnterior.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior }
                 *     
                 */
                public DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior getSaldoAnterior() {
                    return saldoAnterior;
                }

                /**
                 * Define el valor de la propiedad saldoAnterior.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior }
                 *     
                 */
                public void setSaldoAnterior(DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior value) {
                    this.saldoAnterior = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "pesos",
                    "dolares",
                    "pagoMinimo"
                })
                public static class SaldoActual {

                    @XmlElement(required = true, nillable = true)
                    protected BigDecimal pesos;
                    @XmlElement(required = true, nillable = true)
                    protected BigDecimal dolares;
                    @XmlElement(required = true)
                    protected BigDecimal pagoMinimo;

                    /**
                     * Obtiene el valor de la propiedad pesos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPesos() {
                        return pesos;
                    }

                    /**
                     * Define el valor de la propiedad pesos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPesos(BigDecimal value) {
                        this.pesos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dolares.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getDolares() {
                        return dolares;
                    }

                    /**
                     * Define el valor de la propiedad dolares.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setDolares(BigDecimal value) {
                        this.dolares = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad pagoMinimo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPagoMinimo() {
                        return pagoMinimo;
                    }

                    /**
                     * Define el valor de la propiedad pagoMinimo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPagoMinimo(BigDecimal value) {
                        this.pagoMinimo = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "pesos",
                    "dolares"
                })
                public static class SaldoAnterior {

                    @XmlElement(required = true, nillable = true)
                    protected BigDecimal pesos;
                    @XmlElement(required = true, nillable = true)
                    protected BigDecimal dolares;

                    /**
                     * Obtiene el valor de la propiedad pesos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPesos() {
                        return pesos;
                    }

                    /**
                     * Define el valor de la propiedad pesos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPesos(BigDecimal value) {
                        this.pesos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dolares.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getDolares() {
                        return dolares;
                    }

                    /**
                     * Define el valor de la propiedad dolares.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setDolares(BigDecimal value) {
                        this.dolares = value;
                    }

                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="tasaMensual" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="tasaAnual" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "tasaMensual",
                "tasaAnual"
            })
            public static class Tasas {

                protected DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual tasaMensual;
                protected DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual tasaAnual;

                /**
                 * Obtiene el valor de la propiedad tasaMensual.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual }
                 *     
                 */
                public DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual getTasaMensual() {
                    return tasaMensual;
                }

                /**
                 * Define el valor de la propiedad tasaMensual.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual }
                 *     
                 */
                public void setTasaMensual(DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual value) {
                    this.tasaMensual = value;
                }

                /**
                 * Obtiene el valor de la propiedad tasaAnual.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual }
                 *     
                 */
                public DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual getTasaAnual() {
                    return tasaAnual;
                }

                /**
                 * Define el valor de la propiedad tasaAnual.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual }
                 *     
                 */
                public void setTasaAnual(DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual value) {
                    this.tasaAnual = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "pesos",
                    "dolares"
                })
                public static class TasaAnual {

                    @XmlElement(required = true)
                    protected BigDecimal pesos;
                    @XmlElement(required = true)
                    protected BigDecimal dolares;

                    /**
                     * Obtiene el valor de la propiedad pesos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPesos() {
                        return pesos;
                    }

                    /**
                     * Define el valor de la propiedad pesos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPesos(BigDecimal value) {
                        this.pesos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dolares.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getDolares() {
                        return dolares;
                    }

                    /**
                     * Define el valor de la propiedad dolares.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setDolares(BigDecimal value) {
                        this.dolares = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "pesos",
                    "dolares"
                })
                public static class TasaMensual {

                    @XmlElement(required = true)
                    protected BigDecimal pesos;
                    @XmlElement(required = true)
                    protected BigDecimal dolares;

                    /**
                     * Obtiene el valor de la propiedad pesos.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPesos() {
                        return pesos;
                    }

                    /**
                     * Define el valor de la propiedad pesos.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPesos(BigDecimal value) {
                        this.pesos = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad dolares.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getDolares() {
                        return dolares;
                    }

                    /**
                     * Define el valor de la propiedad dolares.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setDolares(BigDecimal value) {
                        this.dolares = value;
                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *         &lt;element name="flagCoorporativa" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fechaDeVigencia">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="tipoTarjeta">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="producto">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="grupoAfinidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="limites" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="flagBoletinada" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *         &lt;element name="estadoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="datosEntrega" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "marca",
            "titularidad",
            "flagCoorporativa",
            "denominacion",
            "fechaDeVigencia",
            "tipoTarjeta",
            "producto",
            "grupoAfinidad",
            "limites",
            "flagBoletinada",
            "estadoTarjeta",
            "datosEntrega"
        })
        public static class DetalleTarjeta {

            protected CodDescStringType marca;
            @XmlElementRef(name = "titularidad", type = JAXBElement.class, required = false)
            protected JAXBElement<CodDescStringType> titularidad;
            protected Boolean flagCoorporativa;
            @XmlElement(required = true, nillable = true)
            protected String denominacion;
            @XmlElement(required = true)
            protected DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia fechaDeVigencia;
            @XmlElement(required = true)
            protected DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta tipoTarjeta;
            @XmlElement(required = true)
            protected DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto producto;
            @XmlElement(required = true)
            protected CodDescStringType grupoAfinidad;
            protected DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites limites;
            protected Boolean flagBoletinada;
            @XmlElement(required = true)
            protected CodDescStringType estadoTarjeta;
            protected DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega datosEntrega;

            /**
             * Obtiene el valor de la propiedad marca.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getMarca() {
                return marca;
            }

            /**
             * Define el valor de la propiedad marca.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setMarca(CodDescStringType value) {
                this.marca = value;
            }

            /**
             * Obtiene el valor de la propiedad titularidad.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link CodDescStringType }{@code >}
             *     
             */
            public JAXBElement<CodDescStringType> getTitularidad() {
                return titularidad;
            }

            /**
             * Define el valor de la propiedad titularidad.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link CodDescStringType }{@code >}
             *     
             */
            public void setTitularidad(JAXBElement<CodDescStringType> value) {
                this.titularidad = value;
            }

            /**
             * Obtiene el valor de la propiedad flagCoorporativa.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFlagCoorporativa() {
                return flagCoorporativa;
            }

            /**
             * Define el valor de la propiedad flagCoorporativa.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFlagCoorporativa(Boolean value) {
                this.flagCoorporativa = value;
            }

            /**
             * Obtiene el valor de la propiedad denominacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDenominacion() {
                return denominacion;
            }

            /**
             * Define el valor de la propiedad denominacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDenominacion(String value) {
                this.denominacion = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaDeVigencia.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia getFechaDeVigencia() {
                return fechaDeVigencia;
            }

            /**
             * Define el valor de la propiedad fechaDeVigencia.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia }
             *     
             */
            public void setFechaDeVigencia(DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia value) {
                this.fechaDeVigencia = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta getTipoTarjeta() {
                return tipoTarjeta;
            }

            /**
             * Define el valor de la propiedad tipoTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta }
             *     
             */
            public void setTipoTarjeta(DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta value) {
                this.tipoTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad producto.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto getProducto() {
                return producto;
            }

            /**
             * Define el valor de la propiedad producto.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto }
             *     
             */
            public void setProducto(DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto value) {
                this.producto = value;
            }

            /**
             * Obtiene el valor de la propiedad grupoAfinidad.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getGrupoAfinidad() {
                return grupoAfinidad;
            }

            /**
             * Define el valor de la propiedad grupoAfinidad.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setGrupoAfinidad(CodDescStringType value) {
                this.grupoAfinidad = value;
            }

            /**
             * Obtiene el valor de la propiedad limites.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites getLimites() {
                return limites;
            }

            /**
             * Define el valor de la propiedad limites.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites }
             *     
             */
            public void setLimites(DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites value) {
                this.limites = value;
            }

            /**
             * Obtiene el valor de la propiedad flagBoletinada.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isFlagBoletinada() {
                return flagBoletinada;
            }

            /**
             * Define el valor de la propiedad flagBoletinada.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setFlagBoletinada(Boolean value) {
                this.flagBoletinada = value;
            }

            /**
             * Obtiene el valor de la propiedad estadoTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getEstadoTarjeta() {
                return estadoTarjeta;
            }

            /**
             * Define el valor de la propiedad estadoTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setEstadoTarjeta(CodDescStringType value) {
                this.estadoTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad datosEntrega.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega }
             *     
             */
            public DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega getDatosEntrega() {
                return datosEntrega;
            }

            /**
             * Define el valor de la propiedad datosEntrega.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega }
             *     
             */
            public void setDatosEntrega(DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega value) {
                this.datosEntrega = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "sucursal"
            })
            public static class DatosEntrega {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected String sucursal;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad sucursal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSucursal() {
                    return sucursal;
                }

                /**
                 * Define el valor de la propiedad sucursal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSucursal(String value) {
                    this.sucursal = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "desde",
                "hasta"
            })
            public static class FechaDeVigencia {

                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar desde;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar hasta;

                /**
                 * Obtiene el valor de la propiedad desde.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDesde() {
                    return desde;
                }

                /**
                 * Define el valor de la propiedad desde.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDesde(XMLGregorianCalendar value) {
                    this.desde = value;
                }

                /**
                 * Obtiene el valor de la propiedad hasta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getHasta() {
                    return hasta;
                }

                /**
                 * Define el valor de la propiedad hasta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setHasta(XMLGregorianCalendar value) {
                    this.hasta = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "limiteCompra",
                "limiteFinanciacion"
            })
            public static class Limites {

                @XmlElement(required = true)
                protected BigDecimal limiteCompra;
                @XmlElement(required = true)
                protected BigDecimal limiteFinanciacion;

                /**
                 * Obtiene el valor de la propiedad limiteCompra.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getLimiteCompra() {
                    return limiteCompra;
                }

                /**
                 * Define el valor de la propiedad limiteCompra.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setLimiteCompra(BigDecimal value) {
                    this.limiteCompra = value;
                }

                /**
                 * Obtiene el valor de la propiedad limiteFinanciacion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getLimiteFinanciacion() {
                    return limiteFinanciacion;
                }

                /**
                 * Define el valor de la propiedad limiteFinanciacion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setLimiteFinanciacion(BigDecimal value) {
                    this.limiteFinanciacion = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "sigla"
            })
            public static class Producto {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected String sigla;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad sigla.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSigla() {
                    return sigla;
                }

                /**
                 * Define el valor de la propiedad sigla.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSigla(String value) {
                    this.sigla = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "sigla"
            })
            public static class TipoTarjeta {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected String sigla;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad sigla.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSigla() {
                    return sigla;
                }

                /**
                 * Define el valor de la propiedad sigla.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSigla(String value) {
                    this.sigla = value;
                }

            }

        }

    }

}
