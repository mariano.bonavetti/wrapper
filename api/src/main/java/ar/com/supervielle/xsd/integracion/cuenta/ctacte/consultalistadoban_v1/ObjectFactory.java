
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoban_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoban_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoban_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaListadoBANRespType }
     * 
     */
    public DataConsultaListadoBANRespType createDataConsultaListadoBANRespType() {
        return new DataConsultaListadoBANRespType();
    }

    /**
     * Create an instance of {@link RespConsultaListadoBAN }
     * 
     */
    public RespConsultaListadoBAN createRespConsultaListadoBAN() {
        return new RespConsultaListadoBAN();
    }

    /**
     * Create an instance of {@link ReqConsultaListadoBAN }
     * 
     */
    public ReqConsultaListadoBAN createReqConsultaListadoBAN() {
        return new ReqConsultaListadoBAN();
    }

    /**
     * Create an instance of {@link DataConsultaListadoBANReqType }
     * 
     */
    public DataConsultaListadoBANReqType createDataConsultaListadoBANReqType() {
        return new DataConsultaListadoBANReqType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoBANRespType.Row }
     * 
     */
    public DataConsultaListadoBANRespType.Row createDataConsultaListadoBANRespTypeRow() {
        return new DataConsultaListadoBANRespType.Row();
    }

}
