
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaListadoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *                   &lt;element name="Listado">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tipoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="fechaProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="entidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="saldo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="cierreFecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="marcaInhabilitacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="cuentaNroBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="producto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="gaf" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="prodSigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoRespType", propOrder = {
    "row"
})
public class DataConsultaListadoRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaListadoRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaListadoRespType.Row }
     * 
     * 
     */
    public List<DataConsultaListadoRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaListadoRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
     *         &lt;element name="Listado">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tipoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="fechaProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="entidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="saldo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="cierreFecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="marcaInhabilitacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="cuentaNroBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="producto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="gaf" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="prodSigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "listado"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdTarjetaType identificador;
        @XmlElement(name = "Listado", required = true)
        protected DataConsultaListadoRespType.Row.Listado listado;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdTarjetaType }
         *     
         */
        public IdTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdTarjetaType }
         *     
         */
        public void setIdentificador(IdTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad listado.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoRespType.Row.Listado }
         *     
         */
        public DataConsultaListadoRespType.Row.Listado getListado() {
            return listado;
        }

        /**
         * Define el valor de la propiedad listado.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoRespType.Row.Listado }
         *     
         */
        public void setListado(DataConsultaListadoRespType.Row.Listado value) {
            this.listado = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tipoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="fechaProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="entidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="saldo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="cierreFecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="marcaInhabilitacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="cuentaNroBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="producto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="gaf" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="prodSigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idCliente",
            "marca",
            "cuentaTarjeta",
            "denominacion",
            "tipoTarjeta",
            "sigla",
            "titularidad",
            "cartera",
            "vigencia",
            "fechaProcesamiento",
            "fecha",
            "entidad",
            "banco",
            "saldo",
            "cierreFecha",
            "estado",
            "marcaInhabilitacion",
            "limiteCompra",
            "formaPago",
            "cuentaNroBT",
            "producto",
            "gaf",
            "prodSigla"
        })
        public static class Listado {

            @XmlElement(required = true, nillable = true)
            protected String idCliente;
            @XmlElement(required = true)
            protected CodDescStringType marca;
            @XmlElement(required = true, nillable = true)
            protected String cuentaTarjeta;
            @XmlElement(required = true, nillable = true)
            protected String denominacion;
            @XmlElement(required = true)
            protected CodDescStringType tipoTarjeta;
            @XmlElement(required = true, nillable = true)
            protected String sigla;
            @XmlElement(required = true, nillable = true)
            protected CodDescStringType titularidad;
            @XmlElement(required = true)
            protected CodDescStringType cartera;
            @XmlElementRef(name = "vigencia", type = JAXBElement.class, required = false)
            protected JAXBElement<XMLGregorianCalendar> vigencia;
            @XmlElementRef(name = "fechaProcesamiento", type = JAXBElement.class, required = false)
            protected JAXBElement<XMLGregorianCalendar> fechaProcesamiento;
            @XmlElementRef(name = "fecha", type = JAXBElement.class, required = false)
            protected JAXBElement<XMLGregorianCalendar> fecha;
            @XmlElement(required = true)
            protected CodDescStringType entidad;
            @XmlElement(required = true)
            protected CodDescStringType banco;
            @XmlElement(required = true)
            protected DataConsultaListadoRespType.Row.Listado.Saldo saldo;
            @XmlElementRef(name = "cierreFecha", type = JAXBElement.class, required = false)
            protected JAXBElement<XMLGregorianCalendar> cierreFecha;
            @XmlElement(required = true)
            protected CodDescStringType estado;
            @XmlElement(required = true)
            protected CodDescStringType marcaInhabilitacion;
            @XmlElement(required = true)
            protected BigDecimal limiteCompra;
            @XmlElement(required = true)
            protected CodDescStringType formaPago;
            @XmlElement(required = true)
            protected String cuentaNroBT;
            @XmlElement(required = true)
            protected CodDescStringType producto;
            @XmlElement(required = true)
            protected CodDescStringType gaf;
            @XmlElement(required = true)
            protected String prodSigla;

            /**
             * Obtiene el valor de la propiedad idCliente.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdCliente() {
                return idCliente;
            }

            /**
             * Define el valor de la propiedad idCliente.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdCliente(String value) {
                this.idCliente = value;
            }

            /**
             * Obtiene el valor de la propiedad marca.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getMarca() {
                return marca;
            }

            /**
             * Define el valor de la propiedad marca.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setMarca(CodDescStringType value) {
                this.marca = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaTarjeta() {
                return cuentaTarjeta;
            }

            /**
             * Define el valor de la propiedad cuentaTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaTarjeta(String value) {
                this.cuentaTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad denominacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDenominacion() {
                return denominacion;
            }

            /**
             * Define el valor de la propiedad denominacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDenominacion(String value) {
                this.denominacion = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getTipoTarjeta() {
                return tipoTarjeta;
            }

            /**
             * Define el valor de la propiedad tipoTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setTipoTarjeta(CodDescStringType value) {
                this.tipoTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad sigla.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSigla() {
                return sigla;
            }

            /**
             * Define el valor de la propiedad sigla.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSigla(String value) {
                this.sigla = value;
            }

            /**
             * Obtiene el valor de la propiedad titularidad.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getTitularidad() {
                return titularidad;
            }

            /**
             * Define el valor de la propiedad titularidad.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setTitularidad(CodDescStringType value) {
                this.titularidad = value;
            }

            /**
             * Obtiene el valor de la propiedad cartera.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getCartera() {
                return cartera;
            }

            /**
             * Define el valor de la propiedad cartera.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setCartera(CodDescStringType value) {
                this.cartera = value;
            }

            /**
             * Obtiene el valor de la propiedad vigencia.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public JAXBElement<XMLGregorianCalendar> getVigencia() {
                return vigencia;
            }

            /**
             * Define el valor de la propiedad vigencia.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public void setVigencia(JAXBElement<XMLGregorianCalendar> value) {
                this.vigencia = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaProcesamiento.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public JAXBElement<XMLGregorianCalendar> getFechaProcesamiento() {
                return fechaProcesamiento;
            }

            /**
             * Define el valor de la propiedad fechaProcesamiento.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public void setFechaProcesamiento(JAXBElement<XMLGregorianCalendar> value) {
                this.fechaProcesamiento = value;
            }

            /**
             * Obtiene el valor de la propiedad fecha.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public JAXBElement<XMLGregorianCalendar> getFecha() {
                return fecha;
            }

            /**
             * Define el valor de la propiedad fecha.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public void setFecha(JAXBElement<XMLGregorianCalendar> value) {
                this.fecha = value;
            }

            /**
             * Obtiene el valor de la propiedad entidad.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getEntidad() {
                return entidad;
            }

            /**
             * Define el valor de la propiedad entidad.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setEntidad(CodDescStringType value) {
                this.entidad = value;
            }

            /**
             * Obtiene el valor de la propiedad banco.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getBanco() {
                return banco;
            }

            /**
             * Define el valor de la propiedad banco.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setBanco(CodDescStringType value) {
                this.banco = value;
            }

            /**
             * Obtiene el valor de la propiedad saldo.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoRespType.Row.Listado.Saldo }
             *     
             */
            public DataConsultaListadoRespType.Row.Listado.Saldo getSaldo() {
                return saldo;
            }

            /**
             * Define el valor de la propiedad saldo.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoRespType.Row.Listado.Saldo }
             *     
             */
            public void setSaldo(DataConsultaListadoRespType.Row.Listado.Saldo value) {
                this.saldo = value;
            }

            /**
             * Obtiene el valor de la propiedad cierreFecha.
             * 
             * @return
             *     possible object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public JAXBElement<XMLGregorianCalendar> getCierreFecha() {
                return cierreFecha;
            }

            /**
             * Define el valor de la propiedad cierreFecha.
             * 
             * @param value
             *     allowed object is
             *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
             *     
             */
            public void setCierreFecha(JAXBElement<XMLGregorianCalendar> value) {
                this.cierreFecha = value;
            }

            /**
             * Obtiene el valor de la propiedad estado.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getEstado() {
                return estado;
            }

            /**
             * Define el valor de la propiedad estado.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setEstado(CodDescStringType value) {
                this.estado = value;
            }

            /**
             * Obtiene el valor de la propiedad marcaInhabilitacion.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getMarcaInhabilitacion() {
                return marcaInhabilitacion;
            }

            /**
             * Define el valor de la propiedad marcaInhabilitacion.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setMarcaInhabilitacion(CodDescStringType value) {
                this.marcaInhabilitacion = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimiteCompra() {
                return limiteCompra;
            }

            /**
             * Define el valor de la propiedad limiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimiteCompra(BigDecimal value) {
                this.limiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPago.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getFormaPago() {
                return formaPago;
            }

            /**
             * Define el valor de la propiedad formaPago.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setFormaPago(CodDescStringType value) {
                this.formaPago = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaNroBT.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaNroBT() {
                return cuentaNroBT;
            }

            /**
             * Define el valor de la propiedad cuentaNroBT.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaNroBT(String value) {
                this.cuentaNroBT = value;
            }

            /**
             * Obtiene el valor de la propiedad producto.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getProducto() {
                return producto;
            }

            /**
             * Define el valor de la propiedad producto.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setProducto(CodDescStringType value) {
                this.producto = value;
            }

            /**
             * Obtiene el valor de la propiedad gaf.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getGaf() {
                return gaf;
            }

            /**
             * Define el valor de la propiedad gaf.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setGaf(CodDescStringType value) {
                this.gaf = value;
            }

            /**
             * Obtiene el valor de la propiedad prodSigla.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProdSigla() {
                return prodSigla;
            }

            /**
             * Define el valor de la propiedad prodSigla.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProdSigla(String value) {
                this.prodSigla = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pesos",
                "dolares"
            })
            public static class Saldo {

                @XmlElement(required = true, nillable = true)
                protected BigDecimal pesos;
                @XmlElement(required = true, nillable = true)
                protected BigDecimal dolares;

                /**
                 * Obtiene el valor de la propiedad pesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPesos() {
                    return pesos;
                }

                /**
                 * Define el valor de la propiedad pesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPesos(BigDecimal value) {
                    this.pesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad dolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDolares() {
                    return dolares;
                }

                /**
                 * Define el valor de la propiedad dolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDolares(BigDecimal value) {
                    this.dolares = value;
                }

            }

        }

    }

}
