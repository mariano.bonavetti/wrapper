
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetalle_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetalle_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetalle_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType2 }
     * 
     */
    public DataConsultaDetalleRespType2 createDataConsultaDetalleRespType2() {
        return new DataConsultaDetalleRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType }
     * 
     */
    public DataConsultaDetalleRespType createDataConsultaDetalleRespType() {
        return new DataConsultaDetalleRespType();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row }
     * 
     */
    public DataConsultaDetalleRespType.Row createDataConsultaDetalleRespTypeRow() {
        return new DataConsultaDetalleRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.AtributosExtendidos }
     * 
     */
    public DataConsultaDetalleRespType.Row.AtributosExtendidos createDataConsultaDetalleRespTypeRowAtributosExtendidos() {
        return new DataConsultaDetalleRespType.Row.AtributosExtendidos();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Integrantes }
     * 
     */
    public DataConsultaDetalleRespType.Row.Integrantes createDataConsultaDetalleRespTypeRowIntegrantes() {
        return new DataConsultaDetalleRespType.Row.Integrantes();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Firmas }
     * 
     */
    public DataConsultaDetalleRespType.Row.Firmas createDataConsultaDetalleRespTypeRowFirmas() {
        return new DataConsultaDetalleRespType.Row.Firmas();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.DebitosAutAdhesiones }
     * 
     */
    public DataConsultaDetalleRespType.Row.DebitosAutAdhesiones createDataConsultaDetalleRespTypeRowDebitosAutAdhesiones() {
        return new DataConsultaDetalleRespType.Row.DebitosAutAdhesiones();
    }

    /**
     * Create an instance of {@link RespConsultaDetalle }
     * 
     */
    public RespConsultaDetalle createRespConsultaDetalle() {
        return new RespConsultaDetalle();
    }

    /**
     * Create an instance of {@link ReqConsultaDetalle }
     * 
     */
    public ReqConsultaDetalle createReqConsultaDetalle() {
        return new ReqConsultaDetalle();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleReqType }
     * 
     */
    public DataConsultaDetalleReqType createDataConsultaDetalleReqType() {
        return new DataConsultaDetalleReqType();
    }

    /**
     * Create an instance of {@link RespConsultaDetalle2 }
     * 
     */
    public RespConsultaDetalle2 createRespConsultaDetalle2() {
        return new RespConsultaDetalle2();
    }

    /**
     * Create an instance of {@link ReqConsultaDetalle2 }
     * 
     */
    public ReqConsultaDetalle2 createReqConsultaDetalle2() {
        return new ReqConsultaDetalle2();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleReqType2 }
     * 
     */
    public DataConsultaDetalleReqType2 createDataConsultaDetalleReqType2() {
        return new DataConsultaDetalleReqType2();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType2 .Row }
     * 
     */
    public DataConsultaDetalleRespType2 .Row createDataConsultaDetalleRespType2Row() {
        return new DataConsultaDetalleRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Caracteristicas }
     * 
     */
    public DataConsultaDetalleRespType.Row.Caracteristicas createDataConsultaDetalleRespTypeRowCaracteristicas() {
        return new DataConsultaDetalleRespType.Row.Caracteristicas();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Moneda }
     * 
     */
    public DataConsultaDetalleRespType.Row.Moneda createDataConsultaDetalleRespTypeRowMoneda() {
        return new DataConsultaDetalleRespType.Row.Moneda();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo }
     * 
     */
    public DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo createDataConsultaDetalleRespTypeRowAtributosExtendidosAtributo() {
        return new DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Integrantes.Integrante }
     * 
     */
    public DataConsultaDetalleRespType.Row.Integrantes.Integrante createDataConsultaDetalleRespTypeRowIntegrantesIntegrante() {
        return new DataConsultaDetalleRespType.Row.Integrantes.Integrante();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Firmas.Firmante }
     * 
     */
    public DataConsultaDetalleRespType.Row.Firmas.Firmante createDataConsultaDetalleRespTypeRowFirmasFirmante() {
        return new DataConsultaDetalleRespType.Row.Firmas.Firmante();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad }
     * 
     */
    public DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad createDataConsultaDetalleRespTypeRowDebitosAutAdhesionesEntidad() {
        return new DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad();
    }

}
