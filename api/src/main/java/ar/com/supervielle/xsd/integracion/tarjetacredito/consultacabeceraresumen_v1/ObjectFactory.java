
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultacabeceraresumen_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultacabeceraresumen_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultacabeceraresumen_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCabeceraResumenRespType }
     * 
     */
    public DataConsultaCabeceraResumenRespType createDataConsultaCabeceraResumenRespType() {
        return new DataConsultaCabeceraResumenRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaCabeceraResumen }
     * 
     */
    public ReqConsultaCabeceraResumen createReqConsultaCabeceraResumen() {
        return new ReqConsultaCabeceraResumen();
    }

    /**
     * Create an instance of {@link DataConsultaCabeceraResumenReqType }
     * 
     */
    public DataConsultaCabeceraResumenReqType createDataConsultaCabeceraResumenReqType() {
        return new DataConsultaCabeceraResumenReqType();
    }

    /**
     * Create an instance of {@link RespConsultaCabeceraResumen }
     * 
     */
    public RespConsultaCabeceraResumen createRespConsultaCabeceraResumen() {
        return new RespConsultaCabeceraResumen();
    }

    /**
     * Create an instance of {@link DataConsultaCabeceraResumenRespType.Row }
     * 
     */
    public DataConsultaCabeceraResumenRespType.Row createDataConsultaCabeceraResumenRespTypeRow() {
        return new DataConsultaCabeceraResumenRespType.Row();
    }

}
