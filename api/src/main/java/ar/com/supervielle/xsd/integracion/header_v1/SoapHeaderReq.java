
package ar.com.supervielle.xsd.integracion.header_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SoapHeaderReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SoapHeaderReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="runtimeContext" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="executionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="sessionContext" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}SessionContext"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SoapHeaderReq", propOrder = {
    "runtimeContext",
    "sessionContext"
})
public class SoapHeaderReq {

    protected SoapHeaderReq.RuntimeContext runtimeContext;
    @XmlElement(required = true)
    protected SessionContext sessionContext;

    /**
     * Obtiene el valor de la propiedad runtimeContext.
     * 
     * @return
     *     possible object is
     *     {@link SoapHeaderReq.RuntimeContext }
     *     
     */
    public SoapHeaderReq.RuntimeContext getRuntimeContext() {
        return runtimeContext;
    }

    /**
     * Define el valor de la propiedad runtimeContext.
     * 
     * @param value
     *     allowed object is
     *     {@link SoapHeaderReq.RuntimeContext }
     *     
     */
    public void setRuntimeContext(SoapHeaderReq.RuntimeContext value) {
        this.runtimeContext = value;
    }

    /**
     * Obtiene el valor de la propiedad sessionContext.
     * 
     * @return
     *     possible object is
     *     {@link SessionContext }
     *     
     */
    public SessionContext getSessionContext() {
        return sessionContext;
    }

    /**
     * Define el valor de la propiedad sessionContext.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionContext }
     *     
     */
    public void setSessionContext(SessionContext value) {
        this.sessionContext = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="executionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "executionType"
    })
    public static class RuntimeContext {

        @XmlElement(defaultValue = "normal")
        protected String executionType;

        /**
         * Obtiene el valor de la propiedad executionType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExecutionType() {
            return executionType;
        }

        /**
         * Define el valor de la propiedad executionType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExecutionType(String value) {
            this.executionType = value;
        }

    }

}
