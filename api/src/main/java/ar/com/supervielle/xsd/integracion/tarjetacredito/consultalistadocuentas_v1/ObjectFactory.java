
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistadocuentas_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistadocuentas_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistadocuentas_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType }
     * 
     */
    public DataConsultaListadoCuentasRespType createDataConsultaListadoCuentasRespType() {
        return new DataConsultaListadoCuentasRespType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row createDataConsultaListadoCuentasRespTypeRow() {
        return new DataConsultaListadoCuentasRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta createDataConsultaListadoCuentasRespTypeRowCuentaTarjeta() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaFormaPago() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.Empresa }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.Empresa createDataConsultaListadoCuentasRespTypeRowEmpresa() {
        return new DataConsultaListadoCuentasRespType.Row.Empresa();
    }

    /**
     * Create an instance of {@link ReqConsultaListadoCuentas }
     * 
     */
    public ReqConsultaListadoCuentas createReqConsultaListadoCuentas() {
        return new ReqConsultaListadoCuentas();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasReqType }
     * 
     */
    public DataConsultaListadoCuentasReqType createDataConsultaListadoCuentasReqType() {
        return new DataConsultaListadoCuentasReqType();
    }

    /**
     * Create an instance of {@link RespConsultaListadoCuentas }
     * 
     */
    public RespConsultaListadoCuentas createRespConsultaListadoCuentas() {
        return new RespConsultaListadoCuentas();
    }

    /**
     * Create an instance of {@link IdCtaTarjetaType }
     * 
     */
    public IdCtaTarjetaType createIdCtaTarjetaType() {
        return new IdCtaTarjetaType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaCuentaMarca() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaProducto() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaGAF() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaModeloLiquidacion() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaOrigenAlta() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaLiquidacionActual() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaLiquidacionAnterior() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaDomicilioCuenta() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaDomicilioCorrespondencia() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito createDataConsultaListadoCuentasRespTypeRowCuentaTarjetaFormaPagoCuentaDebito() {
        return new DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.Empresa.Producto }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.Empresa.Producto createDataConsultaListadoCuentasRespTypeRowEmpresaProducto() {
        return new DataConsultaListadoCuentasRespType.Row.Empresa.Producto();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio }
     * 
     */
    public DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio createDataConsultaListadoCuentasRespTypeRowEmpresaDomicilio() {
        return new DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio();
    }

}
