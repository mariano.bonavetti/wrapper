
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EmpresaBasePMC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EmpresaBasePMC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdBanco" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datosAyudaEmpresaBean" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}DatosAyudaEmpresa" minOccurs="0"/>
 *         &lt;element name="orden" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="permiteUsd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="rubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdRubro" minOccurs="0"/>
 *         &lt;element name="tipoEmpresa" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tituloIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmpresaBasePMC", propOrder = {
    "banco",
    "codigo",
    "datosAyudaEmpresaBean",
    "orden",
    "permiteUsd",
    "rubro",
    "tipoEmpresa",
    "tituloIdentificacion",
    "nombre"
})
public class EmpresaBasePMC {

    protected IdBanco banco;
    protected String codigo;
    protected DatosAyudaEmpresa datosAyudaEmpresaBean;
    protected Integer orden;
    protected Boolean permiteUsd;
    protected IdRubro rubro;
    protected EmpresaBasePMC.TipoEmpresa tipoEmpresa;
    protected String tituloIdentificacion;
    protected String nombre;

    /**
     * Obtiene el valor de la propiedad banco.
     * 
     * @return
     *     possible object is
     *     {@link IdBanco }
     *     
     */
    public IdBanco getBanco() {
        return banco;
    }

    /**
     * Define el valor de la propiedad banco.
     * 
     * @param value
     *     allowed object is
     *     {@link IdBanco }
     *     
     */
    public void setBanco(IdBanco value) {
        this.banco = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad datosAyudaEmpresaBean.
     * 
     * @return
     *     possible object is
     *     {@link DatosAyudaEmpresa }
     *     
     */
    public DatosAyudaEmpresa getDatosAyudaEmpresaBean() {
        return datosAyudaEmpresaBean;
    }

    /**
     * Define el valor de la propiedad datosAyudaEmpresaBean.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosAyudaEmpresa }
     *     
     */
    public void setDatosAyudaEmpresaBean(DatosAyudaEmpresa value) {
        this.datosAyudaEmpresaBean = value;
    }

    /**
     * Obtiene el valor de la propiedad orden.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrden() {
        return orden;
    }

    /**
     * Define el valor de la propiedad orden.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrden(Integer value) {
        this.orden = value;
    }

    /**
     * Obtiene el valor de la propiedad permiteUsd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPermiteUsd() {
        return permiteUsd;
    }

    /**
     * Define el valor de la propiedad permiteUsd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPermiteUsd(Boolean value) {
        this.permiteUsd = value;
    }

    /**
     * Obtiene el valor de la propiedad rubro.
     * 
     * @return
     *     possible object is
     *     {@link IdRubro }
     *     
     */
    public IdRubro getRubro() {
        return rubro;
    }

    /**
     * Define el valor de la propiedad rubro.
     * 
     * @param value
     *     allowed object is
     *     {@link IdRubro }
     *     
     */
    public void setRubro(IdRubro value) {
        this.rubro = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link EmpresaBasePMC.TipoEmpresa }
     *     
     */
    public EmpresaBasePMC.TipoEmpresa getTipoEmpresa() {
        return tipoEmpresa;
    }

    /**
     * Define el valor de la propiedad tipoEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpresaBasePMC.TipoEmpresa }
     *     
     */
    public void setTipoEmpresa(EmpresaBasePMC.TipoEmpresa value) {
        this.tipoEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad tituloIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTituloIdentificacion() {
        return tituloIdentificacion;
    }

    /**
     * Define el valor de la propiedad tituloIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTituloIdentificacion(String value) {
        this.tituloIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codigo",
        "descripcion"
    })
    public static class TipoEmpresa {

        protected String codigo;
        protected String descripcion;

        /**
         * Obtiene el valor de la propiedad codigo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigo() {
            return codigo;
        }

        /**
         * Define el valor de la propiedad codigo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigo(String value) {
            this.codigo = value;
        }

        /**
         * Obtiene el valor de la propiedad descripcion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescripcion() {
            return descripcion;
        }

        /**
         * Define el valor de la propiedad descripcion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescripcion(String value) {
            this.descripcion = value;
        }

    }

}
