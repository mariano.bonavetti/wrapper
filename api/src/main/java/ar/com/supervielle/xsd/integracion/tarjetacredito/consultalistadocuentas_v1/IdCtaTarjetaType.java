
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistadocuentas_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para idCtaTarjetaType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="idCtaTarjetaType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idCtaTarjetaType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "idCtaTarjetaType")
public class IdCtaTarjetaType
    extends ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCtaTarjetaType
{


}
