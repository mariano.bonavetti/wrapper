
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimiteempresa_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaLimiteEmpresaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaLimiteEmpresaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;choice>
 *           &lt;element name="cuentaEmpresaVISA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaLimiteEmpresaReqType", propOrder = {
    "canal",
    "subCanal",
    "tipoProducto",
    "cuentaEmpresaVISA",
    "cuentaCliente"
})
public class DataConsultaLimiteEmpresaReqType {

    @XmlElement(required = true)
    protected BigInteger canal;
    @XmlElement(required = true)
    protected BigInteger subCanal;
    @XmlElement(required = true)
    protected BigInteger tipoProducto;
    protected String cuentaEmpresaVISA;
    protected BigInteger cuentaCliente;

    /**
     * Obtiene el valor de la propiedad canal.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCanal() {
        return canal;
    }

    /**
     * Define el valor de la propiedad canal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCanal(BigInteger value) {
        this.canal = value;
    }

    /**
     * Obtiene el valor de la propiedad subCanal.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubCanal() {
        return subCanal;
    }

    /**
     * Define el valor de la propiedad subCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubCanal(BigInteger value) {
        this.subCanal = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProducto.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTipoProducto() {
        return tipoProducto;
    }

    /**
     * Define el valor de la propiedad tipoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTipoProducto(BigInteger value) {
        this.tipoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaEmpresaVISA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaEmpresaVISA() {
        return cuentaEmpresaVISA;
    }

    /**
     * Define el valor de la propiedad cuentaEmpresaVISA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaEmpresaVISA(String value) {
        this.cuentaEmpresaVISA = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCliente.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCuentaCliente() {
        return cuentaCliente;
    }

    /**
     * Define el valor de la propiedad cuentaCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCuentaCliente(BigInteger value) {
        this.cuentaCliente = value;
    }

}
