
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosRespType }
     * 
     */
    public DataConsultaEmbargosRespType createDataConsultaEmbargosRespType() {
        return new DataConsultaEmbargosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosRespType.Row }
     * 
     */
    public DataConsultaEmbargosRespType.Row createDataConsultaEmbargosRespTypeRow() {
        return new DataConsultaEmbargosRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaEmbargos }
     * 
     */
    public ReqConsultaEmbargos createReqConsultaEmbargos() {
        return new ReqConsultaEmbargos();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosReqType }
     * 
     */
    public DataConsultaEmbargosReqType createDataConsultaEmbargosReqType() {
        return new DataConsultaEmbargosReqType();
    }

    /**
     * Create an instance of {@link RespConsultaEmbargos }
     * 
     */
    public RespConsultaEmbargos createRespConsultaEmbargos() {
        return new RespConsultaEmbargos();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosRespType.Row.Oficio }
     * 
     */
    public DataConsultaEmbargosRespType.Row.Oficio createDataConsultaEmbargosRespTypeRowOficio() {
        return new DataConsultaEmbargosRespType.Row.Oficio();
    }

}
