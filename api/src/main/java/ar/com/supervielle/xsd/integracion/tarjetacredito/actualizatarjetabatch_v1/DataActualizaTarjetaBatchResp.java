
package ar.com.supervielle.xsd.integracion.tarjetacredito.actualizatarjetabatch_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataActualizaTarjetaBatchResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataActualizaTarjetaBatchResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idTX" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataActualizaTarjetaBatchResp", propOrder = {
    "row"
})
public class DataActualizaTarjetaBatchResp {

    @XmlElement(name = "Row")
    protected DataActualizaTarjetaBatchResp.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataActualizaTarjetaBatchResp.Row }
     *     
     */
    public DataActualizaTarjetaBatchResp.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataActualizaTarjetaBatchResp.Row }
     *     
     */
    public void setRow(DataActualizaTarjetaBatchResp.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idTX" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idTX",
        "estado"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String idTX;
        @XmlElement(name = "Estado", required = true)
        protected String estado;

        /**
         * Obtiene el valor de la propiedad idTX.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdTX() {
            return idTX;
        }

        /**
         * Define el valor de la propiedad idTX.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdTX(String value) {
            this.idTX = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEstado(String value) {
            this.estado = value;
        }

    }

}
