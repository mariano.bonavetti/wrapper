
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaadicional_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdSexoType;


/**
 * <p>Clase Java para DataAltaAdicionalReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAdicionalReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TitularAdicional">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Principal">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="tipoAlta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="cuentaBanTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="tipoDocTribu" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="numDocTribu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="porcenLCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="porcenLCCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="porcenLAdel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Filiatorios">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nomCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codNacionalidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="nomEmbozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idSexoType"/>
 *                             &lt;element name="codOcupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Complementarios">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="cuentaSocio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAdicionalReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/altaAdicional-v1", propOrder = {
    "titularAdicional"
})
public class DataAltaAdicionalReqType {

    @XmlElement(name = "TitularAdicional", required = true)
    protected DataAltaAdicionalReqType.TitularAdicional titularAdicional;

    /**
     * Obtiene el valor de la propiedad titularAdicional.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAdicionalReqType.TitularAdicional }
     *     
     */
    public DataAltaAdicionalReqType.TitularAdicional getTitularAdicional() {
        return titularAdicional;
    }

    /**
     * Define el valor de la propiedad titularAdicional.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAdicionalReqType.TitularAdicional }
     *     
     */
    public void setTitularAdicional(DataAltaAdicionalReqType.TitularAdicional value) {
        this.titularAdicional = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Principal">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tipoAlta" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="cuentaBanTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tipoDocTribu" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="numDocTribu" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="porcenLCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="porcenLCCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="porcenLAdel" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Filiatorios">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nomCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codNacionalidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="nomEmbozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idSexoType"/>
     *                   &lt;element name="codOcupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Complementarios">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="cuentaSocio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codOperacion",
        "principal",
        "filiatorios",
        "complementarios"
    })
    public static class TitularAdicional {

        protected int codOperacion;
        @XmlElement(name = "Principal", required = true)
        protected DataAltaAdicionalReqType.TitularAdicional.Principal principal;
        @XmlElement(name = "Filiatorios", required = true)
        protected DataAltaAdicionalReqType.TitularAdicional.Filiatorios filiatorios;
        @XmlElement(name = "Complementarios", required = true)
        protected DataAltaAdicionalReqType.TitularAdicional.Complementarios complementarios;

        /**
         * Obtiene el valor de la propiedad codOperacion.
         * 
         */
        public int getCodOperacion() {
            return codOperacion;
        }

        /**
         * Define el valor de la propiedad codOperacion.
         * 
         */
        public void setCodOperacion(int value) {
            this.codOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad principal.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaAdicionalReqType.TitularAdicional.Principal }
         *     
         */
        public DataAltaAdicionalReqType.TitularAdicional.Principal getPrincipal() {
            return principal;
        }

        /**
         * Define el valor de la propiedad principal.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaAdicionalReqType.TitularAdicional.Principal }
         *     
         */
        public void setPrincipal(DataAltaAdicionalReqType.TitularAdicional.Principal value) {
            this.principal = value;
        }

        /**
         * Obtiene el valor de la propiedad filiatorios.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaAdicionalReqType.TitularAdicional.Filiatorios }
         *     
         */
        public DataAltaAdicionalReqType.TitularAdicional.Filiatorios getFiliatorios() {
            return filiatorios;
        }

        /**
         * Define el valor de la propiedad filiatorios.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaAdicionalReqType.TitularAdicional.Filiatorios }
         *     
         */
        public void setFiliatorios(DataAltaAdicionalReqType.TitularAdicional.Filiatorios value) {
            this.filiatorios = value;
        }

        /**
         * Obtiene el valor de la propiedad complementarios.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaAdicionalReqType.TitularAdicional.Complementarios }
         *     
         */
        public DataAltaAdicionalReqType.TitularAdicional.Complementarios getComplementarios() {
            return complementarios;
        }

        /**
         * Define el valor de la propiedad complementarios.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaAdicionalReqType.TitularAdicional.Complementarios }
         *     
         */
        public void setComplementarios(DataAltaAdicionalReqType.TitularAdicional.Complementarios value) {
            this.complementarios = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="cuentaSocio" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "usuarioCarga",
            "fechaSolicitud",
            "cuentaSocio"
        })
        public static class Complementarios {

            @XmlElement(required = true)
            protected String usuarioCarga;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaSolicitud;
            @XmlElement(required = true)
            protected BigInteger cuentaSocio;

            /**
             * Obtiene el valor de la propiedad usuarioCarga.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUsuarioCarga() {
                return usuarioCarga;
            }

            /**
             * Define el valor de la propiedad usuarioCarga.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUsuarioCarga(String value) {
                this.usuarioCarga = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaSolicitud() {
                return fechaSolicitud;
            }

            /**
             * Define el valor de la propiedad fechaSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaSolicitud(XMLGregorianCalendar value) {
                this.fechaSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaSocio.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCuentaSocio() {
                return cuentaSocio;
            }

            /**
             * Define el valor de la propiedad cuentaSocio.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCuentaSocio(BigInteger value) {
                this.cuentaSocio = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nomCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codNacionalidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="nomEmbozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idSexoType"/>
         *         &lt;element name="codOcupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nomCliente",
            "codNacionalidad",
            "nomEmbozado",
            "codEstadoCivil",
            "fechaNacimiento",
            "sexo",
            "codOcupacion"
        })
        public static class Filiatorios {

            @XmlElement(required = true)
            protected String nomCliente;
            protected int codNacionalidad;
            @XmlElement(required = true)
            protected String nomEmbozado;
            protected int codEstadoCivil;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaNacimiento;
            @XmlElement(required = true)
            @XmlSchemaType(name = "string")
            protected IdSexoType sexo;
            protected int codOcupacion;

            /**
             * Obtiene el valor de la propiedad nomCliente.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNomCliente() {
                return nomCliente;
            }

            /**
             * Define el valor de la propiedad nomCliente.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNomCliente(String value) {
                this.nomCliente = value;
            }

            /**
             * Obtiene el valor de la propiedad codNacionalidad.
             * 
             */
            public int getCodNacionalidad() {
                return codNacionalidad;
            }

            /**
             * Define el valor de la propiedad codNacionalidad.
             * 
             */
            public void setCodNacionalidad(int value) {
                this.codNacionalidad = value;
            }

            /**
             * Obtiene el valor de la propiedad nomEmbozado.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNomEmbozado() {
                return nomEmbozado;
            }

            /**
             * Define el valor de la propiedad nomEmbozado.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNomEmbozado(String value) {
                this.nomEmbozado = value;
            }

            /**
             * Obtiene el valor de la propiedad codEstadoCivil.
             * 
             */
            public int getCodEstadoCivil() {
                return codEstadoCivil;
            }

            /**
             * Define el valor de la propiedad codEstadoCivil.
             * 
             */
            public void setCodEstadoCivil(int value) {
                this.codEstadoCivil = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaNacimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaNacimiento() {
                return fechaNacimiento;
            }

            /**
             * Define el valor de la propiedad fechaNacimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaNacimiento(XMLGregorianCalendar value) {
                this.fechaNacimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad sexo.
             * 
             * @return
             *     possible object is
             *     {@link IdSexoType }
             *     
             */
            public IdSexoType getSexo() {
                return sexo;
            }

            /**
             * Define el valor de la propiedad sexo.
             * 
             * @param value
             *     allowed object is
             *     {@link IdSexoType }
             *     
             */
            public void setSexo(IdSexoType value) {
                this.sexo = value;
            }

            /**
             * Obtiene el valor de la propiedad codOcupacion.
             * 
             */
            public int getCodOcupacion() {
                return codOcupacion;
            }

            /**
             * Define el valor de la propiedad codOcupacion.
             * 
             */
            public void setCodOcupacion(int value) {
                this.codOcupacion = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tipoAlta" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="cuentaBanTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tipoDocTribu" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="numDocTribu" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="porcenLCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="porcenLCCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="porcenLAdel" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codMarca",
            "numEntidad",
            "tipoAlta",
            "cuentaBanTotal",
            "nroSolicitud",
            "tipoDoc",
            "nroDoc",
            "tipoDocTribu",
            "numDocTribu",
            "porcenLCompra",
            "porcenLCCuotas",
            "porcenLAdel"
        })
        public static class Principal {

            protected int codMarca;
            protected int numEntidad;
            protected int tipoAlta;
            protected int cuentaBanTotal;
            @XmlElement(required = true)
            protected BigInteger nroSolicitud;
            protected int tipoDoc;
            protected int nroDoc;
            protected int tipoDocTribu;
            @XmlElement(required = true)
            protected String numDocTribu;
            @XmlElement(required = true)
            protected BigDecimal porcenLCompra;
            @XmlElement(required = true)
            protected BigDecimal porcenLCCuotas;
            protected int porcenLAdel;

            /**
             * Obtiene el valor de la propiedad codMarca.
             * 
             */
            public int getCodMarca() {
                return codMarca;
            }

            /**
             * Define el valor de la propiedad codMarca.
             * 
             */
            public void setCodMarca(int value) {
                this.codMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad numEntidad.
             * 
             */
            public int getNumEntidad() {
                return numEntidad;
            }

            /**
             * Define el valor de la propiedad numEntidad.
             * 
             */
            public void setNumEntidad(int value) {
                this.numEntidad = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoAlta.
             * 
             */
            public int getTipoAlta() {
                return tipoAlta;
            }

            /**
             * Define el valor de la propiedad tipoAlta.
             * 
             */
            public void setTipoAlta(int value) {
                this.tipoAlta = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaBanTotal.
             * 
             */
            public int getCuentaBanTotal() {
                return cuentaBanTotal;
            }

            /**
             * Define el valor de la propiedad cuentaBanTotal.
             * 
             */
            public void setCuentaBanTotal(int value) {
                this.cuentaBanTotal = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitud() {
                return nroSolicitud;
            }

            /**
             * Define el valor de la propiedad nroSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitud(BigInteger value) {
                this.nroSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoDoc.
             * 
             */
            public int getTipoDoc() {
                return tipoDoc;
            }

            /**
             * Define el valor de la propiedad tipoDoc.
             * 
             */
            public void setTipoDoc(int value) {
                this.tipoDoc = value;
            }

            /**
             * Obtiene el valor de la propiedad nroDoc.
             * 
             */
            public int getNroDoc() {
                return nroDoc;
            }

            /**
             * Define el valor de la propiedad nroDoc.
             * 
             */
            public void setNroDoc(int value) {
                this.nroDoc = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoDocTribu.
             * 
             */
            public int getTipoDocTribu() {
                return tipoDocTribu;
            }

            /**
             * Define el valor de la propiedad tipoDocTribu.
             * 
             */
            public void setTipoDocTribu(int value) {
                this.tipoDocTribu = value;
            }

            /**
             * Obtiene el valor de la propiedad numDocTribu.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumDocTribu() {
                return numDocTribu;
            }

            /**
             * Define el valor de la propiedad numDocTribu.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumDocTribu(String value) {
                this.numDocTribu = value;
            }

            /**
             * Obtiene el valor de la propiedad porcenLCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPorcenLCompra() {
                return porcenLCompra;
            }

            /**
             * Define el valor de la propiedad porcenLCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPorcenLCompra(BigDecimal value) {
                this.porcenLCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad porcenLCCuotas.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPorcenLCCuotas() {
                return porcenLCCuotas;
            }

            /**
             * Define el valor de la propiedad porcenLCCuotas.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPorcenLCCuotas(BigDecimal value) {
                this.porcenLCCuotas = value;
            }

            /**
             * Obtiene el valor de la propiedad porcenLAdel.
             * 
             */
            public int getPorcenLAdel() {
                return porcenLAdel;
            }

            /**
             * Define el valor de la propiedad porcenLAdel.
             * 
             */
            public void setPorcenLAdel(int value) {
                this.porcenLAdel = value;
            }

        }

    }

}
