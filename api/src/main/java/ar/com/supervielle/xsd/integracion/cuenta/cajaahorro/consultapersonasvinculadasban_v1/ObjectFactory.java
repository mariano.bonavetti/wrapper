
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultapersonasvinculadasban_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultapersonasvinculadasban_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultapersonasvinculadasban_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasBANRespType }
     * 
     */
    public DataConsultaPersonasVinculadasBANRespType createDataConsultaPersonasVinculadasBANRespType() {
        return new DataConsultaPersonasVinculadasBANRespType();
    }

    /**
     * Create an instance of {@link RespConsultaPersonasVinculadasBAN }
     * 
     */
    public RespConsultaPersonasVinculadasBAN createRespConsultaPersonasVinculadasBAN() {
        return new RespConsultaPersonasVinculadasBAN();
    }

    /**
     * Create an instance of {@link ReqConsultaPersonasVinculadasBAN }
     * 
     */
    public ReqConsultaPersonasVinculadasBAN createReqConsultaPersonasVinculadasBAN() {
        return new ReqConsultaPersonasVinculadasBAN();
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasBANReqType }
     * 
     */
    public DataConsultaPersonasVinculadasBANReqType createDataConsultaPersonasVinculadasBANReqType() {
        return new DataConsultaPersonasVinculadasBANReqType();
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasBANRespType.Row }
     * 
     */
    public DataConsultaPersonasVinculadasBANRespType.Row createDataConsultaPersonasVinculadasBANRespTypeRow() {
        return new DataConsultaPersonasVinculadasBANRespType.Row();
    }

}
