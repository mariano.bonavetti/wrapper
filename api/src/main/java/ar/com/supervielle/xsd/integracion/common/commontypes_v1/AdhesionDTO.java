
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AdhesionDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AdhesionDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coordenadas" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}CoordenadasPMC" minOccurs="0"/>
 *         &lt;element name="empresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}EmpresaBasePMC" minOccurs="0"/>
 *         &lt;element name="identificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdhesionDTO", propOrder = {
    "coordenadas",
    "empresa",
    "identificacion",
    "password"
})
public class AdhesionDTO {

    protected CoordenadasPMC coordenadas;
    protected EmpresaBasePMC empresa;
    protected String identificacion;
    protected String password;

    /**
     * Obtiene el valor de la propiedad coordenadas.
     * 
     * @return
     *     possible object is
     *     {@link CoordenadasPMC }
     *     
     */
    public CoordenadasPMC getCoordenadas() {
        return coordenadas;
    }

    /**
     * Define el valor de la propiedad coordenadas.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordenadasPMC }
     *     
     */
    public void setCoordenadas(CoordenadasPMC value) {
        this.coordenadas = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link EmpresaBasePMC }
     *     
     */
    public EmpresaBasePMC getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpresaBasePMC }
     *     
     */
    public void setEmpresa(EmpresaBasePMC value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad identificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Define el valor de la propiedad identificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificacion(String value) {
        this.identificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad password.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Define el valor de la propiedad password.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
