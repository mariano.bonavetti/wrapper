
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoxcliente_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoxcliente_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoxcliente_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp }
     * 
     */
    public DataConsultaEstadoxClienteResp createDataConsultaEstadoxClienteResp() {
        return new DataConsultaEstadoxClienteResp();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp.Row }
     * 
     */
    public DataConsultaEstadoxClienteResp.Row createDataConsultaEstadoxClienteRespRow() {
        return new DataConsultaEstadoxClienteResp.Row();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp.Row.Historial }
     * 
     */
    public DataConsultaEstadoxClienteResp.Row.Historial createDataConsultaEstadoxClienteRespRowHistorial() {
        return new DataConsultaEstadoxClienteResp.Row.Historial();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro }
     * 
     */
    public DataConsultaEstadoxClienteResp.Row.Historial.Registro createDataConsultaEstadoxClienteRespRowHistorialRegistro() {
        return new DataConsultaEstadoxClienteResp.Row.Historial.Registro();
    }

    /**
     * Create an instance of {@link ReqConsultaEstadoxCliente }
     * 
     */
    public ReqConsultaEstadoxCliente createReqConsultaEstadoxCliente() {
        return new ReqConsultaEstadoxCliente();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteReq }
     * 
     */
    public DataConsultaEstadoxClienteReq createDataConsultaEstadoxClienteReq() {
        return new DataConsultaEstadoxClienteReq();
    }

    /**
     * Create an instance of {@link RespConsultaEstadoxCliente }
     * 
     */
    public RespConsultaEstadoxCliente createRespConsultaEstadoxCliente() {
        return new RespConsultaEstadoxCliente();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp.Row.Marca }
     * 
     */
    public DataConsultaEstadoxClienteResp.Row.Marca createDataConsultaEstadoxClienteRespRowMarca() {
        return new DataConsultaEstadoxClienteResp.Row.Marca();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado }
     * 
     */
    public DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado createDataConsultaEstadoxClienteRespRowHistorialRegistroEstado() {
        return new DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error }
     * 
     */
    public DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error createDataConsultaEstadoxClienteRespRowHistorialRegistroError() {
        return new DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error();
    }

}
