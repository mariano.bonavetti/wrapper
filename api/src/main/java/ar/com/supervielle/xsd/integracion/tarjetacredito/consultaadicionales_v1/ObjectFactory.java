
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaadicionales_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaadicionales_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaadicionales_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaAdicionalesRespType }
     * 
     */
    public DataConsultaAdicionalesRespType createDataConsultaAdicionalesRespType() {
        return new DataConsultaAdicionalesRespType();
    }

    /**
     * Create an instance of {@link DataConsultaAdicionalesRespType.Row }
     * 
     */
    public DataConsultaAdicionalesRespType.Row createDataConsultaAdicionalesRespTypeRow() {
        return new DataConsultaAdicionalesRespType.Row();
    }

    /**
     * Create an instance of {@link RespConsultaAdicionales }
     * 
     */
    public RespConsultaAdicionales createRespConsultaAdicionales() {
        return new RespConsultaAdicionales();
    }

    /**
     * Create an instance of {@link ReqConsultaAdicionales }
     * 
     */
    public ReqConsultaAdicionales createReqConsultaAdicionales() {
        return new ReqConsultaAdicionales();
    }

    /**
     * Create an instance of {@link DataConsultaAdicionalesReqType }
     * 
     */
    public DataConsultaAdicionalesReqType createDataConsultaAdicionalesReqType() {
        return new DataConsultaAdicionalesReqType();
    }

    /**
     * Create an instance of {@link DataConsultaAdicionalesRespType.Row.Saldos }
     * 
     */
    public DataConsultaAdicionalesRespType.Row.Saldos createDataConsultaAdicionalesRespTypeRowSaldos() {
        return new DataConsultaAdicionalesRespType.Row.Saldos();
    }

}
