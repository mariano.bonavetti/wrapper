
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultaproductos_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.RangoFechaType;


/**
 * <p>Clase Java para DataConsultaProductosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaProductosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="productos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="producto" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="descripcion" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="puntos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                       &lt;element name="puntosAnteriores" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                                       &lt;element name="rubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="subRubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="vigenciaFecha" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}rangoFechaType" minOccurs="0"/>
 *                                       &lt;element name="canje" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="novedad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                                       &lt;element name="stock" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                                                 &lt;element name="maxDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                                                 &lt;element name="minDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="pathImagen" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="proveedor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="precio" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="conIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                                 &lt;element name="sinIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                                 &lt;element name="publico" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="escala" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                                       &lt;element name="consumo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="codProductoExterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaProductosRespType", propOrder = {
    "row"
})
public class DataConsultaProductosRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaProductosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaProductosRespType.Row }
     * 
     * 
     */
    public List<DataConsultaProductosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaProductosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="productos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="producto" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="descripcion" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="puntos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                             &lt;element name="puntosAnteriores" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                             &lt;element name="rubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="subRubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="vigenciaFecha" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}rangoFechaType" minOccurs="0"/>
     *                             &lt;element name="canje" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="novedad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                             &lt;element name="stock" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                                       &lt;element name="maxDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                                       &lt;element name="minDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="pathImagen" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="proveedor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="precio" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="conIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                                       &lt;element name="sinIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                                       &lt;element name="publico" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="escala" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                             &lt;element name="consumo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="codProductoExterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultado",
        "productos"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String resultado;
        @XmlElement(required = true)
        protected DataConsultaProductosRespType.Row.Productos productos;

        /**
         * Obtiene el valor de la propiedad resultado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultado() {
            return resultado;
        }

        /**
         * Define el valor de la propiedad resultado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultado(String value) {
            this.resultado = value;
        }

        /**
         * Obtiene el valor de la propiedad productos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaProductosRespType.Row.Productos }
         *     
         */
        public DataConsultaProductosRespType.Row.Productos getProductos() {
            return productos;
        }

        /**
         * Define el valor de la propiedad productos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaProductosRespType.Row.Productos }
         *     
         */
        public void setProductos(DataConsultaProductosRespType.Row.Productos value) {
            this.productos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="producto" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="descripcion" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="puntos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                   &lt;element name="puntosAnteriores" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *                   &lt;element name="rubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="subRubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="vigenciaFecha" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}rangoFechaType" minOccurs="0"/>
         *                   &lt;element name="canje" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="novedad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *                   &lt;element name="stock" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *                             &lt;element name="maxDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *                             &lt;element name="minDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="pathImagen" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="proveedor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="precio" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="conIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                             &lt;element name="sinIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                             &lt;element name="publico" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="escala" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *                   &lt;element name="consumo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="codProductoExterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "producto"
        })
        public static class Productos {

            protected List<DataConsultaProductosRespType.Row.Productos.Producto> producto;

            /**
             * Gets the value of the producto property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the producto property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getProducto().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaProductosRespType.Row.Productos.Producto }
             * 
             * 
             */
            public List<DataConsultaProductosRespType.Row.Productos.Producto> getProducto() {
                if (producto == null) {
                    producto = new ArrayList<DataConsultaProductosRespType.Row.Productos.Producto>();
                }
                return this.producto;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="descripcion" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="puntos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *         &lt;element name="puntosAnteriores" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
             *         &lt;element name="rubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="subRubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="vigenciaFecha" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}rangoFechaType" minOccurs="0"/>
             *         &lt;element name="canje" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="novedad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
             *         &lt;element name="stock" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
             *                   &lt;element name="maxDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
             *                   &lt;element name="minDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="pathImagen" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="proveedor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="precio" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="conIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *                   &lt;element name="sinIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *                   &lt;element name="publico" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="escala" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
             *         &lt;element name="consumo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="codProductoExterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idProducto",
                "descripcion",
                "puntos",
                "puntosAnteriores",
                "rubro",
                "subRubro",
                "vigencia",
                "vigenciaFecha",
                "canje",
                "novedad",
                "stock",
                "pathImagen",
                "tipoProducto",
                "proveedor",
                "precio",
                "escala",
                "consumo",
                "codProductoExterno"
            })
            public static class Producto {

                protected int idProducto;
                protected DataConsultaProductosRespType.Row.Productos.Producto.Descripcion descripcion;
                protected BigInteger puntos;
                protected Integer puntosAnteriores;
                protected CodDescStringType rubro;
                protected CodDescStringType subRubro;
                protected String vigencia;
                protected RangoFechaType vigenciaFecha;
                protected CodDescStringType canje;
                protected Boolean novedad;
                protected DataConsultaProductosRespType.Row.Productos.Producto.Stock stock;
                protected DataConsultaProductosRespType.Row.Productos.Producto.PathImagen pathImagen;
                protected String tipoProducto;
                protected CodDescStringType proveedor;
                protected DataConsultaProductosRespType.Row.Productos.Producto.Precio precio;
                protected Integer escala;
                protected BigDecimal consumo;
                protected String codProductoExterno;

                /**
                 * Obtiene el valor de la propiedad idProducto.
                 * 
                 */
                public int getIdProducto() {
                    return idProducto;
                }

                /**
                 * Define el valor de la propiedad idProducto.
                 * 
                 */
                public void setIdProducto(int value) {
                    this.idProducto = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.Descripcion }
                 *     
                 */
                public DataConsultaProductosRespType.Row.Productos.Producto.Descripcion getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.Descripcion }
                 *     
                 */
                public void setDescripcion(DataConsultaProductosRespType.Row.Productos.Producto.Descripcion value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad puntos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getPuntos() {
                    return puntos;
                }

                /**
                 * Define el valor de la propiedad puntos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setPuntos(BigInteger value) {
                    this.puntos = value;
                }

                /**
                 * Obtiene el valor de la propiedad puntosAnteriores.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getPuntosAnteriores() {
                    return puntosAnteriores;
                }

                /**
                 * Define el valor de la propiedad puntosAnteriores.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setPuntosAnteriores(Integer value) {
                    this.puntosAnteriores = value;
                }

                /**
                 * Obtiene el valor de la propiedad rubro.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRubro() {
                    return rubro;
                }

                /**
                 * Define el valor de la propiedad rubro.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRubro(CodDescStringType value) {
                    this.rubro = value;
                }

                /**
                 * Obtiene el valor de la propiedad subRubro.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getSubRubro() {
                    return subRubro;
                }

                /**
                 * Define el valor de la propiedad subRubro.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setSubRubro(CodDescStringType value) {
                    this.subRubro = value;
                }

                /**
                 * Obtiene el valor de la propiedad vigencia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getVigencia() {
                    return vigencia;
                }

                /**
                 * Define el valor de la propiedad vigencia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setVigencia(String value) {
                    this.vigencia = value;
                }

                /**
                 * Obtiene el valor de la propiedad vigenciaFecha.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RangoFechaType }
                 *     
                 */
                public RangoFechaType getVigenciaFecha() {
                    return vigenciaFecha;
                }

                /**
                 * Define el valor de la propiedad vigenciaFecha.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RangoFechaType }
                 *     
                 */
                public void setVigenciaFecha(RangoFechaType value) {
                    this.vigenciaFecha = value;
                }

                /**
                 * Obtiene el valor de la propiedad canje.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getCanje() {
                    return canje;
                }

                /**
                 * Define el valor de la propiedad canje.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setCanje(CodDescStringType value) {
                    this.canje = value;
                }

                /**
                 * Obtiene el valor de la propiedad novedad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isNovedad() {
                    return novedad;
                }

                /**
                 * Define el valor de la propiedad novedad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setNovedad(Boolean value) {
                    this.novedad = value;
                }

                /**
                 * Obtiene el valor de la propiedad stock.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.Stock }
                 *     
                 */
                public DataConsultaProductosRespType.Row.Productos.Producto.Stock getStock() {
                    return stock;
                }

                /**
                 * Define el valor de la propiedad stock.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.Stock }
                 *     
                 */
                public void setStock(DataConsultaProductosRespType.Row.Productos.Producto.Stock value) {
                    this.stock = value;
                }

                /**
                 * Obtiene el valor de la propiedad pathImagen.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.PathImagen }
                 *     
                 */
                public DataConsultaProductosRespType.Row.Productos.Producto.PathImagen getPathImagen() {
                    return pathImagen;
                }

                /**
                 * Define el valor de la propiedad pathImagen.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.PathImagen }
                 *     
                 */
                public void setPathImagen(DataConsultaProductosRespType.Row.Productos.Producto.PathImagen value) {
                    this.pathImagen = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoProducto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTipoProducto() {
                    return tipoProducto;
                }

                /**
                 * Define el valor de la propiedad tipoProducto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTipoProducto(String value) {
                    this.tipoProducto = value;
                }

                /**
                 * Obtiene el valor de la propiedad proveedor.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getProveedor() {
                    return proveedor;
                }

                /**
                 * Define el valor de la propiedad proveedor.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setProveedor(CodDescStringType value) {
                    this.proveedor = value;
                }

                /**
                 * Obtiene el valor de la propiedad precio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.Precio }
                 *     
                 */
                public DataConsultaProductosRespType.Row.Productos.Producto.Precio getPrecio() {
                    return precio;
                }

                /**
                 * Define el valor de la propiedad precio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaProductosRespType.Row.Productos.Producto.Precio }
                 *     
                 */
                public void setPrecio(DataConsultaProductosRespType.Row.Productos.Producto.Precio value) {
                    this.precio = value;
                }

                /**
                 * Obtiene el valor de la propiedad escala.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getEscala() {
                    return escala;
                }

                /**
                 * Define el valor de la propiedad escala.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setEscala(Integer value) {
                    this.escala = value;
                }

                /**
                 * Obtiene el valor de la propiedad consumo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getConsumo() {
                    return consumo;
                }

                /**
                 * Define el valor de la propiedad consumo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setConsumo(BigDecimal value) {
                    this.consumo = value;
                }

                /**
                 * Obtiene el valor de la propiedad codProductoExterno.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodProductoExterno() {
                    return codProductoExterno;
                }

                /**
                 * Define el valor de la propiedad codProductoExterno.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodProductoExterno(String value) {
                    this.codProductoExterno = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "corta",
                    "larga"
                })
                public static class Descripcion {

                    @XmlElement(required = true)
                    protected String corta;
                    @XmlElement(required = true)
                    protected String larga;

                    /**
                     * Obtiene el valor de la propiedad corta.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCorta() {
                        return corta;
                    }

                    /**
                     * Define el valor de la propiedad corta.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCorta(String value) {
                        this.corta = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad larga.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLarga() {
                        return larga;
                    }

                    /**
                     * Define el valor de la propiedad larga.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLarga(String value) {
                        this.larga = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="larga" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="corta" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "larga",
                    "corta"
                })
                public static class PathImagen {

                    @XmlElement(required = true)
                    protected String larga;
                    @XmlElement(required = true)
                    protected String corta;

                    /**
                     * Obtiene el valor de la propiedad larga.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLarga() {
                        return larga;
                    }

                    /**
                     * Define el valor de la propiedad larga.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLarga(String value) {
                        this.larga = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad corta.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCorta() {
                        return corta;
                    }

                    /**
                     * Define el valor de la propiedad corta.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCorta(String value) {
                        this.corta = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="conIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
                 *         &lt;element name="sinIva" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
                 *         &lt;element name="publico" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "conIva",
                    "sinIva",
                    "publico"
                })
                public static class Precio {

                    protected BigDecimal conIva;
                    protected BigDecimal sinIva;
                    protected BigDecimal publico;

                    /**
                     * Obtiene el valor de la propiedad conIva.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getConIva() {
                        return conIva;
                    }

                    /**
                     * Define el valor de la propiedad conIva.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setConIva(BigDecimal value) {
                        this.conIva = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad sinIva.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getSinIva() {
                        return sinIva;
                    }

                    /**
                     * Define el valor de la propiedad sinIva.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setSinIva(BigDecimal value) {
                        this.sinIva = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad publico.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPublico() {
                        return publico;
                    }

                    /**
                     * Define el valor de la propiedad publico.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPublico(BigDecimal value) {
                        this.publico = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
                 *         &lt;element name="maxDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
                 *         &lt;element name="minDisponible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "disponible",
                    "maxDisponible",
                    "minDisponible"
                })
                public static class Stock {

                    protected Integer disponible;
                    protected Integer maxDisponible;
                    protected Integer minDisponible;

                    /**
                     * Obtiene el valor de la propiedad disponible.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getDisponible() {
                        return disponible;
                    }

                    /**
                     * Define el valor de la propiedad disponible.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setDisponible(Integer value) {
                        this.disponible = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad maxDisponible.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getMaxDisponible() {
                        return maxDisponible;
                    }

                    /**
                     * Define el valor de la propiedad maxDisponible.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setMaxDisponible(Integer value) {
                        this.maxDisponible = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad minDisponible.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getMinDisponible() {
                        return minDisponible;
                    }

                    /**
                     * Define el valor de la propiedad minDisponible.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setMinDisponible(Integer value) {
                        this.minDisponible = value;
                    }

                }

            }

        }

    }

}
