
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosxmarca_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaParametrosxMarcaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaParametrosxMarcaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="principal" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="codParametro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="tipoCodProc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaParametrosxMarcaReqType", propOrder = {
    "operacion",
    "principal"
})
public class DataConsultaParametrosxMarcaReqType {

    @XmlElement(required = true)
    protected String operacion;
    protected DataConsultaParametrosxMarcaReqType.Principal principal;

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad principal.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaParametrosxMarcaReqType.Principal }
     *     
     */
    public DataConsultaParametrosxMarcaReqType.Principal getPrincipal() {
        return principal;
    }

    /**
     * Define el valor de la propiedad principal.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaParametrosxMarcaReqType.Principal }
     *     
     */
    public void setPrincipal(DataConsultaParametrosxMarcaReqType.Principal value) {
        this.principal = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="codParametro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="tipoCodProc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codMarca",
        "codParametro",
        "numEntidad",
        "tipoCodProc"
    })
    public static class Principal {

        protected String codMarca;
        protected String codParametro;
        protected String numEntidad;
        protected String tipoCodProc;

        /**
         * Obtiene el valor de la propiedad codMarca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodMarca() {
            return codMarca;
        }

        /**
         * Define el valor de la propiedad codMarca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodMarca(String value) {
            this.codMarca = value;
        }

        /**
         * Obtiene el valor de la propiedad codParametro.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodParametro() {
            return codParametro;
        }

        /**
         * Define el valor de la propiedad codParametro.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodParametro(String value) {
            this.codParametro = value;
        }

        /**
         * Obtiene el valor de la propiedad numEntidad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumEntidad() {
            return numEntidad;
        }

        /**
         * Define el valor de la propiedad numEntidad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumEntidad(String value) {
            this.numEntidad = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoCodProc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoCodProc() {
            return tipoCodProc;
        }

        /**
         * Define el valor de la propiedad tipoCodProc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoCodProc(String value) {
            this.tipoCodProc = value;
        }

    }

}
