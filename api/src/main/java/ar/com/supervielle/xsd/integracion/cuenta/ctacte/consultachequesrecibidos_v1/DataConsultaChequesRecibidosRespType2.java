
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesrecibidos_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaChequesRecibidosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaChequesRecibidosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="nroCheque" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="fechaDeposito" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="sucursalLibradora" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaChequesRecibidosRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1", propOrder = {
    "row"
})
public class DataConsultaChequesRecibidosRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaChequesRecibidosRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaChequesRecibidosRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaChequesRecibidosRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaChequesRecibidosRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="nroCheque" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="fechaDeposito" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="sucursalLibradora" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "nroCheque",
        "banco",
        "importe",
        "fechaDeposito",
        "estado",
        "sucursalLibradora"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaBANTOTALType identificador;
        @XmlElement(required = true)
        protected String nroCheque;
        @XmlElement(required = true)
        protected String banco;
        protected BigDecimal importe;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaDeposito;
        @XmlElement(required = true)
        protected CodDescStringType estado;
        @XmlElement(required = true)
        protected String sucursalLibradora;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad nroCheque.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNroCheque() {
            return nroCheque;
        }

        /**
         * Define el valor de la propiedad nroCheque.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNroCheque(String value) {
            this.nroCheque = value;
        }

        /**
         * Obtiene el valor de la propiedad banco.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBanco() {
            return banco;
        }

        /**
         * Define el valor de la propiedad banco.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBanco(String value) {
            this.banco = value;
        }

        /**
         * Obtiene el valor de la propiedad importe.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getImporte() {
            return importe;
        }

        /**
         * Define el valor de la propiedad importe.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setImporte(BigDecimal value) {
            this.importe = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaDeposito.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaDeposito() {
            return fechaDeposito;
        }

        /**
         * Define el valor de la propiedad fechaDeposito.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaDeposito(XMLGregorianCalendar value) {
            this.fechaDeposito = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursalLibradora.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSucursalLibradora() {
            return sucursalLibradora;
        }

        /**
         * Define el valor de la propiedad sucursalLibradora.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSucursalLibradora(String value) {
            this.sucursalLibradora = value;
        }

    }

}
