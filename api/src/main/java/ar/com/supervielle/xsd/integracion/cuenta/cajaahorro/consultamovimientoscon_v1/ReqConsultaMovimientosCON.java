
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientoscon_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.OrderType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.PagingType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}Paging" minOccurs="0"/>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}Order" minOccurs="0"/>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaMovimientosCON-v1}DataConsultaMovReqType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paging",
    "order",
    "data"
})
@XmlRootElement(name = "ReqConsultaMovimientosCON")
public class ReqConsultaMovimientosCON {

    @XmlElement(name = "Paging", namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1")
    protected PagingType paging;
    @XmlElement(name = "Order", namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1")
    protected OrderType order;
    @XmlElement(name = "Data", required = true)
    protected DataConsultaMovReqType data;

    /**
     * Obtiene el valor de la propiedad paging.
     * 
     * @return
     *     possible object is
     *     {@link PagingType }
     *     
     */
    public PagingType getPaging() {
        return paging;
    }

    /**
     * Define el valor de la propiedad paging.
     * 
     * @param value
     *     allowed object is
     *     {@link PagingType }
     *     
     */
    public void setPaging(PagingType value) {
        this.paging = value;
    }

    /**
     * Obtiene el valor de la propiedad order.
     * 
     * @return
     *     possible object is
     *     {@link OrderType }
     *     
     */
    public OrderType getOrder() {
        return order;
    }

    /**
     * Define el valor de la propiedad order.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderType }
     *     
     */
    public void setOrder(OrderType value) {
        this.order = value;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaMovReqType }
     *     
     */
    public DataConsultaMovReqType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaMovReqType }
     *     
     */
    public void setData(DataConsultaMovReqType value) {
        this.data = value;
    }

}
