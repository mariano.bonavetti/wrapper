
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaSaldosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaSaldosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="sobregiro" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldoBloqueado" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldoContable" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldoDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="acuerdo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaSaldosRespType", propOrder = {
    "row"
})
public class DataConsultaSaldosRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaSaldosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaSaldosRespType.Row }
     * 
     * 
     */
    public List<DataConsultaSaldosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaSaldosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="sobregiro" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldoBloqueado" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldoContable" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldoDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="acuerdo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "sobregiro",
        "saldoBloqueado",
        "saldoContable",
        "saldoDisponible",
        "acuerdo"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaBANTOTALType identificador;
        protected BigDecimal sobregiro;
        protected BigDecimal saldoBloqueado;
        protected BigDecimal saldoContable;
        protected BigDecimal saldoDisponible;
        protected BigDecimal acuerdo;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad sobregiro.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSobregiro() {
            return sobregiro;
        }

        /**
         * Define el valor de la propiedad sobregiro.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSobregiro(BigDecimal value) {
            this.sobregiro = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoBloqueado.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoBloqueado() {
            return saldoBloqueado;
        }

        /**
         * Define el valor de la propiedad saldoBloqueado.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoBloqueado(BigDecimal value) {
            this.saldoBloqueado = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoContable.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoContable() {
            return saldoContable;
        }

        /**
         * Define el valor de la propiedad saldoContable.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoContable(BigDecimal value) {
            this.saldoContable = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoDisponible.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoDisponible() {
            return saldoDisponible;
        }

        /**
         * Define el valor de la propiedad saldoDisponible.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoDisponible(BigDecimal value) {
            this.saldoDisponible = value;
        }

        /**
         * Obtiene el valor de la propiedad acuerdo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAcuerdo() {
            return acuerdo;
        }

        /**
         * Define el valor de la propiedad acuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAcuerdo(BigDecimal value) {
            this.acuerdo = value;
        }

    }

}
