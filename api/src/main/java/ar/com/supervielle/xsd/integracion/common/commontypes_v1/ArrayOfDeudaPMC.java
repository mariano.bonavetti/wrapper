
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para ArrayOfDeudaPMC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDeudaPMC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agregadaManualmente" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="coordenadas" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}CoordenadasPMC" minOccurs="0"/>
 *         &lt;element name="cuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}CuentaPMC" minOccurs="0"/>
 *         &lt;element name="descPantalla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcionUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="empresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}EmpresaBasePMC" minOccurs="0"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaStopDebit" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="flagControlPagoDirecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAdelanto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="modoAlta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}MonedaPMC" minOccurs="0"/>
 *         &lt;element name="nroFactura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otroImporte" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pagoRecurrente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requiereCoordenadas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="topePagoAutomatico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDeudaPMC", propOrder = {
    "agregadaManualmente",
    "coordenadas",
    "cuenta",
    "descPantalla",
    "descripcionUsuario",
    "empresa",
    "error",
    "fechaStopDebit",
    "flagControlPagoDirecto",
    "idAdelanto",
    "idCliente",
    "importe",
    "modoAlta",
    "moneda",
    "nroFactura",
    "otroImporte",
    "pagoRecurrente",
    "requiereCoordenadas",
    "topePagoAutomatico",
    "vencimiento"
})
public class ArrayOfDeudaPMC {

    protected Boolean agregadaManualmente;
    protected CoordenadasPMC coordenadas;
    protected CuentaPMC cuenta;
    protected String descPantalla;
    protected String descripcionUsuario;
    protected EmpresaBasePMC empresa;
    protected String error;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaStopDebit;
    protected String flagControlPagoDirecto;
    protected String idAdelanto;
    protected String idCliente;
    protected Double importe;
    protected String modoAlta;
    protected MonedaPMC moneda;
    protected String nroFactura;
    protected Double otroImporte;
    protected String pagoRecurrente;
    protected String requiereCoordenadas;
    protected String topePagoAutomatico;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vencimiento;

    /**
     * Obtiene el valor de la propiedad agregadaManualmente.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAgregadaManualmente() {
        return agregadaManualmente;
    }

    /**
     * Define el valor de la propiedad agregadaManualmente.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAgregadaManualmente(Boolean value) {
        this.agregadaManualmente = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenadas.
     * 
     * @return
     *     possible object is
     *     {@link CoordenadasPMC }
     *     
     */
    public CoordenadasPMC getCoordenadas() {
        return coordenadas;
    }

    /**
     * Define el valor de la propiedad coordenadas.
     * 
     * @param value
     *     allowed object is
     *     {@link CoordenadasPMC }
     *     
     */
    public void setCoordenadas(CoordenadasPMC value) {
        this.coordenadas = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link CuentaPMC }
     *     
     */
    public CuentaPMC getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link CuentaPMC }
     *     
     */
    public void setCuenta(CuentaPMC value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad descPantalla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescPantalla() {
        return descPantalla;
    }

    /**
     * Define el valor de la propiedad descPantalla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescPantalla(String value) {
        this.descPantalla = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionUsuario() {
        return descripcionUsuario;
    }

    /**
     * Define el valor de la propiedad descripcionUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionUsuario(String value) {
        this.descripcionUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link EmpresaBasePMC }
     *     
     */
    public EmpresaBasePMC getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpresaBasePMC }
     *     
     */
    public void setEmpresa(EmpresaBasePMC value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad error.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getError() {
        return error;
    }

    /**
     * Define el valor de la propiedad error.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setError(String value) {
        this.error = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaStopDebit.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaStopDebit() {
        return fechaStopDebit;
    }

    /**
     * Define el valor de la propiedad fechaStopDebit.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaStopDebit(XMLGregorianCalendar value) {
        this.fechaStopDebit = value;
    }

    /**
     * Obtiene el valor de la propiedad flagControlPagoDirecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagControlPagoDirecto() {
        return flagControlPagoDirecto;
    }

    /**
     * Define el valor de la propiedad flagControlPagoDirecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagControlPagoDirecto(String value) {
        this.flagControlPagoDirecto = value;
    }

    /**
     * Obtiene el valor de la propiedad idAdelanto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAdelanto() {
        return idAdelanto;
    }

    /**
     * Define el valor de la propiedad idAdelanto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAdelanto(String value) {
        this.idAdelanto = value;
    }

    /**
     * Obtiene el valor de la propiedad idCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * Define el valor de la propiedad idCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCliente(String value) {
        this.idCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad importe.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getImporte() {
        return importe;
    }

    /**
     * Define el valor de la propiedad importe.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setImporte(Double value) {
        this.importe = value;
    }

    /**
     * Obtiene el valor de la propiedad modoAlta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModoAlta() {
        return modoAlta;
    }

    /**
     * Define el valor de la propiedad modoAlta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModoAlta(String value) {
        this.modoAlta = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link MonedaPMC }
     *     
     */
    public MonedaPMC getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link MonedaPMC }
     *     
     */
    public void setMoneda(MonedaPMC value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad nroFactura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * Define el valor de la propiedad nroFactura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroFactura(String value) {
        this.nroFactura = value;
    }

    /**
     * Obtiene el valor de la propiedad otroImporte.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOtroImporte() {
        return otroImporte;
    }

    /**
     * Define el valor de la propiedad otroImporte.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOtroImporte(Double value) {
        this.otroImporte = value;
    }

    /**
     * Obtiene el valor de la propiedad pagoRecurrente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPagoRecurrente() {
        return pagoRecurrente;
    }

    /**
     * Define el valor de la propiedad pagoRecurrente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPagoRecurrente(String value) {
        this.pagoRecurrente = value;
    }

    /**
     * Obtiene el valor de la propiedad requiereCoordenadas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequiereCoordenadas() {
        return requiereCoordenadas;
    }

    /**
     * Define el valor de la propiedad requiereCoordenadas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequiereCoordenadas(String value) {
        this.requiereCoordenadas = value;
    }

    /**
     * Obtiene el valor de la propiedad topePagoAutomatico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTopePagoAutomatico() {
        return topePagoAutomatico;
    }

    /**
     * Define el valor de la propiedad topePagoAutomatico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTopePagoAutomatico(String value) {
        this.topePagoAutomatico = value;
    }

    /**
     * Obtiene el valor de la propiedad vencimiento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVencimiento() {
        return vencimiento;
    }

    /**
     * Define el valor de la propiedad vencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVencimiento(XMLGregorianCalendar value) {
        this.vencimiento = value;
    }

}
