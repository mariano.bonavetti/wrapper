
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaListadoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="cuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="cbu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="moneda">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="tipoProducto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
 *                   &lt;element name="acuerdo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="fechaUltimoMov" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="AtributosExtendidos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoRespType", propOrder = {
    "row"
})
public class DataConsultaListadoRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaListadoRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaListadoRespType.Row }
     * 
     * 
     */
    public List<DataConsultaListadoRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaListadoRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="cuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="cbu" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="moneda">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tipoProducto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
     *         &lt;element name="acuerdo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="interes" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="fechaUltimoMov" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="AtributosExtendidos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "sucursal",
        "cuenta",
        "cbu",
        "vinculo",
        "moneda",
        "tipoProducto",
        "paquete",
        "acuerdo",
        "saldo",
        "interes",
        "fechaUltimoMov",
        "fechaAlta",
        "fechaBaja",
        "estado",
        "atributosExtendidos"
    })
    public static class Row {

        @XmlElement(name = "Identificador", required = true)
        protected IdCuentaBANTOTALType identificador;
        @XmlElement(required = true)
        protected CodDescNumType sucursal;
        @XmlElement(required = true)
        protected CodDescStringType cuenta;
        @XmlElement(required = true)
        protected String cbu;
        @XmlElement(required = true)
        protected CodDescNumType vinculo;
        @XmlElement(required = true)
        protected DataConsultaListadoRespType.Row.Moneda moneda;
        @XmlElement(required = true)
        protected CodDescNumType tipoProducto;
        protected CodDescNumType paquete;
        protected BigDecimal acuerdo;
        @XmlElement(required = true)
        protected BigDecimal saldo;
        @XmlElement(required = true)
        protected BigDecimal interes;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaUltimoMov;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAlta;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaBaja;
        @XmlElement(required = true)
        protected CodDescNumType estado;
        @XmlElement(name = "AtributosExtendidos", required = true)
        protected DataConsultaListadoRespType.Row.AtributosExtendidos atributosExtendidos;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setSucursal(CodDescNumType value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad cuenta.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getCuenta() {
            return cuenta;
        }

        /**
         * Define el valor de la propiedad cuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setCuenta(CodDescStringType value) {
            this.cuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad cbu.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCbu() {
            return cbu;
        }

        /**
         * Define el valor de la propiedad cbu.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCbu(String value) {
            this.cbu = value;
        }

        /**
         * Obtiene el valor de la propiedad vinculo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getVinculo() {
            return vinculo;
        }

        /**
         * Define el valor de la propiedad vinculo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setVinculo(CodDescNumType value) {
            this.vinculo = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoRespType.Row.Moneda }
         *     
         */
        public DataConsultaListadoRespType.Row.Moneda getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoRespType.Row.Moneda }
         *     
         */
        public void setMoneda(DataConsultaListadoRespType.Row.Moneda value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoProducto.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getTipoProducto() {
            return tipoProducto;
        }

        /**
         * Define el valor de la propiedad tipoProducto.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setTipoProducto(CodDescNumType value) {
            this.tipoProducto = value;
        }

        /**
         * Obtiene el valor de la propiedad paquete.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getPaquete() {
            return paquete;
        }

        /**
         * Define el valor de la propiedad paquete.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setPaquete(CodDescNumType value) {
            this.paquete = value;
        }

        /**
         * Obtiene el valor de la propiedad acuerdo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAcuerdo() {
            return acuerdo;
        }

        /**
         * Define el valor de la propiedad acuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAcuerdo(BigDecimal value) {
            this.acuerdo = value;
        }

        /**
         * Obtiene el valor de la propiedad saldo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldo() {
            return saldo;
        }

        /**
         * Define el valor de la propiedad saldo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldo(BigDecimal value) {
            this.saldo = value;
        }

        /**
         * Obtiene el valor de la propiedad interes.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getInteres() {
            return interes;
        }

        /**
         * Define el valor de la propiedad interes.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setInteres(BigDecimal value) {
            this.interes = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaUltimoMov.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaUltimoMov() {
            return fechaUltimoMov;
        }

        /**
         * Define el valor de la propiedad fechaUltimoMov.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaUltimoMov(XMLGregorianCalendar value) {
            this.fechaUltimoMov = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAlta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAlta() {
            return fechaAlta;
        }

        /**
         * Define el valor de la propiedad fechaAlta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAlta(XMLGregorianCalendar value) {
            this.fechaAlta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaBaja.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaBaja() {
            return fechaBaja;
        }

        /**
         * Define el valor de la propiedad fechaBaja.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaBaja(XMLGregorianCalendar value) {
            this.fechaBaja = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setEstado(CodDescNumType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad atributosExtendidos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoRespType.Row.AtributosExtendidos }
         *     
         */
        public DataConsultaListadoRespType.Row.AtributosExtendidos getAtributosExtendidos() {
            return atributosExtendidos;
        }

        /**
         * Define el valor de la propiedad atributosExtendidos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoRespType.Row.AtributosExtendidos }
         *     
         */
        public void setAtributosExtendidos(DataConsultaListadoRespType.Row.AtributosExtendidos value) {
            this.atributosExtendidos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "atributo"
        })
        public static class AtributosExtendidos {

            protected List<DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo> atributo;

            /**
             * Gets the value of the atributo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the atributo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAtributo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo }
             * 
             * 
             */
            public List<DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo> getAtributo() {
                if (atributo == null) {
                    atributo = new ArrayList<DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo>();
                }
                return this.atributo;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Atributo {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "id")
                protected String id;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad id.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Define el valor de la propiedad id.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setId(String value) {
                    this.id = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigo",
            "simbolo",
            "descripcion"
        })
        public static class Moneda {

            protected int codigo;
            @XmlElement(required = true)
            protected String simbolo;
            @XmlElement(required = true)
            protected String descripcion;

            /**
             * Obtiene el valor de la propiedad codigo.
             * 
             */
            public int getCodigo() {
                return codigo;
            }

            /**
             * Define el valor de la propiedad codigo.
             * 
             */
            public void setCodigo(int value) {
                this.codigo = value;
            }

            /**
             * Obtiene el valor de la propiedad simbolo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSimbolo() {
                return simbolo;
            }

            /**
             * Define el valor de la propiedad simbolo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSimbolo(String value) {
                this.simbolo = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Define el valor de la propiedad descripcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

        }

    }

}
