
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultatarjetasxcuenta_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para idCtaTarjetaType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="idCtaTarjetaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCuentaMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="dataTypeName" type="{http://www.w3.org/2001/XMLSchema}string" default="IDCuentaTarjeta" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "idCtaTarjetaType", propOrder = {
    "idCuentaMarca"
})
public class IdCtaTarjetaType {

    @XmlElement(required = true)
    protected String idCuentaMarca;
    @XmlAttribute(name = "dataTypeName")
    protected String dataTypeName;

    /**
     * Obtiene el valor de la propiedad idCuentaMarca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCuentaMarca() {
        return idCuentaMarca;
    }

    /**
     * Define el valor de la propiedad idCuentaMarca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCuentaMarca(String value) {
        this.idCuentaMarca = value;
    }

    /**
     * Obtiene el valor de la propiedad dataTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTypeName() {
        if (dataTypeName == null) {
            return "IDCuentaTarjeta";
        } else {
            return dataTypeName;
        }
    }

    /**
     * Define el valor de la propiedad dataTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTypeName(String value) {
        this.dataTypeName = value;
    }

}
