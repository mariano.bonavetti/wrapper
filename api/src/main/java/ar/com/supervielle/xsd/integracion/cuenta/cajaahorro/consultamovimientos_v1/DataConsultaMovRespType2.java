
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaMovRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaMovRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Saldo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="bloqueado" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="noDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="sobregiroFormal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="Subtotal">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="debito" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="credito" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Movimientos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Movimiento" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                                       &lt;element name="tipoMovimiento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                       &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="codigoConcepto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="debitoOCredito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="numeroCheque" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                                       &lt;element name="Renglon1" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon2" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon3" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon4" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon5" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon6" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon7" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon8" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                       &lt;element name="Renglon9" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AtributosExtendidos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaMovRespType", propOrder = {
    "row"
})
public class DataConsultaMovRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaMovRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaMovRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaMovRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaMovRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Saldo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="bloqueado" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="noDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="sobregiroFormal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="Subtotal">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="debito" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="credito" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Movimientos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Movimiento" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *                             &lt;element name="tipoMovimiento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                             &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="codigoConcepto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="debitoOCredito" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="numeroCheque" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                             &lt;element name="Renglon1" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon2" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon3" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon4" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon5" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon6" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon7" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon8" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                             &lt;element name="Renglon9" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AtributosExtendidos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saldo",
        "sobregiroFormal",
        "subtotal",
        "movimientos",
        "atributosExtendidos"
    })
    public static class Row {

        @XmlElement(name = "Saldo", required = true)
        protected DataConsultaMovRespType2 .Row.Saldo saldo;
        @XmlElement(required = true)
        protected BigDecimal sobregiroFormal;
        @XmlElement(name = "Subtotal", required = true)
        protected DataConsultaMovRespType2 .Row.Subtotal subtotal;
        @XmlElement(name = "Movimientos", required = true)
        protected DataConsultaMovRespType2 .Row.Movimientos movimientos;
        @XmlElement(name = "AtributosExtendidos", required = true)
        protected DataConsultaMovRespType2 .Row.AtributosExtendidos atributosExtendidos;

        /**
         * Obtiene el valor de la propiedad saldo.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaMovRespType2 .Row.Saldo }
         *     
         */
        public DataConsultaMovRespType2 .Row.Saldo getSaldo() {
            return saldo;
        }

        /**
         * Define el valor de la propiedad saldo.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaMovRespType2 .Row.Saldo }
         *     
         */
        public void setSaldo(DataConsultaMovRespType2 .Row.Saldo value) {
            this.saldo = value;
        }

        /**
         * Obtiene el valor de la propiedad sobregiroFormal.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSobregiroFormal() {
            return sobregiroFormal;
        }

        /**
         * Define el valor de la propiedad sobregiroFormal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSobregiroFormal(BigDecimal value) {
            this.sobregiroFormal = value;
        }

        /**
         * Obtiene el valor de la propiedad subtotal.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaMovRespType2 .Row.Subtotal }
         *     
         */
        public DataConsultaMovRespType2 .Row.Subtotal getSubtotal() {
            return subtotal;
        }

        /**
         * Define el valor de la propiedad subtotal.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaMovRespType2 .Row.Subtotal }
         *     
         */
        public void setSubtotal(DataConsultaMovRespType2 .Row.Subtotal value) {
            this.subtotal = value;
        }

        /**
         * Obtiene el valor de la propiedad movimientos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaMovRespType2 .Row.Movimientos }
         *     
         */
        public DataConsultaMovRespType2 .Row.Movimientos getMovimientos() {
            return movimientos;
        }

        /**
         * Define el valor de la propiedad movimientos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaMovRespType2 .Row.Movimientos }
         *     
         */
        public void setMovimientos(DataConsultaMovRespType2 .Row.Movimientos value) {
            this.movimientos = value;
        }

        /**
         * Obtiene el valor de la propiedad atributosExtendidos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaMovRespType2 .Row.AtributosExtendidos }
         *     
         */
        public DataConsultaMovRespType2 .Row.AtributosExtendidos getAtributosExtendidos() {
            return atributosExtendidos;
        }

        /**
         * Define el valor de la propiedad atributosExtendidos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaMovRespType2 .Row.AtributosExtendidos }
         *     
         */
        public void setAtributosExtendidos(DataConsultaMovRespType2 .Row.AtributosExtendidos value) {
            this.atributosExtendidos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "atributo"
        })
        public static class AtributosExtendidos {

            protected List<DataConsultaMovRespType2 .Row.AtributosExtendidos.Atributo> atributo;

            /**
             * Gets the value of the atributo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the atributo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAtributo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaMovRespType2 .Row.AtributosExtendidos.Atributo }
             * 
             * 
             */
            public List<DataConsultaMovRespType2 .Row.AtributosExtendidos.Atributo> getAtributo() {
                if (atributo == null) {
                    atributo = new ArrayList<DataConsultaMovRespType2 .Row.AtributosExtendidos.Atributo>();
                }
                return this.atributo;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Atributo {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "id")
                protected String id;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad id.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Define el valor de la propiedad id.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setId(String value) {
                    this.id = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Movimiento" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
         *                   &lt;element name="tipoMovimiento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                   &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="codigoConcepto" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="debitoOCredito" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="numeroCheque" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *                   &lt;element name="Renglon1" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon2" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon3" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon4" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon5" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon6" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon7" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon8" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                   &lt;element name="Renglon9" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "movimiento"
        })
        public static class Movimientos {

            @XmlElement(name = "Movimiento")
            protected List<DataConsultaMovRespType2 .Row.Movimientos.Movimiento> movimiento;

            /**
             * Gets the value of the movimiento property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the movimiento property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getMovimiento().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaMovRespType2 .Row.Movimientos.Movimiento }
             * 
             * 
             */
            public List<DataConsultaMovRespType2 .Row.Movimientos.Movimiento> getMovimiento() {
                if (movimiento == null) {
                    movimiento = new ArrayList<DataConsultaMovRespType2 .Row.Movimientos.Movimiento>();
                }
                return this.movimiento;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
             *         &lt;element name="tipoMovimiento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *         &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="codigoConcepto" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="debitoOCredito" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="numeroCheque" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
             *         &lt;element name="Renglon1" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon2" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon3" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon4" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon5" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon6" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon7" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon8" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *         &lt;element name="Renglon9" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "fecha",
                "tipoMovimiento",
                "sucursal",
                "codigoConcepto",
                "debitoOCredito",
                "importe",
                "saldo",
                "numeroCheque",
                "renglon1",
                "renglon2",
                "renglon3",
                "renglon4",
                "renglon5",
                "renglon6",
                "renglon7",
                "renglon8",
                "renglon9"
            })
            public static class Movimiento {

                @XmlElement(required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar fecha;
                @XmlElement(required = true)
                protected CodDescStringType tipoMovimiento;
                @XmlElement(name = "Sucursal", required = true)
                protected CodDescNumType sucursal;
                @XmlElement(required = true)
                protected String codigoConcepto;
                @XmlElement(required = true)
                protected String debitoOCredito;
                @XmlElement(required = true)
                protected BigDecimal importe;
                protected BigDecimal saldo;
                protected Integer numeroCheque;
                @XmlElement(name = "Renglon1")
                protected CodDescStringType renglon1;
                @XmlElement(name = "Renglon2")
                protected CodDescStringType renglon2;
                @XmlElement(name = "Renglon3")
                protected CodDescStringType renglon3;
                @XmlElement(name = "Renglon4")
                protected CodDescStringType renglon4;
                @XmlElement(name = "Renglon5")
                protected CodDescStringType renglon5;
                @XmlElement(name = "Renglon6")
                protected CodDescStringType renglon6;
                @XmlElement(name = "Renglon7")
                protected CodDescStringType renglon7;
                @XmlElement(name = "Renglon8")
                protected CodDescStringType renglon8;
                @XmlElement(name = "Renglon9")
                protected CodDescStringType renglon9;

                /**
                 * Obtiene el valor de la propiedad fecha.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFecha() {
                    return fecha;
                }

                /**
                 * Define el valor de la propiedad fecha.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFecha(XMLGregorianCalendar value) {
                    this.fecha = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoMovimiento.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getTipoMovimiento() {
                    return tipoMovimiento;
                }

                /**
                 * Define el valor de la propiedad tipoMovimiento.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setTipoMovimiento(CodDescStringType value) {
                    this.tipoMovimiento = value;
                }

                /**
                 * Obtiene el valor de la propiedad sucursal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getSucursal() {
                    return sucursal;
                }

                /**
                 * Define el valor de la propiedad sucursal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setSucursal(CodDescNumType value) {
                    this.sucursal = value;
                }

                /**
                 * Obtiene el valor de la propiedad codigoConcepto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigoConcepto() {
                    return codigoConcepto;
                }

                /**
                 * Define el valor de la propiedad codigoConcepto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigoConcepto(String value) {
                    this.codigoConcepto = value;
                }

                /**
                 * Obtiene el valor de la propiedad debitoOCredito.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDebitoOCredito() {
                    return debitoOCredito;
                }

                /**
                 * Define el valor de la propiedad debitoOCredito.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDebitoOCredito(String value) {
                    this.debitoOCredito = value;
                }

                /**
                 * Obtiene el valor de la propiedad importe.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getImporte() {
                    return importe;
                }

                /**
                 * Define el valor de la propiedad importe.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setImporte(BigDecimal value) {
                    this.importe = value;
                }

                /**
                 * Obtiene el valor de la propiedad saldo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getSaldo() {
                    return saldo;
                }

                /**
                 * Define el valor de la propiedad saldo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setSaldo(BigDecimal value) {
                    this.saldo = value;
                }

                /**
                 * Obtiene el valor de la propiedad numeroCheque.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNumeroCheque() {
                    return numeroCheque;
                }

                /**
                 * Define el valor de la propiedad numeroCheque.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNumeroCheque(Integer value) {
                    this.numeroCheque = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon1.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon1() {
                    return renglon1;
                }

                /**
                 * Define el valor de la propiedad renglon1.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon1(CodDescStringType value) {
                    this.renglon1 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon2.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon2() {
                    return renglon2;
                }

                /**
                 * Define el valor de la propiedad renglon2.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon2(CodDescStringType value) {
                    this.renglon2 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon3.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon3() {
                    return renglon3;
                }

                /**
                 * Define el valor de la propiedad renglon3.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon3(CodDescStringType value) {
                    this.renglon3 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon4.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon4() {
                    return renglon4;
                }

                /**
                 * Define el valor de la propiedad renglon4.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon4(CodDescStringType value) {
                    this.renglon4 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon5.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon5() {
                    return renglon5;
                }

                /**
                 * Define el valor de la propiedad renglon5.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon5(CodDescStringType value) {
                    this.renglon5 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon6.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon6() {
                    return renglon6;
                }

                /**
                 * Define el valor de la propiedad renglon6.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon6(CodDescStringType value) {
                    this.renglon6 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon7.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon7() {
                    return renglon7;
                }

                /**
                 * Define el valor de la propiedad renglon7.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon7(CodDescStringType value) {
                    this.renglon7 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon8.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon8() {
                    return renglon8;
                }

                /**
                 * Define el valor de la propiedad renglon8.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon8(CodDescStringType value) {
                    this.renglon8 = value;
                }

                /**
                 * Obtiene el valor de la propiedad renglon9.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getRenglon9() {
                    return renglon9;
                }

                /**
                 * Define el valor de la propiedad renglon9.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setRenglon9(CodDescStringType value) {
                    this.renglon9 = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="bloqueado" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="noDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "anterior",
            "actual",
            "bloqueado",
            "noDisponible"
        })
        public static class Saldo {

            @XmlElement(required = true)
            protected BigDecimal anterior;
            @XmlElement(required = true)
            protected BigDecimal actual;
            @XmlElement(required = true)
            protected BigDecimal bloqueado;
            @XmlElement(required = true)
            protected BigDecimal noDisponible;

            /**
             * Obtiene el valor de la propiedad anterior.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAnterior() {
                return anterior;
            }

            /**
             * Define el valor de la propiedad anterior.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAnterior(BigDecimal value) {
                this.anterior = value;
            }

            /**
             * Obtiene el valor de la propiedad actual.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getActual() {
                return actual;
            }

            /**
             * Define el valor de la propiedad actual.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setActual(BigDecimal value) {
                this.actual = value;
            }

            /**
             * Obtiene el valor de la propiedad bloqueado.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getBloqueado() {
                return bloqueado;
            }

            /**
             * Define el valor de la propiedad bloqueado.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setBloqueado(BigDecimal value) {
                this.bloqueado = value;
            }

            /**
             * Obtiene el valor de la propiedad noDisponible.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getNoDisponible() {
                return noDisponible;
            }

            /**
             * Define el valor de la propiedad noDisponible.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setNoDisponible(BigDecimal value) {
                this.noDisponible = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="debito" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="credito" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "debito",
            "credito"
        })
        public static class Subtotal {

            @XmlElement(required = true)
            protected BigDecimal debito;
            @XmlElement(required = true)
            protected BigDecimal credito;

            /**
             * Obtiene el valor de la propiedad debito.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getDebito() {
                return debito;
            }

            /**
             * Define el valor de la propiedad debito.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setDebito(BigDecimal value) {
                this.debito = value;
            }

            /**
             * Obtiene el valor de la propiedad credito.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCredito() {
                return credito;
            }

            /**
             * Define el valor de la propiedad credito.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCredito(BigDecimal value) {
                this.credito = value;
            }

        }

    }

}
