
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametros_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametros_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametros_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaParametrosResp }
     * 
     */
    public DataConsultaParametrosResp createDataConsultaParametrosResp() {
        return new DataConsultaParametrosResp();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosResp.Row }
     * 
     */
    public DataConsultaParametrosResp.Row createDataConsultaParametrosRespRow() {
        return new DataConsultaParametrosResp.Row();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosResp.Row.Operacion }
     * 
     */
    public DataConsultaParametrosResp.Row.Operacion createDataConsultaParametrosRespRowOperacion() {
        return new DataConsultaParametrosResp.Row.Operacion();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosResp.Row.Operacion.Errores }
     * 
     */
    public DataConsultaParametrosResp.Row.Operacion.Errores createDataConsultaParametrosRespRowOperacionErrores() {
        return new DataConsultaParametrosResp.Row.Operacion.Errores();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosReq }
     * 
     */
    public DataConsultaParametrosReq createDataConsultaParametrosReq() {
        return new DataConsultaParametrosReq();
    }

    /**
     * Create an instance of {@link ReqConsultaParametros }
     * 
     */
    public ReqConsultaParametros createReqConsultaParametros() {
        return new ReqConsultaParametros();
    }

    /**
     * Create an instance of {@link RespConsultaParametros }
     * 
     */
    public RespConsultaParametros createRespConsultaParametros() {
        return new RespConsultaParametros();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosResp.Row.Operacion.Detail }
     * 
     */
    public DataConsultaParametrosResp.Row.Operacion.Detail createDataConsultaParametrosRespRowOperacionDetail() {
        return new DataConsultaParametrosResp.Row.Operacion.Detail();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosResp.Row.Operacion.Errores.Error }
     * 
     */
    public DataConsultaParametrosResp.Row.Operacion.Errores.Error createDataConsultaParametrosRespRowOperacionErroresError() {
        return new DataConsultaParametrosResp.Row.Operacion.Errores.Error();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosReq.Principal }
     * 
     */
    public DataConsultaParametrosReq.Principal createDataConsultaParametrosReqPrincipal() {
        return new DataConsultaParametrosReq.Principal();
    }

}
