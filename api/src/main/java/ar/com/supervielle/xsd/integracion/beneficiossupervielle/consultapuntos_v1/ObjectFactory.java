
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataConsultaPuntosRespType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1", "DataConsultaPuntosRespType");
    private final static QName _DataConsultaPuntosReqType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1", "DataConsultaPuntosReqType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaPuntosRespType2 }
     * 
     */
    public DataConsultaPuntosRespType2 createDataConsultaPuntosRespType2() {
        return new DataConsultaPuntosRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosRespType }
     * 
     */
    public DataConsultaPuntosRespType createDataConsultaPuntosRespType() {
        return new DataConsultaPuntosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosRespType.Row }
     * 
     */
    public DataConsultaPuntosRespType.Row createDataConsultaPuntosRespTypeRow() {
        return new DataConsultaPuntosRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosRespType.Row.ListaPuntosAVencer }
     * 
     */
    public DataConsultaPuntosRespType.Row.ListaPuntosAVencer createDataConsultaPuntosRespTypeRowListaPuntosAVencer() {
        return new DataConsultaPuntosRespType.Row.ListaPuntosAVencer();
    }

    /**
     * Create an instance of {@link RespConsultaPuntos }
     * 
     */
    public RespConsultaPuntos createRespConsultaPuntos() {
        return new RespConsultaPuntos();
    }

    /**
     * Create an instance of {@link ReqConsultaPuntos }
     * 
     */
    public ReqConsultaPuntos createReqConsultaPuntos() {
        return new ReqConsultaPuntos();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosReqType }
     * 
     */
    public DataConsultaPuntosReqType createDataConsultaPuntosReqType() {
        return new DataConsultaPuntosReqType();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosReqType2 }
     * 
     */
    public DataConsultaPuntosReqType2 createDataConsultaPuntosReqType2() {
        return new DataConsultaPuntosReqType2();
    }

    /**
     * Create an instance of {@link RespConsultaPuntos2 }
     * 
     */
    public RespConsultaPuntos2 createRespConsultaPuntos2() {
        return new RespConsultaPuntos2();
    }

    /**
     * Create an instance of {@link ReqConsultaPuntos2 }
     * 
     */
    public ReqConsultaPuntos2 createReqConsultaPuntos2() {
        return new ReqConsultaPuntos2();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosRespType2 .Row }
     * 
     */
    public DataConsultaPuntosRespType2 .Row createDataConsultaPuntosRespType2Row() {
        return new DataConsultaPuntosRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento }
     * 
     */
    public DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento createDataConsultaPuntosRespTypeRowListaPuntosAVencerVencimiento() {
        return new DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsultaPuntosRespType2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1", name = "DataConsultaPuntosRespType")
    public JAXBElement<DataConsultaPuntosRespType2> createDataConsultaPuntosRespType(DataConsultaPuntosRespType2 value) {
        return new JAXBElement<DataConsultaPuntosRespType2>(_DataConsultaPuntosRespType_QNAME, DataConsultaPuntosRespType2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsultaPuntosReqType2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1", name = "DataConsultaPuntosReqType")
    public JAXBElement<DataConsultaPuntosReqType2> createDataConsultaPuntosReqType(DataConsultaPuntosReqType2 value) {
        return new JAXBElement<DataConsultaPuntosReqType2>(_DataConsultaPuntosReqType_QNAME, DataConsultaPuntosReqType2 .class, null, value);
    }

}
