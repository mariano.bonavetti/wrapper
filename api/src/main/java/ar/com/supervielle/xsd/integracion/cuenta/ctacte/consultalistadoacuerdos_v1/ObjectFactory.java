
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoacuerdos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoacuerdos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoacuerdos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaListadoAcuerdosRespType }
     * 
     */
    public DataConsultaListadoAcuerdosRespType createDataConsultaListadoAcuerdosRespType() {
        return new DataConsultaListadoAcuerdosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoAcuerdosRespType.Row }
     * 
     */
    public DataConsultaListadoAcuerdosRespType.Row createDataConsultaListadoAcuerdosRespTypeRow() {
        return new DataConsultaListadoAcuerdosRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaListadoAcuerdos }
     * 
     */
    public ReqConsultaListadoAcuerdos createReqConsultaListadoAcuerdos() {
        return new ReqConsultaListadoAcuerdos();
    }

    /**
     * Create an instance of {@link DataConsultaListadoAcuerdosReqType }
     * 
     */
    public DataConsultaListadoAcuerdosReqType createDataConsultaListadoAcuerdosReqType() {
        return new DataConsultaListadoAcuerdosReqType();
    }

    /**
     * Create an instance of {@link RespConsultaListadoAcuerdos }
     * 
     */
    public RespConsultaListadoAcuerdos createRespConsultaListadoAcuerdos() {
        return new RespConsultaListadoAcuerdos();
    }

    /**
     * Create an instance of {@link DataConsultaListadoAcuerdosRespType.Row.Renovacion }
     * 
     */
    public DataConsultaListadoAcuerdosRespType.Row.Renovacion createDataConsultaListadoAcuerdosRespTypeRowRenovacion() {
        return new DataConsultaListadoAcuerdosRespType.Row.Renovacion();
    }

}
