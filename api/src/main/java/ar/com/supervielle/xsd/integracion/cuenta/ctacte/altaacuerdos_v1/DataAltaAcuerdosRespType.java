
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.EstadoType;


/**
 * <p>Clase Java para DataAltaAcuerdosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAcuerdosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
 *                   &lt;element name="codAcuerdo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAcuerdosRespType", propOrder = {
    "row"
})
public class DataAltaAcuerdosRespType {

    @XmlElement(name = "Row")
    protected DataAltaAcuerdosRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAcuerdosRespType.Row }
     *     
     */
    public DataAltaAcuerdosRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAcuerdosRespType.Row }
     *     
     */
    public void setRow(DataAltaAcuerdosRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
     *         &lt;element name="codAcuerdo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "estado",
        "codAcuerdo"
    })
    public static class Row {

        @XmlElement(required = true)
        protected EstadoType estado;
        protected Integer codAcuerdo;

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link EstadoType }
         *     
         */
        public EstadoType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link EstadoType }
         *     
         */
        public void setEstado(EstadoType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad codAcuerdo.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodAcuerdo() {
            return codAcuerdo;
        }

        /**
         * Define el valor de la propiedad codAcuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodAcuerdo(Integer value) {
            this.codAcuerdo = value;
        }

    }

}
