
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.bajaacuerdos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.bajaacuerdos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.bajaacuerdos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataBajaAcuerdosReqType }
     * 
     */
    public DataBajaAcuerdosReqType createDataBajaAcuerdosReqType() {
        return new DataBajaAcuerdosReqType();
    }

    /**
     * Create an instance of {@link DataBajaAcuerdosRespType }
     * 
     */
    public DataBajaAcuerdosRespType createDataBajaAcuerdosRespType() {
        return new DataBajaAcuerdosRespType();
    }

    /**
     * Create an instance of {@link RespBajaAcuerdos }
     * 
     */
    public RespBajaAcuerdos createRespBajaAcuerdos() {
        return new RespBajaAcuerdos();
    }

    /**
     * Create an instance of {@link ReqBajaAcuerdos }
     * 
     */
    public ReqBajaAcuerdos createReqBajaAcuerdos() {
        return new ReqBajaAcuerdos();
    }

    /**
     * Create an instance of {@link OperacionType }
     * 
     */
    public OperacionType createOperacionType() {
        return new OperacionType();
    }

    /**
     * Create an instance of {@link DataBajaAcuerdosReqType.Acuerdo }
     * 
     */
    public DataBajaAcuerdosReqType.Acuerdo createDataBajaAcuerdosReqTypeAcuerdo() {
        return new DataBajaAcuerdosReqType.Acuerdo();
    }

    /**
     * Create an instance of {@link DataBajaAcuerdosReqType.Cuenta }
     * 
     */
    public DataBajaAcuerdosReqType.Cuenta createDataBajaAcuerdosReqTypeCuenta() {
        return new DataBajaAcuerdosReqType.Cuenta();
    }

    /**
     * Create an instance of {@link DataBajaAcuerdosRespType.Row }
     * 
     */
    public DataBajaAcuerdosRespType.Row createDataBajaAcuerdosRespTypeRow() {
        return new DataBajaAcuerdosRespType.Row();
    }

}
