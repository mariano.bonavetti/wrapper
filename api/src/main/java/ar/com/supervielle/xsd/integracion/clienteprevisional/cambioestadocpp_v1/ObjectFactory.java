
package ar.com.supervielle.xsd.integracion.clienteprevisional.cambioestadocpp_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.clienteprevisional.cambioestadocpp_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.clienteprevisional.cambioestadocpp_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataCambioEstadoCPPRespType }
     * 
     */
    public DataCambioEstadoCPPRespType createDataCambioEstadoCPPRespType() {
        return new DataCambioEstadoCPPRespType();
    }

    /**
     * Create an instance of {@link ReqCambioEstadoCPP }
     * 
     */
    public ReqCambioEstadoCPP createReqCambioEstadoCPP() {
        return new ReqCambioEstadoCPP();
    }

    /**
     * Create an instance of {@link DataCambioEstadoCPPReqType }
     * 
     */
    public DataCambioEstadoCPPReqType createDataCambioEstadoCPPReqType() {
        return new DataCambioEstadoCPPReqType();
    }

    /**
     * Create an instance of {@link RespCambioEstadoCPP }
     * 
     */
    public RespCambioEstadoCPP createRespCambioEstadoCPP() {
        return new RespCambioEstadoCPP();
    }

    /**
     * Create an instance of {@link DataCambioEstadoCPPRespType.Row }
     * 
     */
    public DataCambioEstadoCPPRespType.Row createDataCambioEstadoCPPRespTypeRow() {
        return new DataCambioEstadoCPPRespType.Row();
    }

}
