
package ar.com.supervielle.xsd.integracion.header_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestData complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userContext" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}UsernameToken"/>
 *         &lt;element name="serviceContext" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}Destination"/>
 *         &lt;element name="sessionContext" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}SessionContext"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestData", propOrder = {
    "userContext",
    "serviceContext",
    "sessionContext"
})
public class RequestData {

    @XmlElement(required = true)
    protected UsernameToken userContext;
    @XmlElement(required = true)
    protected Destination serviceContext;
    @XmlElement(required = true)
    protected SessionContext sessionContext;

    /**
     * Obtiene el valor de la propiedad userContext.
     * 
     * @return
     *     possible object is
     *     {@link UsernameToken }
     *     
     */
    public UsernameToken getUserContext() {
        return userContext;
    }

    /**
     * Define el valor de la propiedad userContext.
     * 
     * @param value
     *     allowed object is
     *     {@link UsernameToken }
     *     
     */
    public void setUserContext(UsernameToken value) {
        this.userContext = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceContext.
     * 
     * @return
     *     possible object is
     *     {@link Destination }
     *     
     */
    public Destination getServiceContext() {
        return serviceContext;
    }

    /**
     * Define el valor de la propiedad serviceContext.
     * 
     * @param value
     *     allowed object is
     *     {@link Destination }
     *     
     */
    public void setServiceContext(Destination value) {
        this.serviceContext = value;
    }

    /**
     * Obtiene el valor de la propiedad sessionContext.
     * 
     * @return
     *     possible object is
     *     {@link SessionContext }
     *     
     */
    public SessionContext getSessionContext() {
        return sessionContext;
    }

    /**
     * Define el valor de la propiedad sessionContext.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionContext }
     *     
     */
    public void setSessionContext(SessionContext value) {
        this.sessionContext = value;
    }

}
