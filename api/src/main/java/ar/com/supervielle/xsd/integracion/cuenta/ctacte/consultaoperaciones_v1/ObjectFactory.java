
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaoperaciones_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaoperaciones_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaoperaciones_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaOperacionesReqType }
     * 
     */
    public DataConsultaOperacionesReqType createDataConsultaOperacionesReqType() {
        return new DataConsultaOperacionesReqType();
    }

    /**
     * Create an instance of {@link DataConsultaOperacionesRespType }
     * 
     */
    public DataConsultaOperacionesRespType createDataConsultaOperacionesRespType() {
        return new DataConsultaOperacionesRespType();
    }

    /**
     * Create an instance of {@link RespConsultaOperaciones }
     * 
     */
    public RespConsultaOperaciones createRespConsultaOperaciones() {
        return new RespConsultaOperaciones();
    }

    /**
     * Create an instance of {@link ReqConsultaOperaciones }
     * 
     */
    public ReqConsultaOperaciones createReqConsultaOperaciones() {
        return new ReqConsultaOperaciones();
    }

    /**
     * Create an instance of {@link DataConsultaOperacionesReqType.RangoFecha }
     * 
     */
    public DataConsultaOperacionesReqType.RangoFecha createDataConsultaOperacionesReqTypeRangoFecha() {
        return new DataConsultaOperacionesReqType.RangoFecha();
    }

    /**
     * Create an instance of {@link DataConsultaOperacionesRespType.Row }
     * 
     */
    public DataConsultaOperacionesRespType.Row createDataConsultaOperacionesRespTypeRow() {
        return new DataConsultaOperacionesRespType.Row();
    }

}
