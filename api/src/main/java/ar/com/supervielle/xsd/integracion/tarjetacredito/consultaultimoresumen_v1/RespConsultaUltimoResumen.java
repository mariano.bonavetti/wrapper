
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaultimoresumen_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaUltimoResumen-v1}DataConsultaUltimoResumenRespType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "RespConsultaUltimoResumen", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaUltimoResumen-v1")
public class RespConsultaUltimoResumen {

    @XmlElement(name = "Data", required = true)
    protected DataConsultaUltimoResumenRespType data;

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaUltimoResumenRespType }
     *     
     */
    public DataConsultaUltimoResumenRespType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaUltimoResumenRespType }
     *     
     */
    public void setData(DataConsultaUltimoResumenRespType value) {
        this.data = value;
    }

}
