
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametros_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaParametrosReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaParametrosReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Principal">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="InterCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="ProducCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Filiatorios" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Complementarios" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaParametrosReq", propOrder = {
    "operacion",
    "principal",
    "filiatorios",
    "complementarios"
})
public class DataConsultaParametrosReq {

    @XmlElement(name = "Operacion")
    protected int operacion;
    @XmlElement(name = "Principal", required = true)
    protected DataConsultaParametrosReq.Principal principal;
    @XmlElement(name = "Filiatorios")
    protected String filiatorios;
    @XmlElement(name = "Complementarios")
    protected String complementarios;

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     */
    public int getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     */
    public void setOperacion(int value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad principal.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaParametrosReq.Principal }
     *     
     */
    public DataConsultaParametrosReq.Principal getPrincipal() {
        return principal;
    }

    /**
     * Define el valor de la propiedad principal.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaParametrosReq.Principal }
     *     
     */
    public void setPrincipal(DataConsultaParametrosReq.Principal value) {
        this.principal = value;
    }

    /**
     * Obtiene el valor de la propiedad filiatorios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiliatorios() {
        return filiatorios;
    }

    /**
     * Define el valor de la propiedad filiatorios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiliatorios(String value) {
        this.filiatorios = value;
    }

    /**
     * Obtiene el valor de la propiedad complementarios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplementarios() {
        return complementarios;
    }

    /**
     * Define el valor de la propiedad complementarios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplementarios(String value) {
        this.complementarios = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="InterCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="ProducCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "interCodi",
        "producCodi"
    })
    public static class Principal {

        @XmlElement(name = "InterCodi")
        protected int interCodi;
        @XmlElement(name = "ProducCodi")
        protected int producCodi;

        /**
         * Obtiene el valor de la propiedad interCodi.
         * 
         */
        public int getInterCodi() {
            return interCodi;
        }

        /**
         * Define el valor de la propiedad interCodi.
         * 
         */
        public void setInterCodi(int value) {
            this.interCodi = value;
        }

        /**
         * Obtiene el valor de la propiedad producCodi.
         * 
         */
        public int getProducCodi() {
            return producCodi;
        }

        /**
         * Define el valor de la propiedad producCodi.
         * 
         */
        public void setProducCodi(int value) {
            this.producCodi = value;
        }

    }

}
