
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para IdCajaSegType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="IdCajaSegType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nroCajaSeguridad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maxCorrelativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdCajaSegType", propOrder = {
    "empresa",
    "sucursal",
    "cuenta",
    "nroCajaSeguridad",
    "maxCorrelativo"
})
public class IdCajaSegType {

    @XmlElement(required = true)
    protected String empresa;
    @XmlElement(required = true)
    protected String sucursal;
    @XmlElement(required = true)
    protected String cuenta;
    @XmlElement(required = true)
    protected String nroCajaSeguridad;
    protected String maxCorrelativo;

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Define el valor de la propiedad sucursal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucursal(String value) {
        this.sucursal = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCajaSeguridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCajaSeguridad() {
        return nroCajaSeguridad;
    }

    /**
     * Define el valor de la propiedad nroCajaSeguridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCajaSeguridad(String value) {
        this.nroCajaSeguridad = value;
    }

    /**
     * Obtiene el valor de la propiedad maxCorrelativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxCorrelativo() {
        return maxCorrelativo;
    }

    /**
     * Define el valor de la propiedad maxCorrelativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxCorrelativo(String value) {
        this.maxCorrelativo = value;
    }

}
