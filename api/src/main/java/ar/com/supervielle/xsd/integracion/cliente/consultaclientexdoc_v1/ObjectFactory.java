
package ar.com.supervielle.xsd.integracion.cliente.consultaclientexdoc_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultaclientexdoc_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultaclientexdoc_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocRespType2 }
     * 
     */
    public DataConsultaClientexDocRespType2 createDataConsultaClientexDocRespType2() {
        return new DataConsultaClientexDocRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocRespType }
     * 
     */
    public DataConsultaClientexDocRespType createDataConsultaClientexDocRespType() {
        return new DataConsultaClientexDocRespType();
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocReqType }
     * 
     */
    public DataConsultaClientexDocReqType createDataConsultaClientexDocReqType() {
        return new DataConsultaClientexDocReqType();
    }

    /**
     * Create an instance of {@link ReqConsultaClientexDoc }
     * 
     */
    public ReqConsultaClientexDoc createReqConsultaClientexDoc() {
        return new ReqConsultaClientexDoc();
    }

    /**
     * Create an instance of {@link RespConsultaClientexDoc }
     * 
     */
    public RespConsultaClientexDoc createRespConsultaClientexDoc() {
        return new RespConsultaClientexDoc();
    }

    /**
     * Create an instance of {@link ReqConsultaClientexDoc2 }
     * 
     */
    public ReqConsultaClientexDoc2 createReqConsultaClientexDoc2() {
        return new ReqConsultaClientexDoc2();
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocReqType2 }
     * 
     */
    public DataConsultaClientexDocReqType2 createDataConsultaClientexDocReqType2() {
        return new DataConsultaClientexDocReqType2();
    }

    /**
     * Create an instance of {@link RespConsultaClientexDoc2 }
     * 
     */
    public RespConsultaClientexDoc2 createRespConsultaClientexDoc2() {
        return new RespConsultaClientexDoc2();
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocRespType2 .Row }
     * 
     */
    public DataConsultaClientexDocRespType2 .Row createDataConsultaClientexDocRespType2Row() {
        return new DataConsultaClientexDocRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocRespType.Row }
     * 
     */
    public DataConsultaClientexDocRespType.Row createDataConsultaClientexDocRespTypeRow() {
        return new DataConsultaClientexDocRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaClientexDocReqType.Identificador }
     * 
     */
    public DataConsultaClientexDocReqType.Identificador createDataConsultaClientexDocReqTypeIdentificador() {
        return new DataConsultaClientexDocReqType.Identificador();
    }

}
