
package ar.com.supervielle.xsd.integracion.tarjetacredito.adhesioneresumen_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.adhesioneresumen_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataAdhesionEResumenRespType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/adhesionEResumen-v1", "DataAdhesionEResumenRespType");
    private final static QName _DataAdhesionEResumensReqType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/adhesionEResumen-v1", "DataAdhesionEResumensReqType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.adhesioneresumen_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAdhesionEResumenRespType }
     * 
     */
    public DataAdhesionEResumenRespType createDataAdhesionEResumenRespType() {
        return new DataAdhesionEResumenRespType();
    }

    /**
     * Create an instance of {@link DataAdhesionEResumenRespType.Row }
     * 
     */
    public DataAdhesionEResumenRespType.Row createDataAdhesionEResumenRespTypeRow() {
        return new DataAdhesionEResumenRespType.Row();
    }

    /**
     * Create an instance of {@link DataAdhesionEResumensReqType }
     * 
     */
    public DataAdhesionEResumensReqType createDataAdhesionEResumensReqType() {
        return new DataAdhesionEResumensReqType();
    }

    /**
     * Create an instance of {@link RespAdhesionEResumen }
     * 
     */
    public RespAdhesionEResumen createRespAdhesionEResumen() {
        return new RespAdhesionEResumen();
    }

    /**
     * Create an instance of {@link ReqAdhesionEResumen }
     * 
     */
    public ReqAdhesionEResumen createReqAdhesionEResumen() {
        return new ReqAdhesionEResumen();
    }

    /**
     * Create an instance of {@link DataAdhesionEResumenRespType.Row.Resultado }
     * 
     */
    public DataAdhesionEResumenRespType.Row.Resultado createDataAdhesionEResumenRespTypeRowResultado() {
        return new DataAdhesionEResumenRespType.Row.Resultado();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataAdhesionEResumenRespType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/adhesionEResumen-v1", name = "DataAdhesionEResumenRespType")
    public JAXBElement<DataAdhesionEResumenRespType> createDataAdhesionEResumenRespType(DataAdhesionEResumenRespType value) {
        return new JAXBElement<DataAdhesionEResumenRespType>(_DataAdhesionEResumenRespType_QNAME, DataAdhesionEResumenRespType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataAdhesionEResumensReqType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/adhesionEResumen-v1", name = "DataAdhesionEResumensReqType")
    public JAXBElement<DataAdhesionEResumensReqType> createDataAdhesionEResumensReqType(DataAdhesionEResumensReqType value) {
        return new JAXBElement<DataAdhesionEResumensReqType>(_DataAdhesionEResumensReqType_QNAME, DataAdhesionEResumensReqType.class, null, value);
    }

}
