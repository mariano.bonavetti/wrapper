
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacbubantotal_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacbubantotal_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacbubantotal_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCBUBANRespType }
     * 
     */
    public DataConsultaCBUBANRespType createDataConsultaCBUBANRespType() {
        return new DataConsultaCBUBANRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaCBUBAN }
     * 
     */
    public ReqConsultaCBUBAN createReqConsultaCBUBAN() {
        return new ReqConsultaCBUBAN();
    }

    /**
     * Create an instance of {@link DataConsultaCBUBANReqType }
     * 
     */
    public DataConsultaCBUBANReqType createDataConsultaCBUBANReqType() {
        return new DataConsultaCBUBANReqType();
    }

    /**
     * Create an instance of {@link RespConsultaCBUBAN }
     * 
     */
    public RespConsultaCBUBAN createRespConsultaCBUBAN() {
        return new RespConsultaCBUBAN();
    }

    /**
     * Create an instance of {@link DataConsultaCBUBANRespType.Row }
     * 
     */
    public DataConsultaCBUBANRespType.Row createDataConsultaCBUBANRespTypeRow() {
        return new DataConsultaCBUBANRespType.Row();
    }

}
