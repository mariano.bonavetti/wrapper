
package ar.com.supervielle.xsd.integracion.cliente.consultaclientexdoc_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/cliente/consultaClientexDoc-v1}DataConsultaClientexDocRespType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "RespConsultaClientexDoc", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cliente/consultaClientexDoc-v1")
public class RespConsultaClientexDoc2 {

    @XmlElement(name = "Data", required = true)
    protected DataConsultaClientexDocRespType2 data;

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaClientexDocRespType2 }
     *     
     */
    public DataConsultaClientexDocRespType2 getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaClientexDocRespType2 }
     *     
     */
    public void setData(DataConsultaClientexDocRespType2 value) {
        this.data = value;
    }

}
