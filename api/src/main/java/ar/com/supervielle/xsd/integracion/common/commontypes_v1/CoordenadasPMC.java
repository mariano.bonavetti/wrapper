
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CoordenadasPMC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CoordenadasPMC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantCoorDisp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenada1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenada2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenada3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenada4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenadaDisplay1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenadaDisplay2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenadaDisplay3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="coordenadaDisplay4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nroSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoordenadasPMC", propOrder = {
    "canal",
    "cantCoorDisp",
    "coordenada1",
    "coordenada2",
    "coordenada3",
    "coordenada4",
    "coordenadaDisplay1",
    "coordenadaDisplay2",
    "coordenadaDisplay3",
    "coordenadaDisplay4",
    "hash",
    "nroSerie"
})
public class CoordenadasPMC {

    protected String canal;
    protected String cantCoorDisp;
    protected String coordenada1;
    protected String coordenada2;
    protected String coordenada3;
    protected String coordenada4;
    protected String coordenadaDisplay1;
    protected String coordenadaDisplay2;
    protected String coordenadaDisplay3;
    protected String coordenadaDisplay4;
    protected String hash;
    protected String nroSerie;

    /**
     * Obtiene el valor de la propiedad canal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanal() {
        return canal;
    }

    /**
     * Define el valor de la propiedad canal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanal(String value) {
        this.canal = value;
    }

    /**
     * Obtiene el valor de la propiedad cantCoorDisp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantCoorDisp() {
        return cantCoorDisp;
    }

    /**
     * Define el valor de la propiedad cantCoorDisp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantCoorDisp(String value) {
        this.cantCoorDisp = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenada1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenada1() {
        return coordenada1;
    }

    /**
     * Define el valor de la propiedad coordenada1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenada1(String value) {
        this.coordenada1 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenada2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenada2() {
        return coordenada2;
    }

    /**
     * Define el valor de la propiedad coordenada2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenada2(String value) {
        this.coordenada2 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenada3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenada3() {
        return coordenada3;
    }

    /**
     * Define el valor de la propiedad coordenada3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenada3(String value) {
        this.coordenada3 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenada4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenada4() {
        return coordenada4;
    }

    /**
     * Define el valor de la propiedad coordenada4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenada4(String value) {
        this.coordenada4 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenadaDisplay1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenadaDisplay1() {
        return coordenadaDisplay1;
    }

    /**
     * Define el valor de la propiedad coordenadaDisplay1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenadaDisplay1(String value) {
        this.coordenadaDisplay1 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenadaDisplay2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenadaDisplay2() {
        return coordenadaDisplay2;
    }

    /**
     * Define el valor de la propiedad coordenadaDisplay2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenadaDisplay2(String value) {
        this.coordenadaDisplay2 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenadaDisplay3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenadaDisplay3() {
        return coordenadaDisplay3;
    }

    /**
     * Define el valor de la propiedad coordenadaDisplay3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenadaDisplay3(String value) {
        this.coordenadaDisplay3 = value;
    }

    /**
     * Obtiene el valor de la propiedad coordenadaDisplay4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoordenadaDisplay4() {
        return coordenadaDisplay4;
    }

    /**
     * Define el valor de la propiedad coordenadaDisplay4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoordenadaDisplay4(String value) {
        this.coordenadaDisplay4 = value;
    }

    /**
     * Obtiene el valor de la propiedad hash.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHash() {
        return hash;
    }

    /**
     * Define el valor de la propiedad hash.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHash(String value) {
        this.hash = value;
    }

    /**
     * Obtiene el valor de la propiedad nroSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroSerie() {
        return nroSerie;
    }

    /**
     * Define el valor de la propiedad nroSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroSerie(String value) {
        this.nroSerie = value;
    }

}
