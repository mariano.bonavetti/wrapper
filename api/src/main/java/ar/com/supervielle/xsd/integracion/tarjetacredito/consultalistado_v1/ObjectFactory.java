
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataConsultaListadoRespType3RowDetalleTarjetaTitularidad_QNAME = new QName("", "titularidad");
    private final static QName _DataConsultaListadoRespType4RowListadoVigencia_QNAME = new QName("", "vigencia");
    private final static QName _DataConsultaListadoRespType4RowListadoFecha_QNAME = new QName("", "fecha");
    private final static QName _DataConsultaListadoRespType4RowListadoFechaProcesamiento_QNAME = new QName("", "fechaProcesamiento");
    private final static QName _DataConsultaListadoRespType4RowListadoCierreFecha_QNAME = new QName("", "cierreFecha");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DatosTarjeta2 }
     * 
     */
    public DatosTarjeta2 createDatosTarjeta2() {
        return new DatosTarjeta2();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType4 }
     * 
     */
    public DataConsultaListadoRespType4 createDataConsultaListadoRespType4() {
        return new DataConsultaListadoRespType4();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType4 .Row }
     * 
     */
    public DataConsultaListadoRespType4 .Row createDataConsultaListadoRespType4Row() {
        return new DataConsultaListadoRespType4 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType4 .Row.Listado }
     * 
     */
    public DataConsultaListadoRespType4 .Row.Listado createDataConsultaListadoRespType4RowListado() {
        return new DataConsultaListadoRespType4 .Row.Listado();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType3 }
     * 
     */
    public DataConsultaListadoReqType3 createDataConsultaListadoReqType3() {
        return new DataConsultaListadoReqType3();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 }
     * 
     */
    public DataConsultaListadoRespType3 createDataConsultaListadoRespType3() {
        return new DataConsultaListadoRespType3();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row }
     * 
     */
    public DataConsultaListadoRespType3 .Row createDataConsultaListadoRespType3Row() {
        return new DataConsultaListadoRespType3 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DatosAdicionales }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DatosAdicionales createDataConsultaListadoRespType3RowDatosAdicionales() {
        return new DataConsultaListadoRespType3 .Row.DatosAdicionales();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta createDataConsultaListadoRespType3RowDetalleCuenta() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas createDataConsultaListadoRespType3RowDetalleCuentaTasas() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos createDataConsultaListadoRespType3RowDetalleCuentaSaldos() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleTarjeta createDataConsultaListadoRespType3RowDetalleTarjeta() {
        return new DataConsultaListadoRespType3 .Row.DetalleTarjeta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType2 }
     * 
     */
    public DataConsultaListadoRespType2 createDataConsultaListadoRespType2() {
        return new DataConsultaListadoRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType2 .Row }
     * 
     */
    public DataConsultaListadoRespType2 .Row createDataConsultaListadoRespType2Row() {
        return new DataConsultaListadoRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType2 .Row.Listado }
     * 
     */
    public DataConsultaListadoRespType2 .Row.Listado createDataConsultaListadoRespType2RowListado() {
        return new DataConsultaListadoRespType2 .Row.Listado();
    }

    /**
     * Create an instance of {@link DatosTarjeta }
     * 
     */
    public DatosTarjeta createDatosTarjeta() {
        return new DatosTarjeta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType }
     * 
     */
    public DataConsultaListadoRespType createDataConsultaListadoRespType() {
        return new DataConsultaListadoRespType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row }
     * 
     */
    public DataConsultaListadoRespType.Row createDataConsultaListadoRespTypeRow() {
        return new DataConsultaListadoRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row.Listado }
     * 
     */
    public DataConsultaListadoRespType.Row.Listado createDataConsultaListadoRespTypeRowListado() {
        return new DataConsultaListadoRespType.Row.Listado();
    }

    /**
     * Create an instance of {@link RespConsultaListado }
     * 
     */
    public RespConsultaListado createRespConsultaListado() {
        return new RespConsultaListado();
    }

    /**
     * Create an instance of {@link ReqConsultaListado }
     * 
     */
    public ReqConsultaListado createReqConsultaListado() {
        return new ReqConsultaListado();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType }
     * 
     */
    public DataConsultaListadoReqType createDataConsultaListadoReqType() {
        return new DataConsultaListadoReqType();
    }

    /**
     * Create an instance of {@link RespConsultaListado2 }
     * 
     */
    public RespConsultaListado2 createRespConsultaListado2() {
        return new RespConsultaListado2();
    }

    /**
     * Create an instance of {@link ReqConsultaListado2 }
     * 
     */
    public ReqConsultaListado2 createReqConsultaListado2() {
        return new ReqConsultaListado2();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType2 }
     * 
     */
    public DataConsultaListadoReqType2 createDataConsultaListadoReqType2() {
        return new DataConsultaListadoReqType2();
    }

    /**
     * Create an instance of {@link RespConsultaListado3 }
     * 
     */
    public RespConsultaListado3 createRespConsultaListado3() {
        return new RespConsultaListado3();
    }

    /**
     * Create an instance of {@link ReqConsultaListado3 }
     * 
     */
    public ReqConsultaListado3 createReqConsultaListado3() {
        return new ReqConsultaListado3();
    }

    /**
     * Create an instance of {@link RespConsultaListado4 }
     * 
     */
    public RespConsultaListado4 createRespConsultaListado4() {
        return new RespConsultaListado4();
    }

    /**
     * Create an instance of {@link ReqConsultaListado4 }
     * 
     */
    public ReqConsultaListado4 createReqConsultaListado4() {
        return new ReqConsultaListado4();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType4 }
     * 
     */
    public DataConsultaListadoReqType4 createDataConsultaListadoReqType4() {
        return new DataConsultaListadoReqType4();
    }

    /**
     * Create an instance of {@link DatosTarjeta2 .MarcaPago }
     * 
     */
    public DatosTarjeta2 .MarcaPago createDatosTarjeta2MarcaPago() {
        return new DatosTarjeta2 .MarcaPago();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType4 .Row.Listado.Saldo }
     * 
     */
    public DataConsultaListadoRespType4 .Row.Listado.Saldo createDataConsultaListadoRespType4RowListadoSaldo() {
        return new DataConsultaListadoRespType4 .Row.Listado.Saldo();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType3 .Identificador }
     * 
     */
    public DataConsultaListadoReqType3 .Identificador createDataConsultaListadoReqType3Identificador() {
        return new DataConsultaListadoReqType3 .Identificador();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo createDataConsultaListadoRespType3RowDatosAdicionalesAtributo() {
        return new DataConsultaListadoRespType3 .Row.DatosAdicionales.Atributo();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre createDataConsultaListadoRespType3RowDetalleCuentaFechaCierre() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaCierre();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento createDataConsultaListadoRespType3RowDetalleCuentaFechaVencimiento() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.FechaVencimiento();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito createDataConsultaListadoRespType3RowDetalleCuentaCuentaDebito() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.CuentaDebito();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual createDataConsultaListadoRespType3RowDetalleCuentaTasasTasaMensual() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaMensual();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual createDataConsultaListadoRespType3RowDetalleCuentaTasasTasaAnual() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.Tasas.TasaAnual();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual createDataConsultaListadoRespType3RowDetalleCuentaSaldosSaldoActual() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoActual();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior createDataConsultaListadoRespType3RowDetalleCuentaSaldosSaldoAnterior() {
        return new DataConsultaListadoRespType3 .Row.DetalleCuenta.Saldos.SaldoAnterior();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia createDataConsultaListadoRespType3RowDetalleTarjetaFechaDeVigencia() {
        return new DataConsultaListadoRespType3 .Row.DetalleTarjeta.FechaDeVigencia();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta createDataConsultaListadoRespType3RowDetalleTarjetaTipoTarjeta() {
        return new DataConsultaListadoRespType3 .Row.DetalleTarjeta.TipoTarjeta();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto createDataConsultaListadoRespType3RowDetalleTarjetaProducto() {
        return new DataConsultaListadoRespType3 .Row.DetalleTarjeta.Producto();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites createDataConsultaListadoRespType3RowDetalleTarjetaLimites() {
        return new DataConsultaListadoRespType3 .Row.DetalleTarjeta.Limites();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega }
     * 
     */
    public DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega createDataConsultaListadoRespType3RowDetalleTarjetaDatosEntrega() {
        return new DataConsultaListadoRespType3 .Row.DetalleTarjeta.DatosEntrega();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType2 .Row.Listado.Saldo }
     * 
     */
    public DataConsultaListadoRespType2 .Row.Listado.Saldo createDataConsultaListadoRespType2RowListadoSaldo() {
        return new DataConsultaListadoRespType2 .Row.Listado.Saldo();
    }

    /**
     * Create an instance of {@link DatosTarjeta.MarcaPago }
     * 
     */
    public DatosTarjeta.MarcaPago createDatosTarjetaMarcaPago() {
        return new DatosTarjeta.MarcaPago();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row.Listado.Saldo }
     * 
     */
    public DataConsultaListadoRespType.Row.Listado.Saldo createDataConsultaListadoRespTypeRowListadoSaldo() {
        return new DataConsultaListadoRespType.Row.Listado.Saldo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodDescStringType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "titularidad", scope = DataConsultaListadoRespType3 .Row.DetalleTarjeta.class)
    public JAXBElement<CodDescStringType> createDataConsultaListadoRespType3RowDetalleTarjetaTitularidad(CodDescStringType value) {
        return new JAXBElement<CodDescStringType>(_DataConsultaListadoRespType3RowDetalleTarjetaTitularidad_QNAME, CodDescStringType.class, DataConsultaListadoRespType3 .Row.DetalleTarjeta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vigencia", scope = DataConsultaListadoRespType4 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType4RowListadoVigencia(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoVigencia_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType4 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fecha", scope = DataConsultaListadoRespType4 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType4RowListadoFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoFecha_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType4 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fechaProcesamiento", scope = DataConsultaListadoRespType4 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType4RowListadoFechaProcesamiento(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoFechaProcesamiento_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType4 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cierreFecha", scope = DataConsultaListadoRespType4 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType4RowListadoCierreFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoCierreFecha_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType4 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vigencia", scope = DataConsultaListadoRespType.Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespTypeRowListadoVigencia(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoVigencia_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType.Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fecha", scope = DataConsultaListadoRespType.Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespTypeRowListadoFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoFecha_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType.Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fechaProcesamiento", scope = DataConsultaListadoRespType.Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespTypeRowListadoFechaProcesamiento(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoFechaProcesamiento_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType.Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cierreFecha", scope = DataConsultaListadoRespType.Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespTypeRowListadoCierreFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoCierreFecha_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType.Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vigencia", scope = DataConsultaListadoRespType2 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType2RowListadoVigencia(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoVigencia_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType2 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fecha", scope = DataConsultaListadoRespType2 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType2RowListadoFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoFecha_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType2 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fechaProcesamiento", scope = DataConsultaListadoRespType2 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType2RowListadoFechaProcesamiento(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoFechaProcesamiento_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType2 .Row.Listado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cierreFecha", scope = DataConsultaListadoRespType2 .Row.Listado.class)
    public JAXBElement<XMLGregorianCalendar> createDataConsultaListadoRespType2RowListadoCierreFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DataConsultaListadoRespType4RowListadoCierreFecha_QNAME, XMLGregorianCalendar.class, DataConsultaListadoRespType2 .Row.Listado.class, value);
    }

}
