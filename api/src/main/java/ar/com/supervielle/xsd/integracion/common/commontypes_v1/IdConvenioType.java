
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para idConvenioType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="idConvenioType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="subempresa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="numeroDependencia" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="dependencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="puerta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="unidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDependencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cantidadBeneficiarios" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "idConvenioType", propOrder = {
    "empresa",
    "pais",
    "tipoDocumento",
    "subempresa",
    "numeroDependencia",
    "dependencia",
    "nombre",
    "calle",
    "puerta",
    "piso",
    "unidad",
    "localidad",
    "codigoPostal",
    "tipoDependencia",
    "cantidadBeneficiarios",
    "estado"
})
public class IdConvenioType {

    @XmlElement(required = true)
    protected BigDecimal empresa;
    @XmlElement(required = true)
    protected BigDecimal pais;
    @XmlElement(required = true)
    protected BigDecimal tipoDocumento;
    @XmlElement(required = true)
    protected BigDecimal subempresa;
    @XmlElement(required = true)
    protected BigDecimal numeroDependencia;
    @XmlElement(required = true)
    protected String dependencia;
    @XmlElement(required = true)
    protected Object nombre;
    @XmlElement(required = true)
    protected String calle;
    @XmlElement(required = true)
    protected String puerta;
    @XmlElement(required = true)
    protected String piso;
    @XmlElement(required = true)
    protected String unidad;
    @XmlElement(required = true)
    protected String localidad;
    @XmlElement(required = true)
    protected String codigoPostal;
    @XmlElement(required = true)
    protected String tipoDependencia;
    @XmlElement(required = true)
    protected BigDecimal cantidadBeneficiarios;
    @XmlElement(required = true)
    protected String estado;

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEmpresa(BigDecimal value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad pais.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPais() {
        return pais;
    }

    /**
     * Define el valor de la propiedad pais.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPais(BigDecimal value) {
        this.pais = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTipoDocumento(BigDecimal value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad subempresa.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubempresa() {
        return subempresa;
    }

    /**
     * Define el valor de la propiedad subempresa.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubempresa(BigDecimal value) {
        this.subempresa = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDependencia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumeroDependencia() {
        return numeroDependencia;
    }

    /**
     * Define el valor de la propiedad numeroDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumeroDependencia(BigDecimal value) {
        this.numeroDependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad dependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencia() {
        return dependencia;
    }

    /**
     * Define el valor de la propiedad dependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependencia(String value) {
        this.dependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setNombre(Object value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad calle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalle() {
        return calle;
    }

    /**
     * Define el valor de la propiedad calle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalle(String value) {
        this.calle = value;
    }

    /**
     * Obtiene el valor de la propiedad puerta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPuerta() {
        return puerta;
    }

    /**
     * Define el valor de la propiedad puerta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPuerta(String value) {
        this.puerta = value;
    }

    /**
     * Obtiene el valor de la propiedad piso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPiso() {
        return piso;
    }

    /**
     * Define el valor de la propiedad piso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPiso(String value) {
        this.piso = value;
    }

    /**
     * Obtiene el valor de la propiedad unidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidad() {
        return unidad;
    }

    /**
     * Define el valor de la propiedad unidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidad(String value) {
        this.unidad = value;
    }

    /**
     * Obtiene el valor de la propiedad localidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalidad() {
        return localidad;
    }

    /**
     * Define el valor de la propiedad localidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalidad(String value) {
        this.localidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoPostal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Define el valor de la propiedad codigoPostal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPostal(String value) {
        this.codigoPostal = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDependencia() {
        return tipoDependencia;
    }

    /**
     * Define el valor de la propiedad tipoDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDependencia(String value) {
        this.tipoDependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadBeneficiarios.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCantidadBeneficiarios() {
        return cantidadBeneficiarios;
    }

    /**
     * Define el valor de la propiedad cantidadBeneficiarios.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCantidadBeneficiarios(BigDecimal value) {
        this.cantidadBeneficiarios = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

}
