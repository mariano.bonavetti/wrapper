
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargosban_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargosban_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargosban_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosBANRespType }
     * 
     */
    public DataConsultaEmbargosBANRespType createDataConsultaEmbargosBANRespType() {
        return new DataConsultaEmbargosBANRespType();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosBANRespType.Row }
     * 
     */
    public DataConsultaEmbargosBANRespType.Row createDataConsultaEmbargosBANRespTypeRow() {
        return new DataConsultaEmbargosBANRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaEmbargosBAN }
     * 
     */
    public ReqConsultaEmbargosBAN createReqConsultaEmbargosBAN() {
        return new ReqConsultaEmbargosBAN();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosBANReqType }
     * 
     */
    public DataConsultaEmbargosBANReqType createDataConsultaEmbargosBANReqType() {
        return new DataConsultaEmbargosBANReqType();
    }

    /**
     * Create an instance of {@link RespConsultaEmbargosBAN }
     * 
     */
    public RespConsultaEmbargosBAN createRespConsultaEmbargosBAN() {
        return new RespConsultaEmbargosBAN();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosBANRespType.Row.Oficio }
     * 
     */
    public DataConsultaEmbargosBANRespType.Row.Oficio createDataConsultaEmbargosBANRespTypeRowOficio() {
        return new DataConsultaEmbargosBANRespType.Row.Oficio();
    }

}
