
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaMovimientosRespType }
     * 
     */
    public DataConsultaMovimientosRespType createDataConsultaMovimientosRespType() {
        return new DataConsultaMovimientosRespType();
    }

    /**
     * Create an instance of {@link RespConsultaMovimientos }
     * 
     */
    public RespConsultaMovimientos createRespConsultaMovimientos() {
        return new RespConsultaMovimientos();
    }

    /**
     * Create an instance of {@link ReqConsultaMovimientos }
     * 
     */
    public ReqConsultaMovimientos createReqConsultaMovimientos() {
        return new ReqConsultaMovimientos();
    }

    /**
     * Create an instance of {@link DataConsultaMovimientosReqType }
     * 
     */
    public DataConsultaMovimientosReqType createDataConsultaMovimientosReqType() {
        return new DataConsultaMovimientosReqType();
    }

    /**
     * Create an instance of {@link DataConsultaMovimientosRespType.Row }
     * 
     */
    public DataConsultaMovimientosRespType.Row createDataConsultaMovimientosRespTypeRow() {
        return new DataConsultaMovimientosRespType.Row();
    }

}
