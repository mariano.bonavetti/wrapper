
package ar.com.supervielle.xsd.integracion.tarjetacredito.altatarjeta_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataAltaTarjetaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaTarjetaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Empresa">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="IdentificadorSolicitud">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                               &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                               &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                               &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="Oficial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
 *                     &lt;element name="cuentaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                     &lt;element name="DatosCliente">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;choice>
 *                                 &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                 &lt;element name="datosClienteNuevo">
 *                                   &lt;complexType>
 *                                     &lt;complexContent>
 *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                         &lt;sequence>
 *                                           &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                           &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                           &lt;element name="Domicilio" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                     &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                     &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                     &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                     &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                     &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                     &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                     &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                     &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                         &lt;/sequence>
 *                                       &lt;/restriction>
 *                                     &lt;/complexContent>
 *                                   &lt;/complexType>
 *                                 &lt;/element>
 *                               &lt;/choice>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="embozado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                     &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="documentoTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                     &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                     &lt;element name="sucursalOrigenBT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                     &lt;element name="habilitacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="Limites">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                               &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                               &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                               &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaTarjetaReqType", propOrder = {
    "empresa"
})
public class DataAltaTarjetaReqType {

    @XmlElement(name = "Empresa")
    protected DataAltaTarjetaReqType.Empresa empresa;

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaTarjetaReqType.Empresa }
     *     
     */
    public DataAltaTarjetaReqType.Empresa getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaTarjetaReqType.Empresa }
     *     
     */
    public void setEmpresa(DataAltaTarjetaReqType.Empresa value) {
        this.empresa = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdentificadorSolicitud">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Oficial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
     *         &lt;element name="cuentaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="DatosCliente">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;choice>
     *                     &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                     &lt;element name="datosClienteNuevo">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                               &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                               &lt;element name="Domicilio" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                         &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                   &lt;/choice>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="embozado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="documentoTributario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="estadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="sucursalOrigenBT" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="habilitacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Limites">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                   &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                   &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                   &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificadorSolicitud",
        "codigoMarca",
        "codigoEntidad",
        "oficial",
        "cuentaEmpresa",
        "identificador",
        "datosCliente",
        "embozado",
        "nacionalidad",
        "sexo",
        "documentoTributario",
        "estadoCivil",
        "fechaNacimiento",
        "ocupacion",
        "sucursalOrigenBT",
        "habilitacion",
        "area",
        "sector",
        "limites",
        "cuartaLinea"
    })
    public static class Empresa {

        @XmlElement(name = "IdentificadorSolicitud", required = true)
        protected DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud identificadorSolicitud;
        protected String codigoMarca;
        protected String codigoEntidad;
        @XmlElement(name = "Oficial")
        protected CodDescNumType oficial;
        @XmlElement(required = true)
        protected String cuentaEmpresa;
        @XmlElement(name = "Identificador", required = true)
        protected IdClienteType identificador;
        @XmlElement(name = "DatosCliente", required = true)
        protected DataAltaTarjetaReqType.Empresa.DatosCliente datosCliente;
        protected String embozado;
        protected BigInteger nacionalidad;
        protected String sexo;
        protected String documentoTributario;
        protected String estadoCivil;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaNacimiento;
        protected Integer ocupacion;
        protected Integer sucursalOrigenBT;
        protected String habilitacion;
        protected String area;
        protected String sector;
        @XmlElement(name = "Limites", required = true)
        protected DataAltaTarjetaReqType.Empresa.Limites limites;
        protected String cuartaLinea;

        /**
         * Obtiene el valor de la propiedad identificadorSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud }
         *     
         */
        public DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud getIdentificadorSolicitud() {
            return identificadorSolicitud;
        }

        /**
         * Define el valor de la propiedad identificadorSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud }
         *     
         */
        public void setIdentificadorSolicitud(DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud value) {
            this.identificadorSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoMarca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoMarca() {
            return codigoMarca;
        }

        /**
         * Define el valor de la propiedad codigoMarca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoMarca(String value) {
            this.codigoMarca = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoEntidad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoEntidad() {
            return codigoEntidad;
        }

        /**
         * Define el valor de la propiedad codigoEntidad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoEntidad(String value) {
            this.codigoEntidad = value;
        }

        /**
         * Obtiene el valor de la propiedad oficial.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getOficial() {
            return oficial;
        }

        /**
         * Define el valor de la propiedad oficial.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setOficial(CodDescNumType value) {
            this.oficial = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaEmpresa.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaEmpresa() {
            return cuentaEmpresa;
        }

        /**
         * Define el valor de la propiedad cuentaEmpresa.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaEmpresa(String value) {
            this.cuentaEmpresa = value;
        }

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad datosCliente.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaTarjetaReqType.Empresa.DatosCliente }
         *     
         */
        public DataAltaTarjetaReqType.Empresa.DatosCliente getDatosCliente() {
            return datosCliente;
        }

        /**
         * Define el valor de la propiedad datosCliente.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaTarjetaReqType.Empresa.DatosCliente }
         *     
         */
        public void setDatosCliente(DataAltaTarjetaReqType.Empresa.DatosCliente value) {
            this.datosCliente = value;
        }

        /**
         * Obtiene el valor de la propiedad embozado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmbozado() {
            return embozado;
        }

        /**
         * Define el valor de la propiedad embozado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmbozado(String value) {
            this.embozado = value;
        }

        /**
         * Obtiene el valor de la propiedad nacionalidad.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNacionalidad() {
            return nacionalidad;
        }

        /**
         * Define el valor de la propiedad nacionalidad.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNacionalidad(BigInteger value) {
            this.nacionalidad = value;
        }

        /**
         * Obtiene el valor de la propiedad sexo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSexo() {
            return sexo;
        }

        /**
         * Define el valor de la propiedad sexo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSexo(String value) {
            this.sexo = value;
        }

        /**
         * Obtiene el valor de la propiedad documentoTributario.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumentoTributario() {
            return documentoTributario;
        }

        /**
         * Define el valor de la propiedad documentoTributario.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumentoTributario(String value) {
            this.documentoTributario = value;
        }

        /**
         * Obtiene el valor de la propiedad estadoCivil.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEstadoCivil() {
            return estadoCivil;
        }

        /**
         * Define el valor de la propiedad estadoCivil.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEstadoCivil(String value) {
            this.estadoCivil = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaNacimiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaNacimiento() {
            return fechaNacimiento;
        }

        /**
         * Define el valor de la propiedad fechaNacimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaNacimiento(XMLGregorianCalendar value) {
            this.fechaNacimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad ocupacion.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOcupacion() {
            return ocupacion;
        }

        /**
         * Define el valor de la propiedad ocupacion.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOcupacion(Integer value) {
            this.ocupacion = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursalOrigenBT.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSucursalOrigenBT() {
            return sucursalOrigenBT;
        }

        /**
         * Define el valor de la propiedad sucursalOrigenBT.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSucursalOrigenBT(Integer value) {
            this.sucursalOrigenBT = value;
        }

        /**
         * Obtiene el valor de la propiedad habilitacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHabilitacion() {
            return habilitacion;
        }

        /**
         * Define el valor de la propiedad habilitacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHabilitacion(String value) {
            this.habilitacion = value;
        }

        /**
         * Obtiene el valor de la propiedad area.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArea() {
            return area;
        }

        /**
         * Define el valor de la propiedad area.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArea(String value) {
            this.area = value;
        }

        /**
         * Obtiene el valor de la propiedad sector.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSector() {
            return sector;
        }

        /**
         * Define el valor de la propiedad sector.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSector(String value) {
            this.sector = value;
        }

        /**
         * Obtiene el valor de la propiedad limites.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaTarjetaReqType.Empresa.Limites }
         *     
         */
        public DataAltaTarjetaReqType.Empresa.Limites getLimites() {
            return limites;
        }

        /**
         * Define el valor de la propiedad limites.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaTarjetaReqType.Empresa.Limites }
         *     
         */
        public void setLimites(DataAltaTarjetaReqType.Empresa.Limites value) {
            this.limites = value;
        }

        /**
         * Obtiene el valor de la propiedad cuartaLinea.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuartaLinea() {
            return cuartaLinea;
        }

        /**
         * Define el valor de la propiedad cuartaLinea.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuartaLinea(String value) {
            this.cuartaLinea = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;choice>
         *           &lt;element name="cuentaClienteExistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *           &lt;element name="datosClienteNuevo">
         *             &lt;complexType>
         *               &lt;complexContent>
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                   &lt;sequence>
         *                     &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                     &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                     &lt;element name="Domicilio" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                               &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                               &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                               &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                               &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/sequence>
         *                 &lt;/restriction>
         *               &lt;/complexContent>
         *             &lt;/complexType>
         *           &lt;/element>
         *         &lt;/choice>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cuentaClienteExistente",
            "datosClienteNuevo"
        })
        public static class DatosCliente {

            protected String cuentaClienteExistente;
            protected DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo datosClienteNuevo;

            /**
             * Obtiene el valor de la propiedad cuentaClienteExistente.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaClienteExistente() {
                return cuentaClienteExistente;
            }

            /**
             * Define el valor de la propiedad cuentaClienteExistente.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaClienteExistente(String value) {
                this.cuentaClienteExistente = value;
            }

            /**
             * Obtiene el valor de la propiedad datosClienteNuevo.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo }
             *     
             */
            public DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo getDatosClienteNuevo() {
                return datosClienteNuevo;
            }

            /**
             * Define el valor de la propiedad datosClienteNuevo.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo }
             *     
             */
            public void setDatosClienteNuevo(DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo value) {
                this.datosClienteNuevo = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="Domicilio" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nombre",
                "apellido",
                "domicilio"
            })
            public static class DatosClienteNuevo {

                protected String nombre;
                protected String apellido;
                @XmlElement(name = "Domicilio")
                protected DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio domicilio;

                /**
                 * Obtiene el valor de la propiedad nombre.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNombre() {
                    return nombre;
                }

                /**
                 * Define el valor de la propiedad nombre.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNombre(String value) {
                    this.nombre = value;
                }

                /**
                 * Obtiene el valor de la propiedad apellido.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getApellido() {
                    return apellido;
                }

                /**
                 * Define el valor de la propiedad apellido.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setApellido(String value) {
                    this.apellido = value;
                }

                /**
                 * Obtiene el valor de la propiedad domicilio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio }
                 *     
                 */
                public DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio getDomicilio() {
                    return domicilio;
                }

                /**
                 * Define el valor de la propiedad domicilio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio }
                 *     
                 */
                public void setDomicilio(DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio value) {
                    this.domicilio = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="apartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="codigoPostalNuevo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "calle",
                    "numero",
                    "apartamento",
                    "piso",
                    "pais",
                    "localidad",
                    "provincia",
                    "codigoPostal",
                    "codigoPostalNuevo"
                })
                public static class Domicilio {

                    protected String calle;
                    protected String numero;
                    protected String apartamento;
                    protected String piso;
                    protected BigInteger pais;
                    protected BigInteger localidad;
                    protected BigInteger provincia;
                    protected String codigoPostal;
                    protected String codigoPostalNuevo;

                    /**
                     * Obtiene el valor de la propiedad calle.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCalle() {
                        return calle;
                    }

                    /**
                     * Define el valor de la propiedad calle.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCalle(String value) {
                        this.calle = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad numero.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNumero() {
                        return numero;
                    }

                    /**
                     * Define el valor de la propiedad numero.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNumero(String value) {
                        this.numero = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad apartamento.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getApartamento() {
                        return apartamento;
                    }

                    /**
                     * Define el valor de la propiedad apartamento.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setApartamento(String value) {
                        this.apartamento = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad piso.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPiso() {
                        return piso;
                    }

                    /**
                     * Define el valor de la propiedad piso.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPiso(String value) {
                        this.piso = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad pais.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getPais() {
                        return pais;
                    }

                    /**
                     * Define el valor de la propiedad pais.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setPais(BigInteger value) {
                        this.pais = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad localidad.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getLocalidad() {
                        return localidad;
                    }

                    /**
                     * Define el valor de la propiedad localidad.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setLocalidad(BigInteger value) {
                        this.localidad = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad provincia.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getProvincia() {
                        return provincia;
                    }

                    /**
                     * Define el valor de la propiedad provincia.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setProvincia(BigInteger value) {
                        this.provincia = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad codigoPostal.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCodigoPostal() {
                        return codigoPostal;
                    }

                    /**
                     * Define el valor de la propiedad codigoPostal.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCodigoPostal(String value) {
                        this.codigoPostal = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad codigoPostalNuevo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCodigoPostalNuevo() {
                        return codigoPostalNuevo;
                    }

                    /**
                     * Define el valor de la propiedad codigoPostalNuevo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCodigoPostalNuevo(String value) {
                        this.codigoPostalNuevo = value;
                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "operacion",
            "tipoSolicitud",
            "nroSolicitudGrupo",
            "nroSolicitud",
            "canal",
            "subCanal"
        })
        public static class IdentificadorSolicitud {

            protected int operacion;
            protected int tipoSolicitud;
            protected BigInteger nroSolicitudGrupo;
            protected BigInteger nroSolicitud;
            @XmlElement(required = true)
            protected BigInteger canal;
            @XmlElement(required = true)
            protected BigInteger subCanal;

            /**
             * Obtiene el valor de la propiedad operacion.
             * 
             */
            public int getOperacion() {
                return operacion;
            }

            /**
             * Define el valor de la propiedad operacion.
             * 
             */
            public void setOperacion(int value) {
                this.operacion = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoSolicitud.
             * 
             */
            public int getTipoSolicitud() {
                return tipoSolicitud;
            }

            /**
             * Define el valor de la propiedad tipoSolicitud.
             * 
             */
            public void setTipoSolicitud(int value) {
                this.tipoSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitudGrupo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitudGrupo() {
                return nroSolicitudGrupo;
            }

            /**
             * Define el valor de la propiedad nroSolicitudGrupo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitudGrupo(BigInteger value) {
                this.nroSolicitudGrupo = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitud() {
                return nroSolicitud;
            }

            /**
             * Define el valor de la propiedad nroSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitud(BigInteger value) {
                this.nroSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad canal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCanal() {
                return canal;
            }

            /**
             * Define el valor de la propiedad canal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCanal(BigInteger value) {
                this.canal = value;
            }

            /**
             * Obtiene el valor de la propiedad subCanal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubCanal() {
                return subCanal;
            }

            /**
             * Define el valor de la propiedad subCanal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubCanal(BigInteger value) {
                this.subCanal = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *         &lt;element name="limiteCuotas" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *         &lt;element name="limiteAdelante" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *         &lt;element name="limiteTransaccion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "limiteCompra",
            "limiteCuotas",
            "limiteAdelante",
            "limiteTransaccion"
        })
        public static class Limites {

            protected Double limiteCompra;
            protected Double limiteCuotas;
            protected Double limiteAdelante;
            protected Double limiteTransaccion;

            /**
             * Obtiene el valor de la propiedad limiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link Double }
             *     
             */
            public Double getLimiteCompra() {
                return limiteCompra;
            }

            /**
             * Define el valor de la propiedad limiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link Double }
             *     
             */
            public void setLimiteCompra(Double value) {
                this.limiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCuotas.
             * 
             * @return
             *     possible object is
             *     {@link Double }
             *     
             */
            public Double getLimiteCuotas() {
                return limiteCuotas;
            }

            /**
             * Define el valor de la propiedad limiteCuotas.
             * 
             * @param value
             *     allowed object is
             *     {@link Double }
             *     
             */
            public void setLimiteCuotas(Double value) {
                this.limiteCuotas = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteAdelante.
             * 
             * @return
             *     possible object is
             *     {@link Double }
             *     
             */
            public Double getLimiteAdelante() {
                return limiteAdelante;
            }

            /**
             * Define el valor de la propiedad limiteAdelante.
             * 
             * @param value
             *     allowed object is
             *     {@link Double }
             *     
             */
            public void setLimiteAdelante(Double value) {
                this.limiteAdelante = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteTransaccion.
             * 
             * @return
             *     possible object is
             *     {@link Double }
             *     
             */
            public Double getLimiteTransaccion() {
                return limiteTransaccion;
            }

            /**
             * Define el valor de la propiedad limiteTransaccion.
             * 
             * @param value
             *     allowed object is
             *     {@link Double }
             *     
             */
            public void setLimiteTransaccion(Double value) {
                this.limiteTransaccion = value;
            }

        }

    }

}
