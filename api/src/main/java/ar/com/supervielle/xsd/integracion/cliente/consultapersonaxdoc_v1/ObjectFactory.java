
package ar.com.supervielle.xsd.integracion.cliente.consultapersonaxdoc_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultapersonaxdoc_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultapersonaxdoc_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaPersonaxDocReqType }
     * 
     */
    public DataConsultaPersonaxDocReqType createDataConsultaPersonaxDocReqType() {
        return new DataConsultaPersonaxDocReqType();
    }

    /**
     * Create an instance of {@link DataConsultaPersonaxDocRespType }
     * 
     */
    public DataConsultaPersonaxDocRespType createDataConsultaPersonaxDocRespType() {
        return new DataConsultaPersonaxDocRespType();
    }

    /**
     * Create an instance of {@link RespConsultaPersonaxDoc }
     * 
     */
    public RespConsultaPersonaxDoc createRespConsultaPersonaxDoc() {
        return new RespConsultaPersonaxDoc();
    }

    /**
     * Create an instance of {@link ReqConsultaPersonaxDoc }
     * 
     */
    public ReqConsultaPersonaxDoc createReqConsultaPersonaxDoc() {
        return new ReqConsultaPersonaxDoc();
    }

    /**
     * Create an instance of {@link DataConsultaPersonaxDocReqType.Identificador }
     * 
     */
    public DataConsultaPersonaxDocReqType.Identificador createDataConsultaPersonaxDocReqTypeIdentificador() {
        return new DataConsultaPersonaxDocReqType.Identificador();
    }

    /**
     * Create an instance of {@link DataConsultaPersonaxDocRespType.Row }
     * 
     */
    public DataConsultaPersonaxDocRespType.Row createDataConsultaPersonaxDocRespTypeRow() {
        return new DataConsultaPersonaxDocRespType.Row();
    }

}
