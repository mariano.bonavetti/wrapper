
package ar.com.supervielle.xsd.integracion.tarjetacredito.actualizatarjetabatch_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataActualizaTarjetaBatchReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataActualizaTarjetaBatchReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoMarca">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodigoEntidad">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NumCuentaSocio">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodTipoNovedad">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodProdNuevo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ImportLimNuevo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="9"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PorceMod" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SignoMod" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodFormaPago" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodTipoDebito" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodSucBco" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodTipoCtaBcaria" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NumCtaBcaria" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodGrupAfinidad" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodModLiquidacion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PorcBonfPeriodo" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodGrupCtaCte" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;totalDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CuotasServicio" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PorLimAdelantado" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MantieneBonf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MarcaHabATM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CodLimPre" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FechaEfectiva" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataActualizaTarjetaBatchReq", propOrder = {
    "codigoMarca",
    "codigoEntidad",
    "numCuentaSocio",
    "codTipoNovedad",
    "codProdNuevo",
    "importLimNuevo",
    "porceMod",
    "signoMod",
    "codFormaPago",
    "codTipoDebito",
    "codSucBco",
    "codTipoCtaBcaria",
    "numCtaBcaria",
    "codGrupAfinidad",
    "codModLiquidacion",
    "porcBonfPeriodo",
    "codGrupCtaCte",
    "cuotasServicio",
    "porLimAdelantado",
    "mantieneBonf",
    "marcaHabATM",
    "codLimPre",
    "fechaEfectiva"
})
public class DataActualizaTarjetaBatchReq {

    @XmlElement(name = "CodigoMarca")
    protected int codigoMarca;
    @XmlElement(name = "CodigoEntidad")
    protected int codigoEntidad;
    @XmlElement(name = "NumCuentaSocio", required = true)
    protected String numCuentaSocio;
    @XmlElement(name = "CodTipoNovedad")
    protected int codTipoNovedad;
    @XmlElement(name = "CodProdNuevo")
    protected Integer codProdNuevo;
    @XmlElement(name = "ImportLimNuevo")
    protected Integer importLimNuevo;
    @XmlElement(name = "PorceMod")
    protected Integer porceMod;
    @XmlElement(name = "SignoMod")
    protected String signoMod;
    @XmlElement(name = "CodFormaPago")
    protected String codFormaPago;
    @XmlElement(name = "CodTipoDebito")
    protected Integer codTipoDebito;
    @XmlElement(name = "CodSucBco")
    protected Integer codSucBco;
    @XmlElement(name = "CodTipoCtaBcaria")
    protected String codTipoCtaBcaria;
    @XmlElement(name = "NumCtaBcaria")
    protected Integer numCtaBcaria;
    @XmlElement(name = "CodGrupAfinidad")
    protected Integer codGrupAfinidad;
    @XmlElement(name = "CodModLiquidacion")
    protected Integer codModLiquidacion;
    @XmlElement(name = "PorcBonfPeriodo")
    protected String porcBonfPeriodo;
    @XmlElement(name = "CodGrupCtaCte")
    protected Integer codGrupCtaCte;
    @XmlElement(name = "CuotasServicio")
    protected String cuotasServicio;
    @XmlElement(name = "PorLimAdelantado")
    protected String porLimAdelantado;
    @XmlElement(name = "MantieneBonf")
    protected String mantieneBonf;
    @XmlElement(name = "MarcaHabATM")
    protected String marcaHabATM;
    @XmlElement(name = "CodLimPre")
    protected String codLimPre;
    @XmlElement(name = "FechaEfectiva", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaEfectiva;

    /**
     * Obtiene el valor de la propiedad codigoMarca.
     * 
     */
    public int getCodigoMarca() {
        return codigoMarca;
    }

    /**
     * Define el valor de la propiedad codigoMarca.
     * 
     */
    public void setCodigoMarca(int value) {
        this.codigoMarca = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoEntidad.
     * 
     */
    public int getCodigoEntidad() {
        return codigoEntidad;
    }

    /**
     * Define el valor de la propiedad codigoEntidad.
     * 
     */
    public void setCodigoEntidad(int value) {
        this.codigoEntidad = value;
    }

    /**
     * Obtiene el valor de la propiedad numCuentaSocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCuentaSocio() {
        return numCuentaSocio;
    }

    /**
     * Define el valor de la propiedad numCuentaSocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCuentaSocio(String value) {
        this.numCuentaSocio = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoNovedad.
     * 
     */
    public int getCodTipoNovedad() {
        return codTipoNovedad;
    }

    /**
     * Define el valor de la propiedad codTipoNovedad.
     * 
     */
    public void setCodTipoNovedad(int value) {
        this.codTipoNovedad = value;
    }

    /**
     * Obtiene el valor de la propiedad codProdNuevo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodProdNuevo() {
        return codProdNuevo;
    }

    /**
     * Define el valor de la propiedad codProdNuevo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodProdNuevo(Integer value) {
        this.codProdNuevo = value;
    }

    /**
     * Obtiene el valor de la propiedad importLimNuevo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImportLimNuevo() {
        return importLimNuevo;
    }

    /**
     * Define el valor de la propiedad importLimNuevo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImportLimNuevo(Integer value) {
        this.importLimNuevo = value;
    }

    /**
     * Obtiene el valor de la propiedad porceMod.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPorceMod() {
        return porceMod;
    }

    /**
     * Define el valor de la propiedad porceMod.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPorceMod(Integer value) {
        this.porceMod = value;
    }

    /**
     * Obtiene el valor de la propiedad signoMod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignoMod() {
        return signoMod;
    }

    /**
     * Define el valor de la propiedad signoMod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignoMod(String value) {
        this.signoMod = value;
    }

    /**
     * Obtiene el valor de la propiedad codFormaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFormaPago() {
        return codFormaPago;
    }

    /**
     * Define el valor de la propiedad codFormaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFormaPago(String value) {
        this.codFormaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDebito.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodTipoDebito() {
        return codTipoDebito;
    }

    /**
     * Define el valor de la propiedad codTipoDebito.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodTipoDebito(Integer value) {
        this.codTipoDebito = value;
    }

    /**
     * Obtiene el valor de la propiedad codSucBco.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodSucBco() {
        return codSucBco;
    }

    /**
     * Define el valor de la propiedad codSucBco.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodSucBco(Integer value) {
        this.codSucBco = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoCtaBcaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoCtaBcaria() {
        return codTipoCtaBcaria;
    }

    /**
     * Define el valor de la propiedad codTipoCtaBcaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoCtaBcaria(String value) {
        this.codTipoCtaBcaria = value;
    }

    /**
     * Obtiene el valor de la propiedad numCtaBcaria.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumCtaBcaria() {
        return numCtaBcaria;
    }

    /**
     * Define el valor de la propiedad numCtaBcaria.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumCtaBcaria(Integer value) {
        this.numCtaBcaria = value;
    }

    /**
     * Obtiene el valor de la propiedad codGrupAfinidad.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodGrupAfinidad() {
        return codGrupAfinidad;
    }

    /**
     * Define el valor de la propiedad codGrupAfinidad.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodGrupAfinidad(Integer value) {
        this.codGrupAfinidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codModLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodModLiquidacion() {
        return codModLiquidacion;
    }

    /**
     * Define el valor de la propiedad codModLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodModLiquidacion(Integer value) {
        this.codModLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad porcBonfPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPorcBonfPeriodo() {
        return porcBonfPeriodo;
    }

    /**
     * Define el valor de la propiedad porcBonfPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorcBonfPeriodo(String value) {
        this.porcBonfPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad codGrupCtaCte.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodGrupCtaCte() {
        return codGrupCtaCte;
    }

    /**
     * Define el valor de la propiedad codGrupCtaCte.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodGrupCtaCte(Integer value) {
        this.codGrupCtaCte = value;
    }

    /**
     * Obtiene el valor de la propiedad cuotasServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuotasServicio() {
        return cuotasServicio;
    }

    /**
     * Define el valor de la propiedad cuotasServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuotasServicio(String value) {
        this.cuotasServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad porLimAdelantado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPorLimAdelantado() {
        return porLimAdelantado;
    }

    /**
     * Define el valor de la propiedad porLimAdelantado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorLimAdelantado(String value) {
        this.porLimAdelantado = value;
    }

    /**
     * Obtiene el valor de la propiedad mantieneBonf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMantieneBonf() {
        return mantieneBonf;
    }

    /**
     * Define el valor de la propiedad mantieneBonf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMantieneBonf(String value) {
        this.mantieneBonf = value;
    }

    /**
     * Obtiene el valor de la propiedad marcaHabATM.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarcaHabATM() {
        return marcaHabATM;
    }

    /**
     * Define el valor de la propiedad marcaHabATM.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarcaHabATM(String value) {
        this.marcaHabATM = value;
    }

    /**
     * Obtiene el valor de la propiedad codLimPre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodLimPre() {
        return codLimPre;
    }

    /**
     * Define el valor de la propiedad codLimPre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodLimPre(String value) {
        this.codLimPre = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaEfectiva.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaEfectiva() {
        return fechaEfectiva;
    }

    /**
     * Define el valor de la propiedad fechaEfectiva.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaEfectiva(XMLGregorianCalendar value) {
        this.fechaEfectiva = value;
    }

}
