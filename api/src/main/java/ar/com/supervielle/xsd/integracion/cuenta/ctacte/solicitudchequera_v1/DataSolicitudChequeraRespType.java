
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.solicitudchequera_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdSolicitudChequeraType;


/**
 * <p>Clase Java para DataSolicitudChequeraRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataSolicitudChequeraRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdSolicitudChequeraType"/>
 *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cantCheques" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSolicitudChequeraRespType", propOrder = {
    "row"
})
public class DataSolicitudChequeraRespType {

    @XmlElement(name = "Row")
    protected DataSolicitudChequeraRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataSolicitudChequeraRespType.Row }
     *     
     */
    public DataSolicitudChequeraRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataSolicitudChequeraRespType.Row }
     *     
     */
    public void setRow(DataSolicitudChequeraRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdSolicitudChequeraType"/>
     *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cantCheques" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "nroSolicitud",
        "cantCheques"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdSolicitudChequeraType identificador;
        @XmlElement(required = true)
        protected String nroSolicitud;
        protected Integer cantCheques;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdSolicitudChequeraType }
         *     
         */
        public IdSolicitudChequeraType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdSolicitudChequeraType }
         *     
         */
        public void setIdentificador(IdSolicitudChequeraType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad nroSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNroSolicitud() {
            return nroSolicitud;
        }

        /**
         * Define el valor de la propiedad nroSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNroSolicitud(String value) {
            this.nroSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad cantCheques.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCantCheques() {
            return cantCheques;
        }

        /**
         * Define el valor de la propiedad cantCheques.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCantCheques(Integer value) {
            this.cantCheques = value;
        }

    }

}
