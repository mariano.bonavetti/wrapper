
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultarepositorioextracto_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultarepositorioextracto_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultarepositorioextracto_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaRepositorioExtractoReqType }
     * 
     */
    public DataConsultaRepositorioExtractoReqType createDataConsultaRepositorioExtractoReqType() {
        return new DataConsultaRepositorioExtractoReqType();
    }

    /**
     * Create an instance of {@link DataConsultaRepositorioExtractoRespType }
     * 
     */
    public DataConsultaRepositorioExtractoRespType createDataConsultaRepositorioExtractoRespType() {
        return new DataConsultaRepositorioExtractoRespType();
    }

    /**
     * Create an instance of {@link RespConsultaRepositorioExtracto }
     * 
     */
    public RespConsultaRepositorioExtracto createRespConsultaRepositorioExtracto() {
        return new RespConsultaRepositorioExtracto();
    }

    /**
     * Create an instance of {@link ReqConsultaRepositorioExtracto }
     * 
     */
    public ReqConsultaRepositorioExtracto createReqConsultaRepositorioExtracto() {
        return new ReqConsultaRepositorioExtracto();
    }

    /**
     * Create an instance of {@link DataConsultaRepositorioExtractoReqType.Identificador }
     * 
     */
    public DataConsultaRepositorioExtractoReqType.Identificador createDataConsultaRepositorioExtractoReqTypeIdentificador() {
        return new DataConsultaRepositorioExtractoReqType.Identificador();
    }

    /**
     * Create an instance of {@link DataConsultaRepositorioExtractoRespType.Row }
     * 
     */
    public DataConsultaRepositorioExtractoRespType.Row createDataConsultaRepositorioExtractoRespTypeRow() {
        return new DataConsultaRepositorioExtractoRespType.Row();
    }

}
