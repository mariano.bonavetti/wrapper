
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacobrospendientes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaCobrosPendientesReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCobrosPendientesReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificador">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="papel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCobrosPendientesReqType", propOrder = {
    "identificador"
})
public class DataConsultaCobrosPendientesReqType {

    @XmlElement(name = "Identificador", required = true)
    protected DataConsultaCobrosPendientesReqType.Identificador identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaCobrosPendientesReqType.Identificador }
     *     
     */
    public DataConsultaCobrosPendientesReqType.Identificador getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaCobrosPendientesReqType.Identificador }
     *     
     */
    public void setIdentificador(DataConsultaCobrosPendientesReqType.Identificador value) {
        this.identificador = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="papel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "empresa",
        "modulo",
        "sucursal",
        "moneda",
        "papel",
        "cuenta",
        "subOperacion"
    })
    public static class Identificador {

        @XmlElement(required = true)
        protected String empresa;
        @XmlElement(required = true)
        protected String modulo;
        @XmlElement(required = true)
        protected String sucursal;
        @XmlElement(required = true)
        protected String moneda;
        @XmlElement(required = true)
        protected String papel;
        @XmlElement(required = true)
        protected String cuenta;
        @XmlElement(required = true)
        protected String subOperacion;

        /**
         * Obtiene el valor de la propiedad empresa.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmpresa() {
            return empresa;
        }

        /**
         * Define el valor de la propiedad empresa.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmpresa(String value) {
            this.empresa = value;
        }

        /**
         * Obtiene el valor de la propiedad modulo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModulo() {
            return modulo;
        }

        /**
         * Define el valor de la propiedad modulo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModulo(String value) {
            this.modulo = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSucursal(String value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoneda(String value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad papel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPapel() {
            return papel;
        }

        /**
         * Define el valor de la propiedad papel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPapel(String value) {
            this.papel = value;
        }

        /**
         * Obtiene el valor de la propiedad cuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuenta() {
            return cuenta;
        }

        /**
         * Define el valor de la propiedad cuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuenta(String value) {
            this.cuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad subOperacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubOperacion() {
            return subOperacion;
        }

        /**
         * Define el valor de la propiedad subOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubOperacion(String value) {
            this.subOperacion = value;
        }

    }

}
