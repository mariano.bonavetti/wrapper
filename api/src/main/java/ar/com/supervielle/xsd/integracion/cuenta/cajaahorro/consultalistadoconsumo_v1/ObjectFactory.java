
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistadoconsumo_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistadoconsumo_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistadoconsumo_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaListadoCONRespType }
     * 
     */
    public DataConsultaListadoCONRespType createDataConsultaListadoCONRespType() {
        return new DataConsultaListadoCONRespType();
    }

    /**
     * Create an instance of {@link RespConsultaListadoCON }
     * 
     */
    public RespConsultaListadoCON createRespConsultaListadoCON() {
        return new RespConsultaListadoCON();
    }

    /**
     * Create an instance of {@link ReqConsultaListadoCON }
     * 
     */
    public ReqConsultaListadoCON createReqConsultaListadoCON() {
        return new ReqConsultaListadoCON();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCONReqType }
     * 
     */
    public DataConsultaListadoCONReqType createDataConsultaListadoCONReqType() {
        return new DataConsultaListadoCONReqType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoCONRespType.Row }
     * 
     */
    public DataConsultaListadoCONRespType.Row createDataConsultaListadoCONRespTypeRow() {
        return new DataConsultaListadoCONRespType.Row();
    }

}
