
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EmpresaPMC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EmpresaPMC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdBanco" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datoAdicional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datosAyudaEmpresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}DatosAyudaEmpresa" minOccurs="0"/>
 *         &lt;element name="datosEBPPBean" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}DatosEBPP" minOccurs="0"/>
 *         &lt;element name="importePermitido" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="info" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}MonedaPMC" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orden" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="permitePagosRecurrentes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="permiteUsd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="rubro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdRubro" minOccurs="0"/>
 *         &lt;element name="soloConsulta" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tipoAdhesion" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tipoEmpresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
 *         &lt;element name="tipoPago" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tituloIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmpresaPMC", propOrder = {
    "banco",
    "codigo",
    "datoAdicional",
    "datosAyudaEmpresa",
    "datosEBPPBean",
    "importePermitido",
    "info",
    "moneda",
    "nombre",
    "orden",
    "permitePagosRecurrentes",
    "permiteUsd",
    "rubro",
    "soloConsulta",
    "tipoAdhesion",
    "tipoEmpresa",
    "tipoPago",
    "tituloIdentificacion"
})
public class EmpresaPMC {

    protected IdBanco banco;
    protected String codigo;
    protected String datoAdicional;
    protected DatosAyudaEmpresa datosAyudaEmpresa;
    protected DatosEBPP datosEBPPBean;
    protected Integer importePermitido;
    protected String info;
    protected MonedaPMC moneda;
    protected String nombre;
    protected Integer orden;
    protected Boolean permitePagosRecurrentes;
    protected Boolean permiteUsd;
    protected IdRubro rubro;
    protected Boolean soloConsulta;
    protected EmpresaPMC.TipoAdhesion tipoAdhesion;
    protected CodDescNumType tipoEmpresa;
    protected EmpresaPMC.TipoPago tipoPago;
    protected String tituloIdentificacion;

    /**
     * Obtiene el valor de la propiedad banco.
     * 
     * @return
     *     possible object is
     *     {@link IdBanco }
     *     
     */
    public IdBanco getBanco() {
        return banco;
    }

    /**
     * Define el valor de la propiedad banco.
     * 
     * @param value
     *     allowed object is
     *     {@link IdBanco }
     *     
     */
    public void setBanco(IdBanco value) {
        this.banco = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad datoAdicional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatoAdicional() {
        return datoAdicional;
    }

    /**
     * Define el valor de la propiedad datoAdicional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatoAdicional(String value) {
        this.datoAdicional = value;
    }

    /**
     * Obtiene el valor de la propiedad datosAyudaEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link DatosAyudaEmpresa }
     *     
     */
    public DatosAyudaEmpresa getDatosAyudaEmpresa() {
        return datosAyudaEmpresa;
    }

    /**
     * Define el valor de la propiedad datosAyudaEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosAyudaEmpresa }
     *     
     */
    public void setDatosAyudaEmpresa(DatosAyudaEmpresa value) {
        this.datosAyudaEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad datosEBPPBean.
     * 
     * @return
     *     possible object is
     *     {@link DatosEBPP }
     *     
     */
    public DatosEBPP getDatosEBPPBean() {
        return datosEBPPBean;
    }

    /**
     * Define el valor de la propiedad datosEBPPBean.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosEBPP }
     *     
     */
    public void setDatosEBPPBean(DatosEBPP value) {
        this.datosEBPPBean = value;
    }

    /**
     * Obtiene el valor de la propiedad importePermitido.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImportePermitido() {
        return importePermitido;
    }

    /**
     * Define el valor de la propiedad importePermitido.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImportePermitido(Integer value) {
        this.importePermitido = value;
    }

    /**
     * Obtiene el valor de la propiedad info.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo() {
        return info;
    }

    /**
     * Define el valor de la propiedad info.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo(String value) {
        this.info = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link MonedaPMC }
     *     
     */
    public MonedaPMC getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link MonedaPMC }
     *     
     */
    public void setMoneda(MonedaPMC value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad orden.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrden() {
        return orden;
    }

    /**
     * Define el valor de la propiedad orden.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrden(Integer value) {
        this.orden = value;
    }

    /**
     * Obtiene el valor de la propiedad permitePagosRecurrentes.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPermitePagosRecurrentes() {
        return permitePagosRecurrentes;
    }

    /**
     * Define el valor de la propiedad permitePagosRecurrentes.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPermitePagosRecurrentes(Boolean value) {
        this.permitePagosRecurrentes = value;
    }

    /**
     * Obtiene el valor de la propiedad permiteUsd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPermiteUsd() {
        return permiteUsd;
    }

    /**
     * Define el valor de la propiedad permiteUsd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPermiteUsd(Boolean value) {
        this.permiteUsd = value;
    }

    /**
     * Obtiene el valor de la propiedad rubro.
     * 
     * @return
     *     possible object is
     *     {@link IdRubro }
     *     
     */
    public IdRubro getRubro() {
        return rubro;
    }

    /**
     * Define el valor de la propiedad rubro.
     * 
     * @param value
     *     allowed object is
     *     {@link IdRubro }
     *     
     */
    public void setRubro(IdRubro value) {
        this.rubro = value;
    }

    /**
     * Obtiene el valor de la propiedad soloConsulta.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSoloConsulta() {
        return soloConsulta;
    }

    /**
     * Define el valor de la propiedad soloConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSoloConsulta(Boolean value) {
        this.soloConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoAdhesion.
     * 
     * @return
     *     possible object is
     *     {@link EmpresaPMC.TipoAdhesion }
     *     
     */
    public EmpresaPMC.TipoAdhesion getTipoAdhesion() {
        return tipoAdhesion;
    }

    /**
     * Define el valor de la propiedad tipoAdhesion.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpresaPMC.TipoAdhesion }
     *     
     */
    public void setTipoAdhesion(EmpresaPMC.TipoAdhesion value) {
        this.tipoAdhesion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link CodDescNumType }
     *     
     */
    public CodDescNumType getTipoEmpresa() {
        return tipoEmpresa;
    }

    /**
     * Define el valor de la propiedad tipoEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescNumType }
     *     
     */
    public void setTipoEmpresa(CodDescNumType value) {
        this.tipoEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPago.
     * 
     * @return
     *     possible object is
     *     {@link EmpresaPMC.TipoPago }
     *     
     */
    public EmpresaPMC.TipoPago getTipoPago() {
        return tipoPago;
    }

    /**
     * Define el valor de la propiedad tipoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpresaPMC.TipoPago }
     *     
     */
    public void setTipoPago(EmpresaPMC.TipoPago value) {
        this.tipoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad tituloIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTituloIdentificacion() {
        return tituloIdentificacion;
    }

    /**
     * Define el valor de la propiedad tituloIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTituloIdentificacion(String value) {
        this.tituloIdentificacion = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codigo"
    })
    public static class TipoAdhesion {

        protected Integer codigo;

        /**
         * Obtiene el valor de la propiedad codigo.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodigo() {
            return codigo;
        }

        /**
         * Define el valor de la propiedad codigo.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodigo(Integer value) {
            this.codigo = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codigo"
    })
    public static class TipoPago {

        protected Integer codigo;

        /**
         * Obtiene el valor de la propiedad codigo.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodigo() {
            return codigo;
        }

        /**
         * Define el valor de la propiedad codigo.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodigo(Integer value) {
            this.codigo = value;
        }

    }

}
