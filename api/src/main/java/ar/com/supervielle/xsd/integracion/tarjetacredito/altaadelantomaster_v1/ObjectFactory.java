
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaadelantomaster_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altaadelantomaster_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altaadelantomaster_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaAdelantoMasterReqType }
     * 
     */
    public DataAltaAdelantoMasterReqType createDataAltaAdelantoMasterReqType() {
        return new DataAltaAdelantoMasterReqType();
    }

    /**
     * Create an instance of {@link DataAltaAdelantoMasterReqType.Principal }
     * 
     */
    public DataAltaAdelantoMasterReqType.Principal createDataAltaAdelantoMasterReqTypePrincipal() {
        return new DataAltaAdelantoMasterReqType.Principal();
    }

    /**
     * Create an instance of {@link DataAltaAdelantoMasterRespType }
     * 
     */
    public DataAltaAdelantoMasterRespType createDataAltaAdelantoMasterRespType() {
        return new DataAltaAdelantoMasterRespType();
    }

    /**
     * Create an instance of {@link DataAltaAdelantoMasterRespType.Row }
     * 
     */
    public DataAltaAdelantoMasterRespType.Row createDataAltaAdelantoMasterRespTypeRow() {
        return new DataAltaAdelantoMasterRespType.Row();
    }

    /**
     * Create an instance of {@link RespAltaAdelantoMaster }
     * 
     */
    public RespAltaAdelantoMaster createRespAltaAdelantoMaster() {
        return new RespAltaAdelantoMaster();
    }

    /**
     * Create an instance of {@link ReqAltaAdelantoMaster }
     * 
     */
    public ReqAltaAdelantoMaster createReqAltaAdelantoMaster() {
        return new ReqAltaAdelantoMaster();
    }

    /**
     * Create an instance of {@link DataAltaAdelantoMasterReqType.Principal.DatosTarjeta }
     * 
     */
    public DataAltaAdelantoMasterReqType.Principal.DatosTarjeta createDataAltaAdelantoMasterReqTypePrincipalDatosTarjeta() {
        return new DataAltaAdelantoMasterReqType.Principal.DatosTarjeta();
    }

    /**
     * Create an instance of {@link DataAltaAdelantoMasterRespType.Row.Error }
     * 
     */
    public DataAltaAdelantoMasterRespType.Row.Error createDataAltaAdelantoMasterRespTypeRowError() {
        return new DataAltaAdelantoMasterRespType.Row.Error();
    }

}
