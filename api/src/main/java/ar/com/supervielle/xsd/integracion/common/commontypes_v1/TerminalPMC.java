
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TerminalPMC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TerminalPMC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="futureUse1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="futureUse2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idSession" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ipOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TerminalPMC", propOrder = {
    "terminal",
    "futureUse1",
    "futureUse2",
    "idSession",
    "ipOrigen"
})
public class TerminalPMC {

    protected String terminal;
    protected String futureUse1;
    protected String futureUse2;
    protected String idSession;
    protected String ipOrigen;

    /**
     * Obtiene el valor de la propiedad terminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminal() {
        return terminal;
    }

    /**
     * Define el valor de la propiedad terminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminal(String value) {
        this.terminal = value;
    }

    /**
     * Obtiene el valor de la propiedad futureUse1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFutureUse1() {
        return futureUse1;
    }

    /**
     * Define el valor de la propiedad futureUse1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFutureUse1(String value) {
        this.futureUse1 = value;
    }

    /**
     * Obtiene el valor de la propiedad futureUse2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFutureUse2() {
        return futureUse2;
    }

    /**
     * Define el valor de la propiedad futureUse2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFutureUse2(String value) {
        this.futureUse2 = value;
    }

    /**
     * Obtiene el valor de la propiedad idSession.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSession() {
        return idSession;
    }

    /**
     * Define el valor de la propiedad idSession.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSession(String value) {
        this.idSession = value;
    }

    /**
     * Obtiene el valor de la propiedad ipOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpOrigen() {
        return ipOrigen;
    }

    /**
     * Define el valor de la propiedad ipOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpOrigen(String value) {
        this.ipOrigen = value;
    }

}
