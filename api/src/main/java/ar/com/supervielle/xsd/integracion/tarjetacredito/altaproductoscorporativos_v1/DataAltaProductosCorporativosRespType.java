
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaproductoscorporativos_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;


/**
 * <p>Clase Java para DataAltaProductosCorporativosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaProductosCorporativosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SolicitudesGeneradas">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="Solicitud" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                       &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="Resultado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaProductosCorporativosRespType", propOrder = {
    "row"
})
public class DataAltaProductosCorporativosRespType {

    @XmlElement(name = "Row")
    protected List<DataAltaProductosCorporativosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataAltaProductosCorporativosRespType.Row }
     * 
     * 
     */
    public List<DataAltaProductosCorporativosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataAltaProductosCorporativosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SolicitudesGeneradas">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="Solicitud" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="Resultado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "solicitudesGeneradas"
    })
    public static class Row {

        @XmlElement(name = "SolicitudesGeneradas", required = true)
        protected DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas solicitudesGeneradas;

        /**
         * Obtiene el valor de la propiedad solicitudesGeneradas.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas }
         *     
         */
        public DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas getSolicitudesGeneradas() {
            return solicitudesGeneradas;
        }

        /**
         * Define el valor de la propiedad solicitudesGeneradas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas }
         *     
         */
        public void setSolicitudesGeneradas(DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas value) {
            this.solicitudesGeneradas = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="Solicitud" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="Resultado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nroSolicitudGrupo",
            "solicitud"
        })
        public static class SolicitudesGeneradas {

            @XmlElement(required = true)
            protected BigInteger nroSolicitudGrupo;
            @XmlElement(name = "Solicitud", required = true)
            protected List<DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud> solicitud;

            /**
             * Obtiene el valor de la propiedad nroSolicitudGrupo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitudGrupo() {
                return nroSolicitudGrupo;
            }

            /**
             * Define el valor de la propiedad nroSolicitudGrupo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitudGrupo(BigInteger value) {
                this.nroSolicitudGrupo = value;
            }

            /**
             * Gets the value of the solicitud property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the solicitud property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSolicitud().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud }
             * 
             * 
             */
            public List<DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud> getSolicitud() {
                if (solicitud == null) {
                    solicitud = new ArrayList<DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud>();
                }
                return this.solicitud;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="nroReferencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="Resultado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "numero",
                "tipo",
                "nroReferencia",
                "resultado"
            })
            public static class Solicitud {

                protected BigInteger numero;
                @XmlElement(required = true)
                protected String tipo;
                protected String nroReferencia;
                @XmlElement(name = "Resultado", required = true)
                protected CodDescNumType resultado;

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNumero(BigInteger value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTipo() {
                    return tipo;
                }

                /**
                 * Define el valor de la propiedad tipo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTipo(String value) {
                    this.tipo = value;
                }

                /**
                 * Obtiene el valor de la propiedad nroReferencia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNroReferencia() {
                    return nroReferencia;
                }

                /**
                 * Define el valor de la propiedad nroReferencia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNroReferencia(String value) {
                    this.nroReferencia = value;
                }

                /**
                 * Obtiene el valor de la propiedad resultado.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getResultado() {
                    return resultado;
                }

                /**
                 * Define el valor de la propiedad resultado.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setResultado(CodDescNumType value) {
                    this.resultado = value;
                }

            }

        }

    }

}
