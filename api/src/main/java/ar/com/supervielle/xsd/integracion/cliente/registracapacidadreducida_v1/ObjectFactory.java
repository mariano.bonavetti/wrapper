
package ar.com.supervielle.xsd.integracion.cliente.registracapacidadreducida_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.registracapacidadreducida_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.registracapacidadreducida_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataRegistraCapacidadReducidaRespType }
     * 
     */
    public DataRegistraCapacidadReducidaRespType createDataRegistraCapacidadReducidaRespType() {
        return new DataRegistraCapacidadReducidaRespType();
    }

    /**
     * Create an instance of {@link RespRegistraCapacidadReducida }
     * 
     */
    public RespRegistraCapacidadReducida createRespRegistraCapacidadReducida() {
        return new RespRegistraCapacidadReducida();
    }

    /**
     * Create an instance of {@link ReqRegistraCapacidadReducida }
     * 
     */
    public ReqRegistraCapacidadReducida createReqRegistraCapacidadReducida() {
        return new ReqRegistraCapacidadReducida();
    }

    /**
     * Create an instance of {@link DataRegistraCapacidadReducidaReqType }
     * 
     */
    public DataRegistraCapacidadReducidaReqType createDataRegistraCapacidadReducidaReqType() {
        return new DataRegistraCapacidadReducidaReqType();
    }

    /**
     * Create an instance of {@link DataRegistraCapacidadReducidaRespType.Row }
     * 
     */
    public DataRegistraCapacidadReducidaRespType.Row createDataRegistraCapacidadReducidaRespTypeRow() {
        return new DataRegistraCapacidadReducidaRespType.Row();
    }

}
