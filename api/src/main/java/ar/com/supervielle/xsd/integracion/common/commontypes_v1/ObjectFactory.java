
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.common.commontypes_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Order_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1", "Order");
    private final static QName _Paging_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1", "Paging");
    private final static QName _RubroDTOFiid_QNAME = new QName("", "fiid");
    private final static QName _RubroDTORubro_QNAME = new QName("", "rubro");
    private final static QName _RubroDTOTipoDeRubro_QNAME = new QName("", "tipoDeRubro");
    private final static QName _IdRubroCodigo_QNAME = new QName("", "codigo");
    private final static QName _IdRubroTipo_QNAME = new QName("", "tipo");
    private final static QName _IdRubroTipoRubro_QNAME = new QName("", "tipoRubro");
    private final static QName _IdRubroNombre_QNAME = new QName("", "nombre");
    private final static QName _IdRubroInfo_QNAME = new QName("", "info");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.common.commontypes_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IdListadoTypeV1 }
     * 
     */
    public IdListadoTypeV1 createIdListadoTypeV1() {
        return new IdListadoTypeV1();
    }

    /**
     * Create an instance of {@link FechaGregorianCalendar }
     * 
     */
    public FechaGregorianCalendar createFechaGregorianCalendar() {
        return new FechaGregorianCalendar();
    }

    /**
     * Create an instance of {@link IdComprobante }
     * 
     */
    public IdComprobante createIdComprobante() {
        return new IdComprobante();
    }

    /**
     * Create an instance of {@link IdComprobante.Opcionales }
     * 
     */
    public IdComprobante.Opcionales createIdComprobanteOpcionales() {
        return new IdComprobante.Opcionales();
    }

    /**
     * Create an instance of {@link IdListadoType }
     * 
     */
    public IdListadoType createIdListadoType() {
        return new IdListadoType();
    }

    /**
     * Create an instance of {@link ResumenTCV11 }
     * 
     */
    public ResumenTCV11 createResumenTCV11() {
        return new ResumenTCV11();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DetalleMovimientos }
     * 
     */
    public ResumenTCV11 .DetalleMovimientos createResumenTCV11DetalleMovimientos() {
        return new ResumenTCV11 .DetalleMovimientos();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DetalleMovimientos.Movimiento }
     * 
     */
    public ResumenTCV11 .DetalleMovimientos.Movimiento createResumenTCV11DetalleMovimientosMovimiento() {
        return new ResumenTCV11 .DetalleMovimientos.Movimiento();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta }
     * 
     */
    public ResumenTCV11 .DatosCuenta createResumenTCV11DatosCuenta() {
        return new ResumenTCV11 .DatosCuenta();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Tasas }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Tasas createResumenTCV11DatosCuentaTasas() {
        return new ResumenTCV11 .DatosCuenta.Tasas();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Saldos }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Saldos createResumenTCV11DatosCuentaSaldos() {
        return new ResumenTCV11 .DatosCuenta.Saldos();
    }

    /**
     * Create an instance of {@link EmpresaBasePMC }
     * 
     */
    public EmpresaBasePMC createEmpresaBasePMC() {
        return new EmpresaBasePMC();
    }

    /**
     * Create an instance of {@link OperacionTC }
     * 
     */
    public OperacionTC createOperacionTC() {
        return new OperacionTC();
    }

    /**
     * Create an instance of {@link OperacionTC.Movimientos }
     * 
     */
    public OperacionTC.Movimientos createOperacionTCMovimientos() {
        return new OperacionTC.Movimientos();
    }

    /**
     * Create an instance of {@link OperacionTC.Movimientos.Movimiento }
     * 
     */
    public OperacionTC.Movimientos.Movimiento createOperacionTCMovimientosMovimiento() {
        return new OperacionTC.Movimientos.Movimiento();
    }

    /**
     * Create an instance of {@link EstadoType }
     * 
     */
    public EstadoType createEstadoType() {
        return new EstadoType();
    }

    /**
     * Create an instance of {@link IdPersonaType }
     * 
     */
    public IdPersonaType createIdPersonaType() {
        return new IdPersonaType();
    }

    /**
     * Create an instance of {@link EmpresaPMC }
     * 
     */
    public EmpresaPMC createEmpresaPMC() {
        return new EmpresaPMC();
    }

    /**
     * Create an instance of {@link ResumenTC }
     * 
     */
    public ResumenTC createResumenTC() {
        return new ResumenTC();
    }

    /**
     * Create an instance of {@link ResumenTC.Saldos }
     * 
     */
    public ResumenTC.Saldos createResumenTCSaldos() {
        return new ResumenTC.Saldos();
    }

    /**
     * Create an instance of {@link ResumenTC.Fechas }
     * 
     */
    public ResumenTC.Fechas createResumenTCFechas() {
        return new ResumenTC.Fechas();
    }

    /**
     * Create an instance of {@link OrderType }
     * 
     */
    public OrderType createOrderType() {
        return new OrderType();
    }

    /**
     * Create an instance of {@link PagingType }
     * 
     */
    public PagingType createPagingType() {
        return new PagingType();
    }

    /**
     * Create an instance of {@link IdRubro }
     * 
     */
    public IdRubro createIdRubro() {
        return new IdRubro();
    }

    /**
     * Create an instance of {@link TipoCuentaPMC }
     * 
     */
    public TipoCuentaPMC createTipoCuentaPMC() {
        return new TipoCuentaPMC();
    }

    /**
     * Create an instance of {@link PeriodoBean }
     * 
     */
    public PeriodoBean createPeriodoBean() {
        return new PeriodoBean();
    }

    /**
     * Create an instance of {@link IdSolicitudChequeraType }
     * 
     */
    public IdSolicitudChequeraType createIdSolicitudChequeraType() {
        return new IdSolicitudChequeraType();
    }

    /**
     * Create an instance of {@link IdCuentaCONSUMOType }
     * 
     */
    public IdCuentaCONSUMOType createIdCuentaCONSUMOType() {
        return new IdCuentaCONSUMOType();
    }

    /**
     * Create an instance of {@link DatosCuentaBean }
     * 
     */
    public DatosCuentaBean createDatosCuentaBean() {
        return new DatosCuentaBean();
    }

    /**
     * Create an instance of {@link IdClienteType }
     * 
     */
    public IdClienteType createIdClienteType() {
        return new IdClienteType();
    }

    /**
     * Create an instance of {@link CodDescNumType }
     * 
     */
    public CodDescNumType createCodDescNumType() {
        return new CodDescNumType();
    }

    /**
     * Create an instance of {@link CuentaPMC }
     * 
     */
    public CuentaPMC createCuentaPMC() {
        return new CuentaPMC();
    }

    /**
     * Create an instance of {@link PasajesBean }
     * 
     */
    public PasajesBean createPasajesBean() {
        return new PasajesBean();
    }

    /**
     * Create an instance of {@link DatosAutenticacionExtSinTD }
     * 
     */
    public DatosAutenticacionExtSinTD createDatosAutenticacionExtSinTD() {
        return new DatosAutenticacionExtSinTD();
    }

    /**
     * Create an instance of {@link DetalleTicket }
     * 
     */
    public DetalleTicket createDetalleTicket() {
        return new DetalleTicket();
    }

    /**
     * Create an instance of {@link AdhesionDTO }
     * 
     */
    public AdhesionDTO createAdhesionDTO() {
        return new AdhesionDTO();
    }

    /**
     * Create an instance of {@link CommonDataResponseType }
     * 
     */
    public CommonDataResponseType createCommonDataResponseType() {
        return new CommonDataResponseType();
    }

    /**
     * Create an instance of {@link InformacionVentaBean }
     * 
     */
    public InformacionVentaBean createInformacionVentaBean() {
        return new InformacionVentaBean();
    }

    /**
     * Create an instance of {@link IdBiometricoType }
     * 
     */
    public IdBiometricoType createIdBiometricoType() {
        return new IdBiometricoType();
    }

    /**
     * Create an instance of {@link IdCuentaBANTOTALType }
     * 
     */
    public IdCuentaBANTOTALType createIdCuentaBANTOTALType() {
        return new IdCuentaBANTOTALType();
    }

    /**
     * Create an instance of {@link VepBean }
     * 
     */
    public VepBean createVepBean() {
        return new VepBean();
    }

    /**
     * Create an instance of {@link CoordenadasPMC }
     * 
     */
    public CoordenadasPMC createCoordenadasPMC() {
        return new CoordenadasPMC();
    }

    /**
     * Create an instance of {@link IdCtaTarjetaType }
     * 
     */
    public IdCtaTarjetaType createIdCtaTarjetaType() {
        return new IdCtaTarjetaType();
    }

    /**
     * Create an instance of {@link IdMovimientoPagoSueldoType }
     * 
     */
    public IdMovimientoPagoSueldoType createIdMovimientoPagoSueldoType() {
        return new IdMovimientoPagoSueldoType();
    }

    /**
     * Create an instance of {@link IdPrestamo }
     * 
     */
    public IdPrestamo createIdPrestamo() {
        return new IdPrestamo();
    }

    /**
     * Create an instance of {@link IdTarjetaType }
     * 
     */
    public IdTarjetaType createIdTarjetaType() {
        return new IdTarjetaType();
    }

    /**
     * Create an instance of {@link ComplejoBean }
     * 
     */
    public ComplejoBean createComplejoBean() {
        return new ComplejoBean();
    }

    /**
     * Create an instance of {@link CommonDataRequestType }
     * 
     */
    public CommonDataRequestType createCommonDataRequestType() {
        return new CommonDataRequestType();
    }

    /**
     * Create an instance of {@link DateTimeRangeType }
     * 
     */
    public DateTimeRangeType createDateTimeRangeType() {
        return new DateTimeRangeType();
    }

    /**
     * Create an instance of {@link ComboBean }
     * 
     */
    public ComboBean createComboBean() {
        return new ComboBean();
    }

    /**
     * Create an instance of {@link IdLiquidacionANSESType }
     * 
     */
    public IdLiquidacionANSESType createIdLiquidacionANSESType() {
        return new IdLiquidacionANSESType();
    }

    /**
     * Create an instance of {@link TerminalPMC }
     * 
     */
    public TerminalPMC createTerminalPMC() {
        return new TerminalPMC();
    }

    /**
     * Create an instance of {@link CanalPMC }
     * 
     */
    public CanalPMC createCanalPMC() {
        return new CanalPMC();
    }

    /**
     * Create an instance of {@link IdConvenioType }
     * 
     */
    public IdConvenioType createIdConvenioType() {
        return new IdConvenioType();
    }

    /**
     * Create an instance of {@link IdClienteOficioType }
     * 
     */
    public IdClienteOficioType createIdClienteOficioType() {
        return new IdClienteOficioType();
    }

    /**
     * Create an instance of {@link PrePagoBean }
     * 
     */
    public PrePagoBean createPrePagoBean() {
        return new PrePagoBean();
    }

    /**
     * Create an instance of {@link DatosEBPP }
     * 
     */
    public DatosEBPP createDatosEBPP() {
        return new DatosEBPP();
    }

    /**
     * Create an instance of {@link MandatoExtSinTD }
     * 
     */
    public MandatoExtSinTD createMandatoExtSinTD() {
        return new MandatoExtSinTD();
    }

    /**
     * Create an instance of {@link DetalleTicketPagoBean }
     * 
     */
    public DetalleTicketPagoBean createDetalleTicketPagoBean() {
        return new DetalleTicketPagoBean();
    }

    /**
     * Create an instance of {@link MandatarioExtSinTD }
     * 
     */
    public MandatarioExtSinTD createMandatarioExtSinTD() {
        return new MandatarioExtSinTD();
    }

    /**
     * Create an instance of {@link UsuarioPMC }
     * 
     */
    public UsuarioPMC createUsuarioPMC() {
        return new UsuarioPMC();
    }

    /**
     * Create an instance of {@link PeliculaBean }
     * 
     */
    public PeliculaBean createPeliculaBean() {
        return new PeliculaBean();
    }

    /**
     * Create an instance of {@link TipoMonedaType }
     * 
     */
    public TipoMonedaType createTipoMonedaType() {
        return new TipoMonedaType();
    }

    /**
     * Create an instance of {@link IdBanco }
     * 
     */
    public IdBanco createIdBanco() {
        return new IdBanco();
    }

    /**
     * Create an instance of {@link MonedaPMC }
     * 
     */
    public MonedaPMC createMonedaPMC() {
        return new MonedaPMC();
    }

    /**
     * Create an instance of {@link Ticket }
     * 
     */
    public Ticket createTicket() {
        return new Ticket();
    }

    /**
     * Create an instance of {@link IdAutenticacion }
     * 
     */
    public IdAutenticacion createIdAutenticacion() {
        return new IdAutenticacion();
    }

    /**
     * Create an instance of {@link ArrayOfDeudaPMC }
     * 
     */
    public ArrayOfDeudaPMC createArrayOfDeudaPMC() {
        return new ArrayOfDeudaPMC();
    }

    /**
     * Create an instance of {@link PrePagoPesBean }
     * 
     */
    public PrePagoPesBean createPrePagoPesBean() {
        return new PrePagoPesBean();
    }

    /**
     * Create an instance of {@link ISorteo }
     * 
     */
    public ISorteo createISorteo() {
        return new ISorteo();
    }

    /**
     * Create an instance of {@link TelefonoType }
     * 
     */
    public TelefonoType createTelefonoType() {
        return new TelefonoType();
    }

    /**
     * Create an instance of {@link RubroDTO }
     * 
     */
    public RubroDTO createRubroDTO() {
        return new RubroDTO();
    }

    /**
     * Create an instance of {@link FuncionBean }
     * 
     */
    public FuncionBean createFuncionBean() {
        return new FuncionBean();
    }

    /**
     * Create an instance of {@link EntretenimientosBean }
     * 
     */
    public EntretenimientosBean createEntretenimientosBean() {
        return new EntretenimientosBean();
    }

    /**
     * Create an instance of {@link IdUsuarioType }
     * 
     */
    public IdUsuarioType createIdUsuarioType() {
        return new IdUsuarioType();
    }

    /**
     * Create an instance of {@link DeudaInfoPMC }
     * 
     */
    public DeudaInfoPMC createDeudaInfoPMC() {
        return new DeudaInfoPMC();
    }

    /**
     * Create an instance of {@link IdCajaSegType }
     * 
     */
    public IdCajaSegType createIdCajaSegType() {
        return new IdCajaSegType();
    }

    /**
     * Create an instance of {@link IdReclamoType }
     * 
     */
    public IdReclamoType createIdReclamoType() {
        return new IdReclamoType();
    }

    /**
     * Create an instance of {@link AdhesionBean }
     * 
     */
    public AdhesionBean createAdhesionBean() {
        return new AdhesionBean();
    }

    /**
     * Create an instance of {@link RangoFechaType }
     * 
     */
    public RangoFechaType createRangoFechaType() {
        return new RangoFechaType();
    }

    /**
     * Create an instance of {@link CodDescStringType }
     * 
     */
    public CodDescStringType createCodDescStringType() {
        return new CodDescStringType();
    }

    /**
     * Create an instance of {@link VerazBean }
     * 
     */
    public VerazBean createVerazBean() {
        return new VerazBean();
    }

    /**
     * Create an instance of {@link DatosAyudaEmpresa }
     * 
     */
    public DatosAyudaEmpresa createDatosAyudaEmpresa() {
        return new DatosAyudaEmpresa();
    }

    /**
     * Create an instance of {@link IdListadoTypeV1 .Moneda }
     * 
     */
    public IdListadoTypeV1 .Moneda createIdListadoTypeV1Moneda() {
        return new IdListadoTypeV1 .Moneda();
    }

    /**
     * Create an instance of {@link FechaGregorianCalendar.TimeZone }
     * 
     */
    public FechaGregorianCalendar.TimeZone createFechaGregorianCalendarTimeZone() {
        return new FechaGregorianCalendar.TimeZone();
    }

    /**
     * Create an instance of {@link IdComprobante.DatosComprobante }
     * 
     */
    public IdComprobante.DatosComprobante createIdComprobanteDatosComprobante() {
        return new IdComprobante.DatosComprobante();
    }

    /**
     * Create an instance of {@link IdComprobante.DatosReceptor }
     * 
     */
    public IdComprobante.DatosReceptor createIdComprobanteDatosReceptor() {
        return new IdComprobante.DatosReceptor();
    }

    /**
     * Create an instance of {@link IdComprobante.Opcionales.Opcional }
     * 
     */
    public IdComprobante.Opcionales.Opcional createIdComprobanteOpcionalesOpcional() {
        return new IdComprobante.Opcionales.Opcional();
    }

    /**
     * Create an instance of {@link IdListadoType.Moneda }
     * 
     */
    public IdListadoType.Moneda createIdListadoTypeMoneda() {
        return new IdListadoType.Moneda();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas }
     * 
     */
    public ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas createResumenTCV11DetalleMovimientosMovimientoCuotas() {
        return new ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DetalleMovimientos.Movimiento.Importe }
     * 
     */
    public ResumenTCV11 .DetalleMovimientos.Movimiento.Importe createResumenTCV11DetalleMovimientosMovimientoImporte() {
        return new ResumenTCV11 .DetalleMovimientos.Movimiento.Importe();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen }
     * 
     */
    public ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen createResumenTCV11DetalleMovimientosMovimientoImporteOrigen() {
        return new ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.FechaCierre }
     * 
     */
    public ResumenTCV11 .DatosCuenta.FechaCierre createResumenTCV11DatosCuentaFechaCierre() {
        return new ResumenTCV11 .DatosCuenta.FechaCierre();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.FechaVencimiento }
     * 
     */
    public ResumenTCV11 .DatosCuenta.FechaVencimiento createResumenTCV11DatosCuentaFechaVencimiento() {
        return new ResumenTCV11 .DatosCuenta.FechaVencimiento();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Limites }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Limites createResumenTCV11DatosCuentaLimites() {
        return new ResumenTCV11 .DatosCuenta.Limites();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Tasas.TNA }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Tasas.TNA createResumenTCV11DatosCuentaTasasTNA() {
        return new ResumenTCV11 .DatosCuenta.Tasas.TNA();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Tasas.TEM }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Tasas.TEM createResumenTCV11DatosCuentaTasasTEM() {
        return new ResumenTCV11 .DatosCuenta.Tasas.TEM();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Saldos.Anterior }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Saldos.Anterior createResumenTCV11DatosCuentaSaldosAnterior() {
        return new ResumenTCV11 .DatosCuenta.Saldos.Anterior();
    }

    /**
     * Create an instance of {@link ResumenTCV11 .DatosCuenta.Saldos.Actual }
     * 
     */
    public ResumenTCV11 .DatosCuenta.Saldos.Actual createResumenTCV11DatosCuentaSaldosActual() {
        return new ResumenTCV11 .DatosCuenta.Saldos.Actual();
    }

    /**
     * Create an instance of {@link EmpresaBasePMC.TipoEmpresa }
     * 
     */
    public EmpresaBasePMC.TipoEmpresa createEmpresaBasePMCTipoEmpresa() {
        return new EmpresaBasePMC.TipoEmpresa();
    }

    /**
     * Create an instance of {@link OperacionTC.Movimientos.Movimiento.Comercio }
     * 
     */
    public OperacionTC.Movimientos.Movimiento.Comercio createOperacionTCMovimientosMovimientoComercio() {
        return new OperacionTC.Movimientos.Movimiento.Comercio();
    }

    /**
     * Create an instance of {@link OperacionTC.Movimientos.Movimiento.Rubro }
     * 
     */
    public OperacionTC.Movimientos.Movimiento.Rubro createOperacionTCMovimientosMovimientoRubro() {
        return new OperacionTC.Movimientos.Movimiento.Rubro();
    }

    /**
     * Create an instance of {@link OperacionTC.Movimientos.Movimiento.Cuota }
     * 
     */
    public OperacionTC.Movimientos.Movimiento.Cuota createOperacionTCMovimientosMovimientoCuota() {
        return new OperacionTC.Movimientos.Movimiento.Cuota();
    }

    /**
     * Create an instance of {@link OperacionTC.Movimientos.Movimiento.Importes }
     * 
     */
    public OperacionTC.Movimientos.Movimiento.Importes createOperacionTCMovimientosMovimientoImportes() {
        return new OperacionTC.Movimientos.Movimiento.Importes();
    }

    /**
     * Create an instance of {@link EstadoType.Errores }
     * 
     */
    public EstadoType.Errores createEstadoTypeErrores() {
        return new EstadoType.Errores();
    }

    /**
     * Create an instance of {@link IdPersonaType.Documentos }
     * 
     */
    public IdPersonaType.Documentos createIdPersonaTypeDocumentos() {
        return new IdPersonaType.Documentos();
    }

    /**
     * Create an instance of {@link EmpresaPMC.TipoAdhesion }
     * 
     */
    public EmpresaPMC.TipoAdhesion createEmpresaPMCTipoAdhesion() {
        return new EmpresaPMC.TipoAdhesion();
    }

    /**
     * Create an instance of {@link EmpresaPMC.TipoPago }
     * 
     */
    public EmpresaPMC.TipoPago createEmpresaPMCTipoPago() {
        return new EmpresaPMC.TipoPago();
    }

    /**
     * Create an instance of {@link ResumenTC.Socio }
     * 
     */
    public ResumenTC.Socio createResumenTCSocio() {
        return new ResumenTC.Socio();
    }

    /**
     * Create an instance of {@link ResumenTC.Limites }
     * 
     */
    public ResumenTC.Limites createResumenTCLimites() {
        return new ResumenTC.Limites();
    }

    /**
     * Create an instance of {@link ResumenTC.Saldos.Anterior }
     * 
     */
    public ResumenTC.Saldos.Anterior createResumenTCSaldosAnterior() {
        return new ResumenTC.Saldos.Anterior();
    }

    /**
     * Create an instance of {@link ResumenTC.Saldos.Actual }
     * 
     */
    public ResumenTC.Saldos.Actual createResumenTCSaldosActual() {
        return new ResumenTC.Saldos.Actual();
    }

    /**
     * Create an instance of {@link ResumenTC.Fechas.ResumenAnterior }
     * 
     */
    public ResumenTC.Fechas.ResumenAnterior createResumenTCFechasResumenAnterior() {
        return new ResumenTC.Fechas.ResumenAnterior();
    }

    /**
     * Create an instance of {@link ResumenTC.Fechas.ResumenActual }
     * 
     */
    public ResumenTC.Fechas.ResumenActual createResumenTCFechasResumenActual() {
        return new ResumenTC.Fechas.ResumenActual();
    }

    /**
     * Create an instance of {@link ResumenTC.Fechas.ResumenProximo }
     * 
     */
    public ResumenTC.Fechas.ResumenProximo createResumenTCFechasResumenProximo() {
        return new ResumenTC.Fechas.ResumenProximo();
    }

    /**
     * Create an instance of {@link OrderType.OrderBy }
     * 
     */
    public OrderType.OrderBy createOrderTypeOrderBy() {
        return new OrderType.OrderBy();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1", name = "Order")
    public JAXBElement<OrderType> createOrder(OrderType value) {
        return new JAXBElement<OrderType>(_Order_QNAME, OrderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1", name = "Paging")
    public JAXBElement<PagingType> createPaging(PagingType value) {
        return new JAXBElement<PagingType>(_Paging_QNAME, PagingType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fiid", scope = RubroDTO.class)
    public JAXBElement<String> createRubroDTOFiid(String value) {
        return new JAXBElement<String>(_RubroDTOFiid_QNAME, String.class, RubroDTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rubro", scope = RubroDTO.class)
    public JAXBElement<String> createRubroDTORubro(String value) {
        return new JAXBElement<String>(_RubroDTORubro_QNAME, String.class, RubroDTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoDeRubro", scope = RubroDTO.class)
    public JAXBElement<String> createRubroDTOTipoDeRubro(String value) {
        return new JAXBElement<String>(_RubroDTOTipoDeRubro_QNAME, String.class, RubroDTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codigo", scope = IdRubro.class)
    public JAXBElement<String> createIdRubroCodigo(String value) {
        return new JAXBElement<String>(_IdRubroCodigo_QNAME, String.class, IdRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipo", scope = IdRubro.class)
    public JAXBElement<String> createIdRubroTipo(String value) {
        return new JAXBElement<String>(_IdRubroTipo_QNAME, String.class, IdRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRubro", scope = IdRubro.class)
    public JAXBElement<String> createIdRubroTipoRubro(String value) {
        return new JAXBElement<String>(_IdRubroTipoRubro_QNAME, String.class, IdRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nombre", scope = IdRubro.class)
    public JAXBElement<String> createIdRubroNombre(String value) {
        return new JAXBElement<String>(_IdRubroNombre_QNAME, String.class, IdRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "info", scope = IdRubro.class)
    public JAXBElement<String> createIdRubroInfo(String value) {
        return new JAXBElement<String>(_IdRubroInfo_QNAME, String.class, IdRubro.class, value);
    }

}
