
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;


/**
 * <p>Clase Java para DataConsultaPuntosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaPuntosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="programa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="idEstadoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="estadoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="puntosDisponibles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencerEsteMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencerProximoMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencer2Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencer3Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaPuntosRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1", propOrder = {
    "row"
})
public class DataConsultaPuntosRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaPuntosRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaPuntosRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaPuntosRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaPuntosRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="programa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idEstadoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="estadoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="puntosDisponibles" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencerEsteMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencerProximoMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencer2Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencer3Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultado",
        "programa",
        "titular",
        "idEstadoCuenta",
        "estadoCuenta",
        "fechaAlta",
        "fechaBaja",
        "puntosDisponibles",
        "puntosAVencerEsteMes",
        "puntosAVencerProximoMes",
        "puntosAVencer2Meses",
        "puntosAVencer3Meses"
    })
    public static class Row {

        protected int resultado;
        @XmlElement(required = true)
        protected CodDescNumType programa;
        @XmlElement(required = true)
        protected String titular;
        @XmlElement(required = true)
        protected String idEstadoCuenta;
        @XmlElement(required = true)
        protected String estadoCuenta;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAlta;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaBaja;
        protected int puntosDisponibles;
        protected int puntosAVencerEsteMes;
        protected int puntosAVencerProximoMes;
        protected int puntosAVencer2Meses;
        protected int puntosAVencer3Meses;

        /**
         * Obtiene el valor de la propiedad resultado.
         * 
         */
        public int getResultado() {
            return resultado;
        }

        /**
         * Define el valor de la propiedad resultado.
         * 
         */
        public void setResultado(int value) {
            this.resultado = value;
        }

        /**
         * Obtiene el valor de la propiedad programa.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getPrograma() {
            return programa;
        }

        /**
         * Define el valor de la propiedad programa.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setPrograma(CodDescNumType value) {
            this.programa = value;
        }

        /**
         * Obtiene el valor de la propiedad titular.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitular() {
            return titular;
        }

        /**
         * Define el valor de la propiedad titular.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitular(String value) {
            this.titular = value;
        }

        /**
         * Obtiene el valor de la propiedad idEstadoCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdEstadoCuenta() {
            return idEstadoCuenta;
        }

        /**
         * Define el valor de la propiedad idEstadoCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdEstadoCuenta(String value) {
            this.idEstadoCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad estadoCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEstadoCuenta() {
            return estadoCuenta;
        }

        /**
         * Define el valor de la propiedad estadoCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEstadoCuenta(String value) {
            this.estadoCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAlta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAlta() {
            return fechaAlta;
        }

        /**
         * Define el valor de la propiedad fechaAlta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAlta(XMLGregorianCalendar value) {
            this.fechaAlta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaBaja.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaBaja() {
            return fechaBaja;
        }

        /**
         * Define el valor de la propiedad fechaBaja.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaBaja(XMLGregorianCalendar value) {
            this.fechaBaja = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosDisponibles.
         * 
         */
        public int getPuntosDisponibles() {
            return puntosDisponibles;
        }

        /**
         * Define el valor de la propiedad puntosDisponibles.
         * 
         */
        public void setPuntosDisponibles(int value) {
            this.puntosDisponibles = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencerEsteMes.
         * 
         */
        public int getPuntosAVencerEsteMes() {
            return puntosAVencerEsteMes;
        }

        /**
         * Define el valor de la propiedad puntosAVencerEsteMes.
         * 
         */
        public void setPuntosAVencerEsteMes(int value) {
            this.puntosAVencerEsteMes = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencerProximoMes.
         * 
         */
        public int getPuntosAVencerProximoMes() {
            return puntosAVencerProximoMes;
        }

        /**
         * Define el valor de la propiedad puntosAVencerProximoMes.
         * 
         */
        public void setPuntosAVencerProximoMes(int value) {
            this.puntosAVencerProximoMes = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencer2Meses.
         * 
         */
        public int getPuntosAVencer2Meses() {
            return puntosAVencer2Meses;
        }

        /**
         * Define el valor de la propiedad puntosAVencer2Meses.
         * 
         */
        public void setPuntosAVencer2Meses(int value) {
            this.puntosAVencer2Meses = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencer3Meses.
         * 
         */
        public int getPuntosAVencer3Meses() {
            return puntosAVencer3Meses;
        }

        /**
         * Define el valor de la propiedad puntosAVencer3Meses.
         * 
         */
        public void setPuntosAVencer3Meses(int value) {
            this.puntosAVencer3Meses = value;
        }

    }

}
