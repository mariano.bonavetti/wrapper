
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaatributosxtipoproducto_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaAtributosXTipoProductoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaAtributosXTipoProductoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="codigoParametro" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaAtributosXTipoProductoReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaAtributosXTipoProducto-v1.1", propOrder = {
    "canal",
    "subCanal",
    "tipoProducto",
    "codigoParametro"
})
public class DataConsultaAtributosXTipoProductoReqType2 {

    @XmlElement(required = true)
    protected BigInteger canal;
    @XmlElement(required = true)
    protected BigInteger subCanal;
    @XmlElement(required = true)
    protected BigInteger tipoProducto;
    protected BigInteger codigoParametro;

    /**
     * Obtiene el valor de la propiedad canal.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCanal() {
        return canal;
    }

    /**
     * Define el valor de la propiedad canal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCanal(BigInteger value) {
        this.canal = value;
    }

    /**
     * Obtiene el valor de la propiedad subCanal.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubCanal() {
        return subCanal;
    }

    /**
     * Define el valor de la propiedad subCanal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubCanal(BigInteger value) {
        this.subCanal = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProducto.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTipoProducto() {
        return tipoProducto;
    }

    /**
     * Define el valor de la propiedad tipoProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTipoProducto(BigInteger value) {
        this.tipoProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoParametro.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCodigoParametro() {
        return codigoParametro;
    }

    /**
     * Define el valor de la propiedad codigoParametro.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCodigoParametro(BigInteger value) {
        this.codigoParametro = value;
    }

}
