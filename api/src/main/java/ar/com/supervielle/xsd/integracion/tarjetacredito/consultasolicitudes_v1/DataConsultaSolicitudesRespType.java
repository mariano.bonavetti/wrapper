
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultasolicitudes_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaSolicitudesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaSolicitudesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="TipoSolicitud" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="IdentificadorEmpresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="IdentificadorIndividuo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Subcanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaSolicitudesRespType", propOrder = {
    "row"
})
public class DataConsultaSolicitudesRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaSolicitudesRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaSolicitudesRespType.Row }
     * 
     * 
     */
    public List<DataConsultaSolicitudesRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaSolicitudesRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="TipoSolicitud" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="IdentificadorEmpresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="IdentificadorIndividuo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Subcanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroSolicitudGrupo",
        "nroSolicitud",
        "tipoSolicitud",
        "identificadorEmpresa",
        "identificadorIndividuo",
        "estado",
        "canal",
        "subcanal"
    })
    public static class Row {

        @XmlElement(required = true)
        protected BigInteger nroSolicitudGrupo;
        @XmlElement(required = true)
        protected BigInteger nroSolicitud;
        @XmlElement(name = "TipoSolicitud", required = true)
        protected CodDescNumType tipoSolicitud;
        @XmlElement(name = "IdentificadorEmpresa", required = true)
        protected IdClienteType identificadorEmpresa;
        @XmlElement(name = "IdentificadorIndividuo", required = true)
        protected IdClienteType identificadorIndividuo;
        @XmlElement(name = "Estado", required = true)
        protected CodDescNumType estado;
        @XmlElement(name = "Canal", required = true)
        protected CodDescNumType canal;
        @XmlElement(name = "Subcanal", required = true)
        protected CodDescNumType subcanal;

        /**
         * Obtiene el valor de la propiedad nroSolicitudGrupo.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNroSolicitudGrupo() {
            return nroSolicitudGrupo;
        }

        /**
         * Define el valor de la propiedad nroSolicitudGrupo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNroSolicitudGrupo(BigInteger value) {
            this.nroSolicitudGrupo = value;
        }

        /**
         * Obtiene el valor de la propiedad nroSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNroSolicitud() {
            return nroSolicitud;
        }

        /**
         * Define el valor de la propiedad nroSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNroSolicitud(BigInteger value) {
            this.nroSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getTipoSolicitud() {
            return tipoSolicitud;
        }

        /**
         * Define el valor de la propiedad tipoSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setTipoSolicitud(CodDescNumType value) {
            this.tipoSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad identificadorEmpresa.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificadorEmpresa() {
            return identificadorEmpresa;
        }

        /**
         * Define el valor de la propiedad identificadorEmpresa.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificadorEmpresa(IdClienteType value) {
            this.identificadorEmpresa = value;
        }

        /**
         * Obtiene el valor de la propiedad identificadorIndividuo.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificadorIndividuo() {
            return identificadorIndividuo;
        }

        /**
         * Define el valor de la propiedad identificadorIndividuo.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificadorIndividuo(IdClienteType value) {
            this.identificadorIndividuo = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setEstado(CodDescNumType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad canal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getCanal() {
            return canal;
        }

        /**
         * Define el valor de la propiedad canal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setCanal(CodDescNumType value) {
            this.canal = value;
        }

        /**
         * Obtiene el valor de la propiedad subcanal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getSubcanal() {
            return subcanal;
        }

        /**
         * Define el valor de la propiedad subcanal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setSubcanal(CodDescNumType value) {
            this.subcanal = value;
        }

    }

}
