
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoxcliente_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaEstadoxClienteReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaEstadoxClienteReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaEstadoxClienteReq", propOrder = {
    "nroSolicitud",
    "identificador"
})
public class DataConsultaEstadoxClienteReq {

    protected int nroSolicitud;
    @XmlElement(required = true)
    protected IdClienteType identificador;

    /**
     * Obtiene el valor de la propiedad nroSolicitud.
     * 
     */
    public int getNroSolicitud() {
        return nroSolicitud;
    }

    /**
     * Define el valor de la propiedad nroSolicitud.
     * 
     */
    public void setNroSolicitud(int value) {
        this.nroSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

}
