
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultasolicitudes_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultasolicitudes_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultasolicitudes_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudesRespType }
     * 
     */
    public DataConsultaSolicitudesRespType createDataConsultaSolicitudesRespType() {
        return new DataConsultaSolicitudesRespType();
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudesReqType }
     * 
     */
    public DataConsultaSolicitudesReqType createDataConsultaSolicitudesReqType() {
        return new DataConsultaSolicitudesReqType();
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudesReqType.Empresa }
     * 
     */
    public DataConsultaSolicitudesReqType.Empresa createDataConsultaSolicitudesReqTypeEmpresa() {
        return new DataConsultaSolicitudesReqType.Empresa();
    }

    /**
     * Create an instance of {@link ReqConsultaSolicitudes }
     * 
     */
    public ReqConsultaSolicitudes createReqConsultaSolicitudes() {
        return new ReqConsultaSolicitudes();
    }

    /**
     * Create an instance of {@link RespConsultaSolicitudes }
     * 
     */
    public RespConsultaSolicitudes createRespConsultaSolicitudes() {
        return new RespConsultaSolicitudes();
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudesRespType.Row }
     * 
     */
    public DataConsultaSolicitudesRespType.Row createDataConsultaSolicitudesRespTypeRow() {
        return new DataConsultaSolicitudesRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud }
     * 
     */
    public DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud createDataConsultaSolicitudesReqTypeEmpresaIdentificadorSolicitud() {
        return new DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud();
    }

}
