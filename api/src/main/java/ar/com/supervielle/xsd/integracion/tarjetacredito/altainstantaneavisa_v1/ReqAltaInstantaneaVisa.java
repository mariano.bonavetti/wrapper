
package ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneavisa_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/altaInstantaneaVisa-v1}DataAltaInstantaneaVisaReqType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "ReqAltaInstantaneaVisa", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/altaInstantaneaVisa-v1")
public class ReqAltaInstantaneaVisa {

    @XmlElement(name = "Data", required = true)
    protected DataAltaInstantaneaVisaReqType data;

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType }
     *     
     */
    public DataAltaInstantaneaVisaReqType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType }
     *     
     */
    public void setData(DataAltaInstantaneaVisaReqType value) {
        this.data = value;
    }

}
