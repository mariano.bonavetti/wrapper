
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaultimoresumen_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.ResumenTCV11;


/**
 * <p>Clase Java para DataConsultaUltimoResumenRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaUltimoResumenRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}resumenTC_v1.1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaUltimoResumenRespType", propOrder = {
    "row"
})
public class DataConsultaUltimoResumenRespType2 {

    @XmlElement(name = "Row")
    protected ResumenTCV11 row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTCV11 }
     *     
     */
    public ResumenTCV11 getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTCV11 }
     *     
     */
    public void setRow(ResumenTCV11 value) {
        this.row = value;
    }

}
