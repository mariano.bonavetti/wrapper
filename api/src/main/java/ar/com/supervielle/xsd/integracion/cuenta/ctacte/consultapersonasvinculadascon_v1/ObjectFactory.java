
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadascon_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadascon_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadascon_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasCONRespType }
     * 
     */
    public DataConsultaPersonasVinculadasCONRespType createDataConsultaPersonasVinculadasCONRespType() {
        return new DataConsultaPersonasVinculadasCONRespType();
    }

    /**
     * Create an instance of {@link RespConsultaPersonasVinculadasCON }
     * 
     */
    public RespConsultaPersonasVinculadasCON createRespConsultaPersonasVinculadasCON() {
        return new RespConsultaPersonasVinculadasCON();
    }

    /**
     * Create an instance of {@link ReqConsultaPersonasVinculadasCON }
     * 
     */
    public ReqConsultaPersonasVinculadasCON createReqConsultaPersonasVinculadasCON() {
        return new ReqConsultaPersonasVinculadasCON();
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasCONReqType }
     * 
     */
    public DataConsultaPersonasVinculadasCONReqType createDataConsultaPersonasVinculadasCONReqType() {
        return new DataConsultaPersonasVinculadasCONReqType();
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasCONRespType.Row }
     * 
     */
    public DataConsultaPersonasVinculadasCONRespType.Row createDataConsultaPersonasVinculadasCONRespTypeRow() {
        return new DataConsultaPersonasVinculadasCONRespType.Row();
    }

}
