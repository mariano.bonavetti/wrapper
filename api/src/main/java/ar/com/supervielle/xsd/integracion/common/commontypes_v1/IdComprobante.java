
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para idComprobante complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="idComprobante">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="modoComprobante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cuitEmisor" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ptoVenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="datosComprobante">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tipoComprobante" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="nroComprobante" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="fechaComprobante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="codAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datosReceptor" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="opcionales" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="opcional" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType">
 *                           &lt;sequence>
 *                             &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "idComprobante", propOrder = {
    "modoComprobante",
    "cuitEmisor",
    "ptoVenta",
    "datosComprobante",
    "importeTotal",
    "codAutorizacion",
    "datosReceptor",
    "opcionales"
})
public class IdComprobante {

    protected String modoComprobante;
    protected long cuitEmisor;
    protected int ptoVenta;
    @XmlElement(required = true)
    protected IdComprobante.DatosComprobante datosComprobante;
    protected double importeTotal;
    protected String codAutorizacion;
    protected IdComprobante.DatosReceptor datosReceptor;
    protected IdComprobante.Opcionales opcionales;

    /**
     * Obtiene el valor de la propiedad modoComprobante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModoComprobante() {
        return modoComprobante;
    }

    /**
     * Define el valor de la propiedad modoComprobante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModoComprobante(String value) {
        this.modoComprobante = value;
    }

    /**
     * Obtiene el valor de la propiedad cuitEmisor.
     * 
     */
    public long getCuitEmisor() {
        return cuitEmisor;
    }

    /**
     * Define el valor de la propiedad cuitEmisor.
     * 
     */
    public void setCuitEmisor(long value) {
        this.cuitEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad ptoVenta.
     * 
     */
    public int getPtoVenta() {
        return ptoVenta;
    }

    /**
     * Define el valor de la propiedad ptoVenta.
     * 
     */
    public void setPtoVenta(int value) {
        this.ptoVenta = value;
    }

    /**
     * Obtiene el valor de la propiedad datosComprobante.
     * 
     * @return
     *     possible object is
     *     {@link IdComprobante.DatosComprobante }
     *     
     */
    public IdComprobante.DatosComprobante getDatosComprobante() {
        return datosComprobante;
    }

    /**
     * Define el valor de la propiedad datosComprobante.
     * 
     * @param value
     *     allowed object is
     *     {@link IdComprobante.DatosComprobante }
     *     
     */
    public void setDatosComprobante(IdComprobante.DatosComprobante value) {
        this.datosComprobante = value;
    }

    /**
     * Obtiene el valor de la propiedad importeTotal.
     * 
     */
    public double getImporteTotal() {
        return importeTotal;
    }

    /**
     * Define el valor de la propiedad importeTotal.
     * 
     */
    public void setImporteTotal(double value) {
        this.importeTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad codAutorizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAutorizacion() {
        return codAutorizacion;
    }

    /**
     * Define el valor de la propiedad codAutorizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAutorizacion(String value) {
        this.codAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad datosReceptor.
     * 
     * @return
     *     possible object is
     *     {@link IdComprobante.DatosReceptor }
     *     
     */
    public IdComprobante.DatosReceptor getDatosReceptor() {
        return datosReceptor;
    }

    /**
     * Define el valor de la propiedad datosReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link IdComprobante.DatosReceptor }
     *     
     */
    public void setDatosReceptor(IdComprobante.DatosReceptor value) {
        this.datosReceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad opcionales.
     * 
     * @return
     *     possible object is
     *     {@link IdComprobante.Opcionales }
     *     
     */
    public IdComprobante.Opcionales getOpcionales() {
        return opcionales;
    }

    /**
     * Define el valor de la propiedad opcionales.
     * 
     * @param value
     *     allowed object is
     *     {@link IdComprobante.Opcionales }
     *     
     */
    public void setOpcionales(IdComprobante.Opcionales value) {
        this.opcionales = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tipoComprobante" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="nroComprobante" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="fechaComprobante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tipoComprobante",
        "nroComprobante",
        "fechaComprobante"
    })
    public static class DatosComprobante {

        protected int tipoComprobante;
        protected long nroComprobante;
        protected String fechaComprobante;

        /**
         * Obtiene el valor de la propiedad tipoComprobante.
         * 
         */
        public int getTipoComprobante() {
            return tipoComprobante;
        }

        /**
         * Define el valor de la propiedad tipoComprobante.
         * 
         */
        public void setTipoComprobante(int value) {
            this.tipoComprobante = value;
        }

        /**
         * Obtiene el valor de la propiedad nroComprobante.
         * 
         */
        public long getNroComprobante() {
            return nroComprobante;
        }

        /**
         * Define el valor de la propiedad nroComprobante.
         * 
         */
        public void setNroComprobante(long value) {
            this.nroComprobante = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaComprobante.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaComprobante() {
            return fechaComprobante;
        }

        /**
         * Define el valor de la propiedad fechaComprobante.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaComprobante(String value) {
            this.fechaComprobante = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tipoDoc",
        "nroDoc"
    })
    public static class DatosReceptor {

        @XmlElement(required = true)
        protected String tipoDoc;
        @XmlElement(required = true)
        protected String nroDoc;

        /**
         * Obtiene el valor de la propiedad tipoDoc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoDoc() {
            return tipoDoc;
        }

        /**
         * Define el valor de la propiedad tipoDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoDoc(String value) {
            this.tipoDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad nroDoc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNroDoc() {
            return nroDoc;
        }

        /**
         * Define el valor de la propiedad nroDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNroDoc(String value) {
            this.nroDoc = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="opcional" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType">
     *                 &lt;sequence>
     *                   &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "opcional"
    })
    public static class Opcionales {

        protected List<IdComprobante.Opcionales.Opcional> opcional;

        /**
         * Gets the value of the opcional property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the opcional property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOpcional().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link IdComprobante.Opcionales.Opcional }
         * 
         * 
         */
        public List<IdComprobante.Opcionales.Opcional> getOpcional() {
            if (opcional == null) {
                opcional = new ArrayList<IdComprobante.Opcionales.Opcional>();
            }
            return this.opcional;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType">
         *       &lt;sequence>
         *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "valor"
        })
        public static class Opcional
            extends CodDescStringType
        {

            protected String id;
            protected String valor;

            /**
             * Obtiene el valor de la propiedad id.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getId() {
                return id;
            }

            /**
             * Define el valor de la propiedad id.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Obtiene el valor de la propiedad valor.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValor() {
                return valor;
            }

            /**
             * Define el valor de la propiedad valor.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValor(String value) {
                this.valor = value;
            }

        }

    }

}
