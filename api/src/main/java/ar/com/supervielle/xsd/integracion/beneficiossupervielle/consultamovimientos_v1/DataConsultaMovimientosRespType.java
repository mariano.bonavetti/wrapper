
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultamovimientos_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataConsultaMovimientosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaMovimientosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="listaMovimientos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="movimientos" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="idMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="dtmfechademov" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="montoPunto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="montoPesos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idExterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idexterno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaMovimientosRespType", propOrder = {
    "row"
})
public class DataConsultaMovimientosRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaMovimientosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaMovimientosRespType.Row }
     * 
     * 
     */
    public List<DataConsultaMovimientosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaMovimientosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="listaMovimientos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="movimientos" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="idMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="dtmfechademov" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="montoPunto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="montoPesos" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idExterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idexterno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultado",
        "listaMovimientos"
    })
    public static class Row {

        protected String resultado;
        protected DataConsultaMovimientosRespType.Row.ListaMovimientos listaMovimientos;

        /**
         * Obtiene el valor de la propiedad resultado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultado() {
            return resultado;
        }

        /**
         * Define el valor de la propiedad resultado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultado(String value) {
            this.resultado = value;
        }

        /**
         * Obtiene el valor de la propiedad listaMovimientos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaMovimientosRespType.Row.ListaMovimientos }
         *     
         */
        public DataConsultaMovimientosRespType.Row.ListaMovimientos getListaMovimientos() {
            return listaMovimientos;
        }

        /**
         * Define el valor de la propiedad listaMovimientos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaMovimientosRespType.Row.ListaMovimientos }
         *     
         */
        public void setListaMovimientos(DataConsultaMovimientosRespType.Row.ListaMovimientos value) {
            this.listaMovimientos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="movimientos" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="idMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="dtmfechademov" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="montoPunto" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="montoPesos" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idExterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idexterno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "movimientos"
        })
        public static class ListaMovimientos {

            protected List<DataConsultaMovimientosRespType.Row.ListaMovimientos.Movimientos> movimientos;

            /**
             * Gets the value of the movimientos property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the movimientos property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getMovimientos().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaMovimientosRespType.Row.ListaMovimientos.Movimientos }
             * 
             * 
             */
            public List<DataConsultaMovimientosRespType.Row.ListaMovimientos.Movimientos> getMovimientos() {
                if (movimientos == null) {
                    movimientos = new ArrayList<DataConsultaMovimientosRespType.Row.ListaMovimientos.Movimientos>();
                }
                return this.movimientos;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="dtmfechademov" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="montoPunto" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="montoPesos" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descTipoMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idExterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idexterno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idMovimiento",
                "idTarjeta",
                "idTipoMov",
                "dtmfechademov",
                "montoPunto",
                "montoPesos",
                "descTipoMov",
                "nroCuenta",
                "idExterno",
                "idexterno2",
                "descTipoCtaCte"
            })
            public static class Movimientos {

                @XmlElement(required = true)
                protected String idMovimiento;
                @XmlElement(required = true)
                protected String idTarjeta;
                @XmlElement(required = true)
                protected String idTipoMov;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar dtmfechademov;
                @XmlElement(required = true)
                protected String montoPunto;
                @XmlElement(required = true)
                protected String montoPesos;
                @XmlElement(required = true)
                protected String descTipoMov;
                @XmlElement(required = true)
                protected String nroCuenta;
                @XmlElement(required = true)
                protected String idExterno;
                @XmlElement(required = true)
                protected String idexterno2;
                @XmlElement(required = true)
                protected String descTipoCtaCte;

                /**
                 * Obtiene el valor de la propiedad idMovimiento.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdMovimiento() {
                    return idMovimiento;
                }

                /**
                 * Define el valor de la propiedad idMovimiento.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdMovimiento(String value) {
                    this.idMovimiento = value;
                }

                /**
                 * Obtiene el valor de la propiedad idTarjeta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdTarjeta() {
                    return idTarjeta;
                }

                /**
                 * Define el valor de la propiedad idTarjeta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdTarjeta(String value) {
                    this.idTarjeta = value;
                }

                /**
                 * Obtiene el valor de la propiedad idTipoMov.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdTipoMov() {
                    return idTipoMov;
                }

                /**
                 * Define el valor de la propiedad idTipoMov.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdTipoMov(String value) {
                    this.idTipoMov = value;
                }

                /**
                 * Obtiene el valor de la propiedad dtmfechademov.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDtmfechademov() {
                    return dtmfechademov;
                }

                /**
                 * Define el valor de la propiedad dtmfechademov.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDtmfechademov(XMLGregorianCalendar value) {
                    this.dtmfechademov = value;
                }

                /**
                 * Obtiene el valor de la propiedad montoPunto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMontoPunto() {
                    return montoPunto;
                }

                /**
                 * Define el valor de la propiedad montoPunto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMontoPunto(String value) {
                    this.montoPunto = value;
                }

                /**
                 * Obtiene el valor de la propiedad montoPesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMontoPesos() {
                    return montoPesos;
                }

                /**
                 * Define el valor de la propiedad montoPesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMontoPesos(String value) {
                    this.montoPesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad descTipoMov.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescTipoMov() {
                    return descTipoMov;
                }

                /**
                 * Define el valor de la propiedad descTipoMov.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescTipoMov(String value) {
                    this.descTipoMov = value;
                }

                /**
                 * Obtiene el valor de la propiedad nroCuenta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNroCuenta() {
                    return nroCuenta;
                }

                /**
                 * Define el valor de la propiedad nroCuenta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNroCuenta(String value) {
                    this.nroCuenta = value;
                }

                /**
                 * Obtiene el valor de la propiedad idExterno.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdExterno() {
                    return idExterno;
                }

                /**
                 * Define el valor de la propiedad idExterno.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdExterno(String value) {
                    this.idExterno = value;
                }

                /**
                 * Obtiene el valor de la propiedad idexterno2.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdexterno2() {
                    return idexterno2;
                }

                /**
                 * Define el valor de la propiedad idexterno2.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdexterno2(String value) {
                    this.idexterno2 = value;
                }

                /**
                 * Obtiene el valor de la propiedad descTipoCtaCte.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescTipoCtaCte() {
                    return descTipoCtaCte;
                }

                /**
                 * Define el valor de la propiedad descTipoCtaCte.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescTipoCtaCte(String value) {
                    this.descTipoCtaCte = value;
                }

            }

        }

    }

}
