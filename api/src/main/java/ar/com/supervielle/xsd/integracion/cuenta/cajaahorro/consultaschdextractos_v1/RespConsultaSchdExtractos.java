
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaschdextractos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.PagingType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}Paging" minOccurs="0"/>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaSchdExtractos-v1}DataConsultaSchdExtractosRespType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paging",
    "data"
})
@XmlRootElement(name = "RespConsultaSchdExtractos")
public class RespConsultaSchdExtractos {

    @XmlElement(name = "Paging", namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1")
    protected PagingType paging;
    @XmlElement(name = "Data", required = true)
    protected DataConsultaSchdExtractosRespType data;

    /**
     * Obtiene el valor de la propiedad paging.
     * 
     * @return
     *     possible object is
     *     {@link PagingType }
     *     
     */
    public PagingType getPaging() {
        return paging;
    }

    /**
     * Define el valor de la propiedad paging.
     * 
     * @param value
     *     allowed object is
     *     {@link PagingType }
     *     
     */
    public void setPaging(PagingType value) {
        this.paging = value;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaSchdExtractosRespType }
     *     
     */
    public DataConsultaSchdExtractosRespType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaSchdExtractosRespType }
     *     
     */
    public void setData(DataConsultaSchdExtractosRespType value) {
        this.data = value;
    }

}
