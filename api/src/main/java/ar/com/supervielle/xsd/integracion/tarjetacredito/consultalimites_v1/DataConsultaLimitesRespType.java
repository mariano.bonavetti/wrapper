
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimites_v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaLimitesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaLimitesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteCompraCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteCompraDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteCompraCuotasDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteCompraDisponibleAdelantos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteOperacionesInternacionales" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteAdelantos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaLimitesRespType", propOrder = {
    "row"
})
public class DataConsultaLimitesRespType {

    @XmlElement(name = "Row")
    protected DataConsultaLimitesRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaLimitesRespType.Row }
     *     
     */
    public DataConsultaLimitesRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaLimitesRespType.Row }
     *     
     */
    public void setRow(DataConsultaLimitesRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
     *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteCompraCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteCompraDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteCompraCuotasDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteCompraDisponibleAdelantos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteOperacionesInternacionales" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteAdelantos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "limiteCompra",
        "limiteCompraCuotas",
        "limiteCompraDisponible",
        "limiteCompraCuotasDisponible",
        "limiteCompraDisponibleAdelantos",
        "limiteOperacionesInternacionales",
        "limiteFinanciacion",
        "limiteAdelantos"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdTarjetaType identificador;
        protected BigDecimal limiteCompra;
        protected BigDecimal limiteCompraCuotas;
        protected BigDecimal limiteCompraDisponible;
        protected BigDecimal limiteCompraCuotasDisponible;
        protected BigDecimal limiteCompraDisponibleAdelantos;
        protected BigDecimal limiteOperacionesInternacionales;
        protected BigDecimal limiteFinanciacion;
        protected BigDecimal limiteAdelantos;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdTarjetaType }
         *     
         */
        public IdTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdTarjetaType }
         *     
         */
        public void setIdentificador(IdTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompra.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompra() {
            return limiteCompra;
        }

        /**
         * Define el valor de la propiedad limiteCompra.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompra(BigDecimal value) {
            this.limiteCompra = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompraCuotas.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompraCuotas() {
            return limiteCompraCuotas;
        }

        /**
         * Define el valor de la propiedad limiteCompraCuotas.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompraCuotas(BigDecimal value) {
            this.limiteCompraCuotas = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompraDisponible.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompraDisponible() {
            return limiteCompraDisponible;
        }

        /**
         * Define el valor de la propiedad limiteCompraDisponible.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompraDisponible(BigDecimal value) {
            this.limiteCompraDisponible = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompraCuotasDisponible.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompraCuotasDisponible() {
            return limiteCompraCuotasDisponible;
        }

        /**
         * Define el valor de la propiedad limiteCompraCuotasDisponible.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompraCuotasDisponible(BigDecimal value) {
            this.limiteCompraCuotasDisponible = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompraDisponibleAdelantos.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompraDisponibleAdelantos() {
            return limiteCompraDisponibleAdelantos;
        }

        /**
         * Define el valor de la propiedad limiteCompraDisponibleAdelantos.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompraDisponibleAdelantos(BigDecimal value) {
            this.limiteCompraDisponibleAdelantos = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteOperacionesInternacionales.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteOperacionesInternacionales() {
            return limiteOperacionesInternacionales;
        }

        /**
         * Define el valor de la propiedad limiteOperacionesInternacionales.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteOperacionesInternacionales(BigDecimal value) {
            this.limiteOperacionesInternacionales = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteFinanciacion.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteFinanciacion() {
            return limiteFinanciacion;
        }

        /**
         * Define el valor de la propiedad limiteFinanciacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteFinanciacion(BigDecimal value) {
            this.limiteFinanciacion = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteAdelantos.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteAdelantos() {
            return limiteAdelantos;
        }

        /**
         * Define el valor de la propiedad limiteAdelantos.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteAdelantos(BigDecimal value) {
            this.limiteAdelantos = value;
        }

    }

}
