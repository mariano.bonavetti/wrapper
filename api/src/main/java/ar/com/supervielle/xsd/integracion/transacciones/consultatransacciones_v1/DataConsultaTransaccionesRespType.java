
package ar.com.supervielle.xsd.integracion.transacciones.consultatransacciones_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1.ReqAltaAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1.RespAltaAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.bajaacuerdos_v1.ReqBajaAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.bajaacuerdos_v1.RespBajaAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdos_v1.ReqConsultaAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdos_v1.RespConsultaAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdosban_v1.ReqConsultaAcuerdosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdosban_v1.RespConsultaAcuerdosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdoscon_v1.ReqConsultaAcuerdosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdoscon_v1.RespConsultaAcuerdosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbu_v1.ReqConsultaCBU;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbu_v1.RespConsultaCBU;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbuban_v1.ReqConsultaCBUBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbuban_v1.RespConsultaCBUBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbucon_v1.ReqConsultaCBUCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbucon_v1.RespConsultaCBUCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesemitidos_v1.ReqConsultaChequesEmitidos2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesemitidos_v1.RespConsultaChequesEmitidos2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesrecibidos_v1.ReqConsultaChequesRecibidos2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesrecibidos_v1.RespConsultaChequesRecibidos2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetalle_v1.ReqConsultaDetalle2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetalle_v1.RespConsultaDetalle2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetalleban_v1.ReqConsultaDetalleBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetalleban_v1.RespConsultaDetalleBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetallecon_v1.ReqConsultaDetalleCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetallecon_v1.RespConsultaDetalleCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatraso_v1.ReqConsultaDiasAtraso;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatraso_v1.RespConsultaDiasAtraso;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasoban_v1.ReqConsultaDiasAtrasoBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasoban_v1.RespConsultaDiasAtrasoBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasocon_v1.ReqConsultaDiasAtrasoCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasocon_v1.RespConsultaDiasAtrasoCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargos_v1.ReqConsultaEmbargos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargos_v1.RespConsultaEmbargos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargosban_v1.ReqConsultaEmbargosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargosban_v1.RespConsultaEmbargosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargoscon_v1.ReqConsultaEmbargosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargoscon_v1.RespConsultaEmbargosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.ReqConsultaListado2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.ReqConsultaListado3;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.RespConsultaListado2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.RespConsultaListado3;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoacuerdos_v1.ReqConsultaListadoAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoacuerdos_v1.RespConsultaListadoAcuerdos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoban_v1.ReqConsultaListadoBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadoban_v1.RespConsultaListadoBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadocon_v1.ReqConsultaListadoCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistadocon_v1.RespConsultaListadoCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientos_v1.ReqConsultaMovimientos2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientos_v1.RespConsultaMovimientos2;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientosban_v1.ReqConsultaMovimientosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientosban_v1.RespConsultaMovimientosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientoscon_v1.ReqConsultaMovimientosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientoscon_v1.RespConsultaMovimientosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaoperaciones_v1.ReqConsultaOperaciones;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaoperaciones_v1.RespConsultaOperaciones;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaordendecuenta_v1.ReqConsultaOrdenDeCuenta;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaordendecuenta_v1.RespConsultaOrdenDeCuenta;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadas_v1.ReqConsultaPersonasVinculadas;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadas_v1.RespConsultaPersonasVinculadas;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadasban_v1.ReqConsultaPersonasVinculadasBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadasban_v1.RespConsultaPersonasVinculadasBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadascon_v1.ReqConsultaPersonasVinculadasCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultapersonasvinculadascon_v1.RespConsultaPersonasVinculadasCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultarepositorioextracto_v1.ReqConsultaRepositorioExtracto;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultarepositorioextracto_v1.RespConsultaRepositorioExtracto;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldopromedio_v1.ReqConsultaSaldoPromedio;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldopromedio_v1.RespConsultaSaldoPromedio;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1.ReqConsultaSaldos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1.RespConsultaSaldos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaschdextractos_v1.ReqConsultaSchdExtractos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaschdextractos_v1.RespConsultaSchdExtractos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaschdextractosban_v1.ReqConsultaSchdExtractosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaschdextractosban_v1.RespConsultaSchdExtractosBAN;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaschdextractoscon_v1.ReqConsultaSchdExtractosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaschdextractoscon_v1.RespConsultaSchdExtractosCON;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasolicitudchequera_v1.ReqConsultaSolicitudChequera;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasolicitudchequera_v1.RespConsultaSolicitudChequera;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.solicitudchequera_v1.ReqSolicitudChequera;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.solicitudchequera_v1.RespSolicitudChequera;
import ar.com.supervielle.xsd.servicios.integracion.exception_v1.Exception;


/**
 * <p>Clase Java para DataConsultaTransaccionesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaTransaccionesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="respuesta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1}ReqConsultaListado"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1}RespConsultaListado"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1}ReqConsultaListadoBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1}RespConsultaListadoBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1}ReqConsultaListadoCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1}RespConsultaListadoCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1}ReqConsultaMovimientos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1}RespConsultaMovimientos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1}ReqConsultaMovimientosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1}RespConsultaMovimientosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1}ReqConsultaMovimientosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1}RespConsultaMovimientosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1}ReqConsultaSaldos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1}RespConsultaSaldos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1}ReqConsultaSaldoPromedio"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1}RespConsultaSaldoPromedio"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1}ReqSolicitudChequera"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1}RespSolicitudChequera"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1}ReqConsultaSolicitudChequera"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1}RespConsultaSolicitudChequera"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1}ReqConsultaCBUBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1}RespConsultaCBUBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1}ReqConsultaCBUCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1}RespConsultaCBUCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1}ReqConsultaCBU"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1}RespConsultaCBU"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1}ReqConsultaDetalleBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1}RespConsultaDetalleBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1}ReqConsultaDetalleCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1}RespConsultaDetalleCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1}ReqConsultaDetalle"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1}RespConsultaDetalle"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1}ReqConsultaPersonasVinculadasBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1}RespConsultaPersonasVinculadasBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1}ReqConsultaPersonasVinculadasCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1}RespConsultaPersonasVinculadasCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1}ReqConsultaPersonasVinculadas"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1}RespConsultaPersonasVinculadas"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1}ReqConsultaEmbargosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1}RespConsultaEmbargosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1}ReqConsultaEmbargosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1}RespConsultaEmbargosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1}ReqConsultaEmbargos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1}RespConsultaEmbargos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1}ReqConsultaSchdExtractosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1}RespConsultaSchdExtractosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1}ReqConsultaSchdExtractosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1}RespConsultaSchdExtractosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1}ReqConsultaSchdExtractos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1}RespConsultaSchdExtractos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1}ReqConsultaAcuerdosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1}RespConsultaAcuerdosBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1}ReqConsultaAcuerdosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1}RespConsultaAcuerdosCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1}ReqConsultaAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1}RespConsultaAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1}ReqConsultaDiasAtrasoBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1}RespConsultaDiasAtrasoBAN"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1}ReqConsultaDiasAtrasoCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1}RespConsultaDiasAtrasoCON"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1}ReqConsultaDiasAtraso"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1}RespConsultaDiasAtraso"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1}ReqConsultaChequesEmitidos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1}RespConsultaChequesEmitidos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1}ReqConsultaChequesRecibidos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1}RespConsultaChequesRecibidos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1}ReqConsultaRepositorioExtracto"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1}RespConsultaRepositorioExtracto"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1}ReqAltaAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1}RespAltaAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1}ReqConsultaListadoAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1}RespConsultaListadoAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1}ReqBajaAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1}RespBajaAcuerdos"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1}ReqConsultaOperaciones"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1}RespConsultaOperaciones"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1}ReqConsultaOrdenDeCuenta"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1}RespConsultaOrdenDeCuenta"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1}ReqConsultaListado"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1}RespConsultaListado"/>
 *                             &lt;element ref="{http://www.supervielle.com.ar/xsd/servicios/integracion/exception-v1}exception"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaTransaccionesRespType", propOrder = {
    "row"
})
public class DataConsultaTransaccionesRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaTransaccionesRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaTransaccionesRespType.Row }
     * 
     * 
     */
    public List<DataConsultaTransaccionesRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaTransaccionesRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="respuesta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1}ReqConsultaListado"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1}RespConsultaListado"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1}ReqConsultaListadoBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1}RespConsultaListadoBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1}ReqConsultaListadoCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1}RespConsultaListadoCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1}ReqConsultaMovimientos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1}RespConsultaMovimientos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1}ReqConsultaMovimientosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1}RespConsultaMovimientosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1}ReqConsultaMovimientosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1}RespConsultaMovimientosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1}ReqConsultaSaldos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1}RespConsultaSaldos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1}ReqConsultaSaldoPromedio"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1}RespConsultaSaldoPromedio"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1}ReqSolicitudChequera"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1}RespSolicitudChequera"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1}ReqConsultaSolicitudChequera"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1}RespConsultaSolicitudChequera"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1}ReqConsultaCBUBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1}RespConsultaCBUBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1}ReqConsultaCBUCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1}RespConsultaCBUCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1}ReqConsultaCBU"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1}RespConsultaCBU"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1}ReqConsultaDetalleBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1}RespConsultaDetalleBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1}ReqConsultaDetalleCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1}RespConsultaDetalleCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1}ReqConsultaDetalle"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1}RespConsultaDetalle"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1}ReqConsultaPersonasVinculadasBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1}RespConsultaPersonasVinculadasBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1}ReqConsultaPersonasVinculadasCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1}RespConsultaPersonasVinculadasCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1}ReqConsultaPersonasVinculadas"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1}RespConsultaPersonasVinculadas"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1}ReqConsultaEmbargosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1}RespConsultaEmbargosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1}ReqConsultaEmbargosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1}RespConsultaEmbargosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1}ReqConsultaEmbargos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1}RespConsultaEmbargos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1}ReqConsultaSchdExtractosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1}RespConsultaSchdExtractosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1}ReqConsultaSchdExtractosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1}RespConsultaSchdExtractosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1}ReqConsultaSchdExtractos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1}RespConsultaSchdExtractos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1}ReqConsultaAcuerdosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1}RespConsultaAcuerdosBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1}ReqConsultaAcuerdosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1}RespConsultaAcuerdosCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1}ReqConsultaAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1}RespConsultaAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1}ReqConsultaDiasAtrasoBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1}RespConsultaDiasAtrasoBAN"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1}ReqConsultaDiasAtrasoCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1}RespConsultaDiasAtrasoCON"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1}ReqConsultaDiasAtraso"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1}RespConsultaDiasAtraso"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1}ReqConsultaChequesEmitidos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1}RespConsultaChequesEmitidos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1}ReqConsultaChequesRecibidos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1}RespConsultaChequesRecibidos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1}ReqConsultaRepositorioExtracto"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1}RespConsultaRepositorioExtracto"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1}ReqAltaAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1}RespAltaAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1}ReqConsultaListadoAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1}RespConsultaListadoAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1}ReqBajaAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1}RespBajaAcuerdos"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1}ReqConsultaOperaciones"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1}RespConsultaOperaciones"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1}ReqConsultaOrdenDeCuenta"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1}RespConsultaOrdenDeCuenta"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1}ReqConsultaListado"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1}RespConsultaListado"/>
     *                   &lt;element ref="{http://www.supervielle.com.ar/xsd/servicios/integracion/exception-v1}exception"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "respuesta"
    })
    public static class Row {

        @XmlElement(required = true)
        protected DataConsultaTransaccionesRespType.Row.Respuesta respuesta;

        /**
         * Obtiene el valor de la propiedad respuesta.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaTransaccionesRespType.Row.Respuesta }
         *     
         */
        public DataConsultaTransaccionesRespType.Row.Respuesta getRespuesta() {
            return respuesta;
        }

        /**
         * Define el valor de la propiedad respuesta.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaTransaccionesRespType.Row.Respuesta }
         *     
         */
        public void setRespuesta(DataConsultaTransaccionesRespType.Row.Respuesta value) {
            this.respuesta = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1}ReqConsultaListado"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1}RespConsultaListado"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1}ReqConsultaListadoBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1}RespConsultaListadoBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1}ReqConsultaListadoCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1}RespConsultaListadoCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1}ReqConsultaMovimientos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1}RespConsultaMovimientos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1}ReqConsultaMovimientosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1}RespConsultaMovimientosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1}ReqConsultaMovimientosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1}RespConsultaMovimientosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1}ReqConsultaSaldos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1}RespConsultaSaldos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1}ReqConsultaSaldoPromedio"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1}RespConsultaSaldoPromedio"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1}ReqSolicitudChequera"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1}RespSolicitudChequera"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1}ReqConsultaSolicitudChequera"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1}RespConsultaSolicitudChequera"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1}ReqConsultaCBUBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1}RespConsultaCBUBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1}ReqConsultaCBUCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1}RespConsultaCBUCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1}ReqConsultaCBU"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1}RespConsultaCBU"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1}ReqConsultaDetalleBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1}RespConsultaDetalleBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1}ReqConsultaDetalleCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1}RespConsultaDetalleCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1}ReqConsultaDetalle"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1}RespConsultaDetalle"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1}ReqConsultaPersonasVinculadasBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1}RespConsultaPersonasVinculadasBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1}ReqConsultaPersonasVinculadasCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1}RespConsultaPersonasVinculadasCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1}ReqConsultaPersonasVinculadas"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1}RespConsultaPersonasVinculadas"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1}ReqConsultaEmbargosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1}RespConsultaEmbargosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1}ReqConsultaEmbargosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1}RespConsultaEmbargosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1}ReqConsultaEmbargos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1}RespConsultaEmbargos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1}ReqConsultaSchdExtractosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1}RespConsultaSchdExtractosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1}ReqConsultaSchdExtractosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1}RespConsultaSchdExtractosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1}ReqConsultaSchdExtractos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1}RespConsultaSchdExtractos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1}ReqConsultaAcuerdosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1}RespConsultaAcuerdosBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1}ReqConsultaAcuerdosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1}RespConsultaAcuerdosCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1}ReqConsultaAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1}RespConsultaAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1}ReqConsultaDiasAtrasoBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1}RespConsultaDiasAtrasoBAN"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1}ReqConsultaDiasAtrasoCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1}RespConsultaDiasAtrasoCON"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1}ReqConsultaDiasAtraso"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1}RespConsultaDiasAtraso"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1}ReqConsultaChequesEmitidos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1}RespConsultaChequesEmitidos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1}ReqConsultaChequesRecibidos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1}RespConsultaChequesRecibidos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1}ReqConsultaRepositorioExtracto"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1}RespConsultaRepositorioExtracto"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1}ReqAltaAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1}RespAltaAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1}ReqConsultaListadoAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1}RespConsultaListadoAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1}ReqBajaAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1}RespBajaAcuerdos"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1}ReqConsultaOperaciones"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1}RespConsultaOperaciones"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1}ReqConsultaOrdenDeCuenta"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1}RespConsultaOrdenDeCuenta"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1}ReqConsultaListado"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1}RespConsultaListado"/>
         *         &lt;element ref="{http://www.supervielle.com.ar/xsd/servicios/integracion/exception-v1}exception"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "content"
        })
        public static class Respuesta {

            @XmlElementRefs({
                @XmlElementRef(name = "ReqConsultaDiasAtrasoCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1", type = ReqConsultaDiasAtrasoCON.class, required = false),
                @XmlElementRef(name = "RespConsultaCBUCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1", type = RespConsultaCBUCON.class, required = false),
                @XmlElementRef(name = "RespConsultaCBUBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1", type = RespConsultaCBUBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaSolicitudChequera", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1", type = RespConsultaSolicitudChequera.class, required = false),
                @XmlElementRef(name = "RespConsultaChequesRecibidos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1", type = RespConsultaChequesRecibidos2 .class, required = false),
                @XmlElementRef(name = "exception", namespace = "http://www.supervielle.com.ar/xsd/servicios/integracion/exception-v1", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "RespConsultaOperaciones", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1", type = RespConsultaOperaciones.class, required = false),
                @XmlElementRef(name = "RespConsultaMovimientosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1", type = RespConsultaMovimientosCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaCBUBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUBAN-v1", type = ReqConsultaCBUBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaEmbargos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1", type = ReqConsultaEmbargos.class, required = false),
                @XmlElementRef(name = "RespConsultaRepositorioExtracto", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1", type = RespConsultaRepositorioExtracto.class, required = false),
                @XmlElementRef(name = "ReqConsultaChequesRecibidos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesRecibidos-v1", type = ReqConsultaChequesRecibidos2 .class, required = false),
                @XmlElementRef(name = "ReqConsultaMovimientosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosCON-v1", type = ReqConsultaMovimientosCON.class, required = false),
                @XmlElementRef(name = "RespBajaAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1", type = RespBajaAcuerdos.class, required = false),
                @XmlElementRef(name = "ReqConsultaListadoAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1", type = ReqConsultaListadoAcuerdos.class, required = false),
                @XmlElementRef(name = "ReqConsultaListado", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1", type = ReqConsultaListado3 .class, required = false),
                @XmlElementRef(name = "ReqConsultaSchdExtractos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1", type = ReqConsultaSchdExtractos.class, required = false),
                @XmlElementRef(name = "RespConsultaSaldoPromedio", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1", type = RespConsultaSaldoPromedio.class, required = false),
                @XmlElementRef(name = "RespSolicitudChequera", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1", type = RespSolicitudChequera.class, required = false),
                @XmlElementRef(name = "ReqConsultaAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1", type = ReqConsultaAcuerdos.class, required = false),
                @XmlElementRef(name = "ReqConsultaDiasAtraso", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1", type = ReqConsultaDiasAtraso.class, required = false),
                @XmlElementRef(name = "ReqConsultaMovimientosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1", type = ReqConsultaMovimientosBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaListadoAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoAcuerdos-v1", type = RespConsultaListadoAcuerdos.class, required = false),
                @XmlElementRef(name = "RespConsultaMovimientosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientosBAN-v1", type = RespConsultaMovimientosBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaSchdExtractosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1", type = ReqConsultaSchdExtractosBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaSaldos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1", type = ReqConsultaSaldos.class, required = false),
                @XmlElementRef(name = "RespConsultaSchdExtractosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1", type = RespConsultaSchdExtractosCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaPersonasVinculadasCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1", type = ReqConsultaPersonasVinculadasCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaListadoCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1", type = ReqConsultaListadoCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaOrdenDeCuenta", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1", type = ReqConsultaOrdenDeCuenta.class, required = false),
                @XmlElementRef(name = "RespConsultaListado", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1", type = RespConsultaListado3 .class, required = false),
                @XmlElementRef(name = "RespAltaAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1", type = RespAltaAcuerdos.class, required = false),
                @XmlElementRef(name = "ReqAltaAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/altaAcuerdos-v1", type = ReqAltaAcuerdos.class, required = false),
                @XmlElementRef(name = "ReqConsultaRepositorioExtracto", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaRepositorioExtracto-v1", type = ReqConsultaRepositorioExtracto.class, required = false),
                @XmlElementRef(name = "RespConsultaPersonasVinculadasBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1", type = RespConsultaPersonasVinculadasBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaOrdenDeCuenta", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOrdenDeCuenta-v1", type = RespConsultaOrdenDeCuenta.class, required = false),
                @XmlElementRef(name = "ReqConsultaCBU", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1", type = ReqConsultaCBU.class, required = false),
                @XmlElementRef(name = "ReqConsultaAcuerdosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1", type = ReqConsultaAcuerdosBAN.class, required = false),
                @XmlElementRef(name = "ReqBajaAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/bajaAcuerdos-v1", type = ReqBajaAcuerdos.class, required = false),
                @XmlElementRef(name = "RespConsultaDetalleCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1", type = RespConsultaDetalleCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaPersonasVinculadasBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasBAN-v1", type = ReqConsultaPersonasVinculadasBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaAcuerdosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1", type = ReqConsultaAcuerdosCON.class, required = false),
                @XmlElementRef(name = "RespConsultaListadoCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoCON-v1", type = RespConsultaListadoCON.class, required = false),
                @XmlElementRef(name = "ReqSolicitudChequera", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/solicitudChequera-v1", type = ReqSolicitudChequera.class, required = false),
                @XmlElementRef(name = "RespConsultaDetalle", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1", type = RespConsultaDetalle2 .class, required = false),
                @XmlElementRef(name = "ReqConsultaDiasAtrasoBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1", type = ReqConsultaDiasAtrasoBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaAcuerdosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosCON-v1", type = RespConsultaAcuerdosCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaOperaciones", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaOperaciones-v1", type = ReqConsultaOperaciones.class, required = false),
                @XmlElementRef(name = "ReqConsultaSaldoPromedio", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldoPromedio-v1", type = ReqConsultaSaldoPromedio.class, required = false),
                @XmlElementRef(name = "ReqConsultaSchdExtractosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosCON-v1", type = ReqConsultaSchdExtractosCON.class, required = false),
                @XmlElementRef(name = "RespConsultaSaldos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSaldos-v1", type = RespConsultaSaldos.class, required = false),
                @XmlElementRef(name = "ReqConsultaEmbargosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1", type = ReqConsultaEmbargosCON.class, required = false),
                @XmlElementRef(name = "RespConsultaDiasAtrasoCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoCON-v1", type = RespConsultaDiasAtrasoCON.class, required = false),
                @XmlElementRef(name = "ReqConsultaListado", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1", type = ReqConsultaListado2 .class, required = false),
                @XmlElementRef(name = "RespConsultaMovimientos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1", type = RespConsultaMovimientos2 .class, required = false),
                @XmlElementRef(name = "RespConsultaSchdExtractosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractosBAN-v1", type = RespConsultaSchdExtractosBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaPersonasVinculadasCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadasCON-v1", type = RespConsultaPersonasVinculadasCON.class, required = false),
                @XmlElementRef(name = "RespConsultaEmbargosCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosCON-v1", type = RespConsultaEmbargosCON.class, required = false),
                @XmlElementRef(name = "RespConsultaChequesEmitidos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1", type = RespConsultaChequesEmitidos2 .class, required = false),
                @XmlElementRef(name = "ReqConsultaSolicitudChequera", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSolicitudChequera-v1", type = ReqConsultaSolicitudChequera.class, required = false),
                @XmlElementRef(name = "ReqConsultaCBUCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBUCON-v1", type = ReqConsultaCBUCON.class, required = false),
                @XmlElementRef(name = "RespConsultaDiasAtraso", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtraso-v1", type = RespConsultaDiasAtraso.class, required = false),
                @XmlElementRef(name = "RespConsultaListadoBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1", type = RespConsultaListadoBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaEmbargos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargos-v1", type = RespConsultaEmbargos.class, required = false),
                @XmlElementRef(name = "ReqConsultaListadoBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListadoBAN-v1", type = ReqConsultaListadoBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaAcuerdosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdosBAN-v1", type = RespConsultaAcuerdosBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaChequesEmitidos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaChequesEmitidos-v1", type = ReqConsultaChequesEmitidos2 .class, required = false),
                @XmlElementRef(name = "RespConsultaSchdExtractos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaSchdExtractos-v1", type = RespConsultaSchdExtractos.class, required = false),
                @XmlElementRef(name = "RespConsultaPersonasVinculadas", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1", type = RespConsultaPersonasVinculadas.class, required = false),
                @XmlElementRef(name = "ReqConsultaPersonasVinculadas", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaPersonasVinculadas-v1", type = ReqConsultaPersonasVinculadas.class, required = false),
                @XmlElementRef(name = "RespConsultaDiasAtrasoBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDiasAtrasoBAN-v1", type = RespConsultaDiasAtrasoBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaListado", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.1", type = RespConsultaListado2 .class, required = false),
                @XmlElementRef(name = "RespConsultaDetalleBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1", type = RespConsultaDetalleBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaEmbargosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1", type = RespConsultaEmbargosBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaDetalleBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleBAN-v1", type = ReqConsultaDetalleBAN.class, required = false),
                @XmlElementRef(name = "RespConsultaAcuerdos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaAcuerdos-v1", type = RespConsultaAcuerdos.class, required = false),
                @XmlElementRef(name = "ReqConsultaMovimientos", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1", type = ReqConsultaMovimientos2 .class, required = false),
                @XmlElementRef(name = "ReqConsultaEmbargosBAN", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaEmbargosBAN-v1", type = ReqConsultaEmbargosBAN.class, required = false),
                @XmlElementRef(name = "ReqConsultaDetalle", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalle-v1", type = ReqConsultaDetalle2 .class, required = false),
                @XmlElementRef(name = "ReqConsultaDetalleCON", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaDetalleCON-v1", type = ReqConsultaDetalleCON.class, required = false),
                @XmlElementRef(name = "RespConsultaCBU", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaCBU-v1", type = RespConsultaCBU.class, required = false)
            })
            protected List<Object> content;

            /**
             * Obtiene el resto del modelo de contenido. 
             * 
             * <p>
             * Ha obtenido esta propiedad que permite capturar todo por el siguiente motivo: 
             * El nombre de campo "ReqConsultaListado" se est� utilizando en dos partes diferentes de un esquema. Consulte:
             * l�nea 0 de http://dasoa-01:9812/wsdl/servicios/integracion/cuenta/ctaCte-v1.2?xsd=xsd29
             * l�nea 0 de http://dasoa-01:9812/wsdl/servicios/integracion/cuenta/ctaCte-v1.2?xsd=xsd29
             * <p>
             * Para deshacerse de esta propiedad, aplique una personalizaci�n de propiedad a una
             * de las dos declaraciones siguientes para cambiarles de nombre: 
             * Gets the value of the content property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the content property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContent().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ReqConsultaDiasAtrasoCON }
             * {@link RespConsultaCBUCON }
             * {@link RespConsultaCBUBAN }
             * {@link RespConsultaSolicitudChequera }
             * {@link RespConsultaChequesRecibidos2 }
             * {@link JAXBElement }{@code <}{@link Exception }{@code >}
             * {@link RespConsultaOperaciones }
             * {@link RespConsultaMovimientosCON }
             * {@link ReqConsultaCBUBAN }
             * {@link ReqConsultaEmbargos }
             * {@link RespConsultaRepositorioExtracto }
             * {@link ReqConsultaChequesRecibidos2 }
             * {@link ReqConsultaMovimientosCON }
             * {@link RespBajaAcuerdos }
             * {@link ReqConsultaListadoAcuerdos }
             * {@link ReqConsultaListado3 }
             * {@link ReqConsultaSchdExtractos }
             * {@link RespConsultaSaldoPromedio }
             * {@link RespSolicitudChequera }
             * {@link ReqConsultaAcuerdos }
             * {@link ReqConsultaDiasAtraso }
             * {@link ReqConsultaMovimientosBAN }
             * {@link RespConsultaListadoAcuerdos }
             * {@link RespConsultaMovimientosBAN }
             * {@link ReqConsultaSchdExtractosBAN }
             * {@link ReqConsultaSaldos }
             * {@link RespConsultaSchdExtractosCON }
             * {@link ReqConsultaPersonasVinculadasCON }
             * {@link ReqConsultaListadoCON }
             * {@link ReqConsultaOrdenDeCuenta }
             * {@link RespConsultaListado3 }
             * {@link RespAltaAcuerdos }
             * {@link ReqAltaAcuerdos }
             * {@link ReqConsultaRepositorioExtracto }
             * {@link RespConsultaPersonasVinculadasBAN }
             * {@link RespConsultaOrdenDeCuenta }
             * {@link ReqConsultaCBU }
             * {@link ReqConsultaAcuerdosBAN }
             * {@link ReqBajaAcuerdos }
             * {@link RespConsultaDetalleCON }
             * {@link ReqConsultaPersonasVinculadasBAN }
             * {@link ReqConsultaAcuerdosCON }
             * {@link RespConsultaListadoCON }
             * {@link ReqSolicitudChequera }
             * {@link RespConsultaDetalle2 }
             * {@link ReqConsultaDiasAtrasoBAN }
             * {@link RespConsultaAcuerdosCON }
             * {@link ReqConsultaOperaciones }
             * {@link ReqConsultaSaldoPromedio }
             * {@link ReqConsultaSchdExtractosCON }
             * {@link RespConsultaSaldos }
             * {@link ReqConsultaEmbargosCON }
             * {@link RespConsultaDiasAtrasoCON }
             * {@link ReqConsultaListado2 }
             * {@link RespConsultaMovimientos2 }
             * {@link RespConsultaSchdExtractosBAN }
             * {@link RespConsultaPersonasVinculadasCON }
             * {@link RespConsultaEmbargosCON }
             * {@link RespConsultaChequesEmitidos2 }
             * {@link ReqConsultaSolicitudChequera }
             * {@link ReqConsultaCBUCON }
             * {@link RespConsultaDiasAtraso }
             * {@link RespConsultaListadoBAN }
             * {@link RespConsultaEmbargos }
             * {@link ReqConsultaListadoBAN }
             * {@link RespConsultaAcuerdosBAN }
             * {@link ReqConsultaChequesEmitidos2 }
             * {@link RespConsultaSchdExtractos }
             * {@link RespConsultaPersonasVinculadas }
             * {@link ReqConsultaPersonasVinculadas }
             * {@link RespConsultaDiasAtrasoBAN }
             * {@link RespConsultaListado2 }
             * {@link RespConsultaDetalleBAN }
             * {@link RespConsultaEmbargosBAN }
             * {@link ReqConsultaDetalleBAN }
             * {@link RespConsultaAcuerdos }
             * {@link ReqConsultaMovimientos2 }
             * {@link ReqConsultaEmbargosBAN }
             * {@link ReqConsultaDetalle2 }
             * {@link ReqConsultaDetalleCON }
             * {@link RespConsultaCBU }
             * 
             * 
             */
            public List<Object> getContent() {
                if (content == null) {
                    content = new ArrayList<Object>();
                }
                return this.content;
            }

        }

    }

}
