
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.solicitudchequera_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataSolicitudChequeraReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataSolicitudChequeraReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *         &lt;element name="tipoChequera" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSolicitudChequeraReqType", propOrder = {
    "identificador",
    "tipoChequera",
    "cantidad"
})
public class DataSolicitudChequeraReqType {

    @XmlElement(required = true)
    protected IdCuentaBANTOTALType identificador;
    @XmlElement(required = true)
    protected String tipoChequera;
    protected int cantidad;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdCuentaBANTOTALType }
     *     
     */
    public IdCuentaBANTOTALType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdCuentaBANTOTALType }
     *     
     */
    public void setIdentificador(IdCuentaBANTOTALType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoChequera.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoChequera() {
        return tipoChequera;
    }

    /**
     * Define el valor de la propiedad tipoChequera.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoChequera(String value) {
        this.tipoChequera = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidad.
     * 
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Define el valor de la propiedad cantidad.
     * 
     */
    public void setCantidad(int value) {
        this.cantidad = value;
    }

}
