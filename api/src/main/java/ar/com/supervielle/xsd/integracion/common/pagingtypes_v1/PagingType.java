
package ar.com.supervielle.xsd.integracion.common.pagingtypes_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para pagingType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pagingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pageSize">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="1"/>
 *               &lt;maxInclusive value="500"/>
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="getPage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="totalRows" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pagingType", propOrder = {
    "pageSize",
    "getPage",
    "totalRows"
})
public class PagingType {

    protected int pageSize;
    protected int getPage;
    protected BigInteger totalRows;

    /**
     * Obtiene el valor de la propiedad pageSize.
     * 
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Define el valor de la propiedad pageSize.
     * 
     */
    public void setPageSize(int value) {
        this.pageSize = value;
    }

    /**
     * Obtiene el valor de la propiedad getPage.
     * 
     */
    public int getGetPage() {
        return getPage;
    }

    /**
     * Define el valor de la propiedad getPage.
     * 
     */
    public void setGetPage(int value) {
        this.getPage = value;
    }

    /**
     * Obtiene el valor de la propiedad totalRows.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalRows() {
        return totalRows;
    }

    /**
     * Define el valor de la propiedad totalRows.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalRows(BigInteger value) {
        this.totalRows = value;
    }

}
