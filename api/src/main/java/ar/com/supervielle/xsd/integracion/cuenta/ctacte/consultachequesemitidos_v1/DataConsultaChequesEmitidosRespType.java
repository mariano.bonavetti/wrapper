
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesemitidos_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaChequesEmitidosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaChequesEmitidosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroCheque" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="fechaDeposito" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="EntidadReceptora">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaChequesEmitidosRespType", propOrder = {
    "row"
})
public class DataConsultaChequesEmitidosRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaChequesEmitidosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaChequesEmitidosRespType.Row }
     * 
     * 
     */
    public List<DataConsultaChequesEmitidosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaChequesEmitidosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroCheque" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="fechaDeposito" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="EntidadReceptora">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroCheque",
        "importe",
        "estado",
        "fechaDeposito",
        "entidadReceptora"
    })
    public static class Row {

        protected int nroCheque;
        @XmlElement(required = true)
        protected BigDecimal importe;
        @XmlElement(name = "Estado", required = true)
        protected CodDescStringType estado;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaDeposito;
        @XmlElement(name = "EntidadReceptora", required = true)
        protected DataConsultaChequesEmitidosRespType.Row.EntidadReceptora entidadReceptora;

        /**
         * Obtiene el valor de la propiedad nroCheque.
         * 
         */
        public int getNroCheque() {
            return nroCheque;
        }

        /**
         * Define el valor de la propiedad nroCheque.
         * 
         */
        public void setNroCheque(int value) {
            this.nroCheque = value;
        }

        /**
         * Obtiene el valor de la propiedad importe.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getImporte() {
            return importe;
        }

        /**
         * Define el valor de la propiedad importe.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setImporte(BigDecimal value) {
            this.importe = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaDeposito.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaDeposito() {
            return fechaDeposito;
        }

        /**
         * Define el valor de la propiedad fechaDeposito.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaDeposito(XMLGregorianCalendar value) {
            this.fechaDeposito = value;
        }

        /**
         * Obtiene el valor de la propiedad entidadReceptora.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaChequesEmitidosRespType.Row.EntidadReceptora }
         *     
         */
        public DataConsultaChequesEmitidosRespType.Row.EntidadReceptora getEntidadReceptora() {
            return entidadReceptora;
        }

        /**
         * Define el valor de la propiedad entidadReceptora.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaChequesEmitidosRespType.Row.EntidadReceptora }
         *     
         */
        public void setEntidadReceptora(DataConsultaChequesEmitidosRespType.Row.EntidadReceptora value) {
            this.entidadReceptora = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "banco",
            "cuenta",
            "sucursal"
        })
        public static class EntidadReceptora {

            protected int banco;
            @XmlElement(required = true)
            protected String cuenta;
            protected int sucursal;

            /**
             * Obtiene el valor de la propiedad banco.
             * 
             */
            public int getBanco() {
                return banco;
            }

            /**
             * Define el valor de la propiedad banco.
             * 
             */
            public void setBanco(int value) {
                this.banco = value;
            }

            /**
             * Obtiene el valor de la propiedad cuenta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuenta() {
                return cuenta;
            }

            /**
             * Define el valor de la propiedad cuenta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuenta(String value) {
                this.cuenta = value;
            }

            /**
             * Obtiene el valor de la propiedad sucursal.
             * 
             */
            public int getSucursal() {
                return sucursal;
            }

            /**
             * Define el valor de la propiedad sucursal.
             * 
             */
            public void setSucursal(int value) {
                this.sucursal = value;
            }

        }

    }

}
