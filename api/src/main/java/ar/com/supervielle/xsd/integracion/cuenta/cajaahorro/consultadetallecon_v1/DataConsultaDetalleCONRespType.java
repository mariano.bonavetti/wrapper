
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetallecon_v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.TipoMonedaType;


/**
 * <p>Clase Java para DataConsultaDetalleCONRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDetalleCONRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
 *                   &lt;element name="sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="modulo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="moneda" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}tipoMonedaType"/>
 *                   &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="papel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDetalleCONRespType", propOrder = {
    "row"
})
public class DataConsultaDetalleCONRespType {

    @XmlElement(name = "Row")
    protected DataConsultaDetalleCONRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaDetalleCONRespType.Row }
     *     
     */
    public DataConsultaDetalleCONRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaDetalleCONRespType.Row }
     *     
     */
    public void setRow(DataConsultaDetalleCONRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
     *         &lt;element name="sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="modulo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="moneda" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}tipoMonedaType"/>
     *         &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="papel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "sucursal",
        "cuentaCliente",
        "subOperacion",
        "modulo",
        "moneda",
        "tipoOperacion",
        "operacion",
        "papel",
        "estado",
        "saldo",
        "titularidad",
        "paquete"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaCONSUMOType identificador;
        @XmlElement(required = true)
        protected CodDescStringType sucursal;
        @XmlElement(required = true)
        protected String cuentaCliente;
        @XmlElement(required = true)
        protected String subOperacion;
        @XmlElement(required = true)
        protected CodDescStringType modulo;
        @XmlElement(required = true)
        protected TipoMonedaType moneda;
        @XmlElement(required = true)
        protected String tipoOperacion;
        @XmlElement(required = true)
        protected String operacion;
        @XmlElement(required = true)
        protected String papel;
        @XmlElement(required = true)
        protected CodDescStringType estado;
        protected BigDecimal saldo;
        @XmlElement(required = true)
        protected CodDescStringType titularidad;
        @XmlElement(required = true)
        protected CodDescStringType paquete;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public IdCuentaCONSUMOType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public void setIdentificador(IdCuentaCONSUMOType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setSucursal(CodDescStringType value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaCliente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaCliente() {
            return cuentaCliente;
        }

        /**
         * Define el valor de la propiedad cuentaCliente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaCliente(String value) {
            this.cuentaCliente = value;
        }

        /**
         * Obtiene el valor de la propiedad subOperacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubOperacion() {
            return subOperacion;
        }

        /**
         * Define el valor de la propiedad subOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubOperacion(String value) {
            this.subOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad modulo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getModulo() {
            return modulo;
        }

        /**
         * Define el valor de la propiedad modulo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setModulo(CodDescStringType value) {
            this.modulo = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link TipoMonedaType }
         *     
         */
        public TipoMonedaType getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoMonedaType }
         *     
         */
        public void setMoneda(TipoMonedaType value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoOperacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoOperacion() {
            return tipoOperacion;
        }

        /**
         * Define el valor de la propiedad tipoOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoOperacion(String value) {
            this.tipoOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad operacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperacion() {
            return operacion;
        }

        /**
         * Define el valor de la propiedad operacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperacion(String value) {
            this.operacion = value;
        }

        /**
         * Obtiene el valor de la propiedad papel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPapel() {
            return papel;
        }

        /**
         * Define el valor de la propiedad papel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPapel(String value) {
            this.papel = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad saldo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldo() {
            return saldo;
        }

        /**
         * Define el valor de la propiedad saldo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldo(BigDecimal value) {
            this.saldo = value;
        }

        /**
         * Obtiene el valor de la propiedad titularidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTitularidad() {
            return titularidad;
        }

        /**
         * Define el valor de la propiedad titularidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTitularidad(CodDescStringType value) {
            this.titularidad = value;
        }

        /**
         * Obtiene el valor de la propiedad paquete.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getPaquete() {
            return paquete;
        }

        /**
         * Define el valor de la propiedad paquete.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setPaquete(CodDescStringType value) {
            this.paquete = value;
        }

    }

}
