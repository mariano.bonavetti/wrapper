
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetallecon_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;


/**
 * <p>Clase Java para DataConsultaDetalleCONReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDetalleCONReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDetalleCONReqType", propOrder = {
    "identificador"
})
public class DataConsultaDetalleCONReqType {

    @XmlElement(required = true)
    protected IdCuentaCONSUMOType identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdCuentaCONSUMOType }
     *     
     */
    public IdCuentaCONSUMOType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdCuentaCONSUMOType }
     *     
     */
    public void setIdentificador(IdCuentaCONSUMOType value) {
        this.identificador = value;
    }

}
