
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaultdetalleliquidacion_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaultdetalleliquidacion_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaultdetalleliquidacion_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RespConsultaUltDetalleLiquidacion }
     * 
     */
    public RespConsultaUltDetalleLiquidacion createRespConsultaUltDetalleLiquidacion() {
        return new RespConsultaUltDetalleLiquidacion();
    }

    /**
     * Create an instance of {@link DataConsultaUltDetalleLiquidacionRespType }
     * 
     */
    public DataConsultaUltDetalleLiquidacionRespType createDataConsultaUltDetalleLiquidacionRespType() {
        return new DataConsultaUltDetalleLiquidacionRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaUltDetalleLiquidacion }
     * 
     */
    public ReqConsultaUltDetalleLiquidacion createReqConsultaUltDetalleLiquidacion() {
        return new ReqConsultaUltDetalleLiquidacion();
    }

    /**
     * Create an instance of {@link DataConsultaUltDetalleLiquidacionReqType }
     * 
     */
    public DataConsultaUltDetalleLiquidacionReqType createDataConsultaUltDetalleLiquidacionReqType() {
        return new DataConsultaUltDetalleLiquidacionReqType();
    }

}
