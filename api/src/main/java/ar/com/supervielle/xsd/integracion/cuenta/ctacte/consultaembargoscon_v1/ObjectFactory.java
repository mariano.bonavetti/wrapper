
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargoscon_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargoscon_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaembargoscon_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataRespConsultaEmbargosCONType }
     * 
     */
    public DataRespConsultaEmbargosCONType createDataRespConsultaEmbargosCONType() {
        return new DataRespConsultaEmbargosCONType();
    }

    /**
     * Create an instance of {@link DataRespConsultaEmbargosCONType.Row }
     * 
     */
    public DataRespConsultaEmbargosCONType.Row createDataRespConsultaEmbargosCONTypeRow() {
        return new DataRespConsultaEmbargosCONType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaEmbargosCON }
     * 
     */
    public ReqConsultaEmbargosCON createReqConsultaEmbargosCON() {
        return new ReqConsultaEmbargosCON();
    }

    /**
     * Create an instance of {@link DataReqConsultaEmbargosCONType }
     * 
     */
    public DataReqConsultaEmbargosCONType createDataReqConsultaEmbargosCONType() {
        return new DataReqConsultaEmbargosCONType();
    }

    /**
     * Create an instance of {@link RespConsultaEmbargosCON }
     * 
     */
    public RespConsultaEmbargosCON createRespConsultaEmbargosCON() {
        return new RespConsultaEmbargosCON();
    }

    /**
     * Create an instance of {@link DataRespConsultaEmbargosCONType.Row.Oficio }
     * 
     */
    public DataRespConsultaEmbargosCONType.Row.Oficio createDataRespConsultaEmbargosCONTypeRowOficio() {
        return new DataRespConsultaEmbargosCONType.Row.Oficio();
    }

}
