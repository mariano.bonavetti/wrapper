
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FuncionBean complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FuncionBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complejo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}ComplejoBean" minOccurs="0"/>
 *         &lt;element name="fechaFuncion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}FechaGregorianCalendar" minOccurs="0"/>
 *         &lt;element name="idioma" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}ComboBean" minOccurs="0"/>
 *         &lt;element name="informacionVenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}InformacionVentaBean" minOccurs="0"/>
 *         &lt;element name="pelicula" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}PeliculaBean" minOccurs="0"/>
 *         &lt;element name="sala" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FuncionBean", propOrder = {
    "codigo",
    "complejo",
    "fechaFuncion",
    "idioma",
    "informacionVenta",
    "pelicula",
    "sala"
})
public class FuncionBean {

    protected String codigo;
    protected ComplejoBean complejo;
    protected FechaGregorianCalendar fechaFuncion;
    protected ComboBean idioma;
    protected InformacionVentaBean informacionVenta;
    protected PeliculaBean pelicula;
    protected String sala;

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad complejo.
     * 
     * @return
     *     possible object is
     *     {@link ComplejoBean }
     *     
     */
    public ComplejoBean getComplejo() {
        return complejo;
    }

    /**
     * Define el valor de la propiedad complejo.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplejoBean }
     *     
     */
    public void setComplejo(ComplejoBean value) {
        this.complejo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFuncion.
     * 
     * @return
     *     possible object is
     *     {@link FechaGregorianCalendar }
     *     
     */
    public FechaGregorianCalendar getFechaFuncion() {
        return fechaFuncion;
    }

    /**
     * Define el valor de la propiedad fechaFuncion.
     * 
     * @param value
     *     allowed object is
     *     {@link FechaGregorianCalendar }
     *     
     */
    public void setFechaFuncion(FechaGregorianCalendar value) {
        this.fechaFuncion = value;
    }

    /**
     * Obtiene el valor de la propiedad idioma.
     * 
     * @return
     *     possible object is
     *     {@link ComboBean }
     *     
     */
    public ComboBean getIdioma() {
        return idioma;
    }

    /**
     * Define el valor de la propiedad idioma.
     * 
     * @param value
     *     allowed object is
     *     {@link ComboBean }
     *     
     */
    public void setIdioma(ComboBean value) {
        this.idioma = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionVenta.
     * 
     * @return
     *     possible object is
     *     {@link InformacionVentaBean }
     *     
     */
    public InformacionVentaBean getInformacionVenta() {
        return informacionVenta;
    }

    /**
     * Define el valor de la propiedad informacionVenta.
     * 
     * @param value
     *     allowed object is
     *     {@link InformacionVentaBean }
     *     
     */
    public void setInformacionVenta(InformacionVentaBean value) {
        this.informacionVenta = value;
    }

    /**
     * Obtiene el valor de la propiedad pelicula.
     * 
     * @return
     *     possible object is
     *     {@link PeliculaBean }
     *     
     */
    public PeliculaBean getPelicula() {
        return pelicula;
    }

    /**
     * Define el valor de la propiedad pelicula.
     * 
     * @param value
     *     allowed object is
     *     {@link PeliculaBean }
     *     
     */
    public void setPelicula(PeliculaBean value) {
        this.pelicula = value;
    }

    /**
     * Obtiene el valor de la propiedad sala.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSala() {
        return sala;
    }

    /**
     * Define el valor de la propiedad sala.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSala(String value) {
        this.sala = value;
    }

}
