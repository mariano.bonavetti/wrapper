
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmultixmarca_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaParametrosMultixMarcaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaParametrosMultixMarcaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="respuestas">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="respuesta" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaParametrosMultixMarcaRespType", propOrder = {
    "respuestas"
})
public class DataConsultaParametrosMultixMarcaRespType {

    @XmlElement(required = true)
    protected DataConsultaParametrosMultixMarcaRespType.Respuestas respuestas;

    /**
     * Obtiene el valor de la propiedad respuestas.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaParametrosMultixMarcaRespType.Respuestas }
     *     
     */
    public DataConsultaParametrosMultixMarcaRespType.Respuestas getRespuestas() {
        return respuestas;
    }

    /**
     * Define el valor de la propiedad respuestas.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaParametrosMultixMarcaRespType.Respuestas }
     *     
     */
    public void setRespuestas(DataConsultaParametrosMultixMarcaRespType.Respuestas value) {
        this.respuestas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="respuesta" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "respuesta"
    })
    public static class Respuestas {

        @XmlElement(required = true)
        protected List<Object> respuesta;

        /**
         * Gets the value of the respuesta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the respuesta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRespuesta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRespuesta() {
            if (respuesta == null) {
                respuesta = new ArrayList<Object>();
            }
            return this.respuesta;
        }

    }

}
