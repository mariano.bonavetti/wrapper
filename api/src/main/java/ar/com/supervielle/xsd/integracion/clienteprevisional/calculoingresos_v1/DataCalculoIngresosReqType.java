
package ar.com.supervielle.xsd.integracion.clienteprevisional.calculoingresos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataCalculoIngresosReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataCalculoIngresosReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataCalculoIngresosReqType", propOrder = {
    "cuil"
})
public class DataCalculoIngresosReqType {

    @XmlElement(name = "CUIL", required = true)
    protected String cuil;

    /**
     * Obtiene el valor de la propiedad cuil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUIL() {
        return cuil;
    }

    /**
     * Define el valor de la propiedad cuil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUIL(String value) {
        this.cuil = value;
    }

}
