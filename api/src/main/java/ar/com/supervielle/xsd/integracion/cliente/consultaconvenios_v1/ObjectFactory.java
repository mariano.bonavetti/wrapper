
package ar.com.supervielle.xsd.integracion.cliente.consultaconvenios_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultaconvenios_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultaconvenios_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaConveniosRespType }
     * 
     */
    public DataConsultaConveniosRespType createDataConsultaConveniosRespType() {
        return new DataConsultaConveniosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaConveniosRespType.Row }
     * 
     */
    public DataConsultaConveniosRespType.Row createDataConsultaConveniosRespTypeRow() {
        return new DataConsultaConveniosRespType.Row();
    }

    /**
     * Create an instance of {@link RespConsultaConvenios }
     * 
     */
    public RespConsultaConvenios createRespConsultaConvenios() {
        return new RespConsultaConvenios();
    }

    /**
     * Create an instance of {@link ReqConsultaConvenios }
     * 
     */
    public ReqConsultaConvenios createReqConsultaConvenios() {
        return new ReqConsultaConvenios();
    }

    /**
     * Create an instance of {@link DataConsultaConveniosReqType }
     * 
     */
    public DataConsultaConveniosReqType createDataConsultaConveniosReqType() {
        return new DataConsultaConveniosReqType();
    }

    /**
     * Create an instance of {@link DataConsultaConveniosRespType.Row.Linea }
     * 
     */
    public DataConsultaConveniosRespType.Row.Linea createDataConsultaConveniosRespTypeRowLinea() {
        return new DataConsultaConveniosRespType.Row.Linea();
    }

}
