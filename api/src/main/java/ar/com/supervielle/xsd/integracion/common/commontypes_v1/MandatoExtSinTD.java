
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para mandatoExtSinTD complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="mandatoExtSinTD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="baja" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="codigoIdentificacionMandato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="montoMandato" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="nroDocBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mandatoExtSinTD", propOrder = {
    "baja",
    "codigoIdentificacionMandato",
    "fechaVencimiento",
    "montoMandato",
    "nroDocBeneficiario",
    "tipoDocBeneficiario"
})
public class MandatoExtSinTD {

    protected boolean baja;
    protected String codigoIdentificacionMandato;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaVencimiento;
    protected double montoMandato;
    protected String nroDocBeneficiario;
    protected String tipoDocBeneficiario;

    /**
     * Obtiene el valor de la propiedad baja.
     * 
     */
    public boolean isBaja() {
        return baja;
    }

    /**
     * Define el valor de la propiedad baja.
     * 
     */
    public void setBaja(boolean value) {
        this.baja = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoIdentificacionMandato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoIdentificacionMandato() {
        return codigoIdentificacionMandato;
    }

    /**
     * Define el valor de la propiedad codigoIdentificacionMandato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoIdentificacionMandato(String value) {
        this.codigoIdentificacionMandato = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Define el valor de la propiedad fechaVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaVencimiento(XMLGregorianCalendar value) {
        this.fechaVencimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad montoMandato.
     * 
     */
    public double getMontoMandato() {
        return montoMandato;
    }

    /**
     * Define el valor de la propiedad montoMandato.
     * 
     */
    public void setMontoMandato(double value) {
        this.montoMandato = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDocBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDocBeneficiario() {
        return nroDocBeneficiario;
    }

    /**
     * Define el valor de la propiedad nroDocBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDocBeneficiario(String value) {
        this.nroDocBeneficiario = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocBeneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocBeneficiario() {
        return tipoDocBeneficiario;
    }

    /**
     * Define el valor de la propiedad tipoDocBeneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocBeneficiario(String value) {
        this.tipoDocBeneficiario = value;
    }

}
