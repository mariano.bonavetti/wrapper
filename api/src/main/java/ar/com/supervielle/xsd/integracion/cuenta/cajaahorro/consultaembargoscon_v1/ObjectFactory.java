
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaembargoscon_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaembargoscon_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaembargoscon_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosCONRespType }
     * 
     */
    public DataConsultaEmbargosCONRespType createDataConsultaEmbargosCONRespType() {
        return new DataConsultaEmbargosCONRespType();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosCONRespType.Row }
     * 
     */
    public DataConsultaEmbargosCONRespType.Row createDataConsultaEmbargosCONRespTypeRow() {
        return new DataConsultaEmbargosCONRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaEmbargosCON }
     * 
     */
    public ReqConsultaEmbargosCON createReqConsultaEmbargosCON() {
        return new ReqConsultaEmbargosCON();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosCONReqType }
     * 
     */
    public DataConsultaEmbargosCONReqType createDataConsultaEmbargosCONReqType() {
        return new DataConsultaEmbargosCONReqType();
    }

    /**
     * Create an instance of {@link RespConsultaEmbargosCON }
     * 
     */
    public RespConsultaEmbargosCON createRespConsultaEmbargosCON() {
        return new RespConsultaEmbargosCON();
    }

    /**
     * Create an instance of {@link DataConsultaEmbargosCONRespType.Row.Oficio }
     * 
     */
    public DataConsultaEmbargosCONRespType.Row.Oficio createDataConsultaEmbargosCONRespTypeRowOficio() {
        return new DataConsultaEmbargosCONRespType.Row.Oficio();
    }

}
