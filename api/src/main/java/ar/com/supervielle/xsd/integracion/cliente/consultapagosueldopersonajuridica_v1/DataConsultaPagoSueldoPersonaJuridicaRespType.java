
package ar.com.supervielle.xsd.integracion.cliente.consultapagosueldopersonajuridica_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaPagoSueldoPersonaJuridicaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaPagoSueldoPersonaJuridicaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="sectores">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="sector" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="idSector" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="convenvenios">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="convenio" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                                           &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="descEstConvenio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cuentas">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="cuenta" maxOccurs="unbounded">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;sequence>
 *                                                                               &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                                                                               &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                             &lt;/sequence>
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaPagoSueldoPersonaJuridicaRespType", propOrder = {
    "row"
})
public class DataConsultaPagoSueldoPersonaJuridicaRespType {

    @XmlElement(name = "Row")
    protected DataConsultaPagoSueldoPersonaJuridicaRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row }
     *     
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row }
     *     
     */
    public void setRow(DataConsultaPagoSueldoPersonaJuridicaRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="sectores">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="sector" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="idSector" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="convenvenios">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="convenio" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                                                 &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="descEstConvenio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cuentas">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="cuenta" maxOccurs="unbounded">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;sequence>
     *                                                                     &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *                                                                     &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                   &lt;/sequence>
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "razonSocial",
        "sectores"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected String razonSocial;
        @XmlElement(required = true)
        protected DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores sectores;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad razonSocial.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocial() {
            return razonSocial;
        }

        /**
         * Define el valor de la propiedad razonSocial.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocial(String value) {
            this.razonSocial = value;
        }

        /**
         * Obtiene el valor de la propiedad sectores.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores }
         *     
         */
        public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores getSectores() {
            return sectores;
        }

        /**
         * Define el valor de la propiedad sectores.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores }
         *     
         */
        public void setSectores(DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores value) {
            this.sectores = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="sector" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="idSector" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="convenvenios">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="convenio" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                                       &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="descEstConvenio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cuentas">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="cuenta" maxOccurs="unbounded">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;sequence>
         *                                                           &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
         *                                                           &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                         &lt;/sequence>
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "sector"
        })
        public static class Sectores {

            @XmlElement(required = true)
            protected List<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector> sector;

            /**
             * Gets the value of the sector property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the sector property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSector().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector }
             * 
             * 
             */
            public List<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector> getSector() {
                if (sector == null) {
                    sector = new ArrayList<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector>();
                }
                return this.sector;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idSector" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="convenvenios">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="convenio" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *                             &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="descEstConvenio" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cuentas">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="cuenta" maxOccurs="unbounded">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;sequence>
             *                                                 &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
             *                                                 &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                               &lt;/sequence>
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idSector",
                "convenvenios"
            })
            public static class Sector {

                @XmlElement(required = true)
                protected BigDecimal idSector;
                @XmlElement(required = true)
                protected DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios convenvenios;

                /**
                 * Obtiene el valor de la propiedad idSector.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getIdSector() {
                    return idSector;
                }

                /**
                 * Define el valor de la propiedad idSector.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setIdSector(BigDecimal value) {
                    this.idSector = value;
                }

                /**
                 * Obtiene el valor de la propiedad convenvenios.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios }
                 *     
                 */
                public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios getConvenvenios() {
                    return convenvenios;
                }

                /**
                 * Define el valor de la propiedad convenvenios.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios }
                 *     
                 */
                public void setConvenvenios(DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios value) {
                    this.convenvenios = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="convenio" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                 *                   &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="descEstConvenio" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cuentas">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="cuenta" maxOccurs="unbounded">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
                 *                                       &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "convenio"
                })
                public static class Convenvenios {

                    @XmlElement(required = true)
                    protected List<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio> convenio;

                    /**
                     * Gets the value of the convenio property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the convenio property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getConvenio().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio }
                     * 
                     * 
                     */
                    public List<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio> getConvenio() {
                        if (convenio == null) {
                            convenio = new ArrayList<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio>();
                        }
                        return this.convenio;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
                     *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="descEstConvenio" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cuentas">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="cuenta" maxOccurs="unbounded">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
                     *                             &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "numero",
                        "estado",
                        "descEstConvenio",
                        "cuentas"
                    })
                    public static class Convenio {

                        @XmlElement(required = true)
                        protected BigDecimal numero;
                        @XmlElement(required = true)
                        protected String estado;
                        @XmlElement(required = true)
                        protected String descEstConvenio;
                        @XmlElement(required = true)
                        protected DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas cuentas;

                        /**
                         * Obtiene el valor de la propiedad numero.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getNumero() {
                            return numero;
                        }

                        /**
                         * Define el valor de la propiedad numero.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setNumero(BigDecimal value) {
                            this.numero = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad estado.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getEstado() {
                            return estado;
                        }

                        /**
                         * Define el valor de la propiedad estado.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEstado(String value) {
                            this.estado = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad descEstConvenio.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getDescEstConvenio() {
                            return descEstConvenio;
                        }

                        /**
                         * Define el valor de la propiedad descEstConvenio.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setDescEstConvenio(String value) {
                            this.descEstConvenio = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad cuentas.
                         * 
                         * @return
                         *     possible object is
                         *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas }
                         *     
                         */
                        public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas getCuentas() {
                            return cuentas;
                        }

                        /**
                         * Define el valor de la propiedad cuentas.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas }
                         *     
                         */
                        public void setCuentas(DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas value) {
                            this.cuentas = value;
                        }


                        /**
                         * <p>Clase Java para anonymous complex type.
                         * 
                         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="cuenta" maxOccurs="unbounded">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
                         *                   &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "cuenta"
                        })
                        public static class Cuentas {

                            @XmlElement(required = true)
                            protected List<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta> cuenta;

                            /**
                             * Gets the value of the cuenta property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the cuenta property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getCuenta().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta }
                             * 
                             * 
                             */
                            public List<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta> getCuenta() {
                                if (cuenta == null) {
                                    cuenta = new ArrayList<DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta>();
                                }
                                return this.cuenta;
                            }


                            /**
                             * <p>Clase Java para anonymous complex type.
                             * 
                             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="indentificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
                             *         &lt;element name="tipoCobro" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "indentificador",
                                "tipoCobro"
                            })
                            public static class Cuenta {

                                @XmlElement(required = true)
                                protected IdCuentaBANTOTALType indentificador;
                                @XmlElement(required = true)
                                protected Object tipoCobro;

                                /**
                                 * Obtiene el valor de la propiedad indentificador.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link IdCuentaBANTOTALType }
                                 *     
                                 */
                                public IdCuentaBANTOTALType getIndentificador() {
                                    return indentificador;
                                }

                                /**
                                 * Define el valor de la propiedad indentificador.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link IdCuentaBANTOTALType }
                                 *     
                                 */
                                public void setIndentificador(IdCuentaBANTOTALType value) {
                                    this.indentificador = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad tipoCobro.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getTipoCobro() {
                                    return tipoCobro;
                                }

                                /**
                                 * Define el valor de la propiedad tipoCobro.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setTipoCobro(Object value) {
                                    this.tipoCobro = value;
                                }

                            }

                        }

                    }

                }

            }

        }

    }

}
