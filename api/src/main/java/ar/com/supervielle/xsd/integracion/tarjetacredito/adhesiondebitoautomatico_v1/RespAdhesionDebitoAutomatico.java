
package ar.com.supervielle.xsd.integracion.tarjetacredito.adhesiondebitoautomatico_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/adhesionDebitoAutomatico-v1}DataAdhesionDebitoAutomaticoRespType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "RespAdhesionDebitoAutomatico")
public class RespAdhesionDebitoAutomatico {

    @XmlElement(name = "Data", required = true)
    protected DataAdhesionDebitoAutomaticoRespType data;

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataAdhesionDebitoAutomaticoRespType }
     *     
     */
    public DataAdhesionDebitoAutomaticoRespType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAdhesionDebitoAutomaticoRespType }
     *     
     */
    public void setData(DataAdhesionDebitoAutomaticoRespType value) {
        this.data = value;
    }

}
