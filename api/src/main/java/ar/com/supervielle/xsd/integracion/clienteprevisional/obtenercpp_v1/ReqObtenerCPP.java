
package ar.com.supervielle.xsd.integracion.clienteprevisional.obtenercpp_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/clientePrevisional/obtenerCPP-v1}DataObtenerCPPReqType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "ReqObtenerCPP")
public class ReqObtenerCPP {

    @XmlElement(name = "Data", required = true)
    protected DataObtenerCPPReqType data;

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataObtenerCPPReqType }
     *     
     */
    public DataObtenerCPPReqType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObtenerCPPReqType }
     *     
     */
    public void setData(DataObtenerCPPReqType value) {
        this.data = value;
    }

}
