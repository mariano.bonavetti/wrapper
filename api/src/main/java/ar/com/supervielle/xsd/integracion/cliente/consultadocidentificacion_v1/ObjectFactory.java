
package ar.com.supervielle.xsd.integracion.cliente.consultadocidentificacion_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultadocidentificacion_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultadocidentificacion_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDocIdentRespType }
     * 
     */
    public DataConsultaDocIdentRespType createDataConsultaDocIdentRespType() {
        return new DataConsultaDocIdentRespType();
    }

    /**
     * Create an instance of {@link DataConsultaDocIdentRespType.Row }
     * 
     */
    public DataConsultaDocIdentRespType.Row createDataConsultaDocIdentRespTypeRow() {
        return new DataConsultaDocIdentRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaDocIdent }
     * 
     */
    public ReqConsultaDocIdent createReqConsultaDocIdent() {
        return new ReqConsultaDocIdent();
    }

    /**
     * Create an instance of {@link DataConsultaDocIdentReqType }
     * 
     */
    public DataConsultaDocIdentReqType createDataConsultaDocIdentReqType() {
        return new DataConsultaDocIdentReqType();
    }

    /**
     * Create an instance of {@link RespConsultaDocIdent }
     * 
     */
    public RespConsultaDocIdent createRespConsultaDocIdent() {
        return new RespConsultaDocIdent();
    }

    /**
     * Create an instance of {@link DataConsultaDocIdentRespType.Row.Documento }
     * 
     */
    public DataConsultaDocIdentRespType.Row.Documento createDataConsultaDocIdentRespTypeRowDocumento() {
        return new DataConsultaDocIdentRespType.Row.Documento();
    }

}
