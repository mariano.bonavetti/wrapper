
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultapersonasvinculadas_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultapersonasvinculadas_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultapersonasvinculadas_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasRespType }
     * 
     */
    public DataConsultaPersonasVinculadasRespType createDataConsultaPersonasVinculadasRespType() {
        return new DataConsultaPersonasVinculadasRespType();
    }

    /**
     * Create an instance of {@link RespConsultaPersonasVinculadas }
     * 
     */
    public RespConsultaPersonasVinculadas createRespConsultaPersonasVinculadas() {
        return new RespConsultaPersonasVinculadas();
    }

    /**
     * Create an instance of {@link ReqConsultaPersonasVinculadas }
     * 
     */
    public ReqConsultaPersonasVinculadas createReqConsultaPersonasVinculadas() {
        return new ReqConsultaPersonasVinculadas();
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasReqType }
     * 
     */
    public DataConsultaPersonasVinculadasReqType createDataConsultaPersonasVinculadasReqType() {
        return new DataConsultaPersonasVinculadasReqType();
    }

    /**
     * Create an instance of {@link DataConsultaPersonasVinculadasRespType.Row }
     * 
     */
    public DataConsultaPersonasVinculadasRespType.Row createDataConsultaPersonasVinculadasRespTypeRow() {
        return new DataConsultaPersonasVinculadasRespType.Row();
    }

}
