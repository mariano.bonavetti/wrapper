
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultaproductos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaProductosReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaProductosReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idPrograma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="puntos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="puntosDesde" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="puntosHasta" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="idCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idSubCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="producto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaProductosReqType", propOrder = {
    "idPrograma",
    "puntos",
    "idCategoria",
    "idSubCategoria",
    "producto"
})
public class DataConsultaProductosReqType {

    @XmlElement(required = true)
    protected String idPrograma;
    protected DataConsultaProductosReqType.Puntos puntos;
    protected String idCategoria;
    protected String idSubCategoria;
    protected CodDescStringType producto;

    /**
     * Obtiene el valor de la propiedad idPrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPrograma() {
        return idPrograma;
    }

    /**
     * Define el valor de la propiedad idPrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPrograma(String value) {
        this.idPrograma = value;
    }

    /**
     * Obtiene el valor de la propiedad puntos.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaProductosReqType.Puntos }
     *     
     */
    public DataConsultaProductosReqType.Puntos getPuntos() {
        return puntos;
    }

    /**
     * Define el valor de la propiedad puntos.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaProductosReqType.Puntos }
     *     
     */
    public void setPuntos(DataConsultaProductosReqType.Puntos value) {
        this.puntos = value;
    }

    /**
     * Obtiene el valor de la propiedad idCategoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCategoria() {
        return idCategoria;
    }

    /**
     * Define el valor de la propiedad idCategoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCategoria(String value) {
        this.idCategoria = value;
    }

    /**
     * Obtiene el valor de la propiedad idSubCategoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSubCategoria() {
        return idSubCategoria;
    }

    /**
     * Define el valor de la propiedad idSubCategoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSubCategoria(String value) {
        this.idSubCategoria = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setProducto(CodDescStringType value) {
        this.producto = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="puntosDesde" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="puntosHasta" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "puntosDesde",
        "puntosHasta"
    })
    public static class Puntos {

        @XmlElement(required = true)
        protected Object puntosDesde;
        @XmlElement(required = true)
        protected Object puntosHasta;

        /**
         * Obtiene el valor de la propiedad puntosDesde.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getPuntosDesde() {
            return puntosDesde;
        }

        /**
         * Define el valor de la propiedad puntosDesde.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setPuntosDesde(Object value) {
            this.puntosDesde = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosHasta.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getPuntosHasta() {
            return puntosHasta;
        }

        /**
         * Define el valor de la propiedad puntosHasta.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setPuntosHasta(Object value) {
            this.puntosHasta = value;
        }

    }

}
