
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesrecibidos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesrecibidos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultachequesrecibidos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosRespType2 }
     * 
     */
    public DataConsultaChequesRecibidosRespType2 createDataConsultaChequesRecibidosRespType2() {
        return new DataConsultaChequesRecibidosRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosRespType }
     * 
     */
    public DataConsultaChequesRecibidosRespType createDataConsultaChequesRecibidosRespType() {
        return new DataConsultaChequesRecibidosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosRespType.Row }
     * 
     */
    public DataConsultaChequesRecibidosRespType.Row createDataConsultaChequesRecibidosRespTypeRow() {
        return new DataConsultaChequesRecibidosRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosReqType }
     * 
     */
    public DataConsultaChequesRecibidosReqType createDataConsultaChequesRecibidosReqType() {
        return new DataConsultaChequesRecibidosReqType();
    }

    /**
     * Create an instance of {@link ReqConsultaChequesRecibidos }
     * 
     */
    public ReqConsultaChequesRecibidos createReqConsultaChequesRecibidos() {
        return new ReqConsultaChequesRecibidos();
    }

    /**
     * Create an instance of {@link RespConsultaChequesRecibidos }
     * 
     */
    public RespConsultaChequesRecibidos createRespConsultaChequesRecibidos() {
        return new RespConsultaChequesRecibidos();
    }

    /**
     * Create an instance of {@link ReqConsultaChequesRecibidos2 }
     * 
     */
    public ReqConsultaChequesRecibidos2 createReqConsultaChequesRecibidos2() {
        return new ReqConsultaChequesRecibidos2();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosReqType2 }
     * 
     */
    public DataConsultaChequesRecibidosReqType2 createDataConsultaChequesRecibidosReqType2() {
        return new DataConsultaChequesRecibidosReqType2();
    }

    /**
     * Create an instance of {@link RespConsultaChequesRecibidos2 }
     * 
     */
    public RespConsultaChequesRecibidos2 createRespConsultaChequesRecibidos2() {
        return new RespConsultaChequesRecibidos2();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosRespType2 .Row }
     * 
     */
    public DataConsultaChequesRecibidosRespType2 .Row createDataConsultaChequesRecibidosRespType2Row() {
        return new DataConsultaChequesRecibidosRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosRespType.Row.EntidadEmisora }
     * 
     */
    public DataConsultaChequesRecibidosRespType.Row.EntidadEmisora createDataConsultaChequesRecibidosRespTypeRowEntidadEmisora() {
        return new DataConsultaChequesRecibidosRespType.Row.EntidadEmisora();
    }

    /**
     * Create an instance of {@link DataConsultaChequesRecibidosReqType.RangoFecha }
     * 
     */
    public DataConsultaChequesRecibidosReqType.RangoFecha createDataConsultaChequesRecibidosReqTypeRangoFecha() {
        return new DataConsultaChequesRecibidosReqType.RangoFecha();
    }

}
