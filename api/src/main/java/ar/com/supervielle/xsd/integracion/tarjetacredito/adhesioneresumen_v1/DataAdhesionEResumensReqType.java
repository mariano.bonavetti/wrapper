
package ar.com.supervielle.xsd.integracion.tarjetacredito.adhesioneresumen_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataAdhesionEResumensReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAdhesionEResumensReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *         &lt;element name="IdentificadorBAN" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *         &lt;element name="correoElectronico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adhesionEresumen" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAdhesionEResumensReqType", propOrder = {
    "identificador",
    "identificadorBAN",
    "correoElectronico",
    "adhesionEresumen"
})
public class DataAdhesionEResumensReqType {

    @XmlElement(name = "Identificador", required = true)
    protected IdClienteType identificador;
    @XmlElement(name = "IdentificadorBAN", required = true)
    protected IdCuentaBANTOTALType identificadorBAN;
    @XmlElement(required = true)
    protected String correoElectronico;
    protected boolean adhesionEresumen;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad identificadorBAN.
     * 
     * @return
     *     possible object is
     *     {@link IdCuentaBANTOTALType }
     *     
     */
    public IdCuentaBANTOTALType getIdentificadorBAN() {
        return identificadorBAN;
    }

    /**
     * Define el valor de la propiedad identificadorBAN.
     * 
     * @param value
     *     allowed object is
     *     {@link IdCuentaBANTOTALType }
     *     
     */
    public void setIdentificadorBAN(IdCuentaBANTOTALType value) {
        this.identificadorBAN = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Define el valor de la propiedad correoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

    /**
     * Obtiene el valor de la propiedad adhesionEresumen.
     * 
     */
    public boolean isAdhesionEresumen() {
        return adhesionEresumen;
    }

    /**
     * Define el valor de la propiedad adhesionEresumen.
     * 
     */
    public void setAdhesionEresumen(boolean value) {
        this.adhesionEresumen = value;
    }

}
