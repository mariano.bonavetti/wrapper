
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaListadoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.2}byClienteId"/>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.2}byCuentaId"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoReqType", propOrder = {
    "byClienteId",
    "byCuentaId"
})
public class DataConsultaListadoReqType {

    @XmlElement(namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.2")
    protected ByClienteId byClienteId;
    @XmlElement(namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaListado-v1.2")
    protected ByCuentaId byCuentaId;

    /**
     * Obtiene el valor de la propiedad byClienteId.
     * 
     * @return
     *     possible object is
     *     {@link ByClienteId }
     *     
     */
    public ByClienteId getByClienteId() {
        return byClienteId;
    }

    /**
     * Define el valor de la propiedad byClienteId.
     * 
     * @param value
     *     allowed object is
     *     {@link ByClienteId }
     *     
     */
    public void setByClienteId(ByClienteId value) {
        this.byClienteId = value;
    }

    /**
     * Obtiene el valor de la propiedad byCuentaId.
     * 
     * @return
     *     possible object is
     *     {@link ByCuentaId }
     *     
     */
    public ByCuentaId getByCuentaId() {
        return byCuentaId;
    }

    /**
     * Define el valor de la propiedad byCuentaId.
     * 
     * @param value
     *     allowed object is
     *     {@link ByCuentaId }
     *     
     */
    public void setByCuentaId(ByCuentaId value) {
        this.byCuentaId = value;
    }

}
