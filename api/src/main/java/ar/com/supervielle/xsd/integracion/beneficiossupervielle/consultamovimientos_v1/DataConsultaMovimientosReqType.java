
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultamovimientos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaMovimientosReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaMovimientosReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="idPrograma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaMovimientosReqType", propOrder = {
    "identificador",
    "idPrograma"
})
@XmlSeeAlso({
    ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultamovimientos_v1.ReqConsultaMovimientos.Data.class
})
public class DataConsultaMovimientosReqType {

    @XmlElement(required = true)
    protected DataConsultaMovimientosReqType.Identificador identificador;
    @XmlElement(required = true)
    protected String idPrograma;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaMovimientosReqType.Identificador }
     *     
     */
    public DataConsultaMovimientosReqType.Identificador getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaMovimientosReqType.Identificador }
     *     
     */
    public void setIdentificador(DataConsultaMovimientosReqType.Identificador value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad idPrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPrograma() {
        return idPrograma;
    }

    /**
     * Define el valor de la propiedad idPrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPrograma(String value) {
        this.idPrograma = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Identificador
        extends IdClienteType
    {


    }

}
