
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdosban_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdosban_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdosban_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosBANRespType }
     * 
     */
    public DataConsultaAcuerdosBANRespType createDataConsultaAcuerdosBANRespType() {
        return new DataConsultaAcuerdosBANRespType();
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosBANRespType.Row }
     * 
     */
    public DataConsultaAcuerdosBANRespType.Row createDataConsultaAcuerdosBANRespTypeRow() {
        return new DataConsultaAcuerdosBANRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaAcuerdosBAN }
     * 
     */
    public ReqConsultaAcuerdosBAN createReqConsultaAcuerdosBAN() {
        return new ReqConsultaAcuerdosBAN();
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosBANReqType }
     * 
     */
    public DataConsultaAcuerdosBANReqType createDataConsultaAcuerdosBANReqType() {
        return new DataConsultaAcuerdosBANReqType();
    }

    /**
     * Create an instance of {@link RespConsultaAcuerdosBAN }
     * 
     */
    public RespConsultaAcuerdosBAN createRespConsultaAcuerdosBAN() {
        return new RespConsultaAcuerdosBAN();
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosBANRespType.Row.Renovacion }
     * 
     */
    public DataConsultaAcuerdosBANRespType.Row.Renovacion createDataConsultaAcuerdosBANRespTypeRowRenovacion() {
        return new DataConsultaAcuerdosBANRespType.Row.Renovacion();
    }

}
