
package ar.com.supervielle.xsd.integracion.cliente.consultacapacidadreducida_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultacapacidadreducida_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultacapacidadreducida_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCapacidadReducidaRespType }
     * 
     */
    public DataConsultaCapacidadReducidaRespType createDataConsultaCapacidadReducidaRespType() {
        return new DataConsultaCapacidadReducidaRespType();
    }

    /**
     * Create an instance of {@link RespConsultaCapacidadReducida }
     * 
     */
    public RespConsultaCapacidadReducida createRespConsultaCapacidadReducida() {
        return new RespConsultaCapacidadReducida();
    }

    /**
     * Create an instance of {@link ReqConsultaCapacidadReducida }
     * 
     */
    public ReqConsultaCapacidadReducida createReqConsultaCapacidadReducida() {
        return new ReqConsultaCapacidadReducida();
    }

    /**
     * Create an instance of {@link DataConsultaCapacidadReducidaReqType }
     * 
     */
    public DataConsultaCapacidadReducidaReqType createDataConsultaCapacidadReducidaReqType() {
        return new DataConsultaCapacidadReducidaReqType();
    }

    /**
     * Create an instance of {@link DataConsultaCapacidadReducidaRespType.Row }
     * 
     */
    public DataConsultaCapacidadReducidaRespType.Row createDataConsultaCapacidadReducidaRespTypeRow() {
        return new DataConsultaCapacidadReducidaRespType.Row();
    }

}
