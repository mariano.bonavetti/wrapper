
package ar.com.supervielle.xsd.integracion.cliente.altamodificacioncorreo_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataAltaModificacionCorreoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaModificacionCorreoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *         &lt;element name="tipoOpe" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="correoElectronicoAnterior" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="correoElectronico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comentario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaModificacionCorreoReqType", propOrder = {
    "identificador",
    "tipoOpe",
    "correoElectronicoAnterior",
    "correoElectronico",
    "comentario",
    "usuario"
})
public class DataAltaModificacionCorreoReqType {

    @XmlElement(name = "Identificador", required = true)
    protected IdClienteType identificador;
    @XmlElement(required = true)
    protected String tipoOpe;
    @XmlElement(required = true)
    protected String correoElectronicoAnterior;
    @XmlElement(required = true)
    protected String correoElectronico;
    protected String comentario;
    @XmlElement(required = true)
    protected String usuario;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOpe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOpe() {
        return tipoOpe;
    }

    /**
     * Define el valor de la propiedad tipoOpe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOpe(String value) {
        this.tipoOpe = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronicoAnterior.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronicoAnterior() {
        return correoElectronicoAnterior;
    }

    /**
     * Define el valor de la propiedad correoElectronicoAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronicoAnterior(String value) {
        this.correoElectronicoAnterior = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Define el valor de la propiedad correoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

    /**
     * Obtiene el valor de la propiedad comentario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Define el valor de la propiedad comentario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

    /**
     * Obtiene el valor de la propiedad usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

}
