
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RubroDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RubroDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fiid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDeRubro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RubroDTO", propOrder = {
    "fiid",
    "rubro",
    "tipoDeRubro"
})
public class RubroDTO {

    @XmlElementRef(name = "fiid", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fiid;
    @XmlElementRef(name = "rubro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rubro;
    @XmlElementRef(name = "tipoDeRubro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipoDeRubro;

    /**
     * Obtiene el valor de la propiedad fiid.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFiid() {
        return fiid;
    }

    /**
     * Define el valor de la propiedad fiid.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFiid(JAXBElement<String> value) {
        this.fiid = value;
    }

    /**
     * Obtiene el valor de la propiedad rubro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRubro() {
        return rubro;
    }

    /**
     * Define el valor de la propiedad rubro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRubro(JAXBElement<String> value) {
        this.rubro = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDeRubro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoDeRubro() {
        return tipoDeRubro;
    }

    /**
     * Define el valor de la propiedad tipoDeRubro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoDeRubro(JAXBElement<String> value) {
        this.tipoDeRubro = value;
    }

}
