
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaatributosxtipoproducto_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaatributosxtipoproducto_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaatributosxtipoproducto_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoRespType2 }
     * 
     */
    public DataConsultaAtributosXTipoProductoRespType2 createDataConsultaAtributosXTipoProductoRespType2() {
        return new DataConsultaAtributosXTipoProductoRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoRespType2 .Row }
     * 
     */
    public DataConsultaAtributosXTipoProductoRespType2 .Row createDataConsultaAtributosXTipoProductoRespType2Row() {
        return new DataConsultaAtributosXTipoProductoRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros }
     * 
     */
    public DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros createDataConsultaAtributosXTipoProductoRespType2RowParametros() {
        return new DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoRespType }
     * 
     */
    public DataConsultaAtributosXTipoProductoRespType createDataConsultaAtributosXTipoProductoRespType() {
        return new DataConsultaAtributosXTipoProductoRespType();
    }

    /**
     * Create an instance of {@link RespConsultaAtributosXTipoProducto }
     * 
     */
    public RespConsultaAtributosXTipoProducto createRespConsultaAtributosXTipoProducto() {
        return new RespConsultaAtributosXTipoProducto();
    }

    /**
     * Create an instance of {@link ReqConsultaAtributosXTipoProducto }
     * 
     */
    public ReqConsultaAtributosXTipoProducto createReqConsultaAtributosXTipoProducto() {
        return new ReqConsultaAtributosXTipoProducto();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoReqType }
     * 
     */
    public DataConsultaAtributosXTipoProductoReqType createDataConsultaAtributosXTipoProductoReqType() {
        return new DataConsultaAtributosXTipoProductoReqType();
    }

    /**
     * Create an instance of {@link RespConsultaAtributosXTipoProducto2 }
     * 
     */
    public RespConsultaAtributosXTipoProducto2 createRespConsultaAtributosXTipoProducto2() {
        return new RespConsultaAtributosXTipoProducto2();
    }

    /**
     * Create an instance of {@link ReqConsultaAtributosXTipoProducto2 }
     * 
     */
    public ReqConsultaAtributosXTipoProducto2 createReqConsultaAtributosXTipoProducto2() {
        return new ReqConsultaAtributosXTipoProducto2();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoReqType2 }
     * 
     */
    public DataConsultaAtributosXTipoProductoReqType2 createDataConsultaAtributosXTipoProductoReqType2() {
        return new DataConsultaAtributosXTipoProductoReqType2();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro }
     * 
     */
    public DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro createDataConsultaAtributosXTipoProductoRespType2RowParametrosParametro() {
        return new DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro();
    }

    /**
     * Create an instance of {@link DataConsultaAtributosXTipoProductoRespType.Row }
     * 
     */
    public DataConsultaAtributosXTipoProductoRespType.Row createDataConsultaAtributosXTipoProductoRespTypeRow() {
        return new DataConsultaAtributosXTipoProductoRespType.Row();
    }

}
