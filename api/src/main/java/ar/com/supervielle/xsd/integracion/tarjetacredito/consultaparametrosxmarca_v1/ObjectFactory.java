
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosxmarca_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosxmarca_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosxmarca_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaReqType }
     * 
     */
    public DataConsultaParametrosxMarcaReqType createDataConsultaParametrosxMarcaReqType() {
        return new DataConsultaParametrosxMarcaReqType();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType }
     * 
     */
    public DataConsultaParametrosxMarcaRespType createDataConsultaParametrosxMarcaRespType() {
        return new DataConsultaParametrosxMarcaRespType();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row createDataConsultaParametrosxMarcaRespTypeRow() {
        return new DataConsultaParametrosxMarcaRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row.Detalles }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row.Detalles createDataConsultaParametrosxMarcaRespTypeRowDetalles() {
        return new DataConsultaParametrosxMarcaRespType.Row.Detalles();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle createDataConsultaParametrosxMarcaRespTypeRowDetallesDetalle() {
        return new DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle();
    }

    /**
     * Create an instance of {@link RespConsultaParametrosxMarca }
     * 
     */
    public RespConsultaParametrosxMarca createRespConsultaParametrosxMarca() {
        return new RespConsultaParametrosxMarca();
    }

    /**
     * Create an instance of {@link ReqConsultaParametrosxMarca }
     * 
     */
    public ReqConsultaParametrosxMarca createReqConsultaParametrosxMarca() {
        return new ReqConsultaParametrosxMarca();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaReqType.Principal }
     * 
     */
    public DataConsultaParametrosxMarcaReqType.Principal createDataConsultaParametrosxMarcaReqTypePrincipal() {
        return new DataConsultaParametrosxMarcaReqType.Principal();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row.Error }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row.Error createDataConsultaParametrosxMarcaRespTypeRowError() {
        return new DataConsultaParametrosxMarcaRespType.Row.Error();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam1 }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam1 createDataConsultaParametrosxMarcaRespTypeRowDetallesDetalleAuxParam1() {
        return new DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam1();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam2 }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam2 createDataConsultaParametrosxMarcaRespTypeRowDetallesDetalleAuxParam2() {
        return new DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam2();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam3 }
     * 
     */
    public DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam3 createDataConsultaParametrosxMarcaRespTypeRowDetallesDetalleAuxParam3() {
        return new DataConsultaParametrosxMarcaRespType.Row.Detalles.Detalle.AuxParam3();
    }

}
