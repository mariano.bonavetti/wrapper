
package ar.com.supervielle.xsd.integracion.tarjetacredito.altatarjeta_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altatarjeta_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altatarjeta_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType }
     * 
     */
    public DataAltaTarjetaReqType createDataAltaTarjetaReqType() {
        return new DataAltaTarjetaReqType();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType.Empresa }
     * 
     */
    public DataAltaTarjetaReqType.Empresa createDataAltaTarjetaReqTypeEmpresa() {
        return new DataAltaTarjetaReqType.Empresa();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType.Empresa.DatosCliente }
     * 
     */
    public DataAltaTarjetaReqType.Empresa.DatosCliente createDataAltaTarjetaReqTypeEmpresaDatosCliente() {
        return new DataAltaTarjetaReqType.Empresa.DatosCliente();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo }
     * 
     */
    public DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo createDataAltaTarjetaReqTypeEmpresaDatosClienteDatosClienteNuevo() {
        return new DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaRespType }
     * 
     */
    public DataAltaTarjetaRespType createDataAltaTarjetaRespType() {
        return new DataAltaTarjetaRespType();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaRespType.Row }
     * 
     */
    public DataAltaTarjetaRespType.Row createDataAltaTarjetaRespTypeRow() {
        return new DataAltaTarjetaRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaRespType.Row.SolicitudesGeneradas }
     * 
     */
    public DataAltaTarjetaRespType.Row.SolicitudesGeneradas createDataAltaTarjetaRespTypeRowSolicitudesGeneradas() {
        return new DataAltaTarjetaRespType.Row.SolicitudesGeneradas();
    }

    /**
     * Create an instance of {@link RespAltaTarjeta }
     * 
     */
    public RespAltaTarjeta createRespAltaTarjeta() {
        return new RespAltaTarjeta();
    }

    /**
     * Create an instance of {@link ReqAltaTarjeta }
     * 
     */
    public ReqAltaTarjeta createReqAltaTarjeta() {
        return new ReqAltaTarjeta();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud }
     * 
     */
    public DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud createDataAltaTarjetaReqTypeEmpresaIdentificadorSolicitud() {
        return new DataAltaTarjetaReqType.Empresa.IdentificadorSolicitud();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType.Empresa.Limites }
     * 
     */
    public DataAltaTarjetaReqType.Empresa.Limites createDataAltaTarjetaReqTypeEmpresaLimites() {
        return new DataAltaTarjetaReqType.Empresa.Limites();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio }
     * 
     */
    public DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio createDataAltaTarjetaReqTypeEmpresaDatosClienteDatosClienteNuevoDomicilio() {
        return new DataAltaTarjetaReqType.Empresa.DatosCliente.DatosClienteNuevo.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaTarjetaRespType.Row.SolicitudesGeneradas.Solicitud }
     * 
     */
    public DataAltaTarjetaRespType.Row.SolicitudesGeneradas.Solicitud createDataAltaTarjetaRespTypeRowSolicitudesGeneradasSolicitud() {
        return new DataAltaTarjetaRespType.Row.SolicitudesGeneradas.Solicitud();
    }

}
