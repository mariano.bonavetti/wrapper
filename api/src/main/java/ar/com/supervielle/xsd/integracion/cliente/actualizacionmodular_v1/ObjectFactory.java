
package ar.com.supervielle.xsd.integracion.cliente.actualizacionmodular_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.actualizacionmodular_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.actualizacionmodular_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataActualizacionModularRespType }
     * 
     */
    public DataActualizacionModularRespType createDataActualizacionModularRespType() {
        return new DataActualizacionModularRespType();
    }

    /**
     * Create an instance of {@link DataActualizacionModularReqType }
     * 
     */
    public DataActualizacionModularReqType createDataActualizacionModularReqType() {
        return new DataActualizacionModularReqType();
    }

    /**
     * Create an instance of {@link DataActualizacionModularReqType.Personas }
     * 
     */
    public DataActualizacionModularReqType.Personas createDataActualizacionModularReqTypePersonas() {
        return new DataActualizacionModularReqType.Personas();
    }

    /**
     * Create an instance of {@link DataActualizacionModularReqType.Personas.Persona }
     * 
     */
    public DataActualizacionModularReqType.Personas.Persona createDataActualizacionModularReqTypePersonasPersona() {
        return new DataActualizacionModularReqType.Personas.Persona();
    }

    /**
     * Create an instance of {@link DataActualizacionModularReqType.Personas.Persona.Domicilios }
     * 
     */
    public DataActualizacionModularReqType.Personas.Persona.Domicilios createDataActualizacionModularReqTypePersonasPersonaDomicilios() {
        return new DataActualizacionModularReqType.Personas.Persona.Domicilios();
    }

    /**
     * Create an instance of {@link ReqActualizacionModular }
     * 
     */
    public ReqActualizacionModular createReqActualizacionModular() {
        return new ReqActualizacionModular();
    }

    /**
     * Create an instance of {@link RespActualizacionModular }
     * 
     */
    public RespActualizacionModular createRespActualizacionModular() {
        return new RespActualizacionModular();
    }

    /**
     * Create an instance of {@link DataActualizacionModularRespType.Row }
     * 
     */
    public DataActualizacionModularRespType.Row createDataActualizacionModularRespTypeRow() {
        return new DataActualizacionModularRespType.Row();
    }

    /**
     * Create an instance of {@link DataActualizacionModularReqType.Personas.Persona.InfoPersona }
     * 
     */
    public DataActualizacionModularReqType.Personas.Persona.InfoPersona createDataActualizacionModularReqTypePersonasPersonaInfoPersona() {
        return new DataActualizacionModularReqType.Personas.Persona.InfoPersona();
    }

    /**
     * Create an instance of {@link DataActualizacionModularReqType.Personas.Persona.Domicilios.Domicilio }
     * 
     */
    public DataActualizacionModularReqType.Personas.Persona.Domicilios.Domicilio createDataActualizacionModularReqTypePersonasPersonaDomiciliosDomicilio() {
        return new DataActualizacionModularReqType.Personas.Persona.Domicilios.Domicilio();
    }

}
