
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaMovReqType2 }
     * 
     */
    public DataConsultaMovReqType2 createDataConsultaMovReqType2() {
        return new DataConsultaMovReqType2();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType2 }
     * 
     */
    public DataConsultaMovRespType2 createDataConsultaMovRespType2() {
        return new DataConsultaMovRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaMovReqType }
     * 
     */
    public DataConsultaMovReqType createDataConsultaMovReqType() {
        return new DataConsultaMovReqType();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType }
     * 
     */
    public DataConsultaMovRespType createDataConsultaMovRespType() {
        return new DataConsultaMovRespType();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row }
     * 
     */
    public DataConsultaMovRespType.Row createDataConsultaMovRespTypeRow() {
        return new DataConsultaMovRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row.AtributosExtendidos }
     * 
     */
    public DataConsultaMovRespType.Row.AtributosExtendidos createDataConsultaMovRespTypeRowAtributosExtendidos() {
        return new DataConsultaMovRespType.Row.AtributosExtendidos();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row.Movimientos }
     * 
     */
    public DataConsultaMovRespType.Row.Movimientos createDataConsultaMovRespTypeRowMovimientos() {
        return new DataConsultaMovRespType.Row.Movimientos();
    }

    /**
     * Create an instance of {@link RespConsultaMovimientos }
     * 
     */
    public RespConsultaMovimientos createRespConsultaMovimientos() {
        return new RespConsultaMovimientos();
    }

    /**
     * Create an instance of {@link ReqConsultaMovimientos }
     * 
     */
    public ReqConsultaMovimientos createReqConsultaMovimientos() {
        return new ReqConsultaMovimientos();
    }

    /**
     * Create an instance of {@link RespConsultaMovimientos2 }
     * 
     */
    public RespConsultaMovimientos2 createRespConsultaMovimientos2() {
        return new RespConsultaMovimientos2();
    }

    /**
     * Create an instance of {@link ReqConsultaMovimientos2 }
     * 
     */
    public ReqConsultaMovimientos2 createReqConsultaMovimientos2() {
        return new ReqConsultaMovimientos2();
    }

    /**
     * Create an instance of {@link DataConsultaMovReqType2 .RangoFecha }
     * 
     */
    public DataConsultaMovReqType2 .RangoFecha createDataConsultaMovReqType2RangoFecha() {
        return new DataConsultaMovReqType2 .RangoFecha();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType2 .Row }
     * 
     */
    public DataConsultaMovRespType2 .Row createDataConsultaMovRespType2Row() {
        return new DataConsultaMovRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaMovReqType.RangoFecha }
     * 
     */
    public DataConsultaMovReqType.RangoFecha createDataConsultaMovReqTypeRangoFecha() {
        return new DataConsultaMovReqType.RangoFecha();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row.Saldo }
     * 
     */
    public DataConsultaMovRespType.Row.Saldo createDataConsultaMovRespTypeRowSaldo() {
        return new DataConsultaMovRespType.Row.Saldo();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row.Subtotal }
     * 
     */
    public DataConsultaMovRespType.Row.Subtotal createDataConsultaMovRespTypeRowSubtotal() {
        return new DataConsultaMovRespType.Row.Subtotal();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row.AtributosExtendidos.Atributo }
     * 
     */
    public DataConsultaMovRespType.Row.AtributosExtendidos.Atributo createDataConsultaMovRespTypeRowAtributosExtendidosAtributo() {
        return new DataConsultaMovRespType.Row.AtributosExtendidos.Atributo();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row.Movimientos.Movimiento }
     * 
     */
    public DataConsultaMovRespType.Row.Movimientos.Movimiento createDataConsultaMovRespTypeRowMovimientosMovimiento() {
        return new DataConsultaMovRespType.Row.Movimientos.Movimiento();
    }

}
