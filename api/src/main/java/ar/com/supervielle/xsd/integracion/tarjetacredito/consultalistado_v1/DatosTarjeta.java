
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para datosTarjeta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="datosTarjeta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="marcaPago">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="marcaPagoformadescri" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datosTarjeta", propOrder = {
    "formaPago",
    "marcaPago"
})
public class DatosTarjeta {

    @XmlElement(required = true)
    protected String formaPago;
    @XmlElement(required = true)
    protected DatosTarjeta.MarcaPago marcaPago;

    /**
     * Obtiene el valor de la propiedad formaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Define el valor de la propiedad formaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPago(String value) {
        this.formaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad marcaPago.
     * 
     * @return
     *     possible object is
     *     {@link DatosTarjeta.MarcaPago }
     *     
     */
    public DatosTarjeta.MarcaPago getMarcaPago() {
        return marcaPago;
    }

    /**
     * Define el valor de la propiedad marcaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link DatosTarjeta.MarcaPago }
     *     
     */
    public void setMarcaPago(DatosTarjeta.MarcaPago value) {
        this.marcaPago = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="marcaPagoformadescri" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "marcaPagoformadescri"
    })
    public static class MarcaPago {

        @XmlElement(required = true)
        protected String marcaPagoformadescri;

        /**
         * Obtiene el valor de la propiedad marcaPagoformadescri.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMarcaPagoformadescri() {
            return marcaPagoformadescri;
        }

        /**
         * Define el valor de la propiedad marcaPagoformadescri.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMarcaPagoformadescri(String value) {
            this.marcaPagoformadescri = value;
        }

    }

}
