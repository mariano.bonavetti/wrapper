
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CuentaPMC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CuentaPMC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cbu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disponible" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="intereses" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="limite" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}MonedaPMC" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}TipoCuentaPMC" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CuentaPMC", propOrder = {
    "cbu",
    "disponible",
    "intereses",
    "limite",
    "moneda",
    "numero",
    "saldo",
    "tipo"
})
public class CuentaPMC {

    protected String cbu;
    protected Double disponible;
    protected Double intereses;
    protected Double limite;
    protected MonedaPMC moneda;
    protected String numero;
    protected Double saldo;
    protected TipoCuentaPMC tipo;

    /**
     * Obtiene el valor de la propiedad cbu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbu() {
        return cbu;
    }

    /**
     * Define el valor de la propiedad cbu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbu(String value) {
        this.cbu = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDisponible() {
        return disponible;
    }

    /**
     * Define el valor de la propiedad disponible.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDisponible(Double value) {
        this.disponible = value;
    }

    /**
     * Obtiene el valor de la propiedad intereses.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getIntereses() {
        return intereses;
    }

    /**
     * Define el valor de la propiedad intereses.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setIntereses(Double value) {
        this.intereses = value;
    }

    /**
     * Obtiene el valor de la propiedad limite.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLimite() {
        return limite;
    }

    /**
     * Define el valor de la propiedad limite.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLimite(Double value) {
        this.limite = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link MonedaPMC }
     *     
     */
    public MonedaPMC getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link MonedaPMC }
     *     
     */
    public void setMoneda(MonedaPMC value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad numero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Define el valor de la propiedad numero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Obtiene el valor de la propiedad saldo.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSaldo() {
        return saldo;
    }

    /**
     * Define el valor de la propiedad saldo.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSaldo(Double value) {
        this.saldo = value;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     * 
     * @return
     *     possible object is
     *     {@link TipoCuentaPMC }
     *     
     */
    public TipoCuentaPMC getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCuentaPMC }
     *     
     */
    public void setTipo(TipoCuentaPMC value) {
        this.tipo = value;
    }

}
