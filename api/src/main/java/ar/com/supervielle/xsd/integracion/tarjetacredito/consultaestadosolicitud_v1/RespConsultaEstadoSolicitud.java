
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadosolicitud_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.pagingtypes_v1.PagingType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/common/pagingTypes-v1}Paging" minOccurs="0"/>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaEstadoSolicitud-v1}DataConsultaEstadoSolicitudRespType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paging",
    "data"
})
@XmlRootElement(name = "RespConsultaEstadoSolicitud")
public class RespConsultaEstadoSolicitud {

    @XmlElement(name = "Paging", namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/pagingTypes-v1")
    protected PagingType paging;
    @XmlElement(name = "Data", required = true)
    protected DataConsultaEstadoSolicitudRespType data;

    /**
     * Obtiene el valor de la propiedad paging.
     * 
     * @return
     *     possible object is
     *     {@link PagingType }
     *     
     */
    public PagingType getPaging() {
        return paging;
    }

    /**
     * Define el valor de la propiedad paging.
     * 
     * @param value
     *     allowed object is
     *     {@link PagingType }
     *     
     */
    public void setPaging(PagingType value) {
        this.paging = value;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaEstadoSolicitudRespType }
     *     
     */
    public DataConsultaEstadoSolicitudRespType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaEstadoSolicitudRespType }
     *     
     */
    public void setData(DataConsultaEstadoSolicitudRespType value) {
        this.data = value;
    }

}
