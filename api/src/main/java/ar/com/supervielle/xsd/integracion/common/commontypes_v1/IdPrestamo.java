
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para IdPrestamo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="IdPrestamo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subOpe" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipoOpe" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdPrestamo", propOrder = {
    "sucursal",
    "modulo",
    "cuenta",
    "operacion",
    "subOpe",
    "tipoOpe"
})
public class IdPrestamo {

    protected int sucursal;
    protected int modulo;
    protected int cuenta;
    protected int operacion;
    protected int subOpe;
    protected int tipoOpe;

    /**
     * Obtiene el valor de la propiedad sucursal.
     * 
     */
    public int getSucursal() {
        return sucursal;
    }

    /**
     * Define el valor de la propiedad sucursal.
     * 
     */
    public void setSucursal(int value) {
        this.sucursal = value;
    }

    /**
     * Obtiene el valor de la propiedad modulo.
     * 
     */
    public int getModulo() {
        return modulo;
    }

    /**
     * Define el valor de la propiedad modulo.
     * 
     */
    public void setModulo(int value) {
        this.modulo = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     */
    public int getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     */
    public void setCuenta(int value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     */
    public int getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     */
    public void setOperacion(int value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad subOpe.
     * 
     */
    public int getSubOpe() {
        return subOpe;
    }

    /**
     * Define el valor de la propiedad subOpe.
     * 
     */
    public void setSubOpe(int value) {
        this.subOpe = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOpe.
     * 
     */
    public int getTipoOpe() {
        return tipoOpe;
    }

    /**
     * Define el valor de la propiedad tipoOpe.
     * 
     */
    public void setTipoOpe(int value) {
        this.tipoOpe = value;
    }

}
