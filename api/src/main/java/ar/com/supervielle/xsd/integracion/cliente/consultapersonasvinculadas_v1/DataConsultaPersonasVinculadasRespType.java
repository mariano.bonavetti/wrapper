
package ar.com.supervielle.xsd.integracion.cliente.consultapersonasvinculadas_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;


/**
 * <p>Clase Java para DataConsultaPersonasVinculadasRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaPersonasVinculadasRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PersonasVinculadas">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PersonaVinculada" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="paisOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaPersonasVinculadasRespType", propOrder = {
    "row"
})
public class DataConsultaPersonasVinculadasRespType {

    @XmlElement(name = "Row")
    protected DataConsultaPersonasVinculadasRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaPersonasVinculadasRespType.Row }
     *     
     */
    public DataConsultaPersonasVinculadasRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaPersonasVinculadasRespType.Row }
     *     
     */
    public void setRow(DataConsultaPersonasVinculadasRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PersonasVinculadas">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PersonaVinculada" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="paisOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personasVinculadas"
    })
    public static class Row {

        @XmlElement(name = "PersonasVinculadas", required = true)
        protected DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas personasVinculadas;

        /**
         * Obtiene el valor de la propiedad personasVinculadas.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas }
         *     
         */
        public DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas getPersonasVinculadas() {
            return personasVinculadas;
        }

        /**
         * Define el valor de la propiedad personasVinculadas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas }
         *     
         */
        public void setPersonasVinculadas(DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas value) {
            this.personasVinculadas = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PersonaVinculada" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="paisOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "personaVinculada"
        })
        public static class PersonasVinculadas {

            @XmlElement(name = "PersonaVinculada")
            protected List<DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas.PersonaVinculada> personaVinculada;

            /**
             * Gets the value of the personaVinculada property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the personaVinculada property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPersonaVinculada().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas.PersonaVinculada }
             * 
             * 
             */
            public List<DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas.PersonaVinculada> getPersonaVinculada() {
                if (personaVinculada == null) {
                    personaVinculada = new ArrayList<DataConsultaPersonasVinculadasRespType.Row.PersonasVinculadas.PersonaVinculada>();
                }
                return this.personaVinculada;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="paisOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paisOrigen",
                "tipoDoc",
                "numDoc",
                "nombreApellido",
                "vinculo",
                "cuentaBT"
            })
            public static class PersonaVinculada {

                protected int paisOrigen;
                protected int tipoDoc;
                @XmlElement(required = true)
                protected String numDoc;
                @XmlElement(required = true)
                protected String nombreApellido;
                @XmlElement(required = true)
                protected CodDescNumType vinculo;
                protected int cuentaBT;

                /**
                 * Obtiene el valor de la propiedad paisOrigen.
                 * 
                 */
                public int getPaisOrigen() {
                    return paisOrigen;
                }

                /**
                 * Define el valor de la propiedad paisOrigen.
                 * 
                 */
                public void setPaisOrigen(int value) {
                    this.paisOrigen = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoDoc.
                 * 
                 */
                public int getTipoDoc() {
                    return tipoDoc;
                }

                /**
                 * Define el valor de la propiedad tipoDoc.
                 * 
                 */
                public void setTipoDoc(int value) {
                    this.tipoDoc = value;
                }

                /**
                 * Obtiene el valor de la propiedad numDoc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumDoc() {
                    return numDoc;
                }

                /**
                 * Define el valor de la propiedad numDoc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumDoc(String value) {
                    this.numDoc = value;
                }

                /**
                 * Obtiene el valor de la propiedad nombreApellido.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNombreApellido() {
                    return nombreApellido;
                }

                /**
                 * Define el valor de la propiedad nombreApellido.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNombreApellido(String value) {
                    this.nombreApellido = value;
                }

                /**
                 * Obtiene el valor de la propiedad vinculo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getVinculo() {
                    return vinculo;
                }

                /**
                 * Define el valor de la propiedad vinculo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setVinculo(CodDescNumType value) {
                    this.vinculo = value;
                }

                /**
                 * Obtiene el valor de la propiedad cuentaBT.
                 * 
                 */
                public int getCuentaBT() {
                    return cuentaBT;
                }

                /**
                 * Define el valor de la propiedad cuentaBT.
                 * 
                 */
                public void setCuentaBT(int value) {
                    this.cuentaBT = value;
                }

            }

        }

    }

}
