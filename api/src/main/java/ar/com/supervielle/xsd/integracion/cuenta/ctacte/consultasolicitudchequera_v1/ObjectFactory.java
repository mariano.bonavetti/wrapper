
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasolicitudchequera_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasolicitudchequera_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasolicitudchequera_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudChequeraRespType }
     * 
     */
    public DataConsultaSolicitudChequeraRespType createDataConsultaSolicitudChequeraRespType() {
        return new DataConsultaSolicitudChequeraRespType();
    }

    /**
     * Create an instance of {@link RespConsultaSolicitudChequera }
     * 
     */
    public RespConsultaSolicitudChequera createRespConsultaSolicitudChequera() {
        return new RespConsultaSolicitudChequera();
    }

    /**
     * Create an instance of {@link ReqConsultaSolicitudChequera }
     * 
     */
    public ReqConsultaSolicitudChequera createReqConsultaSolicitudChequera() {
        return new ReqConsultaSolicitudChequera();
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudChequeraReqType }
     * 
     */
    public DataConsultaSolicitudChequeraReqType createDataConsultaSolicitudChequeraReqType() {
        return new DataConsultaSolicitudChequeraReqType();
    }

    /**
     * Create an instance of {@link DataConsultaSolicitudChequeraRespType.Row }
     * 
     */
    public DataConsultaSolicitudChequeraRespType.Row createDataConsultaSolicitudChequeraRespTypeRow() {
        return new DataConsultaSolicitudChequeraRespType.Row();
    }

}
