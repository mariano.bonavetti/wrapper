
package ar.com.supervielle.xsd.integracion.common.pagingtypes_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.common.pagingtypes_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Paging_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/common/pagingTypes-v1", "Paging");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.common.pagingtypes_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PagingType }
     * 
     */
    public PagingType createPagingType() {
        return new PagingType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/pagingTypes-v1", name = "Paging")
    public JAXBElement<PagingType> createPaging(PagingType value) {
        return new JAXBElement<PagingType>(_Paging_QNAME, PagingType.class, null, value);
    }

}
