
package ar.com.supervielle.xsd.integracion.clienteprevisional.cambioestadocpp_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdLiquidacionANSESType;


/**
 * <p>Clase Java para DataCambioEstadoCPPReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataCambioEstadoCPPReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
 *         &lt;element name="tipoImpresion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoTitularidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataCambioEstadoCPPReqType", propOrder = {
    "identificador",
    "tipoImpresion",
    "tipoTitularidad"
})
public class DataCambioEstadoCPPReqType {

    @XmlElement(required = true)
    protected IdLiquidacionANSESType identificador;
    @XmlElement(required = true)
    protected String tipoImpresion;
    protected String tipoTitularidad;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdLiquidacionANSESType }
     *     
     */
    public IdLiquidacionANSESType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdLiquidacionANSESType }
     *     
     */
    public void setIdentificador(IdLiquidacionANSESType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoImpresion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoImpresion() {
        return tipoImpresion;
    }

    /**
     * Define el valor de la propiedad tipoImpresion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoImpresion(String value) {
        this.tipoImpresion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoTitularidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTitularidad() {
        return tipoTitularidad;
    }

    /**
     * Define el valor de la propiedad tipoTitularidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTitularidad(String value) {
        this.tipoTitularidad = value;
    }

}
