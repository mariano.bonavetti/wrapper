
package ar.com.supervielle.xsd.integracion.cliente.altamodificaciontelefono_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataAltaModificacionTelefonoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaModificacionTelefonoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *         &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipoOpe" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Telefono">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="carateristica" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="telefonoAnterior" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaModificacionTelefonoReqType", propOrder = {
    "identificador",
    "codDomicilio",
    "tipoOpe",
    "telefono"
})
public class DataAltaModificacionTelefonoReqType {

    @XmlElement(name = "Identificador", required = true)
    protected IdClienteType identificador;
    protected int codDomicilio;
    @XmlElement(required = true)
    protected String tipoOpe;
    @XmlElement(name = "Telefono", required = true)
    protected DataAltaModificacionTelefonoReqType.Telefono telefono;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad codDomicilio.
     * 
     */
    public int getCodDomicilio() {
        return codDomicilio;
    }

    /**
     * Define el valor de la propiedad codDomicilio.
     * 
     */
    public void setCodDomicilio(int value) {
        this.codDomicilio = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOpe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOpe() {
        return tipoOpe;
    }

    /**
     * Define el valor de la propiedad tipoOpe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOpe(String value) {
        this.tipoOpe = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaModificacionTelefonoReqType.Telefono }
     *     
     */
    public DataAltaModificacionTelefonoReqType.Telefono getTelefono() {
        return telefono;
    }

    /**
     * Define el valor de la propiedad telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaModificacionTelefonoReqType.Telefono }
     *     
     */
    public void setTelefono(DataAltaModificacionTelefonoReqType.Telefono value) {
        this.telefono = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="carateristica" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="telefonoAnterior" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "area",
        "carateristica",
        "numero",
        "tipo",
        "telefonoAnterior"
    })
    public static class Telefono {

        @XmlElement(required = true)
        protected String area;
        @XmlElement(required = true)
        protected String carateristica;
        @XmlElement(required = true)
        protected String numero;
        @XmlElement(required = true)
        protected String tipo;
        @XmlElement(required = true)
        protected String telefonoAnterior;

        /**
         * Obtiene el valor de la propiedad area.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArea() {
            return area;
        }

        /**
         * Define el valor de la propiedad area.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArea(String value) {
            this.area = value;
        }

        /**
         * Obtiene el valor de la propiedad carateristica.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCarateristica() {
            return carateristica;
        }

        /**
         * Define el valor de la propiedad carateristica.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCarateristica(String value) {
            this.carateristica = value;
        }

        /**
         * Obtiene el valor de la propiedad numero.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumero() {
            return numero;
        }

        /**
         * Define el valor de la propiedad numero.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumero(String value) {
            this.numero = value;
        }

        /**
         * Obtiene el valor de la propiedad tipo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipo() {
            return tipo;
        }

        /**
         * Define el valor de la propiedad tipo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipo(String value) {
            this.tipo = value;
        }

        /**
         * Obtiene el valor de la propiedad telefonoAnterior.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTelefonoAnterior() {
            return telefonoAnterior;
        }

        /**
         * Define el valor de la propiedad telefonoAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTelefonoAnterior(String value) {
            this.telefonoAnterior = value;
        }

    }

}
