
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataAltaAcuerdosReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAcuerdosReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codEmpresa">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codPaquete" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="acuerdo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="transacccion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="plazo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaapertura" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="tasa">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tipoTasa" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="valorTasa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="revision">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                         &lt;totalDigits value="5"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="cuenta">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAcuerdosReqType", propOrder = {
    "codEmpresa",
    "codPaquete",
    "acuerdo",
    "cuenta"
})
public class DataAltaAcuerdosReqType {

    @XmlElement(required = true)
    protected BigInteger codEmpresa;
    @XmlElement(required = true)
    protected BigInteger codPaquete;
    @XmlElement(required = true)
    protected DataAltaAcuerdosReqType.Acuerdo acuerdo;
    @XmlElement(required = true)
    protected DataAltaAcuerdosReqType.Cuenta cuenta;

    /**
     * Obtiene el valor de la propiedad codEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCodEmpresa() {
        return codEmpresa;
    }

    /**
     * Define el valor de la propiedad codEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCodEmpresa(BigInteger value) {
        this.codEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad codPaquete.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCodPaquete() {
        return codPaquete;
    }

    /**
     * Define el valor de la propiedad codPaquete.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCodPaquete(BigInteger value) {
        this.codPaquete = value;
    }

    /**
     * Obtiene el valor de la propiedad acuerdo.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAcuerdosReqType.Acuerdo }
     *     
     */
    public DataAltaAcuerdosReqType.Acuerdo getAcuerdo() {
        return acuerdo;
    }

    /**
     * Define el valor de la propiedad acuerdo.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAcuerdosReqType.Acuerdo }
     *     
     */
    public void setAcuerdo(DataAltaAcuerdosReqType.Acuerdo value) {
        this.acuerdo = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAcuerdosReqType.Cuenta }
     *     
     */
    public DataAltaAcuerdosReqType.Cuenta getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAcuerdosReqType.Cuenta }
     *     
     */
    public void setCuenta(DataAltaAcuerdosReqType.Cuenta value) {
        this.cuenta = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="transacccion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="plazo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaapertura" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="tasa">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tipoTasa" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="valorTasa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="revision">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *               &lt;totalDigits value="5"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sucursal",
        "modulo",
        "transacccion",
        "plazo",
        "fechaapertura",
        "importe",
        "tasa",
        "revision"
    })
    public static class Acuerdo {

        @XmlElement(required = true)
        protected BigInteger sucursal;
        @XmlElement(required = true)
        protected BigInteger modulo;
        @XmlElement(required = true)
        protected BigInteger transacccion;
        @XmlElement(required = true)
        protected String plazo;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaapertura;
        @XmlElement(required = true)
        protected BigDecimal importe;
        @XmlElement(required = true)
        protected DataAltaAcuerdosReqType.Acuerdo.Tasa tasa;
        @XmlElement(required = true)
        protected BigInteger revision;

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSucursal(BigInteger value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad modulo.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getModulo() {
            return modulo;
        }

        /**
         * Define el valor de la propiedad modulo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setModulo(BigInteger value) {
            this.modulo = value;
        }

        /**
         * Obtiene el valor de la propiedad transacccion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTransacccion() {
            return transacccion;
        }

        /**
         * Define el valor de la propiedad transacccion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTransacccion(BigInteger value) {
            this.transacccion = value;
        }

        /**
         * Obtiene el valor de la propiedad plazo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlazo() {
            return plazo;
        }

        /**
         * Define el valor de la propiedad plazo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlazo(String value) {
            this.plazo = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaapertura.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaapertura() {
            return fechaapertura;
        }

        /**
         * Define el valor de la propiedad fechaapertura.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaapertura(XMLGregorianCalendar value) {
            this.fechaapertura = value;
        }

        /**
         * Obtiene el valor de la propiedad importe.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getImporte() {
            return importe;
        }

        /**
         * Define el valor de la propiedad importe.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setImporte(BigDecimal value) {
            this.importe = value;
        }

        /**
         * Obtiene el valor de la propiedad tasa.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaAcuerdosReqType.Acuerdo.Tasa }
         *     
         */
        public DataAltaAcuerdosReqType.Acuerdo.Tasa getTasa() {
            return tasa;
        }

        /**
         * Define el valor de la propiedad tasa.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaAcuerdosReqType.Acuerdo.Tasa }
         *     
         */
        public void setTasa(DataAltaAcuerdosReqType.Acuerdo.Tasa value) {
            this.tasa = value;
        }

        /**
         * Obtiene el valor de la propiedad revision.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRevision() {
            return revision;
        }

        /**
         * Define el valor de la propiedad revision.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRevision(BigInteger value) {
            this.revision = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tipoTasa" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="valorTasa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tipoTasa",
            "valorTasa"
        })
        public static class Tasa {

            @XmlElement(required = true)
            protected BigInteger tipoTasa;
            @XmlElement(required = true)
            protected BigDecimal valorTasa;

            /**
             * Obtiene el valor de la propiedad tipoTasa.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTipoTasa() {
                return tipoTasa;
            }

            /**
             * Define el valor de la propiedad tipoTasa.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTipoTasa(BigInteger value) {
                this.tipoTasa = value;
            }

            /**
             * Obtiene el valor de la propiedad valorTasa.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getValorTasa() {
                return valorTasa;
            }

            /**
             * Define el valor de la propiedad valorTasa.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setValorTasa(BigDecimal value) {
                this.valorTasa = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroCuenta",
        "codOperacion",
        "subOperacion",
        "tipoOperacion",
        "moneda",
        "sucursal"
    })
    public static class Cuenta {

        @XmlElement(required = true)
        protected BigInteger nroCuenta;
        @XmlElement(required = true)
        protected BigInteger codOperacion;
        @XmlElement(required = true)
        protected BigInteger subOperacion;
        @XmlElement(required = true)
        protected BigInteger tipoOperacion;
        @XmlElement(required = true)
        protected BigInteger moneda;
        @XmlElement(required = true)
        protected BigInteger sucursal;

        /**
         * Obtiene el valor de la propiedad nroCuenta.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNroCuenta() {
            return nroCuenta;
        }

        /**
         * Define el valor de la propiedad nroCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNroCuenta(BigInteger value) {
            this.nroCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad codOperacion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCodOperacion() {
            return codOperacion;
        }

        /**
         * Define el valor de la propiedad codOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCodOperacion(BigInteger value) {
            this.codOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad subOperacion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSubOperacion() {
            return subOperacion;
        }

        /**
         * Define el valor de la propiedad subOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSubOperacion(BigInteger value) {
            this.subOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoOperacion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTipoOperacion() {
            return tipoOperacion;
        }

        /**
         * Define el valor de la propiedad tipoOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTipoOperacion(BigInteger value) {
            this.tipoOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMoneda(BigInteger value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSucursal(BigInteger value) {
            this.sucursal = value;
        }

    }

}
