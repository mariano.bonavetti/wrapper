
package ar.com.supervielle.xsd.integracion.header_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.header_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SoapHeaderRes_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/Header-v1", "soapHeaderRes");
    private final static QName _SoapHeaderReq_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/Header-v1", "soapHeaderReq");
    private final static QName _Header_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/Header-v1", "Header");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.header_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SessionContext }
     * 
     */
    public SessionContext createSessionContext() {
        return new SessionContext();
    }

    /**
     * Create an instance of {@link ResponseData }
     * 
     */
    public ResponseData createResponseData() {
        return new ResponseData();
    }

    /**
     * Create an instance of {@link InternalHeader }
     * 
     */
    public InternalHeader createInternalHeader() {
        return new InternalHeader();
    }

    /**
     * Create an instance of {@link InternalHeader.ConsumerReferences }
     * 
     */
    public InternalHeader.ConsumerReferences createInternalHeaderConsumerReferences() {
        return new InternalHeader.ConsumerReferences();
    }

    /**
     * Create an instance of {@link SoapHeaderReq }
     * 
     */
    public SoapHeaderReq createSoapHeaderReq() {
        return new SoapHeaderReq();
    }

    /**
     * Create an instance of {@link SoapHeaderRes }
     * 
     */
    public SoapHeaderRes createSoapHeaderRes() {
        return new SoapHeaderRes();
    }

    /**
     * Create an instance of {@link Destination }
     * 
     */
    public Destination createDestination() {
        return new Destination();
    }

    /**
     * Create an instance of {@link Classifications }
     * 
     */
    public Classifications createClassifications() {
        return new Classifications();
    }

    /**
     * Create an instance of {@link RequestData }
     * 
     */
    public RequestData createRequestData() {
        return new RequestData();
    }

    /**
     * Create an instance of {@link UsernameToken }
     * 
     */
    public UsernameToken createUsernameToken() {
        return new UsernameToken();
    }

    /**
     * Create an instance of {@link MessageContext }
     * 
     */
    public MessageContext createMessageContext() {
        return new MessageContext();
    }

    /**
     * Create an instance of {@link Property }
     * 
     */
    public Property createProperty() {
        return new Property();
    }

    /**
     * Create an instance of {@link EndpointReference }
     * 
     */
    public EndpointReference createEndpointReference() {
        return new EndpointReference();
    }

    /**
     * Create an instance of {@link SessionContext.ClientId }
     * 
     */
    public SessionContext.ClientId createSessionContextClientId() {
        return new SessionContext.ClientId();
    }

    /**
     * Create an instance of {@link SessionContext.Session }
     * 
     */
    public SessionContext.Session createSessionContextSession() {
        return new SessionContext.Session();
    }

    /**
     * Create an instance of {@link ResponseData.Warnings }
     * 
     */
    public ResponseData.Warnings createResponseDataWarnings() {
        return new ResponseData.Warnings();
    }

    /**
     * Create an instance of {@link InternalHeader.ConsumerReferences.RouteStack }
     * 
     */
    public InternalHeader.ConsumerReferences.RouteStack createInternalHeaderConsumerReferencesRouteStack() {
        return new InternalHeader.ConsumerReferences.RouteStack();
    }

    /**
     * Create an instance of {@link SoapHeaderReq.RuntimeContext }
     * 
     */
    public SoapHeaderReq.RuntimeContext createSoapHeaderReqRuntimeContext() {
        return new SoapHeaderReq.RuntimeContext();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SoapHeaderRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/Header-v1", name = "soapHeaderRes")
    public JAXBElement<SoapHeaderRes> createSoapHeaderRes(SoapHeaderRes value) {
        return new JAXBElement<SoapHeaderRes>(_SoapHeaderRes_QNAME, SoapHeaderRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SoapHeaderReq }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/Header-v1", name = "soapHeaderReq")
    public JAXBElement<SoapHeaderReq> createSoapHeaderReq(SoapHeaderReq value) {
        return new JAXBElement<SoapHeaderReq>(_SoapHeaderReq_QNAME, SoapHeaderReq.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InternalHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/Header-v1", name = "Header")
    public JAXBElement<InternalHeader> createHeader(InternalHeader value) {
        return new JAXBElement<InternalHeader>(_Header_QNAME, InternalHeader.class, null, value);
    }

}
