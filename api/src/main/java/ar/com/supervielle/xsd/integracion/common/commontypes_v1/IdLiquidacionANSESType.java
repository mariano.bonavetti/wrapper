
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para IdLiquidacionANSESType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="IdLiquidacionANSESType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuil" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="beneficio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nroLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="periodo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="dataTypeName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="idLiquidacionANSESType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdLiquidacionANSESType", propOrder = {
    "cuil",
    "beneficio",
    "nroLiquidacion",
    "periodo",
    "formaPago"
})
public class IdLiquidacionANSESType {

    @XmlElement(required = true)
    protected String cuil;
    @XmlElement(required = true)
    protected String beneficio;
    @XmlElement(required = true)
    protected String nroLiquidacion;
    @XmlElement(required = true)
    protected String periodo;
    @XmlElement(required = true)
    protected String formaPago;
    @XmlAttribute(name = "dataTypeName", required = true)
    protected String dataTypeName;

    /**
     * Obtiene el valor de la propiedad cuil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuil() {
        return cuil;
    }

    /**
     * Define el valor de la propiedad cuil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuil(String value) {
        this.cuil = value;
    }

    /**
     * Obtiene el valor de la propiedad beneficio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficio() {
        return beneficio;
    }

    /**
     * Define el valor de la propiedad beneficio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficio(String value) {
        this.beneficio = value;
    }

    /**
     * Obtiene el valor de la propiedad nroLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroLiquidacion() {
        return nroLiquidacion;
    }

    /**
     * Define el valor de la propiedad nroLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroLiquidacion(String value) {
        this.nroLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad periodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * Define el valor de la propiedad periodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodo(String value) {
        this.periodo = value;
    }

    /**
     * Obtiene el valor de la propiedad formaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Define el valor de la propiedad formaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormaPago(String value) {
        this.formaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad dataTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTypeName() {
        if (dataTypeName == null) {
            return "idLiquidacionANSESType";
        } else {
            return dataTypeName;
        }
    }

    /**
     * Define el valor de la propiedad dataTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTypeName(String value) {
        this.dataTypeName = value;
    }

}
