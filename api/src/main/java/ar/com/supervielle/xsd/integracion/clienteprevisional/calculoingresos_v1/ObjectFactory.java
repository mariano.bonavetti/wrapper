
package ar.com.supervielle.xsd.integracion.clienteprevisional.calculoingresos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.clienteprevisional.calculoingresos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.clienteprevisional.calculoingresos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataCalculoIngresosRespType }
     * 
     */
    public DataCalculoIngresosRespType createDataCalculoIngresosRespType() {
        return new DataCalculoIngresosRespType();
    }

    /**
     * Create an instance of {@link DataCalculoIngresosRespType.Row }
     * 
     */
    public DataCalculoIngresosRespType.Row createDataCalculoIngresosRespTypeRow() {
        return new DataCalculoIngresosRespType.Row();
    }

    /**
     * Create an instance of {@link DataCalculoIngresosRespType.Row.Beneficios }
     * 
     */
    public DataCalculoIngresosRespType.Row.Beneficios createDataCalculoIngresosRespTypeRowBeneficios() {
        return new DataCalculoIngresosRespType.Row.Beneficios();
    }

    /**
     * Create an instance of {@link ReqCalculoIngresos }
     * 
     */
    public ReqCalculoIngresos createReqCalculoIngresos() {
        return new ReqCalculoIngresos();
    }

    /**
     * Create an instance of {@link DataCalculoIngresosReqType }
     * 
     */
    public DataCalculoIngresosReqType createDataCalculoIngresosReqType() {
        return new DataCalculoIngresosReqType();
    }

    /**
     * Create an instance of {@link RespCalculoIngresos }
     * 
     */
    public RespCalculoIngresos createRespCalculoIngresos() {
        return new RespCalculoIngresos();
    }

    /**
     * Create an instance of {@link DataCalculoIngresosRespType.Row.Beneficios.Beneficio }
     * 
     */
    public DataCalculoIngresosRespType.Row.Beneficios.Beneficio createDataCalculoIngresosRespTypeRowBeneficiosBeneficio() {
        return new DataCalculoIngresosRespType.Row.Beneficios.Beneficio();
    }

}
