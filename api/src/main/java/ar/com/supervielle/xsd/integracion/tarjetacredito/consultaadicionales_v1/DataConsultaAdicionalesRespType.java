
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaadicionales_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaAdicionalesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaAdicionalesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *                   &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tarjetaHabiente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="fechaVtoTarjeta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaUltProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="entidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="saldos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="inhabilitacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaAdicionalesRespType", propOrder = {
    "row"
})
public class DataConsultaAdicionalesRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaAdicionalesRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaAdicionalesRespType.Row }
     * 
     * 
     */
    public List<DataConsultaAdicionalesRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaAdicionalesRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
     *         &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tarjetaHabiente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="nombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipoTarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="fechaVtoTarjeta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaUltProcesamiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="entidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="banco" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="saldos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="inhabilitacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "numeroTarjeta",
        "tarjetaHabiente",
        "nombreApellido",
        "titularidad",
        "marca",
        "cuentaTarjeta",
        "tipoTarjeta",
        "sigla",
        "cartera",
        "fechaVtoTarjeta",
        "fechaProcesamiento",
        "fechaUltProcesamiento",
        "entidad",
        "banco",
        "saldos",
        "fechaCierre",
        "estado",
        "inhabilitacion",
        "limiteCompra",
        "vigenciaDesde",
        "vigenciaHasta"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdTarjetaType identificador;
        @XmlElement(required = true)
        protected String numeroTarjeta;
        @XmlElement(required = true)
        protected IdClienteType tarjetaHabiente;
        @XmlElement(required = true)
        protected String nombreApellido;
        @XmlElement(required = true)
        protected CodDescStringType titularidad;
        @XmlElement(required = true)
        protected CodDescStringType marca;
        @XmlElement(required = true)
        protected String cuentaTarjeta;
        @XmlElement(required = true)
        protected CodDescStringType tipoTarjeta;
        @XmlElement(required = true)
        protected String sigla;
        @XmlElement(required = true)
        protected CodDescStringType cartera;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaVtoTarjeta;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaProcesamiento;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaUltProcesamiento;
        @XmlElement(required = true)
        protected CodDescStringType entidad;
        @XmlElement(required = true)
        protected CodDescStringType banco;
        @XmlElement(required = true)
        protected DataConsultaAdicionalesRespType.Row.Saldos saldos;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaCierre;
        @XmlElement(required = true)
        protected CodDescStringType estado;
        @XmlElement(required = true)
        protected CodDescStringType inhabilitacion;
        protected BigDecimal limiteCompra;
        @XmlElement(required = true)
        protected String vigenciaDesde;
        @XmlElement(required = true)
        protected String vigenciaHasta;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdTarjetaType }
         *     
         */
        public IdTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdTarjetaType }
         *     
         */
        public void setIdentificador(IdTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroTarjeta() {
            return numeroTarjeta;
        }

        /**
         * Define el valor de la propiedad numeroTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroTarjeta(String value) {
            this.numeroTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad tarjetaHabiente.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getTarjetaHabiente() {
            return tarjetaHabiente;
        }

        /**
         * Define el valor de la propiedad tarjetaHabiente.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setTarjetaHabiente(IdClienteType value) {
            this.tarjetaHabiente = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreApellido.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreApellido() {
            return nombreApellido;
        }

        /**
         * Define el valor de la propiedad nombreApellido.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreApellido(String value) {
            this.nombreApellido = value;
        }

        /**
         * Obtiene el valor de la propiedad titularidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTitularidad() {
            return titularidad;
        }

        /**
         * Define el valor de la propiedad titularidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTitularidad(CodDescStringType value) {
            this.titularidad = value;
        }

        /**
         * Obtiene el valor de la propiedad marca.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getMarca() {
            return marca;
        }

        /**
         * Define el valor de la propiedad marca.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setMarca(CodDescStringType value) {
            this.marca = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaTarjeta() {
            return cuentaTarjeta;
        }

        /**
         * Define el valor de la propiedad cuentaTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaTarjeta(String value) {
            this.cuentaTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipoTarjeta() {
            return tipoTarjeta;
        }

        /**
         * Define el valor de la propiedad tipoTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipoTarjeta(CodDescStringType value) {
            this.tipoTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad sigla.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSigla() {
            return sigla;
        }

        /**
         * Define el valor de la propiedad sigla.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSigla(String value) {
            this.sigla = value;
        }

        /**
         * Obtiene el valor de la propiedad cartera.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getCartera() {
            return cartera;
        }

        /**
         * Define el valor de la propiedad cartera.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setCartera(CodDescStringType value) {
            this.cartera = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaVtoTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaVtoTarjeta() {
            return fechaVtoTarjeta;
        }

        /**
         * Define el valor de la propiedad fechaVtoTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaVtoTarjeta(XMLGregorianCalendar value) {
            this.fechaVtoTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaProcesamiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaProcesamiento() {
            return fechaProcesamiento;
        }

        /**
         * Define el valor de la propiedad fechaProcesamiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaProcesamiento(XMLGregorianCalendar value) {
            this.fechaProcesamiento = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaUltProcesamiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaUltProcesamiento() {
            return fechaUltProcesamiento;
        }

        /**
         * Define el valor de la propiedad fechaUltProcesamiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaUltProcesamiento(XMLGregorianCalendar value) {
            this.fechaUltProcesamiento = value;
        }

        /**
         * Obtiene el valor de la propiedad entidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEntidad() {
            return entidad;
        }

        /**
         * Define el valor de la propiedad entidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEntidad(CodDescStringType value) {
            this.entidad = value;
        }

        /**
         * Obtiene el valor de la propiedad banco.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getBanco() {
            return banco;
        }

        /**
         * Define el valor de la propiedad banco.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setBanco(CodDescStringType value) {
            this.banco = value;
        }

        /**
         * Obtiene el valor de la propiedad saldos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaAdicionalesRespType.Row.Saldos }
         *     
         */
        public DataConsultaAdicionalesRespType.Row.Saldos getSaldos() {
            return saldos;
        }

        /**
         * Define el valor de la propiedad saldos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaAdicionalesRespType.Row.Saldos }
         *     
         */
        public void setSaldos(DataConsultaAdicionalesRespType.Row.Saldos value) {
            this.saldos = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCierre.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaCierre() {
            return fechaCierre;
        }

        /**
         * Define el valor de la propiedad fechaCierre.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaCierre(XMLGregorianCalendar value) {
            this.fechaCierre = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad inhabilitacion.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getInhabilitacion() {
            return inhabilitacion;
        }

        /**
         * Define el valor de la propiedad inhabilitacion.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setInhabilitacion(CodDescStringType value) {
            this.inhabilitacion = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompra.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompra() {
            return limiteCompra;
        }

        /**
         * Define el valor de la propiedad limiteCompra.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompra(BigDecimal value) {
            this.limiteCompra = value;
        }

        /**
         * Obtiene el valor de la propiedad vigenciaDesde.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVigenciaDesde() {
            return vigenciaDesde;
        }

        /**
         * Define el valor de la propiedad vigenciaDesde.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVigenciaDesde(String value) {
            this.vigenciaDesde = value;
        }

        /**
         * Obtiene el valor de la propiedad vigenciaHasta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVigenciaHasta() {
            return vigenciaHasta;
        }

        /**
         * Define el valor de la propiedad vigenciaHasta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVigenciaHasta(String value) {
            this.vigenciaHasta = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "saldoPesos",
            "saldoDolares"
        })
        public static class Saldos {

            protected BigDecimal saldoPesos;
            protected BigDecimal saldoDolares;

            /**
             * Obtiene el valor de la propiedad saldoPesos.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldoPesos() {
                return saldoPesos;
            }

            /**
             * Define el valor de la propiedad saldoPesos.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldoPesos(BigDecimal value) {
                this.saldoPesos = value;
            }

            /**
             * Obtiene el valor de la propiedad saldoDolares.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldoDolares() {
                return saldoDolares;
            }

            /**
             * Define el valor de la propiedad saldoDolares.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldoDolares(BigDecimal value) {
                this.saldoDolares = value;
            }

        }

    }

}
