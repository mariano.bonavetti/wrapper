
package ar.com.supervielle.xsd.integracion.clienteprevisional.obtenercpp_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdLiquidacionANSESType;


/**
 * <p>Clase Java para DataObtenerCPPRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataObtenerCPPRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
 *                   &lt;element name="lineasCabecera" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="lineasDetalle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="lineasPie" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="comprobante" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataObtenerCPPRespType", propOrder = {
    "row"
})
public class DataObtenerCPPRespType {

    @XmlElement(name = "Row")
    protected DataObtenerCPPRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataObtenerCPPRespType.Row }
     *     
     */
    public DataObtenerCPPRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataObtenerCPPRespType.Row }
     *     
     */
    public void setRow(DataObtenerCPPRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
     *         &lt;element name="lineasCabecera" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="lineasDetalle" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="lineasPie" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="comprobante" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "lineasCabecera",
        "lineasDetalle",
        "lineasPie",
        "comprobante"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdLiquidacionANSESType identificador;
        protected int lineasCabecera;
        protected int lineasDetalle;
        protected int lineasPie;
        @XmlElement(required = true)
        protected String comprobante;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdLiquidacionANSESType }
         *     
         */
        public IdLiquidacionANSESType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdLiquidacionANSESType }
         *     
         */
        public void setIdentificador(IdLiquidacionANSESType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad lineasCabecera.
         * 
         */
        public int getLineasCabecera() {
            return lineasCabecera;
        }

        /**
         * Define el valor de la propiedad lineasCabecera.
         * 
         */
        public void setLineasCabecera(int value) {
            this.lineasCabecera = value;
        }

        /**
         * Obtiene el valor de la propiedad lineasDetalle.
         * 
         */
        public int getLineasDetalle() {
            return lineasDetalle;
        }

        /**
         * Define el valor de la propiedad lineasDetalle.
         * 
         */
        public void setLineasDetalle(int value) {
            this.lineasDetalle = value;
        }

        /**
         * Obtiene el valor de la propiedad lineasPie.
         * 
         */
        public int getLineasPie() {
            return lineasPie;
        }

        /**
         * Define el valor de la propiedad lineasPie.
         * 
         */
        public void setLineasPie(int value) {
            this.lineasPie = value;
        }

        /**
         * Obtiene el valor de la propiedad comprobante.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComprobante() {
            return comprobante;
        }

        /**
         * Define el valor de la propiedad comprobante.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComprobante(String value) {
            this.comprobante = value;
        }

    }

}
