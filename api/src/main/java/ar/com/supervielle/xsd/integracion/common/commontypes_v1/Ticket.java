
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para Ticket complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Ticket">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detalleTicket" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}DetalleTicket" minOccurs="0"/>
 *         &lt;element name="empresa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}EmpresaBasePMC" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fiid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hostOnLine" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="nroControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nroTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sorteo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}ISorteo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ticket", propOrder = {
    "canal",
    "detalleTicket",
    "empresa",
    "fecha",
    "fiid",
    "hostOnLine",
    "nroControl",
    "nroTransaccion",
    "sorteo"
})
public class Ticket {

    protected String canal;
    protected DetalleTicket detalleTicket;
    protected EmpresaBasePMC empresa;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fecha;
    protected String fiid;
    protected Boolean hostOnLine;
    protected String nroControl;
    protected String nroTransaccion;
    protected ISorteo sorteo;

    /**
     * Obtiene el valor de la propiedad canal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanal() {
        return canal;
    }

    /**
     * Define el valor de la propiedad canal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanal(String value) {
        this.canal = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleTicket.
     * 
     * @return
     *     possible object is
     *     {@link DetalleTicket }
     *     
     */
    public DetalleTicket getDetalleTicket() {
        return detalleTicket;
    }

    /**
     * Define el valor de la propiedad detalleTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link DetalleTicket }
     *     
     */
    public void setDetalleTicket(DetalleTicket value) {
        this.detalleTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link EmpresaBasePMC }
     *     
     */
    public EmpresaBasePMC getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpresaBasePMC }
     *     
     */
    public void setEmpresa(EmpresaBasePMC value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecha(XMLGregorianCalendar value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad fiid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiid() {
        return fiid;
    }

    /**
     * Define el valor de la propiedad fiid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiid(String value) {
        this.fiid = value;
    }

    /**
     * Obtiene el valor de la propiedad hostOnLine.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHostOnLine() {
        return hostOnLine;
    }

    /**
     * Define el valor de la propiedad hostOnLine.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHostOnLine(Boolean value) {
        this.hostOnLine = value;
    }

    /**
     * Obtiene el valor de la propiedad nroControl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroControl() {
        return nroControl;
    }

    /**
     * Define el valor de la propiedad nroControl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroControl(String value) {
        this.nroControl = value;
    }

    /**
     * Obtiene el valor de la propiedad nroTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroTransaccion() {
        return nroTransaccion;
    }

    /**
     * Define el valor de la propiedad nroTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroTransaccion(String value) {
        this.nroTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad sorteo.
     * 
     * @return
     *     possible object is
     *     {@link ISorteo }
     *     
     */
    public ISorteo getSorteo() {
        return sorteo;
    }

    /**
     * Define el valor de la propiedad sorteo.
     * 
     * @param value
     *     allowed object is
     *     {@link ISorteo }
     *     
     */
    public void setSorteo(ISorteo value) {
        this.sorteo = value;
    }

}
