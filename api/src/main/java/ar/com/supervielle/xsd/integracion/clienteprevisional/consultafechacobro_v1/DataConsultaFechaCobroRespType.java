
package ar.com.supervielle.xsd.integracion.clienteprevisional.consultafechacobro_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdLiquidacionANSESType;


/**
 * <p>Clase Java para DataConsultaFechaCobroRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaFechaCobroRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
 *                   &lt;element name="nombreBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="condicion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaPago" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaAdelanto" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="fechaSigPago" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="grupoPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaFechaCobroRespType", propOrder = {
    "row"
})
public class DataConsultaFechaCobroRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaFechaCobroRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaFechaCobroRespType.Row }
     * 
     * 
     */
    public List<DataConsultaFechaCobroRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaFechaCobroRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
     *         &lt;element name="nombreBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="condicion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaPago" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaAdelanto" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="fechaSigPago" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="grupoPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "nombreBeneficiario",
        "cliente",
        "condicion",
        "fechaPago",
        "fechaAdelanto",
        "estado",
        "fechaSigPago",
        "grupoPago"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdLiquidacionANSESType identificador;
        @XmlElement(required = true)
        protected String nombreBeneficiario;
        @XmlElement(name = "Cliente", required = true)
        protected IdClienteType cliente;
        @XmlElement(required = true)
        protected String condicion;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaPago;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAdelanto;
        @XmlElement(required = true)
        protected CodDescStringType estado;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaSigPago;
        @XmlElement(required = true)
        protected CodDescStringType grupoPago;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdLiquidacionANSESType }
         *     
         */
        public IdLiquidacionANSESType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdLiquidacionANSESType }
         *     
         */
        public void setIdentificador(IdLiquidacionANSESType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreBeneficiario.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreBeneficiario() {
            return nombreBeneficiario;
        }

        /**
         * Define el valor de la propiedad nombreBeneficiario.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreBeneficiario(String value) {
            this.nombreBeneficiario = value;
        }

        /**
         * Obtiene el valor de la propiedad cliente.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getCliente() {
            return cliente;
        }

        /**
         * Define el valor de la propiedad cliente.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setCliente(IdClienteType value) {
            this.cliente = value;
        }

        /**
         * Obtiene el valor de la propiedad condicion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCondicion() {
            return condicion;
        }

        /**
         * Define el valor de la propiedad condicion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCondicion(String value) {
            this.condicion = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaPago.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaPago() {
            return fechaPago;
        }

        /**
         * Define el valor de la propiedad fechaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaPago(XMLGregorianCalendar value) {
            this.fechaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAdelanto.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAdelanto() {
            return fechaAdelanto;
        }

        /**
         * Define el valor de la propiedad fechaAdelanto.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAdelanto(XMLGregorianCalendar value) {
            this.fechaAdelanto = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaSigPago.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaSigPago() {
            return fechaSigPago;
        }

        /**
         * Define el valor de la propiedad fechaSigPago.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaSigPago(XMLGregorianCalendar value) {
            this.fechaSigPago = value;
        }

        /**
         * Obtiene el valor de la propiedad grupoPago.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getGrupoPago() {
            return grupoPago;
        }

        /**
         * Define el valor de la propiedad grupoPago.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setGrupoPago(CodDescStringType value) {
            this.grupoPago = value;
        }

    }

}
