
package ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneamaster_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneamaster_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneamaster_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType2 }
     * 
     */
    public DataAltaInstantaneaMasterReqType2 createDataAltaInstantaneaMasterReqType2() {
        return new DataAltaInstantaneaMasterReqType2();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType2 .Principal }
     * 
     */
    public DataAltaInstantaneaMasterReqType2 .Principal createDataAltaInstantaneaMasterReqType2Principal() {
        return new DataAltaInstantaneaMasterReqType2 .Principal();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType2 .Principal.RegistroAltaBATCH }
     * 
     */
    public DataAltaInstantaneaMasterReqType2 .Principal.RegistroAltaBATCH createDataAltaInstantaneaMasterReqType2PrincipalRegistroAltaBATCH() {
        return new DataAltaInstantaneaMasterReqType2 .Principal.RegistroAltaBATCH();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType2 }
     * 
     */
    public DataAltaInstantaneaMasterRespType2 createDataAltaInstantaneaMasterRespType2() {
        return new DataAltaInstantaneaMasterRespType2();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType2 .Solicitud }
     * 
     */
    public DataAltaInstantaneaMasterRespType2 .Solicitud createDataAltaInstantaneaMasterRespType2Solicitud() {
        return new DataAltaInstantaneaMasterRespType2 .Solicitud();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType }
     * 
     */
    public DataAltaInstantaneaMasterReqType createDataAltaInstantaneaMasterReqType() {
        return new DataAltaInstantaneaMasterReqType();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType.Principal }
     * 
     */
    public DataAltaInstantaneaMasterReqType.Principal createDataAltaInstantaneaMasterReqTypePrincipal() {
        return new DataAltaInstantaneaMasterReqType.Principal();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH }
     * 
     */
    public DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH createDataAltaInstantaneaMasterReqTypePrincipalRegistroAltaBATCH() {
        return new DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType }
     * 
     */
    public DataAltaInstantaneaMasterRespType createDataAltaInstantaneaMasterRespType() {
        return new DataAltaInstantaneaMasterRespType();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType.Solicitud }
     * 
     */
    public DataAltaInstantaneaMasterRespType.Solicitud createDataAltaInstantaneaMasterRespTypeSolicitud() {
        return new DataAltaInstantaneaMasterRespType.Solicitud();
    }

    /**
     * Create an instance of {@link RespAltaInstantaneaMaster }
     * 
     */
    public RespAltaInstantaneaMaster createRespAltaInstantaneaMaster() {
        return new RespAltaInstantaneaMaster();
    }

    /**
     * Create an instance of {@link ReqAltaInstantaneaMaster }
     * 
     */
    public ReqAltaInstantaneaMaster createReqAltaInstantaneaMaster() {
        return new ReqAltaInstantaneaMaster();
    }

    /**
     * Create an instance of {@link RespAltaInstantaneaMaster2 }
     * 
     */
    public RespAltaInstantaneaMaster2 createRespAltaInstantaneaMaster2() {
        return new RespAltaInstantaneaMaster2();
    }

    /**
     * Create an instance of {@link ReqAltaInstantaneaMaster2 }
     * 
     */
    public ReqAltaInstantaneaMaster2 createReqAltaInstantaneaMaster2() {
        return new ReqAltaInstantaneaMaster2();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType2 .Principal.RegistroAltaBATCH.GrupoAfinidad }
     * 
     */
    public DataAltaInstantaneaMasterReqType2 .Principal.RegistroAltaBATCH.GrupoAfinidad createDataAltaInstantaneaMasterReqType2PrincipalRegistroAltaBATCHGrupoAfinidad() {
        return new DataAltaInstantaneaMasterReqType2 .Principal.RegistroAltaBATCH.GrupoAfinidad();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType2 .Error }
     * 
     */
    public DataAltaInstantaneaMasterRespType2 .Error createDataAltaInstantaneaMasterRespType2Error() {
        return new DataAltaInstantaneaMasterRespType2 .Error();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType2 .Solicitud.Error }
     * 
     */
    public DataAltaInstantaneaMasterRespType2 .Solicitud.Error createDataAltaInstantaneaMasterRespType2SolicitudError() {
        return new DataAltaInstantaneaMasterRespType2 .Solicitud.Error();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad }
     * 
     */
    public DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad createDataAltaInstantaneaMasterReqTypePrincipalRegistroAltaBATCHGrupoAfinidad() {
        return new DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType.Error }
     * 
     */
    public DataAltaInstantaneaMasterRespType.Error createDataAltaInstantaneaMasterRespTypeError() {
        return new DataAltaInstantaneaMasterRespType.Error();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaMasterRespType.Solicitud.Error }
     * 
     */
    public DataAltaInstantaneaMasterRespType.Solicitud.Error createDataAltaInstantaneaMasterRespTypeSolicitudError() {
        return new DataAltaInstantaneaMasterRespType.Solicitud.Error();
    }

}
