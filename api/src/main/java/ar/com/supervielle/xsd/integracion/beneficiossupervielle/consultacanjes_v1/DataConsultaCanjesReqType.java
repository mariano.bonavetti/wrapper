
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacanjes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaCanjesReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCanjesReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *         &lt;element name="idPrograma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCanjesReqType", propOrder = {
    "identificador",
    "idPrograma"
})
public class DataConsultaCanjesReqType {

    @XmlElement(required = true)
    protected IdClienteType identificador;
    @XmlElement(required = true)
    protected String idPrograma;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad idPrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPrograma() {
        return idPrograma;
    }

    /**
     * Define el valor de la propiedad idPrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPrograma(String value) {
        this.idPrograma = value;
    }

}
