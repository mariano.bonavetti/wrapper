
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultaproductos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultaproductos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultaproductos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaProductosReqType }
     * 
     */
    public DataConsultaProductosReqType createDataConsultaProductosReqType() {
        return new DataConsultaProductosReqType();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType }
     * 
     */
    public DataConsultaProductosRespType createDataConsultaProductosRespType() {
        return new DataConsultaProductosRespType();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row }
     * 
     */
    public DataConsultaProductosRespType.Row createDataConsultaProductosRespTypeRow() {
        return new DataConsultaProductosRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row.Productos }
     * 
     */
    public DataConsultaProductosRespType.Row.Productos createDataConsultaProductosRespTypeRowProductos() {
        return new DataConsultaProductosRespType.Row.Productos();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row.Productos.Producto }
     * 
     */
    public DataConsultaProductosRespType.Row.Productos.Producto createDataConsultaProductosRespTypeRowProductosProducto() {
        return new DataConsultaProductosRespType.Row.Productos.Producto();
    }

    /**
     * Create an instance of {@link RespConsultaProductos }
     * 
     */
    public RespConsultaProductos createRespConsultaProductos() {
        return new RespConsultaProductos();
    }

    /**
     * Create an instance of {@link ReqConsultaProductos }
     * 
     */
    public ReqConsultaProductos createReqConsultaProductos() {
        return new ReqConsultaProductos();
    }

    /**
     * Create an instance of {@link DataConsultaProductosReqType.Puntos }
     * 
     */
    public DataConsultaProductosReqType.Puntos createDataConsultaProductosReqTypePuntos() {
        return new DataConsultaProductosReqType.Puntos();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row.Productos.Producto.Descripcion }
     * 
     */
    public DataConsultaProductosRespType.Row.Productos.Producto.Descripcion createDataConsultaProductosRespTypeRowProductosProductoDescripcion() {
        return new DataConsultaProductosRespType.Row.Productos.Producto.Descripcion();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row.Productos.Producto.Stock }
     * 
     */
    public DataConsultaProductosRespType.Row.Productos.Producto.Stock createDataConsultaProductosRespTypeRowProductosProductoStock() {
        return new DataConsultaProductosRespType.Row.Productos.Producto.Stock();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row.Productos.Producto.PathImagen }
     * 
     */
    public DataConsultaProductosRespType.Row.Productos.Producto.PathImagen createDataConsultaProductosRespTypeRowProductosProductoPathImagen() {
        return new DataConsultaProductosRespType.Row.Productos.Producto.PathImagen();
    }

    /**
     * Create an instance of {@link DataConsultaProductosRespType.Row.Productos.Producto.Precio }
     * 
     */
    public DataConsultaProductosRespType.Row.Productos.Producto.Precio createDataConsultaProductosRespTypeRowProductosProductoPrecio() {
        return new DataConsultaProductosRespType.Row.Productos.Producto.Precio();
    }

}
