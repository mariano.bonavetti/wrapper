
package ar.com.supervielle.xsd.integracion.clienteprevisional.altaanticipohaberes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Data" type="{http://www.supervielle.com.ar/xsd/Integracion/clientePrevisional/altaAnticipoHaberes-v1}DataAltaAnticipoHaberesReqType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data"
})
@XmlRootElement(name = "ReqAltaAnticipoHaberes", namespace = "http://www.supervielle.com.ar/xsd/Integracion/clientePrevisional/altaAnticipoHaberes-v1")
public class ReqAltaAnticipoHaberes {

    @XmlElement(name = "Data", required = true)
    protected DataAltaAnticipoHaberesReqType data;

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAnticipoHaberesReqType }
     *     
     */
    public DataAltaAnticipoHaberesReqType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAnticipoHaberesReqType }
     *     
     */
    public void setData(DataAltaAnticipoHaberesReqType value) {
        this.data = value;
    }

}
