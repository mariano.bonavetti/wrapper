
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientoscon_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;


/**
 * <p>Clase Java para DataConsultaMovRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaMovRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
 *                   &lt;element name="fechaCont" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaValor" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="hora" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
 *                   &lt;element name="debitoCredito" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="textoComercio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="movimiento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaMovRespType", propOrder = {
    "row"
})
public class DataConsultaMovRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaMovRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaMovRespType.Row }
     * 
     * 
     */
    public List<DataConsultaMovRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaMovRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
     *         &lt;element name="fechaCont" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaValor" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="hora" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
     *         &lt;element name="debitoCredito" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="textoComercio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="movimiento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "fechaCont",
        "fechaValor",
        "hora",
        "debitoCredito",
        "importe",
        "saldo",
        "textoComercio",
        "movimiento"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaCONSUMOType identificador;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaCont;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaValor;
        @XmlSchemaType(name = "time")
        protected XMLGregorianCalendar hora;
        @XmlElement(required = true)
        protected CodDescStringType debitoCredito;
        protected BigDecimal importe;
        protected BigDecimal saldo;
        @XmlElement(required = true)
        protected String textoComercio;
        @XmlElement(required = true)
        protected CodDescStringType movimiento;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public IdCuentaCONSUMOType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public void setIdentificador(IdCuentaCONSUMOType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCont.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaCont() {
            return fechaCont;
        }

        /**
         * Define el valor de la propiedad fechaCont.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaCont(XMLGregorianCalendar value) {
            this.fechaCont = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaValor.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaValor() {
            return fechaValor;
        }

        /**
         * Define el valor de la propiedad fechaValor.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaValor(XMLGregorianCalendar value) {
            this.fechaValor = value;
        }

        /**
         * Obtiene el valor de la propiedad hora.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getHora() {
            return hora;
        }

        /**
         * Define el valor de la propiedad hora.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setHora(XMLGregorianCalendar value) {
            this.hora = value;
        }

        /**
         * Obtiene el valor de la propiedad debitoCredito.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getDebitoCredito() {
            return debitoCredito;
        }

        /**
         * Define el valor de la propiedad debitoCredito.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setDebitoCredito(CodDescStringType value) {
            this.debitoCredito = value;
        }

        /**
         * Obtiene el valor de la propiedad importe.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getImporte() {
            return importe;
        }

        /**
         * Define el valor de la propiedad importe.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setImporte(BigDecimal value) {
            this.importe = value;
        }

        /**
         * Obtiene el valor de la propiedad saldo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldo() {
            return saldo;
        }

        /**
         * Define el valor de la propiedad saldo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldo(BigDecimal value) {
            this.saldo = value;
        }

        /**
         * Obtiene el valor de la propiedad textoComercio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTextoComercio() {
            return textoComercio;
        }

        /**
         * Define el valor de la propiedad textoComercio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTextoComercio(String value) {
            this.textoComercio = value;
        }

        /**
         * Obtiene el valor de la propiedad movimiento.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getMovimiento() {
            return movimiento;
        }

        /**
         * Define el valor de la propiedad movimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setMovimiento(CodDescStringType value) {
            this.movimiento = value;
        }

    }

}
