
package ar.com.supervielle.xsd.integracion.tarjetacredito.altacuenta_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataAltaCuentaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaCuentaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Empresa">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="IdentificadorSolicitud">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                               &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                               &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                               &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Oficial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
 *                     &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
 *                     &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="denominacionBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="codigoBanca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                     &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                     &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="Limites">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="limiteCompraCuenta" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="tipoIVA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                     &lt;element name="cartera" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                     &lt;element name="grupoAfinidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="nombreContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="comercioMadre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RubrosHabilitados" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="RubrosInhabilitados" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="Domicilio" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="codigoGeografico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="emailUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="emailDominio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                     &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="activaControlLimite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CuentaDebito" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="tipoDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="DomicilioCorrespondencia" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="codigoLimitePrestamo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaCuentaReqType", propOrder = {
    "empresa"
})
public class DataAltaCuentaReqType {

    @XmlElement(name = "Empresa")
    protected DataAltaCuentaReqType.Empresa empresa;

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaCuentaReqType.Empresa }
     *     
     */
    public DataAltaCuentaReqType.Empresa getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaCuentaReqType.Empresa }
     *     
     */
    public void setEmpresa(DataAltaCuentaReqType.Empresa value) {
        this.empresa = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdentificadorSolicitud">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codigoEntidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Oficial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType" minOccurs="0"/>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
     *         &lt;element name="cuentaBT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="denominacionBT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="codigoBanca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="tipoProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="cuartaLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Limites">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="limiteCompraCuenta" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tipoIVA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="cartera" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="grupoAfinidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="nombreContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="comercioMadre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RubrosHabilitados" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RubrosInhabilitados" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Domicilio" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codigoGeografico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="emailUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="emailDominio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="activaControlLimite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CuentaDebito" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tipoDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DomicilioCorrespondencia" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="codigoLimitePrestamo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificadorSolicitud",
        "codigoMarca",
        "codigoEntidad",
        "oficial",
        "identificador",
        "cuentaBT",
        "denominacionBT",
        "codigoBanca",
        "tipoProducto",
        "codigoEmpresa",
        "denominacion",
        "cuartaLinea",
        "limites",
        "tipoIVA",
        "cartera",
        "grupoAfinidad",
        "nombreContacto",
        "comercioMadre",
        "rubrosHabilitados",
        "rubrosInhabilitados",
        "domicilio",
        "emailUsuario",
        "emailDominio",
        "sucursal",
        "modeloLiquidacion",
        "activaControlLimite",
        "formaPago",
        "cuentaDebito",
        "domicilioCorrespondencia",
        "codigoLimitePrestamo"
    })
    public static class Empresa {

        @XmlElement(name = "IdentificadorSolicitud", required = true)
        protected DataAltaCuentaReqType.Empresa.IdentificadorSolicitud identificadorSolicitud;
        @XmlElement(required = true)
        protected String codigoMarca;
        @XmlElement(required = true)
        protected String codigoEntidad;
        @XmlElement(name = "Oficial")
        protected CodDescStringType oficial;
        @XmlElement(name = "Identificador")
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected String cuentaBT;
        protected String denominacionBT;
        protected Integer codigoBanca;
        protected BigInteger tipoProducto;
        protected String codigoEmpresa;
        protected String denominacion;
        protected String cuartaLinea;
        @XmlElement(name = "Limites", required = true)
        protected DataAltaCuentaReqType.Empresa.Limites limites;
        protected BigInteger tipoIVA;
        protected BigInteger cartera;
        protected String grupoAfinidad;
        protected String nombreContacto;
        protected String comercioMadre;
        @XmlElement(name = "RubrosHabilitados")
        protected DataAltaCuentaReqType.Empresa.RubrosHabilitados rubrosHabilitados;
        @XmlElement(name = "RubrosInhabilitados")
        protected DataAltaCuentaReqType.Empresa.RubrosInhabilitados rubrosInhabilitados;
        @XmlElement(name = "Domicilio")
        protected DataAltaCuentaReqType.Empresa.Domicilio domicilio;
        protected String emailUsuario;
        protected String emailDominio;
        protected Integer sucursal;
        protected String modeloLiquidacion;
        protected String activaControlLimite;
        protected String formaPago;
        @XmlElement(name = "CuentaDebito")
        protected DataAltaCuentaReqType.Empresa.CuentaDebito cuentaDebito;
        @XmlElement(name = "DomicilioCorrespondencia")
        protected DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia domicilioCorrespondencia;
        protected Integer codigoLimitePrestamo;

        /**
         * Obtiene el valor de la propiedad identificadorSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.IdentificadorSolicitud }
         *     
         */
        public DataAltaCuentaReqType.Empresa.IdentificadorSolicitud getIdentificadorSolicitud() {
            return identificadorSolicitud;
        }

        /**
         * Define el valor de la propiedad identificadorSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.IdentificadorSolicitud }
         *     
         */
        public void setIdentificadorSolicitud(DataAltaCuentaReqType.Empresa.IdentificadorSolicitud value) {
            this.identificadorSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoMarca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoMarca() {
            return codigoMarca;
        }

        /**
         * Define el valor de la propiedad codigoMarca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoMarca(String value) {
            this.codigoMarca = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoEntidad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoEntidad() {
            return codigoEntidad;
        }

        /**
         * Define el valor de la propiedad codigoEntidad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoEntidad(String value) {
            this.codigoEntidad = value;
        }

        /**
         * Obtiene el valor de la propiedad oficial.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getOficial() {
            return oficial;
        }

        /**
         * Define el valor de la propiedad oficial.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setOficial(CodDescStringType value) {
            this.oficial = value;
        }

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaBT.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaBT() {
            return cuentaBT;
        }

        /**
         * Define el valor de la propiedad cuentaBT.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaBT(String value) {
            this.cuentaBT = value;
        }

        /**
         * Obtiene el valor de la propiedad denominacionBT.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDenominacionBT() {
            return denominacionBT;
        }

        /**
         * Define el valor de la propiedad denominacionBT.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDenominacionBT(String value) {
            this.denominacionBT = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoBanca.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodigoBanca() {
            return codigoBanca;
        }

        /**
         * Define el valor de la propiedad codigoBanca.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodigoBanca(Integer value) {
            this.codigoBanca = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoProducto.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTipoProducto() {
            return tipoProducto;
        }

        /**
         * Define el valor de la propiedad tipoProducto.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTipoProducto(BigInteger value) {
            this.tipoProducto = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoEmpresa.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoEmpresa() {
            return codigoEmpresa;
        }

        /**
         * Define el valor de la propiedad codigoEmpresa.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoEmpresa(String value) {
            this.codigoEmpresa = value;
        }

        /**
         * Obtiene el valor de la propiedad denominacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDenominacion() {
            return denominacion;
        }

        /**
         * Define el valor de la propiedad denominacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDenominacion(String value) {
            this.denominacion = value;
        }

        /**
         * Obtiene el valor de la propiedad cuartaLinea.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuartaLinea() {
            return cuartaLinea;
        }

        /**
         * Define el valor de la propiedad cuartaLinea.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuartaLinea(String value) {
            this.cuartaLinea = value;
        }

        /**
         * Obtiene el valor de la propiedad limites.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.Limites }
         *     
         */
        public DataAltaCuentaReqType.Empresa.Limites getLimites() {
            return limites;
        }

        /**
         * Define el valor de la propiedad limites.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.Limites }
         *     
         */
        public void setLimites(DataAltaCuentaReqType.Empresa.Limites value) {
            this.limites = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoIVA.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTipoIVA() {
            return tipoIVA;
        }

        /**
         * Define el valor de la propiedad tipoIVA.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTipoIVA(BigInteger value) {
            this.tipoIVA = value;
        }

        /**
         * Obtiene el valor de la propiedad cartera.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCartera() {
            return cartera;
        }

        /**
         * Define el valor de la propiedad cartera.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCartera(BigInteger value) {
            this.cartera = value;
        }

        /**
         * Obtiene el valor de la propiedad grupoAfinidad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGrupoAfinidad() {
            return grupoAfinidad;
        }

        /**
         * Define el valor de la propiedad grupoAfinidad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGrupoAfinidad(String value) {
            this.grupoAfinidad = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreContacto.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreContacto() {
            return nombreContacto;
        }

        /**
         * Define el valor de la propiedad nombreContacto.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreContacto(String value) {
            this.nombreContacto = value;
        }

        /**
         * Obtiene el valor de la propiedad comercioMadre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComercioMadre() {
            return comercioMadre;
        }

        /**
         * Define el valor de la propiedad comercioMadre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComercioMadre(String value) {
            this.comercioMadre = value;
        }

        /**
         * Obtiene el valor de la propiedad rubrosHabilitados.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.RubrosHabilitados }
         *     
         */
        public DataAltaCuentaReqType.Empresa.RubrosHabilitados getRubrosHabilitados() {
            return rubrosHabilitados;
        }

        /**
         * Define el valor de la propiedad rubrosHabilitados.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.RubrosHabilitados }
         *     
         */
        public void setRubrosHabilitados(DataAltaCuentaReqType.Empresa.RubrosHabilitados value) {
            this.rubrosHabilitados = value;
        }

        /**
         * Obtiene el valor de la propiedad rubrosInhabilitados.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.RubrosInhabilitados }
         *     
         */
        public DataAltaCuentaReqType.Empresa.RubrosInhabilitados getRubrosInhabilitados() {
            return rubrosInhabilitados;
        }

        /**
         * Define el valor de la propiedad rubrosInhabilitados.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.RubrosInhabilitados }
         *     
         */
        public void setRubrosInhabilitados(DataAltaCuentaReqType.Empresa.RubrosInhabilitados value) {
            this.rubrosInhabilitados = value;
        }

        /**
         * Obtiene el valor de la propiedad domicilio.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.Domicilio }
         *     
         */
        public DataAltaCuentaReqType.Empresa.Domicilio getDomicilio() {
            return domicilio;
        }

        /**
         * Define el valor de la propiedad domicilio.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.Domicilio }
         *     
         */
        public void setDomicilio(DataAltaCuentaReqType.Empresa.Domicilio value) {
            this.domicilio = value;
        }

        /**
         * Obtiene el valor de la propiedad emailUsuario.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmailUsuario() {
            return emailUsuario;
        }

        /**
         * Define el valor de la propiedad emailUsuario.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmailUsuario(String value) {
            this.emailUsuario = value;
        }

        /**
         * Obtiene el valor de la propiedad emailDominio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmailDominio() {
            return emailDominio;
        }

        /**
         * Define el valor de la propiedad emailDominio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmailDominio(String value) {
            this.emailDominio = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSucursal(Integer value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad modeloLiquidacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModeloLiquidacion() {
            return modeloLiquidacion;
        }

        /**
         * Define el valor de la propiedad modeloLiquidacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModeloLiquidacion(String value) {
            this.modeloLiquidacion = value;
        }

        /**
         * Obtiene el valor de la propiedad activaControlLimite.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivaControlLimite() {
            return activaControlLimite;
        }

        /**
         * Define el valor de la propiedad activaControlLimite.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivaControlLimite(String value) {
            this.activaControlLimite = value;
        }

        /**
         * Obtiene el valor de la propiedad formaPago.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFormaPago() {
            return formaPago;
        }

        /**
         * Define el valor de la propiedad formaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFormaPago(String value) {
            this.formaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaDebito.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.CuentaDebito }
         *     
         */
        public DataAltaCuentaReqType.Empresa.CuentaDebito getCuentaDebito() {
            return cuentaDebito;
        }

        /**
         * Define el valor de la propiedad cuentaDebito.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.CuentaDebito }
         *     
         */
        public void setCuentaDebito(DataAltaCuentaReqType.Empresa.CuentaDebito value) {
            this.cuentaDebito = value;
        }

        /**
         * Obtiene el valor de la propiedad domicilioCorrespondencia.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia }
         *     
         */
        public DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia getDomicilioCorrespondencia() {
            return domicilioCorrespondencia;
        }

        /**
         * Define el valor de la propiedad domicilioCorrespondencia.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia }
         *     
         */
        public void setDomicilioCorrespondencia(DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia value) {
            this.domicilioCorrespondencia = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoLimitePrestamo.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCodigoLimitePrestamo() {
            return codigoLimitePrestamo;
        }

        /**
         * Define el valor de la propiedad codigoLimitePrestamo.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCodigoLimitePrestamo(Integer value) {
            this.codigoLimitePrestamo = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tipoDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tipoDebito",
            "sucursal",
            "tipo",
            "numero"
        })
        public static class CuentaDebito {

            protected String tipoDebito;
            protected String sucursal;
            protected String tipo;
            protected String numero;

            /**
             * Obtiene el valor de la propiedad tipoDebito.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipoDebito() {
                return tipoDebito;
            }

            /**
             * Define el valor de la propiedad tipoDebito.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipoDebito(String value) {
                this.tipoDebito = value;
            }

            /**
             * Obtiene el valor de la propiedad sucursal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSucursal() {
                return sucursal;
            }

            /**
             * Define el valor de la propiedad sucursal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSucursal(String value) {
                this.sucursal = value;
            }

            /**
             * Obtiene el valor de la propiedad tipo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipo() {
                return tipo;
            }

            /**
             * Define el valor de la propiedad tipo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipo(String value) {
                this.tipo = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codigoGeografico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "calle",
            "numero",
            "piso",
            "lod",
            "nroLOD",
            "codigoPostal",
            "codigoGeografico",
            "partido",
            "telefono"
        })
        public static class Domicilio {

            protected String calle;
            protected String numero;
            protected String piso;
            @XmlElement(name = "LOD")
            protected String lod;
            protected String nroLOD;
            protected String codigoPostal;
            protected String codigoGeografico;
            protected String partido;
            protected String telefono;

            /**
             * Obtiene el valor de la propiedad calle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalle() {
                return calle;
            }

            /**
             * Define el valor de la propiedad calle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalle(String value) {
                this.calle = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

            /**
             * Obtiene el valor de la propiedad piso.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPiso() {
                return piso;
            }

            /**
             * Define el valor de la propiedad piso.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPiso(String value) {
                this.piso = value;
            }

            /**
             * Obtiene el valor de la propiedad lod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLOD() {
                return lod;
            }

            /**
             * Define el valor de la propiedad lod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLOD(String value) {
                this.lod = value;
            }

            /**
             * Obtiene el valor de la propiedad nroLOD.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNroLOD() {
                return nroLOD;
            }

            /**
             * Define el valor de la propiedad nroLOD.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNroLOD(String value) {
                this.nroLOD = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoPostal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoPostal() {
                return codigoPostal;
            }

            /**
             * Define el valor de la propiedad codigoPostal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoPostal(String value) {
                this.codigoPostal = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoGeografico.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoGeografico() {
                return codigoGeografico;
            }

            /**
             * Define el valor de la propiedad codigoGeografico.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoGeografico(String value) {
                this.codigoGeografico = value;
            }

            /**
             * Obtiene el valor de la propiedad partido.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPartido() {
                return partido;
            }

            /**
             * Define el valor de la propiedad partido.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPartido(String value) {
                this.partido = value;
            }

            /**
             * Obtiene el valor de la propiedad telefono.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTelefono() {
                return telefono;
            }

            /**
             * Define el valor de la propiedad telefono.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTelefono(String value) {
                this.telefono = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="LOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="nroLOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "calle",
            "numero",
            "piso",
            "lod",
            "nroLOD",
            "codigoPostal"
        })
        public static class DomicilioCorrespondencia {

            protected String calle;
            protected String numero;
            protected String piso;
            @XmlElement(name = "LOD")
            protected String lod;
            protected String nroLOD;
            protected String codigoPostal;

            /**
             * Obtiene el valor de la propiedad calle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalle() {
                return calle;
            }

            /**
             * Define el valor de la propiedad calle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalle(String value) {
                this.calle = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

            /**
             * Obtiene el valor de la propiedad piso.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPiso() {
                return piso;
            }

            /**
             * Define el valor de la propiedad piso.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPiso(String value) {
                this.piso = value;
            }

            /**
             * Obtiene el valor de la propiedad lod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLOD() {
                return lod;
            }

            /**
             * Define el valor de la propiedad lod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLOD(String value) {
                this.lod = value;
            }

            /**
             * Obtiene el valor de la propiedad nroLOD.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNroLOD() {
                return nroLOD;
            }

            /**
             * Define el valor de la propiedad nroLOD.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNroLOD(String value) {
                this.nroLOD = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoPostal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoPostal() {
                return codigoPostal;
            }

            /**
             * Define el valor de la propiedad codigoPostal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoPostal(String value) {
                this.codigoPostal = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "operacion",
            "tipoSolicitud",
            "nroSolicitudGrupo",
            "nroSolicitud",
            "canal",
            "subCanal"
        })
        public static class IdentificadorSolicitud {

            protected int operacion;
            protected int tipoSolicitud;
            protected BigInteger nroSolicitudGrupo;
            protected BigInteger nroSolicitud;
            @XmlElement(required = true)
            protected BigInteger canal;
            @XmlElement(required = true)
            protected BigInteger subCanal;

            /**
             * Obtiene el valor de la propiedad operacion.
             * 
             */
            public int getOperacion() {
                return operacion;
            }

            /**
             * Define el valor de la propiedad operacion.
             * 
             */
            public void setOperacion(int value) {
                this.operacion = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoSolicitud.
             * 
             */
            public int getTipoSolicitud() {
                return tipoSolicitud;
            }

            /**
             * Define el valor de la propiedad tipoSolicitud.
             * 
             */
            public void setTipoSolicitud(int value) {
                this.tipoSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitudGrupo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitudGrupo() {
                return nroSolicitudGrupo;
            }

            /**
             * Define el valor de la propiedad nroSolicitudGrupo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitudGrupo(BigInteger value) {
                this.nroSolicitudGrupo = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitud() {
                return nroSolicitud;
            }

            /**
             * Define el valor de la propiedad nroSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitud(BigInteger value) {
                this.nroSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad canal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCanal() {
                return canal;
            }

            /**
             * Define el valor de la propiedad canal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCanal(BigInteger value) {
                this.canal = value;
            }

            /**
             * Obtiene el valor de la propiedad subCanal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubCanal() {
                return subCanal;
            }

            /**
             * Define el valor de la propiedad subCanal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubCanal(BigInteger value) {
                this.subCanal = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="limiteCompraCuenta" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "moneda",
            "limiteDisponible",
            "limiteCompra",
            "limiteCompraCuenta"
        })
        public static class Limites {

            protected String moneda;
            protected BigInteger limiteDisponible;
            protected BigInteger limiteCompra;
            protected Double limiteCompraCuenta;

            /**
             * Obtiene el valor de la propiedad moneda.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoneda() {
                return moneda;
            }

            /**
             * Define el valor de la propiedad moneda.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoneda(String value) {
                this.moneda = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteDisponible.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLimiteDisponible() {
                return limiteDisponible;
            }

            /**
             * Define el valor de la propiedad limiteDisponible.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLimiteDisponible(BigInteger value) {
                this.limiteDisponible = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLimiteCompra() {
                return limiteCompra;
            }

            /**
             * Define el valor de la propiedad limiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLimiteCompra(BigInteger value) {
                this.limiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCompraCuenta.
             * 
             * @return
             *     possible object is
             *     {@link Double }
             *     
             */
            public Double getLimiteCompraCuenta() {
                return limiteCompraCuenta;
            }

            /**
             * Define el valor de la propiedad limiteCompraCuenta.
             * 
             * @param value
             *     allowed object is
             *     {@link Double }
             *     
             */
            public void setLimiteCompraCuenta(Double value) {
                this.limiteCompraCuenta = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rubro"
        })
        public static class RubrosHabilitados {

            @XmlElement(required = true)
            protected List<String> rubro;

            /**
             * Gets the value of the rubro property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rubro property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRubro().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getRubro() {
                if (rubro == null) {
                    rubro = new ArrayList<String>();
                }
                return this.rubro;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="9"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rubro"
        })
        public static class RubrosInhabilitados {

            @XmlElement(required = true)
            protected List<String> rubro;

            /**
             * Gets the value of the rubro property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rubro property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRubro().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getRubro() {
                if (rubro == null) {
                    rubro = new ArrayList<String>();
                }
                return this.rubro;
            }

        }

    }

}
