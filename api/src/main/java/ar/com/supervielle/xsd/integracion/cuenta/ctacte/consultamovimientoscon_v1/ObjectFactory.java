
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientoscon_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientoscon_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientoscon_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType }
     * 
     */
    public DataConsultaMovRespType createDataConsultaMovRespType() {
        return new DataConsultaMovRespType();
    }

    /**
     * Create an instance of {@link DataConsultaMovReqType }
     * 
     */
    public DataConsultaMovReqType createDataConsultaMovReqType() {
        return new DataConsultaMovReqType();
    }

    /**
     * Create an instance of {@link ReqConsultaMovimientosCON }
     * 
     */
    public ReqConsultaMovimientosCON createReqConsultaMovimientosCON() {
        return new ReqConsultaMovimientosCON();
    }

    /**
     * Create an instance of {@link RespConsultaMovimientosCON }
     * 
     */
    public RespConsultaMovimientosCON createRespConsultaMovimientosCON() {
        return new RespConsultaMovimientosCON();
    }

    /**
     * Create an instance of {@link DataConsultaMovRespType.Row }
     * 
     */
    public DataConsultaMovRespType.Row createDataConsultaMovRespTypeRow() {
        return new DataConsultaMovRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaMovReqType.RangoFecha }
     * 
     */
    public DataConsultaMovReqType.RangoFecha createDataConsultaMovReqTypeRangoFecha() {
        return new DataConsultaMovReqType.RangoFecha();
    }

}
