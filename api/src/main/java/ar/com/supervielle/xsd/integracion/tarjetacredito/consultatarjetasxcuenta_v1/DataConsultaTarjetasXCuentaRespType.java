
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultatarjetasxcuenta_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaTarjetasXCuentaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaTarjetasXCuentaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Tipo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="tarjetaCorporativa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="tarjetaBoletinada" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="MotivoBoletin" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="DatosCliente">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                             &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="Sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="Nacionalidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="estadoCivil" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="controlLimiteCompraHabilitado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="LimitesControl" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="porcentajeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="importeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="controlLimiteCompraCuotasHabilitado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="LimitesControlCuotas" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="porcentajeCuotasLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="importeCuotasLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="FechaVigencia" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaTarjetasXCuentaRespType", propOrder = {
    "row"
})
public class DataConsultaTarjetasXCuentaRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaTarjetasXCuentaRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaTarjetasXCuentaRespType.Row }
     * 
     * 
     */
    public List<DataConsultaTarjetasXCuentaRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaTarjetasXCuentaRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
     *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Tipo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="tarjetaCorporativa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="tarjetaBoletinada" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="MotivoBoletin" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="DatosCliente">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="Sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="Nacionalidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="estadoCivil" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="controlLimiteCompraHabilitado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="LimitesControl" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="porcentajeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="importeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="controlLimiteCompraCuotasHabilitado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="LimitesControlCuotas" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="porcentajeCuotasLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="importeCuotasLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="FechaVigencia" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "denominacion",
        "tipo",
        "sucursal",
        "cartera",
        "titularidad",
        "tarjetaCorporativa",
        "tarjetaBoletinada",
        "motivoBoletin",
        "estado",
        "datosCliente",
        "fechaVencimiento",
        "controlLimiteCompraHabilitado",
        "limitesControl",
        "controlLimiteCompraCuotasHabilitado",
        "limitesControlCuotas",
        "fechaVigencia"
    })
    public static class Row {

        @XmlElement(name = "Identificador", required = true)
        protected IdTarjetaType identificador;
        @XmlElement(required = true)
        protected String denominacion;
        @XmlElement(name = "Tipo", required = true)
        protected DataConsultaTarjetasXCuentaRespType.Row.Tipo tipo;
        @XmlElement(name = "Sucursal", required = true)
        protected CodDescNumType sucursal;
        @XmlElement(name = "Cartera", required = true)
        protected CodDescNumType cartera;
        @XmlElement(name = "Titularidad", required = true)
        protected CodDescNumType titularidad;
        protected boolean tarjetaCorporativa;
        protected boolean tarjetaBoletinada;
        @XmlElement(name = "MotivoBoletin")
        protected CodDescNumType motivoBoletin;
        @XmlElement(name = "Estado", required = true)
        protected CodDescNumType estado;
        @XmlElement(name = "DatosCliente", required = true)
        protected DataConsultaTarjetasXCuentaRespType.Row.DatosCliente datosCliente;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaVencimiento;
        protected boolean controlLimiteCompraHabilitado;
        @XmlElement(name = "LimitesControl")
        protected DataConsultaTarjetasXCuentaRespType.Row.LimitesControl limitesControl;
        protected boolean controlLimiteCompraCuotasHabilitado;
        @XmlElement(name = "LimitesControlCuotas")
        protected DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas limitesControlCuotas;
        @XmlElement(name = "FechaVigencia")
        protected DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia fechaVigencia;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdTarjetaType }
         *     
         */
        public IdTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdTarjetaType }
         *     
         */
        public void setIdentificador(IdTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad denominacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDenominacion() {
            return denominacion;
        }

        /**
         * Define el valor de la propiedad denominacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDenominacion(String value) {
            this.denominacion = value;
        }

        /**
         * Obtiene el valor de la propiedad tipo.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.Tipo }
         *     
         */
        public DataConsultaTarjetasXCuentaRespType.Row.Tipo getTipo() {
            return tipo;
        }

        /**
         * Define el valor de la propiedad tipo.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.Tipo }
         *     
         */
        public void setTipo(DataConsultaTarjetasXCuentaRespType.Row.Tipo value) {
            this.tipo = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setSucursal(CodDescNumType value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad cartera.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getCartera() {
            return cartera;
        }

        /**
         * Define el valor de la propiedad cartera.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setCartera(CodDescNumType value) {
            this.cartera = value;
        }

        /**
         * Obtiene el valor de la propiedad titularidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getTitularidad() {
            return titularidad;
        }

        /**
         * Define el valor de la propiedad titularidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setTitularidad(CodDescNumType value) {
            this.titularidad = value;
        }

        /**
         * Obtiene el valor de la propiedad tarjetaCorporativa.
         * 
         */
        public boolean isTarjetaCorporativa() {
            return tarjetaCorporativa;
        }

        /**
         * Define el valor de la propiedad tarjetaCorporativa.
         * 
         */
        public void setTarjetaCorporativa(boolean value) {
            this.tarjetaCorporativa = value;
        }

        /**
         * Obtiene el valor de la propiedad tarjetaBoletinada.
         * 
         */
        public boolean isTarjetaBoletinada() {
            return tarjetaBoletinada;
        }

        /**
         * Define el valor de la propiedad tarjetaBoletinada.
         * 
         */
        public void setTarjetaBoletinada(boolean value) {
            this.tarjetaBoletinada = value;
        }

        /**
         * Obtiene el valor de la propiedad motivoBoletin.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getMotivoBoletin() {
            return motivoBoletin;
        }

        /**
         * Define el valor de la propiedad motivoBoletin.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setMotivoBoletin(CodDescNumType value) {
            this.motivoBoletin = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setEstado(CodDescNumType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad datosCliente.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.DatosCliente }
         *     
         */
        public DataConsultaTarjetasXCuentaRespType.Row.DatosCliente getDatosCliente() {
            return datosCliente;
        }

        /**
         * Define el valor de la propiedad datosCliente.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.DatosCliente }
         *     
         */
        public void setDatosCliente(DataConsultaTarjetasXCuentaRespType.Row.DatosCliente value) {
            this.datosCliente = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaVencimiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaVencimiento() {
            return fechaVencimiento;
        }

        /**
         * Define el valor de la propiedad fechaVencimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaVencimiento(XMLGregorianCalendar value) {
            this.fechaVencimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad controlLimiteCompraHabilitado.
         * 
         */
        public boolean isControlLimiteCompraHabilitado() {
            return controlLimiteCompraHabilitado;
        }

        /**
         * Define el valor de la propiedad controlLimiteCompraHabilitado.
         * 
         */
        public void setControlLimiteCompraHabilitado(boolean value) {
            this.controlLimiteCompraHabilitado = value;
        }

        /**
         * Obtiene el valor de la propiedad limitesControl.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.LimitesControl }
         *     
         */
        public DataConsultaTarjetasXCuentaRespType.Row.LimitesControl getLimitesControl() {
            return limitesControl;
        }

        /**
         * Define el valor de la propiedad limitesControl.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.LimitesControl }
         *     
         */
        public void setLimitesControl(DataConsultaTarjetasXCuentaRespType.Row.LimitesControl value) {
            this.limitesControl = value;
        }

        /**
         * Obtiene el valor de la propiedad controlLimiteCompraCuotasHabilitado.
         * 
         */
        public boolean isControlLimiteCompraCuotasHabilitado() {
            return controlLimiteCompraCuotasHabilitado;
        }

        /**
         * Define el valor de la propiedad controlLimiteCompraCuotasHabilitado.
         * 
         */
        public void setControlLimiteCompraCuotasHabilitado(boolean value) {
            this.controlLimiteCompraCuotasHabilitado = value;
        }

        /**
         * Obtiene el valor de la propiedad limitesControlCuotas.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas }
         *     
         */
        public DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas getLimitesControlCuotas() {
            return limitesControlCuotas;
        }

        /**
         * Define el valor de la propiedad limitesControlCuotas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas }
         *     
         */
        public void setLimitesControlCuotas(DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas value) {
            this.limitesControlCuotas = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaVigencia.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia }
         *     
         */
        public DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia getFechaVigencia() {
            return fechaVigencia;
        }

        /**
         * Define el valor de la propiedad fechaVigencia.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia }
         *     
         */
        public void setFechaVigencia(DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia value) {
            this.fechaVigencia = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="Sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="Nacionalidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="estadoCivil" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identificador",
            "fechaNacimiento",
            "sexo",
            "nacionalidad",
            "estadoCivil"
        })
        public static class DatosCliente {

            @XmlElement(required = true)
            protected IdClienteType identificador;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaNacimiento;
            @XmlElement(name = "Sexo", required = true)
            protected CodDescStringType sexo;
            @XmlElement(name = "Nacionalidad", required = true)
            protected CodDescStringType nacionalidad;
            @XmlElement(required = true)
            protected CodDescStringType estadoCivil;

            /**
             * Obtiene el valor de la propiedad identificador.
             * 
             * @return
             *     possible object is
             *     {@link IdClienteType }
             *     
             */
            public IdClienteType getIdentificador() {
                return identificador;
            }

            /**
             * Define el valor de la propiedad identificador.
             * 
             * @param value
             *     allowed object is
             *     {@link IdClienteType }
             *     
             */
            public void setIdentificador(IdClienteType value) {
                this.identificador = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaNacimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaNacimiento() {
                return fechaNacimiento;
            }

            /**
             * Define el valor de la propiedad fechaNacimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaNacimiento(XMLGregorianCalendar value) {
                this.fechaNacimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad sexo.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getSexo() {
                return sexo;
            }

            /**
             * Define el valor de la propiedad sexo.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setSexo(CodDescStringType value) {
                this.sexo = value;
            }

            /**
             * Obtiene el valor de la propiedad nacionalidad.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getNacionalidad() {
                return nacionalidad;
            }

            /**
             * Define el valor de la propiedad nacionalidad.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setNacionalidad(CodDescStringType value) {
                this.nacionalidad = value;
            }

            /**
             * Obtiene el valor de la propiedad estadoCivil.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getEstadoCivil() {
                return estadoCivil;
            }

            /**
             * Define el valor de la propiedad estadoCivil.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setEstadoCivil(CodDescStringType value) {
                this.estadoCivil = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="desde" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="hasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "desde",
            "hasta"
        })
        public static class FechaVigencia {

            @XmlElement(required = true)
            protected String desde;
            @XmlElement(required = true)
            protected String hasta;

            /**
             * Obtiene el valor de la propiedad desde.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDesde() {
                return desde;
            }

            /**
             * Define el valor de la propiedad desde.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDesde(String value) {
                this.desde = value;
            }

            /**
             * Obtiene el valor de la propiedad hasta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHasta() {
                return hasta;
            }

            /**
             * Define el valor de la propiedad hasta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHasta(String value) {
                this.hasta = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="porcentajeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="importeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "porcentajeLimiteCompra",
            "importeLimiteCompra"
        })
        public static class LimitesControl {

            @XmlElement(required = true)
            protected BigDecimal porcentajeLimiteCompra;
            @XmlElement(required = true)
            protected BigDecimal importeLimiteCompra;

            /**
             * Obtiene el valor de la propiedad porcentajeLimiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPorcentajeLimiteCompra() {
                return porcentajeLimiteCompra;
            }

            /**
             * Define el valor de la propiedad porcentajeLimiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPorcentajeLimiteCompra(BigDecimal value) {
                this.porcentajeLimiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad importeLimiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getImporteLimiteCompra() {
                return importeLimiteCompra;
            }

            /**
             * Define el valor de la propiedad importeLimiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setImporteLimiteCompra(BigDecimal value) {
                this.importeLimiteCompra = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="porcentajeCuotasLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="importeCuotasLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "porcentajeCuotasLimiteCompra",
            "importeCuotasLimiteCompra"
        })
        public static class LimitesControlCuotas {

            @XmlElement(required = true)
            protected BigDecimal porcentajeCuotasLimiteCompra;
            @XmlElement(required = true)
            protected BigDecimal importeCuotasLimiteCompra;

            /**
             * Obtiene el valor de la propiedad porcentajeCuotasLimiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPorcentajeCuotasLimiteCompra() {
                return porcentajeCuotasLimiteCompra;
            }

            /**
             * Define el valor de la propiedad porcentajeCuotasLimiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPorcentajeCuotasLimiteCompra(BigDecimal value) {
                this.porcentajeCuotasLimiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad importeCuotasLimiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getImporteCuotasLimiteCompra() {
                return importeCuotasLimiteCompra;
            }

            /**
             * Define el valor de la propiedad importeCuotasLimiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setImporteCuotasLimiteCompra(BigDecimal value) {
                this.importeCuotasLimiteCompra = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigo",
            "codigoMarca",
            "descripcion",
            "sigla"
        })
        public static class Tipo {

            @XmlElement(required = true)
            protected String codigo;
            @XmlElement(required = true)
            protected String codigoMarca;
            @XmlElement(required = true)
            protected String descripcion;
            @XmlElement(required = true)
            protected String sigla;

            /**
             * Obtiene el valor de la propiedad codigo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigo() {
                return codigo;
            }

            /**
             * Define el valor de la propiedad codigo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigo(String value) {
                this.codigo = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoMarca.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoMarca() {
                return codigoMarca;
            }

            /**
             * Define el valor de la propiedad codigoMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoMarca(String value) {
                this.codigoMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Define el valor de la propiedad descripcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

            /**
             * Obtiene el valor de la propiedad sigla.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSigla() {
                return sigla;
            }

            /**
             * Define el valor de la propiedad sigla.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSigla(String value) {
                this.sigla = value;
            }

        }

    }

}
