
package ar.com.supervielle.xsd.integracion.cliente.consultaextensionpersona_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultaextensionpersona_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultaextensionpersona_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaExtPerRespType }
     * 
     */
    public DataConsultaExtPerRespType createDataConsultaExtPerRespType() {
        return new DataConsultaExtPerRespType();
    }

    /**
     * Create an instance of {@link DataConsultaExtPerRespType.Row }
     * 
     */
    public DataConsultaExtPerRespType.Row createDataConsultaExtPerRespTypeRow() {
        return new DataConsultaExtPerRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaExtPerRespType.Row.EstyFormacion }
     * 
     */
    public DataConsultaExtPerRespType.Row.EstyFormacion createDataConsultaExtPerRespTypeRowEstyFormacion() {
        return new DataConsultaExtPerRespType.Row.EstyFormacion();
    }

    /**
     * Create an instance of {@link RespConsultaExtPer }
     * 
     */
    public RespConsultaExtPer createRespConsultaExtPer() {
        return new RespConsultaExtPer();
    }

    /**
     * Create an instance of {@link ReqConsultaExtPer }
     * 
     */
    public ReqConsultaExtPer createReqConsultaExtPer() {
        return new ReqConsultaExtPer();
    }

    /**
     * Create an instance of {@link DataConsultaExtPerReqType }
     * 
     */
    public DataConsultaExtPerReqType createDataConsultaExtPerReqType() {
        return new DataConsultaExtPerReqType();
    }

    /**
     * Create an instance of {@link DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral }
     * 
     */
    public DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral createDataConsultaExtPerRespTypeRowEstyFormacionActLaboral() {
        return new DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral();
    }

}
