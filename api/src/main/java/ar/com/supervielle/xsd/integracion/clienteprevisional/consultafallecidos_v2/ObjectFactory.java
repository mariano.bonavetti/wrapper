
package ar.com.supervielle.xsd.integracion.clienteprevisional.consultafallecidos_v2;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.clienteprevisional.consultafallecidos_v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.clienteprevisional.consultafallecidos_v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaFallecidosRespType }
     * 
     */
    public DataConsultaFallecidosRespType createDataConsultaFallecidosRespType() {
        return new DataConsultaFallecidosRespType();
    }

    /**
     * Create an instance of {@link RespConsultaFallecidos }
     * 
     */
    public RespConsultaFallecidos createRespConsultaFallecidos() {
        return new RespConsultaFallecidos();
    }

    /**
     * Create an instance of {@link ReqConsultaFallecidos }
     * 
     */
    public ReqConsultaFallecidos createReqConsultaFallecidos() {
        return new ReqConsultaFallecidos();
    }

    /**
     * Create an instance of {@link DataConsultaFallecidosReqType }
     * 
     */
    public DataConsultaFallecidosReqType createDataConsultaFallecidosReqType() {
        return new DataConsultaFallecidosReqType();
    }

    /**
     * Create an instance of {@link DataConsultaFallecidosRespType.Row }
     * 
     */
    public DataConsultaFallecidosRespType.Row createDataConsultaFallecidosRespTypeRow() {
        return new DataConsultaFallecidosRespType.Row();
    }

}
