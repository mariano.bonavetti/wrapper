
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para resumenTC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="resumenTC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="socio" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="numeroSocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="nombreApellidoSocio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="fechas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resumenAnterior" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="resumenActual" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="resumenProximo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="limites" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="limiteCompraEnCoutas" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteFinanciero" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteAdelanto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="saldos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="anterior" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="actual" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tasaTNA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tasaTEM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tarjetaActiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tipoTarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vencimientoPagoMinimo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resumenTC", propOrder = {
    "socio",
    "fechas",
    "producto",
    "pagoMinimoPesos",
    "limites",
    "saldos",
    "tasaTNA",
    "tasaTEM",
    "titular",
    "tarjetaActiva",
    "numeroCuenta",
    "tipoTarjeta",
    "vencimientoPagoMinimo"
})
public class ResumenTC {

    protected ResumenTC.Socio socio;
    protected ResumenTC.Fechas fechas;
    protected String producto;
    protected BigDecimal pagoMinimoPesos;
    protected ResumenTC.Limites limites;
    protected ResumenTC.Saldos saldos;
    protected BigDecimal tasaTNA;
    protected BigDecimal tasaTEM;
    protected String titular;
    protected String tarjetaActiva;
    protected Integer numeroCuenta;
    protected String tipoTarjeta;
    protected BigInteger vencimientoPagoMinimo;

    /**
     * Obtiene el valor de la propiedad socio.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTC.Socio }
     *     
     */
    public ResumenTC.Socio getSocio() {
        return socio;
    }

    /**
     * Define el valor de la propiedad socio.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTC.Socio }
     *     
     */
    public void setSocio(ResumenTC.Socio value) {
        this.socio = value;
    }

    /**
     * Obtiene el valor de la propiedad fechas.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTC.Fechas }
     *     
     */
    public ResumenTC.Fechas getFechas() {
        return fechas;
    }

    /**
     * Define el valor de la propiedad fechas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTC.Fechas }
     *     
     */
    public void setFechas(ResumenTC.Fechas value) {
        this.fechas = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducto(String value) {
        this.producto = value;
    }

    /**
     * Obtiene el valor de la propiedad pagoMinimoPesos.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPagoMinimoPesos() {
        return pagoMinimoPesos;
    }

    /**
     * Define el valor de la propiedad pagoMinimoPesos.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPagoMinimoPesos(BigDecimal value) {
        this.pagoMinimoPesos = value;
    }

    /**
     * Obtiene el valor de la propiedad limites.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTC.Limites }
     *     
     */
    public ResumenTC.Limites getLimites() {
        return limites;
    }

    /**
     * Define el valor de la propiedad limites.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTC.Limites }
     *     
     */
    public void setLimites(ResumenTC.Limites value) {
        this.limites = value;
    }

    /**
     * Obtiene el valor de la propiedad saldos.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTC.Saldos }
     *     
     */
    public ResumenTC.Saldos getSaldos() {
        return saldos;
    }

    /**
     * Define el valor de la propiedad saldos.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTC.Saldos }
     *     
     */
    public void setSaldos(ResumenTC.Saldos value) {
        this.saldos = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaTNA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTasaTNA() {
        return tasaTNA;
    }

    /**
     * Define el valor de la propiedad tasaTNA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTasaTNA(BigDecimal value) {
        this.tasaTNA = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaTEM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTasaTEM() {
        return tasaTEM;
    }

    /**
     * Define el valor de la propiedad tasaTEM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTasaTEM(BigDecimal value) {
        this.tasaTEM = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitular(String value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjetaActiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarjetaActiva() {
        return tarjetaActiva;
    }

    /**
     * Define el valor de la propiedad tarjetaActiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarjetaActiva(String value) {
        this.tarjetaActiva = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCuenta.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Define el valor de la propiedad numeroCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroCuenta(Integer value) {
        this.numeroCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    /**
     * Define el valor de la propiedad tipoTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTarjeta(String value) {
        this.tipoTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad vencimientoPagoMinimo.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVencimientoPagoMinimo() {
        return vencimientoPagoMinimo;
    }

    /**
     * Define el valor de la propiedad vencimientoPagoMinimo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVencimientoPagoMinimo(BigInteger value) {
        this.vencimientoPagoMinimo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resumenAnterior" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="resumenActual" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="resumenProximo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resumenAnterior",
        "resumenActual",
        "resumenProximo"
    })
    public static class Fechas {

        protected ResumenTC.Fechas.ResumenAnterior resumenAnterior;
        protected ResumenTC.Fechas.ResumenActual resumenActual;
        protected ResumenTC.Fechas.ResumenProximo resumenProximo;

        /**
         * Obtiene el valor de la propiedad resumenAnterior.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTC.Fechas.ResumenAnterior }
         *     
         */
        public ResumenTC.Fechas.ResumenAnterior getResumenAnterior() {
            return resumenAnterior;
        }

        /**
         * Define el valor de la propiedad resumenAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTC.Fechas.ResumenAnterior }
         *     
         */
        public void setResumenAnterior(ResumenTC.Fechas.ResumenAnterior value) {
            this.resumenAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad resumenActual.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTC.Fechas.ResumenActual }
         *     
         */
        public ResumenTC.Fechas.ResumenActual getResumenActual() {
            return resumenActual;
        }

        /**
         * Define el valor de la propiedad resumenActual.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTC.Fechas.ResumenActual }
         *     
         */
        public void setResumenActual(ResumenTC.Fechas.ResumenActual value) {
            this.resumenActual = value;
        }

        /**
         * Obtiene el valor de la propiedad resumenProximo.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTC.Fechas.ResumenProximo }
         *     
         */
        public ResumenTC.Fechas.ResumenProximo getResumenProximo() {
            return resumenProximo;
        }

        /**
         * Define el valor de la propiedad resumenProximo.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTC.Fechas.ResumenProximo }
         *     
         */
        public void setResumenProximo(ResumenTC.Fechas.ResumenProximo value) {
            this.resumenProximo = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cierre",
            "vencimiento"
        })
        public static class ResumenActual {

            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar cierre;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar vencimiento;

            /**
             * Obtiene el valor de la propiedad cierre.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCierre() {
                return cierre;
            }

            /**
             * Define el valor de la propiedad cierre.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCierre(XMLGregorianCalendar value) {
                this.cierre = value;
            }

            /**
             * Obtiene el valor de la propiedad vencimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getVencimiento() {
                return vencimiento;
            }

            /**
             * Define el valor de la propiedad vencimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setVencimiento(XMLGregorianCalendar value) {
                this.vencimiento = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cierre",
            "vencimiento"
        })
        public static class ResumenAnterior {

            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar cierre;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar vencimiento;

            /**
             * Obtiene el valor de la propiedad cierre.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCierre() {
                return cierre;
            }

            /**
             * Define el valor de la propiedad cierre.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCierre(XMLGregorianCalendar value) {
                this.cierre = value;
            }

            /**
             * Obtiene el valor de la propiedad vencimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getVencimiento() {
                return vencimiento;
            }

            /**
             * Define el valor de la propiedad vencimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setVencimiento(XMLGregorianCalendar value) {
                this.vencimiento = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cierre" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cierre",
            "vencimiento"
        })
        public static class ResumenProximo {

            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar cierre;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar vencimiento;

            /**
             * Obtiene el valor de la propiedad cierre.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCierre() {
                return cierre;
            }

            /**
             * Define el valor de la propiedad cierre.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCierre(XMLGregorianCalendar value) {
                this.cierre = value;
            }

            /**
             * Obtiene el valor de la propiedad vencimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getVencimiento() {
                return vencimiento;
            }

            /**
             * Define el valor de la propiedad vencimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setVencimiento(XMLGregorianCalendar value) {
                this.vencimiento = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="limiteCompraEnCoutas" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteFinanciero" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteAdelanto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limiteCompraEnCoutas",
        "limiteFinanciero",
        "limiteAdelanto"
    })
    public static class Limites {

        protected BigDecimal limiteCompraEnCoutas;
        protected BigDecimal limiteFinanciero;
        protected BigDecimal limiteAdelanto;

        /**
         * Obtiene el valor de la propiedad limiteCompraEnCoutas.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompraEnCoutas() {
            return limiteCompraEnCoutas;
        }

        /**
         * Define el valor de la propiedad limiteCompraEnCoutas.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompraEnCoutas(BigDecimal value) {
            this.limiteCompraEnCoutas = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteFinanciero.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteFinanciero() {
            return limiteFinanciero;
        }

        /**
         * Define el valor de la propiedad limiteFinanciero.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteFinanciero(BigDecimal value) {
            this.limiteFinanciero = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteAdelanto.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteAdelanto() {
            return limiteAdelanto;
        }

        /**
         * Define el valor de la propiedad limiteAdelanto.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteAdelanto(BigDecimal value) {
            this.limiteAdelanto = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="anterior" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="actual" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "anterior",
        "actual"
    })
    public static class Saldos {

        protected ResumenTC.Saldos.Anterior anterior;
        protected ResumenTC.Saldos.Actual actual;

        /**
         * Obtiene el valor de la propiedad anterior.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTC.Saldos.Anterior }
         *     
         */
        public ResumenTC.Saldos.Anterior getAnterior() {
            return anterior;
        }

        /**
         * Define el valor de la propiedad anterior.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTC.Saldos.Anterior }
         *     
         */
        public void setAnterior(ResumenTC.Saldos.Anterior value) {
            this.anterior = value;
        }

        /**
         * Obtiene el valor de la propiedad actual.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTC.Saldos.Actual }
         *     
         */
        public ResumenTC.Saldos.Actual getActual() {
            return actual;
        }

        /**
         * Define el valor de la propiedad actual.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTC.Saldos.Actual }
         *     
         */
        public void setActual(ResumenTC.Saldos.Actual value) {
            this.actual = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pesos",
            "dolares"
        })
        public static class Actual {

            protected BigDecimal pesos;
            protected BigDecimal dolares;

            /**
             * Obtiene el valor de la propiedad pesos.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPesos() {
                return pesos;
            }

            /**
             * Define el valor de la propiedad pesos.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPesos(BigDecimal value) {
                this.pesos = value;
            }

            /**
             * Obtiene el valor de la propiedad dolares.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getDolares() {
                return dolares;
            }

            /**
             * Define el valor de la propiedad dolares.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setDolares(BigDecimal value) {
                this.dolares = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pesos",
            "dolares"
        })
        public static class Anterior {

            protected BigDecimal pesos;
            protected BigDecimal dolares;

            /**
             * Obtiene el valor de la propiedad pesos.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getPesos() {
                return pesos;
            }

            /**
             * Define el valor de la propiedad pesos.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setPesos(BigDecimal value) {
                this.pesos = value;
            }

            /**
             * Obtiene el valor de la propiedad dolares.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getDolares() {
                return dolares;
            }

            /**
             * Define el valor de la propiedad dolares.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setDolares(BigDecimal value) {
                this.dolares = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="numeroSocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="nombreApellidoSocio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "numeroSocio",
        "nombreApellidoSocio"
    })
    public static class Socio {

        protected String numeroSocio;
        @XmlElement(required = true)
        protected String nombreApellidoSocio;

        /**
         * Obtiene el valor de la propiedad numeroSocio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroSocio() {
            return numeroSocio;
        }

        /**
         * Define el valor de la propiedad numeroSocio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroSocio(String value) {
            this.numeroSocio = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreApellidoSocio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreApellidoSocio() {
            return nombreApellidoSocio;
        }

        /**
         * Define el valor de la propiedad nombreApellidoSocio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreApellidoSocio(String value) {
            this.nombreApellidoSocio = value;
        }

    }

}
