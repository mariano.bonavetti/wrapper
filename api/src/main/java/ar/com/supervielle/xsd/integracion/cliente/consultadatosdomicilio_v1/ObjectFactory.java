
package ar.com.supervielle.xsd.integracion.cliente.consultadatosdomicilio_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultadatosdomicilio_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultadatosdomicilio_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType2 }
     * 
     */
    public DataConsultaDatosDomicilioRespType2 createDataConsultaDatosDomicilioRespType2() {
        return new DataConsultaDatosDomicilioRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType2 .Row }
     * 
     */
    public DataConsultaDatosDomicilioRespType2 .Row createDataConsultaDatosDomicilioRespType2Row() {
        return new DataConsultaDatosDomicilioRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType2 .Row.Telefonos }
     * 
     */
    public DataConsultaDatosDomicilioRespType2 .Row.Telefonos createDataConsultaDatosDomicilioRespType2RowTelefonos() {
        return new DataConsultaDatosDomicilioRespType2 .Row.Telefonos();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType }
     * 
     */
    public DataConsultaDatosDomicilioRespType createDataConsultaDatosDomicilioRespType() {
        return new DataConsultaDatosDomicilioRespType();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType.Row }
     * 
     */
    public DataConsultaDatosDomicilioRespType.Row createDataConsultaDatosDomicilioRespTypeRow() {
        return new DataConsultaDatosDomicilioRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType.Row.Telefonos }
     * 
     */
    public DataConsultaDatosDomicilioRespType.Row.Telefonos createDataConsultaDatosDomicilioRespTypeRowTelefonos() {
        return new DataConsultaDatosDomicilioRespType.Row.Telefonos();
    }

    /**
     * Create an instance of {@link RespConsultaDatosDomicilio }
     * 
     */
    public RespConsultaDatosDomicilio createRespConsultaDatosDomicilio() {
        return new RespConsultaDatosDomicilio();
    }

    /**
     * Create an instance of {@link ReqConsultaDatosDomicilio }
     * 
     */
    public ReqConsultaDatosDomicilio createReqConsultaDatosDomicilio() {
        return new ReqConsultaDatosDomicilio();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioReqType }
     * 
     */
    public DataConsultaDatosDomicilioReqType createDataConsultaDatosDomicilioReqType() {
        return new DataConsultaDatosDomicilioReqType();
    }

    /**
     * Create an instance of {@link RespConsultaDatosDomicilio2 }
     * 
     */
    public RespConsultaDatosDomicilio2 createRespConsultaDatosDomicilio2() {
        return new RespConsultaDatosDomicilio2();
    }

    /**
     * Create an instance of {@link ReqConsultaDatosDomicilio2 }
     * 
     */
    public ReqConsultaDatosDomicilio2 createReqConsultaDatosDomicilio2() {
        return new ReqConsultaDatosDomicilio2();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioReqType2 }
     * 
     */
    public DataConsultaDatosDomicilioReqType2 createDataConsultaDatosDomicilioReqType2() {
        return new DataConsultaDatosDomicilioReqType2();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono }
     * 
     */
    public DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono createDataConsultaDatosDomicilioRespType2RowTelefonosTelefono() {
        return new DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono();
    }

    /**
     * Create an instance of {@link DataConsultaDatosDomicilioRespType.Row.Telefonos.Telefono }
     * 
     */
    public DataConsultaDatosDomicilioRespType.Row.Telefonos.Telefono createDataConsultaDatosDomicilioRespTypeRowTelefonosTelefono() {
        return new DataConsultaDatosDomicilioRespType.Row.Telefonos.Telefono();
    }

}
