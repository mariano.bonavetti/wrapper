
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para resumenTC_v1.1 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="resumenTC_v1.1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="datosCuenta">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="titularCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaCierre">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fechaVencimiento">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="limites">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="compraEnCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="financiero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="adelanto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="saldos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="anterior" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="actual" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="tasas" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TNA" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="TEM" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="pagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="detalleMovimientos">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="movimiento" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fechaMovimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="numeroComprobante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="descripcionMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cuotas">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="importe" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="importeOrigen" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resumenTC_v1.1", propOrder = {
    "datosCuenta",
    "detalleMovimientos"
})
public class ResumenTCV11 {

    @XmlElement(required = true)
    protected ResumenTCV11 .DatosCuenta datosCuenta;
    @XmlElement(required = true)
    protected ResumenTCV11 .DetalleMovimientos detalleMovimientos;

    /**
     * Obtiene el valor de la propiedad datosCuenta.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTCV11 .DatosCuenta }
     *     
     */
    public ResumenTCV11 .DatosCuenta getDatosCuenta() {
        return datosCuenta;
    }

    /**
     * Define el valor de la propiedad datosCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTCV11 .DatosCuenta }
     *     
     */
    public void setDatosCuenta(ResumenTCV11 .DatosCuenta value) {
        this.datosCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleMovimientos.
     * 
     * @return
     *     possible object is
     *     {@link ResumenTCV11 .DetalleMovimientos }
     *     
     */
    public ResumenTCV11 .DetalleMovimientos getDetalleMovimientos() {
        return detalleMovimientos;
    }

    /**
     * Define el valor de la propiedad detalleMovimientos.
     * 
     * @param value
     *     allowed object is
     *     {@link ResumenTCV11 .DetalleMovimientos }
     *     
     */
    public void setDetalleMovimientos(ResumenTCV11 .DetalleMovimientos value) {
        this.detalleMovimientos = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="titularCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaCierre">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fechaVencimiento">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="limites">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="compraEnCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="financiero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="adelanto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="saldos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="anterior" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="actual" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tasas" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TNA" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="TEM" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="pagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "numeroCuenta",
        "titularCuenta",
        "fechaCierre",
        "fechaVencimiento",
        "limites",
        "saldos",
        "tasas",
        "pagoMinimoPesos"
    })
    public static class DatosCuenta {

        protected int numeroCuenta;
        @XmlElement(required = true)
        protected String titularCuenta;
        @XmlElement(required = true)
        protected ResumenTCV11 .DatosCuenta.FechaCierre fechaCierre;
        @XmlElement(required = true)
        protected ResumenTCV11 .DatosCuenta.FechaVencimiento fechaVencimiento;
        @XmlElement(required = true)
        protected ResumenTCV11 .DatosCuenta.Limites limites;
        protected ResumenTCV11 .DatosCuenta.Saldos saldos;
        protected ResumenTCV11 .DatosCuenta.Tasas tasas;
        @XmlElement(required = true)
        protected BigDecimal pagoMinimoPesos;

        /**
         * Obtiene el valor de la propiedad numeroCuenta.
         * 
         */
        public int getNumeroCuenta() {
            return numeroCuenta;
        }

        /**
         * Define el valor de la propiedad numeroCuenta.
         * 
         */
        public void setNumeroCuenta(int value) {
            this.numeroCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad titularCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitularCuenta() {
            return titularCuenta;
        }

        /**
         * Define el valor de la propiedad titularCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitularCuenta(String value) {
            this.titularCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCierre.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTCV11 .DatosCuenta.FechaCierre }
         *     
         */
        public ResumenTCV11 .DatosCuenta.FechaCierre getFechaCierre() {
            return fechaCierre;
        }

        /**
         * Define el valor de la propiedad fechaCierre.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTCV11 .DatosCuenta.FechaCierre }
         *     
         */
        public void setFechaCierre(ResumenTCV11 .DatosCuenta.FechaCierre value) {
            this.fechaCierre = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaVencimiento.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTCV11 .DatosCuenta.FechaVencimiento }
         *     
         */
        public ResumenTCV11 .DatosCuenta.FechaVencimiento getFechaVencimiento() {
            return fechaVencimiento;
        }

        /**
         * Define el valor de la propiedad fechaVencimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTCV11 .DatosCuenta.FechaVencimiento }
         *     
         */
        public void setFechaVencimiento(ResumenTCV11 .DatosCuenta.FechaVencimiento value) {
            this.fechaVencimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad limites.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTCV11 .DatosCuenta.Limites }
         *     
         */
        public ResumenTCV11 .DatosCuenta.Limites getLimites() {
            return limites;
        }

        /**
         * Define el valor de la propiedad limites.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTCV11 .DatosCuenta.Limites }
         *     
         */
        public void setLimites(ResumenTCV11 .DatosCuenta.Limites value) {
            this.limites = value;
        }

        /**
         * Obtiene el valor de la propiedad saldos.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTCV11 .DatosCuenta.Saldos }
         *     
         */
        public ResumenTCV11 .DatosCuenta.Saldos getSaldos() {
            return saldos;
        }

        /**
         * Define el valor de la propiedad saldos.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTCV11 .DatosCuenta.Saldos }
         *     
         */
        public void setSaldos(ResumenTCV11 .DatosCuenta.Saldos value) {
            this.saldos = value;
        }

        /**
         * Obtiene el valor de la propiedad tasas.
         * 
         * @return
         *     possible object is
         *     {@link ResumenTCV11 .DatosCuenta.Tasas }
         *     
         */
        public ResumenTCV11 .DatosCuenta.Tasas getTasas() {
            return tasas;
        }

        /**
         * Define el valor de la propiedad tasas.
         * 
         * @param value
         *     allowed object is
         *     {@link ResumenTCV11 .DatosCuenta.Tasas }
         *     
         */
        public void setTasas(ResumenTCV11 .DatosCuenta.Tasas value) {
            this.tasas = value;
        }

        /**
         * Obtiene el valor de la propiedad pagoMinimoPesos.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPagoMinimoPesos() {
            return pagoMinimoPesos;
        }

        /**
         * Define el valor de la propiedad pagoMinimoPesos.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPagoMinimoPesos(BigDecimal value) {
            this.pagoMinimoPesos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actual",
            "anterior",
            "proximo"
        })
        public static class FechaCierre {

            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar actual;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar anterior;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar proximo;

            /**
             * Obtiene el valor de la propiedad actual.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getActual() {
                return actual;
            }

            /**
             * Define el valor de la propiedad actual.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setActual(XMLGregorianCalendar value) {
                this.actual = value;
            }

            /**
             * Obtiene el valor de la propiedad anterior.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getAnterior() {
                return anterior;
            }

            /**
             * Define el valor de la propiedad anterior.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setAnterior(XMLGregorianCalendar value) {
                this.anterior = value;
            }

            /**
             * Obtiene el valor de la propiedad proximo.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getProximo() {
                return proximo;
            }

            /**
             * Define el valor de la propiedad proximo.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setProximo(XMLGregorianCalendar value) {
                this.proximo = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="actual" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="anterior" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="proximo" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actual",
            "anterior",
            "proximo"
        })
        public static class FechaVencimiento {

            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar actual;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar anterior;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar proximo;

            /**
             * Obtiene el valor de la propiedad actual.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getActual() {
                return actual;
            }

            /**
             * Define el valor de la propiedad actual.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setActual(XMLGregorianCalendar value) {
                this.actual = value;
            }

            /**
             * Obtiene el valor de la propiedad anterior.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getAnterior() {
                return anterior;
            }

            /**
             * Define el valor de la propiedad anterior.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setAnterior(XMLGregorianCalendar value) {
                this.anterior = value;
            }

            /**
             * Obtiene el valor de la propiedad proximo.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getProximo() {
                return proximo;
            }

            /**
             * Define el valor de la propiedad proximo.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setProximo(XMLGregorianCalendar value) {
                this.proximo = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="compraEnCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="financiero" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="adelanto" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "compraEnCuotas",
            "financiero",
            "adelanto"
        })
        public static class Limites {

            @XmlElement(required = true)
            protected BigDecimal compraEnCuotas;
            @XmlElement(required = true)
            protected BigDecimal financiero;
            @XmlElement(required = true)
            protected BigDecimal adelanto;

            /**
             * Obtiene el valor de la propiedad compraEnCuotas.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCompraEnCuotas() {
                return compraEnCuotas;
            }

            /**
             * Define el valor de la propiedad compraEnCuotas.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCompraEnCuotas(BigDecimal value) {
                this.compraEnCuotas = value;
            }

            /**
             * Obtiene el valor de la propiedad financiero.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getFinanciero() {
                return financiero;
            }

            /**
             * Define el valor de la propiedad financiero.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setFinanciero(BigDecimal value) {
                this.financiero = value;
            }

            /**
             * Obtiene el valor de la propiedad adelanto.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getAdelanto() {
                return adelanto;
            }

            /**
             * Define el valor de la propiedad adelanto.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setAdelanto(BigDecimal value) {
                this.adelanto = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="anterior" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="actual" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "anterior",
            "actual"
        })
        public static class Saldos {

            protected ResumenTCV11 .DatosCuenta.Saldos.Anterior anterior;
            protected ResumenTCV11 .DatosCuenta.Saldos.Actual actual;

            /**
             * Obtiene el valor de la propiedad anterior.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DatosCuenta.Saldos.Anterior }
             *     
             */
            public ResumenTCV11 .DatosCuenta.Saldos.Anterior getAnterior() {
                return anterior;
            }

            /**
             * Define el valor de la propiedad anterior.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DatosCuenta.Saldos.Anterior }
             *     
             */
            public void setAnterior(ResumenTCV11 .DatosCuenta.Saldos.Anterior value) {
                this.anterior = value;
            }

            /**
             * Obtiene el valor de la propiedad actual.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DatosCuenta.Saldos.Actual }
             *     
             */
            public ResumenTCV11 .DatosCuenta.Saldos.Actual getActual() {
                return actual;
            }

            /**
             * Define el valor de la propiedad actual.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DatosCuenta.Saldos.Actual }
             *     
             */
            public void setActual(ResumenTCV11 .DatosCuenta.Saldos.Actual value) {
                this.actual = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pesos",
                "dolares"
            })
            public static class Actual {

                protected BigDecimal pesos;
                protected BigDecimal dolares;

                /**
                 * Obtiene el valor de la propiedad pesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPesos() {
                    return pesos;
                }

                /**
                 * Define el valor de la propiedad pesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPesos(BigDecimal value) {
                    this.pesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad dolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDolares() {
                    return dolares;
                }

                /**
                 * Define el valor de la propiedad dolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDolares(BigDecimal value) {
                    this.dolares = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pesos",
                "dolares"
            })
            public static class Anterior {

                protected BigDecimal pesos;
                protected BigDecimal dolares;

                /**
                 * Obtiene el valor de la propiedad pesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPesos() {
                    return pesos;
                }

                /**
                 * Define el valor de la propiedad pesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPesos(BigDecimal value) {
                    this.pesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad dolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDolares() {
                    return dolares;
                }

                /**
                 * Define el valor de la propiedad dolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDolares(BigDecimal value) {
                    this.dolares = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TNA" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="TEM" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tna",
            "tem"
        })
        public static class Tasas {

            @XmlElement(name = "TNA")
            protected ResumenTCV11 .DatosCuenta.Tasas.TNA tna;
            @XmlElement(name = "TEM")
            protected ResumenTCV11 .DatosCuenta.Tasas.TEM tem;

            /**
             * Obtiene el valor de la propiedad tna.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DatosCuenta.Tasas.TNA }
             *     
             */
            public ResumenTCV11 .DatosCuenta.Tasas.TNA getTNA() {
                return tna;
            }

            /**
             * Define el valor de la propiedad tna.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DatosCuenta.Tasas.TNA }
             *     
             */
            public void setTNA(ResumenTCV11 .DatosCuenta.Tasas.TNA value) {
                this.tna = value;
            }

            /**
             * Obtiene el valor de la propiedad tem.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DatosCuenta.Tasas.TEM }
             *     
             */
            public ResumenTCV11 .DatosCuenta.Tasas.TEM getTEM() {
                return tem;
            }

            /**
             * Define el valor de la propiedad tem.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DatosCuenta.Tasas.TEM }
             *     
             */
            public void setTEM(ResumenTCV11 .DatosCuenta.Tasas.TEM value) {
                this.tem = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pesos",
                "dolares"
            })
            public static class TEM {

                @XmlElement(required = true)
                protected BigDecimal pesos;
                @XmlElement(required = true)
                protected BigDecimal dolares;

                /**
                 * Obtiene el valor de la propiedad pesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPesos() {
                    return pesos;
                }

                /**
                 * Define el valor de la propiedad pesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPesos(BigDecimal value) {
                    this.pesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad dolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDolares() {
                    return dolares;
                }

                /**
                 * Define el valor de la propiedad dolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDolares(BigDecimal value) {
                    this.dolares = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pesos",
                "dolares"
            })
            public static class TNA {

                @XmlElement(required = true)
                protected BigDecimal pesos;
                @XmlElement(required = true)
                protected BigDecimal dolares;

                /**
                 * Obtiene el valor de la propiedad pesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPesos() {
                    return pesos;
                }

                /**
                 * Define el valor de la propiedad pesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPesos(BigDecimal value) {
                    this.pesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad dolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDolares() {
                    return dolares;
                }

                /**
                 * Define el valor de la propiedad dolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDolares(BigDecimal value) {
                    this.dolares = value;
                }

            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="movimiento" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fechaMovimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="numeroComprobante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="descripcionMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cuotas">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="importe" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="importeOrigen" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "movimiento"
    })
    public static class DetalleMovimientos {

        protected List<ResumenTCV11 .DetalleMovimientos.Movimiento> movimiento;

        /**
         * Gets the value of the movimiento property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the movimiento property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMovimiento().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ResumenTCV11 .DetalleMovimientos.Movimiento }
         * 
         * 
         */
        public List<ResumenTCV11 .DetalleMovimientos.Movimiento> getMovimiento() {
            if (movimiento == null) {
                movimiento = new ArrayList<ResumenTCV11 .DetalleMovimientos.Movimiento>();
            }
            return this.movimiento;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fechaMovimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="numeroComprobante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="descripcionMovimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cuotas">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="importe" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="importeOrigen" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "numeroTarjeta",
            "denominacion",
            "titularidad",
            "fechaMovimiento",
            "numeroComprobante",
            "descripcionMovimiento",
            "cuotas",
            "importe",
            "importeOrigen"
        })
        public static class Movimiento {

            @XmlElement(required = true)
            protected String numeroTarjeta;
            @XmlElement(required = true)
            protected String denominacion;
            @XmlElement(required = true)
            protected String titularidad;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaMovimiento;
            protected String numeroComprobante;
            @XmlElement(required = true)
            protected String descripcionMovimiento;
            @XmlElement(required = true)
            protected ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas cuotas;
            protected ResumenTCV11 .DetalleMovimientos.Movimiento.Importe importe;
            protected ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen importeOrigen;

            /**
             * Obtiene el valor de la propiedad numeroTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumeroTarjeta() {
                return numeroTarjeta;
            }

            /**
             * Define el valor de la propiedad numeroTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumeroTarjeta(String value) {
                this.numeroTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad denominacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDenominacion() {
                return denominacion;
            }

            /**
             * Define el valor de la propiedad denominacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDenominacion(String value) {
                this.denominacion = value;
            }

            /**
             * Obtiene el valor de la propiedad titularidad.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitularidad() {
                return titularidad;
            }

            /**
             * Define el valor de la propiedad titularidad.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitularidad(String value) {
                this.titularidad = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaMovimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaMovimiento() {
                return fechaMovimiento;
            }

            /**
             * Define el valor de la propiedad fechaMovimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaMovimiento(XMLGregorianCalendar value) {
                this.fechaMovimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad numeroComprobante.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumeroComprobante() {
                return numeroComprobante;
            }

            /**
             * Define el valor de la propiedad numeroComprobante.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumeroComprobante(String value) {
                this.numeroComprobante = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcionMovimiento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcionMovimiento() {
                return descripcionMovimiento;
            }

            /**
             * Define el valor de la propiedad descripcionMovimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcionMovimiento(String value) {
                this.descripcionMovimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad cuotas.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas }
             *     
             */
            public ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas getCuotas() {
                return cuotas;
            }

            /**
             * Define el valor de la propiedad cuotas.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas }
             *     
             */
            public void setCuotas(ResumenTCV11 .DetalleMovimientos.Movimiento.Cuotas value) {
                this.cuotas = value;
            }

            /**
             * Obtiene el valor de la propiedad importe.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DetalleMovimientos.Movimiento.Importe }
             *     
             */
            public ResumenTCV11 .DetalleMovimientos.Movimiento.Importe getImporte() {
                return importe;
            }

            /**
             * Define el valor de la propiedad importe.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DetalleMovimientos.Movimiento.Importe }
             *     
             */
            public void setImporte(ResumenTCV11 .DetalleMovimientos.Movimiento.Importe value) {
                this.importe = value;
            }

            /**
             * Obtiene el valor de la propiedad importeOrigen.
             * 
             * @return
             *     possible object is
             *     {@link ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen }
             *     
             */
            public ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen getImporteOrigen() {
                return importeOrigen;
            }

            /**
             * Define el valor de la propiedad importeOrigen.
             * 
             * @param value
             *     allowed object is
             *     {@link ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen }
             *     
             */
            public void setImporteOrigen(ResumenTCV11 .DetalleMovimientos.Movimiento.ImporteOrigen value) {
                this.importeOrigen = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "cantidad",
                "numeroCuota"
            })
            public static class Cuotas {

                @XmlElement(required = true)
                protected String cantidad;
                @XmlElement(required = true)
                protected String numeroCuota;

                /**
                 * Obtiene el valor de la propiedad cantidad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCantidad() {
                    return cantidad;
                }

                /**
                 * Define el valor de la propiedad cantidad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCantidad(String value) {
                    this.cantidad = value;
                }

                /**
                 * Obtiene el valor de la propiedad numeroCuota.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumeroCuota() {
                    return numeroCuota;
                }

                /**
                 * Define el valor de la propiedad numeroCuota.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumeroCuota(String value) {
                    this.numeroCuota = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="pesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="dolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "pesos",
                "dolares"
            })
            public static class Importe {

                protected BigDecimal pesos;
                protected BigDecimal dolares;

                /**
                 * Obtiene el valor de la propiedad pesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getPesos() {
                    return pesos;
                }

                /**
                 * Define el valor de la propiedad pesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setPesos(BigDecimal value) {
                    this.pesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad dolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDolares() {
                    return dolares;
                }

                /**
                 * Define el valor de la propiedad dolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDolares(BigDecimal value) {
                    this.dolares = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "moneda",
                "importe"
            })
            public static class ImporteOrigen {

                @XmlElement(required = true)
                protected String moneda;
                @XmlElement(required = true)
                protected BigDecimal importe;

                /**
                 * Obtiene el valor de la propiedad moneda.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMoneda() {
                    return moneda;
                }

                /**
                 * Define el valor de la propiedad moneda.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMoneda(String value) {
                    this.moneda = value;
                }

                /**
                 * Obtiene el valor de la propiedad importe.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getImporte() {
                    return importe;
                }

                /**
                 * Define el valor de la propiedad importe.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setImporte(BigDecimal value) {
                    this.importe = value;
                }

            }

        }

    }

}
