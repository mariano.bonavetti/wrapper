
package ar.com.supervielle.xsd.integracion.clienteprevisional.calculoingresos_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataCalculoIngresosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataCalculoIngresosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ingresosTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="beneficios">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="beneficio" maxOccurs="10">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="ingresosBeneficio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataCalculoIngresosRespType", propOrder = {
    "row"
})
public class DataCalculoIngresosRespType {

    @XmlElement(name = "Row")
    protected DataCalculoIngresosRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataCalculoIngresosRespType.Row }
     *     
     */
    public DataCalculoIngresosRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataCalculoIngresosRespType.Row }
     *     
     */
    public void setRow(DataCalculoIngresosRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ingresosTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="beneficios">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="beneficio" maxOccurs="10">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="ingresosBeneficio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ingresosTotal",
        "beneficios"
    })
    public static class Row {

        @XmlElement(required = true)
        protected BigDecimal ingresosTotal;
        @XmlElement(required = true)
        protected DataCalculoIngresosRespType.Row.Beneficios beneficios;

        /**
         * Obtiene el valor de la propiedad ingresosTotal.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIngresosTotal() {
            return ingresosTotal;
        }

        /**
         * Define el valor de la propiedad ingresosTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIngresosTotal(BigDecimal value) {
            this.ingresosTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad beneficios.
         * 
         * @return
         *     possible object is
         *     {@link DataCalculoIngresosRespType.Row.Beneficios }
         *     
         */
        public DataCalculoIngresosRespType.Row.Beneficios getBeneficios() {
            return beneficios;
        }

        /**
         * Define el valor de la propiedad beneficios.
         * 
         * @param value
         *     allowed object is
         *     {@link DataCalculoIngresosRespType.Row.Beneficios }
         *     
         */
        public void setBeneficios(DataCalculoIngresosRespType.Row.Beneficios value) {
            this.beneficios = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="beneficio" maxOccurs="10">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="ingresosBeneficio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "beneficio"
        })
        public static class Beneficios {

            @XmlElement(required = true)
            protected List<DataCalculoIngresosRespType.Row.Beneficios.Beneficio> beneficio;

            /**
             * Gets the value of the beneficio property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the beneficio property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBeneficio().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataCalculoIngresosRespType.Row.Beneficios.Beneficio }
             * 
             * 
             */
            public List<DataCalculoIngresosRespType.Row.Beneficios.Beneficio> getBeneficio() {
                if (beneficio == null) {
                    beneficio = new ArrayList<DataCalculoIngresosRespType.Row.Beneficios.Beneficio>();
                }
                return this.beneficio;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="ingresosBeneficio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nroBeneficioCliente",
                "ingresosBeneficio"
            })
            public static class Beneficio {

                @XmlElement(required = true)
                protected BigInteger nroBeneficioCliente;
                @XmlElement(required = true)
                protected BigDecimal ingresosBeneficio;

                /**
                 * Obtiene el valor de la propiedad nroBeneficioCliente.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNroBeneficioCliente() {
                    return nroBeneficioCliente;
                }

                /**
                 * Define el valor de la propiedad nroBeneficioCliente.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNroBeneficioCliente(BigInteger value) {
                    this.nroBeneficioCliente = value;
                }

                /**
                 * Obtiene el valor de la propiedad ingresosBeneficio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getIngresosBeneficio() {
                    return ingresosBeneficio;
                }

                /**
                 * Define el valor de la propiedad ingresosBeneficio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setIngresosBeneficio(BigDecimal value) {
                    this.ingresosBeneficio = value;
                }

            }

        }

    }

}
