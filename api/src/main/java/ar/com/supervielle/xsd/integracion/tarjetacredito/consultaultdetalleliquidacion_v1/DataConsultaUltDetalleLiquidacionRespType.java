
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaultdetalleliquidacion_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.OperacionTC;


/**
 * <p>Clase Java para DataConsultaUltDetalleLiquidacionRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaUltDetalleLiquidacionRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}operacionTC" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaUltDetalleLiquidacionRespType", propOrder = {
    "row"
})
public class DataConsultaUltDetalleLiquidacionRespType {

    @XmlElement(name = "Row")
    protected List<OperacionTC> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperacionTC }
     * 
     * 
     */
    public List<OperacionTC> getRow() {
        if (row == null) {
            row = new ArrayList<OperacionTC>();
        }
        return this.row;
    }

}
