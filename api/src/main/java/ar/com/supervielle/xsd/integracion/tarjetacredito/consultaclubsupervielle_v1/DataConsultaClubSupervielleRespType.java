
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaclubsupervielle_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaClubSupervielleRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaClubSupervielleRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="puntosAcumulados" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaClubSupervielleRespType", propOrder = {
    "row"
})
public class DataConsultaClubSupervielleRespType {

    @XmlElement(name = "Row")
    protected DataConsultaClubSupervielleRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaClubSupervielleRespType.Row }
     *     
     */
    public DataConsultaClubSupervielleRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaClubSupervielleRespType.Row }
     *     
     */
    public void setRow(DataConsultaClubSupervielleRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="puntosAcumulados" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "puntosAcumulados"
    })
    public static class Row {

        protected long puntosAcumulados;

        /**
         * Obtiene el valor de la propiedad puntosAcumulados.
         * 
         */
        public long getPuntosAcumulados() {
            return puntosAcumulados;
        }

        /**
         * Define el valor de la propiedad puntosAcumulados.
         * 
         */
        public void setPuntosAcumulados(long value) {
            this.puntosAcumulados = value;
        }

    }

}
