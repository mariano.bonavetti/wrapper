
package ar.com.supervielle.xsd.integracion.cliente.consultadatospersona_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultadatospersona_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultadatospersona_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaRespType2 }
     * 
     */
    public DataConsultaDatosPersonaRespType2 createDataConsultaDatosPersonaRespType2() {
        return new DataConsultaDatosPersonaRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaRespType2 .Row }
     * 
     */
    public DataConsultaDatosPersonaRespType2 .Row createDataConsultaDatosPersonaRespType2Row() {
        return new DataConsultaDatosPersonaRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaRespType }
     * 
     */
    public DataConsultaDatosPersonaRespType createDataConsultaDatosPersonaRespType() {
        return new DataConsultaDatosPersonaRespType();
    }

    /**
     * Create an instance of {@link RespConsultaDatosPersona }
     * 
     */
    public RespConsultaDatosPersona createRespConsultaDatosPersona() {
        return new RespConsultaDatosPersona();
    }

    /**
     * Create an instance of {@link ReqConsultaDatosPersona }
     * 
     */
    public ReqConsultaDatosPersona createReqConsultaDatosPersona() {
        return new ReqConsultaDatosPersona();
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaReqType }
     * 
     */
    public DataConsultaDatosPersonaReqType createDataConsultaDatosPersonaReqType() {
        return new DataConsultaDatosPersonaReqType();
    }

    /**
     * Create an instance of {@link RespConsultaDatosPersona2 }
     * 
     */
    public RespConsultaDatosPersona2 createRespConsultaDatosPersona2() {
        return new RespConsultaDatosPersona2();
    }

    /**
     * Create an instance of {@link ReqConsultaDatosPersona2 }
     * 
     */
    public ReqConsultaDatosPersona2 createReqConsultaDatosPersona2() {
        return new ReqConsultaDatosPersona2();
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaReqType2 }
     * 
     */
    public DataConsultaDatosPersonaReqType2 createDataConsultaDatosPersonaReqType2() {
        return new DataConsultaDatosPersonaReqType2();
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaRespType2 .Row.Emails }
     * 
     */
    public DataConsultaDatosPersonaRespType2 .Row.Emails createDataConsultaDatosPersonaRespType2RowEmails() {
        return new DataConsultaDatosPersonaRespType2 .Row.Emails();
    }

    /**
     * Create an instance of {@link DataConsultaDatosPersonaRespType.Row }
     * 
     */
    public DataConsultaDatosPersonaRespType.Row createDataConsultaDatosPersonaRespTypeRow() {
        return new DataConsultaDatosPersonaRespType.Row();
    }

}
