
package ar.com.supervielle.xsd.integracion.cliente.consultaextensionpersona_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaExtPerRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaExtPerRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="estyFormacion">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="actLaboral">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="actividad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                       &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="fechaIngreso" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="profesion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="nivelInstr" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nomPadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="apePadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nomMadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="apeMadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nomConyugue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="apeConyugue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaExtPerRespType", propOrder = {
    "row"
})
public class DataConsultaExtPerRespType {

    @XmlElement(name = "Row")
    protected DataConsultaExtPerRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaExtPerRespType.Row }
     *     
     */
    public DataConsultaExtPerRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaExtPerRespType.Row }
     *     
     */
    public void setRow(DataConsultaExtPerRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="estyFormacion">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="actLaboral">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="actividad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                             &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="fechaIngreso" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="profesion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="nivelInstr" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nomPadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="apePadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nomMadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="apeMadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nomConyugue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="apeConyugue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "estyFormacion",
        "nomPadre",
        "apePadre",
        "nomMadre",
        "apeMadre",
        "nomConyugue",
        "apeConyugue"
    })
    public static class Row {

        @XmlElement(required = true)
        protected DataConsultaExtPerRespType.Row.EstyFormacion estyFormacion;
        @XmlElement(required = true)
        protected String nomPadre;
        @XmlElement(required = true)
        protected String apePadre;
        @XmlElement(required = true)
        protected String nomMadre;
        @XmlElement(required = true)
        protected String apeMadre;
        @XmlElement(required = true)
        protected String nomConyugue;
        @XmlElement(required = true)
        protected String apeConyugue;

        /**
         * Obtiene el valor de la propiedad estyFormacion.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaExtPerRespType.Row.EstyFormacion }
         *     
         */
        public DataConsultaExtPerRespType.Row.EstyFormacion getEstyFormacion() {
            return estyFormacion;
        }

        /**
         * Define el valor de la propiedad estyFormacion.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaExtPerRespType.Row.EstyFormacion }
         *     
         */
        public void setEstyFormacion(DataConsultaExtPerRespType.Row.EstyFormacion value) {
            this.estyFormacion = value;
        }

        /**
         * Obtiene el valor de la propiedad nomPadre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNomPadre() {
            return nomPadre;
        }

        /**
         * Define el valor de la propiedad nomPadre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNomPadre(String value) {
            this.nomPadre = value;
        }

        /**
         * Obtiene el valor de la propiedad apePadre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApePadre() {
            return apePadre;
        }

        /**
         * Define el valor de la propiedad apePadre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApePadre(String value) {
            this.apePadre = value;
        }

        /**
         * Obtiene el valor de la propiedad nomMadre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNomMadre() {
            return nomMadre;
        }

        /**
         * Define el valor de la propiedad nomMadre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNomMadre(String value) {
            this.nomMadre = value;
        }

        /**
         * Obtiene el valor de la propiedad apeMadre.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApeMadre() {
            return apeMadre;
        }

        /**
         * Define el valor de la propiedad apeMadre.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApeMadre(String value) {
            this.apeMadre = value;
        }

        /**
         * Obtiene el valor de la propiedad nomConyugue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNomConyugue() {
            return nomConyugue;
        }

        /**
         * Define el valor de la propiedad nomConyugue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNomConyugue(String value) {
            this.nomConyugue = value;
        }

        /**
         * Obtiene el valor de la propiedad apeConyugue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApeConyugue() {
            return apeConyugue;
        }

        /**
         * Define el valor de la propiedad apeConyugue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApeConyugue(String value) {
            this.apeConyugue = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="actLaboral">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="actividad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                   &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="fechaIngreso" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="profesion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="nivelInstr" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actLaboral",
            "profesion",
            "nivelInstr"
        })
        public static class EstyFormacion {

            @XmlElement(required = true)
            protected DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral actLaboral;
            @XmlElement(required = true)
            protected CodDescStringType profesion;
            @XmlElement(required = true)
            protected CodDescStringType nivelInstr;

            /**
             * Obtiene el valor de la propiedad actLaboral.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral }
             *     
             */
            public DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral getActLaboral() {
                return actLaboral;
            }

            /**
             * Define el valor de la propiedad actLaboral.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral }
             *     
             */
            public void setActLaboral(DataConsultaExtPerRespType.Row.EstyFormacion.ActLaboral value) {
                this.actLaboral = value;
            }

            /**
             * Obtiene el valor de la propiedad profesion.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getProfesion() {
                return profesion;
            }

            /**
             * Define el valor de la propiedad profesion.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setProfesion(CodDescStringType value) {
                this.profesion = value;
            }

            /**
             * Obtiene el valor de la propiedad nivelInstr.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getNivelInstr() {
                return nivelInstr;
            }

            /**
             * Define el valor de la propiedad nivelInstr.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setNivelInstr(CodDescStringType value) {
                this.nivelInstr = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="actividad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *         &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="fechaIngreso" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "actividad",
                "fechaInicio",
                "fechaIngreso"
            })
            public static class ActLaboral {

                @XmlElement(required = true)
                protected CodDescStringType actividad;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaInicio;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaIngreso;

                /**
                 * Obtiene el valor de la propiedad actividad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getActividad() {
                    return actividad;
                }

                /**
                 * Define el valor de la propiedad actividad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setActividad(CodDescStringType value) {
                    this.actividad = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaInicio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaInicio() {
                    return fechaInicio;
                }

                /**
                 * Define el valor de la propiedad fechaInicio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaInicio(XMLGregorianCalendar value) {
                    this.fechaInicio = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaIngreso.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaIngreso() {
                    return fechaIngreso;
                }

                /**
                 * Define el valor de la propiedad fechaIngreso.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaIngreso(XMLGregorianCalendar value) {
                    this.fechaIngreso = value;
                }

            }

        }

    }

}
