
package ar.com.supervielle.xsd.integracion.cliente.registracapacidadreducida_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataRegistraCapacidadReducidaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataRegistraCapacidadReducidaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *         &lt;element name="capacidadReducida" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataRegistraCapacidadReducidaReqType", propOrder = {
    "identificador",
    "capacidadReducida",
    "fechaVencimiento"
})
public class DataRegistraCapacidadReducidaReqType {

    @XmlElement(required = true)
    protected IdClienteType identificador;
    @XmlElement(required = true)
    protected String capacidadReducida;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaVencimiento;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad capacidadReducida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacidadReducida() {
        return capacidadReducida;
    }

    /**
     * Define el valor de la propiedad capacidadReducida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacidadReducida(String value) {
        this.capacidadReducida = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimiento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Define el valor de la propiedad fechaVencimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaVencimiento(XMLGregorianCalendar value) {
        this.fechaVencimiento = value;
    }

}
