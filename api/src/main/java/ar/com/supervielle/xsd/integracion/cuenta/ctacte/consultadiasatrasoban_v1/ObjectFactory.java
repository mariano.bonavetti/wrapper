
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasoban_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasoban_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatrasoban_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDiasAtrasoBANRespType }
     * 
     */
    public DataConsultaDiasAtrasoBANRespType createDataConsultaDiasAtrasoBANRespType() {
        return new DataConsultaDiasAtrasoBANRespType();
    }

    /**
     * Create an instance of {@link RespConsultaDiasAtrasoBAN }
     * 
     */
    public RespConsultaDiasAtrasoBAN createRespConsultaDiasAtrasoBAN() {
        return new RespConsultaDiasAtrasoBAN();
    }

    /**
     * Create an instance of {@link ReqConsultaDiasAtrasoBAN }
     * 
     */
    public ReqConsultaDiasAtrasoBAN createReqConsultaDiasAtrasoBAN() {
        return new ReqConsultaDiasAtrasoBAN();
    }

    /**
     * Create an instance of {@link DataConsultaDiasAtrasoBANReqType }
     * 
     */
    public DataConsultaDiasAtrasoBANReqType createDataConsultaDiasAtrasoBANReqType() {
        return new DataConsultaDiasAtrasoBANReqType();
    }

    /**
     * Create an instance of {@link DataConsultaDiasAtrasoBANRespType.Row }
     * 
     */
    public DataConsultaDiasAtrasoBANRespType.Row createDataConsultaDiasAtrasoBANRespTypeRow() {
        return new DataConsultaDiasAtrasoBANRespType.Row();
    }

}
