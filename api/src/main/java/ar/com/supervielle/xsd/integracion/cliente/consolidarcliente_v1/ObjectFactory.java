
package ar.com.supervielle.xsd.integracion.cliente.consolidarcliente_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consolidarcliente_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consolidarcliente_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsolidarClienteReqType2 }
     * 
     */
    public DataConsolidarClienteReqType2 createDataConsolidarClienteReqType2() {
        return new DataConsolidarClienteReqType2();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteRespType2 }
     * 
     */
    public DataConsolidarClienteRespType2 createDataConsolidarClienteRespType2() {
        return new DataConsolidarClienteRespType2();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteReqType }
     * 
     */
    public DataConsolidarClienteReqType createDataConsolidarClienteReqType() {
        return new DataConsolidarClienteReqType();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteRespType }
     * 
     */
    public DataConsolidarClienteRespType createDataConsolidarClienteRespType() {
        return new DataConsolidarClienteRespType();
    }

    /**
     * Create an instance of {@link RespConsolidarCliente }
     * 
     */
    public RespConsolidarCliente createRespConsolidarCliente() {
        return new RespConsolidarCliente();
    }

    /**
     * Create an instance of {@link ReqConsolidarCliente }
     * 
     */
    public ReqConsolidarCliente createReqConsolidarCliente() {
        return new ReqConsolidarCliente();
    }

    /**
     * Create an instance of {@link RespConsolidarCliente2 }
     * 
     */
    public RespConsolidarCliente2 createRespConsolidarCliente2() {
        return new RespConsolidarCliente2();
    }

    /**
     * Create an instance of {@link ReqConsolidarCliente2 }
     * 
     */
    public ReqConsolidarCliente2 createReqConsolidarCliente2() {
        return new ReqConsolidarCliente2();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteReqType2 .Datos }
     * 
     */
    public DataConsolidarClienteReqType2 .Datos createDataConsolidarClienteReqType2Datos() {
        return new DataConsolidarClienteReqType2 .Datos();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteRespType2 .Row }
     * 
     */
    public DataConsolidarClienteRespType2 .Row createDataConsolidarClienteRespType2Row() {
        return new DataConsolidarClienteRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteReqType.Datos }
     * 
     */
    public DataConsolidarClienteReqType.Datos createDataConsolidarClienteReqTypeDatos() {
        return new DataConsolidarClienteReqType.Datos();
    }

    /**
     * Create an instance of {@link DataConsolidarClienteRespType.Row }
     * 
     */
    public DataConsolidarClienteRespType.Row createDataConsolidarClienteRespTypeRow() {
        return new DataConsolidarClienteRespType.Row();
    }

}
