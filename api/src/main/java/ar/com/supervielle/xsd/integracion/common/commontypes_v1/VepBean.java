
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para VepBean complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="VepBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="concepto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}CuentaPMC" minOccurs="0"/>
 *         &lt;element name="cuota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descExtracto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="impuesto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="leyendaEmpresa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="leyendaPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}MonedaPMC" minOccurs="0"/>
 *         &lt;element name="periodo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subConcepto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VepBean", propOrder = {
    "anio",
    "concepto",
    "cuenta",
    "cuota",
    "descExtracto",
    "idCliente",
    "importe",
    "impuesto",
    "leyendaEmpresa",
    "leyendaPago",
    "moneda",
    "periodo",
    "subConcepto",
    "vep"
})
public class VepBean {

    protected String anio;
    protected String concepto;
    protected CuentaPMC cuenta;
    protected String cuota;
    protected String descExtracto;
    protected String idCliente;
    protected Double importe;
    protected String impuesto;
    protected String leyendaEmpresa;
    protected String leyendaPago;
    protected MonedaPMC moneda;
    protected String periodo;
    protected String subConcepto;
    protected String vep;

    /**
     * Obtiene el valor de la propiedad anio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnio() {
        return anio;
    }

    /**
     * Define el valor de la propiedad anio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnio(String value) {
        this.anio = value;
    }

    /**
     * Obtiene el valor de la propiedad concepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * Define el valor de la propiedad concepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConcepto(String value) {
        this.concepto = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link CuentaPMC }
     *     
     */
    public CuentaPMC getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link CuentaPMC }
     *     
     */
    public void setCuenta(CuentaPMC value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad cuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuota() {
        return cuota;
    }

    /**
     * Define el valor de la propiedad cuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuota(String value) {
        this.cuota = value;
    }

    /**
     * Obtiene el valor de la propiedad descExtracto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescExtracto() {
        return descExtracto;
    }

    /**
     * Define el valor de la propiedad descExtracto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescExtracto(String value) {
        this.descExtracto = value;
    }

    /**
     * Obtiene el valor de la propiedad idCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * Define el valor de la propiedad idCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCliente(String value) {
        this.idCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad importe.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getImporte() {
        return importe;
    }

    /**
     * Define el valor de la propiedad importe.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setImporte(Double value) {
        this.importe = value;
    }

    /**
     * Obtiene el valor de la propiedad impuesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImpuesto() {
        return impuesto;
    }

    /**
     * Define el valor de la propiedad impuesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImpuesto(String value) {
        this.impuesto = value;
    }

    /**
     * Obtiene el valor de la propiedad leyendaEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeyendaEmpresa() {
        return leyendaEmpresa;
    }

    /**
     * Define el valor de la propiedad leyendaEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeyendaEmpresa(String value) {
        this.leyendaEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad leyendaPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeyendaPago() {
        return leyendaPago;
    }

    /**
     * Define el valor de la propiedad leyendaPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeyendaPago(String value) {
        this.leyendaPago = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link MonedaPMC }
     *     
     */
    public MonedaPMC getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link MonedaPMC }
     *     
     */
    public void setMoneda(MonedaPMC value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad periodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * Define el valor de la propiedad periodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodo(String value) {
        this.periodo = value;
    }

    /**
     * Obtiene el valor de la propiedad subConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubConcepto() {
        return subConcepto;
    }

    /**
     * Define el valor de la propiedad subConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubConcepto(String value) {
        this.subConcepto = value;
    }

    /**
     * Obtiene el valor de la propiedad vep.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVep() {
        return vep;
    }

    /**
     * Define el valor de la propiedad vep.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVep(String value) {
        this.vep = value;
    }

}
