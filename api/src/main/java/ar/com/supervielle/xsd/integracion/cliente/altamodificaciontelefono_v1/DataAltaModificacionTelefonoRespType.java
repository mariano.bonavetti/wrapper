
package ar.com.supervielle.xsd.integracion.cliente.altamodificaciontelefono_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataAltaModificacionTelefonoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaModificacionTelefonoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="telefonoRetornado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="resultado" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="secDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="Error" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaModificacionTelefonoRespType", propOrder = {
    "row"
})
public class DataAltaModificacionTelefonoRespType {

    @XmlElement(name = "Row")
    protected DataAltaModificacionTelefonoRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaModificacionTelefonoRespType.Row }
     *     
     */
    public DataAltaModificacionTelefonoRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaModificacionTelefonoRespType.Row }
     *     
     */
    public void setRow(DataAltaModificacionTelefonoRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="telefonoRetornado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="resultado" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="secDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="Error" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "telefonoRetornado",
        "resultado"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String telefonoRetornado;
        protected List<DataAltaModificacionTelefonoRespType.Row.Resultado> resultado;

        /**
         * Obtiene el valor de la propiedad telefonoRetornado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTelefonoRetornado() {
            return telefonoRetornado;
        }

        /**
         * Define el valor de la propiedad telefonoRetornado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTelefonoRetornado(String value) {
            this.telefonoRetornado = value;
        }

        /**
         * Gets the value of the resultado property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the resultado property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResultado().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataAltaModificacionTelefonoRespType.Row.Resultado }
         * 
         * 
         */
        public List<DataAltaModificacionTelefonoRespType.Row.Resultado> getResultado() {
            if (resultado == null) {
                resultado = new ArrayList<DataAltaModificacionTelefonoRespType.Row.Resultado>();
            }
            return this.resultado;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="secDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="Error" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cuenta",
            "codDomicilio",
            "secDomicilio",
            "error"
        })
        public static class Resultado {

            protected int cuenta;
            protected int codDomicilio;
            protected int secDomicilio;
            @XmlElement(name = "Error", required = true)
            protected CodDescStringType error;

            /**
             * Obtiene el valor de la propiedad cuenta.
             * 
             */
            public int getCuenta() {
                return cuenta;
            }

            /**
             * Define el valor de la propiedad cuenta.
             * 
             */
            public void setCuenta(int value) {
                this.cuenta = value;
            }

            /**
             * Obtiene el valor de la propiedad codDomicilio.
             * 
             */
            public int getCodDomicilio() {
                return codDomicilio;
            }

            /**
             * Define el valor de la propiedad codDomicilio.
             * 
             */
            public void setCodDomicilio(int value) {
                this.codDomicilio = value;
            }

            /**
             * Obtiene el valor de la propiedad secDomicilio.
             * 
             */
            public int getSecDomicilio() {
                return secDomicilio;
            }

            /**
             * Define el valor de la propiedad secDomicilio.
             * 
             */
            public void setSecDomicilio(int value) {
                this.secDomicilio = value;
            }

            /**
             * Obtiene el valor de la propiedad error.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getError() {
                return error;
            }

            /**
             * Define el valor de la propiedad error.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setError(CodDescStringType value) {
                this.error = value;
            }

        }

    }

}
