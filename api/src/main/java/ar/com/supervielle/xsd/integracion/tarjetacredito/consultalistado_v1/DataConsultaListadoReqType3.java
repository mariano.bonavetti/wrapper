
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaListadoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType">
 *                 &lt;sequence>
 *                   &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="retornaCorporativas" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaListado-v1.3", propOrder = {
    "identificador"
})
public class DataConsultaListadoReqType3 {

    @XmlElement(required = true)
    protected DataConsultaListadoReqType3 .Identificador identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaListadoReqType3 .Identificador }
     *     
     */
    public DataConsultaListadoReqType3 .Identificador getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaListadoReqType3 .Identificador }
     *     
     */
    public void setIdentificador(DataConsultaListadoReqType3 .Identificador value) {
        this.identificador = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType">
     *       &lt;sequence>
     *         &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="retornaCorporativas" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "titularidad",
        "retornaCorporativas"
    })
    public static class Identificador
        extends IdClienteType
    {

        protected Integer titularidad;
        protected Integer retornaCorporativas;

        /**
         * Obtiene el valor de la propiedad titularidad.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getTitularidad() {
            return titularidad;
        }

        /**
         * Define el valor de la propiedad titularidad.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setTitularidad(Integer value) {
            this.titularidad = value;
        }

        /**
         * Obtiene el valor de la propiedad retornaCorporativas.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRetornaCorporativas() {
            return retornaCorporativas;
        }

        /**
         * Define el valor de la propiedad retornaCorporativas.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRetornaCorporativas(Integer value) {
            this.retornaCorporativas = value;
        }

    }

}
