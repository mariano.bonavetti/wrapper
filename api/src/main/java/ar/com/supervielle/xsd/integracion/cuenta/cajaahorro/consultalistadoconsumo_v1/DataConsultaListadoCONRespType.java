
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistadoconsumo_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdListadoType;


/**
 * <p>Clase Java para DataConsultaListadoCONRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoCONRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
 *                   &lt;element name="Listado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdListadoType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoCONRespType", propOrder = {
    "row"
})
public class DataConsultaListadoCONRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaListadoCONRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaListadoCONRespType.Row }
     * 
     * 
     */
    public List<DataConsultaListadoCONRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaListadoCONRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
     *         &lt;element name="Listado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdListadoType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "listado"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaCONSUMOType identificador;
        @XmlElement(name = "Listado", required = true)
        protected IdListadoType listado;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public IdCuentaCONSUMOType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public void setIdentificador(IdCuentaCONSUMOType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad listado.
         * 
         * @return
         *     possible object is
         *     {@link IdListadoType }
         *     
         */
        public IdListadoType getListado() {
            return listado;
        }

        /**
         * Define el valor de la propiedad listado.
         * 
         * @param value
         *     allowed object is
         *     {@link IdListadoType }
         *     
         */
        public void setListado(IdListadoType value) {
            this.listado = value;
        }

    }

}
