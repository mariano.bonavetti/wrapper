
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DetalleTicket complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DetalleTicket">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="adhesion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}AdhesionBean" minOccurs="0"/>
 *         &lt;element name="detallePago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}DetalleTicketPagoBean" minOccurs="0"/>
 *         &lt;element name="entretenimientos" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}EntretenimientosBean" minOccurs="0"/>
 *         &lt;element name="pasajes" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}PasajesBean" minOccurs="0"/>
 *         &lt;element name="periodo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}PeriodoBean" minOccurs="0"/>
 *         &lt;element name="prepago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}PrePagoBean" minOccurs="0"/>
 *         &lt;element name="prepagoPes" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}PrePagoPesBean" minOccurs="0"/>
 *         &lt;element name="vep" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}VepBean" minOccurs="0"/>
 *         &lt;element name="veraz" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}VerazBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetalleTicket", propOrder = {
    "adhesion",
    "detallePago",
    "entretenimientos",
    "pasajes",
    "periodo",
    "prepago",
    "prepagoPes",
    "vep",
    "veraz"
})
public class DetalleTicket {

    protected AdhesionBean adhesion;
    protected DetalleTicketPagoBean detallePago;
    protected EntretenimientosBean entretenimientos;
    protected PasajesBean pasajes;
    protected PeriodoBean periodo;
    protected PrePagoBean prepago;
    protected PrePagoPesBean prepagoPes;
    protected VepBean vep;
    protected VerazBean veraz;

    /**
     * Obtiene el valor de la propiedad adhesion.
     * 
     * @return
     *     possible object is
     *     {@link AdhesionBean }
     *     
     */
    public AdhesionBean getAdhesion() {
        return adhesion;
    }

    /**
     * Define el valor de la propiedad adhesion.
     * 
     * @param value
     *     allowed object is
     *     {@link AdhesionBean }
     *     
     */
    public void setAdhesion(AdhesionBean value) {
        this.adhesion = value;
    }

    /**
     * Obtiene el valor de la propiedad detallePago.
     * 
     * @return
     *     possible object is
     *     {@link DetalleTicketPagoBean }
     *     
     */
    public DetalleTicketPagoBean getDetallePago() {
        return detallePago;
    }

    /**
     * Define el valor de la propiedad detallePago.
     * 
     * @param value
     *     allowed object is
     *     {@link DetalleTicketPagoBean }
     *     
     */
    public void setDetallePago(DetalleTicketPagoBean value) {
        this.detallePago = value;
    }

    /**
     * Obtiene el valor de la propiedad entretenimientos.
     * 
     * @return
     *     possible object is
     *     {@link EntretenimientosBean }
     *     
     */
    public EntretenimientosBean getEntretenimientos() {
        return entretenimientos;
    }

    /**
     * Define el valor de la propiedad entretenimientos.
     * 
     * @param value
     *     allowed object is
     *     {@link EntretenimientosBean }
     *     
     */
    public void setEntretenimientos(EntretenimientosBean value) {
        this.entretenimientos = value;
    }

    /**
     * Obtiene el valor de la propiedad pasajes.
     * 
     * @return
     *     possible object is
     *     {@link PasajesBean }
     *     
     */
    public PasajesBean getPasajes() {
        return pasajes;
    }

    /**
     * Define el valor de la propiedad pasajes.
     * 
     * @param value
     *     allowed object is
     *     {@link PasajesBean }
     *     
     */
    public void setPasajes(PasajesBean value) {
        this.pasajes = value;
    }

    /**
     * Obtiene el valor de la propiedad periodo.
     * 
     * @return
     *     possible object is
     *     {@link PeriodoBean }
     *     
     */
    public PeriodoBean getPeriodo() {
        return periodo;
    }

    /**
     * Define el valor de la propiedad periodo.
     * 
     * @param value
     *     allowed object is
     *     {@link PeriodoBean }
     *     
     */
    public void setPeriodo(PeriodoBean value) {
        this.periodo = value;
    }

    /**
     * Obtiene el valor de la propiedad prepago.
     * 
     * @return
     *     possible object is
     *     {@link PrePagoBean }
     *     
     */
    public PrePagoBean getPrepago() {
        return prepago;
    }

    /**
     * Define el valor de la propiedad prepago.
     * 
     * @param value
     *     allowed object is
     *     {@link PrePagoBean }
     *     
     */
    public void setPrepago(PrePagoBean value) {
        this.prepago = value;
    }

    /**
     * Obtiene el valor de la propiedad prepagoPes.
     * 
     * @return
     *     possible object is
     *     {@link PrePagoPesBean }
     *     
     */
    public PrePagoPesBean getPrepagoPes() {
        return prepagoPes;
    }

    /**
     * Define el valor de la propiedad prepagoPes.
     * 
     * @param value
     *     allowed object is
     *     {@link PrePagoPesBean }
     *     
     */
    public void setPrepagoPes(PrePagoPesBean value) {
        this.prepagoPes = value;
    }

    /**
     * Obtiene el valor de la propiedad vep.
     * 
     * @return
     *     possible object is
     *     {@link VepBean }
     *     
     */
    public VepBean getVep() {
        return vep;
    }

    /**
     * Define el valor de la propiedad vep.
     * 
     * @param value
     *     allowed object is
     *     {@link VepBean }
     *     
     */
    public void setVep(VepBean value) {
        this.vep = value;
    }

    /**
     * Obtiene el valor de la propiedad veraz.
     * 
     * @return
     *     possible object is
     *     {@link VerazBean }
     *     
     */
    public VerazBean getVeraz() {
        return veraz;
    }

    /**
     * Define el valor de la propiedad veraz.
     * 
     * @param value
     *     allowed object is
     *     {@link VerazBean }
     *     
     */
    public void setVeraz(VerazBean value) {
        this.veraz = value;
    }

}
