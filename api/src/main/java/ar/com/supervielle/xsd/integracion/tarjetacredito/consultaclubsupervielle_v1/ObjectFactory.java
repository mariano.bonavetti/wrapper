
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaclubsupervielle_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaclubsupervielle_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaclubsupervielle_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaClubSupervielleReqType }
     * 
     */
    public DataConsultaClubSupervielleReqType createDataConsultaClubSupervielleReqType() {
        return new DataConsultaClubSupervielleReqType();
    }

    /**
     * Create an instance of {@link DataConsultaClubSupervielleRespType }
     * 
     */
    public DataConsultaClubSupervielleRespType createDataConsultaClubSupervielleRespType() {
        return new DataConsultaClubSupervielleRespType();
    }

    /**
     * Create an instance of {@link RespConsultaClubSupervielle }
     * 
     */
    public RespConsultaClubSupervielle createRespConsultaClubSupervielle() {
        return new RespConsultaClubSupervielle();
    }

    /**
     * Create an instance of {@link ReqConsultaClubSupervielle }
     * 
     */
    public ReqConsultaClubSupervielle createReqConsultaClubSupervielle() {
        return new ReqConsultaClubSupervielle();
    }

    /**
     * Create an instance of {@link DataConsultaClubSupervielleReqType.Identificador }
     * 
     */
    public DataConsultaClubSupervielleReqType.Identificador createDataConsultaClubSupervielleReqTypeIdentificador() {
        return new DataConsultaClubSupervielleReqType.Identificador();
    }

    /**
     * Create an instance of {@link DataConsultaClubSupervielleRespType.Row }
     * 
     */
    public DataConsultaClubSupervielleRespType.Row createDataConsultaClubSupervielleRespTypeRow() {
        return new DataConsultaClubSupervielleRespType.Row();
    }

}
