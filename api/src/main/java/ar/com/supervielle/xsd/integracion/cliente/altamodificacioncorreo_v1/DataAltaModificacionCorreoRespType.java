
package ar.com.supervielle.xsd.integracion.cliente.altamodificacioncorreo_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataAltaModificacionCorreoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaModificacionCorreoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="codSecuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="cantidadRegistros" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="resultado" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="renglon" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaModificacionCorreoRespType", propOrder = {
    "row"
})
public class DataAltaModificacionCorreoRespType {

    @XmlElement(name = "Row")
    protected List<DataAltaModificacionCorreoRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataAltaModificacionCorreoRespType.Row }
     * 
     * 
     */
    public List<DataAltaModificacionCorreoRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataAltaModificacionCorreoRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="codSecuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="cantidadRegistros" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="resultado" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="renglon" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "estado",
        "codSecuencia",
        "cantidadRegistros",
        "resultado"
    })
    public static class Row {

        @XmlElement(name = "Estado", required = true)
        protected CodDescStringType estado;
        protected int codSecuencia;
        protected int cantidadRegistros;
        protected List<DataAltaModificacionCorreoRespType.Row.Resultado> resultado;

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad codSecuencia.
         * 
         */
        public int getCodSecuencia() {
            return codSecuencia;
        }

        /**
         * Define el valor de la propiedad codSecuencia.
         * 
         */
        public void setCodSecuencia(int value) {
            this.codSecuencia = value;
        }

        /**
         * Obtiene el valor de la propiedad cantidadRegistros.
         * 
         */
        public int getCantidadRegistros() {
            return cantidadRegistros;
        }

        /**
         * Define el valor de la propiedad cantidadRegistros.
         * 
         */
        public void setCantidadRegistros(int value) {
            this.cantidadRegistros = value;
        }

        /**
         * Gets the value of the resultado property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the resultado property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getResultado().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataAltaModificacionCorreoRespType.Row.Resultado }
         * 
         * 
         */
        public List<DataAltaModificacionCorreoRespType.Row.Resultado> getResultado() {
            if (resultado == null) {
                resultado = new ArrayList<DataAltaModificacionCorreoRespType.Row.Resultado>();
            }
            return this.resultado;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="renglon" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cuenta",
            "renglon"
        })
        public static class Resultado {

            protected int cuenta;
            protected int renglon;

            /**
             * Obtiene el valor de la propiedad cuenta.
             * 
             */
            public int getCuenta() {
                return cuenta;
            }

            /**
             * Define el valor de la propiedad cuenta.
             * 
             */
            public void setCuenta(int value) {
                this.cuenta = value;
            }

            /**
             * Obtiene el valor de la propiedad renglon.
             * 
             */
            public int getRenglon() {
                return renglon;
            }

            /**
             * Define el valor de la propiedad renglon.
             * 
             */
            public void setRenglon(int value) {
                this.renglon = value;
            }

        }

    }

}
