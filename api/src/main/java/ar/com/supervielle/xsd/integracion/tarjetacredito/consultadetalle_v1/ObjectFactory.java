
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType }
     * 
     */
    public DataConsultaDetalleRespType createDataConsultaDetalleRespType() {
        return new DataConsultaDetalleRespType();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row }
     * 
     */
    public DataConsultaDetalleRespType.Row createDataConsultaDetalleRespTypeRow() {
        return new DataConsultaDetalleRespType.Row();
    }

    /**
     * Create an instance of {@link RespConsultaDetalle }
     * 
     */
    public RespConsultaDetalle createRespConsultaDetalle() {
        return new RespConsultaDetalle();
    }

    /**
     * Create an instance of {@link ReqConsultaDetalle }
     * 
     */
    public ReqConsultaDetalle createReqConsultaDetalle() {
        return new ReqConsultaDetalle();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleReqType }
     * 
     */
    public DataConsultaDetalleReqType createDataConsultaDetalleReqType() {
        return new DataConsultaDetalleReqType();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.SaldoActual }
     * 
     */
    public DataConsultaDetalleRespType.Row.SaldoActual createDataConsultaDetalleRespTypeRowSaldoActual() {
        return new DataConsultaDetalleRespType.Row.SaldoActual();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.SaldoAnterior }
     * 
     */
    public DataConsultaDetalleRespType.Row.SaldoAnterior createDataConsultaDetalleRespTypeRowSaldoAnterior() {
        return new DataConsultaDetalleRespType.Row.SaldoAnterior();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.Tasas }
     * 
     */
    public DataConsultaDetalleRespType.Row.Tasas createDataConsultaDetalleRespTypeRowTasas() {
        return new DataConsultaDetalleRespType.Row.Tasas();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleRespType.Row.OffLine }
     * 
     */
    public DataConsultaDetalleRespType.Row.OffLine createDataConsultaDetalleRespTypeRowOffLine() {
        return new DataConsultaDetalleRespType.Row.OffLine();
    }

}
