
package ar.com.supervielle.xsd.integracion.cliente.altamodificaciontelefono_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.altamodificaciontelefono_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.altamodificaciontelefono_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaModificacionTelefonoRespType }
     * 
     */
    public DataAltaModificacionTelefonoRespType createDataAltaModificacionTelefonoRespType() {
        return new DataAltaModificacionTelefonoRespType();
    }

    /**
     * Create an instance of {@link DataAltaModificacionTelefonoRespType.Row }
     * 
     */
    public DataAltaModificacionTelefonoRespType.Row createDataAltaModificacionTelefonoRespTypeRow() {
        return new DataAltaModificacionTelefonoRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaModificacionTelefonoReqType }
     * 
     */
    public DataAltaModificacionTelefonoReqType createDataAltaModificacionTelefonoReqType() {
        return new DataAltaModificacionTelefonoReqType();
    }

    /**
     * Create an instance of {@link ReqAltaModificacionTelefono }
     * 
     */
    public ReqAltaModificacionTelefono createReqAltaModificacionTelefono() {
        return new ReqAltaModificacionTelefono();
    }

    /**
     * Create an instance of {@link RespAltaModificacionTelefono }
     * 
     */
    public RespAltaModificacionTelefono createRespAltaModificacionTelefono() {
        return new RespAltaModificacionTelefono();
    }

    /**
     * Create an instance of {@link DataAltaModificacionTelefonoRespType.Row.Resultado }
     * 
     */
    public DataAltaModificacionTelefonoRespType.Row.Resultado createDataAltaModificacionTelefonoRespTypeRowResultado() {
        return new DataAltaModificacionTelefonoRespType.Row.Resultado();
    }

    /**
     * Create an instance of {@link DataAltaModificacionTelefonoReqType.Telefono }
     * 
     */
    public DataAltaModificacionTelefonoReqType.Telefono createDataAltaModificacionTelefonoReqTypeTelefono() {
        return new DataAltaModificacionTelefonoReqType.Telefono();
    }

}
