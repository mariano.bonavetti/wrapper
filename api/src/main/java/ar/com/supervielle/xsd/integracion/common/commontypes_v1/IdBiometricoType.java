
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para IdBiometricoType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="IdBiometricoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dedo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="registroBiometrico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="dataTypeName" use="required" fixed="IdBiometricoType">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdBiometricoType", propOrder = {
    "dedo",
    "registroBiometrico"
})
public class IdBiometricoType {

    @XmlElement(required = true)
    protected String dedo;
    @XmlElement(required = true)
    protected String registroBiometrico;
    @XmlAttribute(name = "dataTypeName", required = true)
    protected String dataTypeName;

    /**
     * Obtiene el valor de la propiedad dedo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDedo() {
        return dedo;
    }

    /**
     * Define el valor de la propiedad dedo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDedo(String value) {
        this.dedo = value;
    }

    /**
     * Obtiene el valor de la propiedad registroBiometrico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroBiometrico() {
        return registroBiometrico;
    }

    /**
     * Define el valor de la propiedad registroBiometrico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroBiometrico(String value) {
        this.registroBiometrico = value;
    }

    /**
     * Obtiene el valor de la propiedad dataTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTypeName() {
        if (dataTypeName == null) {
            return "IdBiometricoType";
        } else {
            return dataTypeName;
        }
    }

    /**
     * Define el valor de la propiedad dataTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTypeName(String value) {
        this.dataTypeName = value;
    }

}
