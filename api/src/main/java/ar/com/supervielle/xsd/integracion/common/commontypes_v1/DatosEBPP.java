
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DatosEBPP complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DatosEBPP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoEBPP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaAltaDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fiid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formateo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="urlPropia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosEBPP", propOrder = {
    "codigo",
    "codigoEBPP",
    "fechaAlta",
    "fechaAltaDate",
    "fiid",
    "formateo",
    "info",
    "nombre",
    "urlPropia"
})
public class DatosEBPP {

    protected String codigo;
    protected String codigoEBPP;
    protected String fechaAlta;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaAltaDate;
    protected String fiid;
    protected String formateo;
    protected String info;
    protected String nombre;
    protected String urlPropia;

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoEBPP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoEBPP() {
        return codigoEBPP;
    }

    /**
     * Define el valor de la propiedad codigoEBPP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoEBPP(String value) {
        this.codigoEBPP = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAlta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAlta() {
        return fechaAlta;
    }

    /**
     * Define el valor de la propiedad fechaAlta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAlta(String value) {
        this.fechaAlta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAltaDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaAltaDate() {
        return fechaAltaDate;
    }

    /**
     * Define el valor de la propiedad fechaAltaDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaAltaDate(XMLGregorianCalendar value) {
        this.fechaAltaDate = value;
    }

    /**
     * Obtiene el valor de la propiedad fiid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiid() {
        return fiid;
    }

    /**
     * Define el valor de la propiedad fiid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiid(String value) {
        this.fiid = value;
    }

    /**
     * Obtiene el valor de la propiedad formateo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormateo() {
        return formateo;
    }

    /**
     * Define el valor de la propiedad formateo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormateo(String value) {
        this.formateo = value;
    }

    /**
     * Obtiene el valor de la propiedad info.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo() {
        return info;
    }

    /**
     * Define el valor de la propiedad info.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo(String value) {
        this.info = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad urlPropia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlPropia() {
        return urlPropia;
    }

    /**
     * Define el valor de la propiedad urlPropia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlPropia(String value) {
        this.urlPropia = value;
    }

}
