
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaadicional_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.EstadoType;


/**
 * <p>Clase Java para DataAltaAdicionalRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAdicionalRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
 *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="codigoResolucion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAdicionalRespType", propOrder = {
    "row"
})
public class DataAltaAdicionalRespType2 {

    @XmlElement(name = "Row")
    protected DataAltaAdicionalRespType2 .Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAdicionalRespType2 .Row }
     *     
     */
    public DataAltaAdicionalRespType2 .Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAdicionalRespType2 .Row }
     *     
     */
    public void setRow(DataAltaAdicionalRespType2 .Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
     *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="codigoResolucion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "estado",
        "nroSolicitud",
        "codigoResolucion"
    })
    public static class Row {

        @XmlElement(name = "Estado", required = true)
        protected EstadoType estado;
        protected Integer nroSolicitud;
        protected CodDescNumType codigoResolucion;

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link EstadoType }
         *     
         */
        public EstadoType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link EstadoType }
         *     
         */
        public void setEstado(EstadoType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad nroSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNroSolicitud() {
            return nroSolicitud;
        }

        /**
         * Define el valor de la propiedad nroSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNroSolicitud(Integer value) {
            this.nroSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoResolucion.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getCodigoResolucion() {
            return codigoResolucion;
        }

        /**
         * Define el valor de la propiedad codigoResolucion.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setCodigoResolucion(CodDescNumType value) {
            this.codigoResolucion = value;
        }

    }

}
