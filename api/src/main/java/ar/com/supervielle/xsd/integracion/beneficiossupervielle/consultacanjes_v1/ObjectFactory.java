
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacanjes_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacanjes_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacanjes_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCanjesRespType }
     * 
     */
    public DataConsultaCanjesRespType createDataConsultaCanjesRespType() {
        return new DataConsultaCanjesRespType();
    }

    /**
     * Create an instance of {@link DataConsultaCanjesRespType.Row }
     * 
     */
    public DataConsultaCanjesRespType.Row createDataConsultaCanjesRespTypeRow() {
        return new DataConsultaCanjesRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaCanjesRespType.Row.ListaCanjes }
     * 
     */
    public DataConsultaCanjesRespType.Row.ListaCanjes createDataConsultaCanjesRespTypeRowListaCanjes() {
        return new DataConsultaCanjesRespType.Row.ListaCanjes();
    }

    /**
     * Create an instance of {@link RespConsultaCanjes }
     * 
     */
    public RespConsultaCanjes createRespConsultaCanjes() {
        return new RespConsultaCanjes();
    }

    /**
     * Create an instance of {@link ReqConsultaCanjes }
     * 
     */
    public ReqConsultaCanjes createReqConsultaCanjes() {
        return new ReqConsultaCanjes();
    }

    /**
     * Create an instance of {@link DataConsultaCanjesReqType }
     * 
     */
    public DataConsultaCanjesReqType createDataConsultaCanjesReqType() {
        return new DataConsultaCanjesReqType();
    }

    /**
     * Create an instance of {@link DataConsultaCanjesRespType.Row.ListaCanjes.Canje }
     * 
     */
    public DataConsultaCanjesRespType.Row.ListaCanjes.Canje createDataConsultaCanjesRespTypeRowListaCanjesCanje() {
        return new DataConsultaCanjesRespType.Row.ListaCanjes.Canje();
    }

}
