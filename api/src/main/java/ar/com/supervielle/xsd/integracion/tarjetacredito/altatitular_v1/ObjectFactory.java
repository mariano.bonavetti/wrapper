
package ar.com.supervielle.xsd.integracion.tarjetacredito.altatitular_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altatitular_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altatitular_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaTitularRespType2 }
     * 
     */
    public DataAltaTitularRespType2 createDataAltaTitularRespType2() {
        return new DataAltaTitularRespType2();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 }
     * 
     */
    public DataAltaTitularReqType2 createDataAltaTitularReqType2() {
        return new DataAltaTitularReqType2();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard createDataAltaTitularReqType2TitularStandard() {
        return new DataAltaTitularReqType2 .TitularStandard();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard.Domicilios }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard.Domicilios createDataAltaTitularReqType2TitularStandardDomicilios() {
        return new DataAltaTitularReqType2 .TitularStandard.Domicilios();
    }

    /**
     * Create an instance of {@link DataAltaTitularRespType }
     * 
     */
    public DataAltaTitularRespType createDataAltaTitularRespType() {
        return new DataAltaTitularRespType();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType }
     * 
     */
    public DataAltaTitularReqType createDataAltaTitularReqType() {
        return new DataAltaTitularReqType();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard }
     * 
     */
    public DataAltaTitularReqType.TitularStandard createDataAltaTitularReqTypeTitularStandard() {
        return new DataAltaTitularReqType.TitularStandard();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard.Domicilios }
     * 
     */
    public DataAltaTitularReqType.TitularStandard.Domicilios createDataAltaTitularReqTypeTitularStandardDomicilios() {
        return new DataAltaTitularReqType.TitularStandard.Domicilios();
    }

    /**
     * Create an instance of {@link ReqAltaTitular }
     * 
     */
    public ReqAltaTitular createReqAltaTitular() {
        return new ReqAltaTitular();
    }

    /**
     * Create an instance of {@link RespAltaTitular }
     * 
     */
    public RespAltaTitular createRespAltaTitular() {
        return new RespAltaTitular();
    }

    /**
     * Create an instance of {@link ReqAltaTitular2 }
     * 
     */
    public ReqAltaTitular2 createReqAltaTitular2() {
        return new ReqAltaTitular2();
    }

    /**
     * Create an instance of {@link RespAltaTitular2 }
     * 
     */
    public RespAltaTitular2 createRespAltaTitular2() {
        return new RespAltaTitular2();
    }

    /**
     * Create an instance of {@link DataAltaTitularRespType2 .Row }
     * 
     */
    public DataAltaTitularRespType2 .Row createDataAltaTitularRespType2Row() {
        return new DataAltaTitularRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard.Principal }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard.Principal createDataAltaTitularReqType2TitularStandardPrincipal() {
        return new DataAltaTitularReqType2 .TitularStandard.Principal();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard.Filiatorios }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard.Filiatorios createDataAltaTitularReqType2TitularStandardFiliatorios() {
        return new DataAltaTitularReqType2 .TitularStandard.Filiatorios();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard.Producto }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard.Producto createDataAltaTitularReqType2TitularStandardProducto() {
        return new DataAltaTitularReqType2 .TitularStandard.Producto();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard.Complementarios }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard.Complementarios createDataAltaTitularReqType2TitularStandardComplementarios() {
        return new DataAltaTitularReqType2 .TitularStandard.Complementarios();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType2 .TitularStandard.Domicilios.Domicilio }
     * 
     */
    public DataAltaTitularReqType2 .TitularStandard.Domicilios.Domicilio createDataAltaTitularReqType2TitularStandardDomiciliosDomicilio() {
        return new DataAltaTitularReqType2 .TitularStandard.Domicilios.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaTitularRespType.Row }
     * 
     */
    public DataAltaTitularRespType.Row createDataAltaTitularRespTypeRow() {
        return new DataAltaTitularRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard.Principal }
     * 
     */
    public DataAltaTitularReqType.TitularStandard.Principal createDataAltaTitularReqTypeTitularStandardPrincipal() {
        return new DataAltaTitularReqType.TitularStandard.Principal();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard.Filiatorios }
     * 
     */
    public DataAltaTitularReqType.TitularStandard.Filiatorios createDataAltaTitularReqTypeTitularStandardFiliatorios() {
        return new DataAltaTitularReqType.TitularStandard.Filiatorios();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard.Producto }
     * 
     */
    public DataAltaTitularReqType.TitularStandard.Producto createDataAltaTitularReqTypeTitularStandardProducto() {
        return new DataAltaTitularReqType.TitularStandard.Producto();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard.Complementarios }
     * 
     */
    public DataAltaTitularReqType.TitularStandard.Complementarios createDataAltaTitularReqTypeTitularStandardComplementarios() {
        return new DataAltaTitularReqType.TitularStandard.Complementarios();
    }

    /**
     * Create an instance of {@link DataAltaTitularReqType.TitularStandard.Domicilios.Domicilio }
     * 
     */
    public DataAltaTitularReqType.TitularStandard.Domicilios.Domicilio createDataAltaTitularReqTypeTitularStandardDomiciliosDomicilio() {
        return new DataAltaTitularReqType.TitularStandard.Domicilios.Domicilio();
    }

}
