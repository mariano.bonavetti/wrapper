
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para datosAutenticacionExtSinTD complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="datosAutenticacionExtSinTD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="factor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaHoraAutenticacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="nroSecuenciaAutenticacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datosAutenticacionExtSinTD", propOrder = {
    "factor",
    "fechaHoraAutenticacion",
    "nroSecuenciaAutenticacion"
})
public class DatosAutenticacionExtSinTD {

    protected String factor;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaHoraAutenticacion;
    protected String nroSecuenciaAutenticacion;

    /**
     * Obtiene el valor de la propiedad factor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactor() {
        return factor;
    }

    /**
     * Define el valor de la propiedad factor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactor(String value) {
        this.factor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaHoraAutenticacion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaHoraAutenticacion() {
        return fechaHoraAutenticacion;
    }

    /**
     * Define el valor de la propiedad fechaHoraAutenticacion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaHoraAutenticacion(XMLGregorianCalendar value) {
        this.fechaHoraAutenticacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nroSecuenciaAutenticacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroSecuenciaAutenticacion() {
        return nroSecuenciaAutenticacion;
    }

    /**
     * Define el valor de la propiedad nroSecuenciaAutenticacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroSecuenciaAutenticacion(String value) {
        this.nroSecuenciaAutenticacion = value;
    }

}
