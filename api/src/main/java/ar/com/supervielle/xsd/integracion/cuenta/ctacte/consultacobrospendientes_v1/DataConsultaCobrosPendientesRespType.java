
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacobrospendientes_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaCobrosPendientesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCobrosPendientesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CobroPendiente" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCobrosPendientesRespType", propOrder = {
    "row"
})
public class DataConsultaCobrosPendientesRespType {

    @XmlElement(name = "Row")
    protected DataConsultaCobrosPendientesRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaCobrosPendientesRespType.Row }
     *     
     */
    public DataConsultaCobrosPendientesRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaCobrosPendientesRespType.Row }
     *     
     */
    public void setRow(DataConsultaCobrosPendientesRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CobroPendiente" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cobroPendiente"
    })
    public static class Row {

        @XmlElement(name = "CobroPendiente", required = true)
        protected List<DataConsultaCobrosPendientesRespType.Row.CobroPendiente> cobroPendiente;

        /**
         * Gets the value of the cobroPendiente property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cobroPendiente property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCobroPendiente().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataConsultaCobrosPendientesRespType.Row.CobroPendiente }
         * 
         * 
         */
        public List<DataConsultaCobrosPendientesRespType.Row.CobroPendiente> getCobroPendiente() {
            if (cobroPendiente == null) {
                cobroPendiente = new ArrayList<DataConsultaCobrosPendientesRespType.Row.CobroPendiente>();
            }
            return this.cobroPendiente;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="rubro" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rubro",
            "operacion",
            "moneda",
            "saldo"
        })
        public static class CobroPendiente {

            @XmlElement(required = true)
            protected String rubro;
            @XmlElement(required = true)
            protected String operacion;
            @XmlElement(required = true)
            protected String moneda;
            @XmlElement(required = true)
            protected BigDecimal saldo;

            /**
             * Obtiene el valor de la propiedad rubro.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRubro() {
                return rubro;
            }

            /**
             * Define el valor de la propiedad rubro.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRubro(String value) {
                this.rubro = value;
            }

            /**
             * Obtiene el valor de la propiedad operacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOperacion() {
                return operacion;
            }

            /**
             * Define el valor de la propiedad operacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOperacion(String value) {
                this.operacion = value;
            }

            /**
             * Obtiene el valor de la propiedad moneda.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoneda() {
                return moneda;
            }

            /**
             * Define el valor de la propiedad moneda.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoneda(String value) {
                this.moneda = value;
            }

            /**
             * Obtiene el valor de la propiedad saldo.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldo() {
                return saldo;
            }

            /**
             * Define el valor de la propiedad saldo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldo(BigDecimal value) {
                this.saldo = value;
            }

        }

    }

}
