
package ar.com.supervielle.xsd.integracion.tarjetacredito.adhesiondebitoautomatico_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.adhesiondebitoautomatico_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.adhesiondebitoautomatico_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAdhesionDebitoAutomaticoRespType }
     * 
     */
    public DataAdhesionDebitoAutomaticoRespType createDataAdhesionDebitoAutomaticoRespType() {
        return new DataAdhesionDebitoAutomaticoRespType();
    }

    /**
     * Create an instance of {@link ReqAdhesionDebitoAutomatico }
     * 
     */
    public ReqAdhesionDebitoAutomatico createReqAdhesionDebitoAutomatico() {
        return new ReqAdhesionDebitoAutomatico();
    }

    /**
     * Create an instance of {@link DataAdhesionDebitoAutomaticoReqType }
     * 
     */
    public DataAdhesionDebitoAutomaticoReqType createDataAdhesionDebitoAutomaticoReqType() {
        return new DataAdhesionDebitoAutomaticoReqType();
    }

    /**
     * Create an instance of {@link RespAdhesionDebitoAutomatico }
     * 
     */
    public RespAdhesionDebitoAutomatico createRespAdhesionDebitoAutomatico() {
        return new RespAdhesionDebitoAutomatico();
    }

    /**
     * Create an instance of {@link DataAdhesionDebitoAutomaticoRespType.Row }
     * 
     */
    public DataAdhesionDebitoAutomaticoRespType.Row createDataAdhesionDebitoAutomaticoRespTypeRow() {
        return new DataAdhesionDebitoAutomaticoRespType.Row();
    }

}
