
package ar.com.supervielle.xsd.integracion.cliente.consultadatosadicionales_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultadatosadicionales_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultadatosadicionales_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDatosAdicionalesRespType }
     * 
     */
    public DataConsultaDatosAdicionalesRespType createDataConsultaDatosAdicionalesRespType() {
        return new DataConsultaDatosAdicionalesRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaDatosAdicionales }
     * 
     */
    public ReqConsultaDatosAdicionales createReqConsultaDatosAdicionales() {
        return new ReqConsultaDatosAdicionales();
    }

    /**
     * Create an instance of {@link DataConsultaDatosAdicionalesReqType }
     * 
     */
    public DataConsultaDatosAdicionalesReqType createDataConsultaDatosAdicionalesReqType() {
        return new DataConsultaDatosAdicionalesReqType();
    }

    /**
     * Create an instance of {@link RespConsultaDatosAdicionales }
     * 
     */
    public RespConsultaDatosAdicionales createRespConsultaDatosAdicionales() {
        return new RespConsultaDatosAdicionales();
    }

    /**
     * Create an instance of {@link DataConsultaDatosAdicionalesRespType.Row }
     * 
     */
    public DataConsultaDatosAdicionalesRespType.Row createDataConsultaDatosAdicionalesRespTypeRow() {
        return new DataConsultaDatosAdicionalesRespType.Row();
    }

}
