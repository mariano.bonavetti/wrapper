
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmultixmarca_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmultixmarca_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmultixmarca_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultixMarcaRespType }
     * 
     */
    public DataConsultaParametrosMultixMarcaRespType createDataConsultaParametrosMultixMarcaRespType() {
        return new DataConsultaParametrosMultixMarcaRespType();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultixMarcaReqType }
     * 
     */
    public DataConsultaParametrosMultixMarcaReqType createDataConsultaParametrosMultixMarcaReqType() {
        return new DataConsultaParametrosMultixMarcaReqType();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultixMarcaReqType.DatosLlamada }
     * 
     */
    public DataConsultaParametrosMultixMarcaReqType.DatosLlamada createDataConsultaParametrosMultixMarcaReqTypeDatosLlamada() {
        return new DataConsultaParametrosMultixMarcaReqType.DatosLlamada();
    }

    /**
     * Create an instance of {@link ReqConsultaParametrosMultixMarca }
     * 
     */
    public ReqConsultaParametrosMultixMarca createReqConsultaParametrosMultixMarca() {
        return new ReqConsultaParametrosMultixMarca();
    }

    /**
     * Create an instance of {@link RespConsultaParametrosMultixMarca }
     * 
     */
    public RespConsultaParametrosMultixMarca createRespConsultaParametrosMultixMarca() {
        return new RespConsultaParametrosMultixMarca();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultixMarcaRespType.Respuestas }
     * 
     */
    public DataConsultaParametrosMultixMarcaRespType.Respuestas createDataConsultaParametrosMultixMarcaRespTypeRespuestas() {
        return new DataConsultaParametrosMultixMarcaRespType.Respuestas();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultixMarcaReqType.DatosLlamada.Principal }
     * 
     */
    public DataConsultaParametrosMultixMarcaReqType.DatosLlamada.Principal createDataConsultaParametrosMultixMarcaReqTypeDatosLlamadaPrincipal() {
        return new DataConsultaParametrosMultixMarcaReqType.DatosLlamada.Principal();
    }

}
