
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdoscon_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdoscon_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdoscon_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosCONRespType }
     * 
     */
    public DataConsultaAcuerdosCONRespType createDataConsultaAcuerdosCONRespType() {
        return new DataConsultaAcuerdosCONRespType();
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosCONRespType.Row }
     * 
     */
    public DataConsultaAcuerdosCONRespType.Row createDataConsultaAcuerdosCONRespTypeRow() {
        return new DataConsultaAcuerdosCONRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaAcuerdosCON }
     * 
     */
    public ReqConsultaAcuerdosCON createReqConsultaAcuerdosCON() {
        return new ReqConsultaAcuerdosCON();
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosCONReqType }
     * 
     */
    public DataConsultaAcuerdosCONReqType createDataConsultaAcuerdosCONReqType() {
        return new DataConsultaAcuerdosCONReqType();
    }

    /**
     * Create an instance of {@link RespConsultaAcuerdosCON }
     * 
     */
    public RespConsultaAcuerdosCON createRespConsultaAcuerdosCON() {
        return new RespConsultaAcuerdosCON();
    }

    /**
     * Create an instance of {@link DataConsultaAcuerdosCONRespType.Row.Renovacion }
     * 
     */
    public DataConsultaAcuerdosCONRespType.Row.Renovacion createDataConsultaAcuerdosCONRespTypeRowRenovacion() {
        return new DataConsultaAcuerdosCONRespType.Row.Renovacion();
    }

}
