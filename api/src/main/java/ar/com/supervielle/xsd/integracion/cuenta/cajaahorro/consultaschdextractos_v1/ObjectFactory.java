
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaschdextractos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaschdextractos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaschdextractos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaSchdExtractosRespType }
     * 
     */
    public DataConsultaSchdExtractosRespType createDataConsultaSchdExtractosRespType() {
        return new DataConsultaSchdExtractosRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaSchdExtractos }
     * 
     */
    public ReqConsultaSchdExtractos createReqConsultaSchdExtractos() {
        return new ReqConsultaSchdExtractos();
    }

    /**
     * Create an instance of {@link DataConsultaSchdExtractosReqType }
     * 
     */
    public DataConsultaSchdExtractosReqType createDataConsultaSchdExtractosReqType() {
        return new DataConsultaSchdExtractosReqType();
    }

    /**
     * Create an instance of {@link RespConsultaSchdExtractos }
     * 
     */
    public RespConsultaSchdExtractos createRespConsultaSchdExtractos() {
        return new RespConsultaSchdExtractos();
    }

    /**
     * Create an instance of {@link DataConsultaSchdExtractosRespType.Row }
     * 
     */
    public DataConsultaSchdExtractosRespType.Row createDataConsultaSchdExtractosRespTypeRow() {
        return new DataConsultaSchdExtractosRespType.Row();
    }

}
