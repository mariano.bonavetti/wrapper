
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.altaacuerdos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaAcuerdosRespType }
     * 
     */
    public DataAltaAcuerdosRespType createDataAltaAcuerdosRespType() {
        return new DataAltaAcuerdosRespType();
    }

    /**
     * Create an instance of {@link DataAltaAcuerdosReqType }
     * 
     */
    public DataAltaAcuerdosReqType createDataAltaAcuerdosReqType() {
        return new DataAltaAcuerdosReqType();
    }

    /**
     * Create an instance of {@link DataAltaAcuerdosReqType.Acuerdo }
     * 
     */
    public DataAltaAcuerdosReqType.Acuerdo createDataAltaAcuerdosReqTypeAcuerdo() {
        return new DataAltaAcuerdosReqType.Acuerdo();
    }

    /**
     * Create an instance of {@link ReqAltaAcuerdos }
     * 
     */
    public ReqAltaAcuerdos createReqAltaAcuerdos() {
        return new ReqAltaAcuerdos();
    }

    /**
     * Create an instance of {@link RespAltaAcuerdos }
     * 
     */
    public RespAltaAcuerdos createRespAltaAcuerdos() {
        return new RespAltaAcuerdos();
    }

    /**
     * Create an instance of {@link OperacionType }
     * 
     */
    public OperacionType createOperacionType() {
        return new OperacionType();
    }

    /**
     * Create an instance of {@link DataAltaAcuerdosRespType.Row }
     * 
     */
    public DataAltaAcuerdosRespType.Row createDataAltaAcuerdosRespTypeRow() {
        return new DataAltaAcuerdosRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaAcuerdosReqType.Cuenta }
     * 
     */
    public DataAltaAcuerdosReqType.Cuenta createDataAltaAcuerdosReqTypeCuenta() {
        return new DataAltaAcuerdosReqType.Cuenta();
    }

    /**
     * Create an instance of {@link DataAltaAcuerdosReqType.Acuerdo.Tasa }
     * 
     */
    public DataAltaAcuerdosReqType.Acuerdo.Tasa createDataAltaAcuerdosReqTypeAcuerdoTasa() {
        return new DataAltaAcuerdosReqType.Acuerdo.Tasa();
    }

}
