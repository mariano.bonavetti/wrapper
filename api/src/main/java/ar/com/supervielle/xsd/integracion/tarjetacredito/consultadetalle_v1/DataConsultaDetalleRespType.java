
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaDetalleRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDetalleRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *                   &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="digitoVerifCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="saldoActual">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="saldoAnterior">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="fechaCierreAnterior" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaCierreActual" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaCierreProximo" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaVtoAnterior" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaVtoActual" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="cuentaDebito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipoCuentaDebito" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="esPreembozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="renovacionBonificada" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tasas">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="efectivaMensualPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="efectivaMensualDolares" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="nominalAnualPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="nominalAnualDolares" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="offLine">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="limitePrestamo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDetalleRespType", propOrder = {
    "row"
})
public class DataConsultaDetalleRespType {

    @XmlElement(name = "Row")
    protected DataConsultaDetalleRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaDetalleRespType.Row }
     *     
     */
    public DataConsultaDetalleRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaDetalleRespType.Row }
     *     
     */
    public void setRow(DataConsultaDetalleRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
     *         &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="digitoVerifCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="saldoActual">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="saldoAnterior">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="pagoMinimo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="fechaCierreAnterior" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaCierreActual" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaCierreProximo" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaVtoAnterior" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaVtoActual" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="cuentaDebito" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipoCuentaDebito" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="esPreembozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="renovacionBonificada" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tasas">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="efectivaMensualPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="efectivaMensualDolares" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="nominalAnualPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="nominalAnualDolares" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="offLine">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="limitePrestamo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "numeroTarjeta",
        "tipo",
        "numeroCuenta",
        "tipoCuenta",
        "digitoVerifCuenta",
        "saldoActual",
        "saldoAnterior",
        "pagoMinimo",
        "fechaCierreAnterior",
        "fechaCierreActual",
        "fechaCierreProximo",
        "fechaVtoAnterior",
        "fechaVtoActual",
        "vigenciaDesde",
        "vigenciaHasta",
        "formaPago",
        "cuentaDebito",
        "tipoCuentaDebito",
        "esPreembozado",
        "renovacionBonificada",
        "tasas",
        "offLine"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdTarjetaType identificador;
        @XmlElement(required = true)
        protected String numeroTarjeta;
        @XmlElement(required = true)
        protected CodDescStringType tipo;
        @XmlElement(required = true)
        protected String numeroCuenta;
        @XmlElement(required = true)
        protected String tipoCuenta;
        @XmlElement(required = true)
        protected String digitoVerifCuenta;
        @XmlElement(required = true)
        protected DataConsultaDetalleRespType.Row.SaldoActual saldoActual;
        @XmlElement(required = true)
        protected DataConsultaDetalleRespType.Row.SaldoAnterior saldoAnterior;
        protected BigDecimal pagoMinimo;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaCierreAnterior;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaCierreActual;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaCierreProximo;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaVtoAnterior;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaVtoActual;
        @XmlElement(required = true)
        protected String vigenciaDesde;
        @XmlElement(required = true)
        protected String vigenciaHasta;
        @XmlElement(required = true)
        protected CodDescStringType formaPago;
        @XmlElement(required = true)
        protected String cuentaDebito;
        @XmlElement(required = true)
        protected CodDescStringType tipoCuentaDebito;
        @XmlElement(required = true)
        protected String esPreembozado;
        @XmlElement(required = true)
        protected String renovacionBonificada;
        @XmlElement(required = true)
        protected DataConsultaDetalleRespType.Row.Tasas tasas;
        @XmlElement(required = true)
        protected DataConsultaDetalleRespType.Row.OffLine offLine;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdTarjetaType }
         *     
         */
        public IdTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdTarjetaType }
         *     
         */
        public void setIdentificador(IdTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroTarjeta() {
            return numeroTarjeta;
        }

        /**
         * Define el valor de la propiedad numeroTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroTarjeta(String value) {
            this.numeroTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad tipo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipo() {
            return tipo;
        }

        /**
         * Define el valor de la propiedad tipo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipo(CodDescStringType value) {
            this.tipo = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroCuenta() {
            return numeroCuenta;
        }

        /**
         * Define el valor de la propiedad numeroCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroCuenta(String value) {
            this.numeroCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoCuenta() {
            return tipoCuenta;
        }

        /**
         * Define el valor de la propiedad tipoCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoCuenta(String value) {
            this.tipoCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad digitoVerifCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDigitoVerifCuenta() {
            return digitoVerifCuenta;
        }

        /**
         * Define el valor de la propiedad digitoVerifCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDigitoVerifCuenta(String value) {
            this.digitoVerifCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoActual.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.SaldoActual }
         *     
         */
        public DataConsultaDetalleRespType.Row.SaldoActual getSaldoActual() {
            return saldoActual;
        }

        /**
         * Define el valor de la propiedad saldoActual.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.SaldoActual }
         *     
         */
        public void setSaldoActual(DataConsultaDetalleRespType.Row.SaldoActual value) {
            this.saldoActual = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoAnterior.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.SaldoAnterior }
         *     
         */
        public DataConsultaDetalleRespType.Row.SaldoAnterior getSaldoAnterior() {
            return saldoAnterior;
        }

        /**
         * Define el valor de la propiedad saldoAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.SaldoAnterior }
         *     
         */
        public void setSaldoAnterior(DataConsultaDetalleRespType.Row.SaldoAnterior value) {
            this.saldoAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad pagoMinimo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPagoMinimo() {
            return pagoMinimo;
        }

        /**
         * Define el valor de la propiedad pagoMinimo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPagoMinimo(BigDecimal value) {
            this.pagoMinimo = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCierreAnterior.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaCierreAnterior() {
            return fechaCierreAnterior;
        }

        /**
         * Define el valor de la propiedad fechaCierreAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaCierreAnterior(XMLGregorianCalendar value) {
            this.fechaCierreAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCierreActual.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaCierreActual() {
            return fechaCierreActual;
        }

        /**
         * Define el valor de la propiedad fechaCierreActual.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaCierreActual(XMLGregorianCalendar value) {
            this.fechaCierreActual = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCierreProximo.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaCierreProximo() {
            return fechaCierreProximo;
        }

        /**
         * Define el valor de la propiedad fechaCierreProximo.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaCierreProximo(XMLGregorianCalendar value) {
            this.fechaCierreProximo = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaVtoAnterior.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaVtoAnterior() {
            return fechaVtoAnterior;
        }

        /**
         * Define el valor de la propiedad fechaVtoAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaVtoAnterior(XMLGregorianCalendar value) {
            this.fechaVtoAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaVtoActual.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaVtoActual() {
            return fechaVtoActual;
        }

        /**
         * Define el valor de la propiedad fechaVtoActual.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaVtoActual(XMLGregorianCalendar value) {
            this.fechaVtoActual = value;
        }

        /**
         * Obtiene el valor de la propiedad vigenciaDesde.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVigenciaDesde() {
            return vigenciaDesde;
        }

        /**
         * Define el valor de la propiedad vigenciaDesde.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVigenciaDesde(String value) {
            this.vigenciaDesde = value;
        }

        /**
         * Obtiene el valor de la propiedad vigenciaHasta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVigenciaHasta() {
            return vigenciaHasta;
        }

        /**
         * Define el valor de la propiedad vigenciaHasta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVigenciaHasta(String value) {
            this.vigenciaHasta = value;
        }

        /**
         * Obtiene el valor de la propiedad formaPago.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getFormaPago() {
            return formaPago;
        }

        /**
         * Define el valor de la propiedad formaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setFormaPago(CodDescStringType value) {
            this.formaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaDebito.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaDebito() {
            return cuentaDebito;
        }

        /**
         * Define el valor de la propiedad cuentaDebito.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaDebito(String value) {
            this.cuentaDebito = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoCuentaDebito.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipoCuentaDebito() {
            return tipoCuentaDebito;
        }

        /**
         * Define el valor de la propiedad tipoCuentaDebito.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipoCuentaDebito(CodDescStringType value) {
            this.tipoCuentaDebito = value;
        }

        /**
         * Obtiene el valor de la propiedad esPreembozado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEsPreembozado() {
            return esPreembozado;
        }

        /**
         * Define el valor de la propiedad esPreembozado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEsPreembozado(String value) {
            this.esPreembozado = value;
        }

        /**
         * Obtiene el valor de la propiedad renovacionBonificada.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRenovacionBonificada() {
            return renovacionBonificada;
        }

        /**
         * Define el valor de la propiedad renovacionBonificada.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRenovacionBonificada(String value) {
            this.renovacionBonificada = value;
        }

        /**
         * Obtiene el valor de la propiedad tasas.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.Tasas }
         *     
         */
        public DataConsultaDetalleRespType.Row.Tasas getTasas() {
            return tasas;
        }

        /**
         * Define el valor de la propiedad tasas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.Tasas }
         *     
         */
        public void setTasas(DataConsultaDetalleRespType.Row.Tasas value) {
            this.tasas = value;
        }

        /**
         * Obtiene el valor de la propiedad offLine.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.OffLine }
         *     
         */
        public DataConsultaDetalleRespType.Row.OffLine getOffLine() {
            return offLine;
        }

        /**
         * Define el valor de la propiedad offLine.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.OffLine }
         *     
         */
        public void setOffLine(DataConsultaDetalleRespType.Row.OffLine value) {
            this.offLine = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="limitePrestamo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="limiteDisponible" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "limiteCompra",
            "limiteFinanciacion",
            "limitePrestamo",
            "limiteDisponible"
        })
        public static class OffLine {

            protected BigDecimal limiteCompra;
            protected BigDecimal limiteFinanciacion;
            protected BigDecimal limitePrestamo;
            protected BigDecimal limiteDisponible;

            /**
             * Obtiene el valor de la propiedad limiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimiteCompra() {
                return limiteCompra;
            }

            /**
             * Define el valor de la propiedad limiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimiteCompra(BigDecimal value) {
                this.limiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteFinanciacion.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimiteFinanciacion() {
                return limiteFinanciacion;
            }

            /**
             * Define el valor de la propiedad limiteFinanciacion.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimiteFinanciacion(BigDecimal value) {
                this.limiteFinanciacion = value;
            }

            /**
             * Obtiene el valor de la propiedad limitePrestamo.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimitePrestamo() {
                return limitePrestamo;
            }

            /**
             * Define el valor de la propiedad limitePrestamo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimitePrestamo(BigDecimal value) {
                this.limitePrestamo = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteDisponible.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimiteDisponible() {
                return limiteDisponible;
            }

            /**
             * Define el valor de la propiedad limiteDisponible.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimiteDisponible(BigDecimal value) {
                this.limiteDisponible = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "saldoPesos",
            "saldoDolares"
        })
        public static class SaldoActual {

            protected BigDecimal saldoPesos;
            protected BigDecimal saldoDolares;

            /**
             * Obtiene el valor de la propiedad saldoPesos.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldoPesos() {
                return saldoPesos;
            }

            /**
             * Define el valor de la propiedad saldoPesos.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldoPesos(BigDecimal value) {
                this.saldoPesos = value;
            }

            /**
             * Obtiene el valor de la propiedad saldoDolares.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldoDolares() {
                return saldoDolares;
            }

            /**
             * Define el valor de la propiedad saldoDolares.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldoDolares(BigDecimal value) {
                this.saldoDolares = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "saldoPesos",
            "saldoDolares"
        })
        public static class SaldoAnterior {

            protected BigDecimal saldoPesos;
            protected BigDecimal saldoDolares;

            /**
             * Obtiene el valor de la propiedad saldoPesos.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldoPesos() {
                return saldoPesos;
            }

            /**
             * Define el valor de la propiedad saldoPesos.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldoPesos(BigDecimal value) {
                this.saldoPesos = value;
            }

            /**
             * Obtiene el valor de la propiedad saldoDolares.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getSaldoDolares() {
                return saldoDolares;
            }

            /**
             * Define el valor de la propiedad saldoDolares.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setSaldoDolares(BigDecimal value) {
                this.saldoDolares = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="efectivaMensualPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="efectivaMensualDolares" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="nominalAnualPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="nominalAnualDolares" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "efectivaMensualPesos",
            "efectivaMensualDolares",
            "nominalAnualPesos",
            "nominalAnualDolares"
        })
        public static class Tasas {

            @XmlElement(required = true)
            protected Object efectivaMensualPesos;
            @XmlElement(required = true)
            protected Object efectivaMensualDolares;
            @XmlElement(required = true)
            protected Object nominalAnualPesos;
            @XmlElement(required = true)
            protected Object nominalAnualDolares;

            /**
             * Obtiene el valor de la propiedad efectivaMensualPesos.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEfectivaMensualPesos() {
                return efectivaMensualPesos;
            }

            /**
             * Define el valor de la propiedad efectivaMensualPesos.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEfectivaMensualPesos(Object value) {
                this.efectivaMensualPesos = value;
            }

            /**
             * Obtiene el valor de la propiedad efectivaMensualDolares.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEfectivaMensualDolares() {
                return efectivaMensualDolares;
            }

            /**
             * Define el valor de la propiedad efectivaMensualDolares.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEfectivaMensualDolares(Object value) {
                this.efectivaMensualDolares = value;
            }

            /**
             * Obtiene el valor de la propiedad nominalAnualPesos.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getNominalAnualPesos() {
                return nominalAnualPesos;
            }

            /**
             * Define el valor de la propiedad nominalAnualPesos.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setNominalAnualPesos(Object value) {
                this.nominalAnualPesos = value;
            }

            /**
             * Obtiene el valor de la propiedad nominalAnualDolares.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getNominalAnualDolares() {
                return nominalAnualDolares;
            }

            /**
             * Define el valor de la propiedad nominalAnualDolares.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setNominalAnualDolares(Object value) {
                this.nominalAnualDolares = value;
            }

        }

    }

}
