
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimiteempresa_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimiteempresa_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimiteempresa_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaLimiteEmpresaRespType }
     * 
     */
    public DataConsultaLimiteEmpresaRespType createDataConsultaLimiteEmpresaRespType() {
        return new DataConsultaLimiteEmpresaRespType();
    }

    /**
     * Create an instance of {@link RespConsultaLimiteEmpresa }
     * 
     */
    public RespConsultaLimiteEmpresa createRespConsultaLimiteEmpresa() {
        return new RespConsultaLimiteEmpresa();
    }

    /**
     * Create an instance of {@link ReqConsultaLimiteEmpresa }
     * 
     */
    public ReqConsultaLimiteEmpresa createReqConsultaLimiteEmpresa() {
        return new ReqConsultaLimiteEmpresa();
    }

    /**
     * Create an instance of {@link DataConsultaLimiteEmpresaReqType }
     * 
     */
    public DataConsultaLimiteEmpresaReqType createDataConsultaLimiteEmpresaReqType() {
        return new DataConsultaLimiteEmpresaReqType();
    }

    /**
     * Create an instance of {@link DataConsultaLimiteEmpresaRespType.Row }
     * 
     */
    public DataConsultaLimiteEmpresaRespType.Row createDataConsultaLimiteEmpresaRespTypeRow() {
        return new DataConsultaLimiteEmpresaRespType.Row();
    }

}
