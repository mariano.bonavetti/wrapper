
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadosolicitud_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadosolicitud_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataConsultaEstadoSolicitudReqType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaEstadoSolicitud-v1", "DataConsultaEstadoSolicitudReqType");
    private final static QName _DataConsultaEstadoSolicitudRespType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaEstadoSolicitud-v1", "DataConsultaEstadoSolicitudRespType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadosolicitud_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReqConsultaEstadoSolicitud }
     * 
     */
    public ReqConsultaEstadoSolicitud createReqConsultaEstadoSolicitud() {
        return new ReqConsultaEstadoSolicitud();
    }

    /**
     * Create an instance of {@link ReqConsultaEstadoSolicitud.Data }
     * 
     */
    public ReqConsultaEstadoSolicitud.Data createReqConsultaEstadoSolicitudData() {
        return new ReqConsultaEstadoSolicitud.Data();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoSolicitudRespType }
     * 
     */
    public DataConsultaEstadoSolicitudRespType createDataConsultaEstadoSolicitudRespType() {
        return new DataConsultaEstadoSolicitudRespType();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoSolicitudRespType.Row }
     * 
     */
    public DataConsultaEstadoSolicitudRespType.Row createDataConsultaEstadoSolicitudRespTypeRow() {
        return new DataConsultaEstadoSolicitudRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoSolicitudRespType.Row.Estados }
     * 
     */
    public DataConsultaEstadoSolicitudRespType.Row.Estados createDataConsultaEstadoSolicitudRespTypeRowEstados() {
        return new DataConsultaEstadoSolicitudRespType.Row.Estados();
    }

    /**
     * Create an instance of {@link RespConsultaEstadoSolicitud }
     * 
     */
    public RespConsultaEstadoSolicitud createRespConsultaEstadoSolicitud() {
        return new RespConsultaEstadoSolicitud();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoSolicitudReqType }
     * 
     */
    public DataConsultaEstadoSolicitudReqType createDataConsultaEstadoSolicitudReqType() {
        return new DataConsultaEstadoSolicitudReqType();
    }

    /**
     * Create an instance of {@link ReqConsultaEstadoSolicitud.Data.ByCliente }
     * 
     */
    public ReqConsultaEstadoSolicitud.Data.ByCliente createReqConsultaEstadoSolicitudDataByCliente() {
        return new ReqConsultaEstadoSolicitud.Data.ByCliente();
    }

    /**
     * Create an instance of {@link ReqConsultaEstadoSolicitud.Data.BySolicitud }
     * 
     */
    public ReqConsultaEstadoSolicitud.Data.BySolicitud createReqConsultaEstadoSolicitudDataBySolicitud() {
        return new ReqConsultaEstadoSolicitud.Data.BySolicitud();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud }
     * 
     */
    public DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud createDataConsultaEstadoSolicitudRespTypeRowTipoSolicitud() {
        return new DataConsultaEstadoSolicitudRespType.Row.TipoSolicitud();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoSolicitudRespType.Row.Estados.Estado }
     * 
     */
    public DataConsultaEstadoSolicitudRespType.Row.Estados.Estado createDataConsultaEstadoSolicitudRespTypeRowEstadosEstado() {
        return new DataConsultaEstadoSolicitudRespType.Row.Estados.Estado();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsultaEstadoSolicitudReqType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaEstadoSolicitud-v1", name = "DataConsultaEstadoSolicitudReqType")
    public JAXBElement<DataConsultaEstadoSolicitudReqType> createDataConsultaEstadoSolicitudReqType(DataConsultaEstadoSolicitudReqType value) {
        return new JAXBElement<DataConsultaEstadoSolicitudReqType>(_DataConsultaEstadoSolicitudReqType_QNAME, DataConsultaEstadoSolicitudReqType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsultaEstadoSolicitudRespType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaEstadoSolicitud-v1", name = "DataConsultaEstadoSolicitudRespType")
    public JAXBElement<DataConsultaEstadoSolicitudRespType> createDataConsultaEstadoSolicitudRespType(DataConsultaEstadoSolicitudRespType value) {
        return new JAXBElement<DataConsultaEstadoSolicitudRespType>(_DataConsultaEstadoSolicitudRespType_QNAME, DataConsultaEstadoSolicitudRespType.class, null, value);
    }

}
