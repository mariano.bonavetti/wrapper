
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametros_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaParametrosResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaParametrosResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Operacion">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CodigoResolucion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="Detail" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ParamCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="Defecto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ParamMarcaValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ParamDescri" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Response_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Errores" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="DescripcionResolucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Error">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="DescripcionError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="Codigo" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaParametrosResp", propOrder = {
    "row"
})
public class DataConsultaParametrosResp {

    @XmlElement(name = "Row")
    protected DataConsultaParametrosResp.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaParametrosResp.Row }
     *     
     */
    public DataConsultaParametrosResp.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaParametrosResp.Row }
     *     
     */
    public void setRow(DataConsultaParametrosResp.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Operacion">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CodigoResolucion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="Detail" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ParamCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="Defecto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ParamMarcaValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ParamDescri" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Response_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Errores" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="DescripcionResolucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Error">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="DescripcionError" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *                 &lt;attribute name="Codigo" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operacion"
    })
    public static class Row {

        @XmlElement(name = "Operacion", required = true)
        protected DataConsultaParametrosResp.Row.Operacion operacion;

        /**
         * Obtiene el valor de la propiedad operacion.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaParametrosResp.Row.Operacion }
         *     
         */
        public DataConsultaParametrosResp.Row.Operacion getOperacion() {
            return operacion;
        }

        /**
         * Define el valor de la propiedad operacion.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaParametrosResp.Row.Operacion }
         *     
         */
        public void setOperacion(DataConsultaParametrosResp.Row.Operacion value) {
            this.operacion = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CodigoResolucion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="Detail" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ParamCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="Defecto" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ParamMarcaValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ParamDescri" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Response_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Errores" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="DescripcionResolucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Error">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="DescripcionError" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *       &lt;attribute name="Codigo" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigoResolucion",
            "detail",
            "errores"
        })
        public static class Operacion {

            @XmlElement(name = "CodigoResolucion")
            protected int codigoResolucion;
            @XmlElement(name = "Detail")
            protected List<DataConsultaParametrosResp.Row.Operacion.Detail> detail;
            @XmlElement(name = "Errores")
            protected DataConsultaParametrosResp.Row.Operacion.Errores errores;
            @XmlAttribute(name = "Codigo")
            protected BigInteger codigo;

            /**
             * Obtiene el valor de la propiedad codigoResolucion.
             * 
             */
            public int getCodigoResolucion() {
                return codigoResolucion;
            }

            /**
             * Define el valor de la propiedad codigoResolucion.
             * 
             */
            public void setCodigoResolucion(int value) {
                this.codigoResolucion = value;
            }

            /**
             * Gets the value of the detail property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the detail property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDetail().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaParametrosResp.Row.Operacion.Detail }
             * 
             * 
             */
            public List<DataConsultaParametrosResp.Row.Operacion.Detail> getDetail() {
                if (detail == null) {
                    detail = new ArrayList<DataConsultaParametrosResp.Row.Operacion.Detail>();
                }
                return this.detail;
            }

            /**
             * Obtiene el valor de la propiedad errores.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaParametrosResp.Row.Operacion.Errores }
             *     
             */
            public DataConsultaParametrosResp.Row.Operacion.Errores getErrores() {
                return errores;
            }

            /**
             * Define el valor de la propiedad errores.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaParametrosResp.Row.Operacion.Errores }
             *     
             */
            public void setErrores(DataConsultaParametrosResp.Row.Operacion.Errores value) {
                this.errores = value;
            }

            /**
             * Obtiene el valor de la propiedad codigo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCodigo() {
                return codigo;
            }

            /**
             * Define el valor de la propiedad codigo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCodigo(BigInteger value) {
                this.codigo = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ParamCodi" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="Defecto" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ParamMarcaValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ParamDescri" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Response_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paramCodi",
                "defecto",
                "paramMarcaValor",
                "paramDescri",
                "responseId"
            })
            public static class Detail {

                @XmlElement(name = "ParamCodi")
                protected int paramCodi;
                @XmlElement(name = "Defecto", required = true)
                protected String defecto;
                @XmlElement(name = "ParamMarcaValor", required = true)
                protected String paramMarcaValor;
                @XmlElement(name = "ParamDescri", required = true)
                protected String paramDescri;
                @XmlElement(name = "Response_Id", required = true)
                protected String responseId;

                /**
                 * Obtiene el valor de la propiedad paramCodi.
                 * 
                 */
                public int getParamCodi() {
                    return paramCodi;
                }

                /**
                 * Define el valor de la propiedad paramCodi.
                 * 
                 */
                public void setParamCodi(int value) {
                    this.paramCodi = value;
                }

                /**
                 * Obtiene el valor de la propiedad defecto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDefecto() {
                    return defecto;
                }

                /**
                 * Define el valor de la propiedad defecto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDefecto(String value) {
                    this.defecto = value;
                }

                /**
                 * Obtiene el valor de la propiedad paramMarcaValor.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParamMarcaValor() {
                    return paramMarcaValor;
                }

                /**
                 * Define el valor de la propiedad paramMarcaValor.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParamMarcaValor(String value) {
                    this.paramMarcaValor = value;
                }

                /**
                 * Obtiene el valor de la propiedad paramDescri.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParamDescri() {
                    return paramDescri;
                }

                /**
                 * Define el valor de la propiedad paramDescri.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParamDescri(String value) {
                    this.paramDescri = value;
                }

                /**
                 * Obtiene el valor de la propiedad responseId.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResponseId() {
                    return responseId;
                }

                /**
                 * Define el valor de la propiedad responseId.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResponseId(String value) {
                    this.responseId = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="DescripcionResolucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Error">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="DescripcionError" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "descripcionResolucion",
                "error"
            })
            public static class Errores {

                @XmlElement(name = "DescripcionResolucion", required = true)
                protected String descripcionResolucion;
                @XmlElement(name = "Error", required = true)
                protected DataConsultaParametrosResp.Row.Operacion.Errores.Error error;

                /**
                 * Obtiene el valor de la propiedad descripcionResolucion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcionResolucion() {
                    return descripcionResolucion;
                }

                /**
                 * Define el valor de la propiedad descripcionResolucion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcionResolucion(String value) {
                    this.descripcionResolucion = value;
                }

                /**
                 * Obtiene el valor de la propiedad error.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaParametrosResp.Row.Operacion.Errores.Error }
                 *     
                 */
                public DataConsultaParametrosResp.Row.Operacion.Errores.Error getError() {
                    return error;
                }

                /**
                 * Define el valor de la propiedad error.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaParametrosResp.Row.Operacion.Errores.Error }
                 *     
                 */
                public void setError(DataConsultaParametrosResp.Row.Operacion.Errores.Error value) {
                    this.error = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="DescripcionError" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="CodigoError" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "descripcionError",
                    "codigoError"
                })
                public static class Error {

                    @XmlElement(name = "DescripcionError", required = true)
                    protected String descripcionError;
                    @XmlElement(name = "CodigoError")
                    protected int codigoError;

                    /**
                     * Obtiene el valor de la propiedad descripcionError.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDescripcionError() {
                        return descripcionError;
                    }

                    /**
                     * Define el valor de la propiedad descripcionError.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDescripcionError(String value) {
                        this.descripcionError = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad codigoError.
                     * 
                     */
                    public int getCodigoError() {
                        return codigoError;
                    }

                    /**
                     * Define el valor de la propiedad codigoError.
                     * 
                     */
                    public void setCodigoError(int value) {
                        this.codigoError = value;
                    }

                }

            }

        }

    }

}
