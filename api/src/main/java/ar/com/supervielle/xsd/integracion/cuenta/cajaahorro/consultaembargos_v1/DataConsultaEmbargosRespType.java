
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaembargos_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteOficioType;


/**
 * <p>Clase Java para DataConsultaEmbargosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaEmbargosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteOficioType"/>
 *                   &lt;element name="Oficio">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="levantado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="organismo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="ejecutar" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="registro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="expediente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaProceso" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="intereses" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="fechaCaducidad" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="juzgado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="juez" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaEmbargosRespType", propOrder = {
    "row"
})
public class DataConsultaEmbargosRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaEmbargosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaEmbargosRespType.Row }
     * 
     * 
     */
    public List<DataConsultaEmbargosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaEmbargosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteOficioType"/>
     *         &lt;element name="Oficio">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="levantado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="organismo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="ejecutar" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="registro" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="expediente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaProceso" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="intereses" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="fechaCaducidad" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="juzgado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="juez" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "oficio",
        "tipo",
        "levantado",
        "fechaAlta",
        "organismo",
        "cuit",
        "cliente",
        "estado",
        "ejecutar",
        "registro",
        "expediente",
        "fechaProceso",
        "capital",
        "intereses",
        "fechaCaducidad",
        "juzgado",
        "juez"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdClienteOficioType identificador;
        @XmlElement(name = "Oficio", required = true)
        protected DataConsultaEmbargosRespType.Row.Oficio oficio;
        @XmlElement(required = true)
        protected CodDescStringType tipo;
        @XmlElement(required = true)
        protected String levantado;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAlta;
        @XmlElement(required = true)
        protected CodDescStringType organismo;
        @XmlElement(required = true)
        protected String cuit;
        @XmlElement(required = true)
        protected CodDescStringType cliente;
        @XmlElement(required = true)
        protected CodDescStringType estado;
        @XmlElement(required = true)
        protected String ejecutar;
        @XmlElement(required = true)
        protected String registro;
        @XmlElement(required = true)
        protected String expediente;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaProceso;
        protected BigDecimal capital;
        protected BigDecimal intereses;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaCaducidad;
        @XmlElement(required = true)
        protected CodDescStringType juzgado;
        @XmlElement(required = true)
        protected CodDescStringType juez;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteOficioType }
         *     
         */
        public IdClienteOficioType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteOficioType }
         *     
         */
        public void setIdentificador(IdClienteOficioType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad oficio.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaEmbargosRespType.Row.Oficio }
         *     
         */
        public DataConsultaEmbargosRespType.Row.Oficio getOficio() {
            return oficio;
        }

        /**
         * Define el valor de la propiedad oficio.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaEmbargosRespType.Row.Oficio }
         *     
         */
        public void setOficio(DataConsultaEmbargosRespType.Row.Oficio value) {
            this.oficio = value;
        }

        /**
         * Obtiene el valor de la propiedad tipo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipo() {
            return tipo;
        }

        /**
         * Define el valor de la propiedad tipo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipo(CodDescStringType value) {
            this.tipo = value;
        }

        /**
         * Obtiene el valor de la propiedad levantado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLevantado() {
            return levantado;
        }

        /**
         * Define el valor de la propiedad levantado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLevantado(String value) {
            this.levantado = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAlta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAlta() {
            return fechaAlta;
        }

        /**
         * Define el valor de la propiedad fechaAlta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAlta(XMLGregorianCalendar value) {
            this.fechaAlta = value;
        }

        /**
         * Obtiene el valor de la propiedad organismo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getOrganismo() {
            return organismo;
        }

        /**
         * Define el valor de la propiedad organismo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setOrganismo(CodDescStringType value) {
            this.organismo = value;
        }

        /**
         * Obtiene el valor de la propiedad cuit.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuit() {
            return cuit;
        }

        /**
         * Define el valor de la propiedad cuit.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuit(String value) {
            this.cuit = value;
        }

        /**
         * Obtiene el valor de la propiedad cliente.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getCliente() {
            return cliente;
        }

        /**
         * Define el valor de la propiedad cliente.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setCliente(CodDescStringType value) {
            this.cliente = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad ejecutar.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEjecutar() {
            return ejecutar;
        }

        /**
         * Define el valor de la propiedad ejecutar.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEjecutar(String value) {
            this.ejecutar = value;
        }

        /**
         * Obtiene el valor de la propiedad registro.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegistro() {
            return registro;
        }

        /**
         * Define el valor de la propiedad registro.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegistro(String value) {
            this.registro = value;
        }

        /**
         * Obtiene el valor de la propiedad expediente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpediente() {
            return expediente;
        }

        /**
         * Define el valor de la propiedad expediente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpediente(String value) {
            this.expediente = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaProceso.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaProceso() {
            return fechaProceso;
        }

        /**
         * Define el valor de la propiedad fechaProceso.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaProceso(XMLGregorianCalendar value) {
            this.fechaProceso = value;
        }

        /**
         * Obtiene el valor de la propiedad capital.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCapital() {
            return capital;
        }

        /**
         * Define el valor de la propiedad capital.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCapital(BigDecimal value) {
            this.capital = value;
        }

        /**
         * Obtiene el valor de la propiedad intereses.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIntereses() {
            return intereses;
        }

        /**
         * Define el valor de la propiedad intereses.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIntereses(BigDecimal value) {
            this.intereses = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaCaducidad.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaCaducidad() {
            return fechaCaducidad;
        }

        /**
         * Define el valor de la propiedad fechaCaducidad.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaCaducidad(XMLGregorianCalendar value) {
            this.fechaCaducidad = value;
        }

        /**
         * Obtiene el valor de la propiedad juzgado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getJuzgado() {
            return juzgado;
        }

        /**
         * Define el valor de la propiedad juzgado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setJuzgado(CodDescStringType value) {
            this.juzgado = value;
        }

        /**
         * Obtiene el valor de la propiedad juez.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getJuez() {
            return juez;
        }

        /**
         * Define el valor de la propiedad juez.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setJuez(CodDescStringType value) {
            this.juez = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identificador",
            "numero",
            "descripcion"
        })
        public static class Oficio {

            @XmlElement(required = true)
            protected String identificador;
            @XmlElement(required = true)
            protected String numero;
            @XmlElement(required = true)
            protected String descripcion;

            /**
             * Obtiene el valor de la propiedad identificador.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdentificador() {
                return identificador;
            }

            /**
             * Define el valor de la propiedad identificador.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdentificador(String value) {
                this.identificador = value;
            }

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Define el valor de la propiedad descripcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

        }

    }

}
