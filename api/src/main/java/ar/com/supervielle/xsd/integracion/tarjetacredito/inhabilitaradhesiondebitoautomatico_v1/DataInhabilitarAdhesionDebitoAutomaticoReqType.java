
package ar.com.supervielle.xsd.integracion.tarjetacredito.inhabilitaradhesiondebitoautomatico_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataInhabilitarAdhesionDebitoAutomaticoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataInhabilitarAdhesionDebitoAutomaticoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataInhabilitarAdhesionDebitoAutomaticoReqType", propOrder = {
    "identificador"
})
public class DataInhabilitarAdhesionDebitoAutomaticoReqType {

    @XmlElement(required = true)
    protected List<DataInhabilitarAdhesionDebitoAutomaticoReqType.Identificador> identificador;

    /**
     * Gets the value of the identificador property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identificador property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentificador().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataInhabilitarAdhesionDebitoAutomaticoReqType.Identificador }
     * 
     * 
     */
    public List<DataInhabilitarAdhesionDebitoAutomaticoReqType.Identificador> getIdentificador() {
        if (identificador == null) {
            identificador = new ArrayList<DataInhabilitarAdhesionDebitoAutomaticoReqType.Identificador>();
        }
        return this.identificador;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "marca",
        "cuentaTarjeta",
        "operacion"
    })
    public static class Identificador {

        @XmlElement(required = true)
        protected String marca;
        @XmlElement(required = true)
        protected String cuentaTarjeta;
        @XmlElement(required = true)
        protected String operacion;

        /**
         * Obtiene el valor de la propiedad marca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMarca() {
            return marca;
        }

        /**
         * Define el valor de la propiedad marca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMarca(String value) {
            this.marca = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCuentaTarjeta() {
            return cuentaTarjeta;
        }

        /**
         * Define el valor de la propiedad cuentaTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCuentaTarjeta(String value) {
            this.cuentaTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad operacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperacion() {
            return operacion;
        }

        /**
         * Define el valor de la propiedad operacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperacion(String value) {
            this.operacion = value;
        }

    }

}
