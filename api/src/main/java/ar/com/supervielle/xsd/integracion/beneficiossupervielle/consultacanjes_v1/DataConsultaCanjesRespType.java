
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacanjes_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataConsultaCanjesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCanjesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="listaCanjes">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="canje" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="idCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fechaCanje" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="puntosRedimidos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcionCorta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="pathImagenChica" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descEstadoCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="domicilioEntrega" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="decTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCanjesRespType", propOrder = {
    "row"
})
public class DataConsultaCanjesRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaCanjesRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaCanjesRespType.Row }
     * 
     * 
     */
    public List<DataConsultaCanjesRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaCanjesRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="listaCanjes">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="canje" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="idCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fechaCanje" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="puntosRedimidos" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcionCorta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="pathImagenChica" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descEstadoCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="domicilioEntrega" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="decTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultado",
        "listaCanjes"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String resultado;
        @XmlElement(required = true)
        protected DataConsultaCanjesRespType.Row.ListaCanjes listaCanjes;

        /**
         * Obtiene el valor de la propiedad resultado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultado() {
            return resultado;
        }

        /**
         * Define el valor de la propiedad resultado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultado(String value) {
            this.resultado = value;
        }

        /**
         * Obtiene el valor de la propiedad listaCanjes.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaCanjesRespType.Row.ListaCanjes }
         *     
         */
        public DataConsultaCanjesRespType.Row.ListaCanjes getListaCanjes() {
            return listaCanjes;
        }

        /**
         * Define el valor de la propiedad listaCanjes.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaCanjesRespType.Row.ListaCanjes }
         *     
         */
        public void setListaCanjes(DataConsultaCanjesRespType.Row.ListaCanjes value) {
            this.listaCanjes = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="canje" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="idCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fechaCanje" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="puntosRedimidos" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcionCorta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="pathImagenChica" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descEstadoCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="domicilioEntrega" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="decTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "canje"
        })
        public static class ListaCanjes {

            protected List<DataConsultaCanjesRespType.Row.ListaCanjes.Canje> canje;

            /**
             * Gets the value of the canje property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the canje property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCanje().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaCanjesRespType.Row.ListaCanjes.Canje }
             * 
             * 
             */
            public List<DataConsultaCanjesRespType.Row.ListaCanjes.Canje> getCanje() {
                if (canje == null) {
                    canje = new ArrayList<DataConsultaCanjesRespType.Row.ListaCanjes.Canje>();
                }
                return this.canje;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="idCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fechaCanje" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="puntosRedimidos" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="idProducto" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcionCorta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="pathImagenChica" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descEstadoCanje" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="domicilioEntrega" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="decTipoCtaCte" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idCanje",
                "idTarjeta",
                "fechaCanje",
                "puntosRedimidos",
                "idCuenta",
                "idProducto",
                "descripcionCorta",
                "pathImagenChica",
                "cantidad",
                "descEstadoCanje",
                "domicilioEntrega",
                "decTipoCtaCte"
            })
            public static class Canje {

                @XmlElement(required = true)
                protected String idCanje;
                @XmlElement(required = true)
                protected String idTarjeta;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaCanje;
                @XmlElement(required = true)
                protected String puntosRedimidos;
                @XmlElement(required = true)
                protected String idCuenta;
                @XmlElement(required = true)
                protected String idProducto;
                @XmlElement(required = true)
                protected String descripcionCorta;
                @XmlElement(required = true)
                protected String pathImagenChica;
                @XmlElement(required = true)
                protected String cantidad;
                @XmlElement(required = true)
                protected String descEstadoCanje;
                @XmlElement(required = true)
                protected String domicilioEntrega;
                @XmlElement(required = true)
                protected String decTipoCtaCte;

                /**
                 * Obtiene el valor de la propiedad idCanje.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdCanje() {
                    return idCanje;
                }

                /**
                 * Define el valor de la propiedad idCanje.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdCanje(String value) {
                    this.idCanje = value;
                }

                /**
                 * Obtiene el valor de la propiedad idTarjeta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdTarjeta() {
                    return idTarjeta;
                }

                /**
                 * Define el valor de la propiedad idTarjeta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdTarjeta(String value) {
                    this.idTarjeta = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaCanje.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaCanje() {
                    return fechaCanje;
                }

                /**
                 * Define el valor de la propiedad fechaCanje.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaCanje(XMLGregorianCalendar value) {
                    this.fechaCanje = value;
                }

                /**
                 * Obtiene el valor de la propiedad puntosRedimidos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPuntosRedimidos() {
                    return puntosRedimidos;
                }

                /**
                 * Define el valor de la propiedad puntosRedimidos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPuntosRedimidos(String value) {
                    this.puntosRedimidos = value;
                }

                /**
                 * Obtiene el valor de la propiedad idCuenta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdCuenta() {
                    return idCuenta;
                }

                /**
                 * Define el valor de la propiedad idCuenta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdCuenta(String value) {
                    this.idCuenta = value;
                }

                /**
                 * Obtiene el valor de la propiedad idProducto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdProducto() {
                    return idProducto;
                }

                /**
                 * Define el valor de la propiedad idProducto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdProducto(String value) {
                    this.idProducto = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcionCorta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcionCorta() {
                    return descripcionCorta;
                }

                /**
                 * Define el valor de la propiedad descripcionCorta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcionCorta(String value) {
                    this.descripcionCorta = value;
                }

                /**
                 * Obtiene el valor de la propiedad pathImagenChica.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPathImagenChica() {
                    return pathImagenChica;
                }

                /**
                 * Define el valor de la propiedad pathImagenChica.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPathImagenChica(String value) {
                    this.pathImagenChica = value;
                }

                /**
                 * Obtiene el valor de la propiedad cantidad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCantidad() {
                    return cantidad;
                }

                /**
                 * Define el valor de la propiedad cantidad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCantidad(String value) {
                    this.cantidad = value;
                }

                /**
                 * Obtiene el valor de la propiedad descEstadoCanje.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescEstadoCanje() {
                    return descEstadoCanje;
                }

                /**
                 * Define el valor de la propiedad descEstadoCanje.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescEstadoCanje(String value) {
                    this.descEstadoCanje = value;
                }

                /**
                 * Obtiene el valor de la propiedad domicilioEntrega.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDomicilioEntrega() {
                    return domicilioEntrega;
                }

                /**
                 * Define el valor de la propiedad domicilioEntrega.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDomicilioEntrega(String value) {
                    this.domicilioEntrega = value;
                }

                /**
                 * Obtiene el valor de la propiedad decTipoCtaCte.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDecTipoCtaCte() {
                    return decTipoCtaCte;
                }

                /**
                 * Define el valor de la propiedad decTipoCtaCte.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDecTipoCtaCte(String value) {
                    this.decTipoCtaCte = value;
                }

            }

        }

    }

}
