
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultatarjetasxcuenta_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaTarjetasXCuentaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaTarjetasXCuentaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaTarjetasXCuenta-v1}idCtaTarjetaType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaTarjetasXCuentaReqType", propOrder = {
    "identificador"
})
public class DataConsultaTarjetasXCuentaReqType {

    @XmlElement(required = true)
    protected IdCtaTarjetaType identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdCtaTarjetaType }
     *     
     */
    public IdCtaTarjetaType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdCtaTarjetaType }
     *     
     */
    public void setIdentificador(IdCtaTarjetaType value) {
        this.identificador = value;
    }

}
