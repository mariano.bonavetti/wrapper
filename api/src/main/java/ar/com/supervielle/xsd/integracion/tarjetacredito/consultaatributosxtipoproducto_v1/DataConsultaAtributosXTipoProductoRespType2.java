
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaatributosxtipoproducto_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaAtributosXTipoProductoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaAtributosXTipoProductoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codigoProductoSO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="siglaProductoSO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="descripcionProdSO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Parametros">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Parametro" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcionValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="valorSMARTOPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="valorMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaAtributosXTipoProductoRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaAtributosXTipoProducto-v1.1", propOrder = {
    "row"
})
public class DataConsultaAtributosXTipoProductoRespType2 {

    @XmlElement(name = "Row")
    protected DataConsultaAtributosXTipoProductoRespType2 .Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaAtributosXTipoProductoRespType2 .Row }
     *     
     */
    public DataConsultaAtributosXTipoProductoRespType2 .Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaAtributosXTipoProductoRespType2 .Row }
     *     
     */
    public void setRow(DataConsultaAtributosXTipoProductoRespType2 .Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codigoProductoSO" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="siglaProductoSO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="descripcionProdSO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Parametros">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Parametro" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcionValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="valorSMARTOPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="valorMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codigoProductoSO",
        "siglaProductoSO",
        "descripcionProdSO",
        "parametros"
    })
    public static class Row {

        protected int codigoProductoSO;
        @XmlElement(required = true)
        protected String siglaProductoSO;
        @XmlElement(required = true)
        protected String descripcionProdSO;
        @XmlElement(name = "Parametros", required = true)
        protected DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros parametros;

        /**
         * Obtiene el valor de la propiedad codigoProductoSO.
         * 
         */
        public int getCodigoProductoSO() {
            return codigoProductoSO;
        }

        /**
         * Define el valor de la propiedad codigoProductoSO.
         * 
         */
        public void setCodigoProductoSO(int value) {
            this.codigoProductoSO = value;
        }

        /**
         * Obtiene el valor de la propiedad siglaProductoSO.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSiglaProductoSO() {
            return siglaProductoSO;
        }

        /**
         * Define el valor de la propiedad siglaProductoSO.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSiglaProductoSO(String value) {
            this.siglaProductoSO = value;
        }

        /**
         * Obtiene el valor de la propiedad descripcionProdSO.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescripcionProdSO() {
            return descripcionProdSO;
        }

        /**
         * Define el valor de la propiedad descripcionProdSO.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescripcionProdSO(String value) {
            this.descripcionProdSO = value;
        }

        /**
         * Obtiene el valor de la propiedad parametros.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros }
         *     
         */
        public DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros getParametros() {
            return parametros;
        }

        /**
         * Define el valor de la propiedad parametros.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros }
         *     
         */
        public void setParametros(DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros value) {
            this.parametros = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Parametro" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcionValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="valorSMARTOPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="valorMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "parametro"
        })
        public static class Parametros {

            @XmlElement(name = "Parametro", required = true)
            protected List<DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro> parametro;

            /**
             * Gets the value of the parametro property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the parametro property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getParametro().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro }
             * 
             * 
             */
            public List<DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro> getParametro() {
                if (parametro == null) {
                    parametro = new ArrayList<DataConsultaAtributosXTipoProductoRespType2 .Row.Parametros.Parametro>();
                }
                return this.parametro;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcionValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="valorSMARTOPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="valorMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "descripcionValor",
                "valorSMARTOPEN",
                "valorMarca",
                "_default"
            })
            public static class Parametro {

                @XmlElement(required = true)
                protected BigInteger codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected String descripcionValor;
                @XmlElement(required = true)
                protected String valorSMARTOPEN;
                @XmlElement(required = true)
                protected String valorMarca;
                @XmlElement(name = "default", required = true)
                protected String _default;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCodigo(BigInteger value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcionValor.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcionValor() {
                    return descripcionValor;
                }

                /**
                 * Define el valor de la propiedad descripcionValor.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcionValor(String value) {
                    this.descripcionValor = value;
                }

                /**
                 * Obtiene el valor de la propiedad valorSMARTOPEN.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValorSMARTOPEN() {
                    return valorSMARTOPEN;
                }

                /**
                 * Define el valor de la propiedad valorSMARTOPEN.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValorSMARTOPEN(String value) {
                    this.valorSMARTOPEN = value;
                }

                /**
                 * Obtiene el valor de la propiedad valorMarca.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValorMarca() {
                    return valorMarca;
                }

                /**
                 * Define el valor de la propiedad valorMarca.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValorMarca(String value) {
                    this.valorMarca = value;
                }

                /**
                 * Obtiene el valor de la propiedad default.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDefault() {
                    return _default;
                }

                /**
                 * Define el valor de la propiedad default.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDefault(String value) {
                    this._default = value;
                }

            }

        }

    }

}
