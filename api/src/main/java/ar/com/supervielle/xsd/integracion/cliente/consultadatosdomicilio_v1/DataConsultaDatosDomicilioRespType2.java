
package ar.com.supervielle.xsd.integracion.cliente.consultadatosdomicilio_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaDatosDomicilioRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDatosDomicilioRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipoDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="pais" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="nomCalle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="numCalle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="depto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="localidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="prov" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="telefonos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="telefono" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigoPais" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="codigoArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDatosDomicilioRespType", propOrder = {
    "row"
})
public class DataConsultaDatosDomicilioRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaDatosDomicilioRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaDatosDomicilioRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaDatosDomicilioRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaDatosDomicilioRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipoDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="pais" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="nomCalle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="numCalle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="depto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="localidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="prov" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="telefonos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="telefono" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigoPais" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="codigoArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codDomicilio",
        "tipoDomicilio",
        "pais",
        "nomCalle",
        "numCalle",
        "piso",
        "depto",
        "codigoPostal",
        "localidad",
        "prov",
        "telefonos"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String codDomicilio;
        @XmlElement(required = true)
        protected String tipoDomicilio;
        @XmlElement(required = true)
        protected CodDescStringType pais;
        @XmlElement(required = true)
        protected String nomCalle;
        @XmlElement(required = true)
        protected String numCalle;
        @XmlElement(required = true)
        protected String piso;
        @XmlElement(required = true)
        protected String depto;
        @XmlElement(required = true)
        protected String codigoPostal;
        @XmlElement(required = true)
        protected CodDescStringType localidad;
        @XmlElement(required = true)
        protected CodDescStringType prov;
        @XmlElement(required = true)
        protected DataConsultaDatosDomicilioRespType2 .Row.Telefonos telefonos;

        /**
         * Obtiene el valor de la propiedad codDomicilio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodDomicilio() {
            return codDomicilio;
        }

        /**
         * Define el valor de la propiedad codDomicilio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodDomicilio(String value) {
            this.codDomicilio = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoDomicilio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoDomicilio() {
            return tipoDomicilio;
        }

        /**
         * Define el valor de la propiedad tipoDomicilio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoDomicilio(String value) {
            this.tipoDomicilio = value;
        }

        /**
         * Obtiene el valor de la propiedad pais.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getPais() {
            return pais;
        }

        /**
         * Define el valor de la propiedad pais.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setPais(CodDescStringType value) {
            this.pais = value;
        }

        /**
         * Obtiene el valor de la propiedad nomCalle.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNomCalle() {
            return nomCalle;
        }

        /**
         * Define el valor de la propiedad nomCalle.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNomCalle(String value) {
            this.nomCalle = value;
        }

        /**
         * Obtiene el valor de la propiedad numCalle.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumCalle() {
            return numCalle;
        }

        /**
         * Define el valor de la propiedad numCalle.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumCalle(String value) {
            this.numCalle = value;
        }

        /**
         * Obtiene el valor de la propiedad piso.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPiso() {
            return piso;
        }

        /**
         * Define el valor de la propiedad piso.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPiso(String value) {
            this.piso = value;
        }

        /**
         * Obtiene el valor de la propiedad depto.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepto() {
            return depto;
        }

        /**
         * Define el valor de la propiedad depto.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepto(String value) {
            this.depto = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoPostal.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodigoPostal() {
            return codigoPostal;
        }

        /**
         * Define el valor de la propiedad codigoPostal.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodigoPostal(String value) {
            this.codigoPostal = value;
        }

        /**
         * Obtiene el valor de la propiedad localidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getLocalidad() {
            return localidad;
        }

        /**
         * Define el valor de la propiedad localidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setLocalidad(CodDescStringType value) {
            this.localidad = value;
        }

        /**
         * Obtiene el valor de la propiedad prov.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getProv() {
            return prov;
        }

        /**
         * Define el valor de la propiedad prov.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setProv(CodDescStringType value) {
            this.prov = value;
        }

        /**
         * Obtiene el valor de la propiedad telefonos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDatosDomicilioRespType2 .Row.Telefonos }
         *     
         */
        public DataConsultaDatosDomicilioRespType2 .Row.Telefonos getTelefonos() {
            return telefonos;
        }

        /**
         * Define el valor de la propiedad telefonos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDatosDomicilioRespType2 .Row.Telefonos }
         *     
         */
        public void setTelefonos(DataConsultaDatosDomicilioRespType2 .Row.Telefonos value) {
            this.telefonos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="telefono" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigoPais" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="codigoArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "telefono"
        })
        public static class Telefonos {

            @XmlElement(required = true)
            protected List<DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono> telefono;

            /**
             * Gets the value of the telefono property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the telefono property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTelefono().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono }
             * 
             * 
             */
            public List<DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono> getTelefono() {
                if (telefono == null) {
                    telefono = new ArrayList<DataConsultaDatosDomicilioRespType2 .Row.Telefonos.Telefono>();
                }
                return this.telefono;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigoPais" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="codigoArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigoPais",
                "codigoArea",
                "numero",
                "extension",
                "tipo"
            })
            public static class Telefono {

                @XmlElement(required = true)
                protected String codigoPais;
                @XmlElement(required = true)
                protected String codigoArea;
                @XmlElement(required = true)
                protected String numero;
                @XmlElement(required = true)
                protected String extension;
                @XmlElement(required = true)
                protected CodDescStringType tipo;

                /**
                 * Obtiene el valor de la propiedad codigoPais.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigoPais() {
                    return codigoPais;
                }

                /**
                 * Define el valor de la propiedad codigoPais.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigoPais(String value) {
                    this.codigoPais = value;
                }

                /**
                 * Obtiene el valor de la propiedad codigoArea.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigoArea() {
                    return codigoArea;
                }

                /**
                 * Define el valor de la propiedad codigoArea.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigoArea(String value) {
                    this.codigoArea = value;
                }

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumero(String value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad extension.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExtension() {
                    return extension;
                }

                /**
                 * Define el valor de la propiedad extension.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExtension(String value) {
                    this.extension = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getTipo() {
                    return tipo;
                }

                /**
                 * Define el valor de la propiedad tipo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setTipo(CodDescStringType value) {
                    this.tipo = value;
                }

            }

        }

    }

}
