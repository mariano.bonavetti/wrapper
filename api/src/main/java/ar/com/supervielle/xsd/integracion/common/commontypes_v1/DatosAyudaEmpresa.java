
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DatosAyudaEmpresa complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DatosAyudaEmpresa">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantidadVencimientos" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="fiid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longitudClaveMaxima" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="longitudClaveMinima" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosAyudaEmpresa", propOrder = {
    "cantidadVencimientos",
    "fiid",
    "longitudClaveMaxima",
    "longitudClaveMinima"
})
public class DatosAyudaEmpresa {

    protected Integer cantidadVencimientos;
    protected String fiid;
    protected Integer longitudClaveMaxima;
    protected Integer longitudClaveMinima;

    /**
     * Obtiene el valor de la propiedad cantidadVencimientos.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCantidadVencimientos() {
        return cantidadVencimientos;
    }

    /**
     * Define el valor de la propiedad cantidadVencimientos.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCantidadVencimientos(Integer value) {
        this.cantidadVencimientos = value;
    }

    /**
     * Obtiene el valor de la propiedad fiid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiid() {
        return fiid;
    }

    /**
     * Define el valor de la propiedad fiid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiid(String value) {
        this.fiid = value;
    }

    /**
     * Obtiene el valor de la propiedad longitudClaveMaxima.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLongitudClaveMaxima() {
        return longitudClaveMaxima;
    }

    /**
     * Define el valor de la propiedad longitudClaveMaxima.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLongitudClaveMaxima(Integer value) {
        this.longitudClaveMaxima = value;
    }

    /**
     * Obtiene el valor de la propiedad longitudClaveMinima.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLongitudClaveMinima() {
        return longitudClaveMinima;
    }

    /**
     * Define el valor de la propiedad longitudClaveMinima.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLongitudClaveMinima(Integer value) {
        this.longitudClaveMinima = value;
    }

}
