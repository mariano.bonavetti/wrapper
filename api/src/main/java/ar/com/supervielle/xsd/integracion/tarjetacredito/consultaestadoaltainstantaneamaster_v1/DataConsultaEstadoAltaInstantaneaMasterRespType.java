
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoaltainstantaneamaster_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaEstadoAltaInstantaneaMasterRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaEstadoAltaInstantaneaMasterRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoResolucion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="solicitud" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codigoResolucion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="numSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="tarjeta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="cvc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaEstadoAltaInstantaneaMasterRespType", propOrder = {
    "codigoResolucion",
    "descripcion",
    "solicitud"
})
public class DataConsultaEstadoAltaInstantaneaMasterRespType {

    protected int codigoResolucion;
    @XmlElement(required = true)
    protected String descripcion;
    protected DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud solicitud;

    /**
     * Obtiene el valor de la propiedad codigoResolucion.
     * 
     */
    public int getCodigoResolucion() {
        return codigoResolucion;
    }

    /**
     * Define el valor de la propiedad codigoResolucion.
     * 
     */
    public void setCodigoResolucion(int value) {
        this.codigoResolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Define el valor de la propiedad descripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitud.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud }
     *     
     */
    public DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud getSolicitud() {
        return solicitud;
    }

    /**
     * Define el valor de la propiedad solicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud }
     *     
     */
    public void setSolicitud(DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud value) {
        this.solicitud = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codigoResolucion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="numSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="cuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="tarjeta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="vencimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="cvc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codigoResolucion",
        "descripcion",
        "numSolicitud",
        "cuenta",
        "tarjeta",
        "vencimiento",
        "cvc"
    })
    public static class Solicitud {

        protected int codigoResolucion;
        @XmlElement(required = true)
        protected String descripcion;
        @XmlElement(required = true)
        protected BigInteger numSolicitud;
        @XmlElement(required = true)
        protected BigInteger cuenta;
        @XmlElement(required = true)
        protected BigInteger tarjeta;
        @XmlElement(required = true)
        protected BigInteger vencimiento;
        @XmlElement(required = true)
        protected BigInteger cvc;

        /**
         * Obtiene el valor de la propiedad codigoResolucion.
         * 
         */
        public int getCodigoResolucion() {
            return codigoResolucion;
        }

        /**
         * Define el valor de la propiedad codigoResolucion.
         * 
         */
        public void setCodigoResolucion(int value) {
            this.codigoResolucion = value;
        }

        /**
         * Obtiene el valor de la propiedad descripcion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescripcion() {
            return descripcion;
        }

        /**
         * Define el valor de la propiedad descripcion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescripcion(String value) {
            this.descripcion = value;
        }

        /**
         * Obtiene el valor de la propiedad numSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNumSolicitud() {
            return numSolicitud;
        }

        /**
         * Define el valor de la propiedad numSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNumSolicitud(BigInteger value) {
            this.numSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad cuenta.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCuenta() {
            return cuenta;
        }

        /**
         * Define el valor de la propiedad cuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCuenta(BigInteger value) {
            this.cuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad tarjeta.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTarjeta() {
            return tarjeta;
        }

        /**
         * Define el valor de la propiedad tarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTarjeta(BigInteger value) {
            this.tarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad vencimiento.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getVencimiento() {
            return vencimiento;
        }

        /**
         * Define el valor de la propiedad vencimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setVencimiento(BigInteger value) {
            this.vencimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad cvc.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCvc() {
            return cvc;
        }

        /**
         * Define el valor de la propiedad cvc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCvc(BigInteger value) {
            this.cvc = value;
        }

    }

}
