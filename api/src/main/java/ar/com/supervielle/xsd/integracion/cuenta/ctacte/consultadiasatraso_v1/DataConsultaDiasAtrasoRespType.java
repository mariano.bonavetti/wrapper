
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadiasatraso_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaDiasAtrasoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDiasAtrasoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="diasAtraso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDiasAtrasoRespType", propOrder = {
    "row"
})
public class DataConsultaDiasAtrasoRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaDiasAtrasoRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaDiasAtrasoRespType.Row }
     * 
     * 
     */
    public List<DataConsultaDiasAtrasoRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaDiasAtrasoRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="diasAtraso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "diasAtraso"
    })
    public static class Row {

        @XmlElement(required = true)
        protected Object identificador;
        @XmlElement(required = true)
        protected BigInteger diasAtraso;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setIdentificador(Object value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad diasAtraso.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDiasAtraso() {
            return diasAtraso;
        }

        /**
         * Define el valor de la propiedad diasAtraso.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDiasAtraso(BigInteger value) {
            this.diasAtraso = value;
        }

    }

}
