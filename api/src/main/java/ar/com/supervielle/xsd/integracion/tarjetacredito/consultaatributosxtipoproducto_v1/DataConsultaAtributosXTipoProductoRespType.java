
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaatributosxtipoproducto_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaAtributosXTipoProductoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaAtributosXTipoProductoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codigoParametro" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="descripcionParametro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="descripcionValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="valorSMARTOPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="valorMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaAtributosXTipoProductoRespType", propOrder = {
    "row"
})
public class DataConsultaAtributosXTipoProductoRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaAtributosXTipoProductoRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaAtributosXTipoProductoRespType.Row }
     * 
     * 
     */
    public List<DataConsultaAtributosXTipoProductoRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaAtributosXTipoProductoRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codigoParametro" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="descripcionParametro" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="descripcionValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="valorSMARTOPEN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="valorMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="default" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codigoParametro",
        "descripcionParametro",
        "descripcionValor",
        "valorSMARTOPEN",
        "valorMarca",
        "_default"
    })
    public static class Row {

        @XmlElement(required = true)
        protected BigInteger codigoParametro;
        @XmlElement(required = true)
        protected String descripcionParametro;
        @XmlElement(required = true)
        protected String descripcionValor;
        @XmlElement(required = true)
        protected String valorSMARTOPEN;
        @XmlElement(required = true)
        protected String valorMarca;
        @XmlElement(name = "default", required = true)
        protected String _default;

        /**
         * Obtiene el valor de la propiedad codigoParametro.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCodigoParametro() {
            return codigoParametro;
        }

        /**
         * Define el valor de la propiedad codigoParametro.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCodigoParametro(BigInteger value) {
            this.codigoParametro = value;
        }

        /**
         * Obtiene el valor de la propiedad descripcionParametro.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescripcionParametro() {
            return descripcionParametro;
        }

        /**
         * Define el valor de la propiedad descripcionParametro.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescripcionParametro(String value) {
            this.descripcionParametro = value;
        }

        /**
         * Obtiene el valor de la propiedad descripcionValor.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescripcionValor() {
            return descripcionValor;
        }

        /**
         * Define el valor de la propiedad descripcionValor.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescripcionValor(String value) {
            this.descripcionValor = value;
        }

        /**
         * Obtiene el valor de la propiedad valorSMARTOPEN.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValorSMARTOPEN() {
            return valorSMARTOPEN;
        }

        /**
         * Define el valor de la propiedad valorSMARTOPEN.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValorSMARTOPEN(String value) {
            this.valorSMARTOPEN = value;
        }

        /**
         * Obtiene el valor de la propiedad valorMarca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValorMarca() {
            return valorMarca;
        }

        /**
         * Define el valor de la propiedad valorMarca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValorMarca(String value) {
            this.valorMarca = value;
        }

        /**
         * Obtiene el valor de la propiedad default.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDefault() {
            return _default;
        }

        /**
         * Define el valor de la propiedad default.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDefault(String value) {
            this._default = value;
        }

    }

}
