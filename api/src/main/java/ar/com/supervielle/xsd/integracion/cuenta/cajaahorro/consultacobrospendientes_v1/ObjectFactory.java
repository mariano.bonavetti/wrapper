
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacobrospendientes_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacobrospendientes_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataConsultaCobrosPendientesRespType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaCobrosPendientes-v1", "DataConsultaCobrosPendientesRespType");
    private final static QName _DataConsultaCobrosPendientesReqType_QNAME = new QName("http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaCobrosPendientes-v1", "DataConsultaCobrosPendientesReqType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultacobrospendientes_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCobrosPendientesRespType }
     * 
     */
    public DataConsultaCobrosPendientesRespType createDataConsultaCobrosPendientesRespType() {
        return new DataConsultaCobrosPendientesRespType();
    }

    /**
     * Create an instance of {@link DataConsultaCobrosPendientesRespType.Row }
     * 
     */
    public DataConsultaCobrosPendientesRespType.Row createDataConsultaCobrosPendientesRespTypeRow() {
        return new DataConsultaCobrosPendientesRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaCobrosPendientesReqType }
     * 
     */
    public DataConsultaCobrosPendientesReqType createDataConsultaCobrosPendientesReqType() {
        return new DataConsultaCobrosPendientesReqType();
    }

    /**
     * Create an instance of {@link ReqConsultaCobrosPendientes }
     * 
     */
    public ReqConsultaCobrosPendientes createReqConsultaCobrosPendientes() {
        return new ReqConsultaCobrosPendientes();
    }

    /**
     * Create an instance of {@link RespConsultaCobrosPendientes }
     * 
     */
    public RespConsultaCobrosPendientes createRespConsultaCobrosPendientes() {
        return new RespConsultaCobrosPendientes();
    }

    /**
     * Create an instance of {@link DataConsultaCobrosPendientesRespType.Row.CobroPendiente }
     * 
     */
    public DataConsultaCobrosPendientesRespType.Row.CobroPendiente createDataConsultaCobrosPendientesRespTypeRowCobroPendiente() {
        return new DataConsultaCobrosPendientesRespType.Row.CobroPendiente();
    }

    /**
     * Create an instance of {@link DataConsultaCobrosPendientesReqType.Identificador }
     * 
     */
    public DataConsultaCobrosPendientesReqType.Identificador createDataConsultaCobrosPendientesReqTypeIdentificador() {
        return new DataConsultaCobrosPendientesReqType.Identificador();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsultaCobrosPendientesRespType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaCobrosPendientes-v1", name = "DataConsultaCobrosPendientesRespType")
    public JAXBElement<DataConsultaCobrosPendientesRespType> createDataConsultaCobrosPendientesRespType(DataConsultaCobrosPendientesRespType value) {
        return new JAXBElement<DataConsultaCobrosPendientesRespType>(_DataConsultaCobrosPendientesRespType_QNAME, DataConsultaCobrosPendientesRespType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConsultaCobrosPendientesReqType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaCobrosPendientes-v1", name = "DataConsultaCobrosPendientesReqType")
    public JAXBElement<DataConsultaCobrosPendientesReqType> createDataConsultaCobrosPendientesReqType(DataConsultaCobrosPendientesReqType value) {
        return new JAXBElement<DataConsultaCobrosPendientesReqType>(_DataConsultaCobrosPendientesReqType_QNAME, DataConsultaCobrosPendientesReqType.class, null, value);
    }

}
