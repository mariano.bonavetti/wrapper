
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para operacionTC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="operacionTC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *         &lt;element name="movimientos">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="movimiento" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="comercio">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ticket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="rubro">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cuota">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="cantidadCuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="importes">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="importeCuota" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operacionTC", propOrder = {
    "identificador",
    "movimientos"
})
public class OperacionTC {

    @XmlElement(required = true)
    protected IdTarjetaType identificador;
    @XmlElement(required = true)
    protected OperacionTC.Movimientos movimientos;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdTarjetaType }
     *     
     */
    public IdTarjetaType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdTarjetaType }
     *     
     */
    public void setIdentificador(IdTarjetaType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad movimientos.
     * 
     * @return
     *     possible object is
     *     {@link OperacionTC.Movimientos }
     *     
     */
    public OperacionTC.Movimientos getMovimientos() {
        return movimientos;
    }

    /**
     * Define el valor de la propiedad movimientos.
     * 
     * @param value
     *     allowed object is
     *     {@link OperacionTC.Movimientos }
     *     
     */
    public void setMovimientos(OperacionTC.Movimientos value) {
        this.movimientos = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="movimiento" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="comercio">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ticket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="rubro">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cuota">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="cantidadCuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="importes">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="importeCuota" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "movimiento"
    })
    public static class Movimientos {

        protected List<OperacionTC.Movimientos.Movimiento> movimiento;

        /**
         * Gets the value of the movimiento property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the movimiento property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMovimiento().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link OperacionTC.Movimientos.Movimiento }
         * 
         * 
         */
        public List<OperacionTC.Movimientos.Movimiento> getMovimiento() {
            if (movimiento == null) {
                movimiento = new ArrayList<OperacionTC.Movimientos.Movimiento>();
            }
            return this.movimiento;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="comercio">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ticket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="rubro">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cuota">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="cantidadCuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="importes">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="importeCuota" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tarjeta",
            "comercio",
            "fechaOperacion",
            "rubro",
            "moneda",
            "cuota",
            "importes"
        })
        public static class Movimiento {

            @XmlElement(required = true)
            protected String tarjeta;
            @XmlElement(required = true)
            protected OperacionTC.Movimientos.Movimiento.Comercio comercio;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaOperacion;
            @XmlElement(required = true)
            protected OperacionTC.Movimientos.Movimiento.Rubro rubro;
            @XmlElement(required = true)
            protected String moneda;
            @XmlElement(required = true)
            protected OperacionTC.Movimientos.Movimiento.Cuota cuota;
            @XmlElement(required = true)
            protected OperacionTC.Movimientos.Movimiento.Importes importes;

            /**
             * Obtiene el valor de la propiedad tarjeta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTarjeta() {
                return tarjeta;
            }

            /**
             * Define el valor de la propiedad tarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTarjeta(String value) {
                this.tarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad comercio.
             * 
             * @return
             *     possible object is
             *     {@link OperacionTC.Movimientos.Movimiento.Comercio }
             *     
             */
            public OperacionTC.Movimientos.Movimiento.Comercio getComercio() {
                return comercio;
            }

            /**
             * Define el valor de la propiedad comercio.
             * 
             * @param value
             *     allowed object is
             *     {@link OperacionTC.Movimientos.Movimiento.Comercio }
             *     
             */
            public void setComercio(OperacionTC.Movimientos.Movimiento.Comercio value) {
                this.comercio = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaOperacion.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaOperacion() {
                return fechaOperacion;
            }

            /**
             * Define el valor de la propiedad fechaOperacion.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaOperacion(XMLGregorianCalendar value) {
                this.fechaOperacion = value;
            }

            /**
             * Obtiene el valor de la propiedad rubro.
             * 
             * @return
             *     possible object is
             *     {@link OperacionTC.Movimientos.Movimiento.Rubro }
             *     
             */
            public OperacionTC.Movimientos.Movimiento.Rubro getRubro() {
                return rubro;
            }

            /**
             * Define el valor de la propiedad rubro.
             * 
             * @param value
             *     allowed object is
             *     {@link OperacionTC.Movimientos.Movimiento.Rubro }
             *     
             */
            public void setRubro(OperacionTC.Movimientos.Movimiento.Rubro value) {
                this.rubro = value;
            }

            /**
             * Obtiene el valor de la propiedad moneda.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMoneda() {
                return moneda;
            }

            /**
             * Define el valor de la propiedad moneda.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMoneda(String value) {
                this.moneda = value;
            }

            /**
             * Obtiene el valor de la propiedad cuota.
             * 
             * @return
             *     possible object is
             *     {@link OperacionTC.Movimientos.Movimiento.Cuota }
             *     
             */
            public OperacionTC.Movimientos.Movimiento.Cuota getCuota() {
                return cuota;
            }

            /**
             * Define el valor de la propiedad cuota.
             * 
             * @param value
             *     allowed object is
             *     {@link OperacionTC.Movimientos.Movimiento.Cuota }
             *     
             */
            public void setCuota(OperacionTC.Movimientos.Movimiento.Cuota value) {
                this.cuota = value;
            }

            /**
             * Obtiene el valor de la propiedad importes.
             * 
             * @return
             *     possible object is
             *     {@link OperacionTC.Movimientos.Movimiento.Importes }
             *     
             */
            public OperacionTC.Movimientos.Movimiento.Importes getImportes() {
                return importes;
            }

            /**
             * Define el valor de la propiedad importes.
             * 
             * @param value
             *     allowed object is
             *     {@link OperacionTC.Movimientos.Movimiento.Importes }
             *     
             */
            public void setImportes(OperacionTC.Movimientos.Movimiento.Importes value) {
                this.importes = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ticket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "ticket"
            })
            public static class Comercio {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String descripcion;
                protected String ticket;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad ticket.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicket() {
                    return ticket;
                }

                /**
                 * Define el valor de la propiedad ticket.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicket(String value) {
                    this.ticket = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="numeroCuota" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="cantidadCuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "numeroCuota",
                "cantidadCuotas"
            })
            public static class Cuota {

                @XmlElement(required = true)
                protected BigInteger numeroCuota;
                @XmlElement(required = true)
                protected BigInteger cantidadCuotas;

                /**
                 * Obtiene el valor de la propiedad numeroCuota.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNumeroCuota() {
                    return numeroCuota;
                }

                /**
                 * Define el valor de la propiedad numeroCuota.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNumeroCuota(BigInteger value) {
                    this.numeroCuota = value;
                }

                /**
                 * Obtiene el valor de la propiedad cantidadCuotas.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCantidadCuotas() {
                    return cantidadCuotas;
                }

                /**
                 * Define el valor de la propiedad cantidadCuotas.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCantidadCuotas(BigInteger value) {
                    this.cantidadCuotas = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="importeCuota" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "importeCuota",
                "importeTotal"
            })
            public static class Importes {

                @XmlElement(required = true)
                protected BigDecimal importeCuota;
                @XmlElement(required = true)
                protected BigDecimal importeTotal;

                /**
                 * Obtiene el valor de la propiedad importeCuota.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getImporteCuota() {
                    return importeCuota;
                }

                /**
                 * Define el valor de la propiedad importeCuota.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setImporteCuota(BigDecimal value) {
                    this.importeCuota = value;
                }

                /**
                 * Obtiene el valor de la propiedad importeTotal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getImporteTotal() {
                    return importeTotal;
                }

                /**
                 * Define el valor de la propiedad importeTotal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setImporteTotal(BigDecimal value) {
                    this.importeTotal = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion"
            })
            public static class Rubro {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String descripcion;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

            }

        }

    }

}
