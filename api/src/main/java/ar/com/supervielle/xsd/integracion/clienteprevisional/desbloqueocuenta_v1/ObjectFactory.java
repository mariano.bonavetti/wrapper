
package ar.com.supervielle.xsd.integracion.clienteprevisional.desbloqueocuenta_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.clienteprevisional.desbloqueocuenta_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.clienteprevisional.desbloqueocuenta_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataDesbloqueoCuentaRespType }
     * 
     */
    public DataDesbloqueoCuentaRespType createDataDesbloqueoCuentaRespType() {
        return new DataDesbloqueoCuentaRespType();
    }

    /**
     * Create an instance of {@link ReqDesbloqueoCuenta }
     * 
     */
    public ReqDesbloqueoCuenta createReqDesbloqueoCuenta() {
        return new ReqDesbloqueoCuenta();
    }

    /**
     * Create an instance of {@link DataDesbloqueoCuentaReqType }
     * 
     */
    public DataDesbloqueoCuentaReqType createDataDesbloqueoCuentaReqType() {
        return new DataDesbloqueoCuentaReqType();
    }

    /**
     * Create an instance of {@link RespDesbloqueoCuenta }
     * 
     */
    public RespDesbloqueoCuenta createRespDesbloqueoCuenta() {
        return new RespDesbloqueoCuenta();
    }

    /**
     * Create an instance of {@link DataDesbloqueoCuentaRespType.Row }
     * 
     */
    public DataDesbloqueoCuentaRespType.Row createDataDesbloqueoCuentaRespTypeRow() {
        return new DataDesbloqueoCuentaRespType.Row();
    }

}
