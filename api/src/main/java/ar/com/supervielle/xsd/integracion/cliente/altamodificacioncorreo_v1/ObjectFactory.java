
package ar.com.supervielle.xsd.integracion.cliente.altamodificacioncorreo_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.altamodificacioncorreo_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.altamodificacioncorreo_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaModificacionCorreoRespType }
     * 
     */
    public DataAltaModificacionCorreoRespType createDataAltaModificacionCorreoRespType() {
        return new DataAltaModificacionCorreoRespType();
    }

    /**
     * Create an instance of {@link DataAltaModificacionCorreoRespType.Row }
     * 
     */
    public DataAltaModificacionCorreoRespType.Row createDataAltaModificacionCorreoRespTypeRow() {
        return new DataAltaModificacionCorreoRespType.Row();
    }

    /**
     * Create an instance of {@link RespAltaModificacionCorreo }
     * 
     */
    public RespAltaModificacionCorreo createRespAltaModificacionCorreo() {
        return new RespAltaModificacionCorreo();
    }

    /**
     * Create an instance of {@link ReqAltaModificacionCorreo }
     * 
     */
    public ReqAltaModificacionCorreo createReqAltaModificacionCorreo() {
        return new ReqAltaModificacionCorreo();
    }

    /**
     * Create an instance of {@link DataAltaModificacionCorreoReqType }
     * 
     */
    public DataAltaModificacionCorreoReqType createDataAltaModificacionCorreoReqType() {
        return new DataAltaModificacionCorreoReqType();
    }

    /**
     * Create an instance of {@link DataAltaModificacionCorreoRespType.Row.Resultado }
     * 
     */
    public DataAltaModificacionCorreoRespType.Row.Resultado createDataAltaModificacionCorreoRespTypeRowResultado() {
        return new DataAltaModificacionCorreoRespType.Row.Resultado();
    }

}
