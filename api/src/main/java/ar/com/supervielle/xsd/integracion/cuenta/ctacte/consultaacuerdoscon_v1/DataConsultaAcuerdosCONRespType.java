
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaacuerdoscon_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;


/**
 * <p>Clase Java para DataConsultaAcuerdosCONRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaAcuerdosCONRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
 *                   &lt;element name="numeroAcuerdo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipoAcuerdo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="tasa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="clase" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="renovacion">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaAcuerdosCONRespType", propOrder = {
    "row"
})
public class DataConsultaAcuerdosCONRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaAcuerdosCONRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaAcuerdosCONRespType.Row }
     * 
     * 
     */
    public List<DataConsultaAcuerdosCONRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaAcuerdosCONRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
     *         &lt;element name="numeroAcuerdo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipoAcuerdo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="tasa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="clase" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="renovacion">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "numeroAcuerdo",
        "tipoAcuerdo",
        "fechaAlta",
        "fechaBaja",
        "tasa",
        "clase",
        "monto",
        "renovacion"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaCONSUMOType identificador;
        @XmlElement(required = true)
        protected String numeroAcuerdo;
        @XmlElement(required = true)
        protected CodDescStringType tipoAcuerdo;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAlta;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaBaja;
        @XmlElement(required = true)
        protected CodDescStringType tasa;
        @XmlElement(required = true)
        protected CodDescStringType clase;
        protected BigDecimal monto;
        @XmlElement(required = true)
        protected DataConsultaAcuerdosCONRespType.Row.Renovacion renovacion;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public IdCuentaCONSUMOType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public void setIdentificador(IdCuentaCONSUMOType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroAcuerdo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroAcuerdo() {
            return numeroAcuerdo;
        }

        /**
         * Define el valor de la propiedad numeroAcuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroAcuerdo(String value) {
            this.numeroAcuerdo = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoAcuerdo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipoAcuerdo() {
            return tipoAcuerdo;
        }

        /**
         * Define el valor de la propiedad tipoAcuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipoAcuerdo(CodDescStringType value) {
            this.tipoAcuerdo = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAlta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAlta() {
            return fechaAlta;
        }

        /**
         * Define el valor de la propiedad fechaAlta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAlta(XMLGregorianCalendar value) {
            this.fechaAlta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaBaja.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaBaja() {
            return fechaBaja;
        }

        /**
         * Define el valor de la propiedad fechaBaja.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaBaja(XMLGregorianCalendar value) {
            this.fechaBaja = value;
        }

        /**
         * Obtiene el valor de la propiedad tasa.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTasa() {
            return tasa;
        }

        /**
         * Define el valor de la propiedad tasa.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTasa(CodDescStringType value) {
            this.tasa = value;
        }

        /**
         * Obtiene el valor de la propiedad clase.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getClase() {
            return clase;
        }

        /**
         * Define el valor de la propiedad clase.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setClase(CodDescStringType value) {
            this.clase = value;
        }

        /**
         * Obtiene el valor de la propiedad monto.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getMonto() {
            return monto;
        }

        /**
         * Define el valor de la propiedad monto.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setMonto(BigDecimal value) {
            this.monto = value;
        }

        /**
         * Obtiene el valor de la propiedad renovacion.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaAcuerdosCONRespType.Row.Renovacion }
         *     
         */
        public DataConsultaAcuerdosCONRespType.Row.Renovacion getRenovacion() {
            return renovacion;
        }

        /**
         * Define el valor de la propiedad renovacion.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaAcuerdosCONRespType.Row.Renovacion }
         *     
         */
        public void setRenovacion(DataConsultaAcuerdosCONRespType.Row.Renovacion value) {
            this.renovacion = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "marca",
            "estado",
            "fecha"
        })
        public static class Renovacion {

            @XmlElement(required = true)
            protected String marca;
            @XmlElement(required = true)
            protected String estado;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fecha;

            /**
             * Obtiene el valor de la propiedad marca.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMarca() {
                return marca;
            }

            /**
             * Define el valor de la propiedad marca.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMarca(String value) {
                this.marca = value;
            }

            /**
             * Obtiene el valor de la propiedad estado.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEstado() {
                return estado;
            }

            /**
             * Define el valor de la propiedad estado.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEstado(String value) {
                this.estado = value;
            }

            /**
             * Obtiene el valor de la propiedad fecha.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFecha() {
                return fecha;
            }

            /**
             * Define el valor de la propiedad fecha.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFecha(XMLGregorianCalendar value) {
                this.fecha = value;
            }

        }

    }

}
