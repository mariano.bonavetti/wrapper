
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestado_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaEstTarjetaReq complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaEstTarjetaReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaEstTarjetaReq", propOrder = {
    "nroSolicitud"
})
public class DataConsultaEstTarjetaReq {

    protected int nroSolicitud;

    /**
     * Obtiene el valor de la propiedad nroSolicitud.
     * 
     */
    public int getNroSolicitud() {
        return nroSolicitud;
    }

    /**
     * Define el valor de la propiedad nroSolicitud.
     * 
     */
    public void setNroSolicitud(int value) {
        this.nroSolicitud = value;
    }

}
