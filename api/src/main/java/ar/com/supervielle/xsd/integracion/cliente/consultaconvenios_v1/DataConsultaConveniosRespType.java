
package ar.com.supervielle.xsd.integracion.cliente.consultaconvenios_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdConvenioType;


/**
 * <p>Clase Java para DataConsultaConveniosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaConveniosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Linea" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                             &lt;element name="convenio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idConvenioType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaConveniosRespType", propOrder = {
    "row"
})
public class DataConsultaConveniosRespType {

    @XmlElement(name = "Row")
    protected DataConsultaConveniosRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaConveniosRespType.Row }
     *     
     */
    public DataConsultaConveniosRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaConveniosRespType.Row }
     *     
     */
    public void setRow(DataConsultaConveniosRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Linea" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                   &lt;element name="convenio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idConvenioType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "linea"
    })
    public static class Row {

        @XmlElement(name = "Linea", required = true)
        protected List<DataConsultaConveniosRespType.Row.Linea> linea;

        /**
         * Gets the value of the linea property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the linea property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLinea().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataConsultaConveniosRespType.Row.Linea }
         * 
         * 
         */
        public List<DataConsultaConveniosRespType.Row.Linea> getLinea() {
            if (linea == null) {
                linea = new ArrayList<DataConsultaConveniosRespType.Row.Linea>();
            }
            return this.linea;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *         &lt;element name="convenio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idConvenioType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identificador",
            "convenio"
        })
        public static class Linea {

            @XmlElement(required = true)
            protected IdClienteType identificador;
            @XmlElement(required = true)
            protected IdConvenioType convenio;

            /**
             * Obtiene el valor de la propiedad identificador.
             * 
             * @return
             *     possible object is
             *     {@link IdClienteType }
             *     
             */
            public IdClienteType getIdentificador() {
                return identificador;
            }

            /**
             * Define el valor de la propiedad identificador.
             * 
             * @param value
             *     allowed object is
             *     {@link IdClienteType }
             *     
             */
            public void setIdentificador(IdClienteType value) {
                this.identificador = value;
            }

            /**
             * Obtiene el valor de la propiedad convenio.
             * 
             * @return
             *     possible object is
             *     {@link IdConvenioType }
             *     
             */
            public IdConvenioType getConvenio() {
                return convenio;
            }

            /**
             * Define el valor de la propiedad convenio.
             * 
             * @param value
             *     allowed object is
             *     {@link IdConvenioType }
             *     
             */
            public void setConvenio(IdConvenioType value) {
                this.convenio = value;
            }

        }

    }

}
