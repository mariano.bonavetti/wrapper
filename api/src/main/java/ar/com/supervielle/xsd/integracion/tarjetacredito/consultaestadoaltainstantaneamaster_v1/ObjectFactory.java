
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoaltainstantaneamaster_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoaltainstantaneamaster_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoaltainstantaneamaster_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaEstadoAltaInstantaneaMasterRespType }
     * 
     */
    public DataConsultaEstadoAltaInstantaneaMasterRespType createDataConsultaEstadoAltaInstantaneaMasterRespType() {
        return new DataConsultaEstadoAltaInstantaneaMasterRespType();
    }

    /**
     * Create an instance of {@link RespConsultaEstadoAltaInstantaneaMaster }
     * 
     */
    public RespConsultaEstadoAltaInstantaneaMaster createRespConsultaEstadoAltaInstantaneaMaster() {
        return new RespConsultaEstadoAltaInstantaneaMaster();
    }

    /**
     * Create an instance of {@link ReqConsultaEstadoAltaInstantaneaMaster }
     * 
     */
    public ReqConsultaEstadoAltaInstantaneaMaster createReqConsultaEstadoAltaInstantaneaMaster() {
        return new ReqConsultaEstadoAltaInstantaneaMaster();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoAltaInstantaneaMasterReqType }
     * 
     */
    public DataConsultaEstadoAltaInstantaneaMasterReqType createDataConsultaEstadoAltaInstantaneaMasterReqType() {
        return new DataConsultaEstadoAltaInstantaneaMasterReqType();
    }

    /**
     * Create an instance of {@link DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud }
     * 
     */
    public DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud createDataConsultaEstadoAltaInstantaneaMasterRespTypeSolicitud() {
        return new DataConsultaEstadoAltaInstantaneaMasterRespType.Solicitud();
    }

}
