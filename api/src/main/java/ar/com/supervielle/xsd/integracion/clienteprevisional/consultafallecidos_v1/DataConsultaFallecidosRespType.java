
package ar.com.supervielle.xsd.integracion.clienteprevisional.consultafallecidos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaFallecidosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaFallecidosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaFallecido" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="Novedad">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="tipoSoporte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaFallecidosRespType", propOrder = {
    "row"
})
public class DataConsultaFallecidosRespType {

    @XmlElement(name = "Row")
    protected DataConsultaFallecidosRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaFallecidosRespType.Row }
     *     
     */
    public DataConsultaFallecidosRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaFallecidosRespType.Row }
     *     
     */
    public void setRow(DataConsultaFallecidosRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaFallecido" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="Novedad">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="tipoSoporte" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cuil",
        "identificador",
        "origen",
        "fechaFallecido",
        "novedad"
    })
    public static class Row {

        @XmlElement(name = "CUIL", required = true)
        protected String cuil;
        @XmlElement(name = "Identificador", required = true)
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected String origen;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaFallecido;
        @XmlElement(name = "Novedad", required = true)
        protected DataConsultaFallecidosRespType.Row.Novedad novedad;

        /**
         * Obtiene el valor de la propiedad cuil.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUIL() {
            return cuil;
        }

        /**
         * Define el valor de la propiedad cuil.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUIL(String value) {
            this.cuil = value;
        }

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad origen.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigen() {
            return origen;
        }

        /**
         * Define el valor de la propiedad origen.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigen(String value) {
            this.origen = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaFallecido.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaFallecido() {
            return fechaFallecido;
        }

        /**
         * Define el valor de la propiedad fechaFallecido.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaFallecido(XMLGregorianCalendar value) {
            this.fechaFallecido = value;
        }

        /**
         * Obtiene el valor de la propiedad novedad.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaFallecidosRespType.Row.Novedad }
         *     
         */
        public DataConsultaFallecidosRespType.Row.Novedad getNovedad() {
            return novedad;
        }

        /**
         * Define el valor de la propiedad novedad.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaFallecidosRespType.Row.Novedad }
         *     
         */
        public void setNovedad(DataConsultaFallecidosRespType.Row.Novedad value) {
            this.novedad = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="tipoSoporte" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tipo",
            "descripcion",
            "usuario",
            "terminal",
            "fecha",
            "tipoSoporte"
        })
        public static class Novedad {

            @XmlElement(required = true)
            protected String tipo;
            @XmlElement(required = true)
            protected String descripcion;
            protected String usuario;
            protected String terminal;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fecha;
            @XmlElement(required = true)
            protected String tipoSoporte;

            /**
             * Obtiene el valor de la propiedad tipo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipo() {
                return tipo;
            }

            /**
             * Define el valor de la propiedad tipo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipo(String value) {
                this.tipo = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Define el valor de la propiedad descripcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

            /**
             * Obtiene el valor de la propiedad usuario.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUsuario() {
                return usuario;
            }

            /**
             * Define el valor de la propiedad usuario.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUsuario(String value) {
                this.usuario = value;
            }

            /**
             * Obtiene el valor de la propiedad terminal.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTerminal() {
                return terminal;
            }

            /**
             * Define el valor de la propiedad terminal.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTerminal(String value) {
                this.terminal = value;
            }

            /**
             * Obtiene el valor de la propiedad fecha.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFecha() {
                return fecha;
            }

            /**
             * Define el valor de la propiedad fecha.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFecha(XMLGregorianCalendar value) {
                this.fecha = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoSoporte.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipoSoporte() {
                return tipoSoporte;
            }

            /**
             * Define el valor de la propiedad tipoSoporte.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipoSoporte(String value) {
                this.tipoSoporte = value;
            }

        }

    }

}
