
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *         &lt;element name="tipoVinculo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ordenFechaAlta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "identificador",
    "tipoVinculo",
    "estado",
    "ordenFechaAlta"
})
@XmlRootElement(name = "byClienteId")
public class ByClienteId {

    @XmlElement(name = "Identificador", required = true)
    protected IdClienteType identificador;
    @XmlElement(required = true)
    protected String tipoVinculo;
    @XmlElement(required = true)
    protected String estado;
    @XmlElement(required = true)
    protected String ordenFechaAlta;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdClienteType }
     *     
     */
    public IdClienteType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdClienteType }
     *     
     */
    public void setIdentificador(IdClienteType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoVinculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVinculo() {
        return tipoVinculo;
    }

    /**
     * Define el valor de la propiedad tipoVinculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVinculo(String value) {
        this.tipoVinculo = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad ordenFechaAlta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdenFechaAlta() {
        return ordenFechaAlta;
    }

    /**
     * Define el valor de la propiedad ordenFechaAlta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdenFechaAlta(String value) {
        this.ordenFechaAlta = value;
    }

}
