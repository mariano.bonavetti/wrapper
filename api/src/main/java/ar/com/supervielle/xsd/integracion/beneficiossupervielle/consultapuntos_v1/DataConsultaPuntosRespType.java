
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;


/**
 * <p>Clase Java para DataConsultaPuntosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaPuntosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="programa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="puntosDisponibles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencerEsteMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencerProximoMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencer2Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="puntosAVencer3Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="listaPuntosAVencer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="vencimiento" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="cantidadPuntos" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="fechaVto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaPuntosRespType", propOrder = {
    "row"
})
public class DataConsultaPuntosRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaPuntosRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaPuntosRespType.Row }
     * 
     * 
     */
    public List<DataConsultaPuntosRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaPuntosRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="programa" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="puntosDisponibles" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencerEsteMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencerProximoMes" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencer2Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="puntosAVencer3Meses" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="listaPuntosAVencer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="vencimiento" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="cantidadPuntos" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="fechaVto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultado",
        "programa",
        "titular",
        "puntosDisponibles",
        "puntosAVencerEsteMes",
        "puntosAVencerProximoMes",
        "puntosAVencer2Meses",
        "puntosAVencer3Meses",
        "listaPuntosAVencer"
    })
    public static class Row {

        protected int resultado;
        @XmlElement(required = true)
        protected CodDescNumType programa;
        @XmlElement(required = true)
        protected String titular;
        protected int puntosDisponibles;
        protected int puntosAVencerEsteMes;
        protected int puntosAVencerProximoMes;
        protected int puntosAVencer2Meses;
        protected int puntosAVencer3Meses;
        @XmlElement(required = true)
        protected DataConsultaPuntosRespType.Row.ListaPuntosAVencer listaPuntosAVencer;

        /**
         * Obtiene el valor de la propiedad resultado.
         * 
         */
        public int getResultado() {
            return resultado;
        }

        /**
         * Define el valor de la propiedad resultado.
         * 
         */
        public void setResultado(int value) {
            this.resultado = value;
        }

        /**
         * Obtiene el valor de la propiedad programa.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getPrograma() {
            return programa;
        }

        /**
         * Define el valor de la propiedad programa.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setPrograma(CodDescNumType value) {
            this.programa = value;
        }

        /**
         * Obtiene el valor de la propiedad titular.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitular() {
            return titular;
        }

        /**
         * Define el valor de la propiedad titular.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitular(String value) {
            this.titular = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosDisponibles.
         * 
         */
        public int getPuntosDisponibles() {
            return puntosDisponibles;
        }

        /**
         * Define el valor de la propiedad puntosDisponibles.
         * 
         */
        public void setPuntosDisponibles(int value) {
            this.puntosDisponibles = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencerEsteMes.
         * 
         */
        public int getPuntosAVencerEsteMes() {
            return puntosAVencerEsteMes;
        }

        /**
         * Define el valor de la propiedad puntosAVencerEsteMes.
         * 
         */
        public void setPuntosAVencerEsteMes(int value) {
            this.puntosAVencerEsteMes = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencerProximoMes.
         * 
         */
        public int getPuntosAVencerProximoMes() {
            return puntosAVencerProximoMes;
        }

        /**
         * Define el valor de la propiedad puntosAVencerProximoMes.
         * 
         */
        public void setPuntosAVencerProximoMes(int value) {
            this.puntosAVencerProximoMes = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencer2Meses.
         * 
         */
        public int getPuntosAVencer2Meses() {
            return puntosAVencer2Meses;
        }

        /**
         * Define el valor de la propiedad puntosAVencer2Meses.
         * 
         */
        public void setPuntosAVencer2Meses(int value) {
            this.puntosAVencer2Meses = value;
        }

        /**
         * Obtiene el valor de la propiedad puntosAVencer3Meses.
         * 
         */
        public int getPuntosAVencer3Meses() {
            return puntosAVencer3Meses;
        }

        /**
         * Define el valor de la propiedad puntosAVencer3Meses.
         * 
         */
        public void setPuntosAVencer3Meses(int value) {
            this.puntosAVencer3Meses = value;
        }

        /**
         * Obtiene el valor de la propiedad listaPuntosAVencer.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaPuntosRespType.Row.ListaPuntosAVencer }
         *     
         */
        public DataConsultaPuntosRespType.Row.ListaPuntosAVencer getListaPuntosAVencer() {
            return listaPuntosAVencer;
        }

        /**
         * Define el valor de la propiedad listaPuntosAVencer.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaPuntosRespType.Row.ListaPuntosAVencer }
         *     
         */
        public void setListaPuntosAVencer(DataConsultaPuntosRespType.Row.ListaPuntosAVencer value) {
            this.listaPuntosAVencer = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="vencimiento" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="cantidadPuntos" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="fechaVto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "vencimiento"
        })
        public static class ListaPuntosAVencer {

            protected List<DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento> vencimiento;

            /**
             * Gets the value of the vencimiento property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the vencimiento property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getVencimiento().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento }
             * 
             * 
             */
            public List<DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento> getVencimiento() {
                if (vencimiento == null) {
                    vencimiento = new ArrayList<DataConsultaPuntosRespType.Row.ListaPuntosAVencer.Vencimiento>();
                }
                return this.vencimiento;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="cantidadPuntos" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="fechaVto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "cantidadPuntos",
                "fechaVto"
            })
            public static class Vencimiento {

                protected int cantidadPuntos;
                @XmlElement(required = true)
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar fechaVto;

                /**
                 * Obtiene el valor de la propiedad cantidadPuntos.
                 * 
                 */
                public int getCantidadPuntos() {
                    return cantidadPuntos;
                }

                /**
                 * Define el valor de la propiedad cantidadPuntos.
                 * 
                 */
                public void setCantidadPuntos(int value) {
                    this.cantidadPuntos = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaVto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaVto() {
                    return fechaVto;
                }

                /**
                 * Define el valor de la propiedad fechaVto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaVto(XMLGregorianCalendar value) {
                    this.fechaVto = value;
                }

            }

        }

    }

}
