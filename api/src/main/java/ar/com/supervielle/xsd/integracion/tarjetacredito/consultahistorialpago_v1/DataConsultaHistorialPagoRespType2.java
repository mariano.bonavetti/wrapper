
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultahistorialpago_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaHistorialPagoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaHistorialPagoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="numeroLote" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="numeroCobranza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaUltLiq" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="fechaIngresoCobro" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaPago" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaHistorialPagoRespType", propOrder = {
    "row"
})
public class DataConsultaHistorialPagoRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaHistorialPagoRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaHistorialPagoRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaHistorialPagoRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaHistorialPagoRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="numeroLote" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="numeroCobranza" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaUltLiq" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="fechaIngresoCobro" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaPago" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="formaPago" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "numeroLote",
        "numeroCobranza",
        "fechaUltLiq",
        "importe",
        "fechaIngresoCobro",
        "fechaPago",
        "formaPago",
        "importeTotal"
    })
    public static class Row {

        @XmlElement(required = true)
        protected Object identificador;
        @XmlElement(required = true)
        protected String numeroLote;
        @XmlElement(required = true)
        protected String numeroCobranza;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaUltLiq;
        protected BigDecimal importe;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaIngresoCobro;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaPago;
        @XmlElement(required = true)
        protected CodDescStringType formaPago;
        protected BigDecimal importeTotal;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setIdentificador(Object value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroLote.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroLote() {
            return numeroLote;
        }

        /**
         * Define el valor de la propiedad numeroLote.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroLote(String value) {
            this.numeroLote = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroCobranza.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroCobranza() {
            return numeroCobranza;
        }

        /**
         * Define el valor de la propiedad numeroCobranza.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroCobranza(String value) {
            this.numeroCobranza = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaUltLiq.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaUltLiq() {
            return fechaUltLiq;
        }

        /**
         * Define el valor de la propiedad fechaUltLiq.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaUltLiq(XMLGregorianCalendar value) {
            this.fechaUltLiq = value;
        }

        /**
         * Obtiene el valor de la propiedad importe.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getImporte() {
            return importe;
        }

        /**
         * Define el valor de la propiedad importe.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setImporte(BigDecimal value) {
            this.importe = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaIngresoCobro.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaIngresoCobro() {
            return fechaIngresoCobro;
        }

        /**
         * Define el valor de la propiedad fechaIngresoCobro.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaIngresoCobro(XMLGregorianCalendar value) {
            this.fechaIngresoCobro = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaPago.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaPago() {
            return fechaPago;
        }

        /**
         * Define el valor de la propiedad fechaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaPago(XMLGregorianCalendar value) {
            this.fechaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad formaPago.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getFormaPago() {
            return formaPago;
        }

        /**
         * Define el valor de la propiedad formaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setFormaPago(CodDescStringType value) {
            this.formaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad importeTotal.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getImporteTotal() {
            return importeTotal;
        }

        /**
         * Define el valor de la propiedad importeTotal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setImporteTotal(BigDecimal value) {
            this.importeTotal = value;
        }

    }

}
