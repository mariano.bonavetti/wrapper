
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmulti_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmulti_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaparametrosmulti_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultiReq }
     * 
     */
    public DataConsultaParametrosMultiReq createDataConsultaParametrosMultiReq() {
        return new DataConsultaParametrosMultiReq();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultiReq.DatosLlamada }
     * 
     */
    public DataConsultaParametrosMultiReq.DatosLlamada createDataConsultaParametrosMultiReqDatosLlamada() {
        return new DataConsultaParametrosMultiReq.DatosLlamada();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultiResp }
     * 
     */
    public DataConsultaParametrosMultiResp createDataConsultaParametrosMultiResp() {
        return new DataConsultaParametrosMultiResp();
    }

    /**
     * Create an instance of {@link RespConsultaParametrosMulti }
     * 
     */
    public RespConsultaParametrosMulti createRespConsultaParametrosMulti() {
        return new RespConsultaParametrosMulti();
    }

    /**
     * Create an instance of {@link ReqConsultaParametrosMulti }
     * 
     */
    public ReqConsultaParametrosMulti createReqConsultaParametrosMulti() {
        return new ReqConsultaParametrosMulti();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultiReq.DatosLlamada.Principal }
     * 
     */
    public DataConsultaParametrosMultiReq.DatosLlamada.Principal createDataConsultaParametrosMultiReqDatosLlamadaPrincipal() {
        return new DataConsultaParametrosMultiReq.DatosLlamada.Principal();
    }

    /**
     * Create an instance of {@link DataConsultaParametrosMultiResp.Respuestas }
     * 
     */
    public DataConsultaParametrosMultiResp.Respuestas createDataConsultaParametrosMultiRespRespuestas() {
        return new DataConsultaParametrosMultiResp.Respuestas();
    }

}
