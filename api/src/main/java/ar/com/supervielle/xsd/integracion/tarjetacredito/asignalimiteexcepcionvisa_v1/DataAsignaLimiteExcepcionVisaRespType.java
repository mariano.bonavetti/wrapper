
package ar.com.supervielle.xsd.integracion.tarjetacredito.asignalimiteexcepcionvisa_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataAsignaLimiteExcepcionVisaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAsignaLimiteExcepcionVisaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="retorno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="nroTarjeta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="fechaVigencia" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="montoExcepcion" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAsignaLimiteExcepcionVisaRespType", propOrder = {
    "retorno",
    "nroCuenta",
    "nroTarjeta",
    "fechaVigencia",
    "montoExcepcion"
})
public class DataAsignaLimiteExcepcionVisaRespType {

    @XmlElement(required = true)
    protected String retorno;
    @XmlElement(required = true)
    protected BigInteger nroCuenta;
    protected long nroTarjeta;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaVigencia;
    protected BigDecimal montoExcepcion;

    /**
     * Obtiene el valor de la propiedad retorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetorno() {
        return retorno;
    }

    /**
     * Define el valor de la propiedad retorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetorno(String value) {
        this.retorno = value;
    }

    /**
     * Obtiene el valor de la propiedad nroCuenta.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNroCuenta() {
        return nroCuenta;
    }

    /**
     * Define el valor de la propiedad nroCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNroCuenta(BigInteger value) {
        this.nroCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad nroTarjeta.
     * 
     */
    public long getNroTarjeta() {
        return nroTarjeta;
    }

    /**
     * Define el valor de la propiedad nroTarjeta.
     * 
     */
    public void setNroTarjeta(long value) {
        this.nroTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVigencia.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaVigencia() {
        return fechaVigencia;
    }

    /**
     * Define el valor de la propiedad fechaVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaVigencia(XMLGregorianCalendar value) {
        this.fechaVigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad montoExcepcion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMontoExcepcion() {
        return montoExcepcion;
    }

    /**
     * Define el valor de la propiedad montoExcepcion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMontoExcepcion(BigDecimal value) {
        this.montoExcepcion = value;
    }

}
