
package ar.com.supervielle.xsd.integracion.clienteprevisional.registrarfedevida_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para FeDeVida complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FeDeVida">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="FeDeVidaCP">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="id" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="habDesbloqueo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FeDeVidaBO">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="entidadReceptora" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="codigoTransaccion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="entidadFDV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="terminalFDV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeDeVida", propOrder = {
    "feDeVidaCP",
    "feDeVidaBO"
})
public class FeDeVida {

    @XmlElement(name = "FeDeVidaCP")
    protected FeDeVida.FeDeVidaCP feDeVidaCP;
    @XmlElement(name = "FeDeVidaBO")
    protected FeDeVida.FeDeVidaBO feDeVidaBO;

    /**
     * Obtiene el valor de la propiedad feDeVidaCP.
     * 
     * @return
     *     possible object is
     *     {@link FeDeVida.FeDeVidaCP }
     *     
     */
    public FeDeVida.FeDeVidaCP getFeDeVidaCP() {
        return feDeVidaCP;
    }

    /**
     * Define el valor de la propiedad feDeVidaCP.
     * 
     * @param value
     *     allowed object is
     *     {@link FeDeVida.FeDeVidaCP }
     *     
     */
    public void setFeDeVidaCP(FeDeVida.FeDeVidaCP value) {
        this.feDeVidaCP = value;
    }

    /**
     * Obtiene el valor de la propiedad feDeVidaBO.
     * 
     * @return
     *     possible object is
     *     {@link FeDeVida.FeDeVidaBO }
     *     
     */
    public FeDeVida.FeDeVidaBO getFeDeVidaBO() {
        return feDeVidaBO;
    }

    /**
     * Define el valor de la propiedad feDeVidaBO.
     * 
     * @param value
     *     allowed object is
     *     {@link FeDeVida.FeDeVidaBO }
     *     
     */
    public void setFeDeVidaBO(FeDeVida.FeDeVidaBO value) {
        this.feDeVidaBO = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="entidadReceptora" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codigoTransaccion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="entidadFDV" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="terminalFDV" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cuil",
        "entidadReceptora",
        "codigoTransaccion",
        "tipoDoc",
        "numDoc",
        "entidadFDV",
        "terminalFDV"
    })
    public static class FeDeVidaBO {

        @XmlElement(name = "CUIL", required = true)
        protected BigInteger cuil;
        @XmlElement(required = true)
        protected String entidadReceptora;
        @XmlElement(required = true)
        protected BigInteger codigoTransaccion;
        @XmlElement(required = true)
        protected String tipoDoc;
        @XmlElement(required = true)
        protected BigInteger numDoc;
        @XmlElement(required = true)
        protected String entidadFDV;
        @XmlElement(required = true)
        protected String terminalFDV;

        /**
         * Obtiene el valor de la propiedad cuil.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCUIL() {
            return cuil;
        }

        /**
         * Define el valor de la propiedad cuil.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCUIL(BigInteger value) {
            this.cuil = value;
        }

        /**
         * Obtiene el valor de la propiedad entidadReceptora.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEntidadReceptora() {
            return entidadReceptora;
        }

        /**
         * Define el valor de la propiedad entidadReceptora.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEntidadReceptora(String value) {
            this.entidadReceptora = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoTransaccion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCodigoTransaccion() {
            return codigoTransaccion;
        }

        /**
         * Define el valor de la propiedad codigoTransaccion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCodigoTransaccion(BigInteger value) {
            this.codigoTransaccion = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoDoc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoDoc() {
            return tipoDoc;
        }

        /**
         * Define el valor de la propiedad tipoDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoDoc(String value) {
            this.tipoDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad numDoc.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNumDoc() {
            return numDoc;
        }

        /**
         * Define el valor de la propiedad numDoc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNumDoc(BigInteger value) {
            this.numDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad entidadFDV.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEntidadFDV() {
            return entidadFDV;
        }

        /**
         * Define el valor de la propiedad entidadFDV.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEntidadFDV(String value) {
            this.entidadFDV = value;
        }

        /**
         * Obtiene el valor de la propiedad terminalFDV.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTerminalFDV() {
            return terminalFDV;
        }

        /**
         * Define el valor de la propiedad terminalFDV.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTerminalFDV(String value) {
            this.terminalFDV = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="id" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="habDesbloqueo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "id",
        "habDesbloqueo"
    })
    public static class FeDeVidaCP {

        @XmlElement(required = true)
        protected IdClienteType id;
        protected Boolean habDesbloqueo;

        /**
         * Obtiene el valor de la propiedad id.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getId() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setId(IdClienteType value) {
            this.id = value;
        }

        /**
         * Obtiene el valor de la propiedad habDesbloqueo.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isHabDesbloqueo() {
            return habDesbloqueo;
        }

        /**
         * Define el valor de la propiedad habDesbloqueo.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setHabDesbloqueo(Boolean value) {
            this.habDesbloqueo = value;
        }

    }

}
