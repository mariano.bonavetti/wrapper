
package ar.com.supervielle.xsd.integracion.tarjetacredito.altacuenta_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altacuenta_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altacuenta_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaCuentaRespType }
     * 
     */
    public DataAltaCuentaRespType createDataAltaCuentaRespType() {
        return new DataAltaCuentaRespType();
    }

    /**
     * Create an instance of {@link DataAltaCuentaRespType.Row }
     * 
     */
    public DataAltaCuentaRespType.Row createDataAltaCuentaRespTypeRow() {
        return new DataAltaCuentaRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaCuentaRespType.Row.SolicitudesGeneradas }
     * 
     */
    public DataAltaCuentaRespType.Row.SolicitudesGeneradas createDataAltaCuentaRespTypeRowSolicitudesGeneradas() {
        return new DataAltaCuentaRespType.Row.SolicitudesGeneradas();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType }
     * 
     */
    public DataAltaCuentaReqType createDataAltaCuentaReqType() {
        return new DataAltaCuentaReqType();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa }
     * 
     */
    public DataAltaCuentaReqType.Empresa createDataAltaCuentaReqTypeEmpresa() {
        return new DataAltaCuentaReqType.Empresa();
    }

    /**
     * Create an instance of {@link ReqAltaCuenta }
     * 
     */
    public ReqAltaCuenta createReqAltaCuenta() {
        return new ReqAltaCuenta();
    }

    /**
     * Create an instance of {@link RespAltaCuenta }
     * 
     */
    public RespAltaCuenta createRespAltaCuenta() {
        return new RespAltaCuenta();
    }

    /**
     * Create an instance of {@link DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud }
     * 
     */
    public DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud createDataAltaCuentaRespTypeRowSolicitudesGeneradasSolicitud() {
        return new DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.IdentificadorSolicitud }
     * 
     */
    public DataAltaCuentaReqType.Empresa.IdentificadorSolicitud createDataAltaCuentaReqTypeEmpresaIdentificadorSolicitud() {
        return new DataAltaCuentaReqType.Empresa.IdentificadorSolicitud();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.Limites }
     * 
     */
    public DataAltaCuentaReqType.Empresa.Limites createDataAltaCuentaReqTypeEmpresaLimites() {
        return new DataAltaCuentaReqType.Empresa.Limites();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.RubrosHabilitados }
     * 
     */
    public DataAltaCuentaReqType.Empresa.RubrosHabilitados createDataAltaCuentaReqTypeEmpresaRubrosHabilitados() {
        return new DataAltaCuentaReqType.Empresa.RubrosHabilitados();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.RubrosInhabilitados }
     * 
     */
    public DataAltaCuentaReqType.Empresa.RubrosInhabilitados createDataAltaCuentaReqTypeEmpresaRubrosInhabilitados() {
        return new DataAltaCuentaReqType.Empresa.RubrosInhabilitados();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.Domicilio }
     * 
     */
    public DataAltaCuentaReqType.Empresa.Domicilio createDataAltaCuentaReqTypeEmpresaDomicilio() {
        return new DataAltaCuentaReqType.Empresa.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.CuentaDebito }
     * 
     */
    public DataAltaCuentaReqType.Empresa.CuentaDebito createDataAltaCuentaReqTypeEmpresaCuentaDebito() {
        return new DataAltaCuentaReqType.Empresa.CuentaDebito();
    }

    /**
     * Create an instance of {@link DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia }
     * 
     */
    public DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia createDataAltaCuentaReqTypeEmpresaDomicilioCorrespondencia() {
        return new DataAltaCuentaReqType.Empresa.DomicilioCorrespondencia();
    }

}
