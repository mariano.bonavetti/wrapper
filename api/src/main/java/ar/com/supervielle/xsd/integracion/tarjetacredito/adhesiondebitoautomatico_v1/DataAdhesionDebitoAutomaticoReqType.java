
package ar.com.supervielle.xsd.integracion.tarjetacredito.adhesiondebitoautomatico_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataAdhesionDebitoAutomaticoReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAdhesionDebitoAutomaticoReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cuentaTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cuentaDebito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoFormaDePago" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoTipoDebito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAdhesionDebitoAutomaticoReqType", propOrder = {
    "marca",
    "cuentaTarjeta",
    "cuentaDebito",
    "codigoFormaDePago",
    "codigoTipoDebito"
})
public class DataAdhesionDebitoAutomaticoReqType {

    @XmlElement(required = true)
    protected String marca;
    @XmlElement(required = true)
    protected String cuentaTarjeta;
    @XmlElement(required = true)
    protected String cuentaDebito;
    @XmlElement(required = true)
    protected String codigoFormaDePago;
    @XmlElement(required = true)
    protected String codigoTipoDebito;

    /**
     * Obtiene el valor de la propiedad marca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Define el valor de la propiedad marca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    /**
     * Define el valor de la propiedad cuentaTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaTarjeta(String value) {
        this.cuentaTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaDebito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaDebito() {
        return cuentaDebito;
    }

    /**
     * Define el valor de la propiedad cuentaDebito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaDebito(String value) {
        this.cuentaDebito = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoFormaDePago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoFormaDePago() {
        return codigoFormaDePago;
    }

    /**
     * Define el valor de la propiedad codigoFormaDePago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoFormaDePago(String value) {
        this.codigoFormaDePago = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoTipoDebito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTipoDebito() {
        return codigoTipoDebito;
    }

    /**
     * Define el valor de la propiedad codigoTipoDebito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTipoDebito(String value) {
        this.codigoTipoDebito = value;
    }

}
