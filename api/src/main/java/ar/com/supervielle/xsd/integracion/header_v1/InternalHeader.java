
package ar.com.supervielle.xsd.integracion.header_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para InternalHeader complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InternalHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="txID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;sequence>
 *           &lt;element name="requestData" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}RequestData"/>
 *           &lt;element name="responseData" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}ResponseData" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;element name="consumerReferences" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}EndpointReference">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="routeStack">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *                             &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
 *                             &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="messageContext" type="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}MessageContext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InternalHeader", propOrder = {
    "txID",
    "timestamp",
    "requestData",
    "responseData",
    "consumerReferences",
    "messageContext"
})
public class InternalHeader {

    @XmlElement(required = true)
    protected String txID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(required = true)
    protected RequestData requestData;
    protected ResponseData responseData;
    protected InternalHeader.ConsumerReferences consumerReferences;
    protected MessageContext messageContext;

    /**
     * Obtiene el valor de la propiedad txID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxID() {
        return txID;
    }

    /**
     * Define el valor de la propiedad txID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxID(String value) {
        this.txID = value;
    }

    /**
     * Obtiene el valor de la propiedad timestamp.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Define el valor de la propiedad timestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Obtiene el valor de la propiedad requestData.
     * 
     * @return
     *     possible object is
     *     {@link RequestData }
     *     
     */
    public RequestData getRequestData() {
        return requestData;
    }

    /**
     * Define el valor de la propiedad requestData.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestData }
     *     
     */
    public void setRequestData(RequestData value) {
        this.requestData = value;
    }

    /**
     * Obtiene el valor de la propiedad responseData.
     * 
     * @return
     *     possible object is
     *     {@link ResponseData }
     *     
     */
    public ResponseData getResponseData() {
        return responseData;
    }

    /**
     * Define el valor de la propiedad responseData.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseData }
     *     
     */
    public void setResponseData(ResponseData value) {
        this.responseData = value;
    }

    /**
     * Obtiene el valor de la propiedad consumerReferences.
     * 
     * @return
     *     possible object is
     *     {@link InternalHeader.ConsumerReferences }
     *     
     */
    public InternalHeader.ConsumerReferences getConsumerReferences() {
        return consumerReferences;
    }

    /**
     * Define el valor de la propiedad consumerReferences.
     * 
     * @param value
     *     allowed object is
     *     {@link InternalHeader.ConsumerReferences }
     *     
     */
    public void setConsumerReferences(InternalHeader.ConsumerReferences value) {
        this.consumerReferences = value;
    }

    /**
     * Obtiene el valor de la propiedad messageContext.
     * 
     * @return
     *     possible object is
     *     {@link MessageContext }
     *     
     */
    public MessageContext getMessageContext() {
        return messageContext;
    }

    /**
     * Define el valor de la propiedad messageContext.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageContext }
     *     
     */
    public void setMessageContext(MessageContext value) {
        this.messageContext = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.supervielle.com.ar/xsd/Integracion/Header-v1}EndpointReference">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="routeStack">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
     *                   &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
     *                   &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "routeStack"
    })
    public static class ConsumerReferences
        extends EndpointReference
    {

        @XmlElement(required = true)
        protected List<InternalHeader.ConsumerReferences.RouteStack> routeStack;

        /**
         * Gets the value of the routeStack property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the routeStack property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRouteStack().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link InternalHeader.ConsumerReferences.RouteStack }
         * 
         * 
         */
        public List<InternalHeader.ConsumerReferences.RouteStack> getRouteStack() {
            if (routeStack == null) {
                routeStack = new ArrayList<InternalHeader.ConsumerReferences.RouteStack>();
            }
            return this.routeStack;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
         *         &lt;element name="correlationId" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
         *         &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}hexBinary" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "address",
            "correlationId",
            "messageId"
        })
        public static class RouteStack {

            @XmlElement(required = true)
            @XmlSchemaType(name = "anyURI")
            protected String address;
            @XmlElement(type = String.class)
            @XmlJavaTypeAdapter(HexBinaryAdapter.class)
            @XmlSchemaType(name = "hexBinary")
            protected byte[] correlationId;
            @XmlElement(type = String.class)
            @XmlJavaTypeAdapter(HexBinaryAdapter.class)
            @XmlSchemaType(name = "hexBinary")
            protected byte[] messageId;

            /**
             * Obtiene el valor de la propiedad address.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddress() {
                return address;
            }

            /**
             * Define el valor de la propiedad address.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddress(String value) {
                this.address = value;
            }

            /**
             * Obtiene el valor de la propiedad correlationId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public byte[] getCorrelationId() {
                return correlationId;
            }

            /**
             * Define el valor de la propiedad correlationId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorrelationId(byte[] value) {
                this.correlationId = value;
            }

            /**
             * Obtiene el valor de la propiedad messageId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public byte[] getMessageId() {
                return messageId;
            }

            /**
             * Define el valor de la propiedad messageId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMessageId(byte[] value) {
                this.messageId = value;
            }

        }

    }

}
