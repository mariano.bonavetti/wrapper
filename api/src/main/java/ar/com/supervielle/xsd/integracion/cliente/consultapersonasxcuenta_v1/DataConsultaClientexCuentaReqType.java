
package ar.com.supervielle.xsd.integracion.cliente.consultapersonasxcuenta_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaClientexCuentaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaClientexCuentaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaClientexCuentaReqType", propOrder = {
    "codigoEmpresa",
    "numeroCuenta"
})
public class DataConsultaClientexCuentaReqType {

    protected int codigoEmpresa;
    @XmlElement(required = true)
    protected String numeroCuenta;

    /**
     * Obtiene el valor de la propiedad codigoEmpresa.
     * 
     */
    public int getCodigoEmpresa() {
        return codigoEmpresa;
    }

    /**
     * Define el valor de la propiedad codigoEmpresa.
     * 
     */
    public void setCodigoEmpresa(int value) {
        this.codigoEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Define el valor de la propiedad numeroCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCuenta(String value) {
        this.numeroCuenta = value;
    }

}
