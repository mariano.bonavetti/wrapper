
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasolicitudchequera_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaSolicitudChequeraRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaSolicitudChequeraRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="numeroSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="fechaPedido" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="cantChequeras" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="tipoChequera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="chequeInicial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="chequeFinal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="sucursalRetiro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="fechaRecepcion" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaSolicitudChequeraRespType", propOrder = {
    "row"
})
public class DataConsultaSolicitudChequeraRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaSolicitudChequeraRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaSolicitudChequeraRespType.Row }
     * 
     * 
     */
    public List<DataConsultaSolicitudChequeraRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaSolicitudChequeraRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="numeroSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="fechaPedido" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="cantChequeras" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="tipoChequera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="chequeInicial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="chequeFinal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="sucursalRetiro" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="fechaRecepcion" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "numeroSolicitud",
        "fechaPedido",
        "cantChequeras",
        "tipoChequera",
        "titular",
        "estado",
        "chequeInicial",
        "chequeFinal",
        "sucursalRetiro",
        "fechaRecepcion"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaBANTOTALType identificador;
        @XmlElement(required = true)
        protected String numeroSolicitud;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaPedido;
        protected Integer cantChequeras;
        @XmlElement(required = true)
        protected CodDescStringType tipoChequera;
        @XmlElement(required = true)
        protected String titular;
        @XmlElement(required = true)
        protected CodDescStringType estado;
        @XmlElement(required = true)
        protected String chequeInicial;
        @XmlElement(required = true)
        protected String chequeFinal;
        @XmlElement(required = true)
        protected CodDescStringType sucursalRetiro;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaRecepcion;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad numeroSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroSolicitud() {
            return numeroSolicitud;
        }

        /**
         * Define el valor de la propiedad numeroSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroSolicitud(String value) {
            this.numeroSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaPedido.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaPedido() {
            return fechaPedido;
        }

        /**
         * Define el valor de la propiedad fechaPedido.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaPedido(XMLGregorianCalendar value) {
            this.fechaPedido = value;
        }

        /**
         * Obtiene el valor de la propiedad cantChequeras.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCantChequeras() {
            return cantChequeras;
        }

        /**
         * Define el valor de la propiedad cantChequeras.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCantChequeras(Integer value) {
            this.cantChequeras = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoChequera.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipoChequera() {
            return tipoChequera;
        }

        /**
         * Define el valor de la propiedad tipoChequera.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipoChequera(CodDescStringType value) {
            this.tipoChequera = value;
        }

        /**
         * Obtiene el valor de la propiedad titular.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitular() {
            return titular;
        }

        /**
         * Define el valor de la propiedad titular.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitular(String value) {
            this.titular = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEstado(CodDescStringType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad chequeInicial.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChequeInicial() {
            return chequeInicial;
        }

        /**
         * Define el valor de la propiedad chequeInicial.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChequeInicial(String value) {
            this.chequeInicial = value;
        }

        /**
         * Obtiene el valor de la propiedad chequeFinal.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChequeFinal() {
            return chequeFinal;
        }

        /**
         * Define el valor de la propiedad chequeFinal.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChequeFinal(String value) {
            this.chequeFinal = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursalRetiro.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getSucursalRetiro() {
            return sucursalRetiro;
        }

        /**
         * Define el valor de la propiedad sucursalRetiro.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setSucursalRetiro(CodDescStringType value) {
            this.sucursalRetiro = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaRecepcion.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaRecepcion() {
            return fechaRecepcion;
        }

        /**
         * Define el valor de la propiedad fechaRecepcion.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaRecepcion(XMLGregorianCalendar value) {
            this.fechaRecepcion = value;
        }

    }

}
