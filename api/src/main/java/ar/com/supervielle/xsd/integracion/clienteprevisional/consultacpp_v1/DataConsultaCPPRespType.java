
package ar.com.supervielle.xsd.integracion.clienteprevisional.consultacpp_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdLiquidacionANSESType;


/**
 * <p>Clase Java para DataConsultaCPPRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCPPRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="nombreBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="idLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
 *                   &lt;element name="tipoImpresion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCPPRespType", propOrder = {
    "row"
})
public class DataConsultaCPPRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaCPPRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaCPPRespType.Row }
     * 
     * 
     */
    public List<DataConsultaCPPRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaCPPRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="nombreBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdLiquidacionANSESType"/>
     *         &lt;element name="tipoImpresion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "titularidad",
        "nombreBeneficiario",
        "idLiquidacion",
        "tipoImpresion"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaBANTOTALType identificador;
        @XmlElement(required = true)
        protected CodDescStringType titularidad;
        @XmlElement(required = true)
        protected String nombreBeneficiario;
        @XmlElement(required = true)
        protected IdLiquidacionANSESType idLiquidacion;
        @XmlElement(required = true)
        protected String tipoImpresion;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad titularidad.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTitularidad() {
            return titularidad;
        }

        /**
         * Define el valor de la propiedad titularidad.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTitularidad(CodDescStringType value) {
            this.titularidad = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreBeneficiario.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreBeneficiario() {
            return nombreBeneficiario;
        }

        /**
         * Define el valor de la propiedad nombreBeneficiario.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreBeneficiario(String value) {
            this.nombreBeneficiario = value;
        }

        /**
         * Obtiene el valor de la propiedad idLiquidacion.
         * 
         * @return
         *     possible object is
         *     {@link IdLiquidacionANSESType }
         *     
         */
        public IdLiquidacionANSESType getIdLiquidacion() {
            return idLiquidacion;
        }

        /**
         * Define el valor de la propiedad idLiquidacion.
         * 
         * @param value
         *     allowed object is
         *     {@link IdLiquidacionANSESType }
         *     
         */
        public void setIdLiquidacion(IdLiquidacionANSESType value) {
            this.idLiquidacion = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoImpresion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoImpresion() {
            return tipoImpresion;
        }

        /**
         * Define el valor de la propiedad tipoImpresion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoImpresion(String value) {
            this.tipoImpresion = value;
        }

    }

}
