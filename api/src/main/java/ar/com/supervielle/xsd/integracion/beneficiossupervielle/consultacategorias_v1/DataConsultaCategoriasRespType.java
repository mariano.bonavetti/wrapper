
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacategorias_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;


/**
 * <p>Clase Java para DataConsultaCategoriasRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCategoriasRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Categorias">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Categoria" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="datosCategoria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                       &lt;element name="ordenMenuWeb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="activa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                                       &lt;element name="descActiva" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCategoriasRespType", propOrder = {
    "row"
})
public class DataConsultaCategoriasRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaCategoriasRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaCategoriasRespType.Row }
     * 
     * 
     */
    public List<DataConsultaCategoriasRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaCategoriasRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Categorias">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Categoria" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="datosCategoria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                             &lt;element name="ordenMenuWeb" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="activa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                             &lt;element name="descActiva" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "categorias"
    })
    public static class Row {

        @XmlElement(name = "Categorias", required = true)
        protected DataConsultaCategoriasRespType.Row.Categorias categorias;

        /**
         * Obtiene el valor de la propiedad categorias.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaCategoriasRespType.Row.Categorias }
         *     
         */
        public DataConsultaCategoriasRespType.Row.Categorias getCategorias() {
            return categorias;
        }

        /**
         * Define el valor de la propiedad categorias.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaCategoriasRespType.Row.Categorias }
         *     
         */
        public void setCategorias(DataConsultaCategoriasRespType.Row.Categorias value) {
            this.categorias = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Categoria" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="datosCategoria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                   &lt;element name="ordenMenuWeb" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="activa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *                   &lt;element name="descActiva" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "categoria"
        })
        public static class Categorias {

            @XmlElement(name = "Categoria")
            protected List<DataConsultaCategoriasRespType.Row.Categorias.Categoria> categoria;

            /**
             * Gets the value of the categoria property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the categoria property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCategoria().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaCategoriasRespType.Row.Categorias.Categoria }
             * 
             * 
             */
            public List<DataConsultaCategoriasRespType.Row.Categorias.Categoria> getCategoria() {
                if (categoria == null) {
                    categoria = new ArrayList<DataConsultaCategoriasRespType.Row.Categorias.Categoria>();
                }
                return this.categoria;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="datosCategoria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *         &lt;element name="ordenMenuWeb" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="activa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
             *         &lt;element name="descActiva" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "datosCategoria",
                "ordenMenuWeb",
                "activa",
                "descActiva"
            })
            public static class Categoria {

                @XmlElement(required = true)
                protected CodDescStringType datosCategoria;
                @XmlElement(required = true)
                protected String ordenMenuWeb;
                protected boolean activa;
                @XmlElement(required = true)
                protected String descActiva;

                /**
                 * Obtiene el valor de la propiedad datosCategoria.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getDatosCategoria() {
                    return datosCategoria;
                }

                /**
                 * Define el valor de la propiedad datosCategoria.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setDatosCategoria(CodDescStringType value) {
                    this.datosCategoria = value;
                }

                /**
                 * Obtiene el valor de la propiedad ordenMenuWeb.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOrdenMenuWeb() {
                    return ordenMenuWeb;
                }

                /**
                 * Define el valor de la propiedad ordenMenuWeb.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOrdenMenuWeb(String value) {
                    this.ordenMenuWeb = value;
                }

                /**
                 * Obtiene el valor de la propiedad activa.
                 * 
                 */
                public boolean isActiva() {
                    return activa;
                }

                /**
                 * Define el valor de la propiedad activa.
                 * 
                 */
                public void setActiva(boolean value) {
                    this.activa = value;
                }

                /**
                 * Obtiene el valor de la propiedad descActiva.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescActiva() {
                    return descActiva;
                }

                /**
                 * Define el valor de la propiedad descActiva.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescActiva(String value) {
                    this.descActiva = value;
                }

            }

        }

    }

}
