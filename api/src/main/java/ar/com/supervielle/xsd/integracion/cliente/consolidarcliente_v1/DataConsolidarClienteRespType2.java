
package ar.com.supervielle.xsd.integracion.cliente.consolidarcliente_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsolidarClienteRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsolidarClienteRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="informacionCltResponse" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsolidarClienteRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cliente/consolidarCliente-v1.1", propOrder = {
    "row"
})
public class DataConsolidarClienteRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsolidarClienteRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsolidarClienteRespType2 .Row }
     * 
     * 
     */
    public List<DataConsolidarClienteRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsolidarClienteRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="informacionCltResponse" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "informacionCltResponse"
    })
    public static class Row {

        @XmlElement(required = true)
        protected Object informacionCltResponse;

        /**
         * Obtiene el valor de la propiedad informacionCltResponse.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getInformacionCltResponse() {
            return informacionCltResponse;
        }

        /**
         * Define el valor de la propiedad informacionCltResponse.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setInformacionCltResponse(Object value) {
            this.informacionCltResponse = value;
        }

    }

}
