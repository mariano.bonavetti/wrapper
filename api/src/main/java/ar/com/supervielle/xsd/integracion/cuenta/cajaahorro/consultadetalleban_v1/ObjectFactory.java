
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetalleban_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetalleban_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultadetalleban_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaDetalleBANRespType }
     * 
     */
    public DataConsultaDetalleBANRespType createDataConsultaDetalleBANRespType() {
        return new DataConsultaDetalleBANRespType();
    }

    /**
     * Create an instance of {@link RespConsultaDetalleBAN }
     * 
     */
    public RespConsultaDetalleBAN createRespConsultaDetalleBAN() {
        return new RespConsultaDetalleBAN();
    }

    /**
     * Create an instance of {@link ReqConsultaDetalleBAN }
     * 
     */
    public ReqConsultaDetalleBAN createReqConsultaDetalleBAN() {
        return new ReqConsultaDetalleBAN();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleBANReqType }
     * 
     */
    public DataConsultaDetalleBANReqType createDataConsultaDetalleBANReqType() {
        return new DataConsultaDetalleBANReqType();
    }

    /**
     * Create an instance of {@link DataConsultaDetalleBANRespType.Row }
     * 
     */
    public DataConsultaDetalleBANRespType.Row createDataConsultaDetalleBANRespTypeRow() {
        return new DataConsultaDetalleBANRespType.Row();
    }

}
