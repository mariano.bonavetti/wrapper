
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultaordendecuenta_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaOrdenDeCuentaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaOrdenDeCuentaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="orden" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="firmantes" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="firmante" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                                       &lt;element name="nombreyApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaOrdenDeCuentaRespType", propOrder = {
    "row"
})
public class DataConsultaOrdenDeCuentaRespType {

    @XmlElement(name = "Row")
    protected DataConsultaOrdenDeCuentaRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaOrdenDeCuentaRespType.Row }
     *     
     */
    public DataConsultaOrdenDeCuentaRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaOrdenDeCuentaRespType.Row }
     *     
     */
    public void setRow(DataConsultaOrdenDeCuentaRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="orden" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="firmantes" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="firmante" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                             &lt;element name="nombreyApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "orden",
        "firmantes"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaBANTOTALType identificador;
        @XmlElement(required = true)
        protected CodDescNumType orden;
        protected DataConsultaOrdenDeCuentaRespType.Row.Firmantes firmantes;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad orden.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getOrden() {
            return orden;
        }

        /**
         * Define el valor de la propiedad orden.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setOrden(CodDescNumType value) {
            this.orden = value;
        }

        /**
         * Obtiene el valor de la propiedad firmantes.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaOrdenDeCuentaRespType.Row.Firmantes }
         *     
         */
        public DataConsultaOrdenDeCuentaRespType.Row.Firmantes getFirmantes() {
            return firmantes;
        }

        /**
         * Define el valor de la propiedad firmantes.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaOrdenDeCuentaRespType.Row.Firmantes }
         *     
         */
        public void setFirmantes(DataConsultaOrdenDeCuentaRespType.Row.Firmantes value) {
            this.firmantes = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="firmante" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *                   &lt;element name="nombreyApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "firmante"
        })
        public static class Firmantes {

            @XmlElement(required = true)
            protected List<DataConsultaOrdenDeCuentaRespType.Row.Firmantes.Firmante> firmante;

            /**
             * Gets the value of the firmante property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the firmante property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFirmante().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaOrdenDeCuentaRespType.Row.Firmantes.Firmante }
             * 
             * 
             */
            public List<DataConsultaOrdenDeCuentaRespType.Row.Firmantes.Firmante> getFirmante() {
                if (firmante == null) {
                    firmante = new ArrayList<DataConsultaOrdenDeCuentaRespType.Row.Firmantes.Firmante>();
                }
                return this.firmante;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
             *         &lt;element name="nombreyApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "identificador",
                "nombreyApellido"
            })
            public static class Firmante {

                @XmlElement(required = true)
                protected IdClienteType identificador;
                @XmlElement(required = true)
                protected String nombreyApellido;

                /**
                 * Obtiene el valor de la propiedad identificador.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IdClienteType }
                 *     
                 */
                public IdClienteType getIdentificador() {
                    return identificador;
                }

                /**
                 * Define el valor de la propiedad identificador.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IdClienteType }
                 *     
                 */
                public void setIdentificador(IdClienteType value) {
                    this.identificador = value;
                }

                /**
                 * Obtiene el valor de la propiedad nombreyApellido.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNombreyApellido() {
                    return nombreyApellido;
                }

                /**
                 * Define el valor de la propiedad nombreyApellido.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNombreyApellido(String value) {
                    this.nombreyApellido = value;
                }

            }

        }

    }

}
