
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultaschdextractosban_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaSchdExtractosBANRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaSchdExtractosBANRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                   &lt;element name="frecuenciaExtracto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="tipoExtracto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaSchdExtractosBANRespType", propOrder = {
    "row"
})
public class DataConsultaSchdExtractosBANRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaSchdExtractosBANRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaSchdExtractosBANRespType.Row }
     * 
     * 
     */
    public List<DataConsultaSchdExtractosBANRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaSchdExtractosBANRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *         &lt;element name="frecuenciaExtracto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="tipoExtracto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "frecuenciaExtracto",
        "tipoExtracto"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaBANTOTALType identificador;
        @XmlElement(required = true)
        protected CodDescStringType frecuenciaExtracto;
        @XmlElement(required = true)
        protected CodDescStringType tipoExtracto;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public IdCuentaBANTOTALType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaBANTOTALType }
         *     
         */
        public void setIdentificador(IdCuentaBANTOTALType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad frecuenciaExtracto.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getFrecuenciaExtracto() {
            return frecuenciaExtracto;
        }

        /**
         * Define el valor de la propiedad frecuenciaExtracto.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setFrecuenciaExtracto(CodDescStringType value) {
            this.frecuenciaExtracto = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoExtracto.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getTipoExtracto() {
            return tipoExtracto;
        }

        /**
         * Define el valor de la propiedad tipoExtracto.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setTipoExtracto(CodDescStringType value) {
            this.tipoExtracto = value;
        }

    }

}
