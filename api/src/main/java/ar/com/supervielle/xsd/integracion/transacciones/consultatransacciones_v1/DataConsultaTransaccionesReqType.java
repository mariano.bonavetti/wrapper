
package ar.com.supervielle.xsd.integracion.transacciones.consultatransacciones_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaTransaccionesReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaTransaccionesReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaTransaccionesReqType", propOrder = {
    "identificador"
})
public class DataConsultaTransaccionesReqType {

    @XmlElement(required = true)
    protected DataConsultaTransaccionesReqType.Identificador identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaTransaccionesReqType.Identificador }
     *     
     */
    public DataConsultaTransaccionesReqType.Identificador getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaTransaccionesReqType.Identificador }
     *     
     */
    public void setIdentificador(DataConsultaTransaccionesReqType.Identificador value) {
        this.identificador = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txID",
        "source",
        "servicio",
        "operacion"
    })
    public static class Identificador {

        @XmlElement(required = true)
        protected String txID;
        @XmlElementRef(name = "source", type = JAXBElement.class, required = false)
        protected JAXBElement<String> source;
        @XmlElement(required = true)
        protected String servicio;
        @XmlElement(required = true)
        protected String operacion;

        /**
         * Obtiene el valor de la propiedad txID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTxID() {
            return txID;
        }

        /**
         * Define el valor de la propiedad txID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTxID(String value) {
            this.txID = value;
        }

        /**
         * Obtiene el valor de la propiedad source.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSource() {
            return source;
        }

        /**
         * Define el valor de la propiedad source.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSource(JAXBElement<String> value) {
            this.source = value;
        }

        /**
         * Obtiene el valor de la propiedad servicio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServicio() {
            return servicio;
        }

        /**
         * Define el valor de la propiedad servicio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServicio(String value) {
            this.servicio = value;
        }

        /**
         * Obtiene el valor de la propiedad operacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperacion() {
            return operacion;
        }

        /**
         * Define el valor de la propiedad operacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperacion(String value) {
            this.operacion = value;
        }

    }

}
