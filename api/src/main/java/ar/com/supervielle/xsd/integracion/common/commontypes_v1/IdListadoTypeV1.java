
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para IdListadoType_v1 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="IdListadoType_v1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *         &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="modulo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *         &lt;element name="moneda">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="cod" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="papel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estadoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *         &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *         &lt;element name="FecAlta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FecUltMov" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Orden" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *         &lt;element name="cbu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdListadoType_v1", propOrder = {
    "sucursal",
    "cuentaCliente",
    "subOperacion",
    "modulo",
    "moneda",
    "tipoOperacion",
    "tipoCuenta",
    "operacion",
    "papel",
    "estadoCuenta",
    "saldo",
    "titularidad",
    "paquete",
    "fecAlta",
    "fecUltMov",
    "orden",
    "cbu"
})
public class IdListadoTypeV1 {

    @XmlElement(required = true)
    protected CodDescStringType sucursal;
    @XmlElement(required = true)
    protected String cuentaCliente;
    @XmlElement(required = true)
    protected String subOperacion;
    @XmlElement(required = true)
    protected CodDescStringType modulo;
    @XmlElement(required = true)
    protected IdListadoTypeV1 .Moneda moneda;
    @XmlElement(required = true)
    protected String tipoOperacion;
    @XmlElement(required = true)
    protected String tipoCuenta;
    @XmlElement(required = true)
    protected String operacion;
    @XmlElement(required = true)
    protected String papel;
    @XmlElement(required = true)
    protected CodDescStringType estadoCuenta;
    protected BigDecimal saldo;
    @XmlElement(required = true)
    protected CodDescStringType titularidad;
    @XmlElement(required = true)
    protected CodDescStringType paquete;
    @XmlElement(name = "FecAlta", required = true)
    protected String fecAlta;
    @XmlElement(name = "FecUltMov", required = true)
    protected String fecUltMov;
    @XmlElement(name = "Orden", required = true)
    protected CodDescStringType orden;
    @XmlElement(required = true)
    protected String cbu;

    /**
     * Obtiene el valor de la propiedad sucursal.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getSucursal() {
        return sucursal;
    }

    /**
     * Define el valor de la propiedad sucursal.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setSucursal(CodDescStringType value) {
        this.sucursal = value;
    }

    /**
     * Obtiene el valor de la propiedad cuentaCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuentaCliente() {
        return cuentaCliente;
    }

    /**
     * Define el valor de la propiedad cuentaCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuentaCliente(String value) {
        this.cuentaCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad subOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubOperacion() {
        return subOperacion;
    }

    /**
     * Define el valor de la propiedad subOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubOperacion(String value) {
        this.subOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad modulo.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getModulo() {
        return modulo;
    }

    /**
     * Define el valor de la propiedad modulo.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setModulo(CodDescStringType value) {
        this.modulo = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link IdListadoTypeV1 .Moneda }
     *     
     */
    public IdListadoTypeV1 .Moneda getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link IdListadoTypeV1 .Moneda }
     *     
     */
    public void setMoneda(IdListadoTypeV1 .Moneda value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperacion(String value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Define el valor de la propiedad tipoCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCuenta(String value) {
        this.tipoCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad papel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPapel() {
        return papel;
    }

    /**
     * Define el valor de la propiedad papel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPapel(String value) {
        this.papel = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCuenta.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getEstadoCuenta() {
        return estadoCuenta;
    }

    /**
     * Define el valor de la propiedad estadoCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setEstadoCuenta(CodDescStringType value) {
        this.estadoCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad saldo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaldo() {
        return saldo;
    }

    /**
     * Define el valor de la propiedad saldo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaldo(BigDecimal value) {
        this.saldo = value;
    }

    /**
     * Obtiene el valor de la propiedad titularidad.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getTitularidad() {
        return titularidad;
    }

    /**
     * Define el valor de la propiedad titularidad.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setTitularidad(CodDescStringType value) {
        this.titularidad = value;
    }

    /**
     * Obtiene el valor de la propiedad paquete.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getPaquete() {
        return paquete;
    }

    /**
     * Define el valor de la propiedad paquete.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setPaquete(CodDescStringType value) {
        this.paquete = value;
    }

    /**
     * Obtiene el valor de la propiedad fecAlta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecAlta() {
        return fecAlta;
    }

    /**
     * Define el valor de la propiedad fecAlta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecAlta(String value) {
        this.fecAlta = value;
    }

    /**
     * Obtiene el valor de la propiedad fecUltMov.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecUltMov() {
        return fecUltMov;
    }

    /**
     * Define el valor de la propiedad fecUltMov.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecUltMov(String value) {
        this.fecUltMov = value;
    }

    /**
     * Obtiene el valor de la propiedad orden.
     * 
     * @return
     *     possible object is
     *     {@link CodDescStringType }
     *     
     */
    public CodDescStringType getOrden() {
        return orden;
    }

    /**
     * Define el valor de la propiedad orden.
     * 
     * @param value
     *     allowed object is
     *     {@link CodDescStringType }
     *     
     */
    public void setOrden(CodDescStringType value) {
        this.orden = value;
    }

    /**
     * Obtiene el valor de la propiedad cbu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbu() {
        return cbu;
    }

    /**
     * Define el valor de la propiedad cbu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbu(String value) {
        this.cbu = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="cod" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cod",
        "simbolo",
        "nombre"
    })
    public static class Moneda {

        @XmlElement(required = true)
        protected Object cod;
        @XmlElement(required = true)
        protected Object simbolo;
        @XmlElement(required = true)
        protected Object nombre;

        /**
         * Obtiene el valor de la propiedad cod.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCod() {
            return cod;
        }

        /**
         * Define el valor de la propiedad cod.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCod(Object value) {
            this.cod = value;
        }

        /**
         * Obtiene el valor de la propiedad simbolo.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSimbolo() {
            return simbolo;
        }

        /**
         * Define el valor de la propiedad simbolo.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSimbolo(Object value) {
            this.simbolo = value;
        }

        /**
         * Obtiene el valor de la propiedad nombre.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getNombre() {
            return nombre;
        }

        /**
         * Define el valor de la propiedad nombre.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setNombre(Object value) {
            this.nombre = value;
        }

    }

}
