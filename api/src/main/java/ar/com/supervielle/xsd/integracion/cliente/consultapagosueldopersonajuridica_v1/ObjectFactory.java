
package ar.com.supervielle.xsd.integracion.cliente.consultapagosueldopersonajuridica_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.consultapagosueldopersonajuridica_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.consultapagosueldopersonajuridica_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType createDataConsultaPagoSueldoPersonaJuridicaRespType() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row createDataConsultaPagoSueldoPersonaJuridicaRespTypeRow() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores createDataConsultaPagoSueldoPersonaJuridicaRespTypeRowSectores() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector createDataConsultaPagoSueldoPersonaJuridicaRespTypeRowSectoresSector() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios createDataConsultaPagoSueldoPersonaJuridicaRespTypeRowSectoresSectorConvenvenios() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio createDataConsultaPagoSueldoPersonaJuridicaRespTypeRowSectoresSectorConvenveniosConvenio() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas createDataConsultaPagoSueldoPersonaJuridicaRespTypeRowSectoresSectorConvenveniosConvenioCuentas() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas();
    }

    /**
     * Create an instance of {@link ReqConsultaPagoSueldoPersonaJuridica }
     * 
     */
    public ReqConsultaPagoSueldoPersonaJuridica createReqConsultaPagoSueldoPersonaJuridica() {
        return new ReqConsultaPagoSueldoPersonaJuridica();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaReqType }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaReqType createDataConsultaPagoSueldoPersonaJuridicaReqType() {
        return new DataConsultaPagoSueldoPersonaJuridicaReqType();
    }

    /**
     * Create an instance of {@link RespConsultaPagoSueldoPersonaJuridica }
     * 
     */
    public RespConsultaPagoSueldoPersonaJuridica createRespConsultaPagoSueldoPersonaJuridica() {
        return new RespConsultaPagoSueldoPersonaJuridica();
    }

    /**
     * Create an instance of {@link DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta }
     * 
     */
    public DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta createDataConsultaPagoSueldoPersonaJuridicaRespTypeRowSectoresSectorConvenveniosConvenioCuentasCuenta() {
        return new DataConsultaPagoSueldoPersonaJuridicaRespType.Row.Sectores.Sector.Convenvenios.Convenio.Cuentas.Cuenta();
    }

}
