
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultadetalle_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaDetalleRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDetalleRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nombreDescriptivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Caracteristicas">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Segmento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                             &lt;element name="Clasificacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                             &lt;element name="Sector" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="cbu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Moneda">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TipoProducto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="Paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
 *                   &lt;element name="acuerdo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="Ejecutivo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="fechaUltimoMov" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                   &lt;element name="DebitosAutAdhesiones">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Entidad" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Firmas">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Firmante" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                                       &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Integrantes">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Integrante" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                                       &lt;element name="NombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AtributosExtendidos">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDetalleRespType", propOrder = {
    "row"
})
public class DataConsultaDetalleRespType {

    @XmlElement(name = "Row")
    protected DataConsultaDetalleRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaDetalleRespType.Row }
     *     
     */
    public DataConsultaDetalleRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaDetalleRespType.Row }
     *     
     */
    public void setRow(DataConsultaDetalleRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nombreDescriptivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Caracteristicas">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Segmento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                   &lt;element name="Clasificacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                   &lt;element name="Sector" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="cbu" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Vinculo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Moneda">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TipoProducto" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="Paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType" minOccurs="0"/>
     *         &lt;element name="acuerdo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="Ejecutivo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="fechaUltimoMov" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *         &lt;element name="DebitosAutAdhesiones">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Entidad" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Firmas">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Firmante" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                             &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Integrantes">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Integrante" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                             &lt;element name="NombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AtributosExtendidos">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nombreDescriptivo",
        "caracteristicas",
        "sucursal",
        "cbu",
        "vinculo",
        "moneda",
        "tipoProducto",
        "paquete",
        "acuerdo",
        "saldo",
        "ejecutivo",
        "fechaUltimoMov",
        "fechaAlta",
        "fechaBaja",
        "estado",
        "debitosAutAdhesiones",
        "firmas",
        "integrantes",
        "atributosExtendidos"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String nombreDescriptivo;
        @XmlElement(name = "Caracteristicas", required = true)
        protected DataConsultaDetalleRespType.Row.Caracteristicas caracteristicas;
        @XmlElement(name = "Sucursal", required = true)
        protected CodDescNumType sucursal;
        @XmlElement(required = true)
        protected String cbu;
        @XmlElement(name = "Vinculo", required = true)
        protected CodDescNumType vinculo;
        @XmlElement(name = "Moneda", required = true)
        protected DataConsultaDetalleRespType.Row.Moneda moneda;
        @XmlElement(name = "TipoProducto", required = true)
        protected CodDescNumType tipoProducto;
        @XmlElement(name = "Paquete")
        protected CodDescNumType paquete;
        protected BigDecimal acuerdo;
        @XmlElement(required = true)
        protected BigDecimal saldo;
        @XmlElement(name = "Ejecutivo", required = true)
        protected CodDescStringType ejecutivo;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaUltimoMov;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaAlta;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaBaja;
        @XmlElement(name = "Estado", required = true)
        protected CodDescNumType estado;
        @XmlElement(name = "DebitosAutAdhesiones", required = true)
        protected DataConsultaDetalleRespType.Row.DebitosAutAdhesiones debitosAutAdhesiones;
        @XmlElement(name = "Firmas", required = true)
        protected DataConsultaDetalleRespType.Row.Firmas firmas;
        @XmlElement(name = "Integrantes", required = true)
        protected DataConsultaDetalleRespType.Row.Integrantes integrantes;
        @XmlElement(name = "AtributosExtendidos", required = true)
        protected DataConsultaDetalleRespType.Row.AtributosExtendidos atributosExtendidos;

        /**
         * Obtiene el valor de la propiedad nombreDescriptivo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreDescriptivo() {
            return nombreDescriptivo;
        }

        /**
         * Define el valor de la propiedad nombreDescriptivo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreDescriptivo(String value) {
            this.nombreDescriptivo = value;
        }

        /**
         * Obtiene el valor de la propiedad caracteristicas.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.Caracteristicas }
         *     
         */
        public DataConsultaDetalleRespType.Row.Caracteristicas getCaracteristicas() {
            return caracteristicas;
        }

        /**
         * Define el valor de la propiedad caracteristicas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.Caracteristicas }
         *     
         */
        public void setCaracteristicas(DataConsultaDetalleRespType.Row.Caracteristicas value) {
            this.caracteristicas = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setSucursal(CodDescNumType value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad cbu.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCbu() {
            return cbu;
        }

        /**
         * Define el valor de la propiedad cbu.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCbu(String value) {
            this.cbu = value;
        }

        /**
         * Obtiene el valor de la propiedad vinculo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getVinculo() {
            return vinculo;
        }

        /**
         * Define el valor de la propiedad vinculo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setVinculo(CodDescNumType value) {
            this.vinculo = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.Moneda }
         *     
         */
        public DataConsultaDetalleRespType.Row.Moneda getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.Moneda }
         *     
         */
        public void setMoneda(DataConsultaDetalleRespType.Row.Moneda value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoProducto.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getTipoProducto() {
            return tipoProducto;
        }

        /**
         * Define el valor de la propiedad tipoProducto.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setTipoProducto(CodDescNumType value) {
            this.tipoProducto = value;
        }

        /**
         * Obtiene el valor de la propiedad paquete.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getPaquete() {
            return paquete;
        }

        /**
         * Define el valor de la propiedad paquete.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setPaquete(CodDescNumType value) {
            this.paquete = value;
        }

        /**
         * Obtiene el valor de la propiedad acuerdo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAcuerdo() {
            return acuerdo;
        }

        /**
         * Define el valor de la propiedad acuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAcuerdo(BigDecimal value) {
            this.acuerdo = value;
        }

        /**
         * Obtiene el valor de la propiedad saldo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldo() {
            return saldo;
        }

        /**
         * Define el valor de la propiedad saldo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldo(BigDecimal value) {
            this.saldo = value;
        }

        /**
         * Obtiene el valor de la propiedad ejecutivo.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getEjecutivo() {
            return ejecutivo;
        }

        /**
         * Define el valor de la propiedad ejecutivo.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setEjecutivo(CodDescStringType value) {
            this.ejecutivo = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaUltimoMov.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaUltimoMov() {
            return fechaUltimoMov;
        }

        /**
         * Define el valor de la propiedad fechaUltimoMov.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaUltimoMov(XMLGregorianCalendar value) {
            this.fechaUltimoMov = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaAlta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaAlta() {
            return fechaAlta;
        }

        /**
         * Define el valor de la propiedad fechaAlta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaAlta(XMLGregorianCalendar value) {
            this.fechaAlta = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaBaja.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaBaja() {
            return fechaBaja;
        }

        /**
         * Define el valor de la propiedad fechaBaja.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaBaja(XMLGregorianCalendar value) {
            this.fechaBaja = value;
        }

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link CodDescNumType }
         *     
         */
        public CodDescNumType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescNumType }
         *     
         */
        public void setEstado(CodDescNumType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad debitosAutAdhesiones.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.DebitosAutAdhesiones }
         *     
         */
        public DataConsultaDetalleRespType.Row.DebitosAutAdhesiones getDebitosAutAdhesiones() {
            return debitosAutAdhesiones;
        }

        /**
         * Define el valor de la propiedad debitosAutAdhesiones.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.DebitosAutAdhesiones }
         *     
         */
        public void setDebitosAutAdhesiones(DataConsultaDetalleRespType.Row.DebitosAutAdhesiones value) {
            this.debitosAutAdhesiones = value;
        }

        /**
         * Obtiene el valor de la propiedad firmas.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.Firmas }
         *     
         */
        public DataConsultaDetalleRespType.Row.Firmas getFirmas() {
            return firmas;
        }

        /**
         * Define el valor de la propiedad firmas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.Firmas }
         *     
         */
        public void setFirmas(DataConsultaDetalleRespType.Row.Firmas value) {
            this.firmas = value;
        }

        /**
         * Obtiene el valor de la propiedad integrantes.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.Integrantes }
         *     
         */
        public DataConsultaDetalleRespType.Row.Integrantes getIntegrantes() {
            return integrantes;
        }

        /**
         * Define el valor de la propiedad integrantes.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.Integrantes }
         *     
         */
        public void setIntegrantes(DataConsultaDetalleRespType.Row.Integrantes value) {
            this.integrantes = value;
        }

        /**
         * Obtiene el valor de la propiedad atributosExtendidos.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaDetalleRespType.Row.AtributosExtendidos }
         *     
         */
        public DataConsultaDetalleRespType.Row.AtributosExtendidos getAtributosExtendidos() {
            return atributosExtendidos;
        }

        /**
         * Define el valor de la propiedad atributosExtendidos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaDetalleRespType.Row.AtributosExtendidos }
         *     
         */
        public void setAtributosExtendidos(DataConsultaDetalleRespType.Row.AtributosExtendidos value) {
            this.atributosExtendidos = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="atributo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "atributo"
        })
        public static class AtributosExtendidos {

            protected List<DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo> atributo;

            /**
             * Gets the value of the atributo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the atributo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAtributo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo }
             * 
             * 
             */
            public List<DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo> getAtributo() {
                if (atributo == null) {
                    atributo = new ArrayList<DataConsultaDetalleRespType.Row.AtributosExtendidos.Atributo>();
                }
                return this.atributo;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Atributo {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "id")
                protected String id;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad id.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getId() {
                    return id;
                }

                /**
                 * Define el valor de la propiedad id.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setId(String value) {
                    this.id = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Segmento" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *         &lt;element name="Clasificacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *         &lt;element name="Sector" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "segmento",
            "clasificacion",
            "sector"
        })
        public static class Caracteristicas {

            @XmlElement(name = "Segmento", required = true)
            protected CodDescNumType segmento;
            @XmlElement(name = "Clasificacion", required = true)
            protected CodDescNumType clasificacion;
            @XmlElement(name = "Sector", required = true)
            protected CodDescNumType sector;

            /**
             * Obtiene el valor de la propiedad segmento.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getSegmento() {
                return segmento;
            }

            /**
             * Define el valor de la propiedad segmento.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setSegmento(CodDescNumType value) {
                this.segmento = value;
            }

            /**
             * Obtiene el valor de la propiedad clasificacion.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getClasificacion() {
                return clasificacion;
            }

            /**
             * Define el valor de la propiedad clasificacion.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setClasificacion(CodDescNumType value) {
                this.clasificacion = value;
            }

            /**
             * Obtiene el valor de la propiedad sector.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getSector() {
                return sector;
            }

            /**
             * Define el valor de la propiedad sector.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setSector(CodDescNumType value) {
                this.sector = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Entidad" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "entidad"
        })
        public static class DebitosAutAdhesiones {

            @XmlElement(name = "Entidad")
            protected List<DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad> entidad;

            /**
             * Gets the value of the entidad property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the entidad property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEntidad().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad }
             * 
             * 
             */
            public List<DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad> getEntidad() {
                if (entidad == null) {
                    entidad = new ArrayList<DataConsultaDetalleRespType.Row.DebitosAutAdhesiones.Entidad>();
                }
                return this.entidad;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "cuit",
                "razonSocial"
            })
            public static class Entidad {

                @XmlElement(required = true)
                protected String cuit;
                @XmlElement(required = true)
                protected String razonSocial;

                /**
                 * Obtiene el valor de la propiedad cuit.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCuit() {
                    return cuit;
                }

                /**
                 * Define el valor de la propiedad cuit.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCuit(String value) {
                    this.cuit = value;
                }

                /**
                 * Obtiene el valor de la propiedad razonSocial.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRazonSocial() {
                    return razonSocial;
                }

                /**
                 * Define el valor de la propiedad razonSocial.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRazonSocial(String value) {
                    this.razonSocial = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Firmante" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *                   &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "firmante"
        })
        public static class Firmas {

            @XmlElement(name = "Firmante")
            protected List<DataConsultaDetalleRespType.Row.Firmas.Firmante> firmante;

            /**
             * Gets the value of the firmante property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the firmante property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFirmante().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaDetalleRespType.Row.Firmas.Firmante }
             * 
             * 
             */
            public List<DataConsultaDetalleRespType.Row.Firmas.Firmante> getFirmante() {
                if (firmante == null) {
                    firmante = new ArrayList<DataConsultaDetalleRespType.Row.Firmas.Firmante>();
                }
                return this.firmante;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
             *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "tipo",
                "idPersona",
                "nombre",
                "descripcion"
            })
            public static class Firmante {

                @XmlElement(name = "Tipo", required = true)
                protected CodDescNumType tipo;
                @XmlElement(name = "IdPersona", required = true)
                protected IdClienteType idPersona;
                @XmlElement(required = true)
                protected String nombre;
                @XmlElement(required = true)
                protected String descripcion;

                /**
                 * Obtiene el valor de la propiedad tipo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getTipo() {
                    return tipo;
                }

                /**
                 * Define el valor de la propiedad tipo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setTipo(CodDescNumType value) {
                    this.tipo = value;
                }

                /**
                 * Obtiene el valor de la propiedad idPersona.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IdClienteType }
                 *     
                 */
                public IdClienteType getIdPersona() {
                    return idPersona;
                }

                /**
                 * Define el valor de la propiedad idPersona.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IdClienteType }
                 *     
                 */
                public void setIdPersona(IdClienteType value) {
                    this.idPersona = value;
                }

                /**
                 * Obtiene el valor de la propiedad nombre.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNombre() {
                    return nombre;
                }

                /**
                 * Define el valor de la propiedad nombre.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNombre(String value) {
                    this.nombre = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Integrante" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *                   &lt;element name="NombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "integrante"
        })
        public static class Integrantes {

            @XmlElement(name = "Integrante", required = true)
            protected List<DataConsultaDetalleRespType.Row.Integrantes.Integrante> integrante;

            /**
             * Gets the value of the integrante property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the integrante property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getIntegrante().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaDetalleRespType.Row.Integrantes.Integrante }
             * 
             * 
             */
            public List<DataConsultaDetalleRespType.Row.Integrantes.Integrante> getIntegrante() {
                if (integrante == null) {
                    integrante = new ArrayList<DataConsultaDetalleRespType.Row.Integrantes.Integrante>();
                }
                return this.integrante;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="IdPersona" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
             *         &lt;element name="NombreApellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Titularidad" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idPersona",
                "nombreApellido",
                "titularidad"
            })
            public static class Integrante {

                @XmlElement(name = "IdPersona", required = true)
                protected IdClienteType idPersona;
                @XmlElement(name = "NombreApellido", required = true)
                protected String nombreApellido;
                @XmlElement(name = "Titularidad", required = true)
                protected CodDescNumType titularidad;

                /**
                 * Obtiene el valor de la propiedad idPersona.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IdClienteType }
                 *     
                 */
                public IdClienteType getIdPersona() {
                    return idPersona;
                }

                /**
                 * Define el valor de la propiedad idPersona.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IdClienteType }
                 *     
                 */
                public void setIdPersona(IdClienteType value) {
                    this.idPersona = value;
                }

                /**
                 * Obtiene el valor de la propiedad nombreApellido.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNombreApellido() {
                    return nombreApellido;
                }

                /**
                 * Define el valor de la propiedad nombreApellido.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNombreApellido(String value) {
                    this.nombreApellido = value;
                }

                /**
                 * Obtiene el valor de la propiedad titularidad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getTitularidad() {
                    return titularidad;
                }

                /**
                 * Define el valor de la propiedad titularidad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setTitularidad(CodDescNumType value) {
                    this.titularidad = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="simbolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigo",
            "simbolo",
            "descripcion"
        })
        public static class Moneda {

            protected int codigo;
            @XmlElement(required = true)
            protected String simbolo;
            @XmlElement(required = true)
            protected String descripcion;

            /**
             * Obtiene el valor de la propiedad codigo.
             * 
             */
            public int getCodigo() {
                return codigo;
            }

            /**
             * Define el valor de la propiedad codigo.
             * 
             */
            public void setCodigo(int value) {
                this.codigo = value;
            }

            /**
             * Obtiene el valor de la propiedad simbolo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSimbolo() {
                return simbolo;
            }

            /**
             * Define el valor de la propiedad simbolo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSimbolo(String value) {
                this.simbolo = value;
            }

            /**
             * Obtiene el valor de la propiedad descripcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Define el valor de la propiedad descripcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

        }

    }

}
