
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultatasasmaster_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaTasasMasterRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaTasasMasterRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tasaEfectMensLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaEfectMensDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaDiariaFinanLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaDiariaFinanDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaNomAnualLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaNomAnualDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaIntCompLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaIntCompDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaDiariaPunitLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="tasaIntPunitDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaTasasMasterRespType", propOrder = {
    "row"
})
public class DataConsultaTasasMasterRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaTasasMasterRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaTasasMasterRespType.Row }
     * 
     * 
     */
    public List<DataConsultaTasasMasterRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaTasasMasterRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tasaEfectMensLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaEfectMensDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaDiariaFinanLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaDiariaFinanDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaNomAnualLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaNomAnualDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaIntCompLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaIntCompDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaDiariaPunitLoc" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="tasaIntPunitDol" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tasaEfectMensLoc",
        "tasaEfectMensDol",
        "tasaDiariaFinanLoc",
        "tasaDiariaFinanDol",
        "tasaNomAnualLoc",
        "tasaNomAnualDol",
        "tasaIntCompLoc",
        "tasaIntCompDol",
        "tasaDiariaPunitLoc",
        "tasaIntPunitDol"
    })
    public static class Row {

        protected BigDecimal tasaEfectMensLoc;
        protected BigDecimal tasaEfectMensDol;
        protected BigDecimal tasaDiariaFinanLoc;
        protected BigDecimal tasaDiariaFinanDol;
        protected BigDecimal tasaNomAnualLoc;
        protected BigDecimal tasaNomAnualDol;
        protected BigDecimal tasaIntCompLoc;
        protected BigDecimal tasaIntCompDol;
        protected BigDecimal tasaDiariaPunitLoc;
        protected BigDecimal tasaIntPunitDol;

        /**
         * Obtiene el valor de la propiedad tasaEfectMensLoc.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaEfectMensLoc() {
            return tasaEfectMensLoc;
        }

        /**
         * Define el valor de la propiedad tasaEfectMensLoc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaEfectMensLoc(BigDecimal value) {
            this.tasaEfectMensLoc = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaEfectMensDol.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaEfectMensDol() {
            return tasaEfectMensDol;
        }

        /**
         * Define el valor de la propiedad tasaEfectMensDol.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaEfectMensDol(BigDecimal value) {
            this.tasaEfectMensDol = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaDiariaFinanLoc.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaDiariaFinanLoc() {
            return tasaDiariaFinanLoc;
        }

        /**
         * Define el valor de la propiedad tasaDiariaFinanLoc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaDiariaFinanLoc(BigDecimal value) {
            this.tasaDiariaFinanLoc = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaDiariaFinanDol.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaDiariaFinanDol() {
            return tasaDiariaFinanDol;
        }

        /**
         * Define el valor de la propiedad tasaDiariaFinanDol.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaDiariaFinanDol(BigDecimal value) {
            this.tasaDiariaFinanDol = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaNomAnualLoc.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaNomAnualLoc() {
            return tasaNomAnualLoc;
        }

        /**
         * Define el valor de la propiedad tasaNomAnualLoc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaNomAnualLoc(BigDecimal value) {
            this.tasaNomAnualLoc = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaNomAnualDol.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaNomAnualDol() {
            return tasaNomAnualDol;
        }

        /**
         * Define el valor de la propiedad tasaNomAnualDol.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaNomAnualDol(BigDecimal value) {
            this.tasaNomAnualDol = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaIntCompLoc.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaIntCompLoc() {
            return tasaIntCompLoc;
        }

        /**
         * Define el valor de la propiedad tasaIntCompLoc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaIntCompLoc(BigDecimal value) {
            this.tasaIntCompLoc = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaIntCompDol.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaIntCompDol() {
            return tasaIntCompDol;
        }

        /**
         * Define el valor de la propiedad tasaIntCompDol.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaIntCompDol(BigDecimal value) {
            this.tasaIntCompDol = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaDiariaPunitLoc.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaDiariaPunitLoc() {
            return tasaDiariaPunitLoc;
        }

        /**
         * Define el valor de la propiedad tasaDiariaPunitLoc.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaDiariaPunitLoc(BigDecimal value) {
            this.tasaDiariaPunitLoc = value;
        }

        /**
         * Obtiene el valor de la propiedad tasaIntPunitDol.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTasaIntPunitDol() {
            return tasaIntPunitDol;
        }

        /**
         * Define el valor de la propiedad tasaIntPunitDol.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTasaIntPunitDol(BigDecimal value) {
            this.tasaIntPunitDol = value;
        }

    }

}
