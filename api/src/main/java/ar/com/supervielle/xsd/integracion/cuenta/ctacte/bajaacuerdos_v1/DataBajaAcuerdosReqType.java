
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.bajaacuerdos_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para DataBajaAcuerdosReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataBajaAcuerdosReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codEmpresa">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="acuerdo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroAcuerdo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="fechaapertura">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="cuenta">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataBajaAcuerdosReqType", propOrder = {
    "codEmpresa",
    "acuerdo",
    "cuenta"
})
public class DataBajaAcuerdosReqType {

    @XmlElement(required = true)
    protected BigInteger codEmpresa;
    @XmlElement(required = true)
    protected DataBajaAcuerdosReqType.Acuerdo acuerdo;
    @XmlElement(required = true)
    protected DataBajaAcuerdosReqType.Cuenta cuenta;

    /**
     * Obtiene el valor de la propiedad codEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCodEmpresa() {
        return codEmpresa;
    }

    /**
     * Define el valor de la propiedad codEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCodEmpresa(BigInteger value) {
        this.codEmpresa = value;
    }

    /**
     * Obtiene el valor de la propiedad acuerdo.
     * 
     * @return
     *     possible object is
     *     {@link DataBajaAcuerdosReqType.Acuerdo }
     *     
     */
    public DataBajaAcuerdosReqType.Acuerdo getAcuerdo() {
        return acuerdo;
    }

    /**
     * Define el valor de la propiedad acuerdo.
     * 
     * @param value
     *     allowed object is
     *     {@link DataBajaAcuerdosReqType.Acuerdo }
     *     
     */
    public void setAcuerdo(DataBajaAcuerdosReqType.Acuerdo value) {
        this.acuerdo = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link DataBajaAcuerdosReqType.Cuenta }
     *     
     */
    public DataBajaAcuerdosReqType.Cuenta getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link DataBajaAcuerdosReqType.Cuenta }
     *     
     */
    public void setCuenta(DataBajaAcuerdosReqType.Cuenta value) {
        this.cuenta = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroAcuerdo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="fechaapertura">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroAcuerdo",
        "fechaapertura"
    })
    public static class Acuerdo {

        @XmlElement(required = true)
        protected BigInteger nroAcuerdo;
        @XmlElement(required = true)
        protected XMLGregorianCalendar fechaapertura;

        /**
         * Obtiene el valor de la propiedad nroAcuerdo.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNroAcuerdo() {
            return nroAcuerdo;
        }

        /**
         * Define el valor de la propiedad nroAcuerdo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNroAcuerdo(BigInteger value) {
            this.nroAcuerdo = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaapertura.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaapertura() {
            return fechaapertura;
        }

        /**
         * Define el valor de la propiedad fechaapertura.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaapertura(XMLGregorianCalendar value) {
            this.fechaapertura = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroCuenta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="subOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="modulo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroCuenta",
        "codOperacion",
        "subOperacion",
        "tipoOperacion",
        "moneda",
        "sucursal",
        "modulo"
    })
    public static class Cuenta {

        @XmlElement(required = true)
        protected BigInteger nroCuenta;
        @XmlElement(required = true)
        protected BigInteger codOperacion;
        @XmlElement(required = true)
        protected BigInteger subOperacion;
        @XmlElement(required = true)
        protected BigInteger tipoOperacion;
        @XmlElement(required = true)
        protected BigInteger moneda;
        @XmlElement(required = true)
        protected BigInteger sucursal;
        @XmlElement(required = true)
        protected BigInteger modulo;

        /**
         * Obtiene el valor de la propiedad nroCuenta.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNroCuenta() {
            return nroCuenta;
        }

        /**
         * Define el valor de la propiedad nroCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNroCuenta(BigInteger value) {
            this.nroCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad codOperacion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCodOperacion() {
            return codOperacion;
        }

        /**
         * Define el valor de la propiedad codOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCodOperacion(BigInteger value) {
            this.codOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad subOperacion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSubOperacion() {
            return subOperacion;
        }

        /**
         * Define el valor de la propiedad subOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSubOperacion(BigInteger value) {
            this.subOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoOperacion.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTipoOperacion() {
            return tipoOperacion;
        }

        /**
         * Define el valor de la propiedad tipoOperacion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTipoOperacion(BigInteger value) {
            this.tipoOperacion = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMoneda(BigInteger value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad sucursal.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSucursal() {
            return sucursal;
        }

        /**
         * Define el valor de la propiedad sucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSucursal(BigInteger value) {
            this.sucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad modulo.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getModulo() {
            return modulo;
        }

        /**
         * Define el valor de la propiedad modulo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setModulo(BigInteger value) {
            this.modulo = value;
        }

    }

}
