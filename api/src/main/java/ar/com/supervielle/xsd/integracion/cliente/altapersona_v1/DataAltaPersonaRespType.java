
package ar.com.supervielle.xsd.integracion.cliente.altapersona_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.EstadoType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataAltaPersonaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaPersonaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
 *                   &lt;element name="Productos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Cuentas">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="IdCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType" maxOccurs="unbounded"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Personas" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Persona" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
 *                                       &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
 *                                       &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="Tarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType" minOccurs="0"/>
 *                                       &lt;element name="titularRepresentativo">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="1"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaPersonaRespType", propOrder = {
    "row"
})
public class DataAltaPersonaRespType {

    @XmlElement(name = "Row")
    protected DataAltaPersonaRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaPersonaRespType.Row }
     *     
     */
    public DataAltaPersonaRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaPersonaRespType.Row }
     *     
     */
    public void setRow(DataAltaPersonaRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
     *         &lt;element name="Productos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Cuentas">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="IdCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType" maxOccurs="unbounded"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Personas" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Persona" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
     *                             &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
     *                             &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="Tarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType" minOccurs="0"/>
     *                             &lt;element name="titularRepresentativo">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="1"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "estado",
        "productos",
        "personas"
    })
    public static class Row {

        @XmlElement(name = "Estado", required = true)
        protected EstadoType estado;
        @XmlElement(name = "Productos")
        protected DataAltaPersonaRespType.Row.Productos productos;
        @XmlElement(name = "Personas")
        protected DataAltaPersonaRespType.Row.Personas personas;

        /**
         * Obtiene el valor de la propiedad estado.
         * 
         * @return
         *     possible object is
         *     {@link EstadoType }
         *     
         */
        public EstadoType getEstado() {
            return estado;
        }

        /**
         * Define el valor de la propiedad estado.
         * 
         * @param value
         *     allowed object is
         *     {@link EstadoType }
         *     
         */
        public void setEstado(EstadoType value) {
            this.estado = value;
        }

        /**
         * Obtiene el valor de la propiedad productos.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaPersonaRespType.Row.Productos }
         *     
         */
        public DataAltaPersonaRespType.Row.Productos getProductos() {
            return productos;
        }

        /**
         * Define el valor de la propiedad productos.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaPersonaRespType.Row.Productos }
         *     
         */
        public void setProductos(DataAltaPersonaRespType.Row.Productos value) {
            this.productos = value;
        }

        /**
         * Obtiene el valor de la propiedad personas.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaPersonaRespType.Row.Personas }
         *     
         */
        public DataAltaPersonaRespType.Row.Personas getPersonas() {
            return personas;
        }

        /**
         * Define el valor de la propiedad personas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaPersonaRespType.Row.Personas }
         *     
         */
        public void setPersonas(DataAltaPersonaRespType.Row.Personas value) {
            this.personas = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Persona" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
         *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
         *                   &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="Tarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType" minOccurs="0"/>
         *                   &lt;element name="titularRepresentativo">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;maxLength value="1"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "persona"
        })
        public static class Personas {

            @XmlElement(name = "Persona", required = true)
            protected List<DataAltaPersonaRespType.Row.Personas.Persona> persona;

            /**
             * Gets the value of the persona property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the persona property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPersona().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataAltaPersonaRespType.Row.Personas.Persona }
             * 
             * 
             */
            public List<DataAltaPersonaRespType.Row.Personas.Persona> getPersona() {
                if (persona == null) {
                    persona = new ArrayList<DataAltaPersonaRespType.Row.Personas.Persona>();
                }
                return this.persona;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}estadoType"/>
             *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
             *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="titularidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="Tarjeta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType" minOccurs="0"/>
             *         &lt;element name="titularRepresentativo">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;maxLength value="1"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="cuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "estado",
                "nroSolicitud",
                "identificador",
                "nombre",
                "titularidad",
                "tarjeta",
                "titularRepresentativo",
                "cuentaCliente"
            })
            public static class Persona {

                @XmlElement(name = "Estado", required = true)
                protected EstadoType estado;
                protected int nroSolicitud;
                @XmlElement(name = "Identificador")
                protected IdClienteType identificador;
                @XmlElement(required = true)
                protected String nombre;
                protected int titularidad;
                @XmlElement(name = "Tarjeta")
                protected IdTarjetaType tarjeta;
                @XmlElement(required = true)
                protected String titularRepresentativo;
                @XmlElement(required = true)
                protected String cuentaCliente;

                /**
                 * Obtiene el valor de la propiedad estado.
                 * 
                 * @return
                 *     possible object is
                 *     {@link EstadoType }
                 *     
                 */
                public EstadoType getEstado() {
                    return estado;
                }

                /**
                 * Define el valor de la propiedad estado.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EstadoType }
                 *     
                 */
                public void setEstado(EstadoType value) {
                    this.estado = value;
                }

                /**
                 * Obtiene el valor de la propiedad nroSolicitud.
                 * 
                 */
                public int getNroSolicitud() {
                    return nroSolicitud;
                }

                /**
                 * Define el valor de la propiedad nroSolicitud.
                 * 
                 */
                public void setNroSolicitud(int value) {
                    this.nroSolicitud = value;
                }

                /**
                 * Obtiene el valor de la propiedad identificador.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IdClienteType }
                 *     
                 */
                public IdClienteType getIdentificador() {
                    return identificador;
                }

                /**
                 * Define el valor de la propiedad identificador.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IdClienteType }
                 *     
                 */
                public void setIdentificador(IdClienteType value) {
                    this.identificador = value;
                }

                /**
                 * Obtiene el valor de la propiedad nombre.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNombre() {
                    return nombre;
                }

                /**
                 * Define el valor de la propiedad nombre.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNombre(String value) {
                    this.nombre = value;
                }

                /**
                 * Obtiene el valor de la propiedad titularidad.
                 * 
                 */
                public int getTitularidad() {
                    return titularidad;
                }

                /**
                 * Define el valor de la propiedad titularidad.
                 * 
                 */
                public void setTitularidad(int value) {
                    this.titularidad = value;
                }

                /**
                 * Obtiene el valor de la propiedad tarjeta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IdTarjetaType }
                 *     
                 */
                public IdTarjetaType getTarjeta() {
                    return tarjeta;
                }

                /**
                 * Define el valor de la propiedad tarjeta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IdTarjetaType }
                 *     
                 */
                public void setTarjeta(IdTarjetaType value) {
                    this.tarjeta = value;
                }

                /**
                 * Obtiene el valor de la propiedad titularRepresentativo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTitularRepresentativo() {
                    return titularRepresentativo;
                }

                /**
                 * Define el valor de la propiedad titularRepresentativo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTitularRepresentativo(String value) {
                    this.titularRepresentativo = value;
                }

                /**
                 * Obtiene el valor de la propiedad cuentaCliente.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCuentaCliente() {
                    return cuentaCliente;
                }

                /**
                 * Define el valor de la propiedad cuentaCliente.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCuentaCliente(String value) {
                    this.cuentaCliente = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Cuentas">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="IdCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType" maxOccurs="unbounded"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cuentas"
        })
        public static class Productos {

            @XmlElement(name = "Cuentas", required = true)
            protected DataAltaPersonaRespType.Row.Productos.Cuentas cuentas;

            /**
             * Obtiene el valor de la propiedad cuentas.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaPersonaRespType.Row.Productos.Cuentas }
             *     
             */
            public DataAltaPersonaRespType.Row.Productos.Cuentas getCuentas() {
                return cuentas;
            }

            /**
             * Define el valor de la propiedad cuentas.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaPersonaRespType.Row.Productos.Cuentas }
             *     
             */
            public void setCuentas(DataAltaPersonaRespType.Row.Productos.Cuentas value) {
                this.cuentas = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="IdCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType" maxOccurs="unbounded"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "idCuenta"
            })
            public static class Cuentas {

                @XmlElement(name = "IdCuenta", required = true)
                protected List<IdCuentaBANTOTALType> idCuenta;

                /**
                 * Gets the value of the idCuenta property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the idCuenta property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getIdCuenta().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link IdCuentaBANTOTALType }
                 * 
                 * 
                 */
                public List<IdCuentaBANTOTALType> getIdCuenta() {
                    if (idCuenta == null) {
                        idCuenta = new ArrayList<IdCuentaBANTOTALType>();
                    }
                    return this.idCuenta;
                }

            }

        }

    }

}
