
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultamovimientos_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaMovReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaMovReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *         &lt;element name="RangoFecha">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fechaDesde" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="fechaHasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="codigoMovimiento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="debitoOCredito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ordenamiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaMovReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/ctaCte/consultaMovimientos-v1.1", propOrder = {
    "identificador",
    "rangoFecha",
    "codigoMovimiento",
    "debitoOCredito",
    "ordenamiento"
})
public class DataConsultaMovReqType {

    @XmlElement(name = "Identificador", required = true)
    protected IdCuentaBANTOTALType identificador;
    @XmlElement(name = "RangoFecha", required = true)
    protected DataConsultaMovReqType.RangoFecha rangoFecha;
    protected Integer codigoMovimiento;
    @XmlElement(required = true)
    protected String debitoOCredito;
    @XmlElement(required = true)
    protected String ordenamiento;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdCuentaBANTOTALType }
     *     
     */
    public IdCuentaBANTOTALType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdCuentaBANTOTALType }
     *     
     */
    public void setIdentificador(IdCuentaBANTOTALType value) {
        this.identificador = value;
    }

    /**
     * Obtiene el valor de la propiedad rangoFecha.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaMovReqType.RangoFecha }
     *     
     */
    public DataConsultaMovReqType.RangoFecha getRangoFecha() {
        return rangoFecha;
    }

    /**
     * Define el valor de la propiedad rangoFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaMovReqType.RangoFecha }
     *     
     */
    public void setRangoFecha(DataConsultaMovReqType.RangoFecha value) {
        this.rangoFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoMovimiento.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoMovimiento() {
        return codigoMovimiento;
    }

    /**
     * Define el valor de la propiedad codigoMovimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoMovimiento(Integer value) {
        this.codigoMovimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad debitoOCredito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitoOCredito() {
        return debitoOCredito;
    }

    /**
     * Define el valor de la propiedad debitoOCredito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitoOCredito(String value) {
        this.debitoOCredito = value;
    }

    /**
     * Obtiene el valor de la propiedad ordenamiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdenamiento() {
        return ordenamiento;
    }

    /**
     * Define el valor de la propiedad ordenamiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdenamiento(String value) {
        this.ordenamiento = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fechaDesde" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="fechaHasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fechaDesde",
        "fechaHasta"
    })
    public static class RangoFecha {

        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaDesde;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaHasta;

        /**
         * Obtiene el valor de la propiedad fechaDesde.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaDesde() {
            return fechaDesde;
        }

        /**
         * Define el valor de la propiedad fechaDesde.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaDesde(XMLGregorianCalendar value) {
            this.fechaDesde = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaHasta.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaHasta() {
            return fechaHasta;
        }

        /**
         * Define el valor de la propiedad fechaHasta.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaHasta(XMLGregorianCalendar value) {
            this.fechaHasta = value;
        }

    }

}
