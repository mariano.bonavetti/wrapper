
package ar.com.supervielle.xsd.integracion.clienteprevisional.registrarfedevida_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.clienteprevisional.registrarfedevida_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.clienteprevisional.registrarfedevida_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataRegistrarFeDeVidaRespType }
     * 
     */
    public DataRegistrarFeDeVidaRespType createDataRegistrarFeDeVidaRespType() {
        return new DataRegistrarFeDeVidaRespType();
    }

    /**
     * Create an instance of {@link FeDeVida }
     * 
     */
    public FeDeVida createFeDeVida() {
        return new FeDeVida();
    }

    /**
     * Create an instance of {@link ReqRegistrarFeDeVida }
     * 
     */
    public ReqRegistrarFeDeVida createReqRegistrarFeDeVida() {
        return new ReqRegistrarFeDeVida();
    }

    /**
     * Create an instance of {@link RespRegistrarFeDeVida }
     * 
     */
    public RespRegistrarFeDeVida createRespRegistrarFeDeVida() {
        return new RespRegistrarFeDeVida();
    }

    /**
     * Create an instance of {@link DataRegistrarFeDeVidaRespType.Row }
     * 
     */
    public DataRegistrarFeDeVidaRespType.Row createDataRegistrarFeDeVidaRespTypeRow() {
        return new DataRegistrarFeDeVidaRespType.Row();
    }

    /**
     * Create an instance of {@link FeDeVida.FeDeVidaCP }
     * 
     */
    public FeDeVida.FeDeVidaCP createFeDeVidaFeDeVidaCP() {
        return new FeDeVida.FeDeVidaCP();
    }

    /**
     * Create an instance of {@link FeDeVida.FeDeVidaBO }
     * 
     */
    public FeDeVida.FeDeVidaBO createFeDeVidaFeDeVidaBO() {
        return new FeDeVida.FeDeVidaBO();
    }

}
