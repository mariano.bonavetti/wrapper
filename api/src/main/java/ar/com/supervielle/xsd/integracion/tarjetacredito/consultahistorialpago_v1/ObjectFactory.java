
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultahistorialpago_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultahistorialpago_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultahistorialpago_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaHistorialPagoRespType2 }
     * 
     */
    public DataConsultaHistorialPagoRespType2 createDataConsultaHistorialPagoRespType2() {
        return new DataConsultaHistorialPagoRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaHistorialPagoRespType }
     * 
     */
    public DataConsultaHistorialPagoRespType createDataConsultaHistorialPagoRespType() {
        return new DataConsultaHistorialPagoRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaHistorialPago }
     * 
     */
    public ReqConsultaHistorialPago createReqConsultaHistorialPago() {
        return new ReqConsultaHistorialPago();
    }

    /**
     * Create an instance of {@link DataConsultaHistorialPagoReqType }
     * 
     */
    public DataConsultaHistorialPagoReqType createDataConsultaHistorialPagoReqType() {
        return new DataConsultaHistorialPagoReqType();
    }

    /**
     * Create an instance of {@link RespConsultaHistorialPago }
     * 
     */
    public RespConsultaHistorialPago createRespConsultaHistorialPago() {
        return new RespConsultaHistorialPago();
    }

    /**
     * Create an instance of {@link ReqConsultaHistorialPago2 }
     * 
     */
    public ReqConsultaHistorialPago2 createReqConsultaHistorialPago2() {
        return new ReqConsultaHistorialPago2();
    }

    /**
     * Create an instance of {@link DataConsultaHistorialPagoReqType2 }
     * 
     */
    public DataConsultaHistorialPagoReqType2 createDataConsultaHistorialPagoReqType2() {
        return new DataConsultaHistorialPagoReqType2();
    }

    /**
     * Create an instance of {@link RespConsultaHistorialPago2 }
     * 
     */
    public RespConsultaHistorialPago2 createRespConsultaHistorialPago2() {
        return new RespConsultaHistorialPago2();
    }

    /**
     * Create an instance of {@link DataConsultaHistorialPagoRespType2 .Row }
     * 
     */
    public DataConsultaHistorialPagoRespType2 .Row createDataConsultaHistorialPagoRespType2Row() {
        return new DataConsultaHistorialPagoRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaHistorialPagoRespType.Row }
     * 
     */
    public DataConsultaHistorialPagoRespType.Row createDataConsultaHistorialPagoRespTypeRow() {
        return new DataConsultaHistorialPagoRespType.Row();
    }

}
