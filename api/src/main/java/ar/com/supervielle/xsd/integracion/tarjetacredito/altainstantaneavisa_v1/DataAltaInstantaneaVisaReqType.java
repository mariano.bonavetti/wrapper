
package ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneavisa_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdSexoType;


/**
 * <p>Clase Java para DataAltaInstantaneaVisaReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaInstantaneaVisaReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Principal">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="tipoDocTribu" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="numDocTribu" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="cuentaBanTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Filiatorios">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nomCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="codNacionalidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="nomEmbozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="nombreCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="situacionIva" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idSexoType"/>
 *                   &lt;element name="codOcupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Domicilios">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Domicilio" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="puerta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="dpto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codPostal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Producto">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codGrupoAfinidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="tipoTarjeta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="planSueldo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="codPaquete" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="tipoCartera" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="habilitacionATM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bonificacionPrimerAnio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bonificacionSegundoAnio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="codVigencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="debito" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="sucCuentaDebito" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="garante" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="limitePrestamo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="numCuentaBanca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="porcenLimAdel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="mantieneBonificacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="bonificacionCajero" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="plazoPagoMin" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="ctaPaquetesSucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="ctaBancaTipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Complementarios">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ctaPaquete" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="codResponsableCta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="sucCuenta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="veraz" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Limites">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="novedad">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="1"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Limite" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="tipoExcepcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="montoExcepcion" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                                   &lt;maxInclusive value="9999999999.99"/>
 *                                   &lt;totalDigits value="12"/>
 *                                   &lt;fractionDigits value="2"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaInstantaneaVisaReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/altaInstantaneaVisa-v1", propOrder = {
    "codOperacion",
    "principal",
    "filiatorios",
    "domicilios",
    "producto",
    "complementarios",
    "limites"
})
public class DataAltaInstantaneaVisaReqType {

    protected int codOperacion;
    @XmlElement(name = "Principal", required = true)
    protected DataAltaInstantaneaVisaReqType.Principal principal;
    @XmlElement(name = "Filiatorios", required = true)
    protected DataAltaInstantaneaVisaReqType.Filiatorios filiatorios;
    @XmlElement(name = "Domicilios", required = true)
    protected DataAltaInstantaneaVisaReqType.Domicilios domicilios;
    @XmlElement(name = "Producto", required = true)
    protected DataAltaInstantaneaVisaReqType.Producto producto;
    @XmlElement(name = "Complementarios", required = true)
    protected DataAltaInstantaneaVisaReqType.Complementarios complementarios;
    @XmlElement(name = "Limites", required = true)
    protected DataAltaInstantaneaVisaReqType.Limites limites;

    /**
     * Obtiene el valor de la propiedad codOperacion.
     * 
     */
    public int getCodOperacion() {
        return codOperacion;
    }

    /**
     * Define el valor de la propiedad codOperacion.
     * 
     */
    public void setCodOperacion(int value) {
        this.codOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad principal.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType.Principal }
     *     
     */
    public DataAltaInstantaneaVisaReqType.Principal getPrincipal() {
        return principal;
    }

    /**
     * Define el valor de la propiedad principal.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType.Principal }
     *     
     */
    public void setPrincipal(DataAltaInstantaneaVisaReqType.Principal value) {
        this.principal = value;
    }

    /**
     * Obtiene el valor de la propiedad filiatorios.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType.Filiatorios }
     *     
     */
    public DataAltaInstantaneaVisaReqType.Filiatorios getFiliatorios() {
        return filiatorios;
    }

    /**
     * Define el valor de la propiedad filiatorios.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType.Filiatorios }
     *     
     */
    public void setFiliatorios(DataAltaInstantaneaVisaReqType.Filiatorios value) {
        this.filiatorios = value;
    }

    /**
     * Obtiene el valor de la propiedad domicilios.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType.Domicilios }
     *     
     */
    public DataAltaInstantaneaVisaReqType.Domicilios getDomicilios() {
        return domicilios;
    }

    /**
     * Define el valor de la propiedad domicilios.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType.Domicilios }
     *     
     */
    public void setDomicilios(DataAltaInstantaneaVisaReqType.Domicilios value) {
        this.domicilios = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType.Producto }
     *     
     */
    public DataAltaInstantaneaVisaReqType.Producto getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType.Producto }
     *     
     */
    public void setProducto(DataAltaInstantaneaVisaReqType.Producto value) {
        this.producto = value;
    }

    /**
     * Obtiene el valor de la propiedad complementarios.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType.Complementarios }
     *     
     */
    public DataAltaInstantaneaVisaReqType.Complementarios getComplementarios() {
        return complementarios;
    }

    /**
     * Define el valor de la propiedad complementarios.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType.Complementarios }
     *     
     */
    public void setComplementarios(DataAltaInstantaneaVisaReqType.Complementarios value) {
        this.complementarios = value;
    }

    /**
     * Obtiene el valor de la propiedad limites.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaVisaReqType.Limites }
     *     
     */
    public DataAltaInstantaneaVisaReqType.Limites getLimites() {
        return limites;
    }

    /**
     * Define el valor de la propiedad limites.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaVisaReqType.Limites }
     *     
     */
    public void setLimites(DataAltaInstantaneaVisaReqType.Limites value) {
        this.limites = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ctaPaquete" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="codResponsableCta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="sucCuenta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="veraz" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="fechaSolicitud" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ctaPaquete",
        "codResponsableCta",
        "sucCuenta",
        "usuarioCarga",
        "veraz",
        "fechaSolicitud"
    })
    public static class Complementarios {

        protected int ctaPaquete;
        @XmlElement(required = true)
        protected String codResponsableCta;
        protected Integer sucCuenta;
        @XmlElement(required = true)
        protected String usuarioCarga;
        protected int veraz;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaSolicitud;

        /**
         * Obtiene el valor de la propiedad ctaPaquete.
         * 
         */
        public int getCtaPaquete() {
            return ctaPaquete;
        }

        /**
         * Define el valor de la propiedad ctaPaquete.
         * 
         */
        public void setCtaPaquete(int value) {
            this.ctaPaquete = value;
        }

        /**
         * Obtiene el valor de la propiedad codResponsableCta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodResponsableCta() {
            return codResponsableCta;
        }

        /**
         * Define el valor de la propiedad codResponsableCta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodResponsableCta(String value) {
            this.codResponsableCta = value;
        }

        /**
         * Obtiene el valor de la propiedad sucCuenta.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSucCuenta() {
            return sucCuenta;
        }

        /**
         * Define el valor de la propiedad sucCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSucCuenta(Integer value) {
            this.sucCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad usuarioCarga.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUsuarioCarga() {
            return usuarioCarga;
        }

        /**
         * Define el valor de la propiedad usuarioCarga.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUsuarioCarga(String value) {
            this.usuarioCarga = value;
        }

        /**
         * Obtiene el valor de la propiedad veraz.
         * 
         */
        public int getVeraz() {
            return veraz;
        }

        /**
         * Define el valor de la propiedad veraz.
         * 
         */
        public void setVeraz(int value) {
            this.veraz = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaSolicitud() {
            return fechaSolicitud;
        }

        /**
         * Define el valor de la propiedad fechaSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaSolicitud(XMLGregorianCalendar value) {
            this.fechaSolicitud = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Domicilio" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="puerta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="dpto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codPostal" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "domicilio"
    })
    public static class Domicilios {

        @XmlElement(name = "Domicilio", required = true)
        protected List<DataAltaInstantaneaVisaReqType.Domicilios.Domicilio> domicilio;

        /**
         * Gets the value of the domicilio property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the domicilio property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDomicilio().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataAltaInstantaneaVisaReqType.Domicilios.Domicilio }
         * 
         * 
         */
        public List<DataAltaInstantaneaVisaReqType.Domicilios.Domicilio> getDomicilio() {
            if (domicilio == null) {
                domicilio = new ArrayList<DataAltaInstantaneaVisaReqType.Domicilios.Domicilio>();
            }
            return this.domicilio;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codDomicilio" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="puerta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="dpto" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codPostal" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codDomicilio",
            "calle",
            "puerta",
            "piso",
            "dpto",
            "localidad",
            "codPostal",
            "tel",
            "tipo"
        })
        public static class Domicilio {

            protected int codDomicilio;
            @XmlElement(required = true)
            protected String calle;
            @XmlElement(required = true)
            protected String puerta;
            @XmlElement(required = true)
            protected String piso;
            @XmlElement(required = true)
            protected String dpto;
            @XmlElement(required = true)
            protected String localidad;
            protected int codPostal;
            @XmlElement(required = true)
            protected String tel;
            protected Integer tipo;

            /**
             * Obtiene el valor de la propiedad codDomicilio.
             * 
             */
            public int getCodDomicilio() {
                return codDomicilio;
            }

            /**
             * Define el valor de la propiedad codDomicilio.
             * 
             */
            public void setCodDomicilio(int value) {
                this.codDomicilio = value;
            }

            /**
             * Obtiene el valor de la propiedad calle.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalle() {
                return calle;
            }

            /**
             * Define el valor de la propiedad calle.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalle(String value) {
                this.calle = value;
            }

            /**
             * Obtiene el valor de la propiedad puerta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPuerta() {
                return puerta;
            }

            /**
             * Define el valor de la propiedad puerta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPuerta(String value) {
                this.puerta = value;
            }

            /**
             * Obtiene el valor de la propiedad piso.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPiso() {
                return piso;
            }

            /**
             * Define el valor de la propiedad piso.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPiso(String value) {
                this.piso = value;
            }

            /**
             * Obtiene el valor de la propiedad dpto.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDpto() {
                return dpto;
            }

            /**
             * Define el valor de la propiedad dpto.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDpto(String value) {
                this.dpto = value;
            }

            /**
             * Obtiene el valor de la propiedad localidad.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocalidad() {
                return localidad;
            }

            /**
             * Define el valor de la propiedad localidad.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocalidad(String value) {
                this.localidad = value;
            }

            /**
             * Obtiene el valor de la propiedad codPostal.
             * 
             */
            public int getCodPostal() {
                return codPostal;
            }

            /**
             * Define el valor de la propiedad codPostal.
             * 
             */
            public void setCodPostal(int value) {
                this.codPostal = value;
            }

            /**
             * Obtiene el valor de la propiedad tel.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTel() {
                return tel;
            }

            /**
             * Define el valor de la propiedad tel.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTel(String value) {
                this.tel = value;
            }

            /**
             * Obtiene el valor de la propiedad tipo.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getTipo() {
                return tipo;
            }

            /**
             * Define el valor de la propiedad tipo.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setTipo(Integer value) {
                this.tipo = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nomCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codNacionalidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="nomEmbozado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="nombreCuenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="situacionIva" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="sexo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}idSexoType"/>
     *         &lt;element name="codOcupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nomCliente",
        "codNacionalidad",
        "nomEmbozado",
        "codEstadoCivil",
        "nombreCuenta",
        "situacionIva",
        "fechaNacimiento",
        "sexo",
        "codOcupacion"
    })
    public static class Filiatorios {

        @XmlElement(required = true)
        protected String nomCliente;
        protected int codNacionalidad;
        @XmlElement(required = true)
        protected String nomEmbozado;
        protected int codEstadoCivil;
        @XmlElement(required = true)
        protected String nombreCuenta;
        protected int situacionIva;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar fechaNacimiento;
        @XmlElement(required = true)
        @XmlSchemaType(name = "string")
        protected IdSexoType sexo;
        protected int codOcupacion;

        /**
         * Obtiene el valor de la propiedad nomCliente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNomCliente() {
            return nomCliente;
        }

        /**
         * Define el valor de la propiedad nomCliente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNomCliente(String value) {
            this.nomCliente = value;
        }

        /**
         * Obtiene el valor de la propiedad codNacionalidad.
         * 
         */
        public int getCodNacionalidad() {
            return codNacionalidad;
        }

        /**
         * Define el valor de la propiedad codNacionalidad.
         * 
         */
        public void setCodNacionalidad(int value) {
            this.codNacionalidad = value;
        }

        /**
         * Obtiene el valor de la propiedad nomEmbozado.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNomEmbozado() {
            return nomEmbozado;
        }

        /**
         * Define el valor de la propiedad nomEmbozado.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNomEmbozado(String value) {
            this.nomEmbozado = value;
        }

        /**
         * Obtiene el valor de la propiedad codEstadoCivil.
         * 
         */
        public int getCodEstadoCivil() {
            return codEstadoCivil;
        }

        /**
         * Define el valor de la propiedad codEstadoCivil.
         * 
         */
        public void setCodEstadoCivil(int value) {
            this.codEstadoCivil = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreCuenta.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreCuenta() {
            return nombreCuenta;
        }

        /**
         * Define el valor de la propiedad nombreCuenta.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreCuenta(String value) {
            this.nombreCuenta = value;
        }

        /**
         * Obtiene el valor de la propiedad situacionIva.
         * 
         */
        public int getSituacionIva() {
            return situacionIva;
        }

        /**
         * Define el valor de la propiedad situacionIva.
         * 
         */
        public void setSituacionIva(int value) {
            this.situacionIva = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaNacimiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaNacimiento() {
            return fechaNacimiento;
        }

        /**
         * Define el valor de la propiedad fechaNacimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaNacimiento(XMLGregorianCalendar value) {
            this.fechaNacimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad sexo.
         * 
         * @return
         *     possible object is
         *     {@link IdSexoType }
         *     
         */
        public IdSexoType getSexo() {
            return sexo;
        }

        /**
         * Define el valor de la propiedad sexo.
         * 
         * @param value
         *     allowed object is
         *     {@link IdSexoType }
         *     
         */
        public void setSexo(IdSexoType value) {
            this.sexo = value;
        }

        /**
         * Obtiene el valor de la propiedad codOcupacion.
         * 
         */
        public int getCodOcupacion() {
            return codOcupacion;
        }

        /**
         * Define el valor de la propiedad codOcupacion.
         * 
         */
        public void setCodOcupacion(int value) {
            this.codOcupacion = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="novedad">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="1"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Limite" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="tipoExcepcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="montoExcepcion" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *                         &lt;maxInclusive value="9999999999.99"/>
     *                         &lt;totalDigits value="12"/>
     *                         &lt;fractionDigits value="2"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "novedad",
        "limite"
    })
    public static class Limites {

        @XmlElement(required = true)
        protected String novedad;
        @XmlElement(name = "Limite", required = true)
        protected List<DataAltaInstantaneaVisaReqType.Limites.Limite> limite;

        /**
         * Obtiene el valor de la propiedad novedad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNovedad() {
            return novedad;
        }

        /**
         * Define el valor de la propiedad novedad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNovedad(String value) {
            this.novedad = value;
        }

        /**
         * Gets the value of the limite property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the limite property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLimite().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataAltaInstantaneaVisaReqType.Limites.Limite }
         * 
         * 
         */
        public List<DataAltaInstantaneaVisaReqType.Limites.Limite> getLimite() {
            if (limite == null) {
                limite = new ArrayList<DataAltaInstantaneaVisaReqType.Limites.Limite>();
            }
            return this.limite;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="tipoExcepcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="montoExcepcion" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
         *               &lt;maxInclusive value="9999999999.99"/>
         *               &lt;totalDigits value="12"/>
         *               &lt;fractionDigits value="2"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "tipoExcepcion",
            "montoExcepcion"
        })
        public static class Limite {

            @XmlElement(required = true)
            protected String tipoExcepcion;
            protected BigDecimal montoExcepcion;

            /**
             * Obtiene el valor de la propiedad tipoExcepcion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipoExcepcion() {
                return tipoExcepcion;
            }

            /**
             * Define el valor de la propiedad tipoExcepcion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipoExcepcion(String value) {
                this.tipoExcepcion = value;
            }

            /**
             * Obtiene el valor de la propiedad montoExcepcion.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getMontoExcepcion() {
                return montoExcepcion;
            }

            /**
             * Define el valor de la propiedad montoExcepcion.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setMontoExcepcion(BigDecimal value) {
                this.montoExcepcion = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="numEntidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="nroDoc" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="tipoDocTribu" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="numDocTribu" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="cuentaBanTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codMarca",
        "numEntidad",
        "tipoDoc",
        "nroDoc",
        "tipoDocTribu",
        "numDocTribu",
        "cuentaBanTotal"
    })
    public static class Principal {

        protected int codMarca;
        protected int numEntidad;
        protected int tipoDoc;
        protected int nroDoc;
        protected int tipoDocTribu;
        @XmlElement(required = true)
        protected String numDocTribu;
        protected int cuentaBanTotal;

        /**
         * Obtiene el valor de la propiedad codMarca.
         * 
         */
        public int getCodMarca() {
            return codMarca;
        }

        /**
         * Define el valor de la propiedad codMarca.
         * 
         */
        public void setCodMarca(int value) {
            this.codMarca = value;
        }

        /**
         * Obtiene el valor de la propiedad numEntidad.
         * 
         */
        public int getNumEntidad() {
            return numEntidad;
        }

        /**
         * Define el valor de la propiedad numEntidad.
         * 
         */
        public void setNumEntidad(int value) {
            this.numEntidad = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoDoc.
         * 
         */
        public int getTipoDoc() {
            return tipoDoc;
        }

        /**
         * Define el valor de la propiedad tipoDoc.
         * 
         */
        public void setTipoDoc(int value) {
            this.tipoDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad nroDoc.
         * 
         */
        public int getNroDoc() {
            return nroDoc;
        }

        /**
         * Define el valor de la propiedad nroDoc.
         * 
         */
        public void setNroDoc(int value) {
            this.nroDoc = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoDocTribu.
         * 
         */
        public int getTipoDocTribu() {
            return tipoDocTribu;
        }

        /**
         * Define el valor de la propiedad tipoDocTribu.
         * 
         */
        public void setTipoDocTribu(int value) {
            this.tipoDocTribu = value;
        }

        /**
         * Obtiene el valor de la propiedad numDocTribu.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumDocTribu() {
            return numDocTribu;
        }

        /**
         * Define el valor de la propiedad numDocTribu.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumDocTribu(String value) {
            this.numDocTribu = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaBanTotal.
         * 
         */
        public int getCuentaBanTotal() {
            return cuentaBanTotal;
        }

        /**
         * Define el valor de la propiedad cuentaBanTotal.
         * 
         */
        public void setCuentaBanTotal(int value) {
            this.cuentaBanTotal = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codGrupoAfinidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="tipoTarjeta" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="planSueldo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="codPaquete" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="tipoCartera" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="habilitacionATM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bonificacionPrimerAnio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bonificacionSegundoAnio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="codVigencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="formaPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="debito" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="sucCuentaDebito" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="garante" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="limitePrestamo" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="numCuentaBanca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="porcenLimAdel" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="mantieneBonificacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="bonificacionCajero" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="plazoPagoMin" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="ctaPaquetesSucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="ctaBancaTipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codGrupoAfinidad",
        "tipoTarjeta",
        "planSueldo",
        "empresa",
        "codPaquete",
        "tipoCartera",
        "codProducto",
        "habilitacionATM",
        "modeloLiquidacion",
        "cantCuotas",
        "bonificacionPrimerAnio",
        "bonificacionSegundoAnio",
        "codVigencia",
        "formaPago",
        "debito",
        "sucCuentaDebito",
        "limiteCompra",
        "garante",
        "limitePrestamo",
        "numCuentaBanca",
        "porcenLimAdel",
        "mantieneBonificacion",
        "bonificacionCajero",
        "plazoPagoMin",
        "ctaPaquetesSucursal",
        "ctaBancaTipo"
    })
    public static class Producto {

        protected int codGrupoAfinidad;
        protected int tipoTarjeta;
        protected int planSueldo;
        protected int empresa;
        protected int codPaquete;
        protected int tipoCartera;
        protected int codProducto;
        @XmlElement(required = true)
        protected String habilitacionATM;
        protected int modeloLiquidacion;
        @XmlElement(required = true)
        protected String cantCuotas;
        @XmlElement(required = true)
        protected String bonificacionPrimerAnio;
        @XmlElement(required = true)
        protected String bonificacionSegundoAnio;
        @XmlElement(required = true)
        protected String codVigencia;
        @XmlElement(required = true)
        protected String formaPago;
        protected Integer debito;
        protected Integer sucCuentaDebito;
        @XmlElement(required = true)
        protected BigDecimal limiteCompra;
        protected int garante;
        @XmlElement(required = true)
        protected BigDecimal limitePrestamo;
        @XmlElement(required = true)
        protected String numCuentaBanca;
        protected int porcenLimAdel;
        @XmlElement(required = true)
        protected String mantieneBonificacion;
        protected int bonificacionCajero;
        protected int plazoPagoMin;
        protected Integer ctaPaquetesSucursal;
        @XmlElement(required = true)
        protected String ctaBancaTipo;

        /**
         * Obtiene el valor de la propiedad codGrupoAfinidad.
         * 
         */
        public int getCodGrupoAfinidad() {
            return codGrupoAfinidad;
        }

        /**
         * Define el valor de la propiedad codGrupoAfinidad.
         * 
         */
        public void setCodGrupoAfinidad(int value) {
            this.codGrupoAfinidad = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoTarjeta.
         * 
         */
        public int getTipoTarjeta() {
            return tipoTarjeta;
        }

        /**
         * Define el valor de la propiedad tipoTarjeta.
         * 
         */
        public void setTipoTarjeta(int value) {
            this.tipoTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad planSueldo.
         * 
         */
        public int getPlanSueldo() {
            return planSueldo;
        }

        /**
         * Define el valor de la propiedad planSueldo.
         * 
         */
        public void setPlanSueldo(int value) {
            this.planSueldo = value;
        }

        /**
         * Obtiene el valor de la propiedad empresa.
         * 
         */
        public int getEmpresa() {
            return empresa;
        }

        /**
         * Define el valor de la propiedad empresa.
         * 
         */
        public void setEmpresa(int value) {
            this.empresa = value;
        }

        /**
         * Obtiene el valor de la propiedad codPaquete.
         * 
         */
        public int getCodPaquete() {
            return codPaquete;
        }

        /**
         * Define el valor de la propiedad codPaquete.
         * 
         */
        public void setCodPaquete(int value) {
            this.codPaquete = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoCartera.
         * 
         */
        public int getTipoCartera() {
            return tipoCartera;
        }

        /**
         * Define el valor de la propiedad tipoCartera.
         * 
         */
        public void setTipoCartera(int value) {
            this.tipoCartera = value;
        }

        /**
         * Obtiene el valor de la propiedad codProducto.
         * 
         */
        public int getCodProducto() {
            return codProducto;
        }

        /**
         * Define el valor de la propiedad codProducto.
         * 
         */
        public void setCodProducto(int value) {
            this.codProducto = value;
        }

        /**
         * Obtiene el valor de la propiedad habilitacionATM.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHabilitacionATM() {
            return habilitacionATM;
        }

        /**
         * Define el valor de la propiedad habilitacionATM.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHabilitacionATM(String value) {
            this.habilitacionATM = value;
        }

        /**
         * Obtiene el valor de la propiedad modeloLiquidacion.
         * 
         */
        public int getModeloLiquidacion() {
            return modeloLiquidacion;
        }

        /**
         * Define el valor de la propiedad modeloLiquidacion.
         * 
         */
        public void setModeloLiquidacion(int value) {
            this.modeloLiquidacion = value;
        }

        /**
         * Obtiene el valor de la propiedad cantCuotas.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCantCuotas() {
            return cantCuotas;
        }

        /**
         * Define el valor de la propiedad cantCuotas.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCantCuotas(String value) {
            this.cantCuotas = value;
        }

        /**
         * Obtiene el valor de la propiedad bonificacionPrimerAnio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBonificacionPrimerAnio() {
            return bonificacionPrimerAnio;
        }

        /**
         * Define el valor de la propiedad bonificacionPrimerAnio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBonificacionPrimerAnio(String value) {
            this.bonificacionPrimerAnio = value;
        }

        /**
         * Obtiene el valor de la propiedad bonificacionSegundoAnio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBonificacionSegundoAnio() {
            return bonificacionSegundoAnio;
        }

        /**
         * Define el valor de la propiedad bonificacionSegundoAnio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBonificacionSegundoAnio(String value) {
            this.bonificacionSegundoAnio = value;
        }

        /**
         * Obtiene el valor de la propiedad codVigencia.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodVigencia() {
            return codVigencia;
        }

        /**
         * Define el valor de la propiedad codVigencia.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodVigencia(String value) {
            this.codVigencia = value;
        }

        /**
         * Obtiene el valor de la propiedad formaPago.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFormaPago() {
            return formaPago;
        }

        /**
         * Define el valor de la propiedad formaPago.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFormaPago(String value) {
            this.formaPago = value;
        }

        /**
         * Obtiene el valor de la propiedad debito.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getDebito() {
            return debito;
        }

        /**
         * Define el valor de la propiedad debito.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setDebito(Integer value) {
            this.debito = value;
        }

        /**
         * Obtiene el valor de la propiedad sucCuentaDebito.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSucCuentaDebito() {
            return sucCuentaDebito;
        }

        /**
         * Define el valor de la propiedad sucCuentaDebito.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSucCuentaDebito(Integer value) {
            this.sucCuentaDebito = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompra.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompra() {
            return limiteCompra;
        }

        /**
         * Define el valor de la propiedad limiteCompra.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompra(BigDecimal value) {
            this.limiteCompra = value;
        }

        /**
         * Obtiene el valor de la propiedad garante.
         * 
         */
        public int getGarante() {
            return garante;
        }

        /**
         * Define el valor de la propiedad garante.
         * 
         */
        public void setGarante(int value) {
            this.garante = value;
        }

        /**
         * Obtiene el valor de la propiedad limitePrestamo.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimitePrestamo() {
            return limitePrestamo;
        }

        /**
         * Define el valor de la propiedad limitePrestamo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimitePrestamo(BigDecimal value) {
            this.limitePrestamo = value;
        }

        /**
         * Obtiene el valor de la propiedad numCuentaBanca.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumCuentaBanca() {
            return numCuentaBanca;
        }

        /**
         * Define el valor de la propiedad numCuentaBanca.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumCuentaBanca(String value) {
            this.numCuentaBanca = value;
        }

        /**
         * Obtiene el valor de la propiedad porcenLimAdel.
         * 
         */
        public int getPorcenLimAdel() {
            return porcenLimAdel;
        }

        /**
         * Define el valor de la propiedad porcenLimAdel.
         * 
         */
        public void setPorcenLimAdel(int value) {
            this.porcenLimAdel = value;
        }

        /**
         * Obtiene el valor de la propiedad mantieneBonificacion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMantieneBonificacion() {
            return mantieneBonificacion;
        }

        /**
         * Define el valor de la propiedad mantieneBonificacion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMantieneBonificacion(String value) {
            this.mantieneBonificacion = value;
        }

        /**
         * Obtiene el valor de la propiedad bonificacionCajero.
         * 
         */
        public int getBonificacionCajero() {
            return bonificacionCajero;
        }

        /**
         * Define el valor de la propiedad bonificacionCajero.
         * 
         */
        public void setBonificacionCajero(int value) {
            this.bonificacionCajero = value;
        }

        /**
         * Obtiene el valor de la propiedad plazoPagoMin.
         * 
         */
        public int getPlazoPagoMin() {
            return plazoPagoMin;
        }

        /**
         * Define el valor de la propiedad plazoPagoMin.
         * 
         */
        public void setPlazoPagoMin(int value) {
            this.plazoPagoMin = value;
        }

        /**
         * Obtiene el valor de la propiedad ctaPaquetesSucursal.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCtaPaquetesSucursal() {
            return ctaPaquetesSucursal;
        }

        /**
         * Define el valor de la propiedad ctaPaquetesSucursal.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCtaPaquetesSucursal(Integer value) {
            this.ctaPaquetesSucursal = value;
        }

        /**
         * Obtiene el valor de la propiedad ctaBancaTipo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCtaBancaTipo() {
            return ctaBancaTipo;
        }

        /**
         * Define el valor de la propiedad ctaBancaTipo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCtaBancaTipo(String value) {
            this.ctaBancaTipo = value;
        }

    }

}
