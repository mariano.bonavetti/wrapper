
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadosolicitud_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.pagingtypes_v1.PagingType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.supervielle.com.ar/xsd/Integracion/common/pagingTypes-v1}Paging" minOccurs="0"/>
 *         &lt;element name="Data">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="ByCliente">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                             &lt;element name="grupoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                             &lt;element name="fechaDesde" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="fechaHasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BySolicitud">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                             &lt;element name="grupoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="secuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paging",
    "data"
})
@XmlRootElement(name = "ReqConsultaEstadoSolicitud")
public class ReqConsultaEstadoSolicitud {

    @XmlElement(name = "Paging", namespace = "http://www.supervielle.com.ar/xsd/Integracion/common/pagingTypes-v1")
    protected PagingType paging;
    @XmlElement(name = "Data", required = true)
    protected ReqConsultaEstadoSolicitud.Data data;

    /**
     * Obtiene el valor de la propiedad paging.
     * 
     * @return
     *     possible object is
     *     {@link PagingType }
     *     
     */
    public PagingType getPaging() {
        return paging;
    }

    /**
     * Define el valor de la propiedad paging.
     * 
     * @param value
     *     allowed object is
     *     {@link PagingType }
     *     
     */
    public void setPaging(PagingType value) {
        this.paging = value;
    }

    /**
     * Obtiene el valor de la propiedad data.
     * 
     * @return
     *     possible object is
     *     {@link ReqConsultaEstadoSolicitud.Data }
     *     
     */
    public ReqConsultaEstadoSolicitud.Data getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqConsultaEstadoSolicitud.Data }
     *     
     */
    public void setData(ReqConsultaEstadoSolicitud.Data value) {
        this.data = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="ByCliente">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                   &lt;element name="grupoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                   &lt;element name="fechaDesde" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="fechaHasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BySolicitud">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *                   &lt;element name="grupoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="secuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "byCliente",
        "bySolicitud"
    })
    public static class Data {

        @XmlElement(name = "ByCliente")
        protected ReqConsultaEstadoSolicitud.Data.ByCliente byCliente;
        @XmlElement(name = "BySolicitud")
        protected ReqConsultaEstadoSolicitud.Data.BySolicitud bySolicitud;

        /**
         * Obtiene el valor de la propiedad byCliente.
         * 
         * @return
         *     possible object is
         *     {@link ReqConsultaEstadoSolicitud.Data.ByCliente }
         *     
         */
        public ReqConsultaEstadoSolicitud.Data.ByCliente getByCliente() {
            return byCliente;
        }

        /**
         * Define el valor de la propiedad byCliente.
         * 
         * @param value
         *     allowed object is
         *     {@link ReqConsultaEstadoSolicitud.Data.ByCliente }
         *     
         */
        public void setByCliente(ReqConsultaEstadoSolicitud.Data.ByCliente value) {
            this.byCliente = value;
        }

        /**
         * Obtiene el valor de la propiedad bySolicitud.
         * 
         * @return
         *     possible object is
         *     {@link ReqConsultaEstadoSolicitud.Data.BySolicitud }
         *     
         */
        public ReqConsultaEstadoSolicitud.Data.BySolicitud getBySolicitud() {
            return bySolicitud;
        }

        /**
         * Define el valor de la propiedad bySolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link ReqConsultaEstadoSolicitud.Data.BySolicitud }
         *     
         */
        public void setBySolicitud(ReqConsultaEstadoSolicitud.Data.BySolicitud value) {
            this.bySolicitud = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *         &lt;element name="grupoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *         &lt;element name="fechaDesde" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="fechaHasta" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigoMarca",
            "grupoSolicitud",
            "identificador",
            "fechaDesde",
            "fechaHasta"
        })
        public static class ByCliente {

            protected Integer codigoMarca;
            @XmlElement(required = true)
            protected String grupoSolicitud;
            @XmlElement(name = "Identificador", required = true)
            protected IdClienteType identificador;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaDesde;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaHasta;

            /**
             * Obtiene el valor de la propiedad codigoMarca.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getCodigoMarca() {
                return codigoMarca;
            }

            /**
             * Define el valor de la propiedad codigoMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setCodigoMarca(Integer value) {
                this.codigoMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad grupoSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGrupoSolicitud() {
                return grupoSolicitud;
            }

            /**
             * Define el valor de la propiedad grupoSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGrupoSolicitud(String value) {
                this.grupoSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad identificador.
             * 
             * @return
             *     possible object is
             *     {@link IdClienteType }
             *     
             */
            public IdClienteType getIdentificador() {
                return identificador;
            }

            /**
             * Define el valor de la propiedad identificador.
             * 
             * @param value
             *     allowed object is
             *     {@link IdClienteType }
             *     
             */
            public void setIdentificador(IdClienteType value) {
                this.identificador = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaDesde.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaDesde() {
                return fechaDesde;
            }

            /**
             * Define el valor de la propiedad fechaDesde.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaDesde(XMLGregorianCalendar value) {
                this.fechaDesde = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaHasta.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaHasta() {
                return fechaHasta;
            }

            /**
             * Define el valor de la propiedad fechaHasta.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaHasta(XMLGregorianCalendar value) {
                this.fechaHasta = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigoMarca" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
         *         &lt;element name="grupoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="secuencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigoMarca",
            "grupoSolicitud",
            "nroSolicitud",
            "secuencia"
        })
        public static class BySolicitud {

            protected Integer codigoMarca;
            @XmlElement(required = true)
            protected String grupoSolicitud;
            protected int nroSolicitud;
            protected int secuencia;

            /**
             * Obtiene el valor de la propiedad codigoMarca.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getCodigoMarca() {
                return codigoMarca;
            }

            /**
             * Define el valor de la propiedad codigoMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setCodigoMarca(Integer value) {
                this.codigoMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad grupoSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGrupoSolicitud() {
                return grupoSolicitud;
            }

            /**
             * Define el valor de la propiedad grupoSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGrupoSolicitud(String value) {
                this.grupoSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitud.
             * 
             */
            public int getNroSolicitud() {
                return nroSolicitud;
            }

            /**
             * Define el valor de la propiedad nroSolicitud.
             * 
             */
            public void setNroSolicitud(int value) {
                this.nroSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad secuencia.
             * 
             */
            public int getSecuencia() {
                return secuencia;
            }

            /**
             * Define el valor de la propiedad secuencia.
             * 
             */
            public void setSecuencia(int value) {
                this.secuencia = value;
            }

        }

    }

}
