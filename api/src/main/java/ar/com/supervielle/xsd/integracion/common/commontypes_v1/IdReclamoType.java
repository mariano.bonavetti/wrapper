
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para idReclamoType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="idReclamoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idReclamo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="dataTypeName" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="RECLAMO"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "idReclamoType", propOrder = {
    "idReclamo"
})
public class IdReclamoType {

    @XmlElement(required = true)
    protected String idReclamo;
    @XmlAttribute(name = "dataTypeName", required = true)
    protected String dataTypeName;

    /**
     * Obtiene el valor de la propiedad idReclamo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdReclamo() {
        return idReclamo;
    }

    /**
     * Define el valor de la propiedad idReclamo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdReclamo(String value) {
        this.idReclamo = value;
    }

    /**
     * Obtiene el valor de la propiedad dataTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTypeName() {
        return dataTypeName;
    }

    /**
     * Define el valor de la propiedad dataTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTypeName(String value) {
        this.dataTypeName = value;
    }

}
