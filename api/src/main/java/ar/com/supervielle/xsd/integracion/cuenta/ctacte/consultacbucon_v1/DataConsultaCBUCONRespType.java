
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbucon_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;


/**
 * <p>Clase Java para DataConsultaCBUCONRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCBUCONRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
 *                   &lt;element name="CBU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCBUCONRespType", propOrder = {
    "row"
})
public class DataConsultaCBUCONRespType {

    @XmlElement(name = "Row")
    protected DataConsultaCBUCONRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaCBUCONRespType.Row }
     *     
     */
    public DataConsultaCBUCONRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaCBUCONRespType.Row }
     *     
     */
    public void setRow(DataConsultaCBUCONRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaCONSUMOType"/>
     *         &lt;element name="CBU" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "cbu"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdCuentaCONSUMOType identificador;
        @XmlElement(name = "CBU", required = true)
        protected String cbu;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public IdCuentaCONSUMOType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCuentaCONSUMOType }
         *     
         */
        public void setIdentificador(IdCuentaCONSUMOType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad cbu.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCBU() {
            return cbu;
        }

        /**
         * Define el valor de la propiedad cbu.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCBU(String value) {
            this.cbu = value;
        }

    }

}
