
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para mandatarioExtSinTD complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="mandatarioExtSinTD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nroCuentaMandatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nroDocMandatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCuentaMandatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocMandatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mandatarioExtSinTD", propOrder = {
    "nroCuentaMandatario",
    "nroDocMandatario",
    "tipoCuentaMandatario",
    "tipoDocMandatario"
})
public class MandatarioExtSinTD {

    protected String nroCuentaMandatario;
    protected String nroDocMandatario;
    protected String tipoCuentaMandatario;
    protected String tipoDocMandatario;

    /**
     * Obtiene el valor de la propiedad nroCuentaMandatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroCuentaMandatario() {
        return nroCuentaMandatario;
    }

    /**
     * Define el valor de la propiedad nroCuentaMandatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroCuentaMandatario(String value) {
        this.nroCuentaMandatario = value;
    }

    /**
     * Obtiene el valor de la propiedad nroDocMandatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroDocMandatario() {
        return nroDocMandatario;
    }

    /**
     * Define el valor de la propiedad nroDocMandatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroDocMandatario(String value) {
        this.nroDocMandatario = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCuentaMandatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCuentaMandatario() {
        return tipoCuentaMandatario;
    }

    /**
     * Define el valor de la propiedad tipoCuentaMandatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCuentaMandatario(String value) {
        this.tipoCuentaMandatario = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocMandatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocMandatario() {
        return tipoDocMandatario;
    }

    /**
     * Define el valor de la propiedad tipoDocMandatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocMandatario(String value) {
        this.tipoDocMandatario = value;
    }

}
