
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestado_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestado_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestado_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes }
     * 
     */
    public DataConsultaEstTarjetaRes createDataConsultaEstTarjetaRes() {
        return new DataConsultaEstTarjetaRes();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row }
     * 
     */
    public DataConsultaEstTarjetaRes.Row createDataConsultaEstTarjetaResRow() {
        return new DataConsultaEstTarjetaRes.Row();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Historial }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Historial createDataConsultaEstTarjetaResRowHistorial() {
        return new DataConsultaEstTarjetaRes.Row.Historial();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Historial.Registro }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Historial.Registro createDataConsultaEstTarjetaResRowHistorialRegistro() {
        return new DataConsultaEstTarjetaRes.Row.Historial.Registro();
    }

    /**
     * Create an instance of {@link RespConsultaEstTarjeta }
     * 
     */
    public RespConsultaEstTarjeta createRespConsultaEstTarjeta() {
        return new RespConsultaEstTarjeta();
    }

    /**
     * Create an instance of {@link ReqConsultaEstTarjeta }
     * 
     */
    public ReqConsultaEstTarjeta createReqConsultaEstTarjeta() {
        return new ReqConsultaEstTarjeta();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaReq }
     * 
     */
    public DataConsultaEstTarjetaReq createDataConsultaEstTarjetaReq() {
        return new DataConsultaEstTarjetaReq();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Marca }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Marca createDataConsultaEstTarjetaResRowMarca() {
        return new DataConsultaEstTarjetaRes.Row.Marca();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Historial.Registro.Estado }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Historial.Registro.Estado createDataConsultaEstTarjetaResRowHistorialRegistroEstado() {
        return new DataConsultaEstTarjetaRes.Row.Historial.Registro.Estado();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Historial.Registro.Error }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Historial.Registro.Error createDataConsultaEstTarjetaResRowHistorialRegistroError() {
        return new DataConsultaEstTarjetaRes.Row.Historial.Registro.Error();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Historial.Registro.Titular }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Historial.Registro.Titular createDataConsultaEstTarjetaResRowHistorialRegistroTitular() {
        return new DataConsultaEstTarjetaRes.Row.Historial.Registro.Titular();
    }

    /**
     * Create an instance of {@link DataConsultaEstTarjetaRes.Row.Historial.Registro.Adicional }
     * 
     */
    public DataConsultaEstTarjetaRes.Row.Historial.Registro.Adicional createDataConsultaEstTarjetaResRowHistorialRegistroAdicional() {
        return new DataConsultaEstTarjetaRes.Row.Historial.Registro.Adicional();
    }

}
