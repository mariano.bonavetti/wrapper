
package ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneavisa_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneavisa_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneavisa_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType2 }
     * 
     */
    public DataAltaInstantaneaVisaRespType2 createDataAltaInstantaneaVisaRespType2() {
        return new DataAltaInstantaneaVisaRespType2();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType2 .Row }
     * 
     */
    public DataAltaInstantaneaVisaRespType2 .Row createDataAltaInstantaneaVisaRespType2Row() {
        return new DataAltaInstantaneaVisaRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 createDataAltaInstantaneaVisaReqType2() {
        return new DataAltaInstantaneaVisaReqType2();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Limites }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Limites createDataAltaInstantaneaVisaReqType2Limites() {
        return new DataAltaInstantaneaVisaReqType2 .Limites();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Domicilios }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Domicilios createDataAltaInstantaneaVisaReqType2Domicilios() {
        return new DataAltaInstantaneaVisaReqType2 .Domicilios();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType }
     * 
     */
    public DataAltaInstantaneaVisaRespType createDataAltaInstantaneaVisaRespType() {
        return new DataAltaInstantaneaVisaRespType();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType.Row }
     * 
     */
    public DataAltaInstantaneaVisaRespType.Row createDataAltaInstantaneaVisaRespTypeRow() {
        return new DataAltaInstantaneaVisaRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType }
     * 
     */
    public DataAltaInstantaneaVisaReqType createDataAltaInstantaneaVisaReqType() {
        return new DataAltaInstantaneaVisaReqType();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Limites }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Limites createDataAltaInstantaneaVisaReqTypeLimites() {
        return new DataAltaInstantaneaVisaReqType.Limites();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Domicilios }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Domicilios createDataAltaInstantaneaVisaReqTypeDomicilios() {
        return new DataAltaInstantaneaVisaReqType.Domicilios();
    }

    /**
     * Create an instance of {@link ReqAltaInstantaneaVisa }
     * 
     */
    public ReqAltaInstantaneaVisa createReqAltaInstantaneaVisa() {
        return new ReqAltaInstantaneaVisa();
    }

    /**
     * Create an instance of {@link RespAltaInstantaneaVisa }
     * 
     */
    public RespAltaInstantaneaVisa createRespAltaInstantaneaVisa() {
        return new RespAltaInstantaneaVisa();
    }

    /**
     * Create an instance of {@link ReqAltaInstantaneaVisa2 }
     * 
     */
    public ReqAltaInstantaneaVisa2 createReqAltaInstantaneaVisa2() {
        return new ReqAltaInstantaneaVisa2();
    }

    /**
     * Create an instance of {@link RespAltaInstantaneaVisa2 }
     * 
     */
    public RespAltaInstantaneaVisa2 createRespAltaInstantaneaVisa2() {
        return new RespAltaInstantaneaVisa2();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType2 .Row.Error }
     * 
     */
    public DataAltaInstantaneaVisaRespType2 .Row.Error createDataAltaInstantaneaVisaRespType2RowError() {
        return new DataAltaInstantaneaVisaRespType2 .Row.Error();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType2 .Row.Solicitud }
     * 
     */
    public DataAltaInstantaneaVisaRespType2 .Row.Solicitud createDataAltaInstantaneaVisaRespType2RowSolicitud() {
        return new DataAltaInstantaneaVisaRespType2 .Row.Solicitud();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Principal }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Principal createDataAltaInstantaneaVisaReqType2Principal() {
        return new DataAltaInstantaneaVisaReqType2 .Principal();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Filiatorios }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Filiatorios createDataAltaInstantaneaVisaReqType2Filiatorios() {
        return new DataAltaInstantaneaVisaReqType2 .Filiatorios();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Producto }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Producto createDataAltaInstantaneaVisaReqType2Producto() {
        return new DataAltaInstantaneaVisaReqType2 .Producto();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Complementarios }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Complementarios createDataAltaInstantaneaVisaReqType2Complementarios() {
        return new DataAltaInstantaneaVisaReqType2 .Complementarios();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Limites.Limite }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Limites.Limite createDataAltaInstantaneaVisaReqType2LimitesLimite() {
        return new DataAltaInstantaneaVisaReqType2 .Limites.Limite();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType2 .Domicilios.Domicilio }
     * 
     */
    public DataAltaInstantaneaVisaReqType2 .Domicilios.Domicilio createDataAltaInstantaneaVisaReqType2DomiciliosDomicilio() {
        return new DataAltaInstantaneaVisaReqType2 .Domicilios.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType.Row.Error }
     * 
     */
    public DataAltaInstantaneaVisaRespType.Row.Error createDataAltaInstantaneaVisaRespTypeRowError() {
        return new DataAltaInstantaneaVisaRespType.Row.Error();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaRespType.Row.Solicitud }
     * 
     */
    public DataAltaInstantaneaVisaRespType.Row.Solicitud createDataAltaInstantaneaVisaRespTypeRowSolicitud() {
        return new DataAltaInstantaneaVisaRespType.Row.Solicitud();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Principal }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Principal createDataAltaInstantaneaVisaReqTypePrincipal() {
        return new DataAltaInstantaneaVisaReqType.Principal();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Filiatorios }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Filiatorios createDataAltaInstantaneaVisaReqTypeFiliatorios() {
        return new DataAltaInstantaneaVisaReqType.Filiatorios();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Producto }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Producto createDataAltaInstantaneaVisaReqTypeProducto() {
        return new DataAltaInstantaneaVisaReqType.Producto();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Complementarios }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Complementarios createDataAltaInstantaneaVisaReqTypeComplementarios() {
        return new DataAltaInstantaneaVisaReqType.Complementarios();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Limites.Limite }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Limites.Limite createDataAltaInstantaneaVisaReqTypeLimitesLimite() {
        return new DataAltaInstantaneaVisaReqType.Limites.Limite();
    }

    /**
     * Create an instance of {@link DataAltaInstantaneaVisaReqType.Domicilios.Domicilio }
     * 
     */
    public DataAltaInstantaneaVisaReqType.Domicilios.Domicilio createDataAltaInstantaneaVisaReqTypeDomiciliosDomicilio() {
        return new DataAltaInstantaneaVisaReqType.Domicilios.Domicilio();
    }

}
