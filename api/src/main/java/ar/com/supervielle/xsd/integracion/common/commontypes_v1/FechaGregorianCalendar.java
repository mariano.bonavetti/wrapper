
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para FechaGregorianCalendar complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FechaGregorianCalendar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="firstDayOfWeek" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="gregorianChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="lenient" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="minimalDaysInFirstWeek" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="timeInMillis" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="timeZone" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dstSavings" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="rawOffset" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FechaGregorianCalendar", propOrder = {
    "firstDayOfWeek",
    "gregorianChange",
    "lenient",
    "minimalDaysInFirstWeek",
    "time",
    "timeInMillis",
    "timeZone"
})
public class FechaGregorianCalendar {

    protected Integer firstDayOfWeek;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gregorianChange;
    protected Boolean lenient;
    protected Integer minimalDaysInFirstWeek;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar time;
    protected Long timeInMillis;
    protected FechaGregorianCalendar.TimeZone timeZone;

    /**
     * Obtiene el valor de la propiedad firstDayOfWeek.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    /**
     * Define el valor de la propiedad firstDayOfWeek.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFirstDayOfWeek(Integer value) {
        this.firstDayOfWeek = value;
    }

    /**
     * Obtiene el valor de la propiedad gregorianChange.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGregorianChange() {
        return gregorianChange;
    }

    /**
     * Define el valor de la propiedad gregorianChange.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGregorianChange(XMLGregorianCalendar value) {
        this.gregorianChange = value;
    }

    /**
     * Obtiene el valor de la propiedad lenient.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLenient() {
        return lenient;
    }

    /**
     * Define el valor de la propiedad lenient.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLenient(Boolean value) {
        this.lenient = value;
    }

    /**
     * Obtiene el valor de la propiedad minimalDaysInFirstWeek.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    /**
     * Define el valor de la propiedad minimalDaysInFirstWeek.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinimalDaysInFirstWeek(Integer value) {
        this.minimalDaysInFirstWeek = value;
    }

    /**
     * Obtiene el valor de la propiedad time.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTime() {
        return time;
    }

    /**
     * Define el valor de la propiedad time.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTime(XMLGregorianCalendar value) {
        this.time = value;
    }

    /**
     * Obtiene el valor de la propiedad timeInMillis.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTimeInMillis() {
        return timeInMillis;
    }

    /**
     * Define el valor de la propiedad timeInMillis.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTimeInMillis(Long value) {
        this.timeInMillis = value;
    }

    /**
     * Obtiene el valor de la propiedad timeZone.
     * 
     * @return
     *     possible object is
     *     {@link FechaGregorianCalendar.TimeZone }
     *     
     */
    public FechaGregorianCalendar.TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * Define el valor de la propiedad timeZone.
     * 
     * @param value
     *     allowed object is
     *     {@link FechaGregorianCalendar.TimeZone }
     *     
     */
    public void setTimeZone(FechaGregorianCalendar.TimeZone value) {
        this.timeZone = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="dstSavings" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="rawOffset" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dstSavings",
        "id",
        "displayName",
        "rawOffset"
    })
    public static class TimeZone {

        protected Integer dstSavings;
        protected String id;
        protected String displayName;
        protected Integer rawOffset;

        /**
         * Obtiene el valor de la propiedad dstSavings.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getDstSavings() {
            return dstSavings;
        }

        /**
         * Define el valor de la propiedad dstSavings.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setDstSavings(Integer value) {
            this.dstSavings = value;
        }

        /**
         * Obtiene el valor de la propiedad id.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }

        /**
         * Obtiene el valor de la propiedad displayName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayName() {
            return displayName;
        }

        /**
         * Define el valor de la propiedad displayName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayName(String value) {
            this.displayName = value;
        }

        /**
         * Obtiene el valor de la propiedad rawOffset.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getRawOffset() {
            return rawOffset;
        }

        /**
         * Define el valor de la propiedad rawOffset.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setRawOffset(Integer value) {
            this.rawOffset = value;
        }

    }

}
