
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistadocuentas_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaListadoCuentasRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoCuentasRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaListadoCuentas-v1}idCtaTarjetaType"/>
 *                   &lt;element name="Empresa" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                             &lt;element name="CuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Producto">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="productoMarca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="oficialComercial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="limiteCreditoMarca" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="receptorClaveVisaHome" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Domicilio">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="geoLocalizacionMarca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cuartaLineaPlastico" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="cuentaTarjeta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                             &lt;element name="BancoEmisor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="cuentaClienteAsociada" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cuentaMarca">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="TipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                             &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                             &lt;element name="Cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                             &lt;element name="OficialComercial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                             &lt;element name="Producto">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="GAF">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ModeloLiquidacion">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="OrigenAlta">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="SubCanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                             &lt;element name="HabilitaControlLimitexTarjeta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                             &lt;element name="FormaPago">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="tipoPagoCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="tipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                       &lt;element name="CuentaDebito" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="numeroSucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                                 &lt;element name="cuentaNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="FechaAlta" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="importeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="importeLimiteCompraCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="LiquidacionActual">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="PagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="LiquidacionAnterior">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                       &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="fechaProximoCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="DomicilioCuenta">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DomicilioCorrespondencia">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoCuentasRespType", propOrder = {
    "row"
})
public class DataConsultaListadoCuentasRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaListadoCuentasRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaListadoCuentasRespType.Row }
     * 
     * 
     */
    public List<DataConsultaListadoCuentasRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaListadoCuentasRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/consultaListadoCuentas-v1}idCtaTarjetaType"/>
     *         &lt;element name="Empresa" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                   &lt;element name="CuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Producto">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="productoMarca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="oficialComercial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="limiteCreditoMarca" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="receptorClaveVisaHome" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Domicilio">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="geoLocalizacionMarca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cuartaLineaPlastico" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="cuentaTarjeta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                   &lt;element name="BancoEmisor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="cuentaClienteAsociada" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cuentaMarca">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="TipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *                   &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                   &lt;element name="Cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                   &lt;element name="OficialComercial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                   &lt;element name="Producto">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="GAF">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ModeloLiquidacion">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="OrigenAlta">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="SubCanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                   &lt;element name="HabilitaControlLimitexTarjeta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                   &lt;element name="FormaPago">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="tipoPagoCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="tipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                             &lt;element name="CuentaDebito" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="numeroSucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                                       &lt;element name="cuentaNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="FechaAlta" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="importeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="importeLimiteCompraCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="LiquidacionActual">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="PagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="LiquidacionAnterior">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                             &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="fechaProximoCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="DomicilioCuenta">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DomicilioCorrespondencia">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "empresa",
        "cuentaTarjeta"
    })
    public static class Row {

        @XmlElement(name = "Identificador", required = true)
        protected IdCtaTarjetaType identificador;
        @XmlElement(name = "Empresa")
        protected DataConsultaListadoCuentasRespType.Row.Empresa empresa;
        @XmlElement(required = true)
        protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta cuentaTarjeta;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdCtaTarjetaType }
         *     
         */
        public IdCtaTarjetaType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdCtaTarjetaType }
         *     
         */
        public void setIdentificador(IdCtaTarjetaType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad empresa.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoCuentasRespType.Row.Empresa }
         *     
         */
        public DataConsultaListadoCuentasRespType.Row.Empresa getEmpresa() {
            return empresa;
        }

        /**
         * Define el valor de la propiedad empresa.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoCuentasRespType.Row.Empresa }
         *     
         */
        public void setEmpresa(DataConsultaListadoCuentasRespType.Row.Empresa value) {
            this.empresa = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta }
         *     
         */
        public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta getCuentaTarjeta() {
            return cuentaTarjeta;
        }

        /**
         * Define el valor de la propiedad cuentaTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta }
         *     
         */
        public void setCuentaTarjeta(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta value) {
            this.cuentaTarjeta = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Marca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *         &lt;element name="BancoEmisor" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="cuentaClienteAsociada" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cuentaMarca">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="TipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="Cliente" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *         &lt;element name="Sucursal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *         &lt;element name="Cartera" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *         &lt;element name="OficialComercial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="Producto">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="GAF">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ModeloLiquidacion">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="OrigenAlta">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="SubCanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *         &lt;element name="HabilitaControlLimitexTarjeta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *         &lt;element name="FormaPago">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="tipoPagoCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="tipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                   &lt;element name="CuentaDebito" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="numeroSucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                             &lt;element name="cuentaNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="FechaAlta" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="importeLimiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="importeLimiteCompraCuotas" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="limiteFinanciacion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="LiquidacionActual">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="PagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="LiquidacionAnterior">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                   &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="fechaProximoCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="DomicilioCuenta">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DomicilioCorrespondencia">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "marca",
            "bancoEmisor",
            "cuentaClienteAsociada",
            "cuentaMarca",
            "tipoCuenta",
            "cliente",
            "sucursal",
            "cartera",
            "oficialComercial",
            "producto",
            "gaf",
            "modeloLiquidacion",
            "origenAlta",
            "estado",
            "habilitaControlLimitexTarjeta",
            "formaPago",
            "fechaAlta",
            "importeLimiteCompra",
            "importeLimiteCompraCuotas",
            "limiteFinanciacion",
            "liquidacionActual",
            "liquidacionAnterior",
            "fechaProximoCierre",
            "domicilioCuenta",
            "domicilioCorrespondencia"
        })
        public static class CuentaTarjeta {

            @XmlElement(name = "Marca", required = true)
            protected CodDescNumType marca;
            @XmlElement(name = "BancoEmisor", required = true)
            protected CodDescStringType bancoEmisor;
            @XmlElement(required = true)
            protected String cuentaClienteAsociada;
            @XmlElement(required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca cuentaMarca;
            @XmlElement(name = "TipoCuenta", required = true)
            protected CodDescStringType tipoCuenta;
            @XmlElement(name = "Cliente", required = true)
            protected IdClienteType cliente;
            @XmlElement(name = "Sucursal", required = true)
            protected CodDescNumType sucursal;
            @XmlElement(name = "Cartera", required = true)
            protected CodDescNumType cartera;
            @XmlElement(name = "OficialComercial", required = true)
            protected CodDescStringType oficialComercial;
            @XmlElement(name = "Producto", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto producto;
            @XmlElement(name = "GAF", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF gaf;
            @XmlElement(name = "ModeloLiquidacion", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion modeloLiquidacion;
            @XmlElement(name = "OrigenAlta", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta origenAlta;
            @XmlElement(name = "Estado", required = true)
            protected CodDescNumType estado;
            @XmlElement(name = "HabilitaControlLimitexTarjeta")
            protected boolean habilitaControlLimitexTarjeta;
            @XmlElement(name = "FormaPago", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago formaPago;
            @XmlElement(name = "FechaAlta", required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaAlta;
            @XmlElement(required = true)
            protected BigDecimal importeLimiteCompra;
            @XmlElement(required = true)
            protected BigDecimal importeLimiteCompraCuotas;
            @XmlElement(required = true)
            protected BigDecimal limiteFinanciacion;
            @XmlElement(name = "LiquidacionActual", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual liquidacionActual;
            @XmlElement(name = "LiquidacionAnterior", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior liquidacionAnterior;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar fechaProximoCierre;
            @XmlElement(name = "DomicilioCuenta", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta domicilioCuenta;
            @XmlElement(name = "DomicilioCorrespondencia", required = true)
            protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia domicilioCorrespondencia;

            /**
             * Obtiene el valor de la propiedad marca.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getMarca() {
                return marca;
            }

            /**
             * Define el valor de la propiedad marca.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setMarca(CodDescNumType value) {
                this.marca = value;
            }

            /**
             * Obtiene el valor de la propiedad bancoEmisor.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getBancoEmisor() {
                return bancoEmisor;
            }

            /**
             * Define el valor de la propiedad bancoEmisor.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setBancoEmisor(CodDescStringType value) {
                this.bancoEmisor = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaClienteAsociada.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaClienteAsociada() {
                return cuentaClienteAsociada;
            }

            /**
             * Define el valor de la propiedad cuentaClienteAsociada.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaClienteAsociada(String value) {
                this.cuentaClienteAsociada = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaMarca.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca getCuentaMarca() {
                return cuentaMarca;
            }

            /**
             * Define el valor de la propiedad cuentaMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca }
             *     
             */
            public void setCuentaMarca(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.CuentaMarca value) {
                this.cuentaMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoCuenta.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getTipoCuenta() {
                return tipoCuenta;
            }

            /**
             * Define el valor de la propiedad tipoCuenta.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setTipoCuenta(CodDescStringType value) {
                this.tipoCuenta = value;
            }

            /**
             * Obtiene el valor de la propiedad cliente.
             * 
             * @return
             *     possible object is
             *     {@link IdClienteType }
             *     
             */
            public IdClienteType getCliente() {
                return cliente;
            }

            /**
             * Define el valor de la propiedad cliente.
             * 
             * @param value
             *     allowed object is
             *     {@link IdClienteType }
             *     
             */
            public void setCliente(IdClienteType value) {
                this.cliente = value;
            }

            /**
             * Obtiene el valor de la propiedad sucursal.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getSucursal() {
                return sucursal;
            }

            /**
             * Define el valor de la propiedad sucursal.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setSucursal(CodDescNumType value) {
                this.sucursal = value;
            }

            /**
             * Obtiene el valor de la propiedad cartera.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getCartera() {
                return cartera;
            }

            /**
             * Define el valor de la propiedad cartera.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setCartera(CodDescNumType value) {
                this.cartera = value;
            }

            /**
             * Obtiene el valor de la propiedad oficialComercial.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getOficialComercial() {
                return oficialComercial;
            }

            /**
             * Define el valor de la propiedad oficialComercial.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setOficialComercial(CodDescStringType value) {
                this.oficialComercial = value;
            }

            /**
             * Obtiene el valor de la propiedad producto.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto getProducto() {
                return producto;
            }

            /**
             * Define el valor de la propiedad producto.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto }
             *     
             */
            public void setProducto(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.Producto value) {
                this.producto = value;
            }

            /**
             * Obtiene el valor de la propiedad gaf.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF getGAF() {
                return gaf;
            }

            /**
             * Define el valor de la propiedad gaf.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF }
             *     
             */
            public void setGAF(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.GAF value) {
                this.gaf = value;
            }

            /**
             * Obtiene el valor de la propiedad modeloLiquidacion.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion getModeloLiquidacion() {
                return modeloLiquidacion;
            }

            /**
             * Define el valor de la propiedad modeloLiquidacion.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion }
             *     
             */
            public void setModeloLiquidacion(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.ModeloLiquidacion value) {
                this.modeloLiquidacion = value;
            }

            /**
             * Obtiene el valor de la propiedad origenAlta.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta getOrigenAlta() {
                return origenAlta;
            }

            /**
             * Define el valor de la propiedad origenAlta.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta }
             *     
             */
            public void setOrigenAlta(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.OrigenAlta value) {
                this.origenAlta = value;
            }

            /**
             * Obtiene el valor de la propiedad estado.
             * 
             * @return
             *     possible object is
             *     {@link CodDescNumType }
             *     
             */
            public CodDescNumType getEstado() {
                return estado;
            }

            /**
             * Define el valor de la propiedad estado.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescNumType }
             *     
             */
            public void setEstado(CodDescNumType value) {
                this.estado = value;
            }

            /**
             * Obtiene el valor de la propiedad habilitaControlLimitexTarjeta.
             * 
             */
            public boolean isHabilitaControlLimitexTarjeta() {
                return habilitaControlLimitexTarjeta;
            }

            /**
             * Define el valor de la propiedad habilitaControlLimitexTarjeta.
             * 
             */
            public void setHabilitaControlLimitexTarjeta(boolean value) {
                this.habilitaControlLimitexTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPago.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago getFormaPago() {
                return formaPago;
            }

            /**
             * Define el valor de la propiedad formaPago.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago }
             *     
             */
            public void setFormaPago(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago value) {
                this.formaPago = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaAlta.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaAlta() {
                return fechaAlta;
            }

            /**
             * Define el valor de la propiedad fechaAlta.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaAlta(XMLGregorianCalendar value) {
                this.fechaAlta = value;
            }

            /**
             * Obtiene el valor de la propiedad importeLimiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getImporteLimiteCompra() {
                return importeLimiteCompra;
            }

            /**
             * Define el valor de la propiedad importeLimiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setImporteLimiteCompra(BigDecimal value) {
                this.importeLimiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad importeLimiteCompraCuotas.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getImporteLimiteCompraCuotas() {
                return importeLimiteCompraCuotas;
            }

            /**
             * Define el valor de la propiedad importeLimiteCompraCuotas.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setImporteLimiteCompraCuotas(BigDecimal value) {
                this.importeLimiteCompraCuotas = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteFinanciacion.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimiteFinanciacion() {
                return limiteFinanciacion;
            }

            /**
             * Define el valor de la propiedad limiteFinanciacion.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimiteFinanciacion(BigDecimal value) {
                this.limiteFinanciacion = value;
            }

            /**
             * Obtiene el valor de la propiedad liquidacionActual.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual getLiquidacionActual() {
                return liquidacionActual;
            }

            /**
             * Define el valor de la propiedad liquidacionActual.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual }
             *     
             */
            public void setLiquidacionActual(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionActual value) {
                this.liquidacionActual = value;
            }

            /**
             * Obtiene el valor de la propiedad liquidacionAnterior.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior getLiquidacionAnterior() {
                return liquidacionAnterior;
            }

            /**
             * Define el valor de la propiedad liquidacionAnterior.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior }
             *     
             */
            public void setLiquidacionAnterior(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.LiquidacionAnterior value) {
                this.liquidacionAnterior = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaProximoCierre.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getFechaProximoCierre() {
                return fechaProximoCierre;
            }

            /**
             * Define el valor de la propiedad fechaProximoCierre.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaProximoCierre(XMLGregorianCalendar value) {
                this.fechaProximoCierre = value;
            }

            /**
             * Obtiene el valor de la propiedad domicilioCuenta.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta getDomicilioCuenta() {
                return domicilioCuenta;
            }

            /**
             * Define el valor de la propiedad domicilioCuenta.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta }
             *     
             */
            public void setDomicilioCuenta(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCuenta value) {
                this.domicilioCuenta = value;
            }

            /**
             * Obtiene el valor de la propiedad domicilioCorrespondencia.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia getDomicilioCorrespondencia() {
                return domicilioCorrespondencia;
            }

            /**
             * Define el valor de la propiedad domicilioCorrespondencia.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia }
             *     
             */
            public void setDomicilioCorrespondencia(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.DomicilioCorrespondencia value) {
                this.domicilioCorrespondencia = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "numero",
                "denominacion"
            })
            public static class CuentaMarca {

                @XmlElement(required = true)
                protected String numero;
                @XmlElement(required = true)
                protected String denominacion;

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumero(String value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad denominacion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDenominacion() {
                    return denominacion;
                }

                /**
                 * Define el valor de la propiedad denominacion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDenominacion(String value) {
                    this.denominacion = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "calle",
                "numero",
                "piso",
                "referencia",
                "codigoPostal",
                "localidad",
                "provincia"
            })
            public static class DomicilioCorrespondencia {

                @XmlElement(required = true)
                protected String calle;
                @XmlElement(required = true)
                protected String numero;
                @XmlElement(required = true)
                protected String piso;
                @XmlElement(required = true)
                protected String referencia;
                @XmlElement(required = true)
                protected String codigoPostal;
                @XmlElement(required = true)
                protected String localidad;
                @XmlElement(required = true)
                protected CodDescStringType provincia;

                /**
                 * Obtiene el valor de la propiedad calle.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCalle() {
                    return calle;
                }

                /**
                 * Define el valor de la propiedad calle.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCalle(String value) {
                    this.calle = value;
                }

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumero(String value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad piso.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPiso() {
                    return piso;
                }

                /**
                 * Define el valor de la propiedad piso.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPiso(String value) {
                    this.piso = value;
                }

                /**
                 * Obtiene el valor de la propiedad referencia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReferencia() {
                    return referencia;
                }

                /**
                 * Define el valor de la propiedad referencia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReferencia(String value) {
                    this.referencia = value;
                }

                /**
                 * Obtiene el valor de la propiedad codigoPostal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigoPostal() {
                    return codigoPostal;
                }

                /**
                 * Define el valor de la propiedad codigoPostal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigoPostal(String value) {
                    this.codigoPostal = value;
                }

                /**
                 * Obtiene el valor de la propiedad localidad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocalidad() {
                    return localidad;
                }

                /**
                 * Define el valor de la propiedad localidad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocalidad(String value) {
                    this.localidad = value;
                }

                /**
                 * Obtiene el valor de la propiedad provincia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getProvincia() {
                    return provincia;
                }

                /**
                 * Define el valor de la propiedad provincia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setProvincia(CodDescStringType value) {
                    this.provincia = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="provincia" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "calle",
                "numero",
                "piso",
                "referencia",
                "codigoPostal",
                "localidad",
                "provincia"
            })
            public static class DomicilioCuenta {

                @XmlElement(required = true)
                protected String calle;
                @XmlElement(required = true)
                protected String numero;
                @XmlElement(required = true)
                protected String piso;
                @XmlElement(required = true)
                protected String referencia;
                @XmlElement(required = true)
                protected String codigoPostal;
                @XmlElement(required = true)
                protected String localidad;
                @XmlElement(required = true)
                protected CodDescStringType provincia;

                /**
                 * Obtiene el valor de la propiedad calle.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCalle() {
                    return calle;
                }

                /**
                 * Define el valor de la propiedad calle.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCalle(String value) {
                    this.calle = value;
                }

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumero(String value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad piso.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPiso() {
                    return piso;
                }

                /**
                 * Define el valor de la propiedad piso.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPiso(String value) {
                    this.piso = value;
                }

                /**
                 * Obtiene el valor de la propiedad referencia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReferencia() {
                    return referencia;
                }

                /**
                 * Define el valor de la propiedad referencia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReferencia(String value) {
                    this.referencia = value;
                }

                /**
                 * Obtiene el valor de la propiedad codigoPostal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigoPostal() {
                    return codigoPostal;
                }

                /**
                 * Define el valor de la propiedad codigoPostal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigoPostal(String value) {
                    this.codigoPostal = value;
                }

                /**
                 * Obtiene el valor de la propiedad localidad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocalidad() {
                    return localidad;
                }

                /**
                 * Define el valor de la propiedad localidad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocalidad(String value) {
                    this.localidad = value;
                }

                /**
                 * Obtiene el valor de la propiedad provincia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getProvincia() {
                    return provincia;
                }

                /**
                 * Define el valor de la propiedad provincia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setProvincia(CodDescStringType value) {
                    this.provincia = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="tipoPagoCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="tipoCuenta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *         &lt;element name="CuentaDebito" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="numeroSucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *                   &lt;element name="cuentaNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "tipoPagoCodigo",
                "descripcion",
                "tipoCuenta",
                "cuentaDebito"
            })
            public static class FormaPago {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected BigInteger tipoPagoCodigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected CodDescStringType tipoCuenta;
                @XmlElement(name = "CuentaDebito")
                protected DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito cuentaDebito;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoPagoCodigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTipoPagoCodigo() {
                    return tipoPagoCodigo;
                }

                /**
                 * Define el valor de la propiedad tipoPagoCodigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTipoPagoCodigo(BigInteger value) {
                    this.tipoPagoCodigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoCuenta.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getTipoCuenta() {
                    return tipoCuenta;
                }

                /**
                 * Define el valor de la propiedad tipoCuenta.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setTipoCuenta(CodDescStringType value) {
                    this.tipoCuenta = value;
                }

                /**
                 * Obtiene el valor de la propiedad cuentaDebito.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito }
                 *     
                 */
                public DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito getCuentaDebito() {
                    return cuentaDebito;
                }

                /**
                 * Define el valor de la propiedad cuentaDebito.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito }
                 *     
                 */
                public void setCuentaDebito(DataConsultaListadoCuentasRespType.Row.CuentaTarjeta.FormaPago.CuentaDebito value) {
                    this.cuentaDebito = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="numeroSucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
                 *         &lt;element name="cuentaNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "numeroSucursal",
                    "cuentaNumero"
                })
                public static class CuentaDebito {

                    @XmlElement(required = true)
                    protected BigInteger numeroSucursal;
                    @XmlElement(required = true)
                    protected String cuentaNumero;

                    /**
                     * Obtiene el valor de la propiedad numeroSucursal.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getNumeroSucursal() {
                        return numeroSucursal;
                    }

                    /**
                     * Define el valor de la propiedad numeroSucursal.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setNumeroSucursal(BigInteger value) {
                        this.numeroSucursal = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad cuentaNumero.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCuentaNumero() {
                        return cuentaNumero;
                    }

                    /**
                     * Define el valor de la propiedad cuentaNumero.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCuentaNumero(String value) {
                        this.cuentaNumero = value;
                    }

                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "marcaCodigo",
                "descripcion"
            })
            public static class GAF {

                @XmlElement(required = true)
                protected BigInteger codigo;
                @XmlElement(required = true)
                protected BigInteger marcaCodigo;
                @XmlElement(required = true)
                protected String descripcion;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCodigo(BigInteger value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad marcaCodigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getMarcaCodigo() {
                    return marcaCodigo;
                }

                /**
                 * Define el valor de la propiedad marcaCodigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setMarcaCodigo(BigInteger value) {
                    this.marcaCodigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="PagoMinimoPesos" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "fechaCierre",
                "fechaVencimiento",
                "saldoPesos",
                "saldoDolares",
                "pagoMinimoPesos"
            })
            public static class LiquidacionActual {

                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaCierre;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaVencimiento;
                @XmlElement(required = true)
                protected BigDecimal saldoPesos;
                @XmlElement(required = true)
                protected BigDecimal saldoDolares;
                @XmlElement(name = "PagoMinimoPesos", required = true)
                protected Object pagoMinimoPesos;

                /**
                 * Obtiene el valor de la propiedad fechaCierre.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaCierre() {
                    return fechaCierre;
                }

                /**
                 * Define el valor de la propiedad fechaCierre.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaCierre(XMLGregorianCalendar value) {
                    this.fechaCierre = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaVencimiento.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaVencimiento() {
                    return fechaVencimiento;
                }

                /**
                 * Define el valor de la propiedad fechaVencimiento.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaVencimiento(XMLGregorianCalendar value) {
                    this.fechaVencimiento = value;
                }

                /**
                 * Obtiene el valor de la propiedad saldoPesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getSaldoPesos() {
                    return saldoPesos;
                }

                /**
                 * Define el valor de la propiedad saldoPesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setSaldoPesos(BigDecimal value) {
                    this.saldoPesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad saldoDolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getSaldoDolares() {
                    return saldoDolares;
                }

                /**
                 * Define el valor de la propiedad saldoDolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setSaldoDolares(BigDecimal value) {
                    this.saldoDolares = value;
                }

                /**
                 * Obtiene el valor de la propiedad pagoMinimoPesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getPagoMinimoPesos() {
                    return pagoMinimoPesos;
                }

                /**
                 * Define el valor de la propiedad pagoMinimoPesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setPagoMinimoPesos(Object value) {
                    this.pagoMinimoPesos = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="fechaCierre" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="saldoPesos" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *         &lt;element name="saldoDolares" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "fechaCierre",
                "fechaVencimiento",
                "saldoPesos",
                "saldoDolares"
            })
            public static class LiquidacionAnterior {

                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaCierre;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaVencimiento;
                @XmlElement(required = true)
                protected BigDecimal saldoPesos;
                @XmlElement(required = true)
                protected BigDecimal saldoDolares;

                /**
                 * Obtiene el valor de la propiedad fechaCierre.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaCierre() {
                    return fechaCierre;
                }

                /**
                 * Define el valor de la propiedad fechaCierre.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaCierre(XMLGregorianCalendar value) {
                    this.fechaCierre = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaVencimiento.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaVencimiento() {
                    return fechaVencimiento;
                }

                /**
                 * Define el valor de la propiedad fechaVencimiento.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaVencimiento(XMLGregorianCalendar value) {
                    this.fechaVencimiento = value;
                }

                /**
                 * Obtiene el valor de la propiedad saldoPesos.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getSaldoPesos() {
                    return saldoPesos;
                }

                /**
                 * Define el valor de la propiedad saldoPesos.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setSaldoPesos(BigDecimal value) {
                    this.saldoPesos = value;
                }

                /**
                 * Obtiene el valor de la propiedad saldoDolares.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getSaldoDolares() {
                    return saldoDolares;
                }

                /**
                 * Define el valor de la propiedad saldoDolares.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setSaldoDolares(BigDecimal value) {
                    this.saldoDolares = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="marcaCodigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "marcaCodigo",
                "descripcion"
            })
            public static class ModeloLiquidacion {

                @XmlElement(required = true)
                protected BigInteger codigo;
                @XmlElement(required = true)
                protected String marcaCodigo;
                @XmlElement(required = true)
                protected Object descripcion;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCodigo(BigInteger value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad marcaCodigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMarcaCodigo() {
                    return marcaCodigo;
                }

                /**
                 * Define el valor de la propiedad marcaCodigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMarcaCodigo(String value) {
                    this.marcaCodigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setDescripcion(Object value) {
                    this.descripcion = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Canal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="SubCanal" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "canal",
                "subCanal"
            })
            public static class OrigenAlta {

                @XmlElement(name = "Canal", required = true)
                protected CodDescNumType canal;
                @XmlElement(name = "SubCanal", required = true)
                protected CodDescNumType subCanal;

                /**
                 * Obtiene el valor de la propiedad canal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getCanal() {
                    return canal;
                }

                /**
                 * Define el valor de la propiedad canal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setCanal(CodDescNumType value) {
                    this.canal = value;
                }

                /**
                 * Obtiene el valor de la propiedad subCanal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getSubCanal() {
                    return subCanal;
                }

                /**
                 * Define el valor de la propiedad subCanal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setSubCanal(CodDescNumType value) {
                    this.subCanal = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "sigla"
            })
            public static class Producto {

                @XmlElement(required = true)
                protected String codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected String sigla;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad sigla.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSigla() {
                    return sigla;
                }

                /**
                 * Define el valor de la propiedad sigla.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSigla(String value) {
                    this.sigla = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
         *         &lt;element name="CuentaCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Producto">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="productoMarca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="oficialComercial" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
         *         &lt;element name="limiteCreditoMarca" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="receptorClaveVisaHome" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Domicilio">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="geoLocalizacionMarca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="codigoEmpresa" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cuartaLineaPlastico" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identificador",
            "cuentaCliente",
            "razonSocial",
            "producto",
            "denominacion",
            "oficialComercial",
            "limiteCreditoMarca",
            "receptorClaveVisaHome",
            "domicilio",
            "codigoEmpresa",
            "cuartaLineaPlastico"
        })
        public static class Empresa {

            @XmlElement(name = "Identificador", required = true)
            protected IdClienteType identificador;
            @XmlElement(name = "CuentaCliente", required = true)
            protected String cuentaCliente;
            @XmlElement(required = true)
            protected String razonSocial;
            @XmlElement(name = "Producto", required = true)
            protected DataConsultaListadoCuentasRespType.Row.Empresa.Producto producto;
            @XmlElement(required = true)
            protected String denominacion;
            @XmlElement(required = true)
            protected CodDescStringType oficialComercial;
            @XmlElement(required = true)
            protected BigDecimal limiteCreditoMarca;
            @XmlElement(required = true)
            protected String receptorClaveVisaHome;
            @XmlElement(name = "Domicilio", required = true)
            protected DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio domicilio;
            @XmlElement(required = true)
            protected String codigoEmpresa;
            @XmlElement(required = true)
            protected Object cuartaLineaPlastico;

            /**
             * Obtiene el valor de la propiedad identificador.
             * 
             * @return
             *     possible object is
             *     {@link IdClienteType }
             *     
             */
            public IdClienteType getIdentificador() {
                return identificador;
            }

            /**
             * Define el valor de la propiedad identificador.
             * 
             * @param value
             *     allowed object is
             *     {@link IdClienteType }
             *     
             */
            public void setIdentificador(IdClienteType value) {
                this.identificador = value;
            }

            /**
             * Obtiene el valor de la propiedad cuentaCliente.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCuentaCliente() {
                return cuentaCliente;
            }

            /**
             * Define el valor de la propiedad cuentaCliente.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCuentaCliente(String value) {
                this.cuentaCliente = value;
            }

            /**
             * Obtiene el valor de la propiedad razonSocial.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRazonSocial() {
                return razonSocial;
            }

            /**
             * Define el valor de la propiedad razonSocial.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRazonSocial(String value) {
                this.razonSocial = value;
            }

            /**
             * Obtiene el valor de la propiedad producto.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.Empresa.Producto }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.Empresa.Producto getProducto() {
                return producto;
            }

            /**
             * Define el valor de la propiedad producto.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.Empresa.Producto }
             *     
             */
            public void setProducto(DataConsultaListadoCuentasRespType.Row.Empresa.Producto value) {
                this.producto = value;
            }

            /**
             * Obtiene el valor de la propiedad denominacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDenominacion() {
                return denominacion;
            }

            /**
             * Define el valor de la propiedad denominacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDenominacion(String value) {
                this.denominacion = value;
            }

            /**
             * Obtiene el valor de la propiedad oficialComercial.
             * 
             * @return
             *     possible object is
             *     {@link CodDescStringType }
             *     
             */
            public CodDescStringType getOficialComercial() {
                return oficialComercial;
            }

            /**
             * Define el valor de la propiedad oficialComercial.
             * 
             * @param value
             *     allowed object is
             *     {@link CodDescStringType }
             *     
             */
            public void setOficialComercial(CodDescStringType value) {
                this.oficialComercial = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCreditoMarca.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getLimiteCreditoMarca() {
                return limiteCreditoMarca;
            }

            /**
             * Define el valor de la propiedad limiteCreditoMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setLimiteCreditoMarca(BigDecimal value) {
                this.limiteCreditoMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad receptorClaveVisaHome.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReceptorClaveVisaHome() {
                return receptorClaveVisaHome;
            }

            /**
             * Define el valor de la propiedad receptorClaveVisaHome.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReceptorClaveVisaHome(String value) {
                this.receptorClaveVisaHome = value;
            }

            /**
             * Obtiene el valor de la propiedad domicilio.
             * 
             * @return
             *     possible object is
             *     {@link DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio }
             *     
             */
            public DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio getDomicilio() {
                return domicilio;
            }

            /**
             * Define el valor de la propiedad domicilio.
             * 
             * @param value
             *     allowed object is
             *     {@link DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio }
             *     
             */
            public void setDomicilio(DataConsultaListadoCuentasRespType.Row.Empresa.Domicilio value) {
                this.domicilio = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoEmpresa.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodigoEmpresa() {
                return codigoEmpresa;
            }

            /**
             * Define el valor de la propiedad codigoEmpresa.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodigoEmpresa(String value) {
                this.codigoEmpresa = value;
            }

            /**
             * Obtiene el valor de la propiedad cuartaLineaPlastico.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getCuartaLineaPlastico() {
                return cuartaLineaPlastico;
            }

            /**
             * Define el valor de la propiedad cuartaLineaPlastico.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setCuartaLineaPlastico(Object value) {
                this.cuartaLineaPlastico = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="geoLocalizacionMarca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="partido" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "calle",
                "numero",
                "piso",
                "referencia",
                "codigoPostal",
                "geoLocalizacionMarca",
                "partido",
                "telefono"
            })
            public static class Domicilio {

                @XmlElement(required = true)
                protected String calle;
                @XmlElement(required = true)
                protected String numero;
                @XmlElement(required = true)
                protected String piso;
                @XmlElement(required = true)
                protected Object referencia;
                @XmlElement(required = true)
                protected String codigoPostal;
                @XmlElement(required = true)
                protected BigInteger geoLocalizacionMarca;
                @XmlElement(required = true)
                protected String partido;
                @XmlElement(required = true)
                protected Object telefono;

                /**
                 * Obtiene el valor de la propiedad calle.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCalle() {
                    return calle;
                }

                /**
                 * Define el valor de la propiedad calle.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCalle(String value) {
                    this.calle = value;
                }

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumero(String value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad piso.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPiso() {
                    return piso;
                }

                /**
                 * Define el valor de la propiedad piso.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPiso(String value) {
                    this.piso = value;
                }

                /**
                 * Obtiene el valor de la propiedad referencia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getReferencia() {
                    return referencia;
                }

                /**
                 * Define el valor de la propiedad referencia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setReferencia(Object value) {
                    this.referencia = value;
                }

                /**
                 * Obtiene el valor de la propiedad codigoPostal.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodigoPostal() {
                    return codigoPostal;
                }

                /**
                 * Define el valor de la propiedad codigoPostal.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodigoPostal(String value) {
                    this.codigoPostal = value;
                }

                /**
                 * Obtiene el valor de la propiedad geoLocalizacionMarca.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getGeoLocalizacionMarca() {
                    return geoLocalizacionMarca;
                }

                /**
                 * Define el valor de la propiedad geoLocalizacionMarca.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setGeoLocalizacionMarca(BigInteger value) {
                    this.geoLocalizacionMarca = value;
                }

                /**
                 * Obtiene el valor de la propiedad partido.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPartido() {
                    return partido;
                }

                /**
                 * Define el valor de la propiedad partido.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPartido(String value) {
                    this.partido = value;
                }

                /**
                 * Obtiene el valor de la propiedad telefono.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getTelefono() {
                    return telefono;
                }

                /**
                 * Define el valor de la propiedad telefono.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setTelefono(Object value) {
                    this.telefono = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="productoMarca" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion",
                "sigla",
                "productoMarca"
            })
            public static class Producto {

                @XmlElement(required = true)
                protected BigInteger codigo;
                @XmlElement(required = true)
                protected String descripcion;
                @XmlElement(required = true)
                protected String sigla;
                @XmlElement(required = true)
                protected CodDescStringType productoMarca;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCodigo(BigInteger value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

                /**
                 * Obtiene el valor de la propiedad sigla.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSigla() {
                    return sigla;
                }

                /**
                 * Define el valor de la propiedad sigla.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSigla(String value) {
                    this.sigla = value;
                }

                /**
                 * Obtiene el valor de la propiedad productoMarca.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public CodDescStringType getProductoMarca() {
                    return productoMarca;
                }

                /**
                 * Define el valor de la propiedad productoMarca.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescStringType }
                 *     
                 */
                public void setProductoMarca(CodDescStringType value) {
                    this.productoMarca = value;
                }

            }

        }

    }

}
