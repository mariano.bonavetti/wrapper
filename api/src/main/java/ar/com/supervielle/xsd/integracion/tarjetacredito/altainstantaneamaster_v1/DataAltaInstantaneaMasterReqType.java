
package ar.com.supervielle.xsd.integracion.tarjetacredito.altainstantaneamaster_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataAltaInstantaneaMasterReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaInstantaneaMasterReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="principal">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="entidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="codigoProducto" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="cuentaBantotal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="tribuDocuTipo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="tribuDocuNume" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="registroAltaBATCH">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="fechaMovimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="apellidoYNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="calleYNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codProvincia" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="codCedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="numCedula" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="codDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="duracionTarjeta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="limiteCredito" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="plazoPagoMinimo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="formaPagoTipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="formaPagoDB" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="formaPagoCTA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="formaPagoSUC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="porcentajeBonificacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="mantenimientoBonificacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="grupoCtaCte" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="garantia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="tipoDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="grupoAfinidad">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="bonifCredito" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="categoria" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="coeficienteLimiteCompra">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                                   &lt;totalDigits value="3"/>
 *                                   &lt;fractionDigits value="2"/>
 *                                   &lt;maxInclusive value="9.99"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="coeficienteAdelanto">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                                   &lt;totalDigits value="3"/>
 *                                   &lt;fractionDigits value="2"/>
 *                                   &lt;maxInclusive value="9.99"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="modeloAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="subModeloTasas" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="subModeloCargosSeguros" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="subModeloParametros" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="numeroCBU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaInstantaneaMasterReqType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/tarjetaCredito/altaInstantaneaMaster-v1", propOrder = {
    "operacion",
    "principal"
})
public class DataAltaInstantaneaMasterReqType {

    @XmlElement(required = true)
    protected BigInteger operacion;
    @XmlElement(required = true)
    protected DataAltaInstantaneaMasterReqType.Principal principal;

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOperacion(BigInteger value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad principal.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaInstantaneaMasterReqType.Principal }
     *     
     */
    public DataAltaInstantaneaMasterReqType.Principal getPrincipal() {
        return principal;
    }

    /**
     * Define el valor de la propiedad principal.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaInstantaneaMasterReqType.Principal }
     *     
     */
    public void setPrincipal(DataAltaInstantaneaMasterReqType.Principal value) {
        this.principal = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="entidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="codigoProducto" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="cuentaBantotal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="tribuDocuTipo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="tribuDocuNume" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="registroAltaBATCH">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="fechaMovimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="apellidoYNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="calleYNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codProvincia" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codCedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="numCedula" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="codDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="numDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="duracionTarjeta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="limiteCredito" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="plazoPagoMinimo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="formaPagoTipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="formaPagoDB" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="formaPagoCTA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="formaPagoSUC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="porcentajeBonificacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="mantenimientoBonificacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="grupoCtaCte" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="garantia" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="tipoDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="grupoAfinidad">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="bonifCredito" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="categoria" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="coeficienteLimiteCompra">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *                         &lt;totalDigits value="3"/>
     *                         &lt;fractionDigits value="2"/>
     *                         &lt;maxInclusive value="9.99"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="coeficienteAdelanto">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *                         &lt;totalDigits value="3"/>
     *                         &lt;fractionDigits value="2"/>
     *                         &lt;maxInclusive value="9.99"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="modeloAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="subModeloTasas" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="subModeloCargosSeguros" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="subModeloParametros" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="numeroCBU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "marca",
        "entidad",
        "codigoProducto",
        "cuentaBantotal",
        "identificador",
        "tribuDocuTipo",
        "tribuDocuNume",
        "registroAltaBATCH",
        "usuarioCarga"
    })
    public static class Principal {

        @XmlElement(required = true)
        protected BigInteger marca;
        @XmlElement(required = true)
        protected BigInteger entidad;
        @XmlElement(required = true)
        protected BigInteger codigoProducto;
        @XmlElement(required = true)
        protected BigInteger cuentaBantotal;
        @XmlElement(required = true)
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected BigInteger tribuDocuTipo;
        @XmlElement(required = true)
        protected String tribuDocuNume;
        @XmlElement(required = true)
        protected DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH registroAltaBATCH;
        protected int usuarioCarga;

        /**
         * Obtiene el valor de la propiedad marca.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMarca() {
            return marca;
        }

        /**
         * Define el valor de la propiedad marca.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMarca(BigInteger value) {
            this.marca = value;
        }

        /**
         * Obtiene el valor de la propiedad entidad.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEntidad() {
            return entidad;
        }

        /**
         * Define el valor de la propiedad entidad.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEntidad(BigInteger value) {
            this.entidad = value;
        }

        /**
         * Obtiene el valor de la propiedad codigoProducto.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCodigoProducto() {
            return codigoProducto;
        }

        /**
         * Define el valor de la propiedad codigoProducto.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCodigoProducto(BigInteger value) {
            this.codigoProducto = value;
        }

        /**
         * Obtiene el valor de la propiedad cuentaBantotal.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCuentaBantotal() {
            return cuentaBantotal;
        }

        /**
         * Define el valor de la propiedad cuentaBantotal.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCuentaBantotal(BigInteger value) {
            this.cuentaBantotal = value;
        }

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad tribuDocuTipo.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTribuDocuTipo() {
            return tribuDocuTipo;
        }

        /**
         * Define el valor de la propiedad tribuDocuTipo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTribuDocuTipo(BigInteger value) {
            this.tribuDocuTipo = value;
        }

        /**
         * Obtiene el valor de la propiedad tribuDocuNume.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTribuDocuNume() {
            return tribuDocuNume;
        }

        /**
         * Define el valor de la propiedad tribuDocuNume.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTribuDocuNume(String value) {
            this.tribuDocuNume = value;
        }

        /**
         * Obtiene el valor de la propiedad registroAltaBATCH.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH }
         *     
         */
        public DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH getRegistroAltaBATCH() {
            return registroAltaBATCH;
        }

        /**
         * Define el valor de la propiedad registroAltaBATCH.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH }
         *     
         */
        public void setRegistroAltaBATCH(DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH value) {
            this.registroAltaBATCH = value;
        }

        /**
         * Obtiene el valor de la propiedad usuarioCarga.
         * 
         */
        public int getUsuarioCarga() {
            return usuarioCarga;
        }

        /**
         * Define el valor de la propiedad usuarioCarga.
         * 
         */
        public void setUsuarioCarga(int value) {
            this.usuarioCarga = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="fechaMovimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="apellidoYNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="calleYNumero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codProvincia" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codCedula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="numCedula" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="codDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="numDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="nacionalidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="duracionTarjeta" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="limiteCredito" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="plazoPagoMinimo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="formaPagoTipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="formaPagoDB" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="formaPagoCTA" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="formaPagoSUC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="porcentajeBonificacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="mantenimientoBonificacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="grupoCtaCte" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="garantia" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="ocupacion" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="tipoDomicilio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="grupoAfinidad">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="bonifCredito" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="cuit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="categoria" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="coeficienteLimiteCompra">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
         *               &lt;totalDigits value="3"/>
         *               &lt;fractionDigits value="2"/>
         *               &lt;maxInclusive value="9.99"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="coeficienteAdelanto">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
         *               &lt;totalDigits value="3"/>
         *               &lt;fractionDigits value="2"/>
         *               &lt;maxInclusive value="9.99"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="modeloAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="modeloLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="subModeloTasas" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="subModeloCargosSeguros" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="subModeloParametros" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="numeroCBU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fechaMovimiento",
            "sucursal",
            "apellidoYNombre",
            "calleYNumero",
            "piso",
            "departamento",
            "codigoPostal",
            "localidad",
            "codProvincia",
            "telefono",
            "codCedula",
            "numCedula",
            "codDocumento",
            "numDocumento",
            "fechaNacimiento",
            "nacionalidad",
            "sexo",
            "duracionTarjeta",
            "limiteCompra",
            "limiteCredito",
            "plazoPagoMinimo",
            "formaPagoTipo",
            "formaPagoDB",
            "formaPagoCTA",
            "formaPagoSUC",
            "cantCuotas",
            "porcentajeBonificacion",
            "mantenimientoBonificacion",
            "grupoCtaCte",
            "garantia",
            "ocupacion",
            "tipoDomicilio",
            "grupoAfinidad",
            "bonifCredito",
            "cuit",
            "categoria",
            "coeficienteLimiteCompra",
            "coeficienteAdelanto",
            "modeloAutorizacion",
            "modeloLiquidacion",
            "subModeloTasas",
            "subModeloCargosSeguros",
            "subModeloParametros",
            "numeroCBU",
            "codProducto"
        })
        public static class RegistroAltaBATCH {

            @XmlElement(required = true)
            protected BigInteger fechaMovimiento;
            @XmlElement(required = true)
            protected BigInteger sucursal;
            @XmlElement(required = true)
            protected String apellidoYNombre;
            @XmlElement(required = true)
            protected String calleYNumero;
            @XmlElement(required = true)
            protected String piso;
            @XmlElement(required = true)
            protected String departamento;
            @XmlElement(required = true)
            protected BigInteger codigoPostal;
            @XmlElement(required = true)
            protected String localidad;
            @XmlElement(required = true)
            protected BigInteger codProvincia;
            protected String telefono;
            protected String codCedula;
            protected BigInteger numCedula;
            @XmlElement(required = true)
            protected String codDocumento;
            @XmlElement(required = true)
            protected BigInteger numDocumento;
            @XmlElement(required = true)
            protected BigInteger fechaNacimiento;
            @XmlElement(required = true)
            protected String nacionalidad;
            @XmlElement(required = true)
            protected String sexo;
            @XmlElement(required = true)
            protected BigInteger duracionTarjeta;
            @XmlElement(required = true)
            protected String limiteCompra;
            protected BigInteger limiteCredito;
            @XmlElement(required = true)
            protected BigInteger plazoPagoMinimo;
            @XmlElement(required = true)
            protected String formaPagoTipo;
            protected BigInteger formaPagoDB;
            protected BigInteger formaPagoCTA;
            protected BigInteger formaPagoSUC;
            protected int cantCuotas;
            protected int porcentajeBonificacion;
            protected int mantenimientoBonificacion;
            protected int grupoCtaCte;
            protected int garantia;
            protected int ocupacion;
            @XmlElement(required = true)
            protected String tipoDomicilio;
            @XmlElement(required = true)
            protected DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad grupoAfinidad;
            @XmlElement(required = true)
            protected BigInteger bonifCredito;
            @XmlElement(required = true)
            protected BigInteger cuit;
            @XmlElement(required = true)
            protected BigInteger categoria;
            @XmlElement(required = true)
            protected BigDecimal coeficienteLimiteCompra;
            @XmlElement(required = true)
            protected BigDecimal coeficienteAdelanto;
            protected String modeloAutorizacion;
            protected String modeloLiquidacion;
            protected BigInteger subModeloTasas;
            protected BigInteger subModeloCargosSeguros;
            protected BigInteger subModeloParametros;
            protected String numeroCBU;
            protected BigInteger codProducto;

            /**
             * Obtiene el valor de la propiedad fechaMovimiento.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFechaMovimiento() {
                return fechaMovimiento;
            }

            /**
             * Define el valor de la propiedad fechaMovimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFechaMovimiento(BigInteger value) {
                this.fechaMovimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad sucursal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSucursal() {
                return sucursal;
            }

            /**
             * Define el valor de la propiedad sucursal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSucursal(BigInteger value) {
                this.sucursal = value;
            }

            /**
             * Obtiene el valor de la propiedad apellidoYNombre.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApellidoYNombre() {
                return apellidoYNombre;
            }

            /**
             * Define el valor de la propiedad apellidoYNombre.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApellidoYNombre(String value) {
                this.apellidoYNombre = value;
            }

            /**
             * Obtiene el valor de la propiedad calleYNumero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalleYNumero() {
                return calleYNumero;
            }

            /**
             * Define el valor de la propiedad calleYNumero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalleYNumero(String value) {
                this.calleYNumero = value;
            }

            /**
             * Obtiene el valor de la propiedad piso.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPiso() {
                return piso;
            }

            /**
             * Define el valor de la propiedad piso.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPiso(String value) {
                this.piso = value;
            }

            /**
             * Obtiene el valor de la propiedad departamento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDepartamento() {
                return departamento;
            }

            /**
             * Define el valor de la propiedad departamento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDepartamento(String value) {
                this.departamento = value;
            }

            /**
             * Obtiene el valor de la propiedad codigoPostal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCodigoPostal() {
                return codigoPostal;
            }

            /**
             * Define el valor de la propiedad codigoPostal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCodigoPostal(BigInteger value) {
                this.codigoPostal = value;
            }

            /**
             * Obtiene el valor de la propiedad localidad.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocalidad() {
                return localidad;
            }

            /**
             * Define el valor de la propiedad localidad.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocalidad(String value) {
                this.localidad = value;
            }

            /**
             * Obtiene el valor de la propiedad codProvincia.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCodProvincia() {
                return codProvincia;
            }

            /**
             * Define el valor de la propiedad codProvincia.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCodProvincia(BigInteger value) {
                this.codProvincia = value;
            }

            /**
             * Obtiene el valor de la propiedad telefono.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTelefono() {
                return telefono;
            }

            /**
             * Define el valor de la propiedad telefono.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTelefono(String value) {
                this.telefono = value;
            }

            /**
             * Obtiene el valor de la propiedad codCedula.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodCedula() {
                return codCedula;
            }

            /**
             * Define el valor de la propiedad codCedula.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodCedula(String value) {
                this.codCedula = value;
            }

            /**
             * Obtiene el valor de la propiedad numCedula.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNumCedula() {
                return numCedula;
            }

            /**
             * Define el valor de la propiedad numCedula.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNumCedula(BigInteger value) {
                this.numCedula = value;
            }

            /**
             * Obtiene el valor de la propiedad codDocumento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodDocumento() {
                return codDocumento;
            }

            /**
             * Define el valor de la propiedad codDocumento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodDocumento(String value) {
                this.codDocumento = value;
            }

            /**
             * Obtiene el valor de la propiedad numDocumento.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNumDocumento() {
                return numDocumento;
            }

            /**
             * Define el valor de la propiedad numDocumento.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNumDocumento(BigInteger value) {
                this.numDocumento = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaNacimiento.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFechaNacimiento() {
                return fechaNacimiento;
            }

            /**
             * Define el valor de la propiedad fechaNacimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFechaNacimiento(BigInteger value) {
                this.fechaNacimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad nacionalidad.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNacionalidad() {
                return nacionalidad;
            }

            /**
             * Define el valor de la propiedad nacionalidad.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNacionalidad(String value) {
                this.nacionalidad = value;
            }

            /**
             * Obtiene el valor de la propiedad sexo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSexo() {
                return sexo;
            }

            /**
             * Define el valor de la propiedad sexo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSexo(String value) {
                this.sexo = value;
            }

            /**
             * Obtiene el valor de la propiedad duracionTarjeta.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDuracionTarjeta() {
                return duracionTarjeta;
            }

            /**
             * Define el valor de la propiedad duracionTarjeta.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDuracionTarjeta(BigInteger value) {
                this.duracionTarjeta = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLimiteCompra() {
                return limiteCompra;
            }

            /**
             * Define el valor de la propiedad limiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLimiteCompra(String value) {
                this.limiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad limiteCredito.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getLimiteCredito() {
                return limiteCredito;
            }

            /**
             * Define el valor de la propiedad limiteCredito.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setLimiteCredito(BigInteger value) {
                this.limiteCredito = value;
            }

            /**
             * Obtiene el valor de la propiedad plazoPagoMinimo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPlazoPagoMinimo() {
                return plazoPagoMinimo;
            }

            /**
             * Define el valor de la propiedad plazoPagoMinimo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPlazoPagoMinimo(BigInteger value) {
                this.plazoPagoMinimo = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPagoTipo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFormaPagoTipo() {
                return formaPagoTipo;
            }

            /**
             * Define el valor de la propiedad formaPagoTipo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFormaPagoTipo(String value) {
                this.formaPagoTipo = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPagoDB.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFormaPagoDB() {
                return formaPagoDB;
            }

            /**
             * Define el valor de la propiedad formaPagoDB.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFormaPagoDB(BigInteger value) {
                this.formaPagoDB = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPagoCTA.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFormaPagoCTA() {
                return formaPagoCTA;
            }

            /**
             * Define el valor de la propiedad formaPagoCTA.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFormaPagoCTA(BigInteger value) {
                this.formaPagoCTA = value;
            }

            /**
             * Obtiene el valor de la propiedad formaPagoSUC.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getFormaPagoSUC() {
                return formaPagoSUC;
            }

            /**
             * Define el valor de la propiedad formaPagoSUC.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setFormaPagoSUC(BigInteger value) {
                this.formaPagoSUC = value;
            }

            /**
             * Obtiene el valor de la propiedad cantCuotas.
             * 
             */
            public int getCantCuotas() {
                return cantCuotas;
            }

            /**
             * Define el valor de la propiedad cantCuotas.
             * 
             */
            public void setCantCuotas(int value) {
                this.cantCuotas = value;
            }

            /**
             * Obtiene el valor de la propiedad porcentajeBonificacion.
             * 
             */
            public int getPorcentajeBonificacion() {
                return porcentajeBonificacion;
            }

            /**
             * Define el valor de la propiedad porcentajeBonificacion.
             * 
             */
            public void setPorcentajeBonificacion(int value) {
                this.porcentajeBonificacion = value;
            }

            /**
             * Obtiene el valor de la propiedad mantenimientoBonificacion.
             * 
             */
            public int getMantenimientoBonificacion() {
                return mantenimientoBonificacion;
            }

            /**
             * Define el valor de la propiedad mantenimientoBonificacion.
             * 
             */
            public void setMantenimientoBonificacion(int value) {
                this.mantenimientoBonificacion = value;
            }

            /**
             * Obtiene el valor de la propiedad grupoCtaCte.
             * 
             */
            public int getGrupoCtaCte() {
                return grupoCtaCte;
            }

            /**
             * Define el valor de la propiedad grupoCtaCte.
             * 
             */
            public void setGrupoCtaCte(int value) {
                this.grupoCtaCte = value;
            }

            /**
             * Obtiene el valor de la propiedad garantia.
             * 
             */
            public int getGarantia() {
                return garantia;
            }

            /**
             * Define el valor de la propiedad garantia.
             * 
             */
            public void setGarantia(int value) {
                this.garantia = value;
            }

            /**
             * Obtiene el valor de la propiedad ocupacion.
             * 
             */
            public int getOcupacion() {
                return ocupacion;
            }

            /**
             * Define el valor de la propiedad ocupacion.
             * 
             */
            public void setOcupacion(int value) {
                this.ocupacion = value;
            }

            /**
             * Obtiene el valor de la propiedad tipoDomicilio.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipoDomicilio() {
                return tipoDomicilio;
            }

            /**
             * Define el valor de la propiedad tipoDomicilio.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipoDomicilio(String value) {
                this.tipoDomicilio = value;
            }

            /**
             * Obtiene el valor de la propiedad grupoAfinidad.
             * 
             * @return
             *     possible object is
             *     {@link DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad }
             *     
             */
            public DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad getGrupoAfinidad() {
                return grupoAfinidad;
            }

            /**
             * Define el valor de la propiedad grupoAfinidad.
             * 
             * @param value
             *     allowed object is
             *     {@link DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad }
             *     
             */
            public void setGrupoAfinidad(DataAltaInstantaneaMasterReqType.Principal.RegistroAltaBATCH.GrupoAfinidad value) {
                this.grupoAfinidad = value;
            }

            /**
             * Obtiene el valor de la propiedad bonifCredito.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getBonifCredito() {
                return bonifCredito;
            }

            /**
             * Define el valor de la propiedad bonifCredito.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setBonifCredito(BigInteger value) {
                this.bonifCredito = value;
            }

            /**
             * Obtiene el valor de la propiedad cuit.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCuit() {
                return cuit;
            }

            /**
             * Define el valor de la propiedad cuit.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCuit(BigInteger value) {
                this.cuit = value;
            }

            /**
             * Obtiene el valor de la propiedad categoria.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCategoria() {
                return categoria;
            }

            /**
             * Define el valor de la propiedad categoria.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCategoria(BigInteger value) {
                this.categoria = value;
            }

            /**
             * Obtiene el valor de la propiedad coeficienteLimiteCompra.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCoeficienteLimiteCompra() {
                return coeficienteLimiteCompra;
            }

            /**
             * Define el valor de la propiedad coeficienteLimiteCompra.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCoeficienteLimiteCompra(BigDecimal value) {
                this.coeficienteLimiteCompra = value;
            }

            /**
             * Obtiene el valor de la propiedad coeficienteAdelanto.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCoeficienteAdelanto() {
                return coeficienteAdelanto;
            }

            /**
             * Define el valor de la propiedad coeficienteAdelanto.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCoeficienteAdelanto(BigDecimal value) {
                this.coeficienteAdelanto = value;
            }

            /**
             * Obtiene el valor de la propiedad modeloAutorizacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModeloAutorizacion() {
                return modeloAutorizacion;
            }

            /**
             * Define el valor de la propiedad modeloAutorizacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModeloAutorizacion(String value) {
                this.modeloAutorizacion = value;
            }

            /**
             * Obtiene el valor de la propiedad modeloLiquidacion.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModeloLiquidacion() {
                return modeloLiquidacion;
            }

            /**
             * Define el valor de la propiedad modeloLiquidacion.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModeloLiquidacion(String value) {
                this.modeloLiquidacion = value;
            }

            /**
             * Obtiene el valor de la propiedad subModeloTasas.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubModeloTasas() {
                return subModeloTasas;
            }

            /**
             * Define el valor de la propiedad subModeloTasas.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubModeloTasas(BigInteger value) {
                this.subModeloTasas = value;
            }

            /**
             * Obtiene el valor de la propiedad subModeloCargosSeguros.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubModeloCargosSeguros() {
                return subModeloCargosSeguros;
            }

            /**
             * Define el valor de la propiedad subModeloCargosSeguros.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubModeloCargosSeguros(BigInteger value) {
                this.subModeloCargosSeguros = value;
            }

            /**
             * Obtiene el valor de la propiedad subModeloParametros.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubModeloParametros() {
                return subModeloParametros;
            }

            /**
             * Define el valor de la propiedad subModeloParametros.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubModeloParametros(BigInteger value) {
                this.subModeloParametros = value;
            }

            /**
             * Obtiene el valor de la propiedad numeroCBU.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumeroCBU() {
                return numeroCBU;
            }

            /**
             * Define el valor de la propiedad numeroCBU.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumeroCBU(String value) {
                this.numeroCBU = value;
            }

            /**
             * Obtiene el valor de la propiedad codProducto.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCodProducto() {
                return codProducto;
            }

            /**
             * Define el valor de la propiedad codProducto.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCodProducto(BigInteger value) {
                this.codProducto = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "descripcion"
            })
            public static class GrupoAfinidad {

                @XmlElement(required = true)
                protected BigInteger codigo;
                protected String descripcion;

                /**
                 * Obtiene el valor de la propiedad codigo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCodigo() {
                    return codigo;
                }

                /**
                 * Define el valor de la propiedad codigo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCodigo(BigInteger value) {
                    this.codigo = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcion() {
                    return descripcion;
                }

                /**
                 * Define el valor de la propiedad descripcion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcion(String value) {
                    this.descripcion = value;
                }

            }

        }

    }

}
