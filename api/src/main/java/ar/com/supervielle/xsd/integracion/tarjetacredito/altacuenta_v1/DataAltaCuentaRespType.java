
package ar.com.supervielle.xsd.integracion.tarjetacredito.altacuenta_v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;


/**
 * <p>Clase Java para DataAltaCuentaRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaCuentaRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="SolicitudesGeneradas">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Solicitud" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaCuentaRespType", propOrder = {
    "row"
})
public class DataAltaCuentaRespType {

    @XmlElement(name = "Row")
    protected List<DataAltaCuentaRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataAltaCuentaRespType.Row }
     * 
     * 
     */
    public List<DataAltaCuentaRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataAltaCuentaRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="SolicitudesGeneradas">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Solicitud" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nroSolicitudGrupo",
        "solicitudesGeneradas"
    })
    public static class Row {

        @XmlElement(required = true)
        protected BigInteger nroSolicitudGrupo;
        @XmlElement(name = "SolicitudesGeneradas", required = true)
        protected DataAltaCuentaRespType.Row.SolicitudesGeneradas solicitudesGeneradas;

        /**
         * Obtiene el valor de la propiedad nroSolicitudGrupo.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getNroSolicitudGrupo() {
            return nroSolicitudGrupo;
        }

        /**
         * Define el valor de la propiedad nroSolicitudGrupo.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setNroSolicitudGrupo(BigInteger value) {
            this.nroSolicitudGrupo = value;
        }

        /**
         * Obtiene el valor de la propiedad solicitudesGeneradas.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaCuentaRespType.Row.SolicitudesGeneradas }
         *     
         */
        public DataAltaCuentaRespType.Row.SolicitudesGeneradas getSolicitudesGeneradas() {
            return solicitudesGeneradas;
        }

        /**
         * Define el valor de la propiedad solicitudesGeneradas.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaCuentaRespType.Row.SolicitudesGeneradas }
         *     
         */
        public void setSolicitudesGeneradas(DataAltaCuentaRespType.Row.SolicitudesGeneradas value) {
            this.solicitudesGeneradas = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Solicitud" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "solicitud"
        })
        public static class SolicitudesGeneradas {

            @XmlElement(name = "Solicitud", required = true)
            protected List<DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud> solicitud;

            /**
             * Gets the value of the solicitud property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the solicitud property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSolicitud().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud }
             * 
             * 
             */
            public List<DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud> getSolicitud() {
                if (solicitud == null) {
                    solicitud = new ArrayList<DataAltaCuentaRespType.Row.SolicitudesGeneradas.Solicitud>();
                }
                return this.solicitud;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="tipo" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "numero",
                "tipo"
            })
            public static class Solicitud {

                @XmlElement(required = true)
                protected BigInteger numero;
                @XmlElement(required = true)
                protected CodDescNumType tipo;

                /**
                 * Obtiene el valor de la propiedad numero.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getNumero() {
                    return numero;
                }

                /**
                 * Define el valor de la propiedad numero.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setNumero(BigInteger value) {
                    this.numero = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getTipo() {
                    return tipo;
                }

                /**
                 * Define el valor de la propiedad tipo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setTipo(CodDescNumType value) {
                    this.tipo = value;
                }

            }

        }

    }

}
