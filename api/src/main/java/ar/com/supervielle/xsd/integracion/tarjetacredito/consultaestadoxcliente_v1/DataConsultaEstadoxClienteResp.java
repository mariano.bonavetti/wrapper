
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaestadoxcliente_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaEstadoxClienteResp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaEstadoxClienteResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="marca">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="nomMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nroPlastico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="historial">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="registro" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="estado" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="error" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="fechaEst" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaEstadoxClienteResp", propOrder = {
    "row"
})
public class DataConsultaEstadoxClienteResp {

    @XmlElement(name = "Row")
    protected DataConsultaEstadoxClienteResp.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaEstadoxClienteResp.Row }
     *     
     */
    public DataConsultaEstadoxClienteResp.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaEstadoxClienteResp.Row }
     *     
     */
    public void setRow(DataConsultaEstadoxClienteResp.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="marca">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nomMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nroPlastico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="historial">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="registro" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="estado" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="error" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="fechaEst" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "marca",
        "nroPlastico",
        "historial"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdClienteType identificador;
        @XmlElement(required = true)
        protected DataConsultaEstadoxClienteResp.Row.Marca marca;
        protected String nroPlastico;
        @XmlElement(required = true)
        protected DataConsultaEstadoxClienteResp.Row.Historial historial;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad marca.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaEstadoxClienteResp.Row.Marca }
         *     
         */
        public DataConsultaEstadoxClienteResp.Row.Marca getMarca() {
            return marca;
        }

        /**
         * Define el valor de la propiedad marca.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaEstadoxClienteResp.Row.Marca }
         *     
         */
        public void setMarca(DataConsultaEstadoxClienteResp.Row.Marca value) {
            this.marca = value;
        }

        /**
         * Obtiene el valor de la propiedad nroPlastico.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNroPlastico() {
            return nroPlastico;
        }

        /**
         * Define el valor de la propiedad nroPlastico.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNroPlastico(String value) {
            this.nroPlastico = value;
        }

        /**
         * Obtiene el valor de la propiedad historial.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaEstadoxClienteResp.Row.Historial }
         *     
         */
        public DataConsultaEstadoxClienteResp.Row.Historial getHistorial() {
            return historial;
        }

        /**
         * Define el valor de la propiedad historial.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaEstadoxClienteResp.Row.Historial }
         *     
         */
        public void setHistorial(DataConsultaEstadoxClienteResp.Row.Historial value) {
            this.historial = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="registro" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="estado" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="error" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="fechaEst" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "registro"
        })
        public static class Historial {

            protected List<DataConsultaEstadoxClienteResp.Row.Historial.Registro> registro;

            /**
             * Gets the value of the registro property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the registro property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRegistro().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro }
             * 
             * 
             */
            public List<DataConsultaEstadoxClienteResp.Row.Historial.Registro> getRegistro() {
                if (registro == null) {
                    registro = new ArrayList<DataConsultaEstadoxClienteResp.Row.Historial.Registro>();
                }
                return this.registro;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="estado" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="error" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="fechaEst" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "estado",
                "error",
                "fechaEst"
            })
            public static class Registro {

                protected DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado estado;
                protected DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error error;
                @XmlSchemaType(name = "dateTime")
                protected XMLGregorianCalendar fechaEst;

                /**
                 * Obtiene el valor de la propiedad estado.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado }
                 *     
                 */
                public DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado getEstado() {
                    return estado;
                }

                /**
                 * Define el valor de la propiedad estado.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado }
                 *     
                 */
                public void setEstado(DataConsultaEstadoxClienteResp.Row.Historial.Registro.Estado value) {
                    this.estado = value;
                }

                /**
                 * Obtiene el valor de la propiedad error.
                 * 
                 * @return
                 *     possible object is
                 *     {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error }
                 *     
                 */
                public DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error getError() {
                    return error;
                }

                /**
                 * Define el valor de la propiedad error.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error }
                 *     
                 */
                public void setError(DataConsultaEstadoxClienteResp.Row.Historial.Registro.Error value) {
                    this.error = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaEst.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaEst() {
                    return fechaEst;
                }

                /**
                 * Define el valor de la propiedad fechaEst.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaEst(XMLGregorianCalendar value) {
                    this.fechaEst = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "codigo",
                    "descripcion"
                })
                public static class Error {

                    protected int codigo;
                    @XmlElement(required = true)
                    protected String descripcion;

                    /**
                     * Obtiene el valor de la propiedad codigo.
                     * 
                     */
                    public int getCodigo() {
                        return codigo;
                    }

                    /**
                     * Define el valor de la propiedad codigo.
                     * 
                     */
                    public void setCodigo(int value) {
                        this.codigo = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad descripcion.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDescripcion() {
                        return descripcion;
                    }

                    /**
                     * Define el valor de la propiedad descripcion.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDescripcion(String value) {
                        this.descripcion = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "codigo",
                    "descripcion"
                })
                public static class Estado {

                    protected int codigo;
                    @XmlElement(required = true)
                    protected String descripcion;

                    /**
                     * Obtiene el valor de la propiedad codigo.
                     * 
                     */
                    public int getCodigo() {
                        return codigo;
                    }

                    /**
                     * Define el valor de la propiedad codigo.
                     * 
                     */
                    public void setCodigo(int value) {
                        this.codigo = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad descripcion.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDescripcion() {
                        return descripcion;
                    }

                    /**
                     * Define el valor de la propiedad descripcion.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDescripcion(String value) {
                        this.descripcion = value;
                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nomMarca" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="codMarca" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nomMarca",
            "codMarca"
        })
        public static class Marca {

            @XmlElement(required = true)
            protected String nomMarca;
            protected int codMarca;

            /**
             * Obtiene el valor de la propiedad nomMarca.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNomMarca() {
                return nomMarca;
            }

            /**
             * Define el valor de la propiedad nomMarca.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNomMarca(String value) {
                this.nomMarca = value;
            }

            /**
             * Obtiene el valor de la propiedad codMarca.
             * 
             */
            public int getCodMarca() {
                return codMarca;
            }

            /**
             * Define el valor de la propiedad codMarca.
             * 
             */
            public void setCodMarca(int value) {
                this.codMarca = value;
            }

        }

    }

}
