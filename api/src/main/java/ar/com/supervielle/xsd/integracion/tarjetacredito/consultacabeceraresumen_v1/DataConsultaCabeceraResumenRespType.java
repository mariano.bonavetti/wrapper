
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultacabeceraresumen_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;


/**
 * <p>Clase Java para DataConsultaCabeceraResumenRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaCabeceraResumenRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdTarjetaType"/>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="vencimientoActual" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="vencimientoAnterior" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="fechaUltimoPago" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="cierreAnterior" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="cierreActual" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="saldoActualPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldoActualDolar" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="pagoMinimoActual" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldoAnteriorPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="saldoAnteriorDolar" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="proximoVencimiento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="pagoMinimoAnterior" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="comprasAnterioresPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="comprasAnterioresDolar" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="limiteCredito" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaCabeceraResumenRespType", propOrder = {
    "identificador",
    "row"
})
public class DataConsultaCabeceraResumenRespType {

    @XmlElement(required = true)
    protected IdTarjetaType identificador;
    @XmlElement(name = "Row")
    protected List<DataConsultaCabeceraResumenRespType.Row> row;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link IdTarjetaType }
     *     
     */
    public IdTarjetaType getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link IdTarjetaType }
     *     
     */
    public void setIdentificador(IdTarjetaType value) {
        this.identificador = value;
    }

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaCabeceraResumenRespType.Row }
     * 
     * 
     */
    public List<DataConsultaCabeceraResumenRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaCabeceraResumenRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="vencimientoActual" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="vencimientoAnterior" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="fechaUltimoPago" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="cierreAnterior" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="cierreActual" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="saldoActualPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldoActualDolar" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="pagoMinimoActual" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldoAnteriorPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="saldoAnteriorDolar" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="proximoVencimiento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="pagoMinimoAnterior" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="comprasAnterioresPesos" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="comprasAnterioresDolar" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteCompra" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="limiteCredito" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vencimientoActual",
        "vencimientoAnterior",
        "fechaUltimoPago",
        "cierreAnterior",
        "cierreActual",
        "saldoActualPesos",
        "saldoActualDolar",
        "pagoMinimoActual",
        "saldoAnteriorPesos",
        "saldoAnteriorDolar",
        "proximoVencimiento",
        "pagoMinimoAnterior",
        "comprasAnterioresPesos",
        "comprasAnterioresDolar",
        "limiteCompra",
        "limiteCredito"
    })
    public static class Row {

        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar vencimientoActual;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar vencimientoAnterior;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar fechaUltimoPago;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar cierreAnterior;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar cierreActual;
        protected BigDecimal saldoActualPesos;
        protected BigDecimal saldoActualDolar;
        protected BigDecimal pagoMinimoActual;
        protected BigDecimal saldoAnteriorPesos;
        protected BigDecimal saldoAnteriorDolar;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar proximoVencimiento;
        protected BigDecimal pagoMinimoAnterior;
        protected BigDecimal comprasAnterioresPesos;
        protected BigDecimal comprasAnterioresDolar;
        protected BigDecimal limiteCompra;
        protected BigDecimal limiteCredito;

        /**
         * Obtiene el valor de la propiedad vencimientoActual.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVencimientoActual() {
            return vencimientoActual;
        }

        /**
         * Define el valor de la propiedad vencimientoActual.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVencimientoActual(XMLGregorianCalendar value) {
            this.vencimientoActual = value;
        }

        /**
         * Obtiene el valor de la propiedad vencimientoAnterior.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVencimientoAnterior() {
            return vencimientoAnterior;
        }

        /**
         * Define el valor de la propiedad vencimientoAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVencimientoAnterior(XMLGregorianCalendar value) {
            this.vencimientoAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad fechaUltimoPago.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFechaUltimoPago() {
            return fechaUltimoPago;
        }

        /**
         * Define el valor de la propiedad fechaUltimoPago.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFechaUltimoPago(XMLGregorianCalendar value) {
            this.fechaUltimoPago = value;
        }

        /**
         * Obtiene el valor de la propiedad cierreAnterior.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCierreAnterior() {
            return cierreAnterior;
        }

        /**
         * Define el valor de la propiedad cierreAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCierreAnterior(XMLGregorianCalendar value) {
            this.cierreAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad cierreActual.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCierreActual() {
            return cierreActual;
        }

        /**
         * Define el valor de la propiedad cierreActual.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCierreActual(XMLGregorianCalendar value) {
            this.cierreActual = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoActualPesos.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoActualPesos() {
            return saldoActualPesos;
        }

        /**
         * Define el valor de la propiedad saldoActualPesos.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoActualPesos(BigDecimal value) {
            this.saldoActualPesos = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoActualDolar.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoActualDolar() {
            return saldoActualDolar;
        }

        /**
         * Define el valor de la propiedad saldoActualDolar.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoActualDolar(BigDecimal value) {
            this.saldoActualDolar = value;
        }

        /**
         * Obtiene el valor de la propiedad pagoMinimoActual.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPagoMinimoActual() {
            return pagoMinimoActual;
        }

        /**
         * Define el valor de la propiedad pagoMinimoActual.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPagoMinimoActual(BigDecimal value) {
            this.pagoMinimoActual = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoAnteriorPesos.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoAnteriorPesos() {
            return saldoAnteriorPesos;
        }

        /**
         * Define el valor de la propiedad saldoAnteriorPesos.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoAnteriorPesos(BigDecimal value) {
            this.saldoAnteriorPesos = value;
        }

        /**
         * Obtiene el valor de la propiedad saldoAnteriorDolar.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSaldoAnteriorDolar() {
            return saldoAnteriorDolar;
        }

        /**
         * Define el valor de la propiedad saldoAnteriorDolar.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSaldoAnteriorDolar(BigDecimal value) {
            this.saldoAnteriorDolar = value;
        }

        /**
         * Obtiene el valor de la propiedad proximoVencimiento.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getProximoVencimiento() {
            return proximoVencimiento;
        }

        /**
         * Define el valor de la propiedad proximoVencimiento.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setProximoVencimiento(XMLGregorianCalendar value) {
            this.proximoVencimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad pagoMinimoAnterior.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPagoMinimoAnterior() {
            return pagoMinimoAnterior;
        }

        /**
         * Define el valor de la propiedad pagoMinimoAnterior.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPagoMinimoAnterior(BigDecimal value) {
            this.pagoMinimoAnterior = value;
        }

        /**
         * Obtiene el valor de la propiedad comprasAnterioresPesos.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getComprasAnterioresPesos() {
            return comprasAnterioresPesos;
        }

        /**
         * Define el valor de la propiedad comprasAnterioresPesos.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setComprasAnterioresPesos(BigDecimal value) {
            this.comprasAnterioresPesos = value;
        }

        /**
         * Obtiene el valor de la propiedad comprasAnterioresDolar.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getComprasAnterioresDolar() {
            return comprasAnterioresDolar;
        }

        /**
         * Define el valor de la propiedad comprasAnterioresDolar.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setComprasAnterioresDolar(BigDecimal value) {
            this.comprasAnterioresDolar = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCompra.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCompra() {
            return limiteCompra;
        }

        /**
         * Define el valor de la propiedad limiteCompra.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCompra(BigDecimal value) {
            this.limiteCompra = value;
        }

        /**
         * Obtiene el valor de la propiedad limiteCredito.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLimiteCredito() {
            return limiteCredito;
        }

        /**
         * Define el valor de la propiedad limiteCredito.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLimiteCredito(BigDecimal value) {
            this.limiteCredito = value;
        }

    }

}
