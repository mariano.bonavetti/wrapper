
package ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacategorias_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacategorias_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacategorias_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCategoriasRespType }
     * 
     */
    public DataConsultaCategoriasRespType createDataConsultaCategoriasRespType() {
        return new DataConsultaCategoriasRespType();
    }

    /**
     * Create an instance of {@link DataConsultaCategoriasRespType.Row }
     * 
     */
    public DataConsultaCategoriasRespType.Row createDataConsultaCategoriasRespTypeRow() {
        return new DataConsultaCategoriasRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaCategoriasRespType.Row.Categorias }
     * 
     */
    public DataConsultaCategoriasRespType.Row.Categorias createDataConsultaCategoriasRespTypeRowCategorias() {
        return new DataConsultaCategoriasRespType.Row.Categorias();
    }

    /**
     * Create an instance of {@link RespConsultaCategorias }
     * 
     */
    public RespConsultaCategorias createRespConsultaCategorias() {
        return new RespConsultaCategorias();
    }

    /**
     * Create an instance of {@link ReqConsultaCategorias }
     * 
     */
    public ReqConsultaCategorias createReqConsultaCategorias() {
        return new ReqConsultaCategorias();
    }

    /**
     * Create an instance of {@link DataConsultaCategoriasReqType }
     * 
     */
    public DataConsultaCategoriasReqType createDataConsultaCategoriasReqType() {
        return new DataConsultaCategoriasReqType();
    }

    /**
     * Create an instance of {@link DataConsultaCategoriasRespType.Row.Categorias.Categoria }
     * 
     */
    public DataConsultaCategoriasRespType.Row.Categorias.Categoria createDataConsultaCategoriasRespTypeRowCategoriasCategoria() {
        return new DataConsultaCategoriasRespType.Row.Categorias.Categoria();
    }

}
