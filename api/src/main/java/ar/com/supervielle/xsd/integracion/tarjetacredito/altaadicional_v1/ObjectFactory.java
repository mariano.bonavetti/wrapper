
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaadicional_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altaadicional_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altaadicional_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType2 }
     * 
     */
    public DataAltaAdicionalReqType2 createDataAltaAdicionalReqType2() {
        return new DataAltaAdicionalReqType2();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType2 .TitularAdicional }
     * 
     */
    public DataAltaAdicionalReqType2 .TitularAdicional createDataAltaAdicionalReqType2TitularAdicional() {
        return new DataAltaAdicionalReqType2 .TitularAdicional();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalRespType2 }
     * 
     */
    public DataAltaAdicionalRespType2 createDataAltaAdicionalRespType2() {
        return new DataAltaAdicionalRespType2();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType }
     * 
     */
    public DataAltaAdicionalReqType createDataAltaAdicionalReqType() {
        return new DataAltaAdicionalReqType();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType.TitularAdicional }
     * 
     */
    public DataAltaAdicionalReqType.TitularAdicional createDataAltaAdicionalReqTypeTitularAdicional() {
        return new DataAltaAdicionalReqType.TitularAdicional();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalRespType }
     * 
     */
    public DataAltaAdicionalRespType createDataAltaAdicionalRespType() {
        return new DataAltaAdicionalRespType();
    }

    /**
     * Create an instance of {@link RespAltaAdicional }
     * 
     */
    public RespAltaAdicional createRespAltaAdicional() {
        return new RespAltaAdicional();
    }

    /**
     * Create an instance of {@link ReqAltaAdicional }
     * 
     */
    public ReqAltaAdicional createReqAltaAdicional() {
        return new ReqAltaAdicional();
    }

    /**
     * Create an instance of {@link RespAltaAdicional2 }
     * 
     */
    public RespAltaAdicional2 createRespAltaAdicional2() {
        return new RespAltaAdicional2();
    }

    /**
     * Create an instance of {@link ReqAltaAdicional2 }
     * 
     */
    public ReqAltaAdicional2 createReqAltaAdicional2() {
        return new ReqAltaAdicional2();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType2 .TitularAdicional.Principal }
     * 
     */
    public DataAltaAdicionalReqType2 .TitularAdicional.Principal createDataAltaAdicionalReqType2TitularAdicionalPrincipal() {
        return new DataAltaAdicionalReqType2 .TitularAdicional.Principal();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType2 .TitularAdicional.Filiatorios }
     * 
     */
    public DataAltaAdicionalReqType2 .TitularAdicional.Filiatorios createDataAltaAdicionalReqType2TitularAdicionalFiliatorios() {
        return new DataAltaAdicionalReqType2 .TitularAdicional.Filiatorios();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType2 .TitularAdicional.Complementarios }
     * 
     */
    public DataAltaAdicionalReqType2 .TitularAdicional.Complementarios createDataAltaAdicionalReqType2TitularAdicionalComplementarios() {
        return new DataAltaAdicionalReqType2 .TitularAdicional.Complementarios();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalRespType2 .Row }
     * 
     */
    public DataAltaAdicionalRespType2 .Row createDataAltaAdicionalRespType2Row() {
        return new DataAltaAdicionalRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType.TitularAdicional.Principal }
     * 
     */
    public DataAltaAdicionalReqType.TitularAdicional.Principal createDataAltaAdicionalReqTypeTitularAdicionalPrincipal() {
        return new DataAltaAdicionalReqType.TitularAdicional.Principal();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType.TitularAdicional.Filiatorios }
     * 
     */
    public DataAltaAdicionalReqType.TitularAdicional.Filiatorios createDataAltaAdicionalReqTypeTitularAdicionalFiliatorios() {
        return new DataAltaAdicionalReqType.TitularAdicional.Filiatorios();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalReqType.TitularAdicional.Complementarios }
     * 
     */
    public DataAltaAdicionalReqType.TitularAdicional.Complementarios createDataAltaAdicionalReqTypeTitularAdicionalComplementarios() {
        return new DataAltaAdicionalReqType.TitularAdicional.Complementarios();
    }

    /**
     * Create an instance of {@link DataAltaAdicionalRespType.Row }
     * 
     */
    public DataAltaAdicionalRespType.Row createDataAltaAdicionalRespTypeRow() {
        return new DataAltaAdicionalRespType.Row();
    }

}
