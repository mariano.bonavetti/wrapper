
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultaclubsupervielle_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataConsultaClubSupervielleReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaClubSupervielleReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificador">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idClienteTarjetaCred" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaClubSupervielleReqType", propOrder = {
    "identificador"
})
public class DataConsultaClubSupervielleReqType {

    @XmlElement(required = true)
    protected DataConsultaClubSupervielleReqType.Identificador identificador;

    /**
     * Obtiene el valor de la propiedad identificador.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaClubSupervielleReqType.Identificador }
     *     
     */
    public DataConsultaClubSupervielleReqType.Identificador getIdentificador() {
        return identificador;
    }

    /**
     * Define el valor de la propiedad identificador.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaClubSupervielleReqType.Identificador }
     *     
     */
    public void setIdentificador(DataConsultaClubSupervielleReqType.Identificador value) {
        this.identificador = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idClienteTarjetaCred" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idClienteTarjetaCred"
    })
    public static class Identificador {

        @XmlElement(required = true)
        protected String idClienteTarjetaCred;

        /**
         * Obtiene el valor de la propiedad idClienteTarjetaCred.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdClienteTarjetaCred() {
            return idClienteTarjetaCred;
        }

        /**
         * Define el valor de la propiedad idClienteTarjetaCred.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdClienteTarjetaCred(String value) {
            this.idClienteTarjetaCred = value;
        }

    }

}
