
package ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistado_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdListadoType;


/**
 * <p>Clase Java para DataConsultaListadoRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaListadoRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="Listado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdListadoType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaListadoRespType", namespace = "http://www.supervielle.com.ar/xsd/Integracion/cuenta/cajaAhorro/consultaListado-v1", propOrder = {
    "row"
})
public class DataConsultaListadoRespType2 {

    @XmlElement(name = "Row")
    protected List<DataConsultaListadoRespType2 .Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaListadoRespType2 .Row }
     * 
     * 
     */
    public List<DataConsultaListadoRespType2 .Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaListadoRespType2 .Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="Listado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdListadoType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "listado"
    })
    public static class Row {

        @XmlElement(required = true)
        protected Object identificador;
        @XmlElement(name = "Listado", required = true)
        protected IdListadoType listado;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setIdentificador(Object value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad listado.
         * 
         * @return
         *     possible object is
         *     {@link IdListadoType }
         *     
         */
        public IdListadoType getListado() {
            return listado;
        }

        /**
         * Define el valor de la propiedad listado.
         * 
         * @param value
         *     allowed object is
         *     {@link IdListadoType }
         *     
         */
        public void setListado(IdListadoType value) {
            this.listado = value;
        }

    }

}
