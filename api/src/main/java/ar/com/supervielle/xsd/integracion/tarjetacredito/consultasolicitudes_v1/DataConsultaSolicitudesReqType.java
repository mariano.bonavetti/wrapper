
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultasolicitudes_v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaSolicitudesReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaSolicitudesReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Empresa">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="IdentificadorSolicitud">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                               &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                               &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaSolicitudesReqType", propOrder = {
    "empresa"
})
public class DataConsultaSolicitudesReqType {

    @XmlElement(name = "Empresa")
    protected DataConsultaSolicitudesReqType.Empresa empresa;

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaSolicitudesReqType.Empresa }
     *     
     */
    public DataConsultaSolicitudesReqType.Empresa getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaSolicitudesReqType.Empresa }
     *     
     */
    public void setEmpresa(DataConsultaSolicitudesReqType.Empresa value) {
        this.empresa = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdentificadorSolicitud">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificadorSolicitud",
        "identificador"
    })
    public static class Empresa {

        @XmlElement(name = "IdentificadorSolicitud", required = true)
        protected DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud identificadorSolicitud;
        @XmlElement(name = "Identificador")
        protected IdClienteType identificador;

        /**
         * Obtiene el valor de la propiedad identificadorSolicitud.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud }
         *     
         */
        public DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud getIdentificadorSolicitud() {
            return identificadorSolicitud;
        }

        /**
         * Define el valor de la propiedad identificadorSolicitud.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud }
         *     
         */
        public void setIdentificadorSolicitud(DataConsultaSolicitudesReqType.Empresa.IdentificadorSolicitud value) {
            this.identificadorSolicitud = value;
        }

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="nroSolicitudGrupo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="nroSolicitud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="subCanal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nroSolicitudGrupo",
            "nroSolicitud",
            "canal",
            "subCanal"
        })
        public static class IdentificadorSolicitud {

            protected BigInteger nroSolicitudGrupo;
            protected BigInteger nroSolicitud;
            @XmlElement(required = true)
            protected BigInteger canal;
            @XmlElement(required = true)
            protected BigInteger subCanal;

            /**
             * Obtiene el valor de la propiedad nroSolicitudGrupo.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitudGrupo() {
                return nroSolicitudGrupo;
            }

            /**
             * Define el valor de la propiedad nroSolicitudGrupo.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitudGrupo(BigInteger value) {
                this.nroSolicitudGrupo = value;
            }

            /**
             * Obtiene el valor de la propiedad nroSolicitud.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNroSolicitud() {
                return nroSolicitud;
            }

            /**
             * Define el valor de la propiedad nroSolicitud.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNroSolicitud(BigInteger value) {
                this.nroSolicitud = value;
            }

            /**
             * Obtiene el valor de la propiedad canal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCanal() {
                return canal;
            }

            /**
             * Define el valor de la propiedad canal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCanal(BigInteger value) {
                this.canal = value;
            }

            /**
             * Obtiene el valor de la propiedad subCanal.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSubCanal() {
                return subCanal;
            }

            /**
             * Define el valor de la propiedad subCanal.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSubCanal(BigInteger value) {
                this.subCanal = value;
            }

        }

    }

}
