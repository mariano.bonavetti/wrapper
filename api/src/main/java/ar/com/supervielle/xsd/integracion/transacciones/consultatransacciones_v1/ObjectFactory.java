
package ar.com.supervielle.xsd.integracion.transacciones.consultatransacciones_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.transacciones.consultatransacciones_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataConsultaTransaccionesReqTypeIdentificadorSource_QNAME = new QName("", "source");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.transacciones.consultatransacciones_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaTransaccionesReqType }
     * 
     */
    public DataConsultaTransaccionesReqType createDataConsultaTransaccionesReqType() {
        return new DataConsultaTransaccionesReqType();
    }

    /**
     * Create an instance of {@link DataConsultaTransaccionesRespType }
     * 
     */
    public DataConsultaTransaccionesRespType createDataConsultaTransaccionesRespType() {
        return new DataConsultaTransaccionesRespType();
    }

    /**
     * Create an instance of {@link DataConsultaTransaccionesRespType.Row }
     * 
     */
    public DataConsultaTransaccionesRespType.Row createDataConsultaTransaccionesRespTypeRow() {
        return new DataConsultaTransaccionesRespType.Row();
    }

    /**
     * Create an instance of {@link RespConsultaTransacciones }
     * 
     */
    public RespConsultaTransacciones createRespConsultaTransacciones() {
        return new RespConsultaTransacciones();
    }

    /**
     * Create an instance of {@link ReqConsultaTransacciones }
     * 
     */
    public ReqConsultaTransacciones createReqConsultaTransacciones() {
        return new ReqConsultaTransacciones();
    }

    /**
     * Create an instance of {@link DataConsultaTransaccionesReqType.Identificador }
     * 
     */
    public DataConsultaTransaccionesReqType.Identificador createDataConsultaTransaccionesReqTypeIdentificador() {
        return new DataConsultaTransaccionesReqType.Identificador();
    }

    /**
     * Create an instance of {@link DataConsultaTransaccionesRespType.Row.Respuesta }
     * 
     */
    public DataConsultaTransaccionesRespType.Row.Respuesta createDataConsultaTransaccionesRespTypeRowRespuesta() {
        return new DataConsultaTransaccionesRespType.Row.Respuesta();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "source", scope = DataConsultaTransaccionesReqType.Identificador.class)
    public JAXBElement<String> createDataConsultaTransaccionesReqTypeIdentificadorSource(String value) {
        return new JAXBElement<String>(_DataConsultaTransaccionesReqTypeIdentificadorSource_QNAME, String.class, DataConsultaTransaccionesReqType.Identificador.class, value);
    }

}
