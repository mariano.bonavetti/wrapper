
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaproductoscorporativos_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.altaproductoscorporativos_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.altaproductoscorporativos_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosRespType }
     * 
     */
    public DataAltaProductosCorporativosRespType createDataAltaProductosCorporativosRespType() {
        return new DataAltaProductosCorporativosRespType();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosRespType.Row }
     * 
     */
    public DataAltaProductosCorporativosRespType.Row createDataAltaProductosCorporativosRespTypeRow() {
        return new DataAltaProductosCorporativosRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas }
     * 
     */
    public DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas createDataAltaProductosCorporativosRespTypeRowSolicitudesGeneradas() {
        return new DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType }
     * 
     */
    public DataAltaProductosCorporativosReqType createDataAltaProductosCorporativosReqType() {
        return new DataAltaProductosCorporativosReqType();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas createDataAltaProductosCorporativosReqTypeTarjetas() {
        return new DataAltaProductosCorporativosReqType.Tarjetas();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta createDataAltaProductosCorporativosReqTypeTarjetasTarjeta() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente createDataAltaProductosCorporativosReqTypeTarjetasTarjetaDatosCliente() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo createDataAltaProductosCorporativosReqTypeTarjetasTarjetaDatosClienteDatosClienteNuevo() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta createDataAltaProductosCorporativosReqTypeEmpresaYCuenta() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta();
    }

    /**
     * Create an instance of {@link ReqAltaProductosCorporativos }
     * 
     */
    public ReqAltaProductosCorporativos createReqAltaProductosCorporativos() {
        return new ReqAltaProductosCorporativos();
    }

    /**
     * Create an instance of {@link RespAltaProductosCorporativos }
     * 
     */
    public RespAltaProductosCorporativos createRespAltaProductosCorporativos() {
        return new RespAltaProductosCorporativos();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud }
     * 
     */
    public DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud createDataAltaProductosCorporativosRespTypeRowSolicitudesGeneradasSolicitud() {
        return new DataAltaProductosCorporativosRespType.Row.SolicitudesGeneradas.Solicitud();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud createDataAltaProductosCorporativosReqTypeTarjetasTarjetaIdentificadorSolicitud() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.IdentificadorSolicitud();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial createDataAltaProductosCorporativosReqTypeTarjetasTarjetaOficial() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Oficial();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites createDataAltaProductosCorporativosReqTypeTarjetasTarjetaLimites() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.Limites();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio }
     * 
     */
    public DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio createDataAltaProductosCorporativosReqTypeTarjetasTarjetaDatosClienteDatosClienteNuevoDomicilio() {
        return new DataAltaProductosCorporativosReqType.Tarjetas.Tarjeta.DatosCliente.DatosClienteNuevo.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud createDataAltaProductosCorporativosReqTypeEmpresaYCuentaIdentificadorSolicitud() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.IdentificadorSolicitud();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites createDataAltaProductosCorporativosReqTypeEmpresaYCuentaLimites() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.Limites();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados createDataAltaProductosCorporativosReqTypeEmpresaYCuentaRubrosHabilitados() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosHabilitados();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados createDataAltaProductosCorporativosReqTypeEmpresaYCuentaRubrosInhabilitados() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.RubrosInhabilitados();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio createDataAltaProductosCorporativosReqTypeEmpresaYCuentaDomicilio() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito createDataAltaProductosCorporativosReqTypeEmpresaYCuentaCuentaDebito() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.CuentaDebito();
    }

    /**
     * Create an instance of {@link DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia }
     * 
     */
    public DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia createDataAltaProductosCorporativosReqTypeEmpresaYCuentaDomicilioCorrespondencia() {
        return new DataAltaProductosCorporativosReqType.EmpresaYCuenta.DomicilioCorrespondencia();
    }

}
