
package ar.com.supervielle.xsd.integracion.tarjetacredito.consultatarjetasxcuenta_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.tarjetacredito.consultatarjetasxcuenta_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.tarjetacredito.consultatarjetasxcuenta_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType createDataConsultaTarjetasXCuentaRespType() {
        return new DataConsultaTarjetasXCuentaRespType();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType.Row }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType.Row createDataConsultaTarjetasXCuentaRespTypeRow() {
        return new DataConsultaTarjetasXCuentaRespType.Row();
    }

    /**
     * Create an instance of {@link ReqConsultaTarjetasXCuenta }
     * 
     */
    public ReqConsultaTarjetasXCuenta createReqConsultaTarjetasXCuenta() {
        return new ReqConsultaTarjetasXCuenta();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaReqType }
     * 
     */
    public DataConsultaTarjetasXCuentaReqType createDataConsultaTarjetasXCuentaReqType() {
        return new DataConsultaTarjetasXCuentaReqType();
    }

    /**
     * Create an instance of {@link RespConsultaTarjetasXCuenta }
     * 
     */
    public RespConsultaTarjetasXCuenta createRespConsultaTarjetasXCuenta() {
        return new RespConsultaTarjetasXCuenta();
    }

    /**
     * Create an instance of {@link IdCtaTarjetaType }
     * 
     */
    public IdCtaTarjetaType createIdCtaTarjetaType() {
        return new IdCtaTarjetaType();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType.Row.Tipo }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType.Row.Tipo createDataConsultaTarjetasXCuentaRespTypeRowTipo() {
        return new DataConsultaTarjetasXCuentaRespType.Row.Tipo();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType.Row.DatosCliente }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType.Row.DatosCliente createDataConsultaTarjetasXCuentaRespTypeRowDatosCliente() {
        return new DataConsultaTarjetasXCuentaRespType.Row.DatosCliente();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType.Row.LimitesControl }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType.Row.LimitesControl createDataConsultaTarjetasXCuentaRespTypeRowLimitesControl() {
        return new DataConsultaTarjetasXCuentaRespType.Row.LimitesControl();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas createDataConsultaTarjetasXCuentaRespTypeRowLimitesControlCuotas() {
        return new DataConsultaTarjetasXCuentaRespType.Row.LimitesControlCuotas();
    }

    /**
     * Create an instance of {@link DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia }
     * 
     */
    public DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia createDataConsultaTarjetasXCuentaRespTypeRowFechaVigencia() {
        return new DataConsultaTarjetasXCuentaRespType.Row.FechaVigencia();
    }

}
