
package ar.com.supervielle.xsd.integracion.clienteprevisional.consultabeneficios_v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescNumType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;


/**
 * <p>Clase Java para DataConsultaBeneficiosRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaBeneficiosRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="beneficios">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="beneficio" maxOccurs="10">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="formaPagoBeneficio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="admiteAnticipo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                                       &lt;element name="motivoNoAnticipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
 *                                       &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="grupoPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                                       &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ultimoPeriodoLiquidado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ultimoPeriodoPagado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="tipoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="estadoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
 *                                       &lt;element name="cppSinFirmar" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                                       &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaBeneficiosRespType", propOrder = {
    "row"
})
public class DataConsultaBeneficiosRespType {

    @XmlElement(name = "Row")
    protected DataConsultaBeneficiosRespType.Row row;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     * @return
     *     possible object is
     *     {@link DataConsultaBeneficiosRespType.Row }
     *     
     */
    public DataConsultaBeneficiosRespType.Row getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     * @param value
     *     allowed object is
     *     {@link DataConsultaBeneficiosRespType.Row }
     *     
     */
    public void setRow(DataConsultaBeneficiosRespType.Row value) {
        this.row = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="beneficios">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="beneficio" maxOccurs="10">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="formaPagoBeneficio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="admiteAnticipo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                             &lt;element name="motivoNoAnticipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
     *                             &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="grupoPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                             &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ultimoPeriodoLiquidado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ultimoPeriodoPagado" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="tipoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="estadoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
     *                             &lt;element name="cppSinFirmar" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                             &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "beneficios"
    })
    public static class Row {

        @XmlElement(required = true)
        protected DataConsultaBeneficiosRespType.Row.Beneficios beneficios;

        /**
         * Obtiene el valor de la propiedad beneficios.
         * 
         * @return
         *     possible object is
         *     {@link DataConsultaBeneficiosRespType.Row.Beneficios }
         *     
         */
        public DataConsultaBeneficiosRespType.Row.Beneficios getBeneficios() {
            return beneficios;
        }

        /**
         * Define el valor de la propiedad beneficios.
         * 
         * @param value
         *     allowed object is
         *     {@link DataConsultaBeneficiosRespType.Row.Beneficios }
         *     
         */
        public void setBeneficios(DataConsultaBeneficiosRespType.Row.Beneficios value) {
            this.beneficios = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="beneficio" maxOccurs="10">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="formaPagoBeneficio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="admiteAnticipo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *                   &lt;element name="motivoNoAnticipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
         *                   &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="grupoPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *                   &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ultimoPeriodoLiquidado" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ultimoPeriodoPagado" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="tipoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="estadoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
         *                   &lt;element name="cppSinFirmar" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "beneficio"
        })
        public static class Beneficios {

            @XmlElement(required = true)
            protected List<DataConsultaBeneficiosRespType.Row.Beneficios.Beneficio> beneficio;

            /**
             * Gets the value of the beneficio property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the beneficio property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBeneficio().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DataConsultaBeneficiosRespType.Row.Beneficios.Beneficio }
             * 
             * 
             */
            public List<DataConsultaBeneficiosRespType.Row.Beneficios.Beneficio> getBeneficio() {
                if (beneficio == null) {
                    beneficio = new ArrayList<DataConsultaBeneficiosRespType.Row.Beneficios.Beneficio>();
                }
                return this.beneficio;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="nroBeneficioCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="formaPagoBeneficio" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="admiteAnticipo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
             *         &lt;element name="motivoNoAnticipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cuentaBancaria" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdCuentaBANTOTALType"/>
             *         &lt;element name="estado" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="paquete" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="grupoPago" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}date"/>
             *         &lt;element name="CUIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ultimoPeriodoLiquidado" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ultimoPeriodoPagado" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="tipoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="estadoLiquidacion" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescNumType"/>
             *         &lt;element name="cppSinFirmar" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
             *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nroBeneficioCliente",
                "formaPagoBeneficio",
                "admiteAnticipo",
                "motivoNoAnticipo",
                "cuentaBancaria",
                "estado",
                "paquete",
                "grupoPago",
                "fechaInicio",
                "cuil",
                "ultimoPeriodoLiquidado",
                "ultimoPeriodoPagado",
                "tipoLiquidacion",
                "estadoLiquidacion",
                "cppSinFirmar",
                "importe"
            })
            public static class Beneficio {

                @XmlElement(required = true)
                protected String nroBeneficioCliente;
                @XmlElement(required = true)
                protected CodDescNumType formaPagoBeneficio;
                protected boolean admiteAnticipo;
                @XmlElement(required = true)
                protected String motivoNoAnticipo;
                @XmlElement(required = true)
                protected IdCuentaBANTOTALType cuentaBancaria;
                @XmlElement(required = true)
                protected CodDescNumType estado;
                @XmlElement(required = true)
                protected CodDescNumType paquete;
                @XmlElement(required = true)
                protected String grupoPago;
                @XmlElement(required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar fechaInicio;
                @XmlElement(name = "CUIL", required = true)
                protected String cuil;
                @XmlElement(required = true)
                protected String ultimoPeriodoLiquidado;
                @XmlElement(required = true)
                protected String ultimoPeriodoPagado;
                @XmlElement(required = true)
                protected CodDescNumType tipoLiquidacion;
                @XmlElement(required = true)
                protected CodDescNumType estadoLiquidacion;
                protected boolean cppSinFirmar;
                @XmlElement(required = true)
                protected BigDecimal importe;

                /**
                 * Obtiene el valor de la propiedad nroBeneficioCliente.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNroBeneficioCliente() {
                    return nroBeneficioCliente;
                }

                /**
                 * Define el valor de la propiedad nroBeneficioCliente.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNroBeneficioCliente(String value) {
                    this.nroBeneficioCliente = value;
                }

                /**
                 * Obtiene el valor de la propiedad formaPagoBeneficio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getFormaPagoBeneficio() {
                    return formaPagoBeneficio;
                }

                /**
                 * Define el valor de la propiedad formaPagoBeneficio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setFormaPagoBeneficio(CodDescNumType value) {
                    this.formaPagoBeneficio = value;
                }

                /**
                 * Obtiene el valor de la propiedad admiteAnticipo.
                 * 
                 */
                public boolean isAdmiteAnticipo() {
                    return admiteAnticipo;
                }

                /**
                 * Define el valor de la propiedad admiteAnticipo.
                 * 
                 */
                public void setAdmiteAnticipo(boolean value) {
                    this.admiteAnticipo = value;
                }

                /**
                 * Obtiene el valor de la propiedad motivoNoAnticipo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMotivoNoAnticipo() {
                    return motivoNoAnticipo;
                }

                /**
                 * Define el valor de la propiedad motivoNoAnticipo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMotivoNoAnticipo(String value) {
                    this.motivoNoAnticipo = value;
                }

                /**
                 * Obtiene el valor de la propiedad cuentaBancaria.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IdCuentaBANTOTALType }
                 *     
                 */
                public IdCuentaBANTOTALType getCuentaBancaria() {
                    return cuentaBancaria;
                }

                /**
                 * Define el valor de la propiedad cuentaBancaria.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IdCuentaBANTOTALType }
                 *     
                 */
                public void setCuentaBancaria(IdCuentaBANTOTALType value) {
                    this.cuentaBancaria = value;
                }

                /**
                 * Obtiene el valor de la propiedad estado.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getEstado() {
                    return estado;
                }

                /**
                 * Define el valor de la propiedad estado.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setEstado(CodDescNumType value) {
                    this.estado = value;
                }

                /**
                 * Obtiene el valor de la propiedad paquete.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getPaquete() {
                    return paquete;
                }

                /**
                 * Define el valor de la propiedad paquete.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setPaquete(CodDescNumType value) {
                    this.paquete = value;
                }

                /**
                 * Obtiene el valor de la propiedad grupoPago.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGrupoPago() {
                    return grupoPago;
                }

                /**
                 * Define el valor de la propiedad grupoPago.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGrupoPago(String value) {
                    this.grupoPago = value;
                }

                /**
                 * Obtiene el valor de la propiedad fechaInicio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getFechaInicio() {
                    return fechaInicio;
                }

                /**
                 * Define el valor de la propiedad fechaInicio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setFechaInicio(XMLGregorianCalendar value) {
                    this.fechaInicio = value;
                }

                /**
                 * Obtiene el valor de la propiedad cuil.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCUIL() {
                    return cuil;
                }

                /**
                 * Define el valor de la propiedad cuil.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCUIL(String value) {
                    this.cuil = value;
                }

                /**
                 * Obtiene el valor de la propiedad ultimoPeriodoLiquidado.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUltimoPeriodoLiquidado() {
                    return ultimoPeriodoLiquidado;
                }

                /**
                 * Define el valor de la propiedad ultimoPeriodoLiquidado.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUltimoPeriodoLiquidado(String value) {
                    this.ultimoPeriodoLiquidado = value;
                }

                /**
                 * Obtiene el valor de la propiedad ultimoPeriodoPagado.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUltimoPeriodoPagado() {
                    return ultimoPeriodoPagado;
                }

                /**
                 * Define el valor de la propiedad ultimoPeriodoPagado.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUltimoPeriodoPagado(String value) {
                    this.ultimoPeriodoPagado = value;
                }

                /**
                 * Obtiene el valor de la propiedad tipoLiquidacion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getTipoLiquidacion() {
                    return tipoLiquidacion;
                }

                /**
                 * Define el valor de la propiedad tipoLiquidacion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setTipoLiquidacion(CodDescNumType value) {
                    this.tipoLiquidacion = value;
                }

                /**
                 * Obtiene el valor de la propiedad estadoLiquidacion.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public CodDescNumType getEstadoLiquidacion() {
                    return estadoLiquidacion;
                }

                /**
                 * Define el valor de la propiedad estadoLiquidacion.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodDescNumType }
                 *     
                 */
                public void setEstadoLiquidacion(CodDescNumType value) {
                    this.estadoLiquidacion = value;
                }

                /**
                 * Obtiene el valor de la propiedad cppSinFirmar.
                 * 
                 */
                public boolean isCppSinFirmar() {
                    return cppSinFirmar;
                }

                /**
                 * Define el valor de la propiedad cppSinFirmar.
                 * 
                 */
                public void setCppSinFirmar(boolean value) {
                    this.cppSinFirmar = value;
                }

                /**
                 * Obtiene el valor de la propiedad importe.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getImporte() {
                    return importe;
                }

                /**
                 * Define el valor de la propiedad importe.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setImporte(BigDecimal value) {
                    this.importe = value;
                }

            }

        }

    }

}
