
package ar.com.supervielle.xsd.integracion.cliente.consultadatosadicionales_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.CodDescStringType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;


/**
 * <p>Clase Java para DataConsultaDatosAdicionalesRespType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataConsultaDatosAdicionalesRespType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Row" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
 *                   &lt;element name="esEmpleado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="esResidente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="sectorEconomico" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="actividadEconomica" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                   &lt;element name="actividadEconomicaBCRA" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataConsultaDatosAdicionalesRespType", propOrder = {
    "row"
})
public class DataConsultaDatosAdicionalesRespType {

    @XmlElement(name = "Row")
    protected List<DataConsultaDatosAdicionalesRespType.Row> row;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataConsultaDatosAdicionalesRespType.Row }
     * 
     * 
     */
    public List<DataConsultaDatosAdicionalesRespType.Row> getRow() {
        if (row == null) {
            row = new ArrayList<DataConsultaDatosAdicionalesRespType.Row>();
        }
        return this.row;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="identificador" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}IdClienteType"/>
     *         &lt;element name="esEmpleado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="esResidente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="sectorEconomico" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="actividadEconomica" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *         &lt;element name="actividadEconomicaBCRA" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}codDescStringType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificador",
        "esEmpleado",
        "esResidente",
        "sectorEconomico",
        "actividadEconomica",
        "actividadEconomicaBCRA"
    })
    public static class Row {

        @XmlElement(required = true)
        protected IdClienteType identificador;
        protected boolean esEmpleado;
        protected boolean esResidente;
        @XmlElement(required = true)
        protected CodDescStringType sectorEconomico;
        @XmlElement(required = true)
        protected CodDescStringType actividadEconomica;
        @XmlElement(required = true)
        protected CodDescStringType actividadEconomicaBCRA;

        /**
         * Obtiene el valor de la propiedad identificador.
         * 
         * @return
         *     possible object is
         *     {@link IdClienteType }
         *     
         */
        public IdClienteType getIdentificador() {
            return identificador;
        }

        /**
         * Define el valor de la propiedad identificador.
         * 
         * @param value
         *     allowed object is
         *     {@link IdClienteType }
         *     
         */
        public void setIdentificador(IdClienteType value) {
            this.identificador = value;
        }

        /**
         * Obtiene el valor de la propiedad esEmpleado.
         * 
         */
        public boolean isEsEmpleado() {
            return esEmpleado;
        }

        /**
         * Define el valor de la propiedad esEmpleado.
         * 
         */
        public void setEsEmpleado(boolean value) {
            this.esEmpleado = value;
        }

        /**
         * Obtiene el valor de la propiedad esResidente.
         * 
         */
        public boolean isEsResidente() {
            return esResidente;
        }

        /**
         * Define el valor de la propiedad esResidente.
         * 
         */
        public void setEsResidente(boolean value) {
            this.esResidente = value;
        }

        /**
         * Obtiene el valor de la propiedad sectorEconomico.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getSectorEconomico() {
            return sectorEconomico;
        }

        /**
         * Define el valor de la propiedad sectorEconomico.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setSectorEconomico(CodDescStringType value) {
            this.sectorEconomico = value;
        }

        /**
         * Obtiene el valor de la propiedad actividadEconomica.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getActividadEconomica() {
            return actividadEconomica;
        }

        /**
         * Define el valor de la propiedad actividadEconomica.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setActividadEconomica(CodDescStringType value) {
            this.actividadEconomica = value;
        }

        /**
         * Obtiene el valor de la propiedad actividadEconomicaBCRA.
         * 
         * @return
         *     possible object is
         *     {@link CodDescStringType }
         *     
         */
        public CodDescStringType getActividadEconomicaBCRA() {
            return actividadEconomicaBCRA;
        }

        /**
         * Define el valor de la propiedad actividadEconomicaBCRA.
         * 
         * @param value
         *     allowed object is
         *     {@link CodDescStringType }
         *     
         */
        public void setActividadEconomicaBCRA(CodDescStringType value) {
            this.actividadEconomicaBCRA = value;
        }

    }

}
