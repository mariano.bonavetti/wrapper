
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbucon_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbucon_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultacbucon_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaCBUCONRespType }
     * 
     */
    public DataConsultaCBUCONRespType createDataConsultaCBUCONRespType() {
        return new DataConsultaCBUCONRespType();
    }

    /**
     * Create an instance of {@link ReqConsultaCBUCON }
     * 
     */
    public ReqConsultaCBUCON createReqConsultaCBUCON() {
        return new ReqConsultaCBUCON();
    }

    /**
     * Create an instance of {@link DataConsultaCBUCONReqType }
     * 
     */
    public DataConsultaCBUCONReqType createDataConsultaCBUCONReqType() {
        return new DataConsultaCBUCONReqType();
    }

    /**
     * Create an instance of {@link RespConsultaCBUCON }
     * 
     */
    public RespConsultaCBUCON createRespConsultaCBUCON() {
        return new RespConsultaCBUCON();
    }

    /**
     * Create an instance of {@link DataConsultaCBUCONRespType.Row }
     * 
     */
    public DataConsultaCBUCONRespType.Row createDataConsultaCBUCONRespTypeRow() {
        return new DataConsultaCBUCONRespType.Row();
    }

}
