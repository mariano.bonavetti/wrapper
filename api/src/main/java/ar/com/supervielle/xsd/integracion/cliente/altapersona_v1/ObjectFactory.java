
package ar.com.supervielle.xsd.integracion.cliente.altapersona_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cliente.altapersona_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cliente.altapersona_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType }
     * 
     */
    public DataAltaPersonaReqType createDataAltaPersonaReqType() {
        return new DataAltaPersonaReqType();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas }
     * 
     */
    public DataAltaPersonaReqType.Personas createDataAltaPersonaReqTypePersonas() {
        return new DataAltaPersonaReqType.Personas();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona createDataAltaPersonaReqTypePersonasPersona() {
        return new DataAltaPersonaReqType.Personas.Persona();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera createDataAltaPersonaReqTypePersonasPersonaInformacionFinanciera() {
        return new DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.SitImpositiva }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.SitImpositiva createDataAltaPersonaReqTypePersonasPersonaSitImpositiva() {
        return new DataAltaPersonaReqType.Personas.Persona.SitImpositiva();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.Domicilios }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.Domicilios createDataAltaPersonaReqTypePersonasPersonaDomicilios() {
        return new DataAltaPersonaReqType.Personas.Persona.Domicilios();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.InfoPersona }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.InfoPersona createDataAltaPersonaReqTypePersonasPersonaInfoPersona() {
        return new DataAltaPersonaReqType.Personas.Persona.InfoPersona();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Adicionales }
     * 
     */
    public DataAltaPersonaReqType.Adicionales createDataAltaPersonaReqTypeAdicionales() {
        return new DataAltaPersonaReqType.Adicionales();
    }

    /**
     * Create an instance of {@link DataAltaPersonaRespType }
     * 
     */
    public DataAltaPersonaRespType createDataAltaPersonaRespType() {
        return new DataAltaPersonaRespType();
    }

    /**
     * Create an instance of {@link DataAltaPersonaRespType.Row }
     * 
     */
    public DataAltaPersonaRespType.Row createDataAltaPersonaRespTypeRow() {
        return new DataAltaPersonaRespType.Row();
    }

    /**
     * Create an instance of {@link DataAltaPersonaRespType.Row.Personas }
     * 
     */
    public DataAltaPersonaRespType.Row.Personas createDataAltaPersonaRespTypeRowPersonas() {
        return new DataAltaPersonaRespType.Row.Personas();
    }

    /**
     * Create an instance of {@link DataAltaPersonaRespType.Row.Productos }
     * 
     */
    public DataAltaPersonaRespType.Row.Productos createDataAltaPersonaRespTypeRowProductos() {
        return new DataAltaPersonaRespType.Row.Productos();
    }

    /**
     * Create an instance of {@link RespAltaPersona }
     * 
     */
    public RespAltaPersona createRespAltaPersona() {
        return new RespAltaPersona();
    }

    /**
     * Create an instance of {@link ReqAltaPersona }
     * 
     */
    public ReqAltaPersona createReqAltaPersona() {
        return new ReqAltaPersona();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.Conyugue }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.Conyugue createDataAltaPersonaReqTypePersonasPersonaConyugue() {
        return new DataAltaPersonaReqType.Personas.Persona.Conyugue();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera.UIF }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera.UIF createDataAltaPersonaReqTypePersonasPersonaInformacionFinancieraUIF() {
        return new DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera.UIF();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera.FATCA }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera.FATCA createDataAltaPersonaReqTypePersonasPersonaInformacionFinancieraFATCA() {
        return new DataAltaPersonaReqType.Personas.Persona.InformacionFinanciera.FATCA();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.SitImpositiva.Impuesto }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.SitImpositiva.Impuesto createDataAltaPersonaReqTypePersonasPersonaSitImpositivaImpuesto() {
        return new DataAltaPersonaReqType.Personas.Persona.SitImpositiva.Impuesto();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.Domicilios.Domicilio }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.Domicilios.Domicilio createDataAltaPersonaReqTypePersonasPersonaDomiciliosDomicilio() {
        return new DataAltaPersonaReqType.Personas.Persona.Domicilios.Domicilio();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Personas.Persona.InfoPersona.InfAdicional }
     * 
     */
    public DataAltaPersonaReqType.Personas.Persona.InfoPersona.InfAdicional createDataAltaPersonaReqTypePersonasPersonaInfoPersonaInfAdicional() {
        return new DataAltaPersonaReqType.Personas.Persona.InfoPersona.InfAdicional();
    }

    /**
     * Create an instance of {@link DataAltaPersonaReqType.Adicionales.Adicional }
     * 
     */
    public DataAltaPersonaReqType.Adicionales.Adicional createDataAltaPersonaReqTypeAdicionalesAdicional() {
        return new DataAltaPersonaReqType.Adicionales.Adicional();
    }

    /**
     * Create an instance of {@link DataAltaPersonaRespType.Row.Personas.Persona }
     * 
     */
    public DataAltaPersonaRespType.Row.Personas.Persona createDataAltaPersonaRespTypeRowPersonasPersona() {
        return new DataAltaPersonaRespType.Row.Personas.Persona();
    }

    /**
     * Create an instance of {@link DataAltaPersonaRespType.Row.Productos.Cuentas }
     * 
     */
    public DataAltaPersonaRespType.Row.Productos.Cuentas createDataAltaPersonaRespTypeRowProductosCuentas() {
        return new DataAltaPersonaRespType.Row.Productos.Cuentas();
    }

}
