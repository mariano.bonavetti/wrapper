
package ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 }
     * 
     */
    public DataConsultaListadoRespType3 createDataConsultaListadoRespType3() {
        return new DataConsultaListadoRespType3();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType2 }
     * 
     */
    public DataConsultaListadoRespType2 createDataConsultaListadoRespType2() {
        return new DataConsultaListadoRespType2();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType }
     * 
     */
    public DataConsultaListadoRespType createDataConsultaListadoRespType() {
        return new DataConsultaListadoRespType();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row }
     * 
     */
    public DataConsultaListadoRespType.Row createDataConsultaListadoRespTypeRow() {
        return new DataConsultaListadoRespType.Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row.AtributosExtendidos }
     * 
     */
    public DataConsultaListadoRespType.Row.AtributosExtendidos createDataConsultaListadoRespTypeRowAtributosExtendidos() {
        return new DataConsultaListadoRespType.Row.AtributosExtendidos();
    }

    /**
     * Create an instance of {@link RespConsultaListado }
     * 
     */
    public RespConsultaListado createRespConsultaListado() {
        return new RespConsultaListado();
    }

    /**
     * Create an instance of {@link ByCuentaId }
     * 
     */
    public ByCuentaId createByCuentaId() {
        return new ByCuentaId();
    }

    /**
     * Create an instance of {@link ReqConsultaListado }
     * 
     */
    public ReqConsultaListado createReqConsultaListado() {
        return new ReqConsultaListado();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType }
     * 
     */
    public DataConsultaListadoReqType createDataConsultaListadoReqType() {
        return new DataConsultaListadoReqType();
    }

    /**
     * Create an instance of {@link ByClienteId }
     * 
     */
    public ByClienteId createByClienteId() {
        return new ByClienteId();
    }

    /**
     * Create an instance of {@link RespConsultaListado2 }
     * 
     */
    public RespConsultaListado2 createRespConsultaListado2() {
        return new RespConsultaListado2();
    }

    /**
     * Create an instance of {@link ReqConsultaListado2 }
     * 
     */
    public ReqConsultaListado2 createReqConsultaListado2() {
        return new ReqConsultaListado2();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType2 }
     * 
     */
    public DataConsultaListadoReqType2 createDataConsultaListadoReqType2() {
        return new DataConsultaListadoReqType2();
    }

    /**
     * Create an instance of {@link RespConsultaListado3 }
     * 
     */
    public RespConsultaListado3 createRespConsultaListado3() {
        return new RespConsultaListado3();
    }

    /**
     * Create an instance of {@link ReqConsultaListado3 }
     * 
     */
    public ReqConsultaListado3 createReqConsultaListado3() {
        return new ReqConsultaListado3();
    }

    /**
     * Create an instance of {@link DataConsultaListadoReqType3 }
     * 
     */
    public DataConsultaListadoReqType3 createDataConsultaListadoReqType3() {
        return new DataConsultaListadoReqType3();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType3 .Row }
     * 
     */
    public DataConsultaListadoRespType3 .Row createDataConsultaListadoRespType3Row() {
        return new DataConsultaListadoRespType3 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType2 .Row }
     * 
     */
    public DataConsultaListadoRespType2 .Row createDataConsultaListadoRespType2Row() {
        return new DataConsultaListadoRespType2 .Row();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row.Moneda }
     * 
     */
    public DataConsultaListadoRespType.Row.Moneda createDataConsultaListadoRespTypeRowMoneda() {
        return new DataConsultaListadoRespType.Row.Moneda();
    }

    /**
     * Create an instance of {@link DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo }
     * 
     */
    public DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo createDataConsultaListadoRespTypeRowAtributosExtendidosAtributo() {
        return new DataConsultaListadoRespType.Row.AtributosExtendidos.Atributo();
    }

}
