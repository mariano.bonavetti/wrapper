
package ar.com.supervielle.xsd.integracion.common.commontypes_v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para InformacionVentaBean complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InformacionVentaBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cargoAdicional" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="fechaTopeSeleccionPersonal" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fechaTopeVenta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fechaVigenciaDesde" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}FechaGregorianCalendar" minOccurs="0"/>
 *         &lt;element name="fechaVigenciaHasta" type="{http://www.supervielle.com.ar/xsd/Integracion/common/commonTypes-v1}FechaGregorianCalendar" minOccurs="0"/>
 *         &lt;element name="maximoEntradas" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="seleccionPersonal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tarifasDisponibles" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformacionVentaBean", propOrder = {
    "cargoAdicional",
    "fechaTopeSeleccionPersonal",
    "fechaTopeVenta",
    "fechaVigenciaDesde",
    "fechaVigenciaHasta",
    "maximoEntradas",
    "seleccionPersonal",
    "tarifasDisponibles"
})
public class InformacionVentaBean {

    protected Double cargoAdicional;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaTopeSeleccionPersonal;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaTopeVenta;
    protected FechaGregorianCalendar fechaVigenciaDesde;
    protected FechaGregorianCalendar fechaVigenciaHasta;
    protected Integer maximoEntradas;
    protected Boolean seleccionPersonal;
    protected List<Object> tarifasDisponibles;

    /**
     * Obtiene el valor de la propiedad cargoAdicional.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCargoAdicional() {
        return cargoAdicional;
    }

    /**
     * Define el valor de la propiedad cargoAdicional.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCargoAdicional(Double value) {
        this.cargoAdicional = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaTopeSeleccionPersonal.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaTopeSeleccionPersonal() {
        return fechaTopeSeleccionPersonal;
    }

    /**
     * Define el valor de la propiedad fechaTopeSeleccionPersonal.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaTopeSeleccionPersonal(XMLGregorianCalendar value) {
        this.fechaTopeSeleccionPersonal = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaTopeVenta.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaTopeVenta() {
        return fechaTopeVenta;
    }

    /**
     * Define el valor de la propiedad fechaTopeVenta.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaTopeVenta(XMLGregorianCalendar value) {
        this.fechaTopeVenta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVigenciaDesde.
     * 
     * @return
     *     possible object is
     *     {@link FechaGregorianCalendar }
     *     
     */
    public FechaGregorianCalendar getFechaVigenciaDesde() {
        return fechaVigenciaDesde;
    }

    /**
     * Define el valor de la propiedad fechaVigenciaDesde.
     * 
     * @param value
     *     allowed object is
     *     {@link FechaGregorianCalendar }
     *     
     */
    public void setFechaVigenciaDesde(FechaGregorianCalendar value) {
        this.fechaVigenciaDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVigenciaHasta.
     * 
     * @return
     *     possible object is
     *     {@link FechaGregorianCalendar }
     *     
     */
    public FechaGregorianCalendar getFechaVigenciaHasta() {
        return fechaVigenciaHasta;
    }

    /**
     * Define el valor de la propiedad fechaVigenciaHasta.
     * 
     * @param value
     *     allowed object is
     *     {@link FechaGregorianCalendar }
     *     
     */
    public void setFechaVigenciaHasta(FechaGregorianCalendar value) {
        this.fechaVigenciaHasta = value;
    }

    /**
     * Obtiene el valor de la propiedad maximoEntradas.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximoEntradas() {
        return maximoEntradas;
    }

    /**
     * Define el valor de la propiedad maximoEntradas.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximoEntradas(Integer value) {
        this.maximoEntradas = value;
    }

    /**
     * Obtiene el valor de la propiedad seleccionPersonal.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSeleccionPersonal() {
        return seleccionPersonal;
    }

    /**
     * Define el valor de la propiedad seleccionPersonal.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSeleccionPersonal(Boolean value) {
        this.seleccionPersonal = value;
    }

    /**
     * Gets the value of the tarifasDisponibles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tarifasDisponibles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTarifasDisponibles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getTarifasDisponibles() {
        if (tarifasDisponibles == null) {
            tarifasDisponibles = new ArrayList<Object>();
        }
        return this.tarifasDisponibles;
    }

}
