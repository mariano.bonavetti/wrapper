
package ar.com.supervielle.xsd.integracion.tarjetacredito.altaadelantomaster_v1;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DataAltaAdelantoMasterReqType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DataAltaAdelantoMasterReqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="principal">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="entidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="datosTarjeta">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="mesVencimiento">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="2"/>
 *                                   &lt;maxLength value="2"/>
 *                                   &lt;pattern value="\d{2}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="anoVencimiento">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;minLength value="2"/>
 *                                   &lt;maxLength value="2"/>
 *                                   &lt;pattern value="\d{2}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="tarjetaCVC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="linea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataAltaAdelantoMasterReqType", propOrder = {
    "operacion",
    "principal"
})
public class DataAltaAdelantoMasterReqType {

    @XmlElement(required = true)
    protected BigInteger operacion;
    @XmlElement(required = true)
    protected DataAltaAdelantoMasterReqType.Principal principal;

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOperacion(BigInteger value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad principal.
     * 
     * @return
     *     possible object is
     *     {@link DataAltaAdelantoMasterReqType.Principal }
     *     
     */
    public DataAltaAdelantoMasterReqType.Principal getPrincipal() {
        return principal;
    }

    /**
     * Define el valor de la propiedad principal.
     * 
     * @param value
     *     allowed object is
     *     {@link DataAltaAdelantoMasterReqType.Principal }
     *     
     */
    public void setPrincipal(DataAltaAdelantoMasterReqType.Principal value) {
        this.principal = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="entidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="datosTarjeta">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="mesVencimiento">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="2"/>
     *                         &lt;maxLength value="2"/>
     *                         &lt;pattern value="\d{2}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="anoVencimiento">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;minLength value="2"/>
     *                         &lt;maxLength value="2"/>
     *                         &lt;pattern value="\d{2}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="tarjetaCVC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="capital" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="cantCuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="linea" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="usuarioCarga" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "marca",
        "entidad",
        "datosTarjeta",
        "tarjetaCVC",
        "moneda",
        "capital",
        "cantCuotas",
        "linea",
        "usuarioCarga"
    })
    public static class Principal {

        @XmlElement(required = true)
        protected BigInteger marca;
        @XmlElement(required = true)
        protected BigInteger entidad;
        @XmlElement(required = true)
        protected DataAltaAdelantoMasterReqType.Principal.DatosTarjeta datosTarjeta;
        protected BigInteger tarjetaCVC;
        @XmlElement(required = true)
        protected BigInteger moneda;
        @XmlElement(required = true)
        protected BigDecimal capital;
        @XmlElement(required = true)
        protected BigInteger cantCuotas;
        @XmlElement(required = true)
        protected String linea;
        protected int usuarioCarga;

        /**
         * Obtiene el valor de la propiedad marca.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMarca() {
            return marca;
        }

        /**
         * Define el valor de la propiedad marca.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMarca(BigInteger value) {
            this.marca = value;
        }

        /**
         * Obtiene el valor de la propiedad entidad.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getEntidad() {
            return entidad;
        }

        /**
         * Define el valor de la propiedad entidad.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setEntidad(BigInteger value) {
            this.entidad = value;
        }

        /**
         * Obtiene el valor de la propiedad datosTarjeta.
         * 
         * @return
         *     possible object is
         *     {@link DataAltaAdelantoMasterReqType.Principal.DatosTarjeta }
         *     
         */
        public DataAltaAdelantoMasterReqType.Principal.DatosTarjeta getDatosTarjeta() {
            return datosTarjeta;
        }

        /**
         * Define el valor de la propiedad datosTarjeta.
         * 
         * @param value
         *     allowed object is
         *     {@link DataAltaAdelantoMasterReqType.Principal.DatosTarjeta }
         *     
         */
        public void setDatosTarjeta(DataAltaAdelantoMasterReqType.Principal.DatosTarjeta value) {
            this.datosTarjeta = value;
        }

        /**
         * Obtiene el valor de la propiedad tarjetaCVC.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTarjetaCVC() {
            return tarjetaCVC;
        }

        /**
         * Define el valor de la propiedad tarjetaCVC.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTarjetaCVC(BigInteger value) {
            this.tarjetaCVC = value;
        }

        /**
         * Obtiene el valor de la propiedad moneda.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMoneda() {
            return moneda;
        }

        /**
         * Define el valor de la propiedad moneda.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMoneda(BigInteger value) {
            this.moneda = value;
        }

        /**
         * Obtiene el valor de la propiedad capital.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCapital() {
            return capital;
        }

        /**
         * Define el valor de la propiedad capital.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCapital(BigDecimal value) {
            this.capital = value;
        }

        /**
         * Obtiene el valor de la propiedad cantCuotas.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCantCuotas() {
            return cantCuotas;
        }

        /**
         * Define el valor de la propiedad cantCuotas.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCantCuotas(BigInteger value) {
            this.cantCuotas = value;
        }

        /**
         * Obtiene el valor de la propiedad linea.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLinea() {
            return linea;
        }

        /**
         * Define el valor de la propiedad linea.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLinea(String value) {
            this.linea = value;
        }

        /**
         * Obtiene el valor de la propiedad usuarioCarga.
         * 
         */
        public int getUsuarioCarga() {
            return usuarioCarga;
        }

        /**
         * Define el valor de la propiedad usuarioCarga.
         * 
         */
        public void setUsuarioCarga(int value) {
            this.usuarioCarga = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="mesVencimiento">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="2"/>
         *               &lt;maxLength value="2"/>
         *               &lt;pattern value="\d{2}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="anoVencimiento">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;minLength value="2"/>
         *               &lt;maxLength value="2"/>
         *               &lt;pattern value="\d{2}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "numero",
            "mesVencimiento",
            "anoVencimiento"
        })
        public static class DatosTarjeta {

            @XmlElement(required = true)
            protected String numero;
            @XmlElement(required = true)
            protected String mesVencimiento;
            @XmlElement(required = true)
            protected String anoVencimiento;

            /**
             * Obtiene el valor de la propiedad numero.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumero() {
                return numero;
            }

            /**
             * Define el valor de la propiedad numero.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumero(String value) {
                this.numero = value;
            }

            /**
             * Obtiene el valor de la propiedad mesVencimiento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMesVencimiento() {
                return mesVencimiento;
            }

            /**
             * Define el valor de la propiedad mesVencimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMesVencimiento(String value) {
                this.mesVencimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad anoVencimiento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAnoVencimiento() {
                return anoVencimiento;
            }

            /**
             * Define el valor de la propiedad anoVencimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAnoVencimiento(String value) {
                this.anoVencimiento = value;
            }

        }

    }

}
