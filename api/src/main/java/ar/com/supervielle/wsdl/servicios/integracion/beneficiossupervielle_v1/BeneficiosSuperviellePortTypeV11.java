package ar.com.supervielle.wsdl.servicios.integracion.beneficiossupervielle_v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.0.3
 * 2019-09-26T08:29:57.443-03:00
 * Generated source version: 3.0.3
 * 
 */
@WebService(targetNamespace = "http://www.supervielle.com.ar/wsdl/servicios/integracion/beneficiosSupervielle-v1", name = "BeneficiosSuperviellePortType-v1.1")
@XmlSeeAlso({ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.transacciones.consultatransacciones_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultamovimientos_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.header_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacanjes_v1.ObjectFactory.class, ar.com.supervielle.xsd.servicios.integracion.exception_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultacategorias_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultaproductos_v1.ObjectFactory.class, ar.com.supervielle.xsd.integracion.common.commontypes_v1.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface BeneficiosSuperviellePortTypeV11 {

    @WebMethod(action = "http://www.supervielle.com.ar/wsdl/servicios/integracion/beneficiosSupervielle-v1/consultaPuntos")
    public void consultaPuntos(
        @WebParam(partName = "entrada", name = "ReqConsultaPuntos", targetNamespace = "http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1")
        ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.ReqConsultaPuntos2 entrada,
        @WebParam(partName = "header", name = "soapHeaderReq", targetNamespace = "http://www.supervielle.com.ar/xsd/Integracion/Header-v1", header = true)
        ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq header,
        @WebParam(partName = "salida", mode = WebParam.Mode.OUT, name = "RespConsultaPuntos", targetNamespace = "http://www.supervielle.com.ar/xsd/Integracion/beneficiosSupervielle/consultaPuntos-v1.1")
        javax.xml.ws.Holder<ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.RespConsultaPuntos2> salida,
        @WebParam(partName = "header", mode = WebParam.Mode.OUT, name = "soapHeaderRes", targetNamespace = "http://www.supervielle.com.ar/xsd/Integracion/Header-v1", header = true)
        javax.xml.ws.Holder<ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderRes> header1
    ) throws FaultMessage;
}
