package ar.com.wrapper.services;

import ar.com.wrapper.dto.ErrorSOA;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.xml.ws.soap.SOAPFaultException;

@ControllerAdvice
public class ServiceExceptionHandler {

    private static final Logger logger = LogManager.getLogger(ServiceExceptionHandler.class);

    @ExceptionHandler(SOAPFaultException.class)
    public ResponseEntity<ErrorSOA> exceptionSOA(SOAPFaultException t) {

        if (t.getFault() != null && t.getFault().getFaultCode() != null) {
            ErrorSOA error = new ErrorSOA();
            error.setMessage(t.getFault().getFaultCode());
            logger.error(t.getFault().getFaultCode(), t);
            return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.error("Error", t);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<String> exceptionAll(Throwable t) {

        logger.error("Error", t);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
