package ar.com.wrapper.adapters;

import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdCuenta;
import ar.com.wrapper.dto.IdCuentaBANTOTAL;
import ar.com.wrapper.dto.IdCuentaCONSUMO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;
import org.springframework.stereotype.Service;

import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdClienteType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaCONSUMOType;
import ar.com.supervielle.xsd.integracion.header_v1.SessionContext;
import ar.com.supervielle.xsd.integracion.header_v1.SessionContext.Session;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;

@Service
public class SOAAdapter {
	
	private final static Logger log = LogManager.getLogger(SOAAdapter.class);

	protected SoapHeaderReq generateHeader() {
		SoapHeaderReq sh = new SoapHeaderReq();
		SessionContext sc = new SessionContext();
		Session s = new Session();
		s.setUserName("");
		s.setSystemId("ChatBanking");
		s.setCanalID("ChatBanking");
		sc.setSession(s);
		sh.setSessionContext(sc);
		return sh;
	}

	protected IdClienteType generateIDCliente(Documento documento) {
		IdClienteType idc = new IdClienteType();
		idc.setDataTypeName("IdClienteType");
		idc.setPaisOrigen(documento.getPaisOrigen());
		idc.setTipoDoc(documento.getTipo());
		idc.setNumDoc(documento.getNumero());
		return idc;
	}

	protected IdCuenta generateIdCuenta(ElementNSImpl identificador) {
		try {
			String dataTypeName = identificador.getAttribute("dataTypeName");
			if ("BANTOTAL".equals(dataTypeName)) {
				IdCuentaBANTOTAL idCuenta = new IdCuentaBANTOTAL();
				idCuenta.setDataTypeName(dataTypeName);
				idCuenta.setCuenta(identificador.getElementsByTagName("cuenta")
						.item(0).getFirstChild().getNodeValue());
				idCuenta.setEmpresa(identificador
						.getElementsByTagName("empresa").item(0)
						.getFirstChild().getNodeValue());
				idCuenta.setModulo(identificador.getElementsByTagName("modulo")
						.item(0).getFirstChild().getNodeValue());
				idCuenta.setMoneda(identificador.getElementsByTagName("moneda")
						.item(0).getFirstChild().getNodeValue());
				idCuenta.setOperacion(identificador
						.getElementsByTagName("operacion").item(0)
						.getFirstChild().getNodeValue());
				idCuenta.setPapel(identificador.getElementsByTagName("papel")
						.item(0).getFirstChild().getNodeValue());
				idCuenta.setSubCuenta(identificador
						.getElementsByTagName("subCuenta").item(0)
						.getFirstChild().getNodeValue());
				idCuenta.setSucursal(identificador
						.getElementsByTagName("sucursal").item(0)
						.getFirstChild().getNodeValue());
				idCuenta.setTipoOpe(identificador
						.getElementsByTagName("tipoOpe").item(0)
						.getFirstChild().getNodeValue());
				return idCuenta;
			} else {
				IdCuentaCONSUMO idCuenta = new IdCuentaCONSUMO();
				idCuenta.setDataTypeName(dataTypeName);
				idCuenta.setMoneda(identificador.getElementsByTagName("moneda")
						.item(0).getFirstChild().getNodeValue());
				idCuenta.setNumCuenta(identificador
						.getElementsByTagName("numCuenta").item(0)
						.getFirstChild().getNodeValue());
				idCuenta.setSucursal(identificador
						.getElementsByTagName("sucursal").item(0)
						.getFirstChild().getNodeValue());
				idCuenta.setTipoCuenta(identificador
						.getElementsByTagName("tipoCuenta").item(0)
						.getFirstChild().getNodeValue());
				return idCuenta;
			}
		} catch (Throwable t) {
			log.warn("Error al construir IdCuenta desde identificador (ElementNSImpl)",t);
			return null;
		}
	}
	
	protected IdCuentaBANTOTALType generateIdCuentaBANTOTALType(IdCuenta idc) {
		try {
			IdCuentaBANTOTAL idcB = (IdCuentaBANTOTAL) idc;
			IdCuentaBANTOTALType idCuenta = new IdCuentaBANTOTALType();
			idCuenta.setCuenta(idcB.getCuenta());
			idCuenta.setDataTypeName(idcB.getDataTypeName());
			idCuenta.setEmpresa(idcB.getEmpresa());
			idCuenta.setModulo(idcB.getModulo());
			idCuenta.setMoneda(idcB.getMoneda());
			idCuenta.setOperacion(idcB.getOperacion());
			idCuenta.setPapel(idcB.getPapel());
			idCuenta.setSubCuenta(idcB.getSubCuenta());
			idCuenta.setSucursal(idcB.getSucursal());
			idCuenta.setTipoOpe(idcB.getTipoOpe());
			return idCuenta;

		} catch (Throwable t) {
			log.warn("Error al construir IdCuentaBANTOTAL desde IdCuenta",	t);
			return null;
		}
	}
	
	protected Object generateIdCuentaType(IdCuenta idc) {
		try {
			if ("BANTOTAL".equals(idc.getDataTypeName())) {
				IdCuentaBANTOTAL idcB = (IdCuentaBANTOTAL) idc;
				IdCuentaBANTOTALType idCuenta = new IdCuentaBANTOTALType();
				idCuenta.setCuenta(idcB.getCuenta());
				idCuenta.setDataTypeName(idcB.getDataTypeName());
				idCuenta.setEmpresa(idcB.getEmpresa());
				idCuenta.setModulo(idcB.getModulo());
				idCuenta.setMoneda(idcB.getMoneda());
				idCuenta.setOperacion(idcB.getOperacion());
				idCuenta.setPapel(idcB.getPapel());
				idCuenta.setSubCuenta(idcB.getSubCuenta());
				idCuenta.setSucursal(idcB.getSucursal());
				idCuenta.setTipoOpe(idcB.getTipoOpe());
				return idCuenta;
			} else {
				IdCuentaCONSUMO idcC = (IdCuentaCONSUMO) idc;
				IdCuentaCONSUMOType idCuentaConsumo = new IdCuentaCONSUMOType();
				idCuentaConsumo.setDataTypeName(idcC.getDataTypeName());
				idCuentaConsumo.setMoneda(idcC.getMoneda());
				idCuentaConsumo.setNumCuenta(idcC.getNumCuenta());
				idCuentaConsumo.setSucursal(idcC.getSucursal());
				idCuentaConsumo.setTipoCuenta(idcC.getTipoCuenta());
				return idCuentaConsumo;
			}
		} catch (Throwable t) {
			log.warn("Error al construir IdCuentaBANTOTAL/IdCuentaCONSUMO desde IdCuenta", t);
			return null;
		}
	}
}
