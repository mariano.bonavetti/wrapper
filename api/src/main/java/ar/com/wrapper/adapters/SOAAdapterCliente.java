package ar.com.wrapper.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import ar.com.wrapper.dto.Cliente;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdClienteTipo;
import org.springframework.stereotype.Service;

import ar.com.supervielle.wsdl.servicios.integracion.cliente_v1.ClientePortTypeV1;
import ar.com.supervielle.xsd.integracion.cliente.consultadatospersonafisica_v1.DataConsultaDatosPersonaFisicaReqType;
import ar.com.supervielle.xsd.integracion.cliente.consultadatospersonafisica_v1.ReqConsultaDatosPersonaFisica;
import ar.com.supervielle.xsd.integracion.cliente.consultadatospersonafisica_v1.RespConsultaDatosPersonaFisica;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderRes;


@Service
public class SOAAdapterCliente extends SOAAdapter{

	@Resource(name = "soaClienteV1")
	private ClientePortTypeV1 soaClienteV1;
	
	public List<Cliente> consultaDatosPersonaFisica(Documento documento) throws Exception{
		
		SoapHeaderReq header = generateHeader();	

		List<Cliente> clientes = new ArrayList<>();
		
		ReqConsultaDatosPersonaFisica entrada = new ReqConsultaDatosPersonaFisica();
		DataConsultaDatosPersonaFisicaReqType dcl = new DataConsultaDatosPersonaFisicaReqType();
		dcl.setIdentificador(generateIDCliente(documento));
		entrada.setData(dcl);
		
		Holder<RespConsultaDatosPersonaFisica> salida = new Holder<>(new RespConsultaDatosPersonaFisica());
		
		soaClienteV1.consultaDatosPersonaFisica(entrada, header, salida,	new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.cliente.consultadatospersonafisica_v1.DataConsultaDatosPersonaFisicaRespType.Row row : salida.value.getData().getRow()) {
				
				Cliente c = new Cliente();
				IdClienteTipo idc = new IdClienteTipo();
				idc.setNumDoc(row.getIdentificador().getNumDoc());
				idc.setPaisOrigen(row.getIdentificador().getPaisOrigen());
				idc.setTipoDoc(row.getIdentificador().getTipoDoc());
				c.setIdCliente(idc);
				c.setApellido(row.getApellido());
				c.setNombre(row.getNombre());
				c.setSexoCodigo(row.getSexo().getCodigo());
				c.setSexoDescripcion(row.getSexo().getDescripcion());
				c.setEsEmpleado(row.isEsEmpleado());
				c.setEstadoCivilCodigo(row.getEstadoCivil().getCodigo());
				c.setEstadoCivilDescripcion(row.getEstadoCivil().getDescripcion());
				c.setFechaNacimiento(row.getFechaNacimiento() != null ? row.getFechaNacimiento().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				c.setNacionalidadCodigo(row.getNacionalidad().getCodigo());
				c.setNacionalidadDescripcion(row.getNacionalidad().getDescripcion());
				clientes.add(c);
				
			}
		}
		return clientes;
	}
}
