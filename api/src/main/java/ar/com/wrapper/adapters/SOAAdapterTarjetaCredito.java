package ar.com.wrapper.adapters;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import org.springframework.stereotype.Service;

import ar.com.supervielle.wsdl.servicios.integracion.tarjetacredito_v1.TarjetaCreditoPortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.tarjetacredito_v1.TarjetaCreditoPortTypeV12;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdTarjetaType;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.PagingType;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1.DataConsultaDetalleReqType;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1.ReqConsultaDetalle;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1.RespConsultaDetalle;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimites_v1.DataConsultaLimitesReqType;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimites_v1.ReqConsultaLimites;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimites_v1.RespConsultaLimites;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1.DataConsultaListadoReqType4;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1.ReqConsultaListado4;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1.RespConsultaListado4;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1.DataConsultaMovimientosReqType;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1.ReqConsultaMovimientos;
import ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1.RespConsultaMovimientos;
import ar.com.wrapper.dto.DetalleTarjeta;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdTarjeta;
import ar.com.wrapper.dto.Limites;
import ar.com.wrapper.dto.MovimientoTarjeta;
import ar.com.wrapper.dto.TarjetaCredito;
import ar.com.wrapper.dto.Tasa;

@Service
public class SOAAdapterTarjetaCredito extends SOAAdapter {
	
	
	@Resource(name = "soaTarjetaCreditoV1")
	private TarjetaCreditoPortTypeV1 soaTarjetaCreditoV1;
	
	@Resource(name = "soaTarjetaCreditoV12")
	private TarjetaCreditoPortTypeV12 soaTarjetaCreditoV12;
	
	private DetalleTarjeta consultaDetalle(String id, String tarjetaType) throws Exception{
		
		DetalleTarjeta detalle = new DetalleTarjeta();
		SoapHeaderReq header = generateHeader();
		ReqConsultaDetalle entrada = new ReqConsultaDetalle();
		DataConsultaDetalleReqType dcd = new DataConsultaDetalleReqType();
		IdTarjetaType itt = new IdTarjetaType();
		itt.setIdTarjeta(id);
		itt.setDataTypeName(tarjetaType);
		dcd.setIdentificador(itt);
		entrada.setData(dcd);
		
		Holder<RespConsultaDetalle> salida = new Holder<>(new RespConsultaDetalle());
		
		soaTarjetaCreditoV1.consultaDetalle(entrada, header, salida, new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			ar.com.supervielle.xsd.integracion.tarjetacredito.consultadetalle_v1.DataConsultaDetalleRespType.Row detalleTC = salida.value.getData().getRow();
				
			detalle.setId(detalleTC.getIdentificador().getIdTarjeta());
			detalle.setNumeroTarjeta(detalleTC.getNumeroTarjeta());
			detalle.setTipoCodigo(detalleTC.getTipo().getCodigo());
			detalle.setTipoDescripcion(detalleTC.getTipo().getDescripcion());
			detalle.setNumeroCuenta(detalleTC.getNumeroCuenta());
			detalle.setTipoCuenta(detalleTC.getTipoCuenta());
			detalle.setDigitoVerifCuenta(detalleTC.getDigitoVerifCuenta());
			if (detalleTC.getSaldoActual().getSaldoPesos() != null)
				detalle.setSaldoActualPesos(detalleTC.getSaldoActual().getSaldoPesos());
			if (detalleTC.getSaldoActual().getSaldoDolares() != null)
				detalle.setSaldoActualDolares(detalleTC.getSaldoActual().getSaldoDolares());
			if (detalleTC.getSaldoAnterior().getSaldoPesos() != null)
				detalle.setSaldoAnteriorPesos(detalleTC.getSaldoAnterior().getSaldoPesos());
			if (detalleTC.getSaldoAnterior().getSaldoDolares() != null)
				detalle.setSaldoAnteriorDolares(detalleTC.getSaldoAnterior().getSaldoDolares());
			detalle.setPagoMinimo(detalleTC.getPagoMinimo());
			detalle.setFechaCierreAnterior(detalleTC.getFechaCierreAnterior() != null ? detalleTC.getFechaCierreAnterior().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
			detalle.setFechaCierreActual(detalleTC.getFechaCierreActual() != null ? detalleTC.getFechaCierreActual().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
			detalle.setFechaCierreProximo(detalleTC.getFechaCierreProximo() != null ? detalleTC.getFechaCierreProximo().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
			detalle.setFechaVtoAnterior(detalleTC.getFechaVtoAnterior() != null ? detalleTC.getFechaVtoAnterior().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
			detalle.setFechaVtoActual(detalleTC.getFechaVtoActual() != null ? detalleTC.getFechaVtoActual().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
			detalle.setVigenciaDesde(detalleTC.getVigenciaDesde());
			detalle.setVigenciaHasta(detalleTC.getVigenciaHasta());
			detalle.setFormaPagoCodigo(detalleTC.getFormaPago().getCodigo());
			detalle.setFormaPagoDescripcion(detalleTC.getFormaPago().getDescripcion());
			detalle.setCuentaDebito(detalleTC.getCuentaDebito());
			detalle.setTipoCuentaDebitoCodigo(detalleTC.getTipoCuentaDebito().getCodigo());
			detalle.setTipoCuentaDebitoDescripcion(detalleTC.getTipoCuentaDebito().getDescripcion());
			detalle.setEsPreembozado(detalleTC.getEsPreembozado());
			detalle.setRenovacionBonificada(detalle.getRenovacionBonificada());
			Tasa tasa = new Tasa();
			tasa.setEfectivaMensualPesos(detalleTC.getTasas().getEfectivaMensualPesos());
			tasa.setEfectivaMensualDolares(detalleTC.getTasas().getEfectivaMensualDolares());
			tasa.setNominalAnualPesos(detalleTC.getTasas().getNominalAnualPesos());
			tasa.setNominalAnualDolares(detalleTC.getTasas().getNominalAnualDolares());
			detalle.setTasas(tasa);
			detalle.setOfflineLimiteCompra(detalleTC.getOffLine().getLimiteCompra());
			detalle.setOfflineLimiteFinanciacion(detalleTC.getOffLine().getLimiteFinanciacion());
			detalle.setOfflineLimitePrestamo(detalleTC.getOffLine().getLimitePrestamo());
			detalle.setOfflineLimiteDisponible(detalleTC.getOffLine().getLimiteDisponible());
		}
		return detalle;
	}
	
	
	public List<TarjetaCredito> consultaListado(Documento documento) throws Exception{
	
		SoapHeaderReq header = generateHeader();	
	
		List<TarjetaCredito> tarjetas = new ArrayList<>();
		
		ReqConsultaListado4 entrada = new ReqConsultaListado4();
		DataConsultaListadoReqType4 dcl = new DataConsultaListadoReqType4();
		PagingType pagingType = new PagingType();
		pagingType.setGetPage(1);
		pagingType.setPageSize(10);
		pagingType.setTotalRows(BigInteger.valueOf(10));
		entrada.setPaging(pagingType);
		dcl.setIdentificador(generateIDCliente(documento));
		entrada.setData(dcl);

		
		Holder<RespConsultaListado4> salida = new Holder<>(new RespConsultaListado4());
		
		soaTarjetaCreditoV12.consultaListado(entrada, header, salida,	new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.tarjetacredito.consultalistado_v1.DataConsultaListadoRespType4.Row row : salida.value.getData().getRow()) {
				
				TarjetaCredito tc = new TarjetaCredito();
				DetalleTarjeta detalle = consultaDetalle(row.getIdentificador().getIdTarjeta(),row.getIdentificador().getDataTypeName());
				IdTarjeta idt = new IdTarjeta();
				idt.setDataTypeName(row.getIdentificador().getDataTypeName());
				idt.setNumero(row.getIdentificador().getIdTarjeta());
				idt.setId(row.getIdentificador().getIdTarjeta()+row.getIdentificador().getDataTypeName()+"_");
				tc.setId(idt);
				tc.setIdCliente(row.getListado().getIdCliente());
				tc.setMarcaCodigo(row.getListado().getMarca().getCodigo());
				tc.setMarcaDescripcion(row.getListado().getMarca().getDescripcion());
				tc.setCuentaTarjeta(row.getListado().getCuentaTarjeta());
				tc.setDenominacion(row.getListado().getDenominacion());
				tc.setTipoTarjetacodigo(row.getListado().getTipoTarjeta().getCodigo());
				tc.setTipoTarjetaDescripcion(row.getListado().getTipoTarjeta().getDescripcion());
				tc.setSigla(row.getListado().getSigla());
				tc.setTitularidadCodigo(row.getListado().getTitularidad().getCodigo());
				tc.setTitularidadDescripcion(row.getListado().getTitularidad().getDescripcion());
				tc.setCarteraCodigo(row.getListado().getCartera().getCodigo());
				tc.setCarteraDescripcion(row.getListado().getCartera().getDescripcion());
				tc.setVigencia(row.getListado().getVigencia() != null ? row.getListado().getVigencia().getValue().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				tc.setFechaProcesamiento(row.getListado().getFechaProcesamiento() != null ? row.getListado().getFechaProcesamiento().getValue().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				tc.setFecha(row.getListado().getFecha() != null ? row.getListado().getFecha().getValue().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				tc.setEntidadCodigo(row.getListado().getEntidad().getCodigo());
				tc.setEntidadDescripcion(row.getListado().getEntidad().getDescripcion());
				tc.setBancoCodigo(row.getListado().getBanco().getCodigo());
				tc.setBancoDescripcion(row.getListado().getBanco().getDescripcion());
				tc.setSaldoPesos(row.getListado().getSaldo().getPesos());
				tc.setSaldoDolares(row.getListado().getSaldo().getDolares());
				tc.setCierreFecha(row.getListado().getCierreFecha() != null ? row.getListado().getCierreFecha().getValue().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				tc.setEstadoCodigo(row.getListado().getEstado().getCodigo());
				tc.setEstadoDescripcion(row.getListado().getEstado().getDescripcion());		
				tc.setMarcaInhabilitacionCodigo(row.getListado().getMarcaInhabilitacion().getCodigo());
				tc.setMarcaInhabilitacionDescripcion(row.getListado().getMarcaInhabilitacion().getDescripcion());
				tc.setLimiteCompra(row.getListado().getLimiteCompra());
				tc.setFormaPagoCodigo(row.getListado().getFormaPago().getCodigo());
				tc.setFormaPagoDescripcion(row.getListado().getFormaPago().getDescripcion());
				tc.setCuentaNroBT(row.getListado().getCuentaNroBT());
				tc.setProductoCodigo(row.getListado().getProducto().getCodigo());
				tc.setProductoDescripcion(row.getListado().getProducto().getDescripcion());
				tc.setGafCodigo(row.getListado().getGaf().getCodigo());
				tc.setGafDescripcion(row.getListado().getGaf().getDescripcion());
				tc.setProdSigla(row.getListado().getProdSigla());
				tc.setTipoCtaCodigo(row.getListado().getTipoCta().getCodigo());
				tc.setTipoCtaDescripcion(row.getListado().getTipoCta().getDescripcion());
				tc.setFecha_cierre(detalle.getFechaCierreProximo());
				tc.setFecha_vencimiento(detalle.getFechaVtoActual());
				tc.setPago_minimo(detalle.getPagoMinimo());
				tarjetas.add(tc);
			}
		}
		return tarjetas;
	
	}
	
	
	public Limites consultaLimites(String id) throws Exception {
		Limites limites = new Limites();
		
		SoapHeaderReq header = generateHeader();	
		IdTarjeta idTarjeta = new IdTarjeta(id); 
		ReqConsultaLimites entrada = new ReqConsultaLimites();
		DataConsultaLimitesReqType dcl = new DataConsultaLimitesReqType();
		
		IdTarjetaType idtt = new IdTarjetaType();
		idtt.setDataTypeName(idTarjeta.getDataTypeName());
		idtt.setIdTarjeta(idTarjeta.getNumero());
		dcl.setIdentificador(idtt);
		entrada.setData(dcl);
		
		Holder<RespConsultaLimites> salida = new Holder<>(new RespConsultaLimites());
		
		soaTarjetaCreditoV1.consultaLimites(entrada, header, salida,	new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			ar.com.supervielle.xsd.integracion.tarjetacredito.consultalimites_v1.DataConsultaLimitesRespType.Row row = salida.value.getData().getRow();
			
			limites.setLimiteCompra(row.getLimiteCompra());
			limites.setLimiteCompraCuotas(row.getLimiteCompraCuotas());
			limites.setLimiteCompraDisponible(row.getLimiteCompraDisponible());
			limites.setLimiteCompraCuotasDisponible(row.getLimiteCompraCuotasDisponible());
			limites.setLimiteCompraDisponibleAdelantos(row.getLimiteCompraDisponibleAdelantos());
			limites.setLimiteOperacionesInternacionales(row.getLimiteOperacionesInternacionales());
			limites.setLimiteFinanaciacion(row.getLimiteFinanciacion());
			limites.setLimiteAdelantos(row.getLimiteAdelantos());
		}
		return limites;
	}
	
	
	public List<MovimientoTarjeta> consultaMovimientos(String id) throws Exception {
		
		List<MovimientoTarjeta> movimientos = new ArrayList<>();
		
		SoapHeaderReq header = generateHeader();	
		IdTarjeta idTarjeta = new IdTarjeta(id); 
		ReqConsultaMovimientos entrada = new ReqConsultaMovimientos();
		DataConsultaMovimientosReqType dcl = new DataConsultaMovimientosReqType();
	
		IdTarjetaType idtt = new IdTarjetaType();
		idtt.setDataTypeName(idTarjeta.getDataTypeName());
		idtt.setIdTarjeta(idTarjeta.getNumero());
		dcl.setIdentificador(idtt);
		entrada.setData(dcl);
		
		Holder<RespConsultaMovimientos> salida = new Holder<>(new RespConsultaMovimientos());

		soaTarjetaCreditoV1.consultaMovimientos(entrada, header, salida,	new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.tarjetacredito.consultamovimientos_v1.DataConsultaMovimientosRespType.Row row : salida.value.getData().getRow()) {
				
				MovimientoTarjeta mov = new MovimientoTarjeta();
				IdTarjeta idt = new IdTarjeta();
				idt.setId(row.getIdentificador().getDataTypeName()+row.getIdentificador().getIdTarjeta()+"_");
				idt.setDataTypeName(row.getIdentificador().getDataTypeName());
				idt.setNumero(row.getIdentificador().getIdTarjeta());
				mov.setIdTarjeta(idt);
				mov.setFecha(row.getFecha() != null ? row.getFecha().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				mov.setDescripcion(row.getDescripcion());
				mov.setTipoTransaccionCodigo(row.getTipoTransaccion().getCodigo());
				mov.setTipoTansaccionDescripcion(row.getTipoTransaccion().getDescripcion());
				mov.setMoneda(row.getMoneda());
				mov.setImporte(row.getImporte());
				movimientos.add(mov);
			}
		}
		return movimientos;
	}
}
