package ar.com.wrapper.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import ar.com.supervielle.wsdl.servicios.integracion.beneficiossupervielle_v1.FaultMessage;
import ar.com.wrapper.dto.ClubPuntos;
import ar.com.wrapper.dto.Documento;
import org.springframework.stereotype.Service;

import ar.com.supervielle.wsdl.servicios.integracion.beneficiossupervielle_v1.BeneficiosSuperviellePortTypeV11;
import ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.DataConsultaPuntosReqType2;
import ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.ReqConsultaPuntos2;
import ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.RespConsultaPuntos2;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderRes;

@Service
public class SOAAdapterBeneficios extends SOAAdapter{
	
	@Resource(name = "soaBeneficiosV11")
	private BeneficiosSuperviellePortTypeV11 soaBeneficiosV11;
	
	
	public List<ClubPuntos> consultaPuntos(Documento documento) throws Exception {
		
		SoapHeaderReq header = generateHeader();
		
		List<ClubPuntos> puntos = new ArrayList<>();
		ReqConsultaPuntos2 entrada = new ReqConsultaPuntos2();
		DataConsultaPuntosReqType2 dcp = new DataConsultaPuntosReqType2();
		dcp.setIdentificador(generateIDCliente(documento));
		entrada.setData(dcp);
		Holder<RespConsultaPuntos2> salida = new Holder<>(new RespConsultaPuntos2());
		

		soaBeneficiosV11.consultaPuntos(entrada, header, salida, new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.beneficiossupervielle.consultapuntos_v1.DataConsultaPuntosRespType2.Row row : salida.value.getData().getRow()) {
				
				ClubPuntos cp = new ClubPuntos();
				cp.setResultado(row.getResultado());
				cp.setProgramaCodigo(row.getPrograma().getCodigo());
				cp.setProgramaDescripcion(row.getPrograma().getDescripcion());
				cp.setTitular(row.getTitular());
				cp.setIdEstadoCuenta(row.getIdEstadoCuenta());
				cp.setEstadoCuenta(row.getEstadoCuenta());
				cp.setFechaAlta(row.getFechaAlta() != null ? row.getFechaAlta().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				cp.setFechaBaja(row.getFechaBaja() != null ? row.getFechaBaja().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				cp.setPuntosDisponibles(row.getPuntosDisponibles());
				cp.setPuntosAVencerEsteMes(row.getPuntosAVencerEsteMes());
				cp.setPuntosAVencerProximoMes(row.getPuntosAVencerProximoMes());
				cp.setPuntosAVencer2Meses(row.getPuntosAVencer2Meses());
				cp.setPuntosAVencer3Meses(row.getPuntosAVencer3Meses());
				puntos.add(cp);
				
			}
		}
		return puntos;
		
	}
	
	
	
}
