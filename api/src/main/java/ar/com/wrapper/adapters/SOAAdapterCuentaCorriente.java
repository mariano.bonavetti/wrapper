package ar.com.wrapper.adapters;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import org.springframework.stereotype.Service;

import ar.com.supervielle.wsdl.servicios.integracion.cuenta.ctacte_v1.CuentaCorrientePortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.ctacte_v1.CuentaCorrientePortTypeV12;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;
import ar.com.supervielle.xsd.integracion.common.pagingtypes_v1.PagingType;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1.DataConsultaSaldosReqType;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1.ReqConsultaSaldos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1.RespConsultaSaldos;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.ByClienteId;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.DataConsultaListadoReqType;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.ReqConsultaListado;
import ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.RespConsultaListado;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;
import ar.com.wrapper.dto.Cuenta;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdCuentaBANTOTAL;
import ar.com.wrapper.dto.Moneda;
import ar.com.wrapper.dto.SaldoCompleto;

@Service
public class SOAAdapterCuentaCorriente extends SOAAdapter {
	
	@Resource(name = "soaCuentaCorrienteV12")
	private CuentaCorrientePortTypeV12 soaCuentaCorrienteV12;
	
	@Resource(name = "soaCuentaCorrienteV1")
	private CuentaCorrientePortTypeV1 soaCuentaCorrienteV1;
	
	public List<Cuenta> consultaListado(Documento documento) throws Exception{

		SoapHeaderReq header = generateHeader();	

		List<Cuenta> cuentas = new ArrayList<Cuenta>();
		
		ReqConsultaListado entrada = new ReqConsultaListado();
		DataConsultaListadoReqType dcl = new DataConsultaListadoReqType();
		PagingType pagingType = new PagingType();
		pagingType.setGetPage(1);
		pagingType.setPageSize(10);
		pagingType.setTotalRows(BigInteger.valueOf(10));
		entrada.setPaging(pagingType);
		ByClienteId value = new ByClienteId();
		value.setEstado("T");
		value.setTipoVinculo("A");
		value.setOrdenFechaAlta("A");
		value.setIdentificador(generateIDCliente(documento)); 
		dcl.setByClienteId(value);
		entrada.setData(dcl);
		
		Holder<RespConsultaListado> salida = new Holder<>(new RespConsultaListado());

		soaCuentaCorrienteV12.consultaListadoV12(entrada, header, salida,	new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultalistado_v1.DataConsultaListadoRespType.Row row : salida.value.getData().getRow()) {
				
				Cuenta c = new Cuenta();
				IdCuentaBANTOTAL idb = new IdCuentaBANTOTAL();
				idb.setEmpresa(row.getIdentificador().getEmpresa());
				idb.setSucursal(row.getIdentificador().getSucursal());
				idb.setModulo(row.getIdentificador().getModulo());
				idb.setMoneda(row.getIdentificador().getMoneda());
				idb.setPapel(row.getIdentificador().getPapel());
				idb.setCuenta(row.getIdentificador().getCuenta());
				idb.setOperacion(row.getIdentificador().getOperacion());
				idb.setSubCuenta(row.getIdentificador().getSubCuenta());
				idb.setTipoOpe(row.getIdentificador().getTipoOpe());
				c.setCuentaBT(idb);
				c.setSucursalCodigo(row.getSucursal().getCodigo());
				c.setSucursalDescripcion(row.getSucursal().getDescripcion());
				c.setCuentaCodigo(row.getCuenta().getCodigo());
				c.setCuentaDescripcion(row.getCuenta().getDescripcion());
				c.setCbu(row.getCbu());
				c.setVinculoCodigo(row.getVinculo().getCodigo());
				c.setVinculoDescripcion(row.getVinculo().getDescripcion());
				Moneda moneda = new Moneda();
				moneda.setCodigo(row.getMoneda().getCodigo());
				moneda.setDescripcion(row.getMoneda().getDescripcion());
				moneda.setSimbolo(row.getMoneda().getSimbolo());
				c.setMoneda(moneda);
				c.setTipoProductoCodigo(row.getTipoProducto().getCodigo());
				c.setTipoProductoDescripcion(row.getTipoProducto().getDescripcion());
				c.setPaqueteCodigo(row.getPaquete() != null ?  row.getPaquete().getCodigo() : null);
				c.setPaqueteDescripcion(row.getPaquete() != null ? row.getPaquete().getDescripcion() : null);
				c.setAcuerdo(row.getAcuerdo());
				c.setSaldo(row.getSaldo());
				c.setInteres(row.getInteres());
				c.setFechaUltimoMov(row.getFechaUltimoMov() != null ? row.getFechaUltimoMov().toGregorianCalendar().getTime() : null);
				c.setFechaAlta(row.getFechaAlta() != null ? row.getFechaAlta().toGregorianCalendar().getTime() : null);
				c.setFechaBaja(row.getFechaBaja() != null ? row.getFechaBaja().toGregorianCalendar().getTime() : null);
				c.setEstadoCodigo(row.getEstado().getCodigo());
				c.setEstadoDescripcion(row.getEstado().getDescripcion());				

				cuentas.add(c);
				
			}
		}
		return cuentas;
	}

	public List<SaldoCompleto> consultaSaldos(String id) throws Exception{
		List<SaldoCompleto> saldos = new ArrayList<>();
		SoapHeaderReq header = generateHeader();	

		ReqConsultaSaldos entrada = new ReqConsultaSaldos();
		IdCuentaBANTOTALType idBANTOTAL = new IdCuentaBANTOTALType();
		IdCuentaBANTOTAL idCuentaBANTOTAL = new IdCuentaBANTOTAL(id);
		idBANTOTAL.setEmpresa(idCuentaBANTOTAL.getEmpresa());
		idBANTOTAL.setSucursal(idCuentaBANTOTAL.getSucursal());
		idBANTOTAL.setModulo(idCuentaBANTOTAL.getModulo());
		idBANTOTAL.setMoneda(idCuentaBANTOTAL.getMoneda());
		idBANTOTAL.setPapel(idCuentaBANTOTAL.getPapel());
		idBANTOTAL.setCuenta(idCuentaBANTOTAL.getCuenta());
		idBANTOTAL.setOperacion(idCuentaBANTOTAL.getOperacion());
		idBANTOTAL.setSubCuenta(idCuentaBANTOTAL.getSubCuenta());
		idBANTOTAL.setTipoOpe(idCuentaBANTOTAL.getTipoOpe());
		idBANTOTAL.setDataTypeName("BANTOTAL");
		
		DataConsultaSaldosReqType data = new DataConsultaSaldosReqType();
		data.setIdentificador(idBANTOTAL);
		entrada.setData(data);
		
		Holder<RespConsultaSaldos> salida = new Holder<>(new RespConsultaSaldos());

		soaCuentaCorrienteV1.consultaSaldos(entrada, header, salida, new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.cuenta.ctacte.consultasaldos_v1.DataConsultaSaldosRespType.Row row : salida.value.getData().getRow()) {
				
				SaldoCompleto saldo = new SaldoCompleto();
				IdCuentaBANTOTAL idb = new IdCuentaBANTOTAL();
				idb.setEmpresa(row.getIdentificador().getEmpresa());
				idb.setSucursal(row.getIdentificador().getSucursal());
				idb.setModulo(row.getIdentificador().getModulo());
				idb.setMoneda(row.getIdentificador().getMoneda());
				idb.setPapel(row.getIdentificador().getPapel());
				idb.setCuenta(row.getIdentificador().getCuenta());
				idb.setOperacion(row.getIdentificador().getOperacion());
				idb.setSubCuenta(row.getIdentificador().getSubCuenta());
				idb.setTipoOpe(row.getIdentificador().getTipoOpe());
				saldo.setCuenta(idb);
				saldo.setSobregiroFormal(row.getSobregiro());
				saldo.setSaldoBloqueado(row.getSaldoBloqueado());
				saldo.setSaldoContable(row.getSaldoContable());
				saldo.setSaldoDisponible(row.getSaldoDisponible());
				saldo.setAcuerdo(row.getAcuerdo());				
				saldos.add(saldo);
			}	
		}	
		return saldos;
	}
}
