package ar.com.wrapper.adapters;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import ar.com.wrapper.util.XMLGregorianCalendarConverter;
import org.springframework.stereotype.Service;

import ar.com.supervielle.wsdl.servicios.integracion.cuenta.cajaahorro_v1.CajadeAhorroPortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.cajaahorro_v1.CajadeAhorroPortTypeV11;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.cajaahorro_v1.CajadeAhorroPortTypeV12;
import ar.com.supervielle.xsd.integracion.common.commontypes_v1.IdCuentaBANTOTALType;
import ar.com.supervielle.xsd.integracion.common.pagingtypes_v1.PagingType;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistado_v1.ByClienteId;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistado_v1.DataConsultaListadoReqType;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistado_v1.ReqConsultaListado;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistado_v1.RespConsultaListado;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.DataConsultaMovReqType2;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.DataConsultaMovReqType2.RangoFecha;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.ReqConsultaMovimientos2;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.RespConsultaMovimientos2;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultasaldos_v1.DataConsultaSaldosReqType;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultasaldos_v1.ReqConsultaSaldos;
import ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultasaldos_v1.RespConsultaSaldos;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;
import ar.com.wrapper.dto.Cuenta;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdCuentaBANTOTAL;
import ar.com.wrapper.dto.Moneda;
import ar.com.wrapper.dto.Movimiento;
import ar.com.wrapper.dto.Renglon;
import ar.com.wrapper.dto.ResumenMovimientos;
import ar.com.wrapper.dto.Saldo;
import ar.com.wrapper.dto.SaldoCompleto;

@Service
public class SOAAdapterCajaDeAhorro extends SOAAdapter {
	
		
	@Resource(name = "soaCajadeAhorroV12")
	private CajadeAhorroPortTypeV12 soaCajadeAhorroV12;

	@Resource(name = "soaCajadeAhorroV11")
	private CajadeAhorroPortTypeV11 soaCajadeAhorroV11;
	
	@Resource (name = "soaCajadeAhorroV1")
	private CajadeAhorroPortTypeV1 soaCajadeAhorroV1;
	
	public List<Cuenta> consultaListado(Documento documento) throws Exception{
		

		SoapHeaderReq header = generateHeader();	

		List<Cuenta> cuentas = new ArrayList<>();
		
		ReqConsultaListado entrada = new ReqConsultaListado();
		DataConsultaListadoReqType dcl = new DataConsultaListadoReqType();
		PagingType pagingType = new PagingType();
		pagingType.setGetPage(1);
		pagingType.setPageSize(10);
		pagingType.setTotalRows(BigInteger.valueOf(10));
		entrada.setPaging(pagingType);
		ByClienteId value = new ByClienteId();
		value.setEstado("T");
		value.setTipoVinculo("A");
		value.setOrdenFechaAlta("A");
		value.setIdentificador(generateIDCliente(documento)); 
		dcl.setByClienteId(value);
		entrada.setData(dcl);
		
		Holder<RespConsultaListado> salida = new Holder<>(new RespConsultaListado());
		

		soaCajadeAhorroV12.consultaListadoV12(entrada, header, salida,	new Holder<>());


		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultalistado_v1.DataConsultaListadoRespType.Row row : salida.value.getData().getRow()) {
				
				Cuenta c = new Cuenta();
				IdCuentaBANTOTAL idb = new IdCuentaBANTOTAL();
				idb.setEmpresa(row.getIdentificador().getEmpresa());
				idb.setSucursal(row.getIdentificador().getSucursal());
				idb.setModulo(row.getIdentificador().getModulo());
				idb.setMoneda(row.getIdentificador().getMoneda());
				idb.setPapel(row.getIdentificador().getPapel());
				idb.setCuenta(row.getIdentificador().getCuenta());
				idb.setOperacion(row.getIdentificador().getOperacion());
				idb.setSubCuenta(row.getIdentificador().getSubCuenta());
				idb.setTipoOpe(row.getIdentificador().getTipoOpe());
				c.setCuentaBT(idb);
				c.setSucursalCodigo(row.getSucursal().getCodigo());
				c.setSucursalDescripcion(row.getSucursal().getDescripcion());
				c.setCuentaCodigo(row.getCuenta().getCodigo());
				c.setCuentaDescripcion(row.getCuenta().getDescripcion());
				c.setCbu(row.getCbu());
				c.setVinculoCodigo(row.getVinculo().getCodigo());
				c.setVinculoDescripcion(row.getVinculo().getDescripcion());
				Moneda moneda = new Moneda();
				moneda.setCodigo(row.getMoneda().getCodigo());
				moneda.setDescripcion(row.getMoneda().getDescripcion());
				moneda.setSimbolo(row.getMoneda().getSimbolo());
				c.setMoneda(moneda);
				c.setTipoProductoCodigo(row.getTipoProducto().getCodigo());
				c.setTipoProductoDescripcion(row.getTipoProducto().getDescripcion());
				c.setPaqueteCodigo(row.getPaquete() != null ?  row.getPaquete().getCodigo() : null);
				c.setPaqueteDescripcion(row.getPaquete() != null ? row.getPaquete().getDescripcion() : null);
				c.setAcuerdo(row.getAcuerdo());
				c.setSaldo(row.getSaldo());
				c.setInteres(row.getInteres());
				c.setFechaUltimoMov(row.getFechaUltimoMov() != null ? row.getFechaUltimoMov().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				c.setFechaAlta(row.getFechaAlta() != null ? row.getFechaAlta().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				c.setFechaBaja(row.getFechaBaja() != null ? row.getFechaBaja().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				c.setEstadoCodigo(row.getEstado().getCodigo());
				c.setEstadoDescripcion(row.getEstado().getDescripcion());				

				cuentas.add(c);
				
			}
		}
		return cuentas;
	}
	
	
	public ResumenMovimientos consultaMovimientos(String id, Date fechaDesde, Date fechaHasta) throws Exception{
		
		SoapHeaderReq header = generateHeader();	
		
		ResumenMovimientos resumenMovimientos = new ResumenMovimientos();
		
		IdCuentaBANTOTALType idBANTOTAL = new IdCuentaBANTOTALType();
		IdCuentaBANTOTAL idCuentaBANTOTAL = new IdCuentaBANTOTAL(id);
		idBANTOTAL.setEmpresa(idCuentaBANTOTAL.getEmpresa());
		idBANTOTAL.setSucursal(idCuentaBANTOTAL.getSucursal());
		idBANTOTAL.setModulo(idCuentaBANTOTAL.getModulo());
		idBANTOTAL.setMoneda(idCuentaBANTOTAL.getMoneda());
		idBANTOTAL.setPapel(idCuentaBANTOTAL.getPapel());
		idBANTOTAL.setCuenta(idCuentaBANTOTAL.getCuenta());
		idBANTOTAL.setOperacion(idCuentaBANTOTAL.getOperacion());
		idBANTOTAL.setSubCuenta(idCuentaBANTOTAL.getSubCuenta());
		idBANTOTAL.setTipoOpe(idCuentaBANTOTAL.getTipoOpe());
		idBANTOTAL.setDataTypeName("BANTOTAL");
		
		ReqConsultaMovimientos2 entrada = new ReqConsultaMovimientos2();
		DataConsultaMovReqType2 data = new DataConsultaMovReqType2();
		data.setIdentificador(idBANTOTAL);
		data.setDebitoOCredito("T");
		data.setOrdenamiento("D"); 
		
		ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.DataConsultaMovReqType2.RangoFecha rangoFecha = new RangoFecha();
		rangoFecha.setFechaDesde(XMLGregorianCalendarConverter.asXMLGregorianCalendar(fechaDesde));
		rangoFecha.setFechaHasta(XMLGregorianCalendarConverter.asXMLGregorianCalendar(fechaHasta)); 
		data.setRangoFecha(rangoFecha);
		entrada.setData(data); 

		PagingType pagingType = new PagingType();
		pagingType.setGetPage(1);
		pagingType.setPageSize(10);
		pagingType.setTotalRows(BigInteger.valueOf(10));
		entrada.setPaging(pagingType);

		Holder<RespConsultaMovimientos2> salida = new Holder<>(new RespConsultaMovimientos2());
		
		soaCajadeAhorroV11.consultaMovimientos(entrada, header, salida,	new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			
			ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.DataConsultaMovRespType2.Row resumenMov = salida.value.getData().getRow().get(0);
			
			Saldo s = new Saldo();
			s.setActual(resumenMov.getSaldo().getActual());
			s.setAnterior(resumenMov.getSaldo().getAnterior());
			s.setBloqueado(resumenMov.getSaldo().getBloqueado());
			s.setNoDisponible(resumenMov.getSaldo().getNoDisponible());
			resumenMovimientos.setSaldo(s);
			List<Movimiento> movimientos = new ArrayList<>();
			resumenMovimientos.setSobregiroFormal(resumenMov.getSobregiroFormal());
			for ( ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultamovimientos_v1.DataConsultaMovRespType2.Row.Movimientos.Movimiento m : resumenMov.getMovimientos().getMovimiento()) {
			
				Movimiento movimiento = new Movimiento();
				movimiento.setFecha(m.getFecha().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime());
				movimiento.setTipoMovimientoCodigo(m.getTipoMovimiento().getCodigo());
				movimiento.setTipoMovimientoDescripcion(m.getTipoMovimiento().getDescripcion());
				movimiento.setSucursalCodigo(m.getSucursal().getCodigo());
				movimiento.setSucursalDescripcion(m.getSucursal().getDescripcion());
				movimiento.setCodigoConcepto(m.getCodigoConcepto());
				movimiento.setDebitoOCredito(m.getDebitoOCredito());
				movimiento.setImporte(m.getImporte());
				
				List<Renglon> renglones = new ArrayList<>();
				
				if(m.getRenglon1() != null)
					renglones.add(new Renglon(m.getRenglon1().getCodigo(), m.getRenglon1().getDescripcion()));
				if(m.getRenglon2() != null)
					renglones.add(new Renglon(m.getRenglon2().getCodigo(), m.getRenglon2().getDescripcion()));
				if(m.getRenglon3() != null)
					renglones.add(new Renglon(m.getRenglon3().getCodigo(), m.getRenglon3().getDescripcion()));
				if(m.getRenglon4() != null)
					renglones.add(new Renglon(m.getRenglon4().getCodigo(), m.getRenglon4().getDescripcion()));
				if(m.getRenglon5() != null)
					renglones.add(new Renglon(m.getRenglon5().getCodigo(), m.getRenglon5().getDescripcion()));
				if(m.getRenglon6() != null)
					renglones.add(new Renglon(m.getRenglon6().getCodigo(), m.getRenglon6().getDescripcion()));
				if(m.getRenglon7() != null)
					renglones.add(new Renglon(m.getRenglon7().getCodigo(), m.getRenglon7().getDescripcion()));
				if(m.getRenglon8() != null)
					renglones.add(new Renglon(m.getRenglon8().getCodigo(), m.getRenglon8().getDescripcion()));
				if(m.getRenglon9() != null)
					renglones.add(new Renglon(m.getRenglon9().getCodigo(), m.getRenglon9().getDescripcion()));
				
				movimientos.add(movimiento);
				
			}
			resumenMovimientos.setMovimientos(movimientos);
		}
	
		return resumenMovimientos;
		
	}
	
	
	public List<SaldoCompleto> consultaSaldos(String id) throws Exception{
		
		List<SaldoCompleto> saldos = new ArrayList<>();
		SoapHeaderReq header = generateHeader();	

		ReqConsultaSaldos entrada = new ReqConsultaSaldos();
		IdCuentaBANTOTALType idBANTOTAL = new IdCuentaBANTOTALType();
		IdCuentaBANTOTAL idCuentaBANTOTAL = new IdCuentaBANTOTAL(id);
		idBANTOTAL.setEmpresa(idCuentaBANTOTAL.getEmpresa());
		idBANTOTAL.setSucursal(idCuentaBANTOTAL.getSucursal());
		idBANTOTAL.setModulo(idCuentaBANTOTAL.getModulo());
		idBANTOTAL.setMoneda(idCuentaBANTOTAL.getMoneda());
		idBANTOTAL.setPapel(idCuentaBANTOTAL.getPapel());
		idBANTOTAL.setCuenta(idCuentaBANTOTAL.getCuenta());
		idBANTOTAL.setOperacion(idCuentaBANTOTAL.getOperacion());
		idBANTOTAL.setSubCuenta(idCuentaBANTOTAL.getSubCuenta());
		idBANTOTAL.setTipoOpe(idCuentaBANTOTAL.getTipoOpe());
		idBANTOTAL.setDataTypeName("BANTOTAL");
		
		DataConsultaSaldosReqType data = new DataConsultaSaldosReqType();
		data.setIdentificador(idBANTOTAL);
		entrada.setData(data);
		
		Holder<RespConsultaSaldos> salida = new Holder<>(new RespConsultaSaldos());

		soaCajadeAhorroV1.consultaSaldos(entrada, header, salida,new Holder<>());

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.cuenta.cajaahorro.consultasaldos_v1.DataConsultaSaldosRespType.Row row : salida.value.getData().getRow()) {
				
				SaldoCompleto saldo = new SaldoCompleto();
				IdCuentaBANTOTAL idb = new IdCuentaBANTOTAL();
				idb.setEmpresa(row.getIdentificador().getEmpresa());
				idb.setSucursal(row.getIdentificador().getSucursal());
				idb.setModulo(row.getIdentificador().getModulo());
				idb.setMoneda(row.getIdentificador().getMoneda());
				idb.setPapel(row.getIdentificador().getPapel());
				idb.setCuenta(row.getIdentificador().getCuenta());
				idb.setOperacion(row.getIdentificador().getOperacion());
				idb.setSubCuenta(row.getIdentificador().getSubCuenta());
				idb.setTipoOpe(row.getIdentificador().getTipoOpe());
				saldo.setCuenta(idb);
				saldo.setSobregiroFormal(row.getSobregiro());
				saldo.setSaldoBloqueado(row.getSaldoBloqueado());
				saldo.setSaldoContable(row.getSaldoContable());
				saldo.setSaldoDisponible(row.getSaldoDisponible());
				saldo.setAcuerdo(row.getAcuerdo());				
				saldos.add(saldo);
			}	
		}	
		return saldos;
	}
}
