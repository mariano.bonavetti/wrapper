package ar.com.wrapper.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

import ar.com.wrapper.dto.BeneficioPrevisional;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdClienteTipo;
import ar.com.wrapper.dto.IdLiquidacionAnses;
import org.springframework.stereotype.Service;

import ar.com.supervielle.wsdl.servicios.integracion.clienteprevisional_v1.ClientePrevisionalPortTypeV1;
import ar.com.supervielle.xsd.integracion.clienteprevisional.consultafechacobro_v1.DataConsultaFechaCobroReqType;
import ar.com.supervielle.xsd.integracion.clienteprevisional.consultafechacobro_v1.ReqConsultaFechaCobro;
import ar.com.supervielle.xsd.integracion.clienteprevisional.consultafechacobro_v1.RespConsultaFechaCobro;
import ar.com.supervielle.xsd.integracion.header_v1.SoapHeaderReq;

@Service
public class SOAAdapterPrevisional extends SOAAdapter {
	
	@Resource(name = "soaPrevisionalV1")
	private ClientePrevisionalPortTypeV1 soaPrevisionalV1;
	
	public List<BeneficioPrevisional> consultaFechaCobro(Documento documento) throws Exception{
		
		SoapHeaderReq header = generateHeader();
		
		List<BeneficioPrevisional> previsionales = new ArrayList<>();
		
		ReqConsultaFechaCobro entrada = new ReqConsultaFechaCobro();
		DataConsultaFechaCobroReqType dcf = new DataConsultaFechaCobroReqType();
		dcf.setIdentificador(generateIDCliente(documento));
		entrada.setData(dcf);
		
		Holder<RespConsultaFechaCobro> salida = new Holder<>(new RespConsultaFechaCobro());

		soaPrevisionalV1.consultaFechaCobro(header, entrada, new Holder<>(),salida);

		if (salida.value != null && salida.value.getData() != null && salida.value.getData().getRow() != null) {
			for (ar.com.supervielle.xsd.integracion.clienteprevisional.consultafechacobro_v1.DataConsultaFechaCobroRespType.Row row : salida.value.getData().getRow()) {
				
				BeneficioPrevisional previsional = new BeneficioPrevisional();
				IdLiquidacionAnses idAnses = new IdLiquidacionAnses();
				idAnses.setCuil(row.getIdentificador().getCuil());
				idAnses.setBeneficio(row.getIdentificador().getBeneficio());
				idAnses.setNroLiquidacion(row.getIdentificador().getNroLiquidacion());
				idAnses.setPeriodo(row.getIdentificador().getPeriodo());
				idAnses.setFormaPago(row.getIdentificador().getFormaPago());
				previsional.setId(idAnses);
				previsional.setNombreBeneficiario(row.getNombreBeneficiario());
				IdClienteTipo cliente = new IdClienteTipo();
				cliente.setPaisOrigen(row.getCliente().getPaisOrigen());
				cliente.setTipoDoc(row.getCliente().getTipoDoc());
				cliente.setNumDoc(row.getCliente().getNumDoc());
				previsional.setCliente(cliente);
				previsional.setFechaPago(row.getFechaPago() != null ? row.getFechaPago().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				previsional.setEstadoCodigo(row.getEstado().getCodigo());
				previsional.setEstadoDescripcion(row.getEstado().getDescripcion());
				previsional.setFechaSigPago(row.getFechaSigPago() != null ? row.getFechaSigPago().toGregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US, null).getTime() : null);
				previsional.setGrupoPagoCodigo(row.getGrupoPago().getCodigo());
				previsional.setGrupoPagoDescripcion(row.getGrupoPago().getDescripcion());
			}
		}
		
		return previsionales;
	}

}
