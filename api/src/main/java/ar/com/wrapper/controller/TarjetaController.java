package ar.com.wrapper.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.com.wrapper.adapters.SOAAdapterTarjetaCredito;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.Limites;
import ar.com.wrapper.dto.MovimientoTarjeta;
import ar.com.wrapper.dto.TarjetaCredito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TarjetaController {

	@Autowired
    SOAAdapterTarjetaCredito soaTarjeta;

	public static class RespuestaTarjeta implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public Id id;
		public Date fecha_cierre;
		public Date fecha_vencimiento;
		public BigDecimal pago_minimo;
		public String codigo_titularidad;
		public String codigo_estado;
		public String codigo_habilitacion;
		public String marca;
		public String tipo_tarjeta;
		public BigDecimal saldo_dolares;
		public BigDecimal saldo_pesos;
		public static class Id {
			public String dataTypeName;
			public String numero;
			public String id;
		}
	
	}
	
	public static class RespuestaLimites implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public BigDecimal compra;
		public BigDecimal compra_cuotas;
		public BigDecimal compra_disponible;
		public BigDecimal compra_cuotas_disponible;
		public BigDecimal adelanto;
		public BigDecimal adelanto_disponible;
		public BigDecimal financiacion;
		public BigDecimal operaciones_internacionales;
		
	}
	public static class RespuestaMovimiento implements Serializable {
		private static final long serialVersionUID = 1L;
	
		public Date fecha;
		public BigDecimal importe;
		public TipoMovimiento tipo_movimiento;
		public String moneda_movimiento;
		public Boolean esDebito;
		public static class TipoMovimiento{
			public String codigo;
			public String descripcion;
		}
	}




	@GetMapping("/tarjetas")
	public List<RespuestaTarjeta> consultaListado(@RequestParam String codPais, @RequestParam String codDoc, @RequestParam String nroDoc ) throws Exception{
		List<RespuestaTarjeta> respuesta = new ArrayList<>();
		Documento dni = new Documento(codDoc, nroDoc, codPais);
		List<TarjetaCredito> tarjetas = soaTarjeta.consultaListado(dni);
		if (tarjetas != null) {
			for (TarjetaCredito tc : tarjetas) {
				RespuestaTarjeta resp = new RespuestaTarjeta();
				resp.id = new RespuestaTarjeta.Id();
				resp.id.dataTypeName = tc.getId().getDataTypeName();
				resp.id.numero = tc.getId().getNumero();
				resp.id.id = tc.getId().getId();
				resp.fecha_cierre = tc.getFecha_cierre();
				resp.fecha_vencimiento = tc.getFecha_vencimiento();
				resp.pago_minimo = tc.getPago_minimo();
				resp.codigo_titularidad = tc.getTitularidadCodigo();
				resp.codigo_habilitacion = tc.getMarcaInhabilitacionCodigo();
				resp.codigo_estado = tc.getEstadoCodigo();
				
				if (tc.getMarcaCodigo().equals("1"))
					resp.marca = "Mastercard";
				else 
					resp.marca = "Visa";
				resp.tipo_tarjeta = tc.getTipoCtaDescripcion();
				resp.tipo_tarjeta = resp.tipo_tarjeta.replaceAll("MC", "Mastercard");
				
				if (tc.getSaldoDolares() != null)
					resp.saldo_dolares = tc.getSaldoDolares();
				
				if (tc.getSaldoPesos() != null)
					resp.saldo_pesos = tc.getSaldoPesos();
				
				respuesta.add(resp);
			}
		}
		return respuesta;
	}
	
	@GetMapping("tarjetas/{id}/limites")
	public RespuestaLimites consultaLimites(@PathVariable String id) throws Exception{
		RespuestaLimites respuesta = new RespuestaLimites();
		Limites limites = soaTarjeta.consultaLimites(id);
		if (limites != null) {
			respuesta.compra = limites.getLimiteCompra();
			respuesta.compra_cuotas = limites.getLimiteCompraCuotas();
			respuesta.compra_cuotas_disponible = limites.getLimiteCompraCuotasDisponible();
			respuesta.compra_disponible = limites.getLimiteCompraDisponible();
			respuesta.adelanto = limites.getLimiteAdelantos();
			respuesta.adelanto_disponible = limites.getLimiteCompraDisponibleAdelantos();
			respuesta.financiacion = limites.getLimiteFinanaciacion();
			respuesta.operaciones_internacionales = limites.getLimiteOperacionesInternacionales();
		}
		return respuesta;
	}
	
	@GetMapping("tarjetas/{id}/movimientos")
	public List<RespuestaMovimiento> consultaMovimientos(@PathVariable String id) throws Exception{
		List<RespuestaMovimiento> respuesta = new ArrayList<RespuestaMovimiento>();
		List<MovimientoTarjeta> movimientos = soaTarjeta.consultaMovimientos(id);
		if (movimientos != null ) {
			for(MovimientoTarjeta mov : movimientos) {
				RespuestaMovimiento resp = new RespuestaMovimiento();
				resp.fecha = mov.getFecha();
				resp.importe = mov.getImporte();
				resp.tipo_movimiento.descripcion = mov.getDescripcion();
				if (mov.getTipoTansaccionDescripcion().equals("Debito"))
					resp.esDebito = true;
				resp.moneda_movimiento = mov.getMoneda();
				respuesta.add(resp);
			}
		}
		return respuesta;
	}
	
}
