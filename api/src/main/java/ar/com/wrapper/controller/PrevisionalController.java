package ar.com.wrapper.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.com.wrapper.adapters.SOAAdapterPrevisional;
import ar.com.wrapper.dto.BeneficioPrevisional;
import ar.com.wrapper.dto.Documento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrevisionalController {

	@Autowired
    SOAAdapterPrevisional soaPrevisional;
	
	public static class RespuestaPrevisional implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public String id;
		public String beneficio;
		public String nroLiq;
		public Date fechaPago;
		public Estado estado;
		public Date fechaSigPago;
		public static class Estado {
			public String codigo;
			public String descripcion;
		}
		
		
	}
	
	
	@GetMapping("/previsionales")
	public List<RespuestaPrevisional> getPrevisionales(@RequestParam String codPais, @RequestParam String codDoc, @RequestParam String nroDoc) throws Exception{
		
		List<RespuestaPrevisional> respuesta = new ArrayList<RespuestaPrevisional>();
		Documento dni = new Documento(codDoc, nroDoc, codPais);
		List<BeneficioPrevisional> previsionales = soaPrevisional.consultaFechaCobro(dni);
		
		if (previsionales != null) {
			for(BeneficioPrevisional prev : previsionales ) {
				
				RespuestaPrevisional previsional = new RespuestaPrevisional();
				previsional.id = prev.getId().getBeneficio() + prev.getId().getNroLiquidacion();
				previsional.beneficio = prev.getId().getBeneficio();
				previsional.nroLiq = prev.getId().getNroLiquidacion();
				previsional.fechaPago = prev.getFechaPago();
				previsional.estado = new RespuestaPrevisional.Estado();
				previsional.estado.codigo = prev.getEstadoCodigo();
				previsional.estado.descripcion = prev.getEstadoDescripcion();
				previsional.fechaSigPago = prev.getFechaSigPago();
				respuesta.add(previsional);
			}
		}
		return respuesta;
	}
	
	
}
