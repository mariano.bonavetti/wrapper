package ar.com.wrapper.controller;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import ar.com.wrapper.adapters.SOAAdapterCajaDeAhorro;
import ar.com.wrapper.adapters.SOAAdapterCuentaCorriente;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;


import ar.com.wrapper.dto.Cuenta;
import ar.com.wrapper.dto.Documento;
import ar.com.wrapper.dto.IdCuentaBANTOTAL;
import ar.com.wrapper.dto.Movimiento;
import ar.com.wrapper.dto.ResumenMovimientos;
import ar.com.wrapper.dto.SaldoCompleto;

@RestController
public class CuentaController {

@Autowired
SOAAdapterCajaDeAhorro soaCA;
@Autowired
SOAAdapterCuentaCorriente soaCTACTE;

	private static final Logger logger = LogManager.getLogger(CuentaController.class);

	public static class RespuestaCuenta implements Serializable {
		private static final long serialVersionUID = 1L;
	
		public String tipo_producto;
		public String cbu;
		public Integer codigo_estado;
		public Integer codigo_titularidad;
		public BanTotal idBanTotal;
		public Moneda moneda;
		public String id;
		public BigDecimal saldo;
		public Date ultimo_movimiento;
		public static class BanTotal {
			public String empresa;
			public String sucursal;
			public String modulo;
			public String moneda;
			public String papel;
			public String cuenta;
			public String operacion;
			public String subOperacion;
			public String tipoOperacion;
			public String id;
		}
		public static class Moneda {
			public Integer codigo;
			public String descripcion;
			public String simbolo;
			
		}		
	}
	
	public static class RespuestaSaldo implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public BigDecimal sobregiro;
		public BigDecimal saldoBloqueado;
		public BigDecimal saldoContable;
		public BigDecimal saldoDisponible;
		public BigDecimal acuerdo;
	}
	
	public static class RespuestaMovimiento implements Serializable {
		private static final long serialVersionUID = 1L;
	
		public Date fecha;
		public BigDecimal importe;
		public TipoMovimiento tipo_movimiento;
		public Boolean esDebito;
		public String moneda_movimiento;
		public static class TipoMovimiento {
			public String codigo;
			public String descripcion;
		}
	
	}
	
	@GetMapping("/cuentas")
	public List<RespuestaCuenta> cuentas(@RequestParam String codPais, @RequestParam String codDoc, @RequestParam String nroDoc) {
		
		List<RespuestaCuenta> respuesta = new ArrayList<>();
		Documento dni = new Documento(codDoc, nroDoc, codPais);
		try {
			List<Cuenta> cuentasCA = soaCA.consultaListado(dni);
			if (cuentasCA != null) {
				for (Cuenta cuentaSoa : cuentasCA) {
					RespuestaCuenta cuenta = new RespuestaCuenta();
					cuenta.tipo_producto = "Caja de Ahorros";
					cuenta.cbu = cuentaSoa.getCbu();
					cuenta.codigo_estado = cuentaSoa.getEstadoCodigo();
					cuenta.codigo_titularidad = cuentaSoa.getVinculoCodigo();
					cuenta.id = cuentaSoa.getCuentaBT().getId();
					cuenta.idBanTotal = new RespuestaCuenta.BanTotal();
					cuenta.idBanTotal.empresa = cuentaSoa.getCuentaBT().getEmpresa();
					cuenta.idBanTotal.sucursal = cuentaSoa.getCuentaBT().getSucursal();
					cuenta.idBanTotal.modulo = cuentaSoa.getCuentaBT().getModulo();
					cuenta.idBanTotal.moneda = cuentaSoa.getCuentaBT().getMoneda();
					cuenta.idBanTotal.papel = cuentaSoa.getCuentaBT().getPapel();
					cuenta.idBanTotal.cuenta = cuentaSoa.getCuentaBT().getCuenta();
					cuenta.idBanTotal.operacion = cuentaSoa.getCuentaBT().getOperacion();
					cuenta.idBanTotal.subOperacion =  cuentaSoa.getCuentaBT().getSubCuenta();
					cuenta.idBanTotal.tipoOperacion =  cuentaSoa.getCuentaBT().getTipoOpe();
					cuenta.idBanTotal.id = cuentaSoa.getCuentaBT().getId();
					cuenta.moneda = new RespuestaCuenta.Moneda();
					cuenta.moneda.codigo = cuentaSoa.getMoneda().getCodigo();
					cuenta.moneda.descripcion = cuentaSoa.getMoneda().getDescripcion();
					cuenta.moneda.simbolo = cuentaSoa.getMoneda().getSimbolo();
					cuenta.saldo = cuentaSoa.getSaldo();
					cuenta.ultimo_movimiento = cuentaSoa.getFechaUltimoMov();
					respuesta.add(cuenta);
				}
			}
		} catch (Exception e) {
			logger.warn("SOA CajaDeAhorro", e);
		}

		try {
			List<Cuenta> cuentasCC = soaCTACTE.consultaListado(dni);
			if (cuentasCC != null) {
				for(Cuenta cuentaCC : cuentasCC) {
					RespuestaCuenta cuenta = new RespuestaCuenta();
					cuenta.tipo_producto = "Cuenta Corriente";
					cuenta.cbu = cuentaCC.getCbu();
					cuenta.codigo_estado = cuentaCC.getEstadoCodigo();
					cuenta.codigo_titularidad = cuentaCC.getVinculoCodigo();
					cuenta.id = cuentaCC.getCuentaBT().getId();
					cuenta.idBanTotal = new RespuestaCuenta.BanTotal();
					cuenta.idBanTotal.empresa = cuentaCC.getCuentaBT().getEmpresa();
					cuenta.idBanTotal.sucursal = cuentaCC.getCuentaBT().getSucursal();
					cuenta.idBanTotal.modulo = cuentaCC.getCuentaBT().getModulo();
					cuenta.idBanTotal.moneda = cuentaCC.getCuentaBT().getMoneda();
					cuenta.idBanTotal.papel = cuentaCC.getCuentaBT().getPapel();
					cuenta.idBanTotal.cuenta = cuentaCC.getCuentaBT().getCuenta();
					cuenta.idBanTotal.operacion = cuentaCC.getCuentaBT().getOperacion();
					cuenta.idBanTotal.subOperacion =  cuentaCC.getCuentaBT().getSubCuenta();
					cuenta.idBanTotal.tipoOperacion =  cuentaCC.getCuentaBT().getTipoOpe();
					cuenta.idBanTotal.id = cuentaCC.getCuentaBT().getId();
					cuenta.moneda = new RespuestaCuenta.Moneda();
					cuenta.moneda.codigo = cuentaCC.getMoneda().getCodigo();
					cuenta.moneda.descripcion = cuentaCC.getMoneda().getDescripcion();
					cuenta.moneda.simbolo = cuentaCC.getMoneda().getSimbolo();
					cuenta.saldo = cuentaCC.getSaldo();
					cuenta.ultimo_movimiento = cuentaCC.getFechaUltimoMov();
					respuesta.add(cuenta);
				}
			}
		} catch (Exception e) {
			logger.warn("SOA CuentaCorriente", e);
		}

		return respuesta;
	}

	@GetMapping("cuentas/{id}/movimientos")
	public List<RespuestaMovimiento> getMovimientos(@PathVariable String id,@RequestParam String fDesde,@RequestParam String fHasta) throws Exception{

		List<RespuestaMovimiento> respuesta = new ArrayList<>();
		Date fechaDesde = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(fDesde);
		Date fechaHasta = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(fHasta);
		ResumenMovimientos resumenMov = soaCA.consultaMovimientos(id,fechaDesde,fechaHasta);
		if (resumenMov != null) {
			for(Movimiento m : resumenMov.getMovimientos()) {
				RespuestaMovimiento resp = new RespuestaMovimiento();
				resp.fecha = m.getFecha();
				resp.tipo_movimiento = new RespuestaMovimiento.TipoMovimiento();
				resp.tipo_movimiento.codigo = m.getTipoMovimientoCodigo();
				resp.tipo_movimiento.descripcion = m.getTipoMovimientoDescripcion();
				if (m.getDebitoOCredito().equals("Db"))
					resp.importe = m.getImporte().abs();
				else
					resp.importe = m.getImporte();
				respuesta.add(resp);
			}
		}
		return respuesta;
	}
	
	@GetMapping("cuentas/{id}/saldos")
	public List<RespuestaSaldo> getSaldos(@PathVariable String id) throws Exception{
		
		List<RespuestaSaldo> respuesta = new ArrayList<RespuestaSaldo>();
		List<SaldoCompleto> saldos = new ArrayList<SaldoCompleto>();
		IdCuentaBANTOTAL idb = new IdCuentaBANTOTAL(id);
		if (idb.getModulo().equals("020")) {
			saldos = soaCA.consultaSaldos(id);
		}
		else if (idb.getModulo().equals("021")) {
			saldos = soaCTACTE.consultaSaldos(id);
		}
		
		if (saldos != null) {
			for(SaldoCompleto saldo : saldos) {
				RespuestaSaldo resp = new RespuestaSaldo();
				resp.sobregiro = saldo.getSobregiroFormal();
				resp.saldoBloqueado = saldo.getSaldoBloqueado();
				resp.saldoContable = saldo.getSaldoContable();
				resp.saldoDisponible = saldo.getSaldoDisponible();
				resp.acuerdo = saldo.getAcuerdo();
				respuesta.add(resp);
			}
		}		
		return respuesta;
	}
	

	
	
}
