package ar.com.wrapper.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.com.wrapper.adapters.SOAAdapterCliente;
import ar.com.wrapper.dto.Cliente;
import ar.com.wrapper.dto.Documento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {

	@Autowired
    SOAAdapterCliente soaCliente;
	
	
	public static class RespuestaCliente implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public String id;
		public String nombre;
		public String apellido;
		
	}
	
	@GetMapping("/clientes")
	public List<RespuestaCliente> cliente(@RequestParam String codPais, @RequestParam String codDoc, @RequestParam String nroDoc) throws Exception{
		
		List<RespuestaCliente> respuesta = new ArrayList<>();
		Documento dni = new Documento(codDoc, nroDoc, codPais);
		List<Cliente> clientes = soaCliente.consultaDatosPersonaFisica(dni);
		if (clientes != null) {
			for(Cliente cliente : clientes ) {			
				RespuestaCliente respC = new RespuestaCliente();
				respC.id = cliente.getIdCliente().getPaisOrigen() + cliente.getIdCliente().getTipoDoc() + cliente.getIdCliente().getNumDoc();
				respC.nombre = cliente.getNombre();
				respC.apellido = cliente.getApellido();
				respuesta.add(respC);
			}
		}
		return respuesta;
	}
}
