package ar.com.wrapper.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.com.wrapper.adapters.SOAAdapterBeneficios;
import ar.com.wrapper.dto.ClubPuntos;
import ar.com.wrapper.dto.Documento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClubPuntosController {
	
	public static class RespuestaPuntos implements Serializable {
		private static final long serialVersionUID = 1L;
		
		public Integer codigo;
		public String descripcion;
		public Integer puntosDisponibles;
		public Integer puntosAVencerMesActual;
		public Integer puntosAVencerProximoMes;
		public Integer puntosAVence2Meses;
		public Integer puntosAVence3Meses;
	}
	
	@Autowired
    SOAAdapterBeneficios soaBeneficios;
	
	@GetMapping("/clubpuntos")
	public List<RespuestaPuntos> consultaPuntos(@RequestParam String codPais, @RequestParam String codDoc, @RequestParam String nroDoc) throws Exception{
		
		List<RespuestaPuntos> respuesta = new ArrayList<>();
		Documento dni = new Documento(codDoc, nroDoc, codPais);
		List<ClubPuntos> puntos = soaBeneficios.consultaPuntos(dni);
		if (puntos != null) {
			for (ClubPuntos punto : puntos) {
				RespuestaPuntos resp = new RespuestaPuntos();
				resp.codigo = punto.getProgramaCodigo();
				resp.descripcion = punto.getProgramaDescripcion();
				resp.puntosDisponibles = punto.getPuntosDisponibles();
				resp.puntosAVencerMesActual = punto.getPuntosAVencerEsteMes();
				resp.puntosAVencerProximoMes = punto.getPuntosAVencerProximoMes();
				resp.puntosAVence2Meses = punto.getPuntosAVencer2Meses();
				resp.puntosAVence3Meses = punto.getPuntosAVencer3Meses();
				respuesta.add(resp);
			}
		}
		return respuesta;
	}
}
