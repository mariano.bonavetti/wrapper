package ar.com.wrapper.dto;

import java.math.BigDecimal;

public class Limites {

	private IdTarjeta id;
	private BigDecimal limiteCompra;
	private BigDecimal limiteCompraCuotas;
	private BigDecimal limiteCompraDisponible;
	private BigDecimal limiteCompraCuotasDisponible;
	private BigDecimal limiteCompraDisponibleAdelantos;
	private BigDecimal limiteOperacionesInternacionales;
	private BigDecimal limiteFinanaciacion;
	private BigDecimal limiteAdelantos;
	public IdTarjeta getId() {
		return id;
	}
	public void setId(IdTarjeta id) {
		this.id = id;
	}
	public BigDecimal getLimiteCompra() {
		return limiteCompra;
	}
	public void setLimiteCompra(BigDecimal limiteCompra) {
		this.limiteCompra = limiteCompra;
	}
	public BigDecimal getLimiteCompraCuotas() {
		return limiteCompraCuotas;
	}
	public void setLimiteCompraCuotas(BigDecimal limiteCompraCuotas) {
		this.limiteCompraCuotas = limiteCompraCuotas;
	}
	public BigDecimal getLimiteCompraDisponible() {
		return limiteCompraDisponible;
	}
	public void setLimiteCompraDisponible(BigDecimal limiteCompraDisponible) {
		this.limiteCompraDisponible = limiteCompraDisponible;
	}
	public BigDecimal getLimiteCompraCuotasDisponible() {
		return limiteCompraCuotasDisponible;
	}
	public void setLimiteCompraCuotasDisponible(BigDecimal limiteCompraCuotasDisponible) {
		this.limiteCompraCuotasDisponible = limiteCompraCuotasDisponible;
	}
	public BigDecimal getLimiteCompraDisponibleAdelantos() {
		return limiteCompraDisponibleAdelantos;
	}
	public void setLimiteCompraDisponibleAdelantos(BigDecimal limiteCompraDisponibleAdelantos) {
		this.limiteCompraDisponibleAdelantos = limiteCompraDisponibleAdelantos;
	}
	public BigDecimal getLimiteOperacionesInternacionales() {
		return limiteOperacionesInternacionales;
	}
	public void setLimiteOperacionesInternacionales(BigDecimal limiteOperacionesInternacionales) {
		this.limiteOperacionesInternacionales = limiteOperacionesInternacionales;
	}
	public BigDecimal getLimiteFinanaciacion() {
		return limiteFinanaciacion;
	}
	public void setLimiteFinanaciacion(BigDecimal limiteFinanaciacion) {
		this.limiteFinanaciacion = limiteFinanaciacion;
	}
	public BigDecimal getLimiteAdelantos() {
		return limiteAdelantos;
	}
	public void setLimiteAdelantos(BigDecimal limiteAdelantos) {
		this.limiteAdelantos = limiteAdelantos;
	}
	
	
}
