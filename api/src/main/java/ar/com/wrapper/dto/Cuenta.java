package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.Date;

public class Cuenta {
	
	private IdCuentaBANTOTAL cuentaBT;
	private Integer sucursalCodigo;
	private String sucursalDescripcion;
	private String cuentaCodigo;
	private String cuentaDescripcion;
	private String cbu;
	private Integer vinculoCodigo;
	private String vinculoDescripcion;
	private Moneda moneda;
	private Integer tipoProductoCodigo;
	private String tipoProductoDescripcion;
	private Integer paqueteCodigo;
	private String paqueteDescripcion;
	private BigDecimal acuerdo;
	private BigDecimal saldo;
	private BigDecimal interes;
	private Date fechaUltimoMov;
	private Date fechaAlta;
	private Date fechaBaja;
	private Integer estadoCodigo;
	private String estadoDescripcion;
	
	
	
	public Integer getSucursalCodigo() {
		return sucursalCodigo;
	}
	public void setSucursalCodigo(Integer sucursalCodigo) {
		this.sucursalCodigo = sucursalCodigo;
	}
	public String getSucursalDescripcion() {
		return sucursalDescripcion;
	}
	public void setSucursalDescripcion(String sucursalDescripcion) {
		this.sucursalDescripcion = sucursalDescripcion;
	}
	public String getCuentaCodigo() {
		return cuentaCodigo;
	}
	public void setCuentaCodigo(String cuentaCodigo) {
		this.cuentaCodigo = cuentaCodigo;
	}
	public String getCuentaDescripcion() {
		return cuentaDescripcion;
	}
	public void setCuentaDescripcion(String cuentaDescripcion) {
		this.cuentaDescripcion = cuentaDescripcion;
	}
	public Integer getVinculoCodigo() {
		return vinculoCodigo;
	}
	public void setVinculoCodigo(Integer vinculoCodigo) {
		this.vinculoCodigo = vinculoCodigo;
	}
	public String getVinculoDescripcion() {
		return vinculoDescripcion;
	}
	public void setVinculoDescripcion(String vinculoDescripcion) {
		this.vinculoDescripcion = vinculoDescripcion;
	}
	public Integer getTipoProductoCodigo() {
		return tipoProductoCodigo;
	}
	public void setTipoProductoCodigo(Integer tipoProductoCodigo) {
		this.tipoProductoCodigo = tipoProductoCodigo;
	}
	public String getTipoProductoDescripcion() {
		return tipoProductoDescripcion;
	}
	public void setTipoProductoDescripcion(String tipoProductoDescripcion) {
		this.tipoProductoDescripcion = tipoProductoDescripcion;
	}
	public Integer getPaqueteCodigo() {
		return paqueteCodigo;
	}
	public void setPaqueteCodigo(Integer paqueteCodigo) {
		this.paqueteCodigo = paqueteCodigo;
	}
	public String getPaqueteDescripcion() {
		return paqueteDescripcion;
	}
	public void setPaqueteDescripcion(String paqueteDescripcion) {
		this.paqueteDescripcion = paqueteDescripcion;
	}
	public BigDecimal getAcuerdo() {
		return acuerdo;
	}
	public void setAcuerdo(BigDecimal acuerdo) {
		this.acuerdo = acuerdo;
	}
	public BigDecimal getInteres() {
		return interes;
	}
	public void setInteres(BigDecimal interes) {
		this.interes = interes;
	}
	public Date getFechaUltimoMov() {
		return fechaUltimoMov;
	}
	public void setFechaUltimoMov(Date fechaUltimoMov) {
		this.fechaUltimoMov = fechaUltimoMov;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public Integer getEstadoCodigo() {
		return estadoCodigo;
	}
	public void setEstadoCodigo(Integer estadoCodigo) {
		this.estadoCodigo = estadoCodigo;
	}
	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}
	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public Moneda getMoneda() {
		return moneda;
	}
	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}
	public IdCuentaBANTOTAL getCuentaBT() {
		return cuentaBT;
	}
	public void setCuentaBT(IdCuentaBANTOTAL cuentaBT) {
		this.cuentaBT = cuentaBT;
	}

	public String getCbu() {
		return cbu;
	}
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
	
}
