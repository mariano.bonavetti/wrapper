package ar.com.wrapper.dto;

import java.math.BigDecimal;

public class Saldo {
	
	private BigDecimal anterior;
	private BigDecimal actual;
	private BigDecimal bloqueado;
	private BigDecimal noDisponible;
	
	public BigDecimal getAnterior() {
		return anterior;
	}
	public void setAnterior(BigDecimal anterior) {
		this.anterior = anterior;
	}
	public BigDecimal getActual() {
		return actual;
	}
	public void setActual(BigDecimal actual) {
		this.actual = actual;
	}
	public BigDecimal getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(BigDecimal bloqueado) {
		this.bloqueado = bloqueado;
	}
	public BigDecimal getNoDisponible() {
		return noDisponible;
	}
	public void setNoDisponible(BigDecimal noDisponible) {
		this.noDisponible = noDisponible;
	}
	
}
