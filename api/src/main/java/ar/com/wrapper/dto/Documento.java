package ar.com.wrapper.dto;

public class Documento {
	
	private String tipo;

	private String numero;
	
	private String codigoPaisOrigen;

	public String getPaisOrigen() {
		return codigoPaisOrigen;
	}

	public void setPaisOrigen(String paisOrigen) {
		this.codigoPaisOrigen = paisOrigen;
	}

	public Documento() {}

	public Documento(String tipo, String numero, String codigoPaisOrigen) {
		this.tipo = tipo;
		this.numero = numero;
		this.codigoPaisOrigen = codigoPaisOrigen;
	}


	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


}
