package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MovimientoTarjeta {
	
	private IdTarjeta idTarjeta;
	private Date fecha;
	private String descripcion;
	private String tipoTransaccionCodigo;
	private String tipoTansaccionDescripcion;
	private String moneda;
	private BigDecimal importe;
	public IdTarjeta getIdTarjeta() {
		return idTarjeta;
	}
	public void setIdTarjeta(IdTarjeta idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipoTransaccionCodigo() {
		return tipoTransaccionCodigo;
	}
	public void setTipoTransaccionCodigo(String tipoTransaccionCodigo) {
		this.tipoTransaccionCodigo = tipoTransaccionCodigo;
	}
	public String getTipoTansaccionDescripcion() {
		return tipoTansaccionDescripcion;
	}
	public void setTipoTansaccionDescripcion(String tipoTansaccionDescripcion) {
		this.tipoTansaccionDescripcion = tipoTansaccionDescripcion;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

}
