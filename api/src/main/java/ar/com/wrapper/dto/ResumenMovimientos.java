package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.List;

public class ResumenMovimientos {
	
	private Saldo saldo;
	private BigDecimal sobregiroFormal;
	private String subtotal;
	private List<Movimiento> movimientos;
	public Saldo getSaldo() {
		return saldo;
	}
	public void setSaldo(Saldo saldo) {
		this.saldo = saldo;
	}
	public BigDecimal getSobregiroFormal() {
		return sobregiroFormal;
	}
	public void setSobregiroFormal(BigDecimal sobregiroFormal) {
		this.sobregiroFormal = sobregiroFormal;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public List<Movimiento> getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(List<Movimiento> movimientos) {
		this.movimientos = movimientos;
	}
	
	
}
