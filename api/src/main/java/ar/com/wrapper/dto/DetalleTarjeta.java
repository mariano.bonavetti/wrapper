package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DetalleTarjeta {
	
	private String id;
	private String numeroTarjeta;
	private String tipoCodigo;
	private String tipoDescripcion;
	private String numeroCuenta;
	private String tipoCuenta;
	private String digitoVerifCuenta;
	private BigDecimal saldoActualPesos;
	private BigDecimal saldoActualDolares;
	private BigDecimal saldoAnteriorPesos;
	private BigDecimal saldoAnteriorDolares;
	private BigDecimal pagoMinimo;
	private Date fechaCierreAnterior;
	private Date fechaCierreActual;
	private Date fechaCierreProximo;
	private Date fechaVtoAnterior;
	private Date fechaVtoActual;
	private String vigenciaDesde;
	private String vigenciaHasta;
	private String formaPagoCodigo;
	private String formaPagoDescripcion;
	private String cuentaDebito;
	private String tipoCuentaDebitoCodigo;
	private String tipoCuentaDebitoDescripcion;
	private String esPreembozado;
	private String renovacionBonificada;
	private Tasa tasas;
	private BigDecimal offlineLimiteCompra;
	private BigDecimal offlineLimiteFinanciacion;
	private BigDecimal offlineLimitePrestamo;
	private BigDecimal offlineLimiteDisponible;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getTipoCodigo() {
		return tipoCodigo;
	}
	public void setTipoCodigo(String tipoCodigo) {
		this.tipoCodigo = tipoCodigo;
	}
	public String getTipoDescripcion() {
		return tipoDescripcion;
	}
	public void setTipoDescripcion(String tipoDescripcion) {
		this.tipoDescripcion = tipoDescripcion;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getDigitoVerifCuenta() {
		return digitoVerifCuenta;
	}
	public void setDigitoVerifCuenta(String digitoVerifCuenta) {
		this.digitoVerifCuenta = digitoVerifCuenta;
	}
	public BigDecimal getSaldoActualPesos() {
		return saldoActualPesos;
	}
	public void setSaldoActualPesos(BigDecimal saldoActualPesos) {
		this.saldoActualPesos = saldoActualPesos;
	}
	public BigDecimal getSaldoActualDolares() {
		return saldoActualDolares;
	}
	public void setSaldoActualDolares(BigDecimal saldoActualDolares) {
		this.saldoActualDolares = saldoActualDolares;
	}
	public BigDecimal getSaldoAnteriorPesos() {
		return saldoAnteriorPesos;
	}
	public void setSaldoAnteriorPesos(BigDecimal saldoAnteriorPesos) {
		this.saldoAnteriorPesos = saldoAnteriorPesos;
	}
	public BigDecimal getSaldoAnteriorDolares() {
		return saldoAnteriorDolares;
	}
	public void setSaldoAnteriorDolares(BigDecimal saldoAnteriorDolares) {
		this.saldoAnteriorDolares = saldoAnteriorDolares;
	}
	public BigDecimal getPagoMinimo() {
		return pagoMinimo;
	}
	public void setPagoMinimo(BigDecimal pagoMinimo) {
		this.pagoMinimo = pagoMinimo;
	}
	public Date getFechaCierreAnterior() {
		return fechaCierreAnterior;
	}
	public void setFechaCierreAnterior(Date fechaCierreAnterior) {
		this.fechaCierreAnterior = fechaCierreAnterior;
	}
	public Date getFechaCierreActual() {
		return fechaCierreActual;
	}
	public void setFechaCierreActual(Date fechaCierreActual) {
		this.fechaCierreActual = fechaCierreActual;
	}
	public Date getFechaCierreProximo() {
		return fechaCierreProximo;
	}
	public void setFechaCierreProximo(Date fechaCierreProximo) {
		this.fechaCierreProximo = fechaCierreProximo;
	}
	public Date getFechaVtoAnterior() {
		return fechaVtoAnterior;
	}
	public void setFechaVtoAnterior(Date fechaVtoAnterior) {
		this.fechaVtoAnterior = fechaVtoAnterior;
	}
	public Date getFechaVtoActual() {
		return fechaVtoActual;
	}
	public void setFechaVtoActual(Date fechaVtoActual) {
		this.fechaVtoActual = fechaVtoActual;
	}
	public String getVigenciaDesde() {
		return vigenciaDesde;
	}
	public void setVigenciaDesde(String vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}
	public String getVigenciaHasta() {
		return vigenciaHasta;
	}
	public void setVigenciaHasta(String vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}
	public String getFormaPagoCodigo() {
		return formaPagoCodigo;
	}
	public void setFormaPagoCodigo(String formaPagoCodigo) {
		this.formaPagoCodigo = formaPagoCodigo;
	}
	public String getFormaPagoDescripcion() {
		return formaPagoDescripcion;
	}
	public void setFormaPagoDescripcion(String formaPagoDescripcion) {
		this.formaPagoDescripcion = formaPagoDescripcion;
	}
	public String getCuentaDebito() {
		return cuentaDebito;
	}
	public void setCuentaDebito(String cuentaDebito) {
		this.cuentaDebito = cuentaDebito;
	}
	public String getTipoCuentaDebitoCodigo() {
		return tipoCuentaDebitoCodigo;
	}
	public void setTipoCuentaDebitoCodigo(String tipoCuentaDebitoCodigo) {
		this.tipoCuentaDebitoCodigo = tipoCuentaDebitoCodigo;
	}
	public String getTipoCuentaDebitoDescripcion() {
		return tipoCuentaDebitoDescripcion;
	}
	public void setTipoCuentaDebitoDescripcion(String tipoCuentaDebitoDescripcion) {
		this.tipoCuentaDebitoDescripcion = tipoCuentaDebitoDescripcion;
	}
	public String getEsPreembozado() {
		return esPreembozado;
	}
	public void setEsPreembozado(String esPreembozado) {
		this.esPreembozado = esPreembozado;
	}
	public String getRenovacionBonificada() {
		return renovacionBonificada;
	}
	public void setRenovacionBonificada(String renovacionBonificada) {
		this.renovacionBonificada = renovacionBonificada;
	}
	public Tasa getTasas() {
		return tasas;
	}
	public void setTasas(Tasa tasas) {
		this.tasas = tasas;
	}
	public BigDecimal getOfflineLimiteCompra() {
		return offlineLimiteCompra;
	}
	public void setOfflineLimiteCompra(BigDecimal offlineLimiteCompra) {
		this.offlineLimiteCompra = offlineLimiteCompra;
	}
	public BigDecimal getOfflineLimiteFinanciacion() {
		return offlineLimiteFinanciacion;
	}
	public void setOfflineLimiteFinanciacion(BigDecimal offlineLimiteFinanciacion) {
		this.offlineLimiteFinanciacion = offlineLimiteFinanciacion;
	}
	public BigDecimal getOfflineLimitePrestamo() {
		return offlineLimitePrestamo;
	}
	public void setOfflineLimitePrestamo(BigDecimal offlineLimitePrestamo) {
		this.offlineLimitePrestamo = offlineLimitePrestamo;
	}
	public BigDecimal getOfflineLimiteDisponible() {
		return offlineLimiteDisponible;
	}
	public void setOfflineLimiteDisponible(BigDecimal offlineLimiteDisponible) {
		this.offlineLimiteDisponible = offlineLimiteDisponible;
	}
	
}
