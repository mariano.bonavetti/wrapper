package ar.com.wrapper.dto;

public class IdCuentaBANTOTAL extends IdCuenta  {
	
	public IdCuentaBANTOTAL() {
		
	}
	
	public IdCuentaBANTOTAL(String id) {
        this.empresa=id.substring(0,3);
        this.sucursal = id.substring(3,6);
        this.moneda = id.substring(6,10);
        this.papel = id.substring(10,14);
        this.cuenta = id.substring(14,23);
        this.operacion = id.substring(23,32);
        this.subCuenta = id.substring(32,35);
        this.tipoOpe = id.substring(35,38);
        this.modulo = id.substring(38,41);
		this.id = id;
		// TODO Auto-generated constructor stub
	}
	

	private String empresa;

    private String sucursal;

    private String modulo;

    private String moneda;

    private String papel;

    private String cuenta;

    private String operacion;

    private String subCuenta;

    private String tipoOpe;
    
    private String id;

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getPapel() {
		return papel;
	}

	public void setPapel(String papel) {
		this.papel = papel;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getSubCuenta() {
		return subCuenta;
	}

	public void setSubCuenta(String subCuenta) {
		this.subCuenta = subCuenta;
	}

	public String getTipoOpe() {
		return tipoOpe;
	}

	public void setTipoOpe(String tipoOpe) {
		this.tipoOpe = tipoOpe;
	}	
	
	@Override
	public boolean equals(Object o){
		if(o == null)
			return false;
		if(this == o)
			return true;
		if (!this.empresa.equals(((IdCuentaBANTOTAL) o).getEmpresa()))
			return false;
		if (!this.sucursal.equals(((IdCuentaBANTOTAL) o).getSucursal()))
			return false;
		if (!this.modulo.equals(((IdCuentaBANTOTAL) o).getModulo()))
			return false;
		if (!this.moneda.equals(((IdCuentaBANTOTAL) o).getMoneda()))
			return false;
		if (!this.papel.equals(((IdCuentaBANTOTAL) o).getPapel()))
			return false;
		if (!this.cuenta.equals(((IdCuentaBANTOTAL) o).getCuenta()))
			return false;
		if (!this.operacion.equals(((IdCuentaBANTOTAL) o).getOperacion()))
			return false;
		if (!this.subCuenta.equals(((IdCuentaBANTOTAL) o).getSubCuenta()))
			return false;
		if (!this.tipoOpe.equals(((IdCuentaBANTOTAL) o).getTipoOpe()))
			return false;
		return true;
		
	}

	public String getId() {
		return padLeftZeros(this.empresa,3)+
			   padLeftZeros(this.sucursal,3)+
			   padLeftZeros(this.moneda,4)+
			   padLeftZeros(this.papel,4)+
			   padLeftZeros(this.cuenta,9)+
			   padLeftZeros(this.operacion,9)+
			   padLeftZeros(this.subCuenta,3)+
			   padLeftZeros(this.tipoOpe,3)+
			   padLeftZeros(this.modulo,3);
	}

	public void setId(String id) {
		this.id = id;
	}
	

	private String padLeftZeros(String inputString, int length) {
	    if (inputString.length() >= length) {
	        return inputString;
	    }
	    StringBuilder sb = new StringBuilder();
	    while (sb.length() < length - inputString.length()) {
	        sb.append('0');
	    }
	    sb.append(inputString);
	 
	    return sb.toString();
	}
	
	
	
}
