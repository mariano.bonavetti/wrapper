package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Movimiento {
	
	private Date fecha;
	private String tipoMovimientoCodigo;
	private String tipoMovimientoDescripcion;
	private Integer sucursalCodigo;
	private String sucursalDescripcion;
	private String codigoConcepto;
	private String debitoOCredito;
	private BigDecimal importe;
	private Integer saldo;
	private List<Renglon> renglones;

	
	public String getTipoMovimientoCodigo() {
		return tipoMovimientoCodigo;
	}
	public void setTipoMovimientoCodigo(String tipoMovimientoCodigo) {
		this.tipoMovimientoCodigo = tipoMovimientoCodigo;
	}
	public String getTipoMovimientoDescripcion() {
		return tipoMovimientoDescripcion;
	}
	public void setTipoMovimientoDescripcion(String tipoMovimientoDescripcion) {
		this.tipoMovimientoDescripcion = tipoMovimientoDescripcion;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getSucursalCodigo() {
		return sucursalCodigo;
	}
	public void setSucursalCodigo(Integer sucursalCodigo) {
		this.sucursalCodigo = sucursalCodigo;
	}
	public String getSucursalDescripcion() {
		return sucursalDescripcion;
	}
	public void setSucursalDescripcion(String sucursalDescripcion) {
		this.sucursalDescripcion = sucursalDescripcion;
	}
	public String getCodigoConcepto() {
		return codigoConcepto;
	}
	public void setCodigoConcepto(String codigoConcepto) {
		this.codigoConcepto = codigoConcepto;
	}
	public String getDebitoOCredito() {
		return debitoOCredito;
	}
	public void setDebitoOCredito(String debitoOCredito) {
		this.debitoOCredito = debitoOCredito;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public Integer getSaldo() {
		return saldo;
	}
	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
	public List<Renglon> getRenglones() {
		return renglones;
	}
	public void setRenglones(List<Renglon> renglones) {
		this.renglones = renglones;
	}

	

}
