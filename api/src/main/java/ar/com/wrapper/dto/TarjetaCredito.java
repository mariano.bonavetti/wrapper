package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TarjetaCredito {
	
	private IdTarjeta id;
	private String idCliente;
	private String marcaCodigo;
	private String marcaDescripcion;
	private String cuentaTarjeta;
	private String denominacion;
	private String tipoTarjetacodigo;
	private String tipoTarjetaDescripcion;
	private String sigla;
	private String titularidadCodigo;
	private String titularidadDescripcion;
	private String carteraCodigo;
	private String carteraDescripcion;
	private Date vigencia;
	private Date fechaProcesamiento;
	private Date fecha;
	private String entidadCodigo;
	private String entidadDescripcion;
	private String bancoCodigo;
	private String bancoDescripcion;
	private BigDecimal saldoPesos;
	private BigDecimal saldoDolares;
	private Date cierreFecha;
	private String estadoCodigo;
	private String estadoDescripcion;
	private String marcaInhabilitacionCodigo;
	private String marcaInhabilitacionDescripcion;
	private BigDecimal limiteCompra;
	private String formaPagoCodigo;
	private String formaPagoDescripcion;
	private String cuentaNroBT;
	private String productoCodigo;
	private String productoDescripcion;
	private String gafCodigo;
	private String gafDescripcion;
	private String prodSigla;
	private String tipoCtaCodigo;
	private String tipoCtaDescripcion;
	private Date fecha_cierre;
	private Date fecha_vencimiento;
	private BigDecimal pago_minimo;
	
	public Date getFecha_cierre() {
		return fecha_cierre;
	}
	public void setFecha_cierre(Date fecha_cierre) {
		this.fecha_cierre = fecha_cierre;
	}
	public Date getFecha_vencimiento() {
		return fecha_vencimiento;
	}
	public void setFecha_vencimiento(Date fecha_vencimiento) {
		this.fecha_vencimiento = fecha_vencimiento;
	}
	public BigDecimal getPago_minimo() {
		return pago_minimo;
	}
	public void setPago_minimo(BigDecimal pago_minimo) {
		this.pago_minimo = pago_minimo;
	}
	public IdTarjeta getId() {
		return id;
	}
	public void setId(IdTarjeta id) {
		this.id = id;
	}
	public String getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	public String getMarcaCodigo() {
		return marcaCodigo;
	}
	public void setMarcaCodigo(String marcaCodigo) {
		this.marcaCodigo = marcaCodigo;
	}
	public String getMarcaDescripcion() {
		return marcaDescripcion;
	}
	public void setMarcaDescripcion(String marcaDescripcion) {
		this.marcaDescripcion = marcaDescripcion;
	}
	public String getCuentaTarjeta() {
		return cuentaTarjeta;
	}
	public void setCuentaTarjeta(String cuentaTarjeta) {
		this.cuentaTarjeta = cuentaTarjeta;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getTipoTarjetacodigo() {
		return tipoTarjetacodigo;
	}
	public void setTipoTarjetacodigo(String tipoTarjetacodigo) {
		this.tipoTarjetacodigo = tipoTarjetacodigo;
	}
	public String getTipoTarjetaDescripcion() {
		return tipoTarjetaDescripcion;
	}
	public void setTipoTarjetaDescripcion(String tipoTarjetaDescripcion) {
		this.tipoTarjetaDescripcion = tipoTarjetaDescripcion;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getTitularidadCodigo() {
		return titularidadCodigo;
	}
	public void setTitularidadCodigo(String titularidadCodigo) {
		this.titularidadCodigo = titularidadCodigo;
	}
	public String getTitularidadDescripcion() {
		return titularidadDescripcion;
	}
	public void setTitularidadDescripcion(String titularidadDescripcion) {
		this.titularidadDescripcion = titularidadDescripcion;
	}
	public String getCarteraCodigo() {
		return carteraCodigo;
	}
	public void setCarteraCodigo(String carteraCodigo) {
		this.carteraCodigo = carteraCodigo;
	}
	public String getCarteraDescripcion() {
		return carteraDescripcion;
	}
	public void setCarteraDescripcion(String carteraDescripcion) {
		this.carteraDescripcion = carteraDescripcion;
	}
	public Date getVigencia() {
		return vigencia;
	}
	public void setVigencia(Date vigencia) {
		this.vigencia = vigencia;
	}
	public Date getFechaProcesamiento() {
		return fechaProcesamiento;
	}
	public void setFechaProcesamiento(Date fechaProcesamiento) {
		this.fechaProcesamiento = fechaProcesamiento;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getEntidadCodigo() {
		return entidadCodigo;
	}
	public void setEntidadCodigo(String entidadCodigo) {
		this.entidadCodigo = entidadCodigo;
	}
	public String getEntidadDescripcion() {
		return entidadDescripcion;
	}
	public void setEntidadDescripcion(String entidadDescripcion) {
		this.entidadDescripcion = entidadDescripcion;
	}
	public String getBancoCodigo() {
		return bancoCodigo;
	}
	public void setBancoCodigo(String bancoCodigo) {
		this.bancoCodigo = bancoCodigo;
	}
	public String getBancoDescripcion() {
		return bancoDescripcion;
	}
	public void setBancoDescripcion(String bancoDescripcion) {
		this.bancoDescripcion = bancoDescripcion;
	}
	public BigDecimal getSaldoPesos() {
		return saldoPesos;
	}
	public void setSaldoPesos(BigDecimal saldoPesos) {
		this.saldoPesos = saldoPesos;
	}
	public BigDecimal getSaldoDolares() {
		return saldoDolares;
	}
	public void setSaldoDolares(BigDecimal saldoDolares) {
		this.saldoDolares = saldoDolares;
	}
	public Date getCierreFecha() {
		return cierreFecha;
	}
	public void setCierreFecha(Date cierreFecha) {
		this.cierreFecha = cierreFecha;
	}
	public String getEstadoCodigo() {
		return estadoCodigo;
	}
	public void setEstadoCodigo(String estadoCodigo) {
		this.estadoCodigo = estadoCodigo;
	}
	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}
	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}
	public String getMarcaInhabilitacionCodigo() {
		return marcaInhabilitacionCodigo;
	}
	public void setMarcaInhabilitacionCodigo(String marcaInhabilitacionCodigo) {
		this.marcaInhabilitacionCodigo = marcaInhabilitacionCodigo;
	}
	public String getMarcaInhabilitacionDescripcion() {
		return marcaInhabilitacionDescripcion;
	}
	public void setMarcaInhabilitacionDescripcion(String marcaInhabilitacionDescripcion) {
		this.marcaInhabilitacionDescripcion = marcaInhabilitacionDescripcion;
	}
	public BigDecimal getLimiteCompra() {
		return limiteCompra;
	}
	public void setLimiteCompra(BigDecimal limiteCompra) {
		this.limiteCompra = limiteCompra;
	}
	public String getFormaPagoCodigo() {
		return formaPagoCodigo;
	}
	public void setFormaPagoCodigo(String formaPagoCodigo) {
		this.formaPagoCodigo = formaPagoCodigo;
	}
	public String getFormaPagoDescripcion() {
		return formaPagoDescripcion;
	}
	public void setFormaPagoDescripcion(String formaPagoDescripcion) {
		this.formaPagoDescripcion = formaPagoDescripcion;
	}
	public String getCuentaNroBT() {
		return cuentaNroBT;
	}
	public void setCuentaNroBT(String cuentaNroBT) {
		this.cuentaNroBT = cuentaNroBT;
	}
	public String getProductoCodigo() {
		return productoCodigo;
	}
	public void setProductoCodigo(String productoCodigo) {
		this.productoCodigo = productoCodigo;
	}
	public String getProductoDescripcion() {
		return productoDescripcion;
	}
	public void setProductoDescripcion(String productoDescripcion) {
		this.productoDescripcion = productoDescripcion;
	}
	public String getGafCodigo() {
		return gafCodigo;
	}
	public void setGafCodigo(String gafCodigo) {
		this.gafCodigo = gafCodigo;
	}
	public String getGafDescripcion() {
		return gafDescripcion;
	}
	public void setGafDescripcion(String gafDescripcion) {
		this.gafDescripcion = gafDescripcion;
	}
	public String getProdSigla() {
		return prodSigla;
	}
	public void setProdSigla(String prodSigla) {
		this.prodSigla = prodSigla;
	}
	public String getTipoCtaCodigo() {
		return tipoCtaCodigo;
	}
	public void setTipoCtaCodigo(String tipoCtaCodigo) {
		this.tipoCtaCodigo = tipoCtaCodigo;
	}
	public String getTipoCtaDescripcion() {
		return tipoCtaDescripcion;
	}
	public void setTipoCtaDescripcion(String tipoCtaDescripcion) {
		this.tipoCtaDescripcion = tipoCtaDescripcion;
	}
	
	
	
	
	
}
