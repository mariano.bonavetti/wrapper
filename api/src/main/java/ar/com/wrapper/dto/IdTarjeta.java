package ar.com.wrapper.dto;

public class IdTarjeta {
	
	private String DataTypeName;
	private String numero;
	private String id;
	
	public IdTarjeta() {
		// TODO Auto-generated constructor stub
	}
	
	public IdTarjeta(String id) {
		this.numero = id.substring(0, 16);
		this.DataTypeName = id.substring(16, id.length()-1);
	}
	
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getDataTypeName() {
		return DataTypeName;
	}
	public void setDataTypeName(String dataTypeName) {
		DataTypeName = dataTypeName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
