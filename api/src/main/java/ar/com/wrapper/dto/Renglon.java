package ar.com.wrapper.dto;

public class Renglon {

	public Renglon(String cod, String desc) {
		this.codigo=cod;
		this.descripcion=desc;
		// TODO Auto-generated constructor stub
	}
	
	private String codigo;
	private String descripcion;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	} 
	
	
}
