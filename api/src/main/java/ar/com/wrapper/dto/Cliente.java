package ar.com.wrapper.dto;

import java.util.Date;

public class Cliente {

	private IdClienteTipo idCliente;
	private String apellido;
	private String nombre;
	private String sexoCodigo;
	private String sexoDescripcion;
	private Boolean esEmpleado;
	private String estadoCivilCodigo;
	private String estadoCivilDescripcion;
	private Date fechaNacimiento;
	private Integer nacionalidadCodigo;
	private String nacionalidadDescripcion;
	public IdClienteTipo getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(IdClienteTipo idCliente) {
		this.idCliente = idCliente;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSexoCodigo() {
		return sexoCodigo;
	}
	public void setSexoCodigo(String sexoCodigo) {
		this.sexoCodigo = sexoCodigo;
	}
	public String getSexoDescripcion() {
		return sexoDescripcion;
	}
	public void setSexoDescripcion(String sexoDescripcion) {
		this.sexoDescripcion = sexoDescripcion;
	}
	public Boolean getEsEmpleado() {
		return esEmpleado;
	}
	public void setEsEmpleado(Boolean esEmpleado) {
		this.esEmpleado = esEmpleado;
	}
	public String getEstadoCivilCodigo() {
		return estadoCivilCodigo;
	}
	public void setEstadoCivilCodigo(String estadoCivilCodigo) {
		this.estadoCivilCodigo = estadoCivilCodigo;
	}
	public String getEstadoCivilDescripcion() {
		return estadoCivilDescripcion;
	}
	public void setEstadoCivilDescripcion(String estadoCivilDescripcion) {
		this.estadoCivilDescripcion = estadoCivilDescripcion;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public Integer getNacionalidadCodigo() {
		return nacionalidadCodigo;
	}
	public void setNacionalidadCodigo(Integer nacionalidadCodigo) {
		this.nacionalidadCodigo = nacionalidadCodigo;
	}
	public String getNacionalidadDescripcion() {
		return nacionalidadDescripcion;
	}
	public void setNacionalidadDescripcion(String nacionalidadDescripcion) {
		this.nacionalidadDescripcion = nacionalidadDescripcion;
	}
	
	
}
