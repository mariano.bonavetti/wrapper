package ar.com.wrapper.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ClubPuntos {
	
	private Integer resultado;
	private Integer programaCodigo;
	private String programaDescripcion;
	private String titular;
	private String idEstadoCuenta;
	private String estadoCuenta;
	private Date fechaAlta;
	private Date fechaBaja;
	private Integer puntosDisponibles;
	private Integer puntosAVencerEsteMes;
	private Integer puntosAVencerProximoMes;
	private Integer puntosAVencer2Meses;
	private Integer puntosAVencer3Meses;
	
	public Integer getPuntosDisponibles() {
		return puntosDisponibles;
	}
	public void setPuntosDisponibles(Integer puntosDisponibles) {
		this.puntosDisponibles = puntosDisponibles;
	}
	public Integer getPuntosAVencerEsteMes() {
		return puntosAVencerEsteMes;
	}
	public void setPuntosAVencerEsteMes(Integer puntosAVencerEsteMes) {
		this.puntosAVencerEsteMes = puntosAVencerEsteMes;
	}
	public Integer getPuntosAVencerProximoMes() {
		return puntosAVencerProximoMes;
	}
	public void setPuntosAVencerProximoMes(Integer puntosAVencerProximoMes) {
		this.puntosAVencerProximoMes = puntosAVencerProximoMes;
	}
	public Integer getPuntosAVencer2Meses() {
		return puntosAVencer2Meses;
	}
	public void setPuntosAVencer2Meses(Integer puntosAVencer2Meses) {
		this.puntosAVencer2Meses = puntosAVencer2Meses;
	}
	public Integer getPuntosAVencer3Meses() {
		return puntosAVencer3Meses;
	}
	public void setPuntosAVencer3Meses(Integer puntosAVencer3Meses) {
		this.puntosAVencer3Meses = puntosAVencer3Meses;
	}
	public Integer getResultado() {
		return resultado;
	}
	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}
	public Integer getProgramaCodigo() {
		return programaCodigo;
	}
	public void setProgramaCodigo(Integer programaCodigo) {
		this.programaCodigo = programaCodigo;
	}
	public String getProgramaDescripcion() {
		return programaDescripcion;
	}
	public void setProgramaDescripcion(String programaDescripcion) {
		this.programaDescripcion = programaDescripcion;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getIdEstadoCuenta() {
		return idEstadoCuenta;
	}
	public void setIdEstadoCuenta(String idEstadoCuenta) {
		this.idEstadoCuenta = idEstadoCuenta;
	}
	public String getEstadoCuenta() {
		return estadoCuenta;
	}
	public void setEstadoCuenta(String estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
	

}
