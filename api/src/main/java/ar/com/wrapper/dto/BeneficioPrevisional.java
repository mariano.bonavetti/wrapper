package ar.com.wrapper.dto;

import java.util.Date;

public class BeneficioPrevisional {
	
	private IdLiquidacionAnses id;
	private String nombreBeneficiario;
	private IdClienteTipo cliente;
	private String condicion;
	private Date fechaPago;
	private Date fechaAdelanto;
	private String estadoCodigo;
	private String estadoDescripcion;
	private Date fechaSigPago;
	private String grupoPagoCodigo;
	private String grupoPagoDescripcion;
	public IdLiquidacionAnses getId() {
		return id;
	}
	public void setId(IdLiquidacionAnses id) {
		this.id = id;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public IdClienteTipo getCliente() {
		return cliente;
	}
	public void setCliente(IdClienteTipo cliente) {
		this.cliente = cliente;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public Date getFechaAdelanto() {
		return fechaAdelanto;
	}
	public void setFechaAdelanto(Date fechaAdelanto) {
		this.fechaAdelanto = fechaAdelanto;
	}
	public String getEstadoCodigo() {
		return estadoCodigo;
	}
	public void setEstadoCodigo(String estadoCodigo) {
		this.estadoCodigo = estadoCodigo;
	}
	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}
	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}
	public Date getFechaSigPago() {
		return fechaSigPago;
	}
	public void setFechaSigPago(Date fechaSigPago) {
		this.fechaSigPago = fechaSigPago;
	}
	public String getGrupoPagoCodigo() {
		return grupoPagoCodigo;
	}
	public void setGrupoPagoCodigo(String grupoPagoCodigo) {
		this.grupoPagoCodigo = grupoPagoCodigo;
	}
	public String getGrupoPagoDescripcion() {
		return grupoPagoDescripcion;
	}
	public void setGrupoPagoDescripcion(String grupoPagoDescripcion) {
		this.grupoPagoDescripcion = grupoPagoDescripcion;
	}
	
	

}
