package ar.com.wrapper.dto;

import java.math.BigDecimal;

public class SaldoCompleto {
	
	private IdCuentaBANTOTAL cuenta;
	private BigDecimal sobregiroFormal;
	private BigDecimal saldoBloqueado;
	private BigDecimal saldoContable;
	private BigDecimal saldoDisponible;
	private BigDecimal acuerdo;
	public IdCuentaBANTOTAL getCuenta() {
		return cuenta;
	}
	public void setCuenta(IdCuentaBANTOTAL cuenta) {
		this.cuenta = cuenta;
	}
	public BigDecimal getSobregiroFormal() {
		return sobregiroFormal;
	}
	public void setSobregiroFormal(BigDecimal sobregiroFormal) {
		this.sobregiroFormal = sobregiroFormal;
	}
	public BigDecimal getSaldoBloqueado() {
		return saldoBloqueado;
	}
	public void setSaldoBloqueado(BigDecimal saldoBloqueado) {
		this.saldoBloqueado = saldoBloqueado;
	}
	public BigDecimal getSaldoContable() {
		return saldoContable;
	}
	public void setSaldoContable(BigDecimal saldoContable) {
		this.saldoContable = saldoContable;
	}
	public BigDecimal getSaldoDisponible() {
		return saldoDisponible;
	}
	public void setSaldoDisponible(BigDecimal saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}
	public BigDecimal getAcuerdo() {
		return acuerdo;
	}
	public void setAcuerdo(BigDecimal acuerdo) {
		this.acuerdo = acuerdo;
	}
}
