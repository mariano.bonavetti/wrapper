package ar.com.wrapper.dto;

public class Tasa {

	private Object efectivaMensualPesos;
	private Object efectivaMensualDolares;
	private Object nominalAnualPesos;
	private Object nominalAnualDolares;
	public Object getEfectivaMensualPesos() {
		return efectivaMensualPesos;
	}
	public void setEfectivaMensualPesos(Object efectivaMensualPesos) {
		this.efectivaMensualPesos = efectivaMensualPesos;
	}
	public Object getEfectivaMensualDolares() {
		return efectivaMensualDolares;
	}
	public void setEfectivaMensualDolares(Object efectivaMensualDolares) {
		this.efectivaMensualDolares = efectivaMensualDolares;
	}
	public Object getNominalAnualPesos() {
		return nominalAnualPesos;
	}
	public void setNominalAnualPesos(Object nominalAnualPesos) {
		this.nominalAnualPesos = nominalAnualPesos;
	}
	public Object getNominalAnualDolares() {
		return nominalAnualDolares;
	}
	public void setNominalAnualDolares(Object nominalAnualDolares) {
		this.nominalAnualDolares = nominalAnualDolares;
	}
	
	
}
