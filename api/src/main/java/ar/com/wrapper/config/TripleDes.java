package ar.com.wrapper.config;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TripleDes {

	private static final String ENCRIPTAR = "E";

	private static final String DESENCRIPTAR = "D";

	private static final String SHARED_KEY = "D2EE18969544A012501E2253F5D8C0A98E05FF0AEBA3FFCA";

	private final static Log log = LogFactory.getLog(TripleDes.class);
	/**
	 * Encripta una palabra utilizando el algoritmo DESede.
	 * 
	 * @throws Exception
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		try{
		String palabra = args[0];
		String codigo = args[1];
		
		
		if (codigo.equals(ENCRIPTAR)) {
			log.info("Se encriptará la palabra: " + palabra);
			log.info("Resultado: " + encriptar(palabra));
		} else if (codigo.equals(DESENCRIPTAR)) {
			log.info("Se desencriptará la palabra: " + palabra);
			log.info("Resultado: " + desencriptar(palabra));
		}
		else
			log.info("Parametros: palabra codigo[E,D]");

		}catch(ArrayIndexOutOfBoundsException e){
			log.info("Parametros: palabra codigo[E,D]");
		}
	}

	public static String encriptar(String palabra) throws Exception {
		String algorithm = "DESede";
		String transformation = "DESede/CBC/PKCS5Padding";

		byte[] keyValue = Hex.decodeHex(SHARED_KEY.toCharArray());
		
		DESedeKeySpec keySpec = new DESedeKeySpec(keyValue);
		
		IvParameterSpec iv = new IvParameterSpec(new byte[8]);

		SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
		
		Cipher encrypter = Cipher.getInstance(transformation);
		encrypter.init(Cipher.ENCRYPT_MODE, key, iv);

		byte[] input = palabra.getBytes("UTF-8");
		// Encripto, obtengo arreglo de bytes
		
		byte[] encrypted = encrypter.doFinal(input);
		// Lo paso a string
		String encriptacion = String.valueOf(Hex.encodeHex(encrypted));
		return encriptacion;

	}

	public static String desencriptar(String palabra) throws Exception {

		String algorithm = "DESede";
		String transformation = "DESede/CBC/PKCS5Padding";

		byte[] keyValue = Hex.decodeHex(SHARED_KEY.toCharArray());
		// Construyo el desencriptador
		Cipher decrypter = Cipher.getInstance(transformation);
		
		IvParameterSpec iv = new IvParameterSpec(new byte[8]);
		DESedeKeySpec keySpec = new DESedeKeySpec(keyValue);
		SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
		decrypter.init(Cipher.DECRYPT_MODE, key, iv);

		// Desencripto pasando a byte[] el string encriptado
		byte[] decrypted = decrypter.doFinal(Hex.decodeHex(palabra.toCharArray()));
		// Obtengo el string desencriptado
		String desencriptado = new String(decrypted, "UTF-8");
		return desencriptado;

	}
}