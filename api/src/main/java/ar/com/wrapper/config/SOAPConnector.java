package ar.com.wrapper.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ar.com.supervielle.wsdl.servicios.integracion.beneficiossupervielle_v1.BeneficiosSuperviellePortTypeV11;
import ar.com.supervielle.wsdl.servicios.integracion.cliente_v1.ClientePortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.clienteprevisional_v1.ClientePrevisionalPortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.cajaahorro_v1.CajadeAhorroPortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.cajaahorro_v1.CajadeAhorroPortTypeV11;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.cajaahorro_v1.CajadeAhorroPortTypeV12;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.ctacte_v1.CuentaCorrientePortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.cuenta.ctacte_v1.CuentaCorrientePortTypeV12;
import ar.com.supervielle.wsdl.servicios.integracion.tarjetacredito_v1.TarjetaCreditoPortTypeV1;
import ar.com.supervielle.wsdl.servicios.integracion.tarjetacredito_v1.TarjetaCreditoPortTypeV12;

@Configuration
public class SOAPConnector {
	
	private final static Logger log = LogManager.getLogger(SOAPConnector.class);
	
    @Value("${url.soa.cuenta-ca-v12}")
    private String urlSoaCuentaCAv12;
    @Value("${url.soa.cuenta-ca-v11}")
    private String urlSoaCuentaCAv11;
    @Value("${url.soa.cuenta-ca-v1}")
    private String urlSoaCuentaCAv1; 
    @Value("${url.soa.cuenta-cc-v1}")
    private String urlSoaCuentaCCv1;
    @Value("${url.soa.cuenta-cc-v12}")
    private String urlSoaCuentaCCv12;
    @Value("${url.soa.cliente-v1}")
    private String urlSoaClientev1;
    @Value("${url.soa.previsional-v1}")
    private String urlSoaPrevisionalv1;
    @Value("${url.soa.tarjeta-v1}")
    private String urlSoaTarjetav1;
    @Value("${url.soa.tarjeta-v12}")
    private String urlSoaTarjetav12;
    @Value("${url.soa.beneficios-v11}")
    private String urlSoaBeneficiosv11;
    @Value("${soa.usr}")
    private String soaUsr;
    @Value("${soa.password}")
    private String soaPassword;
	
    @Bean(name = "ws4jSecurityPasswordCallback")
    public WSSecurityPasswordCallbackHandler WSSecurityPassword() {
    	WSSecurityPasswordCallbackHandler wss = new WSSecurityPasswordCallbackHandler();
        Properties properties = new Properties();
        // add more properties in the same way
        properties.put("mail.transport.protocol", "smtp");
        try {
			wss.setPassword(TripleDes.desencriptar(soaPassword));
		} catch (Exception e) {
			log.error("Error al desencriptar el password de SOA", e);
		}
        
        return wss;
    }
    
   @Bean(name = "ws4jSecurity")
    public WSS4JOutInterceptor getWSS4JOutInterceptor() {
	   
        Map<String, Object> outProps = new HashMap<String, Object>();
        outProps.put("action", "UsernameToken");
        outProps.put("passwordType", "PasswordText");
        outProps.put("addUTElements", "Nonce Created");        
        outProps.put("passwordCallbackRef", WSSecurityPassword());
        try {
			outProps.put("user", TripleDes.desencriptar(soaUsr));
		} catch (Exception e) {
			log.error("Error al encriptar el usuario de SOA", e);
		}
      
        WSS4JOutInterceptor wss4j = new WSS4JOutInterceptor(outProps);
        return wss4j;
    }
	
    public JaxWsProxyFactoryBean addInterceptors(JaxWsProxyFactoryBean jaxWsProxyFactoryBean) {
        // add the WSS4J OUT interceptor to sign the request message
        jaxWsProxyFactoryBean.getOutInterceptors().add(getWSS4JOutInterceptor());
    	return jaxWsProxyFactoryBean;
    }
	
    @Bean(name = "soaCajadeAhorroV12")
    public CajadeAhorroPortTypeV12 soaCajadeAhorroV12() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(CajadeAhorroPortTypeV12.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaCuentaCAv12);
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);
      
      return (CajadeAhorroPortTypeV12) jaxWsProxyFactoryBean.create();
    }   
    
    @Bean(name = "soaCajadeAhorroV11")
    public CajadeAhorroPortTypeV11 soaCajadeAhorroV11() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(CajadeAhorroPortTypeV11.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaCuentaCAv11);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (CajadeAhorroPortTypeV11) jaxWsProxyFactoryBean.create();
    }   
    
    @Bean(name = "soaCajadeAhorroV1")
    public CajadeAhorroPortTypeV1 soaCajadeAhorroV1() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(CajadeAhorroPortTypeV1.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaCuentaCAv1);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (CajadeAhorroPortTypeV1) jaxWsProxyFactoryBean.create();
    }   
    
    @Bean(name = "soaCuentaCorrienteV1")
    public CuentaCorrientePortTypeV1 soaCuentaCorrienteV1() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(CuentaCorrientePortTypeV1.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaCuentaCCv1);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (CuentaCorrientePortTypeV1) jaxWsProxyFactoryBean.create();
    }   
    
    
    @Bean(name = "soaCuentaCorrienteV12")
    public CuentaCorrientePortTypeV12 soaCuentaCorrienteV12() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(CuentaCorrientePortTypeV12.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaCuentaCCv12);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (CuentaCorrientePortTypeV12) jaxWsProxyFactoryBean.create();
    }   
    
    @Bean(name = "soaClienteV1")
    public ClientePortTypeV1 soaClienteV1() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(ClientePortTypeV1.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaClientev1);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (ClientePortTypeV1) jaxWsProxyFactoryBean.create();
    }   
    
    
    @Bean(name = "soaPrevisionalV1")
    public ClientePrevisionalPortTypeV1 soaPrevisionalV1() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(ClientePrevisionalPortTypeV1.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaPrevisionalv1);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (ClientePrevisionalPortTypeV1) jaxWsProxyFactoryBean.create();
    }   
    
    @Bean(name = "soaTarjetaCreditoV1")
    public TarjetaCreditoPortTypeV1 soaTarjetaCreditoV1() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(TarjetaCreditoPortTypeV1.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaTarjetav1);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (TarjetaCreditoPortTypeV1) jaxWsProxyFactoryBean.create();
    }  
    
    @Bean(name = "soaTarjetaCreditoV12")
    public TarjetaCreditoPortTypeV12 soaTarjetaCreditoV12() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(TarjetaCreditoPortTypeV12.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaTarjetav12);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (TarjetaCreditoPortTypeV12) jaxWsProxyFactoryBean.create();
    }  
    
    @Bean(name = "soaBeneficiosV11")
    public BeneficiosSuperviellePortTypeV11 soaBeneficiosV11() {
      JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
      jaxWsProxyFactoryBean.setServiceClass(BeneficiosSuperviellePortTypeV11.class);
      jaxWsProxyFactoryBean.setAddress(urlSoaBeneficiosv11);
      
      jaxWsProxyFactoryBean = addInterceptors(jaxWsProxyFactoryBean);

      return (BeneficiosSuperviellePortTypeV11) jaxWsProxyFactoryBean.create();
    }  

}
